-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 11, 2020 at 08:28 AM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.1.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pestcontroldb`
--

-- --------------------------------------------------------

--
-- Table structure for table `purchase_req`
--

CREATE TABLE `purchase_req` (
  `id` int(11) NOT NULL,
  `branchid` int(11) NOT NULL,
  `pr_no_count` int(11) NOT NULL,
  `pr_no` varchar(300) NOT NULL,
  `prdate` date NOT NULL,
  `department` varchar(100) NOT NULL,
  `itemname` varchar(500) NOT NULL,
  `itemcode` varchar(500) NOT NULL,
  `descip` varchar(500) NOT NULL,
  `unit` varchar(20) NOT NULL,
  `remark` varchar(200) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_time` bigint(17) NOT NULL,
  `approvard_by` int(11) NOT NULL,
  `approvard_time` bigint(17) NOT NULL,
  `rejected_by` int(11) NOT NULL,
  `rejected_time` bigint(17) NOT NULL,
  `close_pr_time` bigint(17) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `purchase_req`
--
ALTER TABLE `purchase_req`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `purchase_req`
--
ALTER TABLE `purchase_req`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
