<?php
    
   $base = '../../';
   
   include($base.'_in/connect.php');
   
   header('content-type: application/json; charset=utf-8');
   
   header("access-control-allow-origin: *");
   
   if(isset($_POST['data'])){
      
    $con          = _connect();
      
    $data         = json_decode($_POST["data"]);

    $empid        = get_object_vars($data)['empid'];

    $branchid     = get_object_vars($data)['branchid'];

    $username     = strtoupper(get_object_vars($data)['username']);

    $dob          = get_object_vars($data)['dob'];

    $dataOfBrith  = (strtotime($dob))*1000;

    $phone        = get_object_vars($data)['phone'];
   
    $password     = get_object_vars($data)['password'];
         
    $email        = get_object_vars($data)['email'];

    $tempaddress  = get_object_vars($data)['tempaddress'];
   
    $department   = get_object_vars($data)['department'];
   
    $designation  = get_object_vars($data)['designation'];

    $role         = mysqli_fetch_assoc(mysqli_query($con,"SELECT role x FROM employeemaster WHERE id='$empid' AND branchid='$branchid' "))['x'];


    $employeejson = '{"branchid":"'.$branchid.'","username":"'.$username.'","phone":"'.$phone.'","password":"'.$password.'","role":"'.$role.'","email":"'.$email.'"}';
          
    $results      = mysqli_fetch_assoc(mysqli_query($con,"SELECT password,username,phone FROM employeemaster WHERE id='$empid' AND role <>'None' AND phone<>'$phone' AND branchid='$branchid' "));
 
    if(!$results){

        $updateemployee = mysqli_query($con,"UPDATE employeemaster SET password='$password', username = '$username', phone = '$phone' , role = '$role', employeejson='$employeejson' WHERE id='$empid' ");

        if ($updateemployee) {

            $personaljson       = mysqli_fetch_assoc(mysqli_query($con,"SELECT personaljson x FROM salarymaster WHERE employeeid='$empid' "))['x'];

            $personaldetails    = json_decode($personaljson);

            $gender             = get_object_vars($personaldetails)['gender'];
           
            $experience         = get_object_vars($personaldetails)['experience'];
           
            $qualification      = get_object_vars($personaldetails)['qualification'];
           
            $paddress           = get_object_vars($personaldetails)['paddress'];
           
            $language           = get_object_vars($personaldetails)['language'];
           
            $blood_group        = get_object_vars($personaldetails)['blood_group'];
           
            $emergency_Name     = get_object_vars($personaldetails)['emergency_Name'];
           
            $emergency_Contact  = get_object_vars($personaldetails)['emergency_Contact'];
           
            $emergency_Relation = get_object_vars($personaldetails)['emergency_Relation'];
           
            $machineid          = get_object_vars($personaldetails)['machineid'];
           
            $reference          = get_object_vars($personaldetails)['reference'];
           
            $category           = get_object_vars($personaldetails)['category'];
           
            $company            = get_object_vars($personaldetails)['company'];
           
            $pan                = get_object_vars($personaldetails)['pan'];
           
            $aadharcard         = get_object_vars($personaldetails)['aadharcard'];
           
            $driving            = get_object_vars($personaldetails)['driving'];
           
            $passport           = get_object_vars($personaldetails)['passport'];
           
            $salaryper          = get_object_vars($personaldetails)['salaryper'];
           
            $payment            = get_object_vars($personaldetails)['payment'];
           
            $bankname           = get_object_vars($personaldetails)['bankname'];
           
            $branchname         = get_object_vars($personaldetails)['branchname'];
           
            $ifsccode           = get_object_vars($personaldetails)['ifsccode'];
           
            $accountnumber      = get_object_vars($personaldetails)['accountnumber'];
           
            $smoking            = get_object_vars($personaldetails)['smoking'];
           
            $tobacco            = get_object_vars($personaldetails)['tobacco'];
           
            $liquor             = get_object_vars($personaldetails)['liquor'];

            $json = '{"gender":"'.$gender.'","dob":"'.$dataOfBrith.'","contactno":"'.$phone.'","email":"'.$email.'","experience":"'.$experience.'","qualification":"'.$qualification.'","address":"'.$tempaddress.'","paddress":"'.$paddress.'","language":"'.$language.'","blood_group":"'.$blood_group.'","emergency_Name":"'.$emergency_Name.'","emergency_Contact":"'.$emergency_Contact.'","emergency_Relation":"'.$emergency_Relation.'","machineid":"'.$machineid.'","reference":"'.$reference.'","category":"'.$category.'","department":"'.$department.'","designation":"'.$designation.'","company":"'.$company.'","pan":"'.$pan.'","aadharcard":"'.$aadharcard.'","driving":"'.$driving.'","passport":"'.$password.'","salaryper":"'.$salaryper.'","payment":"'.$payment.'","bankname":"'.$bankname.'","branchname":"'.$branchname.'","ifsccode":"'.$ifsccode.'","accountnumber":"'.$accountnumber.'","smoking":"'.$smoking.'","tobacco":"'.$tobacco.'","liquor":"'.$liquor.'"}';
    
            mysqli_query($con,"UPDATE salarymaster SET personaljson='$json' WHERE employeeid='$empid' AND branchid='$branchid' ");

            echo '{"status":"success"}';

        } else {

            echo '{"status":"falid1"}';

        }

       
      } else {
          
         echo '{"status":"falid2"}';
         
      }
         
      _close($con);
     
   } else {
        
      echo '{"status":"falid"}';
     
   }
   
?>