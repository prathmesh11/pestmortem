<?php 
    
    $base='../../';
    
    include($base.'_in/connect.php');
    
    header('content-type: application/json; charset=utf-8');
    
    header("access-control-allow-origin: *");
    
    if(isset($_POST['data'])) {
    
        if (session_status()==PHP_SESSION_NONE) { session_start(); }

        $created_by   = $_SESSION['employeeid'];
        
        $con          = _connect();
        
        $data         = get_object_vars(json_decode($_POST["data"]));
        
        $fromdate     = $data['fromdate'];
        
        $todate       = $data['todate']; 
        
        $branchid     = _clean($con, $data['branchid']);
        
        $employeeid   = _clean($con, $data['employeeid']);

        $reason       = _clean($con, $data['reason']);

        $appno        = mysqli_fetch_assoc(mysqli_query($con,"SELECT count(appno) x FROM leaveallotmentmaster "))['x'];

        $appno++;
        
        $appno        = 10000+$appno;
                
        $leavetype    = _clean($con, $data['leavetype']);

        $fromdateFull = date('d F,Y',$fromdate/1000);

        $todateFull   = date('d F,Y',$todate/1000);
        
        $numDays      = (abs($fromdate - $todate)/60/60/24/1000)+1;

        $lno          = 0;
        
        if($leavetype =="PL") {

            $lno = mysqli_fetch_assoc(mysqli_query($con,"SELECT paid_leave x FROM leaveopeningbalance WHERE employeeid='$employeeid'"))['x'];
            
            $qry = "paid_leave = paid_leave";

            $leave = "Paternity";

        } else if($leavetype =="CL") {

            $lno = mysqli_fetch_assoc(mysqli_query($con,"SELECT casual_leave x FROM leaveopeningbalance WHERE employeeid='$employeeid'"))['x'];
            
            $qry = "casual_leave = casual_leave ";

            $leave = "Casual";


        } else if($leavetype =="SL") { 

            $lno = mysqli_fetch_assoc(mysqli_query($con,"SELECT standanrd_leave x FROM leaveopeningbalance WHERE employeeid='$employeeid'"))['x'];
            
            $qry = "standanrd_leave = standanrd_leave"; 

            $leave = "Sick";

        }

        if($lno < $numDays){

            echo '{"status":"falid2"}';

        } else {

            
            $create = mysqli_query($con, "INSERT INTO leaveallotmentmaster (branchid,employeeid,appno,fromdate,todate,leavetype,created_by,created_time) VALUES ('$branchid','$employeeid','$appno','$fromdate','$todate','$leavetype','$created_by' ,'$CURRENT_MILLIS')");
            //$update = mysqli_query($con, "UPDATE leaveopeningbalance SET ". $qry ."- ".$numDays." WHERE employeeid = '$employeeid'");

            if($create) {

                $descripation = "Dear Sir/Mamad, Subject : Leave Application. I am SAMEER BAYANI From SEWREE Branch. I want a " . $leave . " leave on date " . $fromdateFull . " To " . $todateFull . " . " . $reason;


                mysqli_query($con, "INSERT INTO leaveapplication (branchid,employeeid,appno,descripation,created_by,created_time) VALUES ('$branchid','$employeeid','$appno','$descripation','$created_by' ,'$CURRENT_MILLIS')");

                echo '{"status":"success"}';

            } else {

                echo '{"status":"falid1"}';

            }
        
        }
            
        _close($con);
        echo trim(ob_get_clean());
    }
    else {
        echo '{"status":"falid3"}';
    }
?>