<?php
session_start();
include('_in/industry.php');
if (isset($_SESSION['phone']) && isset($_SESSION['username']) && isset($_SESSION['aid']) && isset($_SESSION['role'])) {
    header("Location: process/dashboard.php");
 }
?>
<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="css/bootstrap-glyphicons.css">
  <script src="js/jquery-3.3.1.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <meta name="google-site-verification" content="K6GIAmoNy0oWc5G5IyhT2f3XLfTHP7yXDSsbMdmD49E" />
  <style>
      body{
          background:#eee;
      }
      .container{
          margin-top:60px;
      }
      .input-group{
          margin-bottom:10px;
      }
      .section{
            padding:50px;
            background:#fff;
            -webkit-box-shadow: 0 10px 6px -6px #777;
            -moz-box-shadow: 0 10px 6px -6px #777;
            box-shadow: 0 10px 6px -6px #777;
      }
      .logo{
          margin-bottom:10px;
      }
      .phone_error{
        color:red;
        margin:0px;
        padding:2px;
        text-align:center;
        display:none;
      }
      .pass_error{
        color:red;
        margin:0px;
        padding:2px;
        text-align:center;
        display:none;
      }
      #wrong{
        color:red;
        margin:0px;
        padding:2px;
        text-align:center;
        display:none;  
      }
      .input-group{
          margin:0px;
      }
      .form-control{
          border-radius:0px;
      }
      .loader {
        border: 5px solid #f3f3f3;
        border-radius: 50%;
        border-top: 5px solid #3498db;
        width: 35px;
        height: 35px;
        display:none;  
        float:center;
        text-align:center;
        -webkit-animation: spin 2s linear infinite; /* Safari */
        animation: spin 2s linear infinite;
}

/* Safari */
@-webkit-keyframes spin {
  0% { -webkit-transform: rotate(0deg); }
  100% { -webkit-transform: rotate(360deg); }
}

@keyframes spin {
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
}
</style>
</head>
<body>

<div class="container-fluid">
    <div class="row">
        <div class="col-sm-4"></div>
        <div class="col-sm-4 logo">
        <?php 
if($industry_name=='Agarwal Industries'){
        ?>
        <img src="img/icon/logo.png" />
       
        <?php 
}else{
   // echo '<h3 style="font-size:37px;">'.$industry_name.'</h3>';
}
?>
 </div>
        <div class="col-sm-4"></div>
    </div>
<div class="row">
    <div class="col-sm-3"></div>
    
    <div class="col-sm-6 section">
  <div class="input-group">
    <span class="input-group-addon"><img src="img/icon/glyphicons-user.png" width="18px" height="19px"/></span>
    <input type="text" class="form-control" id="inp-phone" placeholder="Phone">
</div>
<p class="phone_error">Please Enter Correct Phone Number</p>  
<br>
  <div class="input-group">
    <span class="input-group-addon"><img src="img/icon/glyphicons-key.png" width="18px" height="19px"></span>
    <input type="password" class="form-control" id="inp-pass" placeholder="Password">
</div>
<p class="pass_error">Please Enter Correct Password</p> 
<div style="clear:both;"></div>
<hr style="margin:5px;">
<p id="wrong">Wrong Credentials Please Try Again</p>
<center><div class="loader"></div></center>
<div class="col-sm-6" align="center"><button class="btn btn-primary btn-sm btn-block" style="margin:5px;" id='btn-login'>Login</button></div>
<div class="col-sm-6" align="center"><button class="btn btn-warning btn-sm btn-block" style="margin:5px;" id='btn-reset'>Reset</button></div>
    </div>
    <div class="col-sm-3"></div>
    
</div>
</div>
</body>
</html>
<script>


$('#inp-pass').keypress(function (e) {
  if (e.which == 13) {
    $('#btn-login').click();
  }
});

$('#btn-login').on('click', function () {
    var phone = $('#inp-phone').val(),
        pass = $('#inp-pass').val();
    var valid = true;
    var regphone=/^[789]\d{9}$/;
    if (!regphone.test(phone)) {
        valid = valid * false;
        $(".phone_error").show();
    } else {
        valid = valid * true;
        $(".phone_error").hide();
    }
    if (pass.length == 0) {
        valid = valid * false;
        $(".pass_error").show();
    } else {
        valid = valid * true;
        $(".pass_error").hide();
    }
    if (valid) {
        $(this).hide();
        $('.loader').show();
        $.ajax({
            type: "POST",
            data: "phone=" + phone + "&pass=" + pass,
            url: '_api/login/login.php',
            success: function (res) {
                if (res.status == 'success') {
                    $('#wrong').hide();
                    $('.loader').hide();
                    $('#btn-login').show();
                    window.location.href = "process/dashboard.php";
                } else {
                    $('.loader').hide();
                    $('#wrong').show();
                    $('#btn-login').show();
                }
            }
        });
    }
});

$('#btn-reset').on('click', function () {
    $('inp-phone').val("");
    $('inp-password').val("");

});
$('a[title="Hosted on free web hosting 000webhost.com. Host your own website for FREE."]').css("display","none");
</script>