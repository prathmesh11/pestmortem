function openNav(e) {
    $(e).attr('onclick', 'closeNav(this)');
    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
      document.getElementById("mySidenav").style.width = "80%";
      $('.viewer').fadeToggle(2000);
    } else {
      document.getElementById("mySidenav").style.width = "20%";
     // $('.img-nav').attr('style','width:48px;height:48px;');
      $('.viewer').fadeToggle(2000);
    }
  }
  
  function closeNav(e) {
    $(e).attr('onclick', 'openNav(this)');
    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
      document.getElementById("mySidenav").style.width = "0%";
      $('.viewer').hide();
    } else {
      document.getElementById("mySidenav").style.width = "5%";
      $('.img-nav').attr('style','');
      $('.viewer').hide();
    }
  }
  