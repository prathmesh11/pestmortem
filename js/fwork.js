	//----------------------------- S : FRAMWORK ---------------------------------------------------------//
    jQuery.fn.extend({
    	sval: function () {
    		var $target = this;
    		var val = $target.val();
    		val = val.replace(/\t/g, " ");
    		val = val.replace(/"/g, '\"');
    		val = val.replace(/\n/g, ' ');
    		val = val.replace(/\\/g, '\\\\');
    		return val;
    	},
    	stext: function () {
    		var $target = this;
    		var val = $target.text();
    		val = val.replace(/\t/g, " ");
    		val = val.replace(/"/g, '\"');
    		val = val.replace(/\n/g, ' ');
    		val = val.replace(/\\/g, '\\\\');
    		return val;
    	},
    	mval: function () {
    		var $target = this;
    		var val = $target.val();
    		val = val.replace(/\n/g, '--n');
    		val = val.replace(/"/g, '\"');
    		val = val.replace(/'/g, '\'');
    		val = val.replace(/\\/g, '\\\\');
    		return val;
    	}
    });

    var modals = {
    	validation: function (modal) {
    		var valid = true,
    			validarr = [],
				numberRegex = /^[+-]?\d+(\.\d+)?([eE][+-]?\d+)?$/,
				regphone=/^[789]\d{9}$/,
				regpassword=/^(?=.*[\d])(?=.*[!@#$%^&*])[\w!@#$%^&*]{6,16}$/;

    		$('#' + modal + ' input, #' + modal + ' select,#' + modal + ' textarea').each(
    			function (index) {
					var input = $(this);
    				switch (input.attr('data-role')) {
    					case 'text':
    						if (input.sval() == "") {
    							input.css('border-bottom', '1px solid red');
    							valid =  valid * false;
    						} else {
    							input.css('border-bottom', '1px solid #2ecc71');
    							valid =  valid * true;
    						};
							break;
						case 'textarea':
    						if (input.sval().length == 1) {
								input.css('border-bottom', '1px solid red');
								valid =  valid * false;
    						} else {
    							input.css('border-bottom', '1px solid #2ecc71');
								valid =  valid * true;
    						};
							break;
						case 'phone':
						if (!regphone.test(input.sval())) {
    							input.css('border-bottom', '1px solid red');
    							valid =  valid * false;
    						} else {
    							input.css('border-bottom', '1px solid #2ecc71');
    							valid =  valid * true;
    						};
							break;
						case 'password':
						if (!regpassword.test(input.sval())) {
    							input.css('border-bottom', '1px solid red');
    							valid =  valid * false;
    						} else {
    							input.css('border-bottom', '1px solid #2ecc71');
    							valid =  valid * true;
    						};
    						break;
    					case 'number':
    						if (!numberRegex.test(input.sval())) {
    							input.css('border-bottom', '1px solid red');
    							valid = valid *  false;
    						} else {
    							input.css('border-bottom', '1px solid #2ecc71');
    							valid =  valid *  true;
    						};
    						break;
    					case 'select':
    						if (input.val() == "Select") {
    							input.css('border-color', 'red');
    							valid = valid *  false;
    						} else {
    							input.css('border-color', '#2ecc71');
    							valid =  valid *  true;
    						};
    						break;
    					case 'date':
    						if (input.val() == "") {
    							input.css('border-color', 'red');
    							valid =  valid * false;
    						} else {
    							input.css('border-color', '#2ecc71');
    							valid =  valid * true;
    						};
							break;
    				}

    				validarr.push(valid);
    			});
    		var len = validarr.length;
    		var count = 0;
    		for (var i in validarr) {
    			if (validarr[i] == true) {
    				count++;
    			}
    		}
    		if (len == count) {
    			return true;
    		} else {
    			return false;
    		}
    	},
    	getvalue: function (modal) {
    		var getarr = [];
			$('#' + modal + ' input, #' + modal + ' select,#' + modal + ' textarea').each(
    			function (index) {
    				var input = $(this);
    				var key = input.attr('data-name');
					var value ='Select';
					if(input.val() != null){
						if (typeof input.attr('data-stamp') !== 'undefined') {
							value = input.attr('data-stamp');
						}else{
							value = input.sval();
						}
							var type=input.attr('type');
							if(type=='radio'){
								var name=input.attr('name');
								value=$("input[name='"+name+"']:checked").val();
							}
					}
    				if (typeof input.attr('data-name') !== 'undefined') {
    					getarr.push({
    						"key": key,
    						"value": value
    					});
    				}
    			});
    		return getarr;
    	},
		putvalue: function (modal,json) {
			for(var i in json){
					var last=json[i];
					for(var keys in last){
						$('#' + modal + ' input, #' + modal + ' select,#' + modal + ' textarea').each(
    				function (index) {
					var input = $(this);
					
					if (typeof input.attr('data-name') !== 'undefined') {
						var key = input.attr('data-name');
						if(key==keys){
							if (input.hasClass("datepicker")){
								if(last[keys]!=""){
									var datepicker = new Date(parseFloat(last[keys]));
									var month = datepicker.getMonth(); //months from 1-12
									var day = datepicker.getDate();
									var year = datepicker.getFullYear();
									var fulldate=day+' '+monthNames[month]+', '+year;
									input.val(fulldate);
									input.attr('data-stamp',last[keys]);
								}
							}else{
								var type=input.attr('type');
								if(type=='radio'){
									var value=input.val();
									if(last[keys]==value){
										input. prop("checked", true);
									}
								}else{
									input.val(last[keys]);
								}
							}
							$(input).trigger('change');
						}
					}
    			 });
					}
				}
		},
    	clear: function (modal) {
    		$('#' + modal + ' input, #' + modal + ' select,#' + modal + ' textarea').each(
    			function (index) {
    				var input = $(this);
    				switch (input.attr('data-role')) {
    					case 'text':
    						input.val("");
							break;
						case 'textarea':
    						input.val("");
    						break;
    					case 'number':
    						input.val("");
    						break;
    					case 'select':
							input.val("Select");
							$(input).trigger('change');
    						break;
    					case 'date':
    						input.val("");
							break;
						case 'phone':
							input.val("");
						case 'password':
    						input.val("");
							break;
						case 'radio':break;
						default: input.val("");
							break;
    				}
    			});
    	},
		gettable:function(table){
			var arr1=[];
			$('#'+table+' tbody tr').each(function(){	
				var arr=[];
				var len = $(this).find('td').length;
					
				for(var i=1;i<(len-1);i++){
					var value=$(this).find('td:eq('+i+')').stext();
					arr.push(value);
				}
				arr1.push({table:arr});
		});
			arr1.shift();
			if(arr1!=''){
				return arr1;
			}else{
				return false;
			}
		},
		gettable1:function(table){
		
			var arr1=[];
			$('#'+table+' tbody tr').each(function(){	
				var arr=[];
				var len = $(this).find('td').length;
					
				for(var i=1;i<(len-1);i++){
					var value=$(this).find('td:eq('+i+')').stext();
					arr.push(value);
				}
				arr1.push({table:arr});
		});
			
			if(arr1!=''){
				return arr1;
			}else{
				return false;
			}
		},
		tabletdbyclass:function(table){
			var arr=[];
			$('#'+table+' > tbody > tr').each(function(){	
				var str='';
					var len=$(this).children('td').length;
					for(var i=0;i<=(len-1);i++){
						var classname=$(this).find('td:eq('+i+')').attr('class');
						if (typeof classname !== 'undefined') {
						var value=$(this).find('.'+classname).stext();
						str+=',"'+classname+'":"'+value+'"';
						
						}
					}
					arr.push(JSON.parse('{'+str.substring(1)+'}'));	
			});

			if(arr!=''){
				return arr;
			}else{
				return false;
			}
		}
    }

    function checker(modal) {
    	if (modals.validation(modal)) {
            var data=modals.getvalue(modal);
            var str='';
                for(var i in data){
                   str+=',"'+data[i].key+'":"'+data[i].value+'"'; 
                }
                str=str.substring(1);
                str='{'+str+'}';
								var obj=jQuery.parseJSON(str);
							
    		return obj;
    	}else{
			return false;
		}
    }

    Number.prototype.round = function (places) {
    	return +(Math.round(this + "e+" + places) + "e-" + places);
    }

    function capitalize_Words(str) {
    	return str.replace(/\w\S*/g, function (txt) {
    		return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
    	});
    }

    function remover(e) {
    	$(e).parent().parent().fadeOut(1000, function () {
    		$(this).remove();
    	});
	}
	const monthNames = ["January", "February", "March", "April", "May", "June","July", "August", "September", "October", "November", "December"];
    //----------------------------- E : FRAMWORK ---------------------------------------------------------//