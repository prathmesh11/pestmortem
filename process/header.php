<style>
    hr.style-hr {
        border: 0;
        height: 1px;
        background-image: linear-gradient(to right, rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.75), rgba(0, 0, 0, 0));
    }
    .table-list td,
    .table-list th {
        border: 1px solid #ddd;
        padding: 1px !important;
        font-size: 13px;
    }
    .table-list td{
        padding-top: 10px !important;

    }

    .table-list tr:nth-child(even) {
        background-color: #f2f2f2;
    }

    .table-list th {
        padding-top: 5px;
        padding-bottom: 5px;
        text-align: center;
        background-color: #16a085;
        color: white;
    }
    .reportnav{
        border-radius:0px;
        text-align: center;
    -webkit-transition-duration: 0.4s; /* Safari */
    transition-duration: 0.4s;
    text-decoration: none;
    overflow: hidden;
    cursor: pointer;
    }
    .table-list td,
    .table-list th {
        border: 1px solid #ddd;
        padding: 1px !important;
        font-size: 13px;
    }
    .table-list td{
        padding-top: 10px !important;

    }

    .table-list tr:nth-child(even) {
        background-color: #f2f2f2;
    }

    .table-list th {
        padding-top: 5px;
        padding-bottom: 5px;
        text-align: center;
        background-color: #16a085;
        color: white;
    }

</style>

<div class="container-fluid">
    <div class=" content">
        <h2 align="center">REPORTS</h2>
        <div class="col-sm-12">
        <div class="col-sm-3" align="center"><a class="reportnav btn btn-block btn-lg btn-primary" href="/process/report/master">Employee Reports</a></div>
        <div class="col-sm-3" align="center"><a class="reportnav btn btn-block btn-lg btn-primary" href="/process/report/transaction">Certificate Reports</a></div>
        <div class="col-sm-3" align="center"><a class="reportnav btn btn-block btn-lg btn-primary" href="/process/report/salary">Salary Reports</a></div>
        <div class="col-sm-3" align="center"><a class="reportnav btn btn-block btn-lg btn-primary" href="/process/report/purchase">Purchase Reports</a></div>
        </div>
      <!-- <a class="btn btn-primary" href="/process/transaction/report/customer-list/index.php" >CUSTOMER LIST</a>
      <a class="btn btn-primary" href="/process/transaction/report/supplier-list/index.php" >SUPPLIER LIST</a>
      <a class="btn btn-primary" href="/process/transaction/report/item-rate-list/index.php" >ITEM RATE LIST</a>
      <a class="btn btn-primary" href="/process/transaction/report/bill-of-material-list/index.php" >BILL OF MATERIAL LIST</a>
   <hr>
      <a class="btn btn-primary" href="/process/dashboard.php" >CUSTOMER ORDER LIST</a>
      <a class="btn btn-primary" href="/process/transaction/report/work-order-list" >WORK ORDER LIST</a>
      <a class="btn btn-primary" href="/process/transaction/report/purchase-order-list" >PURCHASE ORDER LIST</a>
      <a class="btn btn-primary" href="/process/transaction/report/grn-list" >GRN LIST</a>
      <a class="btn btn-primary" href="/process/transaction/report/qc-list" >Q.C LIST</a>
      <a class="btn btn-primary" href="/process/transaction/report/material-issue-list" >MATERIAL ISSUE LIST</a>
      <a class="btn btn-primary" href="/process/transaction/report/material-hold-list" >MATERIAL HOLD LIST</a> -->

      
