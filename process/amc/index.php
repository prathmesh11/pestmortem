<?php 

    $base = '../../';

    include('header.php');

    if (!in_array("Annual Work Contract", $arrysession)) {
   
      echo "<script>window.location.href='/process/dashboard.php';</script>";
     
      exit;

  }

    $navenq1 = 'background:#1B1464;';

    if (session_status() == PHP_SESSION_NONE) { session_start(); }
    $sessionby = $_SESSION['employeeid'];
    $branchid  = $_SESSION['branchid'];

    $signature = mysqli_fetch_assoc(mysqli_query($con,"SELECT signature x FROM branchmaster WHERE branchid='$branchid'"))['x'];

?>
<br>
<style>

.input-container {

    max-width: 300px;
    background-color: #EDEDED;
    border: 1px solid #DFDFDF;
    border-radius: 5px;
}

input[type='file'] {
    display: none;
}

.file-info {
    font-size: 0.9em;
}

.browse-btn {
    background: #03A595;
    color: #fff;
    min-height: 35px;
    padding: 10px;
    border: none;
    border-top-left-radius: 5px;
    border-bottom-left-radius: 5px;
}

.browse-btn:hover {
    background: #4ec0b4;
}

@media (max-width: 300px) {
    button {
        width: 100%;
        border-top-right-radius: 5px;
        border-bottom-left-radius: 0;
    }

    .file-info {
        display: block;
        margin: 10px 5px;
    }
}

</style>

<img style="display:none;" id="imageid" src="<?php echo $base;?>img/PestMortemLogo.jpg">

<img style="display:none;" id="signature" src="<?php echo $base.$signature;?>">

<div class="table-ui container-fluid">

    <div class="tr row">

        <div class="col-sm-2 th">Enquiry No.</div>
        <div class="col-sm-5 th">Customer Info</div>
        <div class="col-sm-1 th">Enquery For</div>
        <!-- <div class="col-sm-2 th">Offer</div> -->
        <div class="col-sm-2 th">Work_Contract</div>
        <div class="col-sm-2 th">Action</div>

    </div>

    <?php

    $result=mysqli_query($con,"SELECT enquiryid ,category,customerid ,enquirynote ,followupnote ,followuptime,contractid FROM crmmaster WHERE category = 'Pest Control' AND created_time<>0 AND created_by<>0 AND enquiry_time<>0 AND enquiry_by<>0 AND wo_time=0 AND wo_by=0 AND close_enquiry_time=0 AND close_enquiry_by=0 AND branchid='$branchid' ORDER BY created_time DESC");

      while($rows = mysqli_fetch_assoc($result)){

        $enquiryid=$rows['enquiryid'];
        $customerid=$rows['customerid'];

        $cifo=mysqli_fetch_assoc(mysqli_query($con,"SELECT * FROM customermaster WHERE customerid='$customerid'"));
        $result4=mysqli_query($con,"SELECT * FROM workcontractbill WHERE enquiryid ='$enquiryid'");
        while($rows3 = mysqli_fetch_assoc($result4)){
            $count++;
        $para = "'".$rows3['billid']."','".$rows['enquiryid']."','1'";    

      }

  ?>

    <div class="row tr">

        <div class="col-sm-2 td" style="word-wrap:break-word;">

            <?php echo $rows['enquiryid']; ?>

        </div>

        <div class="col-sm-5 td">

            <?php

                echo 'Company : '.$cifo['customername']; 
                echo '<br>Address : '.get_object_vars(json_decode($cifo['customerjson']))['address']; 
                echo '<br>Name  : '.get_object_vars(json_decode($cifo['contactjson'])[0])['name'];
                echo '<br>Dept. : '.get_object_vars(json_decode($cifo['contactjson'])[0])['department'];
                echo '<br>Mobile : <a href="tel:'.get_object_vars(json_decode($cifo['contactjson'])[0])['mobile'].'">'.get_object_vars(json_decode($cifo['contactjson'])[0])['mobile'].'</a>';
                echo '<br>Landline : '.get_object_vars(json_decode($cifo['contactjson'])[0])['landline'];
                echo '<br>Email : <a href="mailto:'.get_object_vars(json_decode($cifo['contactjson'])[0])['email'].'">'.get_object_vars(json_decode($cifo['contactjson'])[0])['email'].'</a>';

            ?>

        </div>

        <div class="col-sm-1 td">

            <?php echo $rows['category']; ?>

        </div>

        <!-- <div class="col-sm-2 td">

            <div class="input-container">

                <input type="file" class="real-input" name="quatation" accept="application/pdf">

                <button class="browse-btn">
                    Browse Files
                </button>

                <span class="file-info">Upload a file</span>

            </div>

            <button class="btn btn-primary btn-sm btn-block" data-enquiryid="<?php echo $rows['enquiryid']; ?>"
                onclick="upload(this)">Upload </button>


            <?php 

                $enquir=str_replace('/', '-',$rows['enquiryid']);

                $path="api/offer/".$enquir.'.pdf';

                if(file_exists($path)){

                    echo '<br><center><a href="'.$path.'" target="_blank">View PDF</a></center>';

                } else {

                    echo '<br>Please Upload .pdf File';

                }

            ?>

        </div> -->



        <div class="col-sm-2 td" style="word-wrap:break-word;">

          <?php 

            $contractid=mysqli_fetch_assoc(mysqli_query($con,"SELECT contractid,servicelocation FROM workcontract WHERE enquiryid='$enquiryid'"));

              if($contractid['contractid']){

                if ($rows['category']=="Pest Control") {

                  $servicelocation = json_decode($contractid['servicelocation']);

                  foreach ($servicelocation as $i){

                    $location = get_object_vars($i)['servicelocation'];  
                    
                    echo '<button class="btn btn-sm btn-primary" data-contractid="'.$contractid['contractid'].'" data-location="'.$location.'" onclick="servicepdf(this)"> Service Pdf</button>';

                  }

                  echo '<button class="btn btn-sm btn-primary" data-contractid="'.$contractid['contractid'].'" onclick="contractpdf(this)">Pest Control</button>';
                
                } 

              }

          ?>

        </div>

        <div class="col-sm-2 td">

            <?php if($contractid){ ?>

            <button class="btn btn-success btn-sm btn-block" data-link1="wo_time" data-link2="wo_by"
                data-enquiryid="<?php echo $rows['enquiryid']; ?>" onclick="confirms(this)">Order Confirm</button>

            

            <a class="btn btn-sm btn-block btn-warning"
                href="workcontract.php?enquiryid=<?php echo $rows['enquiryid']; ?>&contractid=<?php echo $rows['contractid']; ?>&edit=true">Edit</a>

            <?php } ?>


            <button class="btn btn-sm btn-block btn-danger" data-enquiryid="<?php echo $rows['enquiryid']; ?>"
                onclick="closes(this)">Order Close</button>

            <?php if(!$contractid){ ?>

                <a class="btn btn-sm btn-block btn-primary"
                    href="workcontract/pest-control.php?enq=<?php echo $rows['enquiryid']; ?> &customerid=<?php echo $customerid;?>">Pest
                    Control Contract</a>

            <?php } ?>

        </div>

    </div>

    <?php

    }

  ?>

</div>

<a class="btn" style="font-size:25px;border-radius:50px;width:60px;height:60px;position: fixed;bottom: 40px;
    right: 40px;background:#eb2f06;color:#fff;" href="/process/amc/workcontract.php">+</a>


<?php 
include('footer.php');
?>
<script>


function getBase64Image(img) {
    var canvas = document.createElement("canvas");
    canvas.width = img.width;
    canvas.height = img.height;
    var ctx = canvas.getContext("2d");
    ctx.drawImage(img, 0, 0);
    var dataURL = canvas.toDataURL("image/png");
    return dataURL.replace('/^data:image\/(png|jpg);base64,/', "");
}

function contractpdf(e) {
  
  var contractid = $(e).data('contractid');

  $.ajax({

    type: "POST",

    data: "contractid=" + contractid,

    url: 'api/contractpdf.php',

    cache: false,

    success: function (res) {

      if (res.status == 'success') {

        var content = [];

        var json = res.json[0];

        let base64 = getBase64Image(document.getElementById("imageid"));

        let signature = getBase64Image(document.getElementById("signature"));

        content.push({
          text: 'PEST MORTEM (INDIA) PRIVATE LIMITED',style: 'header'
        });

        content.push({
          text: '(Approved by National Plant Protection Organization, Goverment of India)',style: 'subheader'
        });

        content.push({
          text: 'AN ISO 9001 : 2008 CERTIFIED COMPANY',style: 'subheader1'
        });

        content.push({
          text: json.address + '\n GST No : ' + json.branchgstnumber,alignment: 'center',fontSize: 11
        });

        content.push({
          text: 'EXPERT IN',alignment: 'center',fontSize: 11,bold: true
        });

        content.push({
          text: 'PEST CONTROL SERVICES AND FUMIGATION OF EXPORT CARGO, SHIPS, CONTAINERS',style: 'subheader'
        });

        if (json.category == 'Pest Control') {

          content.push({
            text:"\n"
          });

          content.push({
            columns: [{ text: 'Contract No. : ' + json.contractid, fontSize: 9, bold: true
              }, {
              text: 'Dated : ' + json.contractdate,alignment: 'right',fontSize: 9, bold: true}]
          });

          content.push({
            table: {
              widths: ['*'],
              body: [
                [{text: 'Pest Control Service Agreement',alignment: 'center',fontSize: 10,bold: true}],
              ]
            }
          });

          var servicelocation = json.servicelocation;

          if (Object.keys(servicelocation).length > 1) {

            var servicelocations="As Per Attached Sheet";

            
          } else {

            var servicelocations=servicelocation[0]['servicelocation'];

          }

          // console.log(Object.keys(servicelocation).length);
          content.push({
            table: {
              widths: ['*', 10, '*'],
              body: [
                [{text: 'Purchaser',alignment: 'center',fontSize: 10,bold: true,border: [false, false, false, true]},{text: '',border: [false, false, false, false]}, {text: 'Premises',alignment: 'center',fontSize: 10,bold: true,border: [false, false, false, true]}],
                [{text: 'Name     : ' + json.companyname,fontSize: 10,border: [false, false, false, true]}, {text: '',border: [false, false, false, false]}, {text: 'Name     : ' + json.companyname,fontSize: 10,border: [false, false, false, true]}],
                [{text: 'Address : ' + json.companyaddress,fontSize: 10,border: [false, false, false, true]}, {text: '',border: [false, false, false, false]},{text: 'Address : ' + servicelocations,fontSize: 10,border: [false, false, false, true]}],
                [{text: 'GST No  : ' + json.custgstnumber,fontSize: 10,border: [false, false, false, true]},{text: '',border: [false, false, false, false]},{text: 'GST No  : ' + json.custgstnumber,fontSize: 10,border: [false, false, false, true]}],
                [{text: 'Email      : ' + json.email,fontSize: 10,border: [false, false, false, true]},{text: '',border: [false, false, false, false]}, {text: 'Email      : ' + json.email,fontSize: 10,border: [false, false, false, true]}],
                [{text: 'Phone     : ' + json.mobile,fontSize: 10,border: [false, false, false, true]},{text: '',border: [false, false, false, false]},{text: 'Phone     : ' + json.mobile,fontSize: 10,border: [false, false, false, true]}],
              ]
            }
          });

          content.push({

            text: '\nThis agreement is for an initial period of (' + json.contractstart + ' TO ' + json.contractend + ') from the date of the frist service and, unless canceled by the purchaser, will automatically continue on a monthly basis until canceled by either party upon thirty days notice.\n\n',fontSize: 9,bold: true

          });

          content.push({
            table: {
              widths: ['*'],
              body: [
                [{text: 'BASIC COVERAGE - TREATMENT OR INSPECTION FOR ',alignment: 'center',fontSize: 9,bold: true}],
              ]
            }
          });

          var table1 = {
            widths: ['auto','*', 50],
            dontBreakRows: true,
            body: []
          };
          
          var table2 = {
            widths: ['auto','*', 50],
            dontBreakRows: true,
            body: []
          };
          
          var services = json.servicejson;
          var serviceslen = parseInt(services.length / 2);

          var counter = 0;

          if (services.length < 5) {

            var count = 1;
            table1['body'].push([{text:'Sr No.',bold:1}, {text:'Type of Services',bold:1,alignment:'left'},{text:'Frequency',bold:1,alignment:'center'}])

            for (var i in services) {

              var arr = [];
              arr.push({text:count,alignment:'center'},services[i].servicename, {text:services[i].frequency,alignment:'center'});
              table1['body'].push(arr);

              count++;

            }

            content.push({

              columns: [{
                style: 'tablfont',
                table: table1,
                layout: 'noBorders',
                margin:[120,0]
              }]

            })
            
          } else {

            table1['body'].push([{text:'Sr No.',bold:1}, {text:'Type of Services',bold:1,alignment:'left'},{text:'Frequency',bold:1,alignment:'center'}])
            table2['body'].push([{text:'Sr No.',bold:1}, {text:'Type of Services',bold:1,alignment:'left'},{text:'Frequency',bold:1,alignment:'center'}])

            var count = 1;

            for (var i in services) {

              var arr = [];

              arr.push({text:count,alignment:'center'},services[i].servicename, {text:services[i].frequency,alignment:'center'});

              if (serviceslen <= counter) {

                table2['body'].push(arr);

              } else {

                table1['body'].push(arr);

              }

              count++;

              counter++;

            }

            content.push({

              columns: [{
                style: 'tablfont',
                table: table1,
                layout: 'noBorders'
              }, {
                width: 10,
                text: ''
              }, {
                style: 'tablfont',
                table: table2,
                layout: 'noBorders'
              }]

            })

          }

          var table3 = {
            widths: ['auto','*', 50, 50],
            dontBreakRows: true,
            body: []
          };
          
          if (json.enquipmentjson.length > 0) {

            content.push({
              table: {
                widths: ['*'],
                body: [
                  [{text: 'INSTALLATION OF EQUIPMENT',alignment: 'center',fontSize: 9,bold: true}],
                ]
              }
            });


            table3['body'].push([{text:'Sr No.',bold:1}, {text:'Type of Equipments',bold:1,alignment:'left'},{text:'Returnable',bold:1,alignment:'center'},{text:'Quantity',bold:1,alignment:'center'}])

            var enquipmentjson = json.enquipmentjson;
            var count = 1;
            for (var i in enquipmentjson) {

              var arr = [];

              arr.push({text:count,alignment:'center'},enquipmentjson[i].machinename, {text:enquipmentjson[i].returnable,alignment:'center'},{text:enquipmentjson[i].qty,alignment:'center'});

              table3['body'].push(arr);
              count++;

            }

            content.push({
              columns: [{
                style: 'tablfont',
                table: table3,
                layout: 'noBorders',
                margin:[120,0]
              }]
            })

          }


          content.push({
            table: {
              widths: ['*', 100, 100],
              body: [
                [{text: 'PAYMENT DETAILS',colSpan: 3,alignment: 'center',fontSize: 9,bold: true}, {}, {}],
                [{text: 'Mode of payment : ' + json.paymentby + '\nAdditional Declaration (if any)\n' + json.additinal,rowSpan: 6,fontSize: 9,bold: true}, {text: 'Contract Value',colSpan: 2,border: [true, true, true, true],alignment: 'center',fontSize: 9,bold: true}, {}],
                ['', {text: 'Service Charges',fontSize: 9,bold: true}, {text: json.amount,fontSize: 9,bold: true}],
                ['', {text: 'GST %',fontSize: 9,bold: true}, {text: (parseFloat(json.cgstper) + parseFloat(json.sgstper) + parseFloat(json.igstper)) + ' % ',fontSize: 9,bold: true}],
                ['', {text: 'GST Amount',fontSize: 9,bold: true}, {text: (parseFloat(json.cgstamt) + parseFloat(json.sgstamt) + parseFloat(json.igstamt)) + '.00',fontSize: 9,bold: true}],
                // ['', {text:'Other Charges',fontSize: 9,bold:true}, {text:  json.othercharges  ,fontSize: 9,bold:true}],
                ['', {text: 'Total Annual Amount',fontSize: 9,bold: true}, {text: json.totalcontractamt,fontSize: 9,bold: true}],
                ['', {text: 'No Of Bill',fontSize: 9,bold: true}, {text: json.billqty,fontSize: 9,bold: true}],
                [{text: convertNumberToWords(json.totalcontractamt),colSpan: 3,fontSize: 9,bold: true,alignment: 'center'}, {}, {}],
              ]
            }
          });

          content.push({
            table: {
              headerRows: 1,
              widths: ['*'],
              body: [
                [''],
                ['']
              ]
            },
            layout: 'headerLineOnly'
          }); //LINE CODE

          content.push({
            table: {
                  widths: [300],
                  body:[
                    
                    [{text:'Company Name : Pest Mortem (India) Pvt. Ltd.  ',border: [true, false, true, false],alignment:'left',fontSize: 9,fonts: 'TimesNewRoman'}],
                    [{text:'Bank Name     : '+ json.bankname  + ' , ' + json.bankbranchname ,border: [true, false, true, false],alignment:'left',fontSize: 9,fonts: 'TimesNewRoman'}],
                    [{text:'Account No     : '+ json.accountno,border: [true, false, true, false],alignment:'left',fontSize: 9,fonts: 'TimesNewRoman'}],
                    [{text:'RTGS/NEFT/IFSC Code : '+ json.ifsccode,border: [true, false, true, false],alignment:'left',fontSize: 9,fonts: 'TimesNewRoman'}],
                  ]
                },layout: 'noBorders',

          });

          content.push({
            text:'\n',
            layout: 'noBorders',

          });

          content.push({
            table: {
              widths: ['*','*'],
              body:[
                
                [{text: 'For Pest Mortem(India) Pvt. Ltd. ',alignment:'left',fontSize: 10,},{text: json.companyname,alignment:'right',fontSize: 10}],
                [{image: signature,width: 100,height: 50,margin:[0,0,0,0],colSpan:2},{}],
                [{text: 'Authorised Signatory ',alignment:'left',fontSize: 10},{text: 'Authorised Signatory ',alignment:'right',fontSize: 10}],
              ]
            },layout: 'noBorders',

          });

        }

       

        if (json.ftype == '') {
          


          //   content.push({table:{headerRows: 1,widths: ['*'],body: [[''],['']]}, layout: 'headerLineOnly' }); //LINE CODE
          // content.push({ columns: [{text:'Contract No. : '+json.contractid,fontSize: 9,bold:true},{text:'Dated : '+json.contractdate,alignment:'right',fontSize:9,bold:true} ]});
          // content.push({ text:'Period : '+json.contractstart+' TO '+ json.contractend,fontSize: 9,bold:true});
          // content.push({table:{headerRows: 1,widths: ['*'],body: [[''],['']]}, layout: 'headerLineOnly' }); //LINE CODE
          // content.push({ text:'In Consultation and as agreed upon a sum of Rs. '+json.amount+' + GST @ '+( parseFloat(json.cgstper)+parseFloat(json.sgstper)+parseFloat(json.igstper) ) +' % Rs. '+( parseFloat(json.cgstamt)+parseFloat(json.sgstamt)+parseFloat(json.igstamt))+' + Other Charges '+json.othercharges+' = '+json.totalcontractamt +' ( '+json.inword+' )',fontSize: 9});
          // content.push({ text:'to be paid by ' +json.companyname+' (herein after called the owners)',fontSize: 9});
          // content.push({ text:json.companyaddress,fontSize: 9});
          // content.push({ text:'TO M/S PEST MORTEM (INDIA) PVT. LTD. '+json.branchname + ' (herein after called the contractor).',fontSize: 9});
          

        } else {
          content.push({
            text: 'WORK CONTRACT',
            alignment: 'center',
            fontSize: 11,
            bold: true
          });
          content.push({
            table: {
              headerRows: 1,
              widths: ['*'],
              body: [
                [''],
                ['']
              ]
            },
            layout: 'headerLineOnly'
          }); //LINE CODE
          content.push({
            columns: [{
              text: 'Contract No. : ' + json.contractid,
              fontSize: 9,
              bold: true
            }, {
              text: 'Dated : ' + json.contractdate,
              alignment: 'right',
              fontSize: 9,
              bold: true
            }]
          });
          content.push({
            text: 'Period : ' + json.contractstart + ' TO ' + json.contractend,
            fontSize: 9,
            bold: true
          });
          content.push({
            table: {
              headerRows: 1,
              widths: ['*'],
              body: [
                [''],
                ['']
              ]
            },
            layout: 'headerLineOnly'
          }); //LINE CODE
          content.push({
            text: 'In Consultation and as agreed upon a sum of Rs. ' + json.amount + ' + GST @ ' + (parseFloat(json.cgstper) + parseFloat(json.sgstper) + parseFloat(json.igstper)) + ' % Rs. ' + (parseFloat(json.cgstamt) + parseFloat(json.sgstamt) + parseFloat(json.igstamt)) + ' + Other Charges ' + json.othercharges + ' = ' + json.totalcontractamt + ' ( ' + json.inword + ' )',
            fontSize: 9
          });
          content.push({
            text: 'to be paid by ' + json.companyname + ' (herein after called the owners)',
            fontSize: 9
          });
          content.push({
            text: json.companyaddress,
            fontSize: 9
          });
          content.push({
            text: 'TO M/S PEST MORTEM (INDIA) PVT. LTD. ' + json.branchname + ' (herein after called the contractor).',
            fontSize: 9
          });
          content.push({
            text: '\nThe contractor shall carry out the following Fumigation services in the premises of owners viz.',
            fontSize: 9,
            bold: true
          });
          content.push({
            table: {
              headerRows: 1,
              widths: ['*'],
              body: [
                [''],
                ['']
              ]
            },
            layout: 'headerLineOnly'
          }); //LINE CODE
        }


        dd = {
          pageSize: 'A4',
          pageOrientation: 'portrait',
          pageMargins: [40, 30, 30, 35],
          footer: function (currentPage, pageCount) {
            return {
              margin: 10,
              columns: [{
                fontSize: 9,
                text: [{
                    text: '--------------------------------------------------------------------------' +
                      '\n',
                    margin: [0, 20]
                  },
                  {
                    text: [{text:'© Pest Mortem (India) PVT. LTD. | '},{text: ' REGD OFFICE : ',bold:1},{text: 'G-2, Sunder Tower, T. J. Road, Sewree (West), Mumbai,400015 | PAGE ' + currentPage.toString() + ' of ' + pageCount}],
                  }
                ],
                alignment: 'center'
              }]
            };

          },
          content,
          styles: {
            tablfont: {
              fontSize: 9
            },
            tablfont1: {
              fontSize: 9
            },
            header: {
              fontSize: 16,
              bold: true,
              alignment: 'center',
            },
            subheader: {
              fontSize: 8,
              bold: true,
              alignment: 'center',
            },
            subheader1: {
              fontSize: 8,
              bold: true,
              alignment: 'center',
            },
            subheader2: {
              fontSize: 10,
              bold: true,

            },
          }
        }
        var win = window.open('', '_blank');
        pdfMake.createPdf(dd).open({}, win);

      }
    }
  })
}




function servicepdf(e) {
  var contractid = $(e).data('contractid');
  var location   = $(e).attr('data-location');
  $.ajax({
    type: "POST",
    data: "contractid=" + contractid,
    url: 'api/servicepdf.php',
    cache: false,
    success: function (res) {
      if (res.status == 'success') {
        var content=[];
        var json=res.json[0];
        var base64 = getBase64Image(document.getElementById("imageid"));


         content.push( {table:	{widths: ['auto','*'],	body: [

            [{rowSpan: 3,  stack: [{image:base64,
                      width: 75,
                      height:40,
                      margin:[0,10],
                      },]}, {text: 'PEST MORTEM (INDIA) PRIVATE LIMITED',style: 'header'}],
            ['', {text:'(Approved by National Plant Protection Organization, Goverment of India)',style: 'subheader'}],
            ['', {text:'AN ISO 9001 : 2008 CERTIFIED COMPANY',style: 'subheader'}],
            ]
            }, layout: 'noBorders',  });
 
          content.push( {table:	{widths: [70,'auto','*'],body: [
            [{text:''  ,style: 'subheader1',bold:true,border:[false,false,false,false]},{text:'REGD. OFF    : '  ,style: 'subheader1',bold:true,border:[false,false,false,false]},{text:json.address.toUpperCase(),style: 'subheader1',bold:true,border:[false,false,false,false]}],
            [{text:''  ,style: 'subheader1',bold:true,border:[false,false,false,false]},{text:'Phone            : '  ,style: 'subheader1',bold:true,border:[false,false,false,false]},{text:'+91-22-2412 7935 / 2411  1976/2414 7425',style: 'subheader1',bold:true,border:[false,false,false,false]}],
            [{text:''  ,style: 'subheader1',bold:true,border:[false,false,false,false]},{text:'FAX                : '  ,style: 'subheader1',bold:true,border:[false,false,false,false]},{text:'+91-22-2415 0261',style: 'subheader1',bold:true,border:[false,false,false,false]}],
            [{text:''  ,style: 'subheader1',bold:true,border:[false,false,false,false]},{text:'E-MAIL          : '  ,style: 'subheader1',bold:true,border:[false,false,false,false]},{text:'pestmortem@pestmortem.com',style: 'subheader1',bold:true,border:[false,false,false,false]}],
            [{text:''  ,style: 'subheader1',bold:true,border:[false,false,false,false]},{text:'WEBSITE       : '  ,style: 'subheader1',bold:true,border:[false,false,false,false]},{text:'http://www.pestmortem.com',style: 'subheader1',bold:true,border:[false,false,false,false]}],        
          ]}  }); 

        content.push({text:'\n'});
          content.push( {table:	{widths: ['*'],body: [[{text:' PEST MANAGEMENT SERVICE RECORD',alignment:'center',fontSize: 10,bold:true}],]}  }); 
          content.push({text:'\n'});
          content.push( {table:	{widths: [100,'*'],body: [
            [{text:'Name of Client        : '  ,fontSize: 9,bold:true,border:[false,false,false,false]},{text:json.companyname.toUpperCase(),fontSize: 9,bold:true,border:[false,false,false,false]}],
            [{text:'Address of Client    : '  ,fontSize: 9,bold:true,border:[false,false,false,false]},{text:json.companyaddress.toUpperCase(),fontSize: 9,bold:true,border:[false,false,false,false]}],
            [{text:'Contract No             : '  ,fontSize: 9,bold:true,border:[false,false,false,false]},{text:json.contractid,fontSize: 9,bold:true,border:[false,false,false,false]}],
            [{text:'Contract Date          : '  ,fontSize: 9,bold:true,border:[false,false,false,false]},{text:json.contractdate,fontSize: 9,bold:true,border:[false,false,false,false]}],         
          ]}  }); 
        
        if(json.ftype==''){
          var servicelocation=json.servicelocation;
          
          if (servicelocation=='Same as Purchaser Address') {
           var servicelocation1=json.companyaddress;
          }else{
           var servicelocation1=json.servicelocation;
           
          }
          content.push( {table:	{widths: [100,'*'],body: [
            [{text:'Service Location     : '  ,fontSize: 9,bold:true,border:[false,false,false,false]},{text:servicelocation1.toUpperCase(),fontSize: 9,bold:true,border:[false,false,false,false]}],
          ]}  });  
        } else{

          content.push( {table:	{widths: [100,'*'],body: [
            [{text:'Service Location     : '  ,fontSize: 9,bold:true,border:[false,false,false,false]},{text:location.toUpperCase(),fontSize: 9,bold:true,border:[false,false,false,false]}],
          ]}  }); 
        }
       
        var table1 = {
          widths: ['auto','*','auto','auto','auto'],
          dontBreakRows: true,
          body: []
        };
        var header=['Date','Service Performed','Infestation Status*','Name of Operator','Customer Signature'];
        table1['body'].push(header);
        var services=json.servicejson;

        for(var i in services){
          var arr=[];
          arr.push(services[i].servicedate,services[i].servicename,'','','');
            table1['body'].push(arr);
     
        }
        

        content.push({ columns: [
          {
            style: 'tablfont',
            table: table1,
            fontSize: 8,
            bold:true
            
            // layout: 'noBorders'
          }
        ]})

        

  
        
        dd = {
          pageSize: 'A5',
          pageOrientation: 'portrait',
          // pageOrientation: 'landscape',
          pageMargins: [40, 30, 30, 35],
          footer: function (currentPage, pageCount) {
            return {
              margin: 10,
              columns: [{
                fontSize: 9,
                text: [{
                    text: '--------------------------------------------------------------------------' +
                      '\n',
                    margin: [0, 20]
                  },
                  {
                    text: '© Pest Mortem (India) PVT. LTD. | PAGE ' + currentPage.toString() + ' of ' + pageCount,
                  }
                ],
                alignment: 'center'
              }]
            };

          },
          content,
          styles: {
            tablfont: {
              fontSize: 8
            },
            tablfont1: {
              fontSize: 8
            },
            header: {
                            fontSize: 14,
                            bold: true,
                            alignment: 'center',
                          },
                            subheader: {
                            fontSize: 8,
                            bold: true,
                            alignment: 'center',
                          }, 
                          subheader1: {
                            fontSize: 8,
                            bold: true,
                            alignment: 'left',
                          },
                           subheader2: {
                            fontSize: 10,
                            bold: true,
                            
                          },
          }
        }
        var win = window.open('', '_blank');
        pdfMake.createPdf(dd).open({}, win);

      }
    }
  })
}

$('.browse-btn').on('click',function(){
  $(this).parent().find('input').click();
});

$('.real-input').on('change',function(){
  $(this).parent().find('span').text($(this).val().replace(/C:\\fakepath\\/i, ''));
});


 
function setidtomidal(e) {
  var enquiryid = $(e).data('enquiryid');
  $('#inp-enquiryidset').val(enquiryid);

}

function confirms(e) {

  let enquiryid = $(e).attr('data-enquiryid');

  if (enquiryid != '') {

    $.ajax({

      type: "POST",

      data: "enquiryid="+enquiryid,

      url: 'api/confirmenq.php',

      cache: false,

      success: function (res) {

        //window.location.reload();

        if (res.status == 'success') {

          swal({
              type: 'success',
              title: 'Pest Control Work Contract Updated',
              showConfirmButton: false,
              timer: 1000
          });

          setTimeout(function () {
              location.href = '/process/amc/index.php';
          }, 1000);

        }
        
      }
      
    })

  }

}



function upload(e) {
    var file = $(e).parent().parent().find('input')[0].files[0];
    var enquiryid = $(e).data('enquiryid');
    var formData = new FormData();

    formData.append('offer', file);
    formData.append('enquiryid', enquiryid);
    $.ajax({
        type: "POST",
        data: formData,
        url: 'api/offer.php',
        cache: false,
        processData: false,
        contentType: false,
        success: function (res) {
            window.location.reload();
        }
    })
}



</script>


