<?php

$css = '<link rel="stylesheet" href="'.$base.'css/classic.css">
<link rel="stylesheet" href="'.$base.'css/classic.date.css">
<link rel="stylesheet" href="'.$base.'css/classic.time.css">
<link href="'.$base.'/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" href="'.$base.'css/alart.css">
<link rel="stylesheet" href="'.$base.'css/grid.min.css">';

$js = '<script src="'.$base.'js/picker.js"></script>
<script src="'.$base.'js/picker.date.js"></script>
<script src="'.$base.'js/picker.time.js"></script>
<script src="'.$base.'js/pdfmake-new.min.js"></script>
<script src="'.$base.'js/vfs_fonts_times.js"></script>
<script src="'.$base.'js/vfs_fonts.js"></script>
<script src="'.$base.'/js/select2.min.js"></script>
<script src="'.$base.'../js/alart.js"></script>
<script src="'.$base.'js/fwork.js"></script>';
// <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.68/pdfmake.min.js"></script>

    include($base.'_in/header.php');
    include($base.'_in/connect.php');

    if (!in_array("Customer Ledger", $arrysession) && !in_array('Supplier Ledger', $arrysession) ) {
   
        echo "<script>window.location.href='/process/dashboard.php';</script>";
       
        exit;

    }


    $con = _connect();

 
    if (session_status() == PHP_SESSION_NONE) { session_start(); }
    $branchid = $_SESSION['branchid'];
    
?>
<style>
    
    #myModal1 .col-sm-4,.col-sm-8{
      margin-bottom:2px;
    }
    .picker__select--year{
        height:auto;
    }
    .picker__select--month{
        height:auto;
    }
    
</style>
<div class="container-fluid">
    <div id="div-content" class="content">
        <table width="100%">
            <tr>

                <td align="center" style="<?php echo $CustomerMaster; ?>"><a
                        href="/process/ledger/customer-master/index.php"
                        style="border:1px solid blue;border-radius:0px;<?php echo $navenq3;?>"
                        class="btn btn-primary btn-block">Customer Master</td>

                <td align="center" style="<?php echo $customerledger; ?>"><a
                        href="/process/ledger/customerledger.php"
                        style="border:1px solid blue;border-radius:0px;<?php echo $navenq1;?>"
                        class="btn btn-primary btn-block">Customer Ledger</td>

                <td align="center" style="<?php echo $supplierledger; ?>"><a
                        href="/process/ledger/supplierledger.php"
                        style="border:1px solid blue;border-radius:0px;<?php echo $navenq2;?>"
                        class="btn btn-primary btn-block">Supplier Ledger</td>
            </tr>
        </table>
