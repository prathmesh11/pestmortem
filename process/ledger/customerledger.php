<?php 

  $base    = '../../';

  $navenq1 = 'background:#1B1464;';

  include('header.php');

  if (!in_array("Customer Ledger", $arrysession)) {
   
    echo "<script>window.location.href='/process/dashboard.php';</script>";
   
    exit;

}

  if (session_status() == PHP_SESSION_NONE) { session_start(); }

  $sessionby = $_SESSION['employeeid'];

  $branchid  = $_SESSION['branchid'];

?>

<br>

<style>

  .input-container {

    max-width: 300px;
    background-color: #EDEDED;
    border: 1px solid #DFDFDF;
    border-radius: 5px;
  }

 

  @media (max-width: 300px) {
    button {
      width: 100%;
      border-top-right-radius: 5px;
      border-bottom-left-radius: 0;
    }

    
  }

    .tableFixHead {
        overflow-y: auto;
        height: 600px;
    }

    .tableFixHead thead th {
        position: sticky;
        top: 0;
    }

    .tableFixHead table {
        border-collapse: collapse;
        width: 100%;
    }

    /* #components th, td { padding: 8px 16px; } */
    .tableFixHead th {
        background: #2980b9;
        color: #fff;
    }

</style>

<img class="hidden" id=imageid src="../../../../img/PestMortemLogo.jpg">

<div class="row">

    <div class="col-sm-1"></div>

    <div class="col-sm-2">

        <b>From Date :</b>

        <div class="form-group">

            <input type="date" class="form-control input-md" data-name="fromdate" id="fromdate">
        
        </div>

    </div>

    <div class="col-sm-2">

        <b>To Date :</b>
    
        <div class="form-group">

            <input type="date" class="form-control input-md" data-name="todate" id="todate">
                
        </div>
            
    </div>


    <div class="col-sm-2">

        <div class="form-group">

            <b>Select Customer</b>

            <select class="form-control input-md select-js1"  data-role="select" data-name="customerid" id="customerid">
            
                <option value="Select">Select Customer</option>

                    <?php 
                                
                    $result2=mysqli_query($con, "SELECT customerid, customername FROM `customermaster` WHERE branchid = '$branchid' ");

                    while($rows=mysqli_fetch_assoc($result2)) {

                        echo '<option value="'.$rows['customerid'].'">'.$rows['customername'].'</option>';

                    }

                    ?>
                            
            </select>

        </div>

    </div>

    <div class="col-sm-2">

        <br>

        <a class="btn btn-primary btn-sm btn-block" onclick="submit()">Submit</a>

    </div>

    <div class="col-sm-2">

      <br>

      <a class="btn btn-warning btn-sm btn-block" style="display:none;" id="printLedger" onclick="print()">Print</a>

    </div>

</div>

<div class="container-fluid tableFixHead table-responsive">

    <table class="table table-bordered " id="table-order">

        <thead>

            <tr>

                <th class="text-center">Date</th>

                <th class="text-center">Document No</th>

                <th class="text-center" width=35%>Description</th>

                <th class="text-center">Debit</th>

                <th class="text-center">Credit</th>

                <th class="text-center">Balance</th>

            </tr>

        </thead>

        <tbody>
        
            
        </tbody>

    </table>

</div>



<?php 

  include('footer.php');

?>

<script>

$(document).ready(function () {

  $('.select-js1').select2();

});


function submit(){

    let customerid = $('#customerid').val();

    let fromdate   = $('#fromdate').val();

    let todate     = $('#todate').val();

    $.ajax({

        type: "POST",

        data: 'customerid=' + customerid + '&fromdate=' + fromdate + '&todate=' + todate,

        url: 'api/custledger.php',

        cache: false,

        success: function (res) {

            if (res.status == 'success') {

              $('#printLedger').show();

              let json = res.billingjson;
      
              var totalamt = 0;

              var totalamt1 = 0;

              let str  = '';

                  str += '<tr>';

                      str +='<td class="opening_date">'+res.opening_date+'</td>';

                      str +='<td></td>';

                      str +='<td class="opDescription">'+res.opDescription+'</td>';

                      str +='<td></td>';

                      str +='<td></td>';

                      str +='<td class="text-right opening_balance">'+parseFloat(res.opening_balance).toFixed(2) + ' ' + res.tag +'</td>';

                  str += '</tr>';


                  for (let i in json) {

                      totalamt  += parseFloat(json[i].debit);
                      
                      totalamt1 += parseFloat(json[i].credit);

                      totalamt2  = 0;

                      if (res.tag == 'Dr') {

                        totalamt2 = (parseFloat(res.opening_balance) + totalamt) - totalamt1;

                          
                      } else {

                        totalamt2 = (parseFloat(res.opening_balance) + totalamt1) - totalamt;

                      }
                      
                      let tag = '';

                      if (totalamt2.toFixed(2) > 0) {

                        tag = "Dr";

                      } else {

                        tag = "Cr";

                      }

                      str += '<tr id="drCr">';

                        str +='<td class="invoiceDate">'+json[i].invoiceDate+'</td>';
                    
                        str +='<td class="billNo">'+json[i].billNo+'</td>';
                        
                        str +='<td class="category">'+json[i].category+'</td>';
                        
                        str +='<td class="text-right debit">'+parseFloat(json[i].debit).toFixed(2)+'</td>';
                        
                        str +='<td class="text-right credit">'+parseFloat(json[i].credit).toFixed(2)+'</td>';
                    
                        str +='<td class="text-right totalamt">'+Math.abs(totalamt2).toFixed(2) + ' ' + tag +'</td>';
                      
                      str += '</tr>';

                  }

                  str += '<tr id="totalAmt">';

                    str +='<td colspan="3" class="text-center"> Total </td>';

                    str +='<td class="text-right totalDebitAmt">'+totalamt.toFixed(2)+'</td>';

                    str +='<td class="text-right totalCreditAmt">'+totalamt1.toFixed(2)+'</td>';

                    str +='<td></td>';

                  str += '</tr>';

              $('#table-order > tbody').html(str);

            }

        }

    })
    
}



//-------------------------img covert to base64----------------------------//

function getBase64FromImageUrl(url) {

  var img = new Image();

  img.setAttribute('crossOrigin', 'anonymous');

  img.onload = function () {

    var canvas    = document.createElement("canvas");
    canvas.width  = this.width;
    canvas.height = this.height;
    var ctx = canvas.getContext("2d");
    ctx.drawImage(this, 0, 0);
    var dataURL = canvas.toDataURL("image/png");
    var base64 = dataURL.replace(/^data:image\/(png|jpg);base64,/, "");
    $('#imageid').val(base64);

  };

  img.src = url;

}

getBase64FromImageUrl('/img/PestMortemLogo.jpg');


function print() {

  let content     = [];

  let fromdate    = $('#fromdate').val();

  let todate      = $('#todate').val();

  let customerid  = $('#customerid').find(":selected").text();


  content.push({
   
    text: customerid + '\n',
    
    alignment: 'center',

    fontSize: 20,

    bold:1

  });


  content.push({

    layout: 'noBorders',
    
    table: {
      
      widths: ['*'],

      body:[
        
        [{text: ' From Date : ' + convertDate(fromdate) + '  To Date :  ' + convertDate(todate),alignment:'left',fontSize: 10}],
                       
      ]
    
    }
  
  });

  content.push({
   
   text: "\n",
 
  });

  let base64      = 'data:image/jpg;base64,' + $('#imageid').val();

  let opeingArray = [];

  $('#table-order > tbody > tr').each(function () {
    
    let opening_date = $(this).find('.opening_date').text();
    
    let opDescription = $(this).find('.opDescription').text();
    
    let opening_balance = $(this).find('.opening_balance').text();

    if (opening_date != '' && opDescription!= '' && opening_balance !='' ) {
      
      opeingArray.push({

        "opening_date": opening_date,
      
        "opDescription": opDescription,
      
        "opening_balance": opening_balance
    
      });

    }
    
  })

  let drCrArray = [];

  $('#table-order tbody  #drCr').each(function () {

    let invoiceDate = $(this).find('.invoiceDate').text();
    
    let billNo      = $(this).find('.billNo').text();
    
    let category    = $(this).find('.category').text();
    
    let debit       = $(this).find('.debit').text();
    
    let credit      = $(this).find('.credit').text();
    
    let totalamt    = $(this).find('.totalamt').text();

    drCrArray.push({

      "invoiceDate": invoiceDate,
      
      "billNo": billNo,
      
      "category": category,
      
      "debit": debit,
      
      "credit": credit,
      
      "totalamt": totalamt
    
    });
  
  })


  let totalDrCr = [];

  $('#table-order tbody  #totalAmt').each(function () {

    let totalDebitAmt  = $(this).find('.totalDebitAmt').text();
    
    let totalCreditAmt = $(this).find('.totalCreditAmt').text();

    totalDrCr.push({

      "totalDebitAmt": totalDebitAmt,
      
      "totalCreditAmt": totalCreditAmt
    
    });
  
  })


  let table1 = {

    widths: ['auto', '*','*', 'auto', 'auto','auto'],
    
    dontBreakRows: true,
    
    fontSize: 20,
    
    body: []

  };

  let tableServiceItemsHeaders = [
      
    {text: 'Date', style: 'tableHeader', alignment: 'center'}, 
      
    {text: 'Document No', style: 'tableHeader', alignment: 'center'}, 
      
    {text: 'Description', style: 'tableHeader', alignment: 'center'}, 
      
    {text: 'Debit', style: 'tableHeader', alignment: 'center'}, 
      
    {text: 'Credit', style: 'tableHeader', alignment: 'center'}, 
      
    {text: 'Balance', style: 'tableHeader',alignment: 'center'}
    
  ];
                   
  table1['body'].push(tableServiceItemsHeaders);

  var tb1 = [];
      
  tb1.push({text: opeingArray[0].opening_date},{}, {text: opeingArray[0].opDescription},{},{},{text: opeingArray[0].opening_balance,alignment: 'right'} );
      
  table1['body'].push(tb1);

  

  for (var i in drCrArray) {

    var tb2 = [];
    
    tb2.push({text: drCrArray[i].invoiceDate}, {text:drCrArray[i].billNo}, {text:drCrArray[i].category}, {text: drCrArray[i].debit,alignment: 'right'},{text: drCrArray[i].credit,alignment: 'right'},{text:drCrArray[i].totalamt,alignment: 'right'});

    table1['body'].push(tb2);

  }

  var tb3 = [];
      
  tb3.push({text: 'Total',alignment: 'center',colSpan:3},{},{}, {text: totalDrCr[0].totalDebitAmt,alignment: 'right'},{text: totalDrCr[0].totalCreditAmt,alignment: 'right'},{});
      
  table1['body'].push(tb3);
  
  content.push({
      
    style: 'tablfont1',
      
    table: table1
    
  });  


  content.push({
   
    text: "\n",
  
  });

  dd = {
    
    pageSize: 'A4',
    
    pageOrientation: 'portrait',
    
    pageMargins: [40, 30, 30, 35],
    
    footer: function (currentPage, pageCount) {
      
      return {
      
        margin: 10,
      
        columns: [{
      
          fontSize: 8,
      
          text: [{
      
              text: '--------------------------------------------------------------------------' + '\n',
              
              margin: [0, 20]
            
            },

            {
              text: '© Pest Mortem (India) PVT. LTD | PAGE ' + currentPage.toString() + ' of ' + pageCount,
            }
          
          ],
          
          alignment: 'center'
        
        }]
      
      };

    },
    
    content,
    
    styles: {
     
      tablfont: {
        
        fontSize: 7
      
      },
      
      tablfont1: {
        
        fontSize: 9
      
      }
    
    }
  
  }

  var win = window.open('', '_blank');

  pdfMake.createPdf(dd).open({}, win);

}


</script>