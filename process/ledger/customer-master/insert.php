<?php

    $base = '../../../';

    include($base.'_in/connect.php');

    header('content-type: application/json; charset=utf-8');

    header("access-control-allow-origin: *");

    if(isset($_POST['data']) && isset($_POST['data2']) && isset($_POST['data3']) && isset($_POST['enquiryid'])) {

        $con               = _connect();

        $data1             = $_POST["data"];

        $data2             = $_POST["data2"];

        $data3             = $_POST["data3"];

        $data4             = $_POST["data4"];

        $enquiryid         = $_POST["enquiryid"];

        $data              = get_object_vars(json_decode($_POST["data"]));
        
        $customername      = strtoupper($data['customername']);

        $gstnumber         = strtoupper($data['gstnumber']);

        $city              = $data['city'];

        $customergroupcode = $data['customergroupcode'];

        $created_by        = $data['created_by'];

        $branchid          = $data['branchid'];

        $opening_balance   = $data['opening_balance'];

        $opening_date      = $data['opening_date'];

        $addressjson       = json_decode($data4);

        $customerid        = mysqli_fetch_assoc(mysqli_query($con, "SELECT max(id) x FROM customermaster"))['x'];

        if($customerid) { $customerid++; } else { $customerid=1; }

        if($gstnumber=='') {
            
            $create = mysqli_query($con, "INSERT INTO customermaster (customerid,branchid,customername,city,gstnumber,customergroupcode,customerjson,contactjson,chajson,addressjson,opening_balance,opening_date,created_by,created_time) VALUES ('$customerid','$branchid','$customername','$city','$gstnumber','$customergroupcode','$data1','$data2','$data3','$data4','$opening_balance','$opening_date','$created_by','$CURRENT_MILLIS')");

            if($create) {

                if ($enquiryid!='') {

                    mysqli_query($con,"UPDATE enquiry SET  order_confirm_by='$created_by', order_confirm_time = '$CURRENT_MILLIS' WHERE enquiryid='$enquiryid'");
                    
                }

                foreach ($addressjson as $i) {

                    $multiAddress = get_object_vars($i)['multiAddress'];

                    $multiCity    = get_object_vars($i)['multiCity'];
                    
                    mysqli_query($con,"INSERT INTO customermultiaddress (branchid, customerid, customeraddress, customercity) VALUES ('$branchid','$customerid','$multiAddress','$multiCity') ");

                }


                echo '{"status":"success"}';

            } else {

                echo '{"status":"falid1"}';

            }

        } else {

            if( !preg_match("/^(0[1-9]|[1-2][0-9]|3[0-5])([a-zA-Z]){5}([0-9]){4}([a-zA-Z]){1}([a-zA-Z0-9]){1}([a-zA-Z]){1}([a-zA-Z0-9]){1}?$/", $gstnumber)) {

                echo '{"status":"falid11"}';

            } else {

                $results = mysqli_fetch_assoc(mysqli_query($con, "SELECT gstnumber x FROM customermaster WHERE gstnumber='$gstnumber' "))['x'];

                if( !$results) {

                    $create = mysqli_query($con, "INSERT INTO customermaster (customerid,branchid,customername,city,gstnumber,customergroupcode,customerjson,contactjson,chajson,addressjson, opening_balance,opening_date,created_by,created_time) VALUES ('$customerid','$branchid','$customername','$city','$gstnumber','$customergroupcode','$data1','$data2','$data3','$data4','$opening_balance','$opening_date','$created_by','$CURRENT_MILLIS')");                    
                    
                    if($create) {

                        if ($enquiryid!='') {

                            mysqli_query($con,"UPDATE enquiry SET  order_confirm_by='$created_by', order_confirm_time = '$CURRENT_MILLIS' WHERE enquiryid='$enquiryid'");
                            
                        }
                        
                        foreach ($addressjson as $i) {

                            $multiAddress = get_object_vars($i)['multiAddress'];

                            $multiCity    = get_object_vars($i)['multiCity'];
                            
                            mysqli_query($con,"INSERT INTO customermultiaddress (branchid, customerid, customeraddress, customercity) VALUES ('$branchid','$customerid','$multiAddress','$multiCity') ");
        
                        }

                        
                        echo '{"status":"success"}';

                    } else {

                        echo '{"status":"falid1"}';

                    }

                } else {

                    echo '{"status":"falid22"}';

                }

            }

        }

        _close($con);

    }  else {

        echo '{"status":"falid"}';

    }
?>