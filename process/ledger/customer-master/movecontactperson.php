<?php

    $base = '../../../';

    include($base.'_in/connect.php');

    header('content-type: application/json; charset=utf-8');

    header("access-control-allow-origin: *");

    if(isset($_POST['srNo']) && isset($_POST['data2']) && isset($_POST['branchid']) && isset($_POST['customerid'])) {

        $con          = _connect();

        $srNo         = $_POST["srNo"];

        $branchid     = $_POST["branchid"];

        $customerid   = $_POST["customerid"];

        $data         = get_object_vars(json_decode($_POST["data2"]));
        
        $MovecustName = $data['custName'];

        $name         = $data['name'];

        $department   = $data['department'];

        $category     = $data['category'];

        $mobile       = $data['mobile'];

        $landline     = $data['landline'];

        $email        = $data['email'];

        $contactjson  = mysqli_fetch_assoc(mysqli_query($con,"SELECT contactjson x FROM customermaster WHERE customerid = '$customerid'"))['x'];

        $contactjson  = json_decode($contactjson);

        // $json         = '';

        foreach ($contactjson as $i) {

            $count          = get_object_vars($i)['srNo'];
            
            $custName       = get_object_vars($i)['name'];
            
            $custDepartment = get_object_vars($i)['department'];
            
            $custCategory   = get_object_vars($i)['category'];
            
            $custMobile     = get_object_vars($i)['mobile'];
            
            $custLandline   = get_object_vars($i)['landline'];
            
            $custEmail      = get_object_vars($i)['email'];

            if ($srNo != $count) {

                $json.=',{"srNo":"'.$count.'","name":"'.$custName.'","department":"'.$custDepartment.'","category":"'.$custCategory.'","mobile":"'.$custMobile.'","landline":"'.$custLandline.'","email":"'.$custEmail.'"}';
                
            }

        }

     
        $json = substr($json,1);

        $json  = '['.$json.']';  
        

        $update = mysqli_query($con, "UPDATE customermaster SET contactjson = '$json' WHERE customerid = '$customerid' AND branchid = '$branchid'  ");

        if ($update) {

            $moveContactjson  = mysqli_fetch_assoc(mysqli_query($con,"SELECT contactjson x FROM customermaster WHERE customerid = '$MovecustName'"))['x'];

            $json_array       = json_decode($moveContactjson);
    
            $counts           = 1 ;
    
            foreach ($json_array as $a) {
                    
                $custName       = get_object_vars($a)['name'];
                
                $custDepartment = get_object_vars($a)['department'];
                
                $custCategory   = get_object_vars($a)['category'];
                
                $custMobile     = get_object_vars($a)['mobile'];
                
                $custLandline   = get_object_vars($a)['landline'];
                
                $custEmail      = get_object_vars($a)['email'];
        
                $jsons.=',{"srNo":"'.$counts.'","name":"'.$custName.'","department":"'.$custDepartment.'","category":"'.$custCategory.'","mobile":"'.$custMobile.'","landline":"'.$custLandline.'","email":"'.$custEmail.'"}';
                    
                $counts++;
            }
    
            $jsons = substr($jsons,1);

            $jsons = '['.$jsons.']';  

            $json_arrays  = json_decode($moveContactjson, true);

            $elementCount = count($json_arrays);

            $plusCount = 1+$elementCount;
    
            $newJson          = '[{"srNo":"'.$plusCount.'","name":"'.$name.'","department":"'.$department.'","category":"'.$category.'","mobile":"'.$mobile.'","landline":"'.$landline.'","email":"'.$email.'"}]';
    
            $mergeJson        = json_encode(array_merge(json_decode($jsons, true),json_decode($newJson, true)));
            
            $moveUpdate       = mysqli_query($con, "UPDATE customermaster SET contactjson = '$mergeJson' WHERE customerid = '$MovecustName' AND branchid = '$branchid'  ");

            if ($moveUpdate) {

                echo '{"status":"success"}';

            } else {

                echo '{"status":"falid2"}';

            }

        } else {

            echo '{"status":"falid1"}';


        }

        _close($con);

    }  else {

        echo '{"status":"falid"}';

    }
?>