<?php

    $base = '../../../';

    include($base.'_in/connect.php');

    header('content-type: application/json; charset=utf-8');

    header("access-control-allow-origin: *");

    if (isset($_POST['supplierid'])) {

        $con        = _connect();
        
        $supplierid = $_POST["supplierid"];

        $fromdate   = '';

        $todate     = '';

        if(isset($_POST['fromdate'])){ $fromdate=$_POST['fromdate']; }

        if(isset($_POST['todate'])){ $todate=$_POST['todate']; }

        if (session_status() == PHP_SESSION_NONE) { session_start(); }

        $branchid = $_SESSION['branchid'];

        if($fromdate=='' && $todate=='' && $supplierid!=''){

            $query = "SELECT opening_balance,opening_date FROM suppliermaster WHERE branchid='$branchid' AND supplierid = '$supplierid' ";

        }

        if($fromdate!='' && $todate!='' && $supplierid!=''){

            $query = "SELECT opening_balance,opening_date FROM suppliermaster WHERE branchid='$branchid' AND supplierid = '$supplierid' ";

        }

        
        $result = mysqli_fetch_assoc(mysqli_query($con,$query));
        
        if ($result) {

            $tag = '';

            $opening_date = $result['opening_date'];

            if ($opening_date > $fromdate ) {
                
                $latestFromdate = $opening_date;

            } else {

                $latestFromdate = $fromdate;

            }

            $onedayMinusDate = date('Y-m-d', strtotime('-1 day', strtotime($fromdate)));

            $contractamt = mysqli_fetch_assoc(mysqli_query($con,"SELECT sum(netamount) x FROM purchasebill WHERE branchid='$branchid' AND supplierid = '$supplierid' AND purchasedate BETWEEN '$opening_date' AND '$onedayMinusDate'  "))['x'];
            
            $receiveAmt  = mysqli_fetch_assoc(mysqli_query($con,"SELECT sum(Amount) x  FROM paymentbill WHERE branchid='$branchid' AND supplierid = '$supplierid' AND paymentdate BETWEEN '$opening_date' AND '$onedayMinusDate' "))['x'];

            $totalOpeingBalance = ($result['opening_balance']+$contractamt)-$receiveAmt;

            if ($totalOpeingBalance < 0 ) {

                $opening_balance = abs($totalOpeingBalance);

                $tag = 'Cr';

            } else {
                
                $opening_balance = $totalOpeingBalance;

                $tag = 'Dr';

            }

           // echo "SELECT * FROM purchasebill WHERE branchid='$branchid' AND supplierid = '$supplierid' AND purchasedate BETWEEN '$latestFromdate' AND '$todate' AND payment_time=0 ";
            $billing = mysqli_query($con,"SELECT DISTINCT billid FROM purchasebill WHERE branchid='$branchid' AND supplierid = '$supplierid' AND purchasedate BETWEEN '$latestFromdate' AND '$todate' AND payment_time=0 ");

            while ($row = mysqli_fetch_assoc($billing)) {

                $billid = $row['billid'];

                $paymentdetails = mysqli_fetch_assoc(mysqli_query($con,"SELECT purchasedate,purchasebillno FROM purchasebill WHERE branchid='$branchid' AND  supplierid = '$supplierid' AND billid = '$billid'  "));
               
                $netamount = mysqli_fetch_assoc(mysqli_query($con,"SELECT sum(netamount) x FROM purchasebill WHERE branchid='$branchid' AND  supplierid = '$supplierid' AND billid = '$billid'  "))['x'];

                $billingjson.=',{"invoiceDate":"'.date("d-m-Y",strtotime($paymentdetails['purchasedate'])).'","billNo":"'.$paymentdetails['purchasebillno'].'","debit":"'.$netamount.'","credit":"0","category":"Bill Pending"}';
                
            }

            $billingjson = substr($billingjson,1);

            $billingjson = '['.$billingjson.']';


            $results = mysqli_query($con,"SELECT DISTINCT purchaseid  FROM paymentbill WHERE branchid='$branchid' AND supplierid = '$supplierid' AND paymentdate BETWEEN '$latestFromdate' AND '$todate' ");

            while ($rows = mysqli_fetch_assoc($results)) {
                
                $purchaseid = $rows['purchaseid'];

                $paymentdetails = mysqli_fetch_assoc(mysqli_query($con,"SELECT paymentdate,instrumentType,instrumentNo FROM paymentbill WHERE branchid='$branchid' AND  supplierid = '$supplierid' AND purchaseid = '$purchaseid'  "));

                $category = $paymentdetails['instrumentType'].','.$paymentdetails['instrumentNo'];
                
                $payment    = mysqli_fetch_assoc(mysqli_query($con,"SELECT sum(Amount) x FROM paymentbill WHERE branchid='$branchid' AND supplierid = '$supplierid' AND purchaseid = '$purchaseid'  "))['x'];
                
                $paymentjson.=',{"invoiceDate":"'.date("d-m-Y",strtotime($paymentdetails['paymentdate'])).'","billNo":"'.$rows['purchaseid'].'","credit":"'.$payment.'","debit":"0","category":"'.$category.'"}';
                
            }

            $paymentjson = substr($paymentjson,1);

            $paymentjson = '['.$paymentjson.']';

            $debitnote  = mysqli_query($con,"SELECT * FROM debitnote WHERE branchid='$branchid' AND supplierid = '$supplierid' AND debit_date BETWEEN '$latestFromdate' AND '$todate' AND delete_time = 0 ");

            while ($row = mysqli_fetch_assoc($debitnote)) {
                
                $debitnotejson.=',{"invoiceDate":"'.date("d-m-Y",strtotime($row['debit_date'])).'","billNo":"'.$row['purchase_bill_no'].'","credit":"'.$row['debit_net_amount'].'","debit":"0","category":"Debit Note"}';
                
            }

            $debitnotejson = substr($debitnotejson,1);

            $debitnotejson = '['.$debitnotejson.']';


            $finaljson   = array_merge(json_decode($billingjson),json_decode($paymentjson),json_decode($debitnotejson));
     
            usort($finaljson, function($a, $b) { 

                return strtotime($a->invoiceDate) < strtotime($b->invoiceDate) ? -1 : 1;

            });
             

            echo '{"status":"success","billingjson":'.json_encode($finaljson).',"opening_balance":"'.$opening_balance.'","tag":"'.$tag.'","opDescription":"Opening Balance","opening_date":"'.date("d-m-Y",strtotime($fromdate)).'"}';   

            
        } else {

            echo '{"status":"falid1"}';

        }


        _close($con);

    } else {

        echo '{"status":"falid"}';
        
    }
?>