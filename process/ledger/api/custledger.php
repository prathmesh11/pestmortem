<?php

    $base = '../../../';

    include($base.'_in/connect.php');

    header('content-type: application/json; charset=utf-8');

    header("access-control-allow-origin: *");

    if (isset($_POST['customerid'])) {

        $con        = _connect();
        
        $customerid = $_POST["customerid"];

        $fromdate   = '';

        $todate     = '';

        if(isset($_POST['fromdate'])){ $fromdate=$_POST['fromdate']; }

        if(isset($_POST['todate'])){ $todate=$_POST['todate']; }

        if (session_status() == PHP_SESSION_NONE) { session_start(); }

        $branchid = $_SESSION['branchid'];

        if($fromdate=='' && $todate=='' && $customerid!=''){

            $query = "SELECT opening_balance,opening_date FROM customermaster WHERE branchid='$branchid' AND customerid = '$customerid' ";

        }

        if($fromdate!='' && $todate!='' && $customerid!=''){

            $query = "SELECT opening_balance,opening_date FROM customermaster WHERE branchid='$branchid' AND customerid = '$customerid' ";

        }

        
        $result = mysqli_fetch_assoc(mysqli_query($con,$query));
        
        if ($result) {

            $tag = '';

            $opening_date = $result['opening_date'];

            if ($opening_date > $fromdate ) {
                
                $latestFromdate = $opening_date;

            } else {

                $latestFromdate = $fromdate;

            }

            $onedayMinusDate    = date('Y-m-d', strtotime('-1 day', strtotime($fromdate)));

            $contractamt        = mysqli_fetch_assoc(mysqli_query($con,"SELECT sum(contractamt) x FROM billing WHERE branchid='$branchid' AND customerid = '$customerid' AND invoiceDate BETWEEN '$opening_date' AND '$onedayMinusDate'  "))['x'];
            
            $receiveAmt         = mysqli_fetch_assoc(mysqli_query($con,"SELECT sum(receiveAmt) x  FROM invoicepayment WHERE branchid='$branchid' AND customerid = '$customerid' AND receipt_date BETWEEN '$opening_date' AND '$onedayMinusDate' "))['x'];

            $totalOpeingBalance = ($result['opening_balance']+$contractamt)-$receiveAmt;

            if ($totalOpeingBalance < 0 ) {

                $opening_balance = abs($totalOpeingBalance);

                $tag = 'Cr';

            } else {
                
                $opening_balance = $totalOpeingBalance;

                $tag = 'Dr';

            }

            $billing = mysqli_query($con,"SELECT * FROM billing WHERE branchid='$branchid' AND customerid = '$customerid' AND invoiceDate BETWEEN '$latestFromdate' AND '$todate' ");

            while ($row = mysqli_fetch_assoc($billing)) {
                
                $billingjson.=',{"invoiceDate":"'.date("d-m-Y",strtotime($row['invoiceDate'])).'","billNo":"'.$row['invoice_bill'].'","debit":"'.$row['contractamt'].'","credit":"0","category":"'.$row['category'].'"}';
                
            }

            $billingjson = substr($billingjson,1);

            $billingjson = '['.$billingjson.']';

            $results = mysqli_query($con,"SELECT DISTINCT receipt_no,receipt_date  FROM invoicepayment WHERE branchid='$branchid' AND customerid = '$customerid' AND receipt_date BETWEEN '$latestFromdate' AND '$todate' ");

            while ($rows = mysqli_fetch_assoc($results)) {
                
                $receipt_no     = $rows['receipt_no'];

                $paymentdetails = mysqli_fetch_assoc(mysqli_query($con,"SELECT instrument_type,instrument_no FROM invoicepayment WHERE branchid='$branchid' AND customerid = '$customerid' AND receipt_no = '$receipt_no'  "));

                $category       = $paymentdetails['instrument_type'].','.$paymentdetails['instrument_no'];
                
                $payment        = mysqli_fetch_assoc(mysqli_query($con,"SELECT sum(receiveAmt) x FROM invoicepayment WHERE branchid='$branchid' AND customerid = '$customerid' AND receipt_no = '$receipt_no'  "))['x'];
                
                $paymentjson.=',{"invoiceDate":"'.date("d-m-Y",strtotime($rows['receipt_date'])).'","billNo":"'.$rows['receipt_no'].'","credit":"'.$payment.'","debit":"0","category":"'.$category.'"}';
                
            }

            $paymentjson = substr($paymentjson,1);

            $paymentjson = '['.$paymentjson.']';

            $creditnote  = mysqli_query($con,"SELECT * FROM creditnote WHERE branchid='$branchid' AND customerid = '$customerid' AND credit_date BETWEEN '$latestFromdate' AND '$todate' AND delete_time = 0 ");

            while ($row = mysqli_fetch_assoc($creditnote)) {
                
                $creditnotejson.=',{"invoiceDate":"'.date("d-m-Y",strtotime($row['credit_date'])).'","billNo":"'.$row['invoiceno'].'","debit":"'.$row['credit_net_amount'].'","credit":"0","category":"Credit Note"}';
                
            }

            $creditnotejson = substr($creditnotejson,1);

            $creditnotejson = '['.$creditnotejson.']';


            $finaljson   = array_merge(json_decode($billingjson),json_decode($paymentjson),json_decode($creditnotejson));
     
            usort($finaljson, function($a, $b) { 

                return strtotime($a->invoiceDate) < strtotime($b->invoiceDate) ? -1 : 1;

            });
             
            echo '{"status":"success","billingjson":'.json_encode($finaljson).',"opening_balance":"'.$opening_balance.'","tag":"'.$tag.'","opDescription":"Opening Balance","opening_date":"'.date("d-m-Y",strtotime($fromdate)).'"}';   
  
        } else {

            echo '{"status":"falid1"}';

        }


        _close($con);

    } else {

        echo '{"status":"falid"}';
        
    }
?>