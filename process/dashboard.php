<?php
$base='../';
$js='<script src="'.$base.'js/fwork.js"></script>
<script src="'.$base.'js/alart.js"></script>';
$css='<link rel="stylesheet" href="'.$base.'css/fwork.css">
<link rel="stylesheet" href="'.$base.'css/alart.css">';
include($base.'_in/header.php');
include($base.'_in/connect.php');
$con=_connect();
include($base.'process/header.php');
?>

<script src="https://www.chartjs.org/dist/2.7.3/Chart.bundle.js"></script>
<script src="https://www.chartjs.org/samples/latest/utils.js"></script>
<br>
<br>
<br>
<br>
<br>
<div class="col-sm-3"></div>
<div class="col-sm-6" id="canvas-holder" >
		<canvas id="chart-area"></canvas>
</div>

	<script>

		var config = {
			type: 'pie',
			data: {
				datasets: [{
					data: [
						25.00,
						25.00,
						25.00,
						25.00,
					],
					backgroundColor: [
						window.chartColors.red,
						window.chartColors.blue,
						window.chartColors.yellow,
						window.chartColors.green
					],
					label: 'Dataset 1'
				}],
				labels: [
					'Master Reports',
					'Transaction Reports',
					'MIS Reports',
					'Purchase Reports'
				]
			},
			options: {
				responsive: true
			}
		};

		window.onload = function() {
			var ctx = document.getElementById('chart-area').getContext('2d');
			window.myPie = new Chart(ctx, config);
		};


	</script>
    </div>
</div>

<?php
include($base.'_in/footer.php');
?>

<script>

</script>

