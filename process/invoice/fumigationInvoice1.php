<?php 
    
        //crm->header.php,pestControlBill.php,pestControlemail.php,fumigationBill.php,fumigationemail.php

        //crm->api->deleteBill.php,fumigationBillSelect.php,fumigationSaveBill.php,manualBillSelect.php,pestControlSaveBill.php,setBill.php
        
        //database->billing

    $base    = '../../';
    $navenq1 = 'background:#1B1464;';

    include('header.php');

    if (session_status() == PHP_SESSION_NONE) { session_start(); }
    $sessionby = $_SESSION['employeeid'];
    $branchid  = $_SESSION['branchid'];

    $branchshortname = mysqli_fetch_assoc(mysqli_query($con,"SELECT branchshortname x FROM branchmaster WHERE branchid='$branchid'"))['x'];

?>

<br>
<img style="display:none;" id=imageid src="<?php echo $base;?>img/PestMortemLogo.jpg">

<div class="table-ui container-fluid">

    <div class="tr row">

        <div class="col-sm-1 th" style="word-wrap:break-word;">Bill No.</div>

        <div class="col-sm-2 th">Customer Info</div>

        <div class="col-sm-4 th">Bill Description</div>

        <div class="col-sm-3 th">Payment info</div>

        <div class="col-sm-2 th">Action</div>

    </div>


    <div class="row tr">

        <?php

            $result = mysqli_query($con,"SELECT * FROM billing WHERE branchid='$branchid' AND deleted_time=0  AND category='Fumigation' ");

            while ($row = mysqli_fetch_assoc($result)) {

                $customerid = $row['customerid'];

                $custEmail = get_object_vars(json_decode($row['customer_json'])[0])['email'];

                $billDate = strtotime(get_object_vars(json_decode($row['customer_json'])[0])['issuedate'])*1000;

                $lastfollowup=$billDate;

                $followUpStatusFlag = '';

                if($lastfollowup!=0){

                  if($START_OF_THE_DAY<$lastfollowup && $END_OF_THE_DAY<=$lastfollowup){

                    $followUpStatusFlag='#3F51B5';

                  }

                  if($START_OF_THE_DAY>$lastfollowup && $END_OF_THE_DAY>$lastfollowup){

                    $followUpStatusFlag='#F44336';

                  }
                  
                  if($START_OF_THE_DAY<=$lastfollowup && $END_OF_THE_DAY>$lastfollowup ){

                    $followUpStatusFlag='#4CAF50';

                  }

                } else {

                  $followUpStatusFlag='#ff9800';

                }
                    
                $cifo      = mysqli_fetch_assoc(mysqli_query($con,"SELECT * FROM customermaster WHERE customerid='$customerid'"));

        ?>

                <div class="col-sm-1 td" style="word-wrap:break-word;">

                  <?php echo $row['invoice_bill']; ?>

                </div>

                <div class="col-sm-2 td">

                    <?php 

                        if ($cifo == '') {

                        echo 'Company : '.$customerid;
                        
                        } else {

                        echo 'Company : '.$cifo['customername']; 
                        echo '<br>Address : '.get_object_vars(json_decode($cifo['customerjson']))['address']; 
                        echo '<br>Name  : '.get_object_vars(json_decode($cifo['contactjson'])[0])['name'];
                        echo '<br>Dept. : '.get_object_vars(json_decode($cifo['contactjson'])[0])['department'];
                        echo '<br>Mobile : <a href="tel:'.get_object_vars(json_decode($cifo['contactjson'])[0])['mobile'].'">'.get_object_vars(json_decode($cifo['contactjson'])[0])['mobile'].'</a>';
                        echo '<br>Landline : '.get_object_vars(json_decode($cifo['contactjson'])[0])['landline'];
                        echo '<br>Email : <a href="mailto:'.get_object_vars(json_decode($cifo['customerjson']))['emailId'].'">'.get_object_vars(json_decode($cifo['customerjson']))['emailId'].'</a>';

                        }

                    ?>

                </div>

                <div class="col-sm-4 td" style="word-wrap:break-word;">

                    <?php
                        
                        if ($row['created_time']<>0) {
                            
                            echo '<table class="table table-list">';

                              echo '<thead>';

                                echo '<tr>';

                                  echo '<th colspan="8">Service Details</th>';

                                echo '</tr>';
                                
                                echo '<tr>';

                                  echo '<th>Item Name</th>';

                                  echo '<th>Sac</th>';

                                  echo '<th>Qty</th>';

                                  echo '<th>Rate</th>';

                                  echo '<th>CGST</th>';

                                  echo '<th>SGST</th>';

                                  echo '<th>IGST</th>';

                                  echo '<th>Amount</th>';

                                echo '</tr>';

                              echo '</thead>';

                              echo '<tbody>';

                                $service =  json_decode($row['service_json']);

                                  foreach ($service as $i) {

                                    echo '<tr>';

                                      echo '<td align="center">'.get_object_vars($i)['servicename'].'</td>';

                                      echo '<td align="center">'.get_object_vars($i)['sac'].'</td>';

                                      echo '<td align="center">'.get_object_vars($i)['qty'].'<br>'.get_object_vars($i)['qty1'].'</td>';

                                      echo '<td align="center">'.get_object_vars($i)['rate'].'</td>';

                                      echo '<td align="center">'.get_object_vars($i)['cgstper'].'%</td>';

                                      echo '<td align="center">'.get_object_vars($i)['sgstper'].'%</td>';

                                      echo '<td align="center">'.get_object_vars($i)['igstper'].'%</td>';

                                      echo '<td align="center">'.get_object_vars($i)['amounts'].'</td>';


                                    echo '</tr>';

                                  }  

                              echo '</tbody>';

                            echo '</table>';


                            echo 'Bill Date : <p style="background:'.$followUpStatusFlag.';color:#fff;">'.date("d-m-Y",$billDate/1000).'</p>'; 

                          
                        }

                    ?>

                </div>

                <div class="col-sm-3 td" style="word-wrap:break-word;">

                  <?php
                  
                    if ($row['created_time']<>0) {

                      echo 'Contract Amount : ' .$row['contractamt'];


                      echo '<table class="table table-list">';

                          echo '<thead>';

                            echo '<tr>';

                              echo '<th>BILL AMT</th>';

                              // echo '<th>CGST%</th>';

                              echo '<th>CGST Amt.</th>';

                              // echo '<th>SGST%</th>';

                              echo '<th>SGST Amt.</th>';

                              // echo '<th>IGST%</th>';

                              echo '<th>IGST Amt.</th>';

                            echo '</tr>';

                          echo '</thead>';

                          echo '<tbody>';

                            echo '<tr>';

                              echo '<td align="center">'.$row['total_amount'].'</td>';

                              // echo '<td align="center">'.$row['cgstper'].'</td>';

                              echo '<td align="center">'.$row['cgstamt'].'</td>';

                              // echo '<td align="center">'.$row['sgstper'].'</td>';

                              echo '<td align="center">'.$row['sgstamt'].'</td>';

                              // echo '<td align="center">'.$row['igstper'].'</td>';

                              echo '<td align="center">'.$row['igstamt'].'</td>';
                              
                             echo '</tr>';

                          echo '</tbody>';

                      echo '</table>';


                    }  


                  ?>

                </div>

                <div class="col-sm-2 td" >

                    <?php

                        if ($row['category']=='Fumigation') {

                          if ($row['created_time']==0) {

                    ?>

                            <a href="bill.php?invoice_bill=<?php echo $row['invoice_bill'];?>&customerid=<?php echo $customerid;?>&category=<?php echo $row['category'];?>" class="btn btn-sm btn-primary btn-block">Fumigation Bill</a>


                    <?php
                          } else {
                    ?>

                            <a href="bill.php?invoice_bill=<?php echo $row['invoice_bill'];?>&customerid=<?php echo $customerid;?>&edit=true&category=<?php echo $row['category'];?>" class="btn btn-sm btn-primary btn-block">Edit</a>

                            <button data-invoice_bill="<?php echo $row['invoice_bill'];?>" data-branchid="<?php echo $row['branchid'];?>" class="btn btn-sm btn-info btn-block" onclick="printfumPdf(this);">Print Bill</button>

                            <button data-invoice_bill="<?php echo $row['invoice_bill'];?>" data-category="<?php echo $row['category'];?>" data-branchid="<?php echo $row['branchid'];?>" data-emailId="<?php echo $custEmail;?>" class="btn btn-sm btn-warning btn-block" onclick="TriggerOutlook(this)">Mail Pdf</a>

                    <?php
                          }

                        }
                    
                    ?>
                       

                        <button class="btn btn-sm btn-danger btn-block" data-BillNo="<?php echo $row['invoice_bill'];?>" data-branchid="<?php echo $row['branchid'];?>" onclick="deletebill(this)">Delete Bill</button>


                </div>

        <?php        
                
            }
        
        ?>

    </div>

</div>

<button class="btn" style="font-size:25px;border-radius:50px;width:60px;height:60px;position: fixed;bottom: 40px;
    right: 40px;background:#eb2f06;color:#fff;" data-toggle="modal" data-target="#myModal">+</button>


<div id="myModal" class="modal fade" role="dialog">

    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">

            <div class="modal-header">

                <h4 class="modal-title" align="center">New Bill</h4>

            </div>

            <div class="modal-body">

                <div class="form-horizontal">

                    <div class="form-group">

                        <label class="control-label col-sm-4" for="email">Select Customer :</label>

                        <div class="col-sm-8">

                            <input type="text" list="customerid"  data-name='customerid' data-role="text" class="form-control input-sm" />
            
                            <datalist id="customerid" >

                                <?php

                                    $result= mysqli_query($con,"SELECT customerid,customername  FROM customermaster ORDER by customerid ASC ");

                                    while($rows = mysqli_fetch_assoc($result)){

                                        echo '<option value="'.$rows['customerid'].' '.$rows['customername'].'"></option>';


                                    }
                                
                                ?>

                            </datalist>

                        </div>
                    </div>


                    <div class="form-group">

                        <label class="control-label col-sm-4" for="email">Select Category </label>

                        <div class="col-sm-8">

                            <select data-role="select" data-name='category' class="form-control input-sm">

                                <option value="Fumigation">Fumigation</option>

                                <option value="Pest Control">Pest Control</option>

                             </select>   
                        </div>

                    </div>


                    <div class="form-group">

                        <div class="col-sm-offset-2 col-sm-10">

                            <button class="btn btn-success btn-block btn-sm" id="newqneuiry" onclick="newBill()">Add
                                New Bill</button>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

</div>

<?php 
include('footer.php');
?>

<script>

function newBill() {

  var valid = true;

  if (checker('myModal') != false) {

    valid = valid * true;

  } else {

    alert('Please Select Customer');

    valid = valid * false;

  }

  if (valid) {

    var data = checker('myModal');

    var datastr = JSON.stringify(data);

    $.ajax({

      type: "POST",

      data: {

        data: datastr

      },

      url: 'api/setbill.php',

      cache: false,

      success: function (res) {

        if (res.status == 'success') {

          if (res.category=='Fumigation') {

            location.href = '/process/invoice/fumigationInvoice.php';

          } else {

            location.href = '/process/invoice/pestcontrolInvoice.php';

          }

        }

      }

    })

  }

}


function deletebill(e) {

  var BillNo = $(e).attr('data-BillNo');

  var branchid = $(e).attr('data-branchid');


  $.ajax({

    type: "POST",

    data: 'invoice_bill=' + BillNo + '&branchid=' + branchid,

    url: 'api/deleteBill.php',

    cache: false,

    success: function (res) {

      if (res.status == 'success') {

        window.location.reload();

      }

    }

  })

}

function getBase64Image(img) {

  //console.log(img);
  var canvas = document.createElement("canvas");
  console.log(canvas);

  canvas.width = img.width;
  canvas.height = img.height;
  console.log(canvas.width);
  console.log(canvas.height);
  var ctx = canvas.getContext("2d");
  ctx.drawImage(img, 0, 0);
  var dataURL = canvas.toDataURL("image/png");
  return dataURL.replace('/^data:image\/(png|jpg);base64,/', "");
}

function printfumPdf(e) {

  var invoice_bill = $(e).attr('data-invoice_bill');

  var branchId = $(e).attr('data-branchid');

  $.ajax({

    type: "POST",

    data: "branchId=" + branchId + "&invoice_bill=" + invoice_bill,

    url: 'api/billSelect.php',

    cache: false,

    success: function (res) {

      if (res.status == 'success') {


        var content = [];

        var json = res.json[0];

        var base64 = getBase64Image(document.getElementById("imageid"));

        //console.log(base64);
        
          content.push(
              {
              table:{
              margin: [10, 0],
              widths: [100, '*'],
              body:[
                [
                  {
                  rowSpan: 2,
                  stack: [{image:base64,
                            width: 100,
                            height:50,
                            margin:[0,20]
                            },],
                  },
                  {
                  rowSpan: 2, 
                  stack: [{
                    text: [
                    {text:'PEST MORTEM (INDIA) PRIVATE LIMITED\n',fontSize: 12,bold:1,alignment:'center'},
                    {text:'(Approved by National Plant Protection Organization, Government of India)'+'\n',fontSize: 10,alignment:'center'},
                    {text:"AN ISO 9001 : 2008 CERTIFIED COMPANY"+'\n',fontSize: 10,alignment:'center'},
                    {text:json.brachaddress+'\n',fontSize: 10,alignment:'center'},
                    {text:'GSTIN : '+ json.brachgstnumber+ ' , Email : '+ json.brachemail+'\n',fontSize: 10,alignment:'center'},
                    {text:'Phone : 24147425/24127935/24111976/24149566/65531746/47. Fax : 91-22-24150261. \n\n',fontSize: 10,alignment:'center'},
                    {text: 'TAX INVOICE',fontSize: 12,alignment:'center',bold:1},

                    ]},
                    ],
                  },        
            ],
            [{}, {}],

            ]
            },layout: {
                hLineColor: function (i, node) {
                  return (i === 0 || i === node.table.body.length) ? 'gray' : 'gray';
                },
                vLineColor: function (i, node) {
                  return (i === 0 || i === node.table.widths.length) ? 'gray' : 'gray';
                },
              }
            },
        ),


        content.push(
              {
              table:{
              // margin: [10, 0],
              widths: ['*', '*'],
              body:[
                [
                  {
                  rowSpan: 2,
                  stack: [{
                    text: [
                    {text: [{text:'Invoice No                 :   ',fontSize: 10},{text: json.invoice_bill+'\n\n',bold:1,fontSize: 10}],alignment:'left'},
                    {text:[{text:'Invoice Date              :   ',fontSize: 10},{text: convertDate(json.issuedate)+'\n\n',bold:1,fontSize: 10}],alignment:'left'},
                    {text:[{text:'P.O.                             :   ',fontSize: 10},{text: json.poNo+'\n\n',bold:1,fontSize: 10}],alignment:'left'},

                    ]},
                    ],
                  },
                  {
                  rowSpan: 2, 
                  stack: [{
                    text: [
                      {text:[{text:'Contract No                 :   ',fontSize: 10},{text: json.contractNo+'\n\n',bold:1,fontSize: 10}],alignment:'left'},
                      {text:[{text:'Contract Date              :   ',fontSize: 10},{text: convertDate(json.contractDate)+'\n\n',bold:1,fontSize: 10}],alignment:'left'},
                    ]},
                    ],
                  },        
            ],
            [{}, {}],

            ]
            },layout: {
                hLineColor: function (i, node) {
                  return (i === 0 || i === node.table.body.length) ? 'gray' : 'gray';
                },
                vLineColor: function (i, node) {
                  return (i === 0 || i === node.table.widths.length) ? 'gray' : 'gray';
                },
              }
            },
        ),

        content.push({
          table: {
            widths: ['*', '*'],
            body: [
              [{text:'Bill To',border: [true, false, true, false],alignment:'left',fontSize: 10,fillColor: '#dfe6e9'}, {text:'Ship To',border: [true, false, true, false],alignment:'left',fontSize: 10,fillColor: '#dfe6e9'}],
            ]
          },layout: {
                hLineColor: function (i, node) {
                  return (i === 0 || i === node.table.body.length) ? 'gray' : 'gray';
                },
                vLineColor: function (i, node) {
                  return (i === 0 || i === node.table.widths.length) ? 'gray' : 'gray';
                },
              }

        });

        content.push(
              {
              table:{
              // margin: [10, 0,10,0],
              widths: ['*', '*'],
              body:[
                [
                  {
                  rowSpan: 2,
                  stack: [{
                    text: [
                    {text: json.custName+'\n',bold:1,fontSize: 10,alignment:'left'},
                    {text: json.custAddress+'\n',fontSize: 9,alignment:'left'},
                    {text: json.personName+'\n',fontSize: 9,alignment:'left'},
                    {text: 'Phone  : ' + json.custPhone+'\n',fontSize: 9,alignment:'left'},
                    {text: 'Mobile : ' + json.custMobile+'\n',fontSize: 9,alignment:'left'},
                    {text: 'GSTIN  : ' + json.gstIn+'\n',fontSize: 9,alignment:'left'},
                    {text: 'email   : ' + json.email+'\n',fontSize: 9,alignment:'left'},

                    ]},
                    ],
                  },
                  {
                  rowSpan: 2, 
                  stack: [{
                    text: [
                      {text: json.premisesName+'\n',fontSize: 9,alignment:'left'},
                      {text: json.premisesAddress+'\n',fontSize: 9,alignment:'left'},
                      {text: 'Mobile : ' + json.premisesMobile+'\n',fontSize: 9,alignment:'left'},
                      {text: 'Phone  : ' + json.premisesLandLine+'\n',fontSize: 9,alignment:'left'},
                      {text: json.account+'\n',fontSize: 9,alignment:'left'},
                      {text: json.account1+'\n',fontSize: 9,alignment:'left'},
                      {text: json.account2+'\n',fontSize: 9,alignment:'left'},
                    ]},
                    ],
                  },        
            ],
            [{}, {}],

            ]
            },layout: {
                hLineColor: function (i, node) {
                  return (i === 0 || i === node.table.body.length) ? 'gray' : 'gray';
                },
                vLineColor: function (i, node) {
                  return (i === 0 || i === node.table.widths.length) ? 'gray' : 'gray';
                },
              }
            },
        ),

        table1 = {
          widths: [],
          dontBreakRows: true,
          body: []
        };
        table1['widths'].push('auto', '*', 50, 'auto', 40, 'auto','auto', 'auto', 50);
        table1['body'].push([{text:'#',border: [true, false, true, false],fillColor: '#dfe6e9',bold:1}, {text:'Item & Description',border: [true, false, true, false],fillColor: '#dfe6e9',bold:1},{text:'HSN/SAC',border: [true, false, true, false],fillColor: '#dfe6e9',bold:1}, {text:'Qty',border: [true, false, true, false],fillColor: '#dfe6e9',bold:1},{text:'Rate',border: [true, false, true, false],fillColor: '#dfe6e9',bold:1,alignment:'right'},{text:'CGST',border: [true, false, true, false],fillColor: '#dfe6e9',bold:1}, {text:'SGST',border: [true, false, true, false],fillColor: '#dfe6e9',bold:1},{text:'IGST',border: [true, false, true, false],bold:1,fillColor: '#dfe6e9'}, {text:'Amount',border: [true, false, true, false],bold:1,fillColor: '#dfe6e9',alignment:'right'}]);

        var serviceJson = json.service_json;
        var couter = 1;

        for (var i in serviceJson) {
          var arr = [];
          arr.push(couter, serviceJson[i].servicename, serviceJson[i].sac, {text:serviceJson[i].qty +'\n'+ serviceJson[i].qty1,alignment:'right'}, {text:serviceJson[i].rate,alignment:'right'},{text:serviceJson[i].cgstper+'%',alignment:'right'},{text:serviceJson[i].sgstper+'%',alignment:'right'},{text:serviceJson[i].igstper+'%',alignment:'right'},{text:serviceJson[i].amounts,alignment:'right'});
          table1['body'].push(arr);
          couter++;

        }

        content.push({
				        style: 'tablfont',
                table: table1,
                layout: {
                hLineColor: function (i, node) {
                  return (i === 0 || i === node.table.body.length) ? 'gray' : 'gray';
                },
                vLineColor: function (i, node) {
                  return (i === 0 || i === node.table.widths.length) ? 'gray' : 'gray';
                },
              },
        });

        content.push({
          table: {
            widths: [300, '*'],
            body: [
              [{text:'\nTotal In Words',border: [true, false, false, false],alignment:'left',fontSize: 9}, {text:[{text:'\nSub Total ',fontSize: 10,alignment:'right'},{text: '   '  +   json.amount,bold:1,fontSize: 10,}],border: [true, false, true, false],fontSize: 10}],
              [{text: convertNumberToWords(json.netAmount),bold:1,border: [true, false, false, false],alignment:'left',fontSize: 9}, {text:[{text:'CGST ',fontSize: 10,alignment:'right'},{text: '     '  +   json.cgstAmt,bold:1,fontSize: 10,}],border: [true, false, true, false],fontSize: 10}],
              [{text:'Payment should be made within 30 days from the date of invoice.',border: [true, false, false, false],alignment:'left',fontSize: 9}, {text:[{text:'SGST ',fontSize: 10,alignment:'right'},{text: '     '  +   json.sgstAmt,bold:1,fontSize: 10,}],border: [true, false, true, false],fontSize: 10}],
              [{text:'Interest @18% will be charged on overdue Invoices.',border: [true, false, false, false],alignment:'left',fontSize: 9}, {text:[{text:'IGST ',fontSize: 10,alignment:'right'},{text: '            '  + json.igstAmt,bold:1,fontSize: 10,alignment:'center'}],border: [true, false, true, false],fontSize: 10}],
              [{text:'GSTIN: 27AFXPP7896Q1ZY\nCategory of Service : Cleaning Service\nE.P.F. A/c No. : MH/40228\nE.S.I.C. Code No. : 31-480-101\nPAN NO. : AACCP25',border: [true, false, false, false],alignment:'left',fontSize: 9}, {text:[{text:'Total ',fontSize: 10,alignment:'right',bold:1},{text: '   '  +   json.netAmount,bold:1,fontSize: 10,}],border: [true, false, true, true],fontSize: 10}],

            ]
            
          },layout: {
                hLineColor: function (i, node) {
                  return (i === 0 || i === node.table.body.length) ? 'gray' : 'gray';
                },
                vLineColor: function (i, node) {
                  return (i === 0 || i === node.table.widths.length) ? 'gray' : 'gray';
                },
              }

        });

        content.push({
          table: {
            widths: [300, '*'],
            body: [
              [{text:'Bank Name     : '+ json.bankName,border: [true, false, false, false],alignment:'left',fontSize: 9}, {text:'For Pest Mortem(India) Pvt. Ltd.',border: [true, false, true, false],fontSize: 9,alignment:'center'}],
              [{text:'Branch Name  : '+ json.branchName,border: [true, false, false, false],alignment:'left',fontSize: 9}, {text:'',border: [true, false, true, false]}],
              [{text:'IFSC Code       : '+ json.ifscCode,border: [true, false, false, false],alignment:'left',fontSize: 9}, {text:'',border: [true, false, true, false]}],
              [{text:'Account No     : '+ json.accountNo,border: [true, false, false, true],alignment:'left',fontSize: 9}, {text:'Authorised Signatory',border: [true, false, true, true],fontSize: 9,alignment:'center'}],

            ]
            
          },layout: {
                hLineColor: function (i, node) {
                  return (i === 0 || i === node.table.body.length) ? 'gray' : 'gray';
                },
                vLineColor: function (i, node) {
                  return (i === 0 || i === node.table.widths.length) ? 'gray' : 'gray';
                },
              }

        });


        dd = {

          pageSize: 'A4',

          pageOrientation: 'portrait',

          pageMargins: [40, 30, 30, 35],

          footer: function (currentPage, pageCount) {

            return {

              margin: 10,

              columns: [{

                fontSize: 9,

                text: [{

                    text: '--------------------------------------------------------------------------' +
                      '\n',
                    margin: [0, 20]

                  },
                  {

                    text: '© Pest Mortem (India) PVT. LTD. | PAGE ' + currentPage.toString() + ' of ' + pageCount,

                  }

                ],

                alignment: 'center'

              }]


            };

          },

          content,
          styles: {

            tablfont: {

              fontSize: 9

            },

            tablfont1: {

              fontSize: 9

            },

            header: {

              fontSize: 16,
              bold: true,
              alignment: 'center',

            },

            subheader: {

              fontSize: 8,
              bold: true,
              alignment: 'center',

            },
            subheader1: {

              fontSize: 8,
              bold: true,
              alignment: 'center',

            },

            subheader2: {

              fontSize: 10,
              bold: true,

            },

          }

        }

        var win = window.open('', '_blank');

        pdfMake.createPdf(dd).open({}, win);


      }

    }

  })

}


function convertDate(inputFormat) {

  var d = new Date(inputFormat);

  var n = d.getMonth() + 1;

  if (n < 10) {

    var month = '0' + n;

  } else {

    var month = n;

  }

  var year = d.getFullYear();

  var da = d.getDate();

  if (da < 10) {

    var date = '0' + da;

  } else {

    var date = da;

  }

  return date + '-' + month + '-' + year;

}



function TriggerOutlook(e) {

  var email        = $(e).attr('data-emailId');
  var invoice_bill = $(e).attr('data-invoice_bill');
  var branchId     = $(e).attr('data-branchid');
  var category     = $(e).attr('data-category');


  var valid = true;

  var regemail = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;

  if (!regemail.test(email)) {

    valid = valid * false;

    alert('Pls Check Customer Email id');

  } else {

    valid = valid * true;

  }

  

   // var body = href = "http://pestmorten.local/process/invoice/email.php?invoice_bill=" + invoice_bill;
    var body = href = "http://pestmortem.co.in/process/invoice/email.php?invoice_bill=" + invoice_bill;

  var subject = "Pest Mortem (India) Pvt. Ltd.";
  var TO      = email;

  if (valid) {

    window.location.href = "mailto: " + TO + "?body=Pls Click Link And Download This Purchase Order To Given Below %0D%0A%0D%0A" + body + "&subject=" + subject;

    window.location.reload();

  }
  
}

</script>