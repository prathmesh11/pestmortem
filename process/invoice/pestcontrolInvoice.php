<?php 
    
    $base    = '../../';
    $navenq2 = 'background:#1B1464;';

    include('header.php');

    if (!in_array('Pestcontrol Invoice', $arrysession)) {
   
      echo "<script>window.location.href='/process/dashboard.php';</script>";
     
      exit;

    }

    if (session_status() == PHP_SESSION_NONE) { session_start(); }
    $sessionby = $_SESSION['employeeid'];
    $branchid  = $_SESSION['branchid'];

    $signature = mysqli_fetch_assoc(mysqli_query($con,"SELECT signature x FROM branchmaster WHERE branchid='$branchid'"))['x'];

    $customerid   = $_GET['customerid'];
    
    $invoice_bill = $_GET['invoiceBill'];
    
    $fromdate     = '';
    
    $todate       = '';

    $selectLimit  = $_GET['selectLimit'];

    if($customerid=='Select'){$customerid='';}

    if($invoice_bill=='Select'){$invoice_bill='';}

    if(isset($_GET['fromdate'])){ $fromdate=$_GET['fromdate']; }
    if(isset($_GET['todate'])){ $todate=$_GET['todate']; }


?>

<style>

/* Basic Styles */
.fancy-button {
  position: relative;
  color: #fff;
  border: 1px solid;
  margin: 5px;
  padding: 10px 25px;
  text-decoration: none;
  display: block;
  outline: none;
  line-height: 100%;
  overflow: hidden;
  height: 40px;
  -webkit-box-sizing: border-box;
  -moz-box-sizing: border-box;
  -o-box-sizing: border-box;
  box-sizing: border-box;
}
.fancy-button:hover {
  text-decoration: none;
}
.fancy-button .icon {
  float: right;
}
.fancy-button.icon{
display: inline-block;
}
.fancy-button:active .icon,
.fancy-button:focus .icon {
  -webkit-transform: scale(0.75);
  -moz-transform: scale(0.75);
  -o-transform: scale(0.75);
  transform: scale(0.75);
}
.small {
  padding: 5px 10px;
  font-size: 1em;
  height: 25px;
}

.asbestos {
  background-color: #7f8c8d;
}
.asbestos:hover {
  color: #7f8c8d;
  background-color: #fff;
}

.basic {
  -webkit-transition: all 0.35s ease;
  -moz-transition: all 0.35s ease;
  -o-transition: all 0.35s ease;
  transition: all 0.35s ease;
}
.basic:hover {
  border: 1px solid;
}


.mail {
  background-color: #c0392b;
  box-shadow: -2px 4px #888888;


}
.edit {
  background-color: #2980b9;
  box-shadow: -5px 4px #888888;


}
.copy {
  background-color: #130f40;
  box-shadow: -2px 4px #888888;


}
.pdf {
  background-color: #eb4d4b;
  box-shadow: -2px 4px #888888;


}

</style>

<br>
<img style="display:none;" id="imageid" src="<?php echo $base;?>img/imageonline-co-blackandwhiteimage.png">
<img style="display:none;" id="taxinvoice" src="<?php echo $base;?>img/tax-invoice1.png">
<img style="display:none;" id="signature" src="<?php echo $base.$signature;?>">

  <div class="row">

    <div class="col-sm-1"></div>

    <div class="col-sm-2">

      <b>From Date :</b>

      <div class="form-group">

          <input type="date" class="form-control input-md" data-name="fromdate" id="fromdate">
      
      </div>
    
    </div>

    <div class="col-sm-2">

      <b>To Date :</b>
      
      <div class="form-group">

        <input type="date" class="form-control input-md" data-name="todate" id="todate">
                
      </div>
            
    </div>

    <div class="col-sm-2">

      <b>Select Bill No</b>

      <div class="form-group">

        <select class="form-control input-md select-js1"  data-role="select" id="invoiceBill">

              <option value="Select">Select Bill No</option>

              <?php

                $result1=mysqli_query($con, "SELECT invoice_bill FROM `billing` WHERE category='Pest Control' AND branchid = '$branchid' ");
                  
                  while($rows=mysqli_fetch_assoc($result1)) {

                      echo '<option value="'.$rows['invoice_bill'].'">'.$rows['invoice_bill'].'</option>';
                  }

              ?> 

        </select>

      </div>

    </div>

    <div class="col-sm-2">

      <b>Select Customer</b>

      <div class="form-group">

        <select class="form-control input-md select-js1"  data-role="select" data-name="customerid" id="customerid">
          
          <option value="Select">Select Customer</option>

            <?php 
                         
              $result2=mysqli_query($con, "SELECT customerid, customername FROM `customermaster` WHERE branchid = '$branchid' ");

              while($rows=mysqli_fetch_assoc($result2)) {

                echo '<option value="'.$rows['customerid'].'">'.$rows['customername'].'</option>';

              }

            ?>
                        
        </select>

      </div>

    </div>

    <div class="col-sm-1">

      <div class="form-group">

      <b>Limit</b>


        <select class="form-control input-md"  data-role="select" data-name="selectLimit" id="selectLimit">
          
          <option value="100">100</option>
          <option value="250">250</option>
          <option value="500">500</option>
          <option value="1000">1000</option>
          <option value="Show All">Show All</option>       
        
        </select>

      </div>

    </div>

    <div class="col-sm-2">

      <br>

      <a class="btn btn-primary btn-sm btn-block" onclick="submit()">Submit</a>

    </div>

  </div>


<div class="table-ui container-fluid">

    <div class="tr row">

        <div class="col-sm-2 th" style="word-wrap:break-word;">Bill No.</div>

        <div class="col-sm-1 th" style="word-wrap:break-word;">Bill Date </div>
        
        <div class="col-sm-2 th">Customer Info</div>

        <div class="col-sm-2 th">Bill A/C</div>

        <div class="col-sm-1 th">CHA</div>

        <div class="col-sm-1 th">Addtional Info</div>

        <div class="col-sm-1 th">Contract Amt.</div>

        <div class="col-sm-2 th">Action</div>

    </div>


    <div class="row tr">

        <?php


          if($fromdate=='' && $todate=='' && $customerid=='' && $invoice_bill=='' && $selectLimit ==''){
          //echo '1';
            $query = "SELECT * FROM billing WHERE branchid='$branchid' AND deleted_time=0 AND category='Pest Control' ORDER BY id DESC LIMIT 250";

          }

            
          if($fromdate=='' && $todate=='' && $customerid!='' && $invoice_bill=='' && $selectLimit !=''){
            //echo '11';
            if ($selectLimit == 'Show All') {

              $query = "SELECT * FROM billing WHERE branchid='$branchid' AND deleted_time=0 AND category='Pest Control' AND customerid = '$customerid' ORDER BY id DESC ";

            } else {

              $query = "SELECT * FROM billing WHERE branchid='$branchid' AND deleted_time=0 AND category='Pest Control' AND customerid = '$customerid' ORDER BY id DESC LIMIT $selectLimit";

            }

          }

          if($fromdate=='' && $todate=='' && $customerid=='' && $invoice_bill!='' && $selectLimit !=''){
            //echo '111';
            $query = "SELECT * FROM billing WHERE branchid='$branchid' AND deleted_time=0 AND category='Pest Control' AND invoice_bill='$invoice_bill' ORDER BY id DESC";

          }

          if($fromdate=='' && $todate=='' && $customerid=='' && $invoice_bill=='' && $selectLimit !=''){
            // echo '111';
            if ($selectLimit == 'Show All') {
              
              $query = "SELECT * FROM billing WHERE branchid='$branchid' AND deleted_time=0 AND category='Pest Control' ORDER BY id DESC";

            } else {

              $query = "SELECT * FROM billing WHERE branchid='$branchid' AND deleted_time=0 AND category='Pest Control' ORDER BY id DESC LIMIT $selectLimit";

            }

          }

          if($fromdate!='' && $todate!='' && $customerid=='' && $invoice_bill=='' && $selectLimit !=''){

            //echo '1111';
            if ($selectLimit == 'Show All') {
              
              $query = "SELECT * FROM billing WHERE branchid='$branchid' AND deleted_time=0 AND category='Pest Control' AND invoiceDate BETWEEN '$fromdate' AND '$todate' ORDER BY id DESC";

            }else {

              $query = "SELECT * FROM billing WHERE branchid='$branchid' AND deleted_time=0 AND category='Pest Control' AND invoiceDate BETWEEN '$fromdate' AND '$todate' ORDER BY id DESC LIMIT $selectLimit ";

            }

          }

          if($fromdate!='' && $todate!='' && $customerid!='' && $invoice_bill=='' && $selectLimit !=''){
            //echo '11111';
            if ($selectLimit == 'Show All') {
              
              $query = "SELECT * FROM billing WHERE branchid='$branchid' AND deleted_time=0 AND category='Pest Control' AND customerid = '$customerid' AND invoiceDate BETWEEN '$fromdate' AND '$todate' ORDER BY id DESC";

            } else {

              $query = "SELECT * FROM billing WHERE branchid='$branchid' AND deleted_time=0 AND category='Pest Control' AND customerid = '$customerid' AND invoiceDate BETWEEN '$fromdate' AND '$todate' ORDER BY id DESC LIMIT $selectLimit ";

              # code...
            }

          }

          if($fromdate!='' && $todate!='' && $customerid=='' && $invoice_bill!='' && $selectLimit !=''){
            //echo '111111';
            $query = "SELECT * FROM billing WHERE branchid='$branchid' AND deleted_time=0 AND category='Pest Control' AND invoice_bill = '$invoice_bill' AND invoiceDate BETWEEN '$fromdate' AND '$todate' ORDER BY id DESC";

          }

          if($fromdate=='' && $todate=='' && $customerid!='' && $invoice_bill!='' && $selectLimit !=''){
            //echo '1111111';
            $query = "SELECT * FROM billing WHERE branchid='$branchid' AND deleted_time=0 AND category='Pest Control' AND invoice_bill='$invoice_bill' AND customerid = '$customerid' ORDER BY id DESC";

          }

          if($fromdate!='' && $todate!='' && $customerid!='' && $invoice_bill!='' && $selectLimit !=''){
            //echo '11111111';
            $query = "SELECT * FROM billing WHERE branchid='$branchid' AND deleted_time=0 AND category='Pest Control' AND invoice_bill='$invoice_bill' AND customerid = '$customerid' AND invoiceDate BETWEEN '$fromdate' AND '$todate' ORDER BY id DESC";

          }

            $result =mysqli_query($con,$query);
            //echo $query;
            while ($row = mysqli_fetch_assoc($result)) {

              $invoice_bill =$row['invoice_bill'];

              $receiveAmt = mysqli_fetch_assoc(mysqli_query($con,"SELECT SUM(receiveAmt) x FROM invoicepayment WHERE branchid='$branchId' AND invoiceNo='$invoice_bill'"))['x'];

                if ($row['contractamt'] != $receiveAmt) {
                
                $customerid = $row['customerid'];

                $custEmail = get_object_vars(json_decode($row['customer_json'])[0])['email'];

                $billDate = strtotime(get_object_vars(json_decode($row['customer_json'])[0])['invoiceDate'])*1000;

                $dueDate = strtotime(get_object_vars(json_decode($row['customer_json'])[0])['dueDate'])*1000;


                $lastfollowup=$billDate;

                $followUpStatusFlag = '';

                if($lastfollowup!=0){

                  if($START_OF_THE_DAY<$lastfollowup && $END_OF_THE_DAY<=$lastfollowup){

                    $followUpStatusFlag='#3F51B5';

                  }

                  if($START_OF_THE_DAY>$lastfollowup && $END_OF_THE_DAY>$lastfollowup){

                    $followUpStatusFlag='#F44336';

                  }
                  
                  if($START_OF_THE_DAY<=$lastfollowup && $END_OF_THE_DAY>$lastfollowup ){

                    $followUpStatusFlag='#4CAF50';

                  }

                } else {

                  $followUpStatusFlag='#ff9800';

                }
                    
                $cifo      = mysqli_fetch_assoc(mysqli_query($con,"SELECT * FROM customermaster WHERE customerid='$customerid'"));

        ?>

                <div class="col-sm-2 td" style="word-wrap:break-word;">

                <p style="font-weight:bold;"> <?php echo  $row['invoice_bill']; ?></p>

                </div>

                <div class="col-sm-1 td" style="word-wrap:break-word;">

                  <?php if ($billDate != '') {

                    echo '<p style="background:'.$followUpStatusFlag.';color:#fff;">'.date("d-m-Y",$billDate/1000).'</p>';

                  } ?>

                </div>

                 <div class="col-sm-2 td">

                    <?php 

                    
                      echo '<p style="font-size:12px; font-weight:bold;">'. $cifo['customername'].','.$cifo['city'].'</p>'; 

                      if ($row['sales_person_name']!=0) {

                        $sales_person_id = $row['sales_person_name'];
                        
                        $salesPerson    = mysqli_fetch_assoc(mysqli_query($con,"SELECT sales_person_name x FROM salesperson WHERE id='$sales_person_id'"))['x'];

                        echo '<p style="font-size:12px; font-weight:bold;">Sales Person Name:'. $salesPerson.'</p>'; 

                      }

                    ?>

                </div>

                <div class="col-sm-2 td">

                    <?php 

                      $account = get_object_vars(json_decode($row['customer_json'])[0])['accountName'];

                      if (get_object_vars(json_decode($row['customer_json'])[0])['accountName'] !='') {
                        echo '<p style="font-weight:bold;">'. $account.'</p>';
                      }
                      

                    ?>

                </div>

                <div class="col-sm-1 td">

                    <?php 

                     if (get_object_vars(json_decode($row['customer_json'])[0])['cha'] !='Select') {
                        echo '<p style="font-weight:bold;">'. get_object_vars(json_decode($row['customer_json'])[0])['cha'].'</p>';
                      }
                      

                    ?>

                </div>

                <div  class="col-sm-1 td">

                    <?php
                       if (get_object_vars(json_decode($row['customer_json'])[0])['emptybox'] !='') {
                          echo get_object_vars(json_decode($row['customer_json'])[0])['emptybox'];
                        }
                    ?>

                </div>


                <div class="col-sm-1 td" style="word-wrap:break-word;">

                  <?php
                  
                    if ($row['created_time']<>0) {


                      echo '<p style="font-weight:bold;">' .number_format($row['contractamt'], 2).'</p>';

                    }  


                  ?>

                </div>

                <div class="col-sm-2 td">

                  <?php

                    if ($row['category']=='Pest Control') {

                      if ($row['created_time']!=0) {

                  ?>
                      <div class="row">

                      <?php if ($row['billCreateBy']=='') {?>                        

                        <div class="col-md-2">
                          <a href="bill.php?invoice_bill=<?php echo $row['invoice_bill'];?>&customerid=<?php echo $customerid;?>&edit=true&category=<?php echo $row['category'];?>" class="fancy-button small asbestos basic icon edit" data-toggle="tooltip" data-placement="bottom" title = "Edit">
                            <span class="icon">
                              <i class="fa fa-pencil"></i>
                            </span>
                          </a>
                        </div>

                        <?php } ?>

                        <div class="col-md-2">
                          <a href="bill.php?invoice_bill=<?php echo $row['invoice_bill'];?>&customerid=<?php echo $customerid;?>&copy=true&category=<?php echo $row['category'];?>" class="fancy-button small rounded asbestos basic icon copy" data-toggle="tooltip" data-placement="bottom" title = "Copy">
                            <span class="icon">
                              <i class="fa fa-files-o" title="Copy"></i>
                            </span>
                          </a>
                        </div>

                        <div class="col-md-2">
                          <a href="javascript:void(0)" data-invoice_bill="<?php echo $row['invoice_bill'];?>" data-branchid="<?php echo $row['branchid'];?>" data-category="<?php echo $row['category'];?>" class="fancy-button small half-left-rounded asbestos basic icon pdf" onclick="printfumPdf(this);" data-toggle="tooltip" data-placement="bottom" title = "Print Pdf">
                            <span class="icon">
                              <i class="fa fa-print"></i>
                            </span>
                          </a>
                        </div>

                        <div class="col-md-2">
                          <a href="javascript:void(0)" data-invoice_bill="<?php echo $row['invoice_bill'];?>" data-category="<?php echo $row['category'];?>" data-branchid="<?php echo $row['branchid'];?>" data-emailId="<?php echo $custEmail;?>" onclick="TriggerOutlook(this)" class="fancy-button small half-right-rounded asbestos basic icon mail" data-toggle="tooltip" data-placement="bottom" title = "Mail">
                            <span class="bg"></span>
                            <span class="icon">
                              <i class="fa fa-envelope-o"></i>
                            </span>
                          </a>
                        </div>
                      </div>

                  <?php }  } ?>

                </div>

        <?php        
                
            }

          }
        
        ?>

    </div>

</div>

<a class="btn" style="font-size:25px;border-radius:50px;width:60px;height:60px;position: fixed;bottom: 40px;
    right: 40px;background:#eb2f06;color:#fff;" href="/process/invoice/bill.php?category=Pest Control">+</a>


<?php 
include('footer.php');
?>

<script>


function confirmbill(e) {

  var BillNo = $(e).attr('data-BillNo');

  var branchid = $(e).attr('data-branchid');

  var category = $(e).attr('data-category');

  $.ajax({

    type: "POST",

    data: 'invoice_bill=' + BillNo + '&branchid=' + branchid + '&category='+category,

    url: 'api/confirmbill.php',

    cache: false,

    success: function (res) {

      if (res.status == 'success') {

        window.location.reload();

      }

    }

  })

}


function deletebill(e) {

  var BillNo = $(e).attr('data-BillNo');

  var branchid = $(e).attr('data-branchid');

  var category = $(e).attr('data-category');

  $.ajax({

    type: "POST",

    data: 'invoice_bill=' + BillNo + '&branchid=' + branchid + "&category=" + category,

    url: 'api/deleteBill.php',

    cache: false,

    success: function (res) {

      if (res.status == 'success') {

        window.location.reload();

      }

    }

  })

}

function getBase64Image(img) {
  var canvas = document.createElement("canvas");
  canvas.width = img.width;
  canvas.height = img.height;
  var ctx = canvas.getContext("2d");
  ctx.drawImage(img, 0, 0);
  var dataURL = canvas.toDataURL("image/png");
  return dataURL.replace('/^data:image\/(png|jpg);base64,/', "");
}

function printfumPdf(e) {

  var invoice_bill = $(e).attr('data-invoice_bill');

  var branchId = $(e).attr('data-branchid');

  var category = $(e).attr('data-category');


  $.ajax({

    type: "POST",

    data: "branchId=" + branchId + "&invoice_bill=" + invoice_bill + "&category=" + category,

    url: 'api/billSelect.php',

    cache: false,

    success: function (res) {

      if (res.status == 'success') {


                let content = [];

        let json = res.json[0];

        let base64 = getBase64Image(document.getElementById("imageid"));

        let signature = getBase64Image(document.getElementById("signature"));

        let taxinvoice = getBase64Image(document.getElementById("taxinvoice"));

      
        window.pdfMake.vfs["Times-New-Roman-Regular.ttf"] = "BASE 64 HERE";

        pdfMake.fonts = {
            // Default font should still be available
            Roboto: {
                normal: 'Roboto-Regular.ttf',
                bold: 'Roboto-Medium.ttf',
                italics: 'Roboto-Italic.ttf',
                bolditalics: 'Roboto-Italic.ttf'
            },
            // Make sure you define all 4 components - normal, bold, italics, bolditalics - (even if they all point to the same font file)
            TimesNewRoman: {
                normal: 'Times-New-Roman-Regular.ttf',
                bold: 'Times-New-Roman-Bold.ttf',
                italics: 'Times-New-Roman-Italics.ttf',
                bolditalics: 'Times-New-Roman-Italics.ttf'
            }
        };


        content.push({
            layout: 'noBorders',
            table: {
                widths: ['*'],
                body: [
                    [{text:'CIN NO. : U24211MH1980PTC023338\n\n',bold:1,fontSize: 10,alignment:'center',fonts: 'TimesNewRoman'}],
                ]
            }

        });


        content.push({
            layout: 'noBorders',
            table: {
                widths: ['*', 325],
                body: [
                    [{image:base64,
                            width: 150,
                            height:80,
                            margin:[0,20]}, 
                            //{text:[{text:'PEST MORTEM (INDIA) PRIVATE LIMITED \n',fontSize: 12},{text: json.brachaddress+'\nState : ' + json.branchStatename + ', StateCode : ' + json.branchStateCode +'\n'+json.brachphone+'\n'+json.brachemail+'\nwww.pestmortem.com\nGST No : ' + json.brachgstnumber + ' \nPAN No : AACCP2255R\n\n',fontSize: 10,}],bold:1,alignment:'right',fonts: 'TimesNewRoman'}],
                            {text:[{text:'PEST MORTEM (INDIA) PRIVATE LIMITED \n',fontSize: 13},{text: json.brachaddress+'\n'+json.brachphone+'\n'+json.brachemail+'\nwww.pestmortem.com\nGST No : ' + json.brachgstnumber +'\nState : ' + json.branchStatename + ', StateCode : ' + json.branchStateCode + ' \nPAN No : AACCP2255R\n\n',fontSize: 10,}],bold:1,alignment:'right',fonts: 'TimesNewRoman'}],
                            
                ]
            }

        });


        if (json.shipto!='' && json.consineeName != '' && json.emptybox != '' && json.accountName != '') {

          if (json.totalGst =="0.00") {

            content.push({

              table: {
                widths: [200, 'auto'],
                 //heights: [0, 10],
                body:[
                  [{image:taxinvoice,width: 204,height:140,border:[true,true,true,true]},{text: [{text:'Bill To : \n',alignment:'left',fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: json.customerName+'\nA/C. : ' + json.accountName+'\n',border:[false,false,false,false],bold:1,alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text:json.custAddress,bold:1,alignment:'left',border:[false,false,false,false],fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nPhone  : ' + json.custPhone,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nEmail    : ' + json.email,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nGSTIN  : ' + json.gstIn,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nState    : ' + json.statename + '   State Code : ' + json.statecode,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\n'+json.emptybox,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nShip To : \n',alignment:'left',fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: json.consineeName,bold:1,alignment:'left',fontSize: 10,fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: '\nConsinee : ' + json.accountaddress,bold:1,alignment:'left',fontSize: 10,fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: '\nPhone  : ' + json.accountlandline,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nEmail    : ' + json.accountemail + '\nGSTIN  : ' + json.accountgst + '\nState    : ' + json.accountStatename + '   State Code : ' + json.accountStateCode + '\n\nPlace Of Supply : ' + json.shipto,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'}],rowSpan:5}],
                  [{text:'Invoice No      :   ' +  json.invoice_bill,fontSize: 10,bold: 1,absolutePosition: {x: 42, y: 303},color: '#000000',fonts: 'TimesNewRoman'},{}],
                  [{text:'Invoice Date   :   '   +  convertDate(json.invoiceDate),margin: [2,-7,0,0],alignment: 'left',fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],
                  [{text:'Due Date         :   '   +  convertDate(json.dueDate),margin: [2,5,0,0],alignment: 'left',fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],
                  [{text:'Lut No              :   ' + json.branchlutno,alignment: 'left',margin: [2,5,0,0],fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],


                ]

              },			layout: 'noBorders'


            })
            
          } else {

            content.push({

              table: {
                widths: [200, 'auto'],
                //  heights: [0, 10],
                body:[
                  [{image:taxinvoice,width: 204,height:140,border:[true,true,true,true]},{text: [{text:'Bill To : \n',alignment:'left',fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: json.customerName+'\nA/C. : ' + json.accountName+'\n',border:[false,false,false,false],bold:1,alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text:json.custAddress,bold:1,alignment:'left',border:[false,false,false,false],fontSize: 9,fonts: 'TimesNewRoman'},{text: '\nPhone  : ' + json.custPhone,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nEmail    : ' + json.email,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nGSTIN  : ' + json.gstIn,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nState    : ' + json.statename + '   State Code : ' + json.statecode,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\n'+json.emptybox,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nShip To : \n',alignment:'left',fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: json.consineeName,bold:1,alignment:'left',fontSize: 10,fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: '\nConsinee : ' + json.accountaddress,bold:1,alignment:'left',fontSize: 10,fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: '\nPhone  : ' + json.accountlandline,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nEmail    : ' + json.accountemail + '\nGSTIN  : ' + json.accountgst + '\nState    : ' + json.accountStatename + '   State Code : ' + json.accountStateCode + '\n\nPlace Of Supply : ' + json.shipto,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'}],rowSpan:4}],
                  [{text:'Invoice No      :   ' +  json.invoice_bill,fontSize: 10,bold: 1,absolutePosition: {x: 42, y: 303},color: '#000000',fonts: 'TimesNewRoman'},{}],
                  [{text:'Invoice Date   :   ' + convertDate(json.invoiceDate),margin: [2,-7,0,0],alignment: 'left',fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],
                  [{text:'Due Date         :   ' + convertDate(json.dueDate),margin: [2,5,0,0],alignment: 'left',fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],

                ]

              },			layout: 'noBorders'


            })

          }

        } else if (json.shipto!='' && json.consineeName != '' && json.emptybox != '' && json.accountName == '') {

          if (json.totalGst =="0.00") {

            content.push({

              table: {
                widths: [200, 'auto'],
                 //heights: [0, 10],
                body:[
                  [{image:taxinvoice,width: 204,height:140,border:[true,true,true,true]},{text: [{text:'Bill To : \n',alignment:'left',fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: json.customerName+'\n',border:[false,false,false,false],bold:1,alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text:json.custAddress,bold:1,alignment:'left',border:[false,false,false,false],fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nPhone  : ' + json.custPhone,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nEmail    : ' + json.email,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nGSTIN  : ' + json.gstIn,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nState    : ' + json.statename + '   State Code : ' + json.statecode,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\n'+json.emptybox,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nShip To : \n',alignment:'left',fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: json.consineeName,bold:1,alignment:'left',fontSize: 10,fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: '\nConsinee : ' + json.accountaddress,bold:1,alignment:'left',fontSize: 10,fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: '\nPhone  : ' + json.accountlandline,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nEmail    : ' + json.accountemail + '\nGSTIN  : ' + json.accountgst + '\nState    : ' + json.accountStatename + '   State Code : ' + json.accountStateCode + '\n\nPlace Of Supply : ' + json.shipto,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'}],rowSpan:5}],
                  [{text:'Invoice No      :   ' +  json.invoice_bill,fontSize: 10,bold: 1,absolutePosition: {x: 42, y: 303},color: '#000000',fonts: 'TimesNewRoman'},{}],
                  [{text:'Invoice Date   :   '   +  convertDate(json.invoiceDate),margin: [2,-7,0,0],alignment: 'left',fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],
                  [{text:'Due Date         :   ' + convertDate(json.dueDate),margin: [2,5,0,0],alignment: 'left',fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],
                  [{text:'Lut No              :   ' + json.branchlutno,alignment: 'left',margin: [2,5,0,0],fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],


                ]

              },			layout: 'noBorders'


            })
            
          } else {

            content.push({

              table: {
                widths: [200, 'auto'],
                //  heights: [0, 10],
                body:[
                  [{image:taxinvoice,width: 204,height:140,border:[true,true,true,true]},{text: [{text:'Bill To : \n',alignment:'left',fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: json.customerName+'\n',border:[false,false,false,false],bold:1,alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text:json.custAddress,bold:1,alignment:'left',border:[false,false,false,false],fontSize: 9,fonts: 'TimesNewRoman'},{text: '\nPhone  : ' + json.custPhone,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nEmail    : ' + json.email,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nGSTIN  : ' + json.gstIn,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nState    : ' + json.statename + '   State Code : ' + json.statecode,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\n'+json.emptybox,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nShip To : \n',alignment:'left',fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: json.consineeName,bold:1,alignment:'left',fontSize: 10,fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: '\nConsinee : ' + json.accountaddress,bold:1,alignment:'left',fontSize: 10,fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: '\nPhone  : ' + json.accountlandline,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nEmail    : ' + json.accountemail + '\nGSTIN  : ' + json.accountgst + '\nState    : ' + json.accountStatename + '   State Code : ' + json.accountStateCode + '\n\nPlace Of Supply : ' + json.shipto,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'}],rowSpan:4}],
                  [{text:'Invoice No      :   ' +  json.invoice_bill,fontSize: 10,bold: 1,absolutePosition: {x: 42, y: 303},color: '#000000',fonts: 'TimesNewRoman'},{}],
                  [{text:'Invoice Date   :   '   +  convertDate(json.invoiceDate),margin: [2,-7,0,0],alignment: 'left',fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],
                  [{text:'Due Date         :   ' + convertDate(json.dueDate),margin: [2,5,0,0],alignment: 'left',fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],

                ]

              },			layout: 'noBorders'


            })

          }

        } else if (json.shipto!='' && json.consineeName != '' && json.emptybox == '' && json.accountName != '') {

          if (json.totalGst =="0.00") {

            content.push({

              table: {
                widths: [200, 'auto'],
                // heights: [85, 0],
                body:[
                  [{image:taxinvoice,width: 204,height:140,border:[true,true,true,true]},{text: [{text:'Bill To : \n',alignment:'left',fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: json.customerName+'\nA/C. : ' + json.accountName+'\n',border:[false,false,false,false],bold:1,alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text:json.custAddress,bold:1,alignment:'left',border:[false,false,false,false],fontSize: 9,fonts: 'TimesNewRoman'},{text: '\nPhone  : ' + json.custPhone,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nEmail    : ' + json.email,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nGSTIN  : ' + json.gstIn,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nState    : ' + json.statename + '   State Code : ' + json.statecode,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nShip To : \n',alignment:'left',fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: json.consineeName,bold:1,alignment:'left',fontSize: 10,fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: '\nConsinee : ' + json.accountaddress,bold:1,alignment:'left',fontSize: 10,fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: '\nPhone  : ' + json.accountlandline,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nEmail    : ' + json.accountemail + '\nGSTIN  : ' + json.accountgst + '\nState    : ' + json.accountStatename + '   State Code : ' + json.accountStateCode + '\n\nPlace Of Supply : ' + json.shipto,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'}],rowSpan:5}],
                  [{text:'Invoice No      :   ' +  json.invoice_bill,fontSize: 10,bold: 1,absolutePosition: {x: 42, y: 303},color: '#000000',fonts: 'TimesNewRoman'},{}],
                  [{text:'Invoice Date   :   '   +  convertDate(json.invoiceDate),margin: [2,-7,0,0],alignment: 'left',fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],
                  [{text:'Due Date         :   ' + convertDate(json.dueDate),margin: [2,5,0,0],alignment: 'left',fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],
                  [{text:'Lut No              :   ' + json.branchlutno,alignment: 'left',margin: [2,5,0,0],fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],


                ]

              },			layout: 'noBorders'


            })
            
          } else {

            content.push({

              table: {
                widths: [200, 'auto'],
                // heights: [85, 0],
                body:[
                  [{image:taxinvoice,width: 204,height:140,border:[true,true,true,true]},{text: [{text:'Bill To : \n',alignment:'left',fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: json.customerName+'\nA/C. : ' + json.accountName+'\n',border:[false,false,false,false],bold:1,alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text:json.custAddress,bold:1,alignment:'left',border:[false,false,false,false],fontSize: 9,fonts: 'TimesNewRoman'},{text: '\nPhone  : ' + json.custPhone,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nEmail    : ' + json.email,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nGSTIN  : ' + json.gstIn,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nState    : ' + json.statename + '   State Code : ' + json.statecode,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nShip To : \n',alignment:'left',fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: json.consineeName,bold:1,alignment:'left',fontSize: 10,fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: '\nConsinee : ' + json.accountaddress,bold:1,alignment:'left',fontSize: 10,fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: '\nPhone  : ' + json.accountlandline,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nEmail    : ' + json.accountemail + '\nGSTIN  : ' + json.accountgst + '\nState    : ' + json.accountStatename + '   State Code : ' + json.accountStateCode + '\n\nPlace Of Supply : ' + json.shipto,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'}],rowSpan:4}],
                  [{text:'Invoice No      :   ' +  json.invoice_bill,fontSize: 10,bold: 1,absolutePosition: {x: 42, y: 303},color: '#000000',fonts: 'TimesNewRoman'},{}],
                  [{text:'Invoice Date   :   '   +  convertDate(json.invoiceDate),margin: [2,-7,0,0],alignment: 'left',fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],
                  [{text:'Due Date         :   ' + convertDate(json.dueDate),margin: [2,5,0,0],alignment: 'left',fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],
                ]

              },			layout: 'noBorders'


            })

          }

        } else if (json.shipto!='' && json.consineeName == '' && json.emptybox != '' && json.accountName != '') {

          if (json.totalGst =="0.00") {

            content.push({

              table: {
                widths: [200, 'auto'],
                // heights: [85, 0],
                body:[
                  [{image:taxinvoice,width: 204,height:140,border:[true,true,true,true]},{text: [{text:'Bill To : \n',alignment:'left',fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: json.customerName+'\nA/C. : ' + json.accountName+'\n',border:[false,false,false,false],bold:1,alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text:json.custAddress,bold:1,alignment:'left',border:[false,false,false,false],fontSize: 9,fonts: 'TimesNewRoman'},{text: '\nPhone  : ' + json.custPhone,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nEmail    : ' + json.email,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nGSTIN  : ' + json.gstIn,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nState    : ' + json.statename + '   State Code : ' + json.statecode,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\n'+json.emptybox,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text:'\n\nPlace Of Supply : ' + json.shipto,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'}],rowSpan:5}],
                  [{text:'Invoice No      :   ' +  json.invoice_bill,fontSize: 10,bold: 1,absolutePosition: {x: 42, y: 303},color: '#000000',fonts: 'TimesNewRoman'},{}],
                  [{text:'Invoice Date   :   '   +  convertDate(json.invoiceDate),margin: [2,-7,0,0],alignment: 'left',fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],
                  [{text:'Due Date         :   ' + convertDate(json.dueDate),margin: [2,5,0,0],alignment: 'left',fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],
                  [{text:'Lut No              :   ' + json.branchlutno,alignment: 'left',margin: [2,5,0,0],fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],


                ]

              },			layout: 'noBorders'


            })
            
          } else {

            content.push({

              table: {
                widths: [200, 'auto'],
                // heights: [85, 0],
                body:[
                  [{image:taxinvoice,width: 204,height:140,border:[true,true,true,true]},{text: [{text:'Bill To : \n',alignment:'left',fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: json.customerName+'\nA/C. : ' + json.accountName+'\n',border:[false,false,false,false],bold:1,alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text:json.custAddress,bold:1,alignment:'left',border:[false,false,false,false],fontSize: 9,fonts: 'TimesNewRoman'},{text: '\nPhone  : ' + json.custPhone,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nEmail    : ' + json.email,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nGSTIN  : ' + json.gstIn,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nState    : ' + json.statename + '   State Code : ' + json.statecode,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\n'+json.emptybox,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text:'\n\nPlace Of Supply : ' + json.shipto,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'}],rowSpan:4}],
                  [{text:'Invoice No      :   ' +  json.invoice_bill,fontSize: 10,bold: 1,absolutePosition: {x: 42, y: 303},color: '#000000',fonts: 'TimesNewRoman'},{}],
                  [{text:'Invoice Date   :   '   +  convertDate(json.invoiceDate),margin: [2,-7,0,0],alignment: 'left',fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],
                  [{text:'Due Date         :   ' + convertDate(json.dueDate),margin: [2,5,0,0],alignment: 'left',fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],

                ]

              },layout: 'noBorders'


            })

          }

        } else if (json.shipto=='' && json.consineeName != '' && json.emptybox != '' && json.accountName != '') {

          if (json.totalGst =="0.00") {

            content.push({

              table: {
                widths: [200, 'auto'],
                 //heights: [0, 10],
                body:[
                  [{image:taxinvoice,width: 204,height:140,border:[true,true,true,true]},{text: [{text:'Bill To : \n',alignment:'left',fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: json.customerName+'\nA/C. : ' + json.accountName+'\n',border:[false,false,false,false],bold:1,alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text:json.custAddress,bold:1,alignment:'left',border:[false,false,false,false],fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nPhone  : ' + json.custPhone,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nEmail    : ' + json.email,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nGSTIN  : ' + json.gstIn,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nState    : ' + json.statename + '   State Code : ' + json.statecode,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\n'+json.emptybox,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nShip To : \n',alignment:'left',fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: json.consineeName,bold:1,alignment:'left',fontSize: 10,fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: '\nConsinee : ' + json.accountaddress,bold:1,alignment:'left',fontSize: 10,fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: '\nPhone  : ' + json.accountlandline,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nEmail    : ' + json.accountemail + '\nGSTIN  : ' + json.accountgst + '\nState    : ' + json.accountStatename + '   State Code : ' + json.accountStateCode,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'}],rowSpan:5}],
                  [{text:'Invoice No      :   ' +  json.invoice_bill,fontSize: 10,bold: 1,absolutePosition: {x: 42, y: 303},color: '#000000',fonts: 'TimesNewRoman'},{}],
                  [{text:'Invoice Date   :   '   +  convertDate(json.invoiceDate),margin: [2,-7,0,0],alignment: 'left',fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],
                  [{text:'Due Date         :   ' + convertDate(json.dueDate),margin: [2,5,0,0],alignment: 'left',fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],
                  [{text:'Lut No              :   ' + json.branchlutno,alignment: 'left',margin: [2,5,0,0],fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],


                ]

              },			layout: 'noBorders'


            })
            
          } else {

            content.push({

              table: {
                widths: [200, 'auto'],
                //  heights: [0, 10],
                body:[
                  [{image:taxinvoice,width: 204,height:140,border:[true,true,true,true]},{text: [{text:'Bill To : \n',alignment:'left',fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: json.customerName+'\nA/C. : ' + json.accountName+'\n',border:[false,false,false,false],bold:1,alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text:json.custAddress,bold:1,alignment:'left',border:[false,false,false,false],fontSize: 9,fonts: 'TimesNewRoman'},{text: '\nPhone  : ' + json.custPhone,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nEmail    : ' + json.email,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nGSTIN  : ' + json.gstIn,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nState    : ' + json.statename + '   State Code : ' + json.statecode,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\n'+json.emptybox,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nShip To : \n',alignment:'left',fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: json.consineeName,bold:1,alignment:'left',fontSize: 10,fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: '\nConsinee : ' + json.accountaddress,bold:1,alignment:'left',fontSize: 10,fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: '\nPhone  : ' + json.accountlandline,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nEmail    : ' + json.accountemail + '\nGSTIN  : ' + json.accountgst + '\nState    : ' + json.accountStatename + '   State Code : ' + json.accountStateCode,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'}],rowSpan:4}],
                  [{text:'Invoice No      :   ' +  json.invoice_bill,fontSize: 10,bold: 1,absolutePosition: {x: 42, y: 303},color: '#000000',fonts: 'TimesNewRoman'},{}],
                  [{text:'Invoice Date   :   '   +  convertDate(json.invoiceDate),margin: [2,-7,0,0],alignment: 'left',fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],
                  [{text:'Due Date         :   ' + convertDate(json.dueDate),margin: [2,5,0,0],alignment: 'left',fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],

                ]

              },			layout: 'noBorders'


            })

          }

        } else if (json.shipto!='' && json.consineeName != '' && json.emptybox == '' && json.accountName == '') {

          if (json.totalGst =="0.00") {

            content.push({

              table: {
                widths: [200, 'auto'],
                // heights: [85, 0],
                body:[
                  [{image:taxinvoice,width: 204,height:140,border:[true,true,true,true]},{text: [{text:'Bill To : \n',alignment:'left',fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: json.customerName+'\n',border:[false,false,false,false],bold:1,alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text:json.custAddress,bold:1,alignment:'left',border:[false,false,false,false],fontSize: 9,fonts: 'TimesNewRoman'},{text: '\nPhone  : ' + json.custPhone,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nEmail    : ' + json.email,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nGSTIN  : ' + json.gstIn,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nState    : ' + json.statename + '   State Code : ' + json.statecode,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nShip To : \n',alignment:'left',fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: json.consineeName,bold:1,alignment:'left',fontSize: 10,fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: '\nShip To : \n',alignment:'left',fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: json.consineeName,bold:1,alignment:'left',fontSize: 10,fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: '\nConsinee : ' + json.accountaddress,bold:1,alignment:'left',fontSize: 10,fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: '\nPhone  : ' + json.accountlandline,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nEmail    : ' + json.accountemail + '\nGSTIN  : ' + json.accountgst + '\nState    : ' + json.accountStatename + '   State Code : ' + json.accountStateCode + '\n\nPlace Of Supply : ' + json.shipto,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'}],rowSpan:5}],
                  [{text:'Invoice No      :   ' +  json.invoice_bill,fontSize: 10,bold: 1,absolutePosition: {x: 42, y: 303},color: '#000000',fonts: 'TimesNewRoman'},{}],
                  [{text:'Invoice Date   :   '   +  convertDate(json.invoiceDate),margin: [2,-7,0,0],alignment: 'left',fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],
                  [{text:'Due Date         :   ' + convertDate(json.dueDate),margin: [2,5,0,0],alignment: 'left',fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],
                  [{text:'Lut No              :   ' + json.branchlutno,alignment: 'left',margin: [2,5,0,0],fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],


                ]

              },			layout: 'noBorders'


            })
            
          } else {

            content.push({

              table: {
                widths: [200, 'auto'],
                // heights: [85, 0],
                body:[
                  [{image:taxinvoice,width: 204,height:140,border:[true,true,true,true]},{text: [{text:'Bill To : \n',alignment:'left',fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: json.customerName+'\n',border:[false,false,false,false],bold:1,alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text:json.custAddress,bold:1,alignment:'left',border:[false,false,false,false],fontSize: 9,fonts: 'TimesNewRoman'},{text: '\nPhone  : ' + json.custPhone,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nEmail    : ' + json.email,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nGSTIN  : ' + json.gstIn,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nState    : ' + json.statename + '   State Code : ' + json.statecode,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nShip To : \n',alignment:'left',fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: json.consineeName,bold:1,alignment:'left',fontSize: 10,fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: '\nConsinee : ' + json.accountaddress,bold:1,alignment:'left',fontSize: 10,fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: '\nPhone  : ' + json.accountlandline,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nEmail    : ' + json.accountemail + '\nGSTIN  : ' + json.accountgst + '\nState    : ' + json.accountStatename + '   State Code : ' + json.accountStateCode + '\n\nPlace Of Supply : ' + json.shipto,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'}],rowSpan:4}],
                  [{text:'Invoice No      :   ' +  json.invoice_bill,fontSize: 10,bold: 1,absolutePosition: {x: 42, y: 303},color: '#000000',fonts: 'TimesNewRoman'},{}],
                  [{text:'Invoice Date   :   '   +  convertDate(json.invoiceDate),margin: [2,-7,0,0],alignment: 'left',fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],
                  [{text:'Due Date         :   ' + convertDate(json.dueDate),margin: [2,5,0,0],alignment: 'left',fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],
                ]

              },			layout: 'noBorders'


            })

          }

        } else if (json.shipto!='' && json.consineeName == '' && json.emptybox != '' && json.accountName == '') {

          if (json.totalGst =="0.00") {

            content.push({

              table: {
                widths: [200, 'auto'],
                // heights: [85, 0],
                body:[
                  [{image:taxinvoice,width: 204,height:140,border:[true,true,true,true]},{text: [{text:'Bill To : \n',alignment:'left',fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: json.customerName+'\n',border:[false,false,false,false],bold:1,alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text:json.custAddress,bold:1,alignment:'left',border:[false,false,false,false],fontSize: 9,fonts: 'TimesNewRoman'},{text: '\nPhone  : ' + json.custPhone,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nEmail    : ' + json.email,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nGSTIN  : ' + json.gstIn,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nState    : ' + json.statename + '   State Code : ' + json.statecode,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\n'+json.emptybox,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text:'\n\nPlace Of Supply : ' + json.shipto,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'}],rowSpan:5}],
                  [{text:'Invoice No      :   ' +  json.invoice_bill,fontSize: 10,bold: 1,absolutePosition: {x: 42, y: 303},color: '#000000',fonts: 'TimesNewRoman'},{}],
                  [{text:'Invoice Date   :   '   +  convertDate(json.invoiceDate),margin: [2,-7,0,0],alignment: 'left',fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],
                  [{text:'Due Date         :   ' + convertDate(json.dueDate),margin: [2,5,0,0],alignment: 'left',fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],
                  [{text:'Lut No              :   ' + json.branchlutno,alignment: 'left',margin: [2,5,0,0],fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],


                ]

              },			layout: 'noBorders'


            })
            
          } else {

            content.push({

              table: {
                widths: [200, 'auto'],
                // heights: [85, 0],
                body:[
                  [{image:taxinvoice,width: 204,height:140,border:[true,true,true,true]},{text: [{text:'Bill To : \n',alignment:'left',fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: json.customerName+'\n',border:[false,false,false,false],bold:1,alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text:json.custAddress,bold:1,alignment:'left',border:[false,false,false,false],fontSize: 9,fonts: 'TimesNewRoman'},{text: '\nPhone  : ' + json.custPhone,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nEmail    : ' + json.email,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nGSTIN  : ' + json.gstIn,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nState    : ' + json.statename + '   State Code : ' + json.statecode,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\n'+json.emptybox,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text:'\n\nPlace Of Supply : ' + json.shipto,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'}],rowSpan:4}],
                  [{text:'Invoice No      :   ' +  json.invoice_bill,fontSize: 10,bold: 1,absolutePosition: {x: 42, y: 303},color: '#000000',fonts: 'TimesNewRoman'},{}],
                  [{text:'Invoice Date   :   '   +  convertDate(json.invoiceDate),margin: [2,-7,0,0],alignment: 'left',fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],
                  [{text:'Due Date         :   ' + convertDate(json.dueDate),margin: [2,5,0,0],alignment: 'left',fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],

                ]

              },layout: 'noBorders'


            })

          }

        } else if (json.shipto!='' && json.consineeName == '' && json.emptybox == '' && json.accountName != '') {

          if (json.totalGst =="0.00") {

            content.push({

              table: {
                widths: [200, 'auto'],
                // heights: [85, 0],
                body:[
                  [{image:taxinvoice,width: 204,height:140,border:[true,true,true,true]},{text: [{text:'Bill To : \n',alignment:'left',fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: json.customerName+'\nA/C. : ' + json.accountName+'\n',border:[false,false,false,false],bold:1,alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text:json.custAddress,bold:1,alignment:'left',border:[false,false,false,false],fontSize: 9,fonts: 'TimesNewRoman'},{text: '\nPhone  : ' + json.custPhone,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nEmail    : ' + json.email,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nGSTIN  : ' + json.gstIn,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nState    : ' + json.statename + '   State Code : ' + json.statecode,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text:'\n\nPlace Of Supply : ' + json.shipto,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'}],rowSpan:5}],
                  [{text:'Invoice No      :   ' +  json.invoice_bill,fontSize: 10,bold: 1,absolutePosition: {x: 42, y: 303},color: '#000000',fonts: 'TimesNewRoman'},{}],
                  [{text:'Invoice Date   :   '   +  convertDate(json.invoiceDate),margin: [2,-7,0,0],alignment: 'left',fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],
                  [{text:'Due Date         :   ' + convertDate(json.dueDate),margin: [2,5,0,0],alignment: 'left',fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],
                  [{text:'Lut No              :   ' + json.branchlutno,alignment: 'left',margin: [2,5,0,0],fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],


                ]

              },			layout: 'noBorders'


            })
            
          } else {

            content.push({

              table: {
                widths: [200, 'auto'],
                // heights: [85, 0],
                body:[
                  [{image:taxinvoice,width: 204,height:140,border:[true,true,true,true]},{text: [{text:'Bill To : \n',alignment:'left',fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: json.customerName+'\nA/C. : ' + json.accountName+'\n',border:[false,false,false,false],bold:1,alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text:json.custAddress,bold:1,alignment:'left',border:[false,false,false,false],fontSize: 9,fonts: 'TimesNewRoman'},{text: '\nPhone  : ' + json.custPhone,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nEmail    : ' + json.email,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nGSTIN  : ' + json.gstIn,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nState    : ' + json.statename + '   State Code : ' + json.statecode,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text:'\n\nPlace Of Supply : ' + json.shipto,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'}],rowSpan:4}],
                  [{text:'Invoice No      :   ' +  json.invoice_bill,fontSize: 10,bold: 1,absolutePosition: {x: 42, y: 303},color: '#000000',fonts: 'TimesNewRoman'},{}],
                  [{text:'Invoice Date   :   '   +  convertDate(json.invoiceDate),margin: [2,-7,0,0],alignment: 'left',fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],
                  [{text:'Due Date         :   ' + convertDate(json.dueDate),margin: [2,5,0,0],alignment: 'left',fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],

                ]

              },layout: 'noBorders'


            })

          }

        } else if (json.shipto =='' && json.consineeName != '' && json.emptybox != '' && json.accountName == '') {

          if (json.totalGst =="0.00") {

            content.push({

              table: {
                widths: [200, 'auto'],
                 //heights: [0, 10],
                body:[
                  [{image:taxinvoice,width: 204,height:140,border:[true,true,true,true]},{text: [{text:'Bill To : \n',alignment:'left',fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: json.customerName+'\n',border:[false,false,false,false],bold:1,alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text:json.custAddress,bold:1,alignment:'left',border:[false,false,false,false],fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nPhone  : ' + json.custPhone,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nEmail    : ' + json.email,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nGSTIN  : ' + json.gstIn,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nState    : ' + json.statename + '   State Code : ' + json.statecode,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\n'+json.emptybox,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nShip To : \n',alignment:'left',fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: json.consineeName,bold:1,alignment:'left',fontSize: 10,fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: '\nConsinee : ' + json.accountaddress,bold:1,alignment:'left',fontSize: 10,fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: '\nPhone  : ' + json.accountlandline,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nEmail    : ' + json.accountemail + '\nGSTIN  : ' + json.accountgst + '\nState    : ' + json.accountStatename + '   State Code : ' + json.accountStateCode,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'}],rowSpan:5}],
                  [{text:'Invoice No      :   ' +  json.invoice_bill,fontSize: 10,bold: 1,absolutePosition: {x: 42, y: 303},color: '#000000',fonts: 'TimesNewRoman'},{}],
                  [{text:'Invoice Date   :   '   +  convertDate(json.invoiceDate),margin: [2,-7,0,0],alignment: 'left',fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],
                  [{text:'Due Date         :   ' + convertDate(json.dueDate),margin: [2,5,0,0],alignment: 'left',fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],
                  [{text:'Lut No              :   ' + json.branchlutno,alignment: 'left',margin: [2,5,0,0],fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],


                ]

              },			layout: 'noBorders'


            })
            
          } else {

            content.push({

              table: {
                widths: [200, 'auto'],
                //  heights: [0, 10],
                body:[
                  [{image:taxinvoice,width: 204,height:140,border:[true,true,true,true]},{text: [{text:'Bill To : \n',alignment:'left',fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: json.customerName+'\n',border:[false,false,false,false],bold:1,alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text:json.custAddress,bold:1,alignment:'left',border:[false,false,false,false],fontSize: 9,fonts: 'TimesNewRoman'},{text: '\nPhone  : ' + json.custPhone,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nEmail    : ' + json.email,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nGSTIN  : ' + json.gstIn,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nState    : ' + json.statename + '   State Code : ' + json.statecode,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\n'+json.emptybox,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nShip To : \n',alignment:'left',fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: json.consineeName,bold:1,alignment:'left',fontSize: 10,fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: '\nConsinee : ' + json.accountaddress,bold:1,alignment:'left',fontSize: 10,fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: '\nPhone  : ' + json.accountlandline,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nEmail    : ' + json.accountemail + '\nGSTIN  : ' + json.accountgst + '\nState    : ' + json.accountStatename + '   State Code : ' + json.accountStateCode,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'}],rowSpan:4}],
                  [{text:'Invoice No      :   ' +  json.invoice_bill,fontSize: 10,bold: 1,absolutePosition: {x: 42, y: 303},color: '#000000',fonts: 'TimesNewRoman'},{}],
                  [{text:'Invoice Date   :   '   +  convertDate(json.invoiceDate),margin: [2,-7,0,0],alignment: 'left',fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],
                  [{text:'Due Date         :   ' + convertDate(json.dueDate),margin: [2,5,0,0],alignment: 'left',fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],

                ]

              },			layout: 'noBorders'


            })

          }

        } else if (json.shipto=='' && json.consineeName != '' && json.emptybox == '' && json.accountName != '') {


          if (json.totalGst =="0.00") {

            content.push({

              table: {
                widths: [200, 'auto'],
                // heights: [85, 0],
                body:[
                  [{image:taxinvoice,width: 204,height:140,border:[true,true,true,true]},{text: [{text:'Bill To : \n',alignment:'left',fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: json.customerName+'\nA/C. : ' + json.accountName+'\n',border:[false,false,false,false],bold:1,alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text:json.custAddress,bold:1,alignment:'left',border:[false,false,false,false],fontSize: 9,fonts: 'TimesNewRoman'},{text: '\nPhone  : ' + json.custPhone,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nEmail    : ' + json.email,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nGSTIN  : ' + json.gstIn,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nState    : ' + json.statename + '   State Code : ' + json.statecode,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nShip To : \n',alignment:'left',fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: json.consineeName,bold:1,alignment:'left',fontSize: 10,fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: '\nConsinee : ' + json.accountaddress,bold:1,alignment:'left',fontSize: 10,fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: '\nPhone  : ' + json.accountlandline,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nEmail    : ' + json.accountemail + '\nGSTIN  : ' + json.accountgst + '\nState    : ' + json.accountStatename + '   State Code : ' + json.accountStateCode,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'}],rowSpan:5}],
                  [{text:'Invoice No      :   ' +  json.invoice_bill,fontSize: 10,bold: 1,absolutePosition: {x: 42, y: 303},color: '#000000',fonts: 'TimesNewRoman'},{}],
                  [{text:'Invoice Date   :   '   +  convertDate(json.invoiceDate),margin: [2,-7,0,0],alignment: 'left',fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],
                  [{text:'Due Date         :   ' + convertDate(json.dueDate),margin: [2,5,0,0],alignment: 'left',fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],
                  [{text:'Lut No              :   ' + json.branchlutno,alignment: 'left',margin: [2,5,0,0],fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],


                ]

              },			layout: 'noBorders'


            })
            
          } else {

            content.push({

              table: {
                widths: [200, 'auto'],
                // heights: [85, 0],
                body:[
                  [{image:taxinvoice,width: 204,height:140,border:[true,true,true,true]},{text: [{text:'Bill To : \n',alignment:'left',fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: json.customerName+'\nA/C. : ' + json.accountName+'\n',border:[false,false,false,false],bold:1,alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text:json.custAddress,bold:1,alignment:'left',border:[false,false,false,false],fontSize: 9,fonts: 'TimesNewRoman'},{text: '\nPhone  : ' + json.custPhone,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nEmail    : ' + json.email,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nGSTIN  : ' + json.gstIn,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nState    : ' + json.statename + '   State Code : ' + json.statecode,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nShip To : \n',alignment:'left',fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: json.consineeName,bold:1,alignment:'left',fontSize: 10,fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: '\nConsinee : ' + json.accountaddress,bold:1,alignment:'left',fontSize: 10,fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: '\nPhone  : ' + json.accountlandline,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nEmail    : ' + json.accountemail + '\nGSTIN  : ' + json.accountgst + '\nState    : ' + json.accountStatename + '   State Code : ' + json.accountStateCode,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'}],rowSpan:4}],
                  [{text:'Invoice No      :   ' +  json.invoice_bill,fontSize: 10,bold: 1,absolutePosition: {x: 42, y: 303},color: '#000000',fonts: 'TimesNewRoman'},{}],
                  [{text:'Invoice Date   :   '   +  convertDate(json.invoiceDate),margin: [2,-7,0,0],alignment: 'left',fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],
                  [{text:'Due Date         :   ' + convertDate(json.dueDate),margin: [2,5,0,0],alignment: 'left',fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],
                ]

              },			layout: 'noBorders'


            })

          }

        } else if (json.shipto=='' && json.consineeName == '' && json.emptybox != '' && json.accountName != '') {
          console.log('abbbbbbbbbbbbbbbbbb');

          if (json.totalGst =="0.00") {

            content.push({

              table: {
                widths: [200, 'auto'],
                // heights: [85, 0],
                body:[
                  [{image:taxinvoice,width: 204,height:140,border:[true,true,true,true]},{text: [{text:'Bill To : \n',alignment:'left',fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: json.customerName+'\nA/C. : ' + json.accountName+'\n',border:[false,false,false,false],bold:1,alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text:json.custAddress,bold:1,alignment:'left',border:[false,false,false,false],fontSize: 9,fonts: 'TimesNewRoman'},{text: '\nPhone  : ' + json.custPhone,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nEmail    : ' + json.email,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nGSTIN  : ' + json.gstIn,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nState    : ' + json.statename + '   State Code : ' + json.statecode,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\n'+json.emptybox,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'}],rowSpan:5}],
                  [{text:'Invoice No      :   ' +  json.invoice_bill,fontSize: 10,bold: 1,absolutePosition: {x: 42, y: 303},color: '#000000',fonts: 'TimesNewRoman'},{}],
                  [{text:'Invoice Date   :   '   +  convertDate(json.invoiceDate),margin: [2,-7,0,0],alignment: 'left',fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],
                  [{text:'Due Date         :   ' + convertDate(json.dueDate),margin: [2,5,0,0],alignment: 'left',fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],
                  [{text:'Lut No              :   ' + json.branchlutno,alignment: 'left',margin: [2,5,0,0],fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],


                ]

              },			layout: 'noBorders'


            })
            
          } else {

            content.push({

              table: {
                widths: [200, 'auto'],
                // heights: [85, 0],
                body:[
                  [{image:taxinvoice,width: 204,height:140},{text: [{text:'Bill To : \n',alignment:'left',fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: json.customerName+'\nA/C. : ' + json.accountName+'\n',border:[false,false,false,false],bold:1,alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text:json.custAddress,bold:1,alignment:'left',border:[false,false,false,false],fontSize: 9,fonts: 'TimesNewRoman'},{text: '\nPhone  : ' + json.custPhone,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nEmail    : ' + json.email,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nGSTIN  : ' + json.gstIn,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nState    : ' + json.statename + '   State Code : ' + json.statecode,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\n'+json.emptybox,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'}],rowSpan:4}],
                  [{text:'Invoice No      :   ' +  json.invoice_bill,fontSize: 10,bold: 1,absolutePosition: {x: 42, y: 303},color: '#000000',fonts: 'TimesNewRoman'},{}],
                  [{text:'Invoice Date   :   '   +  convertDate(json.invoiceDate),margin: [2,-7,0,0],alignment: 'left',fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],
                  [{text:'Due Date         :   ' + convertDate(json.dueDate),margin: [2,5,0,0],alignment: 'left',fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],

                ]

              },layout: 'noBorders'


            })

          }

        } else if (json.shipto!='' && json.consineeName == '' && json.emptybox == '' && json.accountName == '') {

          if (json.totalGst =="0.00") {

            content.push({

              table: {
                widths: [200, 'auto'],
                 //heights: [0, 10],
                body:[
                  [{image:taxinvoice,width: 204,height:140,border:[true,true,true,true]},{text: [{text:'Bill To : \n',alignment:'left',fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: json.customerName+'\n',border:[false,false,false,false],bold:1,alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text:json.custAddress,bold:1,alignment:'left',border:[false,false,false,false],fontSize: 9,fonts: 'TimesNewRoman'},{text: '\nPhone  : ' + json.custPhone,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nEmail    : ' + json.email,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nGSTIN  : ' + json.gstIn,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nState    : ' + json.statename + '   State Code : ' + json.statecode,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text:'\n\nPlace Of Supply : ' + json.shipto,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'}],rowSpan:5}],
                  [{text:'Invoice No      :   ' +  json.invoice_bill,fontSize: 10,bold: 1,absolutePosition: {x: 42, y: 303},color: '#000000',fonts: 'TimesNewRoman'},{}],
                  [{text:'Invoice Date   :   '   +  convertDate(json.invoiceDate),margin: [2,-7,0,0],alignment: 'left',fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],
                  [{text:'Due Date         :   ' + convertDate(json.dueDate),margin: [2,5,0,0],alignment: 'left',fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],
                  [{text:'Lut No              :   ' + json.branchlutno,alignment: 'left',margin: [2,5,0,0],fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],


                ]

              },			layout: 'noBorders'


            })
            
          } else {

            content.push({

              table: {
                widths: [200, 'auto'],
                //  heights: [0, 10],
                body:[
                  [{image:taxinvoice,width: 204,height:140,border:[true,true,true,true]},{text: [{text:'Bill To : \n',alignment:'left',fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: json.customerName+'\n',border:[false,false,false,false],bold:1,alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text:json.custAddress,bold:1,alignment:'left',border:[false,false,false,false],fontSize: 9,fonts: 'TimesNewRoman'},{text: '\nPhone  : ' + json.custPhone,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nEmail    : ' + json.email,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nGSTIN  : ' + json.gstIn,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nState    : ' + json.statename + '   State Code : ' + json.statecode,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text:'\n\nPlace Of Supply : ' + json.shipto,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'}],rowSpan:4}],
                  [{text:'Invoice No      :   ' +  json.invoice_bill,fontSize: 10,bold: 1,absolutePosition: {x: 42, y: 303},color: '#000000',fonts: 'TimesNewRoman'},{}],
                  [{text:'Invoice Date   :   '   +  convertDate(json.invoiceDate),margin: [2,-7,0,0],alignment: 'left',fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],
                  [{text:'Due Date         :   ' + convertDate(json.dueDate),margin: [2,5,0,0],alignment: 'left',fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],
                ]

              },			layout: 'noBorders'


            })

          }


        } else if (json.shipto=='' && json.consineeName != '' && json.emptybox == '' && json.accountName =='') {

          console.log('ppppppppppppppppppppppppp');

          if (json.totalGst =="0.00") {

            content.push({

              table: {
                widths: [200, 'auto'],
                // heights: [85, 0],
                body:[
                  [{image:taxinvoice,width: 204,height:140,border:[true,true,true,true]},{text: [{text:'Bill To : \n',alignment:'left',fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: json.customerName +'\n',border:[false,false,false,false],bold:1,alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text:json.custAddress,bold:1,alignment:'left',border:[false,false,false,false],fontSize: 9,fonts: 'TimesNewRoman'},{text: '\nPhone  : ' + json.custPhone,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nEmail    : ' + json.email,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nGSTIN  : ' + json.gstIn,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nState    : ' + json.statename + '   State Code : ' + json.statecode,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nShip To : \n',alignment:'left',fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: json.consineeName,bold:1,alignment:'left',fontSize: 10,fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: '\nConsinee : ' + json.accountaddress,bold:1,alignment:'left',fontSize: 10,fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: '\nPhone  : ' + json.accountlandline,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nEmail    : ' + json.accountemail + '\nGSTIN  : ' + json.accountgst + '\nState    : ' + json.accountStatename + '   State Code : ' + json.accountStateCode,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'}],rowSpan:5}],
                  [{text:'Invoice No      :   ' +  json.invoice_bill,fontSize: 10,bold: 1,absolutePosition: {x: 42, y: 303},color: '#000000',fonts: 'TimesNewRoman'},{}],
                  [{text:'Invoice Date   :   '   +  convertDate(json.invoiceDate),margin: [2,-7,0,0],alignment: 'left',fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],
                  [{text:'Due Date         :   ' + convertDate(json.dueDate),margin: [2,5,0,0],alignment: 'left',fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],
                  [{text:'Lut No              :   ' + json.branchlutno,alignment: 'left',margin: [2,5,0,0],fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],
                ]

              },			layout: 'noBorders'


            })


          } else {

            content.push({

              table: {
                widths: [200, 'auto'],
                // heights: [85, 0],
                body:[
                  [{image:taxinvoice,width: 204,height:140,border:[true,true,true,true]},{text: [{text:'Bill To : \n',alignment:'left',fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: json.customerName +'\n',border:[false,false,false,false],bold:1,alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text:json.custAddress,bold:1,alignment:'left',border:[false,false,false,false],fontSize: 9,fonts: 'TimesNewRoman'},{text: '\nPhone  : ' + json.custPhone,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nEmail    : ' + json.email,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nGSTIN  : ' + json.gstIn,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nState    : ' + json.statename + '   State Code : ' + json.statecode,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nShip To : \n',alignment:'left',fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: json.consineeName,bold:1,alignment:'left',fontSize: 10,fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: '\nConsinee : ' + json.accountaddress,bold:1,alignment:'left',fontSize: 10,fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: '\nPhone  : ' + json.accountlandline,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nEmail    : ' + json.accountemail + '\nGSTIN  : ' + json.accountgst + '\nState    : ' + json.accountStatename + '   State Code : ' + json.accountStateCode,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'}],rowSpan:4}],
                  [{text:'Invoice No      :   ' +  json.invoice_bill,fontSize: 10,bold: 1,absolutePosition: {x: 42, y: 303},color: '#000000',fonts: 'TimesNewRoman'},{}],
                  [{text:'Invoice Date   :   '   +  convertDate(json.invoiceDate),margin: [2,-7,0,0],alignment: 'left',fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],
                  [{text:'Due Date         :   ' + convertDate(json.dueDate),margin: [2,5,0,0],alignment: 'left',fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],
                ]

              },			layout: 'noBorders'


            })

          }


        } else if (json.shipto=='' && json.consineeName == '' && json.emptybox != '' && json.accountName == '') {
          if (json.totalGst =="0.00") {

            content.push({

              table: {
                widths: [200, 'auto'],
                // heights: [85, 0],
                body:[
                  [{image:taxinvoice,width: 204,height:140,border:[true,true,true,true]},{text: [{text:'Bill To : \n',alignment:'left',fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: json.customerName+'\n',border:[false,false,false,false],bold:1,alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text:json.custAddress,bold:1,alignment:'left',border:[false,false,false,false],fontSize: 9,fonts: 'TimesNewRoman'},{text: '\nPhone  : ' + json.custPhone,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nEmail    : ' + json.email,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nGSTIN  : ' + json.gstIn,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nState    : ' + json.statename + '   State Code : ' + json.statecode,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\n'+json.emptybox,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'}],rowSpan:5}],
                  [{text:'Invoice No      :   ' +  json.invoice_bill,fontSize: 10,bold: 1,absolutePosition: {x: 42, y: 303},color: '#000000',fonts: 'TimesNewRoman'},{}],
                  [{text:'Invoice Date   :   '   +  convertDate(json.invoiceDate),margin: [2,-7,0,0],alignment: 'left',fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],
                  [{text:'Due Date         :   ' + convertDate(json.dueDate),margin: [2,5,0,0],alignment: 'left',fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],
                  [{text:'Lut No              :   ' + json.branchlutno,alignment: 'left',margin: [2,5,0,0],fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],


                ]

              },			layout: 'noBorders'


            })
            
          } else {

            content.push({

              table: {
                widths: [200, 'auto'],
                // heights: [85, 0],
                body:[
                  [{image:taxinvoice,width: 204,height:140,border:[true,true,true,true]},{text: [{text:'Bill To : \n',alignment:'left',fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: json.customerName+'\n',border:[false,false,false,false],bold:1,alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text:json.custAddress,bold:1,alignment:'left',border:[false,false,false,false],fontSize: 9,fonts: 'TimesNewRoman'},{text: '\nPhone  : ' + json.custPhone,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nEmail    : ' + json.email,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nGSTIN  : ' + json.gstIn,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nState    : ' + json.statename + '   State Code : ' + json.statecode,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\n'+json.emptybox,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'}],rowSpan:4}],
                  [{text:'Invoice No      :   ' +  json.invoice_bill,fontSize: 10,bold: 1,absolutePosition: {x: 42, y: 303},color: '#000000',fonts: 'TimesNewRoman'},{}],
                  [{text:'Invoice Date   :   '   +  convertDate(json.invoiceDate),margin: [2,-7,0,0],alignment: 'left',fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],
                  [{text:'Due Date         :   ' + convertDate(json.dueDate),margin: [2,5,0,0],alignment: 'left',fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],

                ]

              },layout: 'noBorders'


            })

          }

        } else if (json.shipto=='' && json.consineeName == '' && json.emptybox == '' && json.accountName != '') {

          if (json.totalGst =="0.00") {

            content.push({

              table: {
                widths: [200, 'auto'],
                // heights: [85, 0],
                body:[
                  [{image:taxinvoice,width: 204,height:140,border:[true,true,true,true]},{text: [{text:'Bill To : \n',alignment:'left',fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: json.customerName+'\nA/C. : ' + json.accountName+'\n',border:[false,false,false,false],bold:1,alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text:json.custAddress,bold:1,alignment:'left',border:[false,false,false,false],fontSize: 9,fonts: 'TimesNewRoman'},{text: '\nPhone  : ' + json.custPhone,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nEmail    : ' + json.email,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nGSTIN  : ' + json.gstIn,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nState    : ' + json.statename + '   State Code : ' + json.statecode,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'}],rowSpan:5}],
                  [{text:'Invoice No      :   ' +  json.invoice_bill,fontSize: 10,bold: 1,absolutePosition: {x: 42, y: 303},color: '#000000',fonts: 'TimesNewRoman'},{}],
                  [{text:'Invoice Date   :   '   +  convertDate(json.invoiceDate),margin: [2,-7,0,0],alignment: 'left',fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],
                  [{text:'Due Date         :   ' + convertDate(json.dueDate),margin: [2,5,0,0],alignment: 'left',fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],
                  [{text:'Lut No              :   ' + json.branchlutno,alignment: 'left',margin: [2,5,0,0],fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],


                ]

              },			layout: 'noBorders'


            })
            
          } else {

            content.push({

              table: {
                widths: [200, 'auto'],
                // heights: [85, 0],
                body:[
                  [{image:taxinvoice,width: 204,height:140,border:[true,true,true,true]},{text: [{text:'Bill To : \n',alignment:'left',fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: json.customerName+'\nA/C. : ' + json.accountName+'\n',border:[false,false,false,false],bold:1,alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text:json.custAddress,bold:1,alignment:'left',border:[false,false,false,false],fontSize: 9,fonts: 'TimesNewRoman'},{text: '\nPhone  : ' + json.custPhone,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nEmail    : ' + json.email,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nGSTIN  : ' + json.gstIn,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nState    : ' + json.statename + '   State Code : ' + json.statecode,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'}],rowSpan:4}],
                  [{text:'Invoice No      :   ' +  json.invoice_bill,fontSize: 10,bold: 1,absolutePosition: {x: 42, y: 303},color: '#000000',fonts: 'TimesNewRoman'},{}],
                  [{text:'Invoice Date   :   '   +  convertDate(json.invoiceDate),margin: [2,-7,0,0],alignment: 'left',fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],
                  [{text:'Due Date         :   ' + convertDate(json.dueDate),margin: [2,5,0,0],alignment: 'left',fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],

                ]

              },layout: 'noBorders'


            })

          }

        } else {


          if (json.totalGst =="0.00") {

            content.push({

              table: {
                widths: [200, 'auto'],
                // heights: [85, 0],
                body:[
                  [{image:taxinvoice,width: 204,height:140,border:[true,true,true,true]},{text: [{text:'Bill To : \n',alignment:'left',fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: json.customerName+'\n',border:[false,false,false,false],bold:1,alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text:json.custAddress,bold:1,alignment:'left',border:[false,false,false,false],fontSize: 9,fonts: 'TimesNewRoman'},{text: '\nPhone  : ' + json.custPhone,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nEmail    : ' + json.email,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nGSTIN  : ' + json.gstIn,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nState    : ' + json.statename + '   State Code : ' + json.statecode,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'}],rowSpan:5}],
                  [{text:'Invoice No      :   ' +  json.invoice_bill,fontSize: 10,bold: 1,absolutePosition: {x: 42, y: 303},color: '#000000',fonts: 'TimesNewRoman'},{}],
                  [{text:'Invoice Date   :   '   +  convertDate(json.invoiceDate),margin: [2,-7,0,0],alignment: 'left',fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],
                  [{text:'Due Date         :   ' + convertDate(json.dueDate),margin: [2,5,0,0],alignment: 'left',fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],
                  [{text:'Lut No              :   ' + json.branchlutno,alignment: 'left',margin: [2,5,0,0],fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],


                ]

              },			layout: 'noBorders'


            })
            
          } else {

            content.push({

              table: {
                widths: [200, 'auto'],
                // heights: [85, 0],
                body:[
                  [{image:taxinvoice,width: 204,height:140,border:[true,true,true,true]},{text: [{text:'Bill To : \n',alignment:'left',fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: json.customerName+'\n',border:[false,false,false,false],bold:1,alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text:json.custAddress,bold:1,alignment:'left',border:[false,false,false,false],fontSize: 9,fonts: 'TimesNewRoman'},{text: '\nPhone  : ' + json.custPhone,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nEmail    : ' + json.email,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nGSTIN  : ' + json.gstIn,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nState    : ' + json.statename + '   State Code : ' + json.statecode,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'}],rowSpan:4}],
                  [{text:'Invoice No      :   ' +  json.invoice_bill,fontSize: 10,bold: 1,absolutePosition: {x: 42, y: 303},color: '#000000',fonts: 'TimesNewRoman'},{}],
                  [{text:'Invoice Date   :   '   +  convertDate(json.invoiceDate),margin: [2,-7,0,0],alignment: 'left',fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],
                  [{text:'Due Date         :   ' + convertDate(json.dueDate),margin: [2,5,0,0],alignment: 'left',fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],

                ]

              },layout: 'noBorders'


            })

          }


        }
                

        content.push({
            text: '\n'
        });

        var currencySymbol = '';
        var currencyFullFom ='';
        if (json.currency == 'Rs') {

          currencySymbol = '₹';
          currencyFullFom = 'Rupees';


        } else if(json.currency == 'USD'){

          currencySymbol = '$';
          currencyFullFom = 'USD';

        }

        table1 = {
          widths: [],
          dontBreakRows: true,
          body: []
        };
        table1['widths'].push('auto', '*', 32, 'auto', 40, 40,'auto','auto', 'auto', 50);
        table1['body'].push([{text:'#',border: [true, true, true, false],bold:1,fillColor: '#bdc3c7'}, {text:'Item & Description',border: [true, true, true, false],bold:1,alignment:'center',fillColor: '#bdc3c7'},{text:'SAC',border: [true, true, true, false],bold:1,alignment:'center',fillColor: '#bdc3c7'}, {text:'Qty',border: [true, true, true, false],bold:1,alignment:'center',fillColor: '#bdc3c7'},{text:'Rate',border: [true, true, true, false],bold:1,alignment:'center',fillColor: '#bdc3c7'},{text:'Taxable Value',border: [true, true, true, false],bold:1,alignment:'center',fillColor: '#bdc3c7'}, {text:'Tax Type',border: [true, true, true, false],bold:1,alignment:'center',fillColor: '#bdc3c7'},{text:'Tax Rate',border: [true, true, true, false],bold:1,alignment:'center',fillColor: '#bdc3c7'},{text:'Tax Amount',border: [true, true, true, false],bold:1,alignment:'center',fillColor: '#bdc3c7'}, {text:'Amount',border: [true, true, true, false],bold:1,alignment:'center',fillColor: '#bdc3c7'}]);
        table1['body'].push([{text:'',border: [true, false, true, false],bold:1,fillColor: '#bdc3c7'}, {text:'',border: [true, false, true, false],bold:1,alignment:'center',fillColor: '#bdc3c7'},{text:'',border: [true, false, true, false],bold:1,alignment:'center',fillColor: '#bdc3c7'}, {text:'',border: [true, false, true, false],bold:1,alignment:'center',fillColor: '#bdc3c7'},{text:currencySymbol,border: [true, true, true, false],bold:1,alignment:'center',fillColor: '#bdc3c7'},{text:currencySymbol,border: [true, true, true, false],bold:1,alignment:'center',fillColor: '#bdc3c7'}, {text:'',border: [true, false, true, false],bold:1,alignment:'center',fillColor: '#bdc3c7'},{text:'',border: [true, false, true, false],bold:1,alignment:'center',fillColor: '#bdc3c7'},{text:currencySymbol,border: [true, true, true, false],bold:1,alignment:'center',fillColor: '#bdc3c7'}, {text:currencySymbol,border: [true, true, true, false],bold:1,alignment:'center',fillColor: '#bdc3c7'}]);

        var serviceJson = json.service_json;
         
        var couter = 1;

        for (var i in serviceJson) {
          var arr = [];


          let regExpr = /[`^]/gi;

          var conditionandscope = serviceJson[i].itemDescription.replace(regExpr, '\n');

          if (json.branchStateCode==json.statecode) {

            arr.push(couter, {text:[{text:serviceJson[i].servicename +'\n',bold:1}, {text:conditionandscope,fontSize: 7}]}, {text:serviceJson[i].sac,alignment: 'center'}, {text:serviceJson[i].qty +'\n'+ serviceJson[i].qty1,alignment: 'center'}, {text: serviceJson[i].rate,alignment: 'center'},{text: serviceJson[i].amount,alignment: 'center'},{text:'CGST \n SGST',alignment: 'center'},{text: serviceJson[i].cgstper+'% \n'+ serviceJson[i].sgstper+'%',alignment: 'center'},{text: serviceJson[i].cgstamount+'\n' +  serviceJson[i].sgstamount,alignment: 'center'},{text: serviceJson[i].totalamount,alignment: 'center'});

          } else {

            arr.push(couter, {text:[{text:serviceJson[i].servicename +'\n',bold:1}, {text:conditionandscope,fontSize: 7}]}, {text:serviceJson[i].sac,alignment: 'center'}, {text:serviceJson[i].qty +'\n'+ serviceJson[i].qty1,alignment: 'center'}, {text: serviceJson[i].rate,alignment: 'center'},{text: serviceJson[i].amount,alignment: 'center'},{text:'IGST',alignment: 'center'},{text: serviceJson[i].igstper+'%',alignment: 'center'},{text: serviceJson[i].igstamount,alignment: 'center'},{text: serviceJson[i].totalamount,alignment: 'center'});

          }
          table1['body'].push(arr);

          couter++;

        }
        content.push({
            style: 'tablfont',
            table: table1,
            layout: {
            hLineColor: function (i, node) {
              return (i === 0 || i === node.table.body.length) ? 'black' : 'black';
            },
            vLineColor: function (i, node) {
              return (i === 0 || i === node.table.widths.length) ? 'black' : 'black';
            },

          },

       
          
        });

       

        content.push({
            text: '\n'
        });

        let borders = '';


        let subTotal =  parseFloat(json.subTotal)+parseFloat(json.totalGst);

            if (json.currency == 'Rs' && json.notes != '' && json.addTerm != '' ) {
              
              content.push({
                id:'signature',
                table: {
                      widths: [300,'*','*'],
                      body:[
                        [{text: 'Total In Words\n' + currencyFullFom + ' ' + convertNumberToWords(json.netAmount),alignment:'left',border:borders,fontSize: 10,rowSpan:2},{text: 'Sub Total ',margin:[20,0,0,0],alignment:'left',fonts: 'TimesNewRoman',fontSize: 10,border:[true,true,false,false]},{text: subTotal,alignment:'right',fontSize: 10,fonts: 'TimesNewRoman',border:[false,true,true,false]}],
                        [{},{text: 'Rounded Off ',margin:[20,0,0,0],alignment:'left',fonts: 'TimesNewRoman',fontSize: 10,border:[true,false,false,false]},{text: json.roundOff,alignment:'right',fontSize: 10,fonts: 'TimesNewRoman',border:[false,false,true,false]}],
                        [{text: 'Terms and Conditions',bold:1,fontSize: 10,alignment:'left',border:[true,false,false,false],rowSpan:1},{text: 'Total ',margin:[20,0,0,0],alignment:'left',bold:1,fonts: 'TimesNewRoman',fontSize: 10,border:[true,false,false,false]},{text: currencySymbol + ' ' + json.netAmount,alignment:'right',fontSize: 10,bold:1,fonts: 'TimesNewRoman',border:[false,false,true,false]}],
                        [{text: 'Interest @18 % will be charged on all overdue Invoices',fontSize: 10,alignment:'left',border:[true,false,false,false]},{text: 'Payment Made ',margin:[20,0,0,0],alignment:'left',fonts: 'TimesNewRoman',fontSize: 10,border:[true,false,false,false]},{text: currencySymbol + ' ' + json.receiveAmt,alignment:'right',fontSize: 10,fonts: 'TimesNewRoman',border:[false,false,true,false]}],
                        [{text:'All disputes, claims and / Or any differences arising on any matter ralating to this invoice are subject to be exclusive jurisdiction of the courts at Mumbai.',fontSize: 10,alignment:'left',border:[true,false,false,false]},{text: 'Balance Due ',margin:[20,0,0,0],alignment:'left',bold:1,fonts: 'TimesNewRoman',fontSize: 10,border:[true,false,false,true]},{text:currencySymbol + ' ' + json.netamt,alignment:'right',fontSize: 10,bold:1,fonts: 'TimesNewRoman',border:[false,false,true,true]}],
                        [{text: 'Rejection or any discrepancies in the invoice to be intimated within a period on 15 days from date of invoice.',fontSize: 10,alignment:'left',border:[true,false,false,false]},{text: 'For Pest Mortem (India) Pvt. Ltd. ',alignment:'center',fonts: 'TimesNewRoman',fontSize: 10,border:[true,false,true,false],colSpan:2,rowSpan:2},{}],
                        [{text: json.addTerm,fontSize: 10,alignment:'left',border:[true,false,false,true]},{},{}],
                        [{text:'Company Name : Pest Mortem (India) Pvt. Ltd.  ',border: [true, false, true, false],alignment:'left',fontSize: 9,fonts: 'TimesNewRoman'},{text: '',width: 100,height: 50,margin:[40,-20,0,0],border: [false, false, true, false],colSpan:2,rowSpan:4},{}],
                        [{text:'Bank Name     : '+ json.bankName  + ' , ' + json.branchName ,border: [true, false, true, false],alignment:'left',fontSize: 9,fonts: 'TimesNewRoman'},{},{}],
                        [{text:'Account No     : '+ json.accountNo,border: [true, false, true, false],alignment:'left',fontSize: 9,fonts: 'TimesNewRoman'},{},{}],
                        [{text:'RTGS/NEFT/IFSC Code : '+ json.ifscCode+'\n',border: [true, false, true, false],alignment:'left',fontSize: 9,fonts: 'TimesNewRoman'},{},{}],
                        [{id:'closingParagraph',text:'Notes : ' + json.notes,margin:[0,0,0,3],alignment:'left',fontSize: 9,fonts: 'TimesNewRoman',border: [true, false, false, true]},{text: 'Authorised Signatory ',alignment:'center',fonts: 'TimesNewRoman',fontSize: 10,border:[true,false,true,true],colSpan:2},{}],

                      ],

                    },layout: {
                        hLineColor: function (i, node) {
                        return (i === 0 || i === node.table.body.length) ? 'black' : 'black';
                        },
                        vLineColor: function (i, node) {
                        return (i === 0 || i === node.table.widths.length) ? 'black' : 'black';
                        },
                    },
                    
                   
                    
                                      
              });

            } else if (json.currency == 'Rs' && json.notes == '' && json.addTerm != '') {

              content.push({
                id:'signature',
                table: {
                      widths: [300,'*','*'],
                      body:[
                        [{text: 'Total In Words\n' + currencyFullFom + ' ' + convertNumberToWords(json.netAmount),alignment:'left',border:borders,fontSize: 10,rowSpan:2},{text: 'Sub Total ',margin:[20,0,0,0],alignment:'left',fonts: 'TimesNewRoman',fontSize: 10,border:[true,true,false,false]},{text: subTotal,alignment:'right',fontSize: 10,fonts: 'TimesNewRoman',border:[false,true,true,false]}],
                        [{},{text: 'Rounded Off ',margin:[20,0,0,0],alignment:'left',fonts: 'TimesNewRoman',fontSize: 10,border:[true,false,false,false]},{text: json.roundOff,alignment:'right',fontSize: 10,fonts: 'TimesNewRoman',border:[false,false,true,false]}],
                        [{text: 'Terms and Conditions',bold:1,fontSize: 10,alignment:'left',border:[true,false,false,false],rowSpan:1},{text: 'Total ',margin:[20,0,0,0],alignment:'left',bold:1,fonts: 'TimesNewRoman',fontSize: 10,border:[true,false,false,false]},{text: currencySymbol + ' ' + json.netAmount,alignment:'right',fontSize: 10,bold:1,fonts: 'TimesNewRoman',border:[false,false,true,false]}],
                        [{text: 'Interest @18 % will be charged on all overdue Invoices',fontSize: 10,alignment:'left',border:[true,false,false,false]},{text: 'Payment Made ',margin:[20,0,0,0],alignment:'left',fonts: 'TimesNewRoman',fontSize: 10,border:[true,false,false,false]},{text: currencySymbol + ' ' + json.receiveAmt,alignment:'right',fontSize: 10,fonts: 'TimesNewRoman',border:[false,false,true,false]}],
                        [{text:'All disputes, claims and / Or any differences arising on any matter ralating to this invoice are subject to be exclusive jurisdiction of the courts at Mumbai.',fontSize: 10,alignment:'left',border:[true,false,false,false]},{text: 'Balance Due ',margin:[20,0,0,0],alignment:'left',bold:1,fonts: 'TimesNewRoman',fontSize: 10,border:[true,false,false,true]},{text:currencySymbol + ' ' + json.netamt,alignment:'right',fontSize: 10,bold:1,fonts: 'TimesNewRoman',border:[false,false,true,true]}],
                        [{text: 'Rejection or any discrepancies in the invoice to be intimated within a period on 15 days from date of invoice.',fontSize: 10,alignment:'left',border:[true,false,false,false]},{text: 'For Pest Mortem (India) Pvt. Ltd. ',alignment:'center',fonts: 'TimesNewRoman',fontSize: 10,border:[true,false,true,false],colSpan:2,rowSpan:2},{}],
                        [{text: json.addTerm,fontSize: 10,alignment:'left',border:[true,false,false,true]},{},{}],
                        [{text:'Company Name : Pest Mortem (India) Pvt. Ltd.  ',border: [true, false, true, false],alignment:'left',fontSize: 9,fonts: 'TimesNewRoman'},{text: '',width: 100,height: 50,margin:[40,-20,0,0],border: [false, false, true, false],colSpan:2,rowSpan:4},{}],
                        [{text:'Bank Name     : '+ json.bankName  + ' , ' + json.branchName ,border: [true, false, true, false],alignment:'left',fontSize: 9,fonts: 'TimesNewRoman'},{},{}],
                        [{text:'Account No     : '+ json.accountNo,border: [true, false, true, false],alignment:'left',fontSize: 9,fonts: 'TimesNewRoman'},{},{}],
                        [{text:'RTGS/NEFT/IFSC Code : '+ json.ifscCode+'\n',border: [true, false, true, true],alignment:'left',fontSize: 9,fonts: 'TimesNewRoman',rowSpan:2},{},{}],
                        [{},{text: 'Authorised Signatory ',alignment:'center',fonts: 'TimesNewRoman',fontSize: 10,border:[true,false,true,true],colSpan:2},{}],



                      ],

                    },layout: {
                        hLineColor: function (i, node) {
                        return (i === 0 || i === node.table.body.length) ? 'black' : 'black';
                        },
                        vLineColor: function (i, node) {
                        return (i === 0 || i === node.table.widths.length) ? 'black' : 'black';
                        },
                    }
              });


            } else if (json.currency == 'Rs' && json.notes != '' && json.addTerm == '') {

              content.push({
                id:'signature',
                table: {
                      widths: [300,'*','*'],
                      body:[
                        [{text: 'Total In Words\n' + currencyFullFom + ' ' + convertNumberToWords(json.netAmount),alignment:'left',border:borders,fontSize: 10,rowSpan:2},{text: 'Sub Total ',margin:[20,0,0,0],alignment:'left',fonts: 'TimesNewRoman',fontSize: 10,border:[true,true,false,false]},{text: subTotal,alignment:'right',fontSize: 10,fonts: 'TimesNewRoman',border:[false,true,true,false]}],
                        [{},{text: 'Rounded Off ',margin:[20,0,0,0],alignment:'left',fonts: 'TimesNewRoman',fontSize: 10,border:[true,false,false,false]},{text: json.roundOff,alignment:'right',fontSize: 10,fonts: 'TimesNewRoman',border:[false,false,true,false]}],
                        [{text: 'Terms and Conditions',bold:1,fontSize: 10,alignment:'left',border:[true,false,false,false],rowSpan:1},{text: 'Total ',margin:[20,0,0,0],alignment:'left',bold:1,fonts: 'TimesNewRoman',fontSize: 10,border:[true,false,false,false]},{text: currencySymbol + ' ' + json.netAmount,alignment:'right',fontSize: 10,bold:1,fonts: 'TimesNewRoman',border:[false,false,true,false]}],
                        [{text: 'Interest @18 % will be charged on all overdue Invoices',fontSize: 10,alignment:'left',border:[true,false,false,false]},{text: 'Payment Made ',margin:[20,0,0,0],alignment:'left',fonts: 'TimesNewRoman',fontSize: 10,border:[true,false,false,false]},{text: currencySymbol + ' ' + json.receiveAmt,alignment:'right',fontSize: 10,fonts: 'TimesNewRoman',border:[false,false,true,false]}],
                        [{text:'All disputes, claims and / Or any differences arising on any matter ralating to this invoice are subject to be exclusive jurisdiction of the courts at Mumbai.',fontSize: 10,alignment:'left',border:[true,false,false,false]},{text: 'Balance Due ',margin:[20,0,0,0],alignment:'left',bold:1,fonts: 'TimesNewRoman',fontSize: 10,border:[true,false,false,true]},{text:currencySymbol + ' ' + json.netamt,alignment:'right',fontSize: 10,bold:1,fonts: 'TimesNewRoman',border:[false,false,true,true]}],
                        [{text: 'Rejection or any discrepancies in the invoice to be intimated within a period on 15 days from date of invoice.',fontSize: 10,alignment:'left',border:[true,false,false,true]},{text: 'For Pest Mortem (India) Pvt. Ltd. ',alignment:'center',fonts: 'TimesNewRoman',fontSize: 10,border:[true,false,true,false],colSpan:2,rowSpan:1},{}],
                        [{text:'Company Name : Pest Mortem (India) Pvt. Ltd.  ',border: [true, false, true, false],alignment:'left',fontSize: 9,fonts: 'TimesNewRoman'},{text: '',width: 100,height: 50,margin:[40,-10,0,0],border: [false, false, true, false],colSpan:2,rowSpan:4},{}],
                        [{text:'Bank Name     : '+ json.bankName  + ' , ' + json.branchName ,border: [true, false, true, false],alignment:'left',fontSize: 9,fonts: 'TimesNewRoman'},{},{}],
                        [{text:'Account No     : '+ json.accountNo,border: [true, false, true, false],alignment:'left',fontSize: 9,fonts: 'TimesNewRoman'},{},{}],
                        [{text:'RTGS/NEFT/IFSC Code : '+ json.ifscCode+'\n',border: [true, false, true, false],alignment:'left',fontSize: 9,fonts: 'TimesNewRoman'},{},{}],
                        [{id:'closingParagraph',text:'Notes : ' + json.notes,margin:[0,0,0,3],alignment:'left',fontSize: 9,fonts: 'TimesNewRoman',border: [true, false, false, true]},{text: 'Authorised Signatory ',alignment:'center',fonts: 'TimesNewRoman',fontSize: 10,border:[true,false,true,true],colSpan:2},{}],



                      ],

                    },layout: {
                        hLineColor: function (i, node) {
                        return (i === 0 || i === node.table.body.length) ? 'black' : 'black';
                        },
                        vLineColor: function (i, node) {
                        return (i === 0 || i === node.table.widths.length) ? 'black' : 'black';
                        },
                    }
              });

            } else if (json.currency == 'Rs' && json.notes == '' && json.addTerm == '') {

              content.push({
                id:'signature',
                table: {
                      widths: [300,'*','*'],
                      body:[
                        [{text: 'Total In Words\n' + currencyFullFom + ' ' + convertNumberToWords(json.netAmount),alignment:'left',border:borders,fontSize: 10,rowSpan:2},{text: 'Sub Total ',margin:[20,0,0,0],alignment:'left',fonts: 'TimesNewRoman',fontSize: 10,border:[true,true,false,false]},{text: subTotal,alignment:'right',fontSize: 10,fonts: 'TimesNewRoman',border:[false,true,true,false]}],
                        [{},{text: 'Rounded Off ',margin:[20,0,0,0],alignment:'left',fonts: 'TimesNewRoman',fontSize: 10,border:[true,false,false,false]},{text: json.roundOff,alignment:'right',fontSize: 10,fonts: 'TimesNewRoman',border:[false,false,true,false]}],
                        [{text: 'Terms and Conditions',bold:1,fontSize: 10,alignment:'left',border:[true,false,false,false],rowSpan:1},{text: 'Total ',margin:[20,0,0,0],alignment:'left',bold:1,fonts: 'TimesNewRoman',fontSize: 10,border:[true,false,false,false]},{text: currencySymbol + ' ' + json.netAmount,alignment:'right',fontSize: 10,bold:1,fonts: 'TimesNewRoman',border:[false,false,true,false]}],
                        [{text: 'Interest @18 % will be charged on all overdue Invoices',fontSize: 10,alignment:'left',border:[true,false,false,false]},{text: 'Payment Made ',margin:[20,0,0,0],alignment:'left',fonts: 'TimesNewRoman',fontSize: 10,border:[true,false,false,false]},{text: currencySymbol + ' ' + json.receiveAmt,alignment:'right',fontSize: 10,fonts: 'TimesNewRoman',border:[false,false,true,false]}],
                        [{text:'All disputes, claims and / Or any differences arising on any matter ralating to this invoice are subject to be exclusive jurisdiction of the courts at Mumbai.',fontSize: 10,alignment:'left',border:[true,false,false,false]},{text: 'Balance Due ',margin:[20,0,0,0],alignment:'left',bold:1,fonts: 'TimesNewRoman',fontSize: 10,border:[true,false,false,true]},{text:currencySymbol + ' ' + json.netamt,alignment:'right',fontSize: 10,bold:1,fonts: 'TimesNewRoman',border:[false,false,true,true]}],
                        [{text: 'Rejection or any discrepancies in the invoice to be intimated within a period on 15 days from date of invoice.',fontSize: 10,alignment:'left',border:[true,false,false,true]},{text: 'For Pest Mortem (India) Pvt. Ltd. ',alignment:'center',fonts: 'TimesNewRoman',fontSize: 10,border:[true,false,true,false],colSpan:2,rowSpan:1},{}],
                        [{text:'Company Name : Pest Mortem (India) Pvt. Ltd.  ',border: [true, false, true, false],alignment:'left',fontSize: 9,fonts: 'TimesNewRoman'},{text: '',width: 100,height: 50,margin:[40,-10,0,0],border: [false, false, true, false],colSpan:2,rowSpan:4},{}],
                        [{text:'Bank Name     : '+ json.bankName  + ' , ' + json.branchName ,border: [true, false, true, false],alignment:'left',fontSize: 9,fonts: 'TimesNewRoman'},{},{}],
                        [{text:'Account No     : '+ json.accountNo,border: [true, false, true, false],alignment:'left',fontSize: 9,fonts: 'TimesNewRoman'},{},{}],
                        [{text:'RTGS/NEFT/IFSC Code : '+ json.ifscCode+'\n',border: [true, false, true, true],alignment:'left',fontSize: 9,fonts: 'TimesNewRoman',rowSpan:2},{},{}],
                        [{},{id:'closingParagraph',text: 'Authorised Signatory ',alignment:'center',fonts: 'TimesNewRoman',fontSize: 10,border:[true,false,true,true],colSpan:2},{}],

                      ],

                    },layout: {
                        hLineColor: function (i, node) {
                        return (i === 0 || i === node.table.body.length) ? 'black' : 'black';
                        },
                        vLineColor: function (i, node) {
                        return (i === 0 || i === node.table.widths.length) ? 'black' : 'black';
                        },
                    }
              });

            }else if (json.currency == 'USD' && json.notes != '' && json.addTerm != '' ) {
              
              content.push({
                id:'signature',
                table: {
                      widths: [300,'*','*'],
                      body:[
                        [{text: 'Total In Words\n' + currencyFullFom + ' ' + convertNumberToWords(json.netAmount),alignment:'left',border:[true,true,false,true],fontSize: 10,rowSpan:2},{text: 'Sub Total ',margin:[20,0,0,0],alignment:'left',fonts: 'TimesNewRoman',fontSize: 10,border:[true,true,false,false]},{text: subTotal,alignment:'right',fontSize: 10,fonts: 'TimesNewRoman',border:[false,true,true,false]}],
                        [{},{text: 'Rounded Off ',margin:[20,0,0,0],alignment:'left',fonts: 'TimesNewRoman',fontSize: 10,border:[true,false,false,false]},{text: json.roundOff,alignment:'right',fontSize: 10,fonts: 'TimesNewRoman',border:[false,false,true,false]}],
                        [{text: 'Terms and Conditions',bold:1,fontSize: 10,alignment:'left',border:[true,false,false,false],rowSpan:1},{text: 'Total ',margin:[20,0,0,0],alignment:'left',bold:1,fonts: 'TimesNewRoman',fontSize: 10,border:[true,false,false,false]},{text: currencySymbol + ' ' + json.netAmount,alignment:'right',fontSize: 10,bold:1,fonts: 'TimesNewRoman',border:[false,false,true,false]}],
                        [{text: 'Interest @18 % will be charged on all overdue Invoices',fontSize: 10,alignment:'left',border:[true,false,false,false]},{text: 'Payment Made ',margin:[20,0,0,0],alignment:'left',fonts: 'TimesNewRoman',fontSize: 10,border:[true,false,false,false]},{text: currencySymbol + ' ' + json.receiveAmt,alignment:'right',fontSize: 10,fonts: 'TimesNewRoman',border:[false,false,true,false]}],
                        [{text:'All disputes, claims and / Or any differences arising on any matter ralating to this invoice are subject to be exclusive jurisdiction of the courts at Mumbai.',fontSize: 10,alignment:'left',border:[true,false,false,false]},{text: 'Balance Due ',margin:[20,0,0,0],alignment:'left',bold:1,fonts: 'TimesNewRoman',fontSize: 10,border:[true,false,false,true]},{text:currencySymbol + ' ' + json.netamt,alignment:'right',fontSize: 10,bold:1,fonts: 'TimesNewRoman',border:[false,false,true,true]}],
                        [{text: 'Rejection or any discrepancies in the invoice to be intimated within a period on 15 days from date of invoice.',fontSize: 10,alignment:'left',border:[true,false,false,false]},{text: 'For Pest Mortem (India) Pvt. Ltd. ',alignment:'center',fonts: 'TimesNewRoman',fontSize: 10,border:[true,false,true,false],colSpan:2,rowSpan:2},{}],
                        [{text: json.addTerm,fontSize: 10,alignment:'left',border:[true,false,false,true]},{},{}],
                        [{text:'PLEASE REMIT FUNDS TO -  \nINTERMEDIARY BANK: JP MORGAN CHASE BANK, NEW YORK, USA.\n SWIFT CODE : CHASUS33 \n FEDWIRE ABA NO.021000021\nCHIPS ABA NO.002 CHIPS UID NO.354459 \n FAVOURING BENEFICIARY BANK : HDFC BANK LTD., \n INDIA SWIFT CODE: HDFCINBB\nHDFC BANK ACCOUNT NO. WITH JP MORGAN 001-1-406717\nBENEFICIARY NAME : Pest Mortem (India) Pvt.Ltd.\n BENEFICIARY ACCOUNT NO.50200017596497',border: [true, false, true, false],alignment:'left',fontSize: 9,fonts: 'TimesNewRoman'},{text: '',width: 100,height: 100,margin:[40,-20,0,0],border: [false, false, true, false],colSpan:2,rowSpan:1},{}],
                        [{id:'closingParagraph',text:'Notes : ' + json.notes,margin:[0,0,0,3],alignment:'left',fontSize: 9,fonts: 'TimesNewRoman',border: [true, false, false, true]},{text: 'Authorised Signatory ',alignment:'center',fonts: 'TimesNewRoman',fontSize: 10,border:[true,false,true,true],colSpan:2},{}],

                      ]
                    },layout: {
                        hLineColor: function (i, node) {
                        return (i === 0 || i === node.table.body.length) ? 'black' : 'black';
                        },
                        vLineColor: function (i, node) {
                        return (i === 0 || i === node.table.widths.length) ? 'black' : 'black';
                        },
                    }
              });

            } else if (json.currency == 'USD' && json.notes == '' && json.addTerm != '' ) {
              
              content.push({
                id:'signature',
                table: {
                      widths: [300,'*','*'],
                      body:[
                        [{text: 'Total In Words\n' + currencyFullFom + ' ' + convertNumberToWords(json.netAmount),alignment:'left',border:[true,true,false,true],fontSize: 10,rowSpan:2},{text: 'Sub Total ',margin:[20,0,0,0],alignment:'left',fonts: 'TimesNewRoman',fontSize: 10,border:[true,true,false,false]},{text: subTotal,alignment:'right',fontSize: 10,fonts: 'TimesNewRoman',border:[false,true,true,false]}],
                        [{},{text: 'Rounded Off ',margin:[20,0,0,0],alignment:'left',fonts: 'TimesNewRoman',fontSize: 10,border:[true,false,false,false]},{text: json.roundOff,alignment:'right',fontSize: 10,fonts: 'TimesNewRoman',border:[false,false,true,false]}],
                        [{text: 'Terms and Conditions',bold:1,fontSize: 10,alignment:'left',border:[true,false,false,false],rowSpan:1},{text: 'Total ',margin:[20,0,0,0],alignment:'left',bold:1,fonts: 'TimesNewRoman',fontSize: 10,border:[true,false,false,false]},{text: currencySymbol + ' ' + json.netAmount,alignment:'right',fontSize: 10,bold:1,fonts: 'TimesNewRoman',border:[false,false,true,false]}],
                        [{text: 'Interest @18 % will be charged on all overdue Invoices',fontSize: 10,alignment:'left',border:[true,false,false,false]},{text: 'Payment Made ',margin:[20,0,0,0],alignment:'left',fonts: 'TimesNewRoman',fontSize: 10,border:[true,false,false,false]},{text: currencySymbol + ' ' + json.receiveAmt,alignment:'right',fontSize: 10,fonts: 'TimesNewRoman',border:[false,false,true,false]}],
                        [{text:'All disputes, claims and / Or any differences arising on any matter ralating to this invoice are subject to be exclusive jurisdiction of the courts at Mumbai.',fontSize: 10,alignment:'left',border:[true,false,false,false]},{text: 'Balance Due ',margin:[20,0,0,0],alignment:'left',bold:1,fonts: 'TimesNewRoman',fontSize: 10,border:[true,false,false,true]},{text:currencySymbol + ' ' + json.netamt,alignment:'right',fontSize: 10,bold:1,fonts: 'TimesNewRoman',border:[false,false,true,true]}],
                        [{text: 'Rejection or any discrepancies in the invoice to be intimated within a period on 15 days from date of invoice.',fontSize: 10,alignment:'left',border:[true,false,false,false]},{text: 'For Pest Mortem (India) Pvt. Ltd. ',alignment:'center',fonts: 'TimesNewRoman',fontSize: 10,border:[true,false,true,false],colSpan:2,rowSpan:2},{}],
                        [{text: json.addTerm,fontSize: 10,alignment:'left',border:[true,false,false,true]},{},{}],
                        [{text:'PLEASE REMIT FUNDS TO -  \nINTERMEDIARY BANK: JP MORGAN CHASE BANK, NEW YORK, USA.\n SWIFT CODE : CHASUS33 \n FEDWIRE ABA NO.021000021\nCHIPS ABA NO.002 CHIPS UID NO.354459 \n FAVOURING BENEFICIARY BANK : HDFC BANK LTD., \n INDIA SWIFT CODE: HDFCINBB\nHDFC BANK ACCOUNT NO. WITH JP MORGAN 001-1-406717\nBENEFICIARY NAME : Pest Mortem (India) Pvt.Ltd.\n BENEFICIARY ACCOUNT NO.50200017596497',border: [true, false, true, true],alignment:'left',fontSize: 9,fonts: 'TimesNewRoman',rowSpan:2},{text: '',width: 100,height: 100,margin:[40,-20,0,0],border: [false, false, true, false],colSpan:2,rowSpan:1},{}],
                        [{},{id:'closingParagraph',text: 'Authorised Signatory ',alignment:'center',fonts: 'TimesNewRoman',fontSize: 10,border:[true,false,true,true],colSpan:2},{}],
                      ]
                    },layout: {
                        hLineColor: function (i, node) {
                        return (i === 0 || i === node.table.body.length) ? 'black' : 'black';
                        },
                        vLineColor: function (i, node) {
                        return (i === 0 || i === node.table.widths.length) ? 'black' : 'black';
                        },
                    }
              });

            } else if (json.currency == 'USD' && json.notes != '' && json.addTerm == '' ) {
              
              content.push({
                id:'signature',
                table: {
                      widths: [300,'*','*'],
                      body:[
                        [{text: 'Total In Words\n' + currencyFullFom + ' ' + convertNumberToWords(json.netAmount),alignment:'left',border:[true,true,false,true],fontSize: 10,rowSpan:2},{text: 'Sub Total ',margin:[20,0,0,0],alignment:'left',fonts: 'TimesNewRoman',fontSize: 10,border:[true,true,false,false]},{text: subTotal,alignment:'right',fontSize: 10,fonts: 'TimesNewRoman',border:[false,true,true,false]}],
                        [{},{text: 'Rounded Off ',margin:[20,0,0,0],alignment:'left',fonts: 'TimesNewRoman',fontSize: 10,border:[true,false,false,false]},{text: json.roundOff,alignment:'right',fontSize: 10,fonts: 'TimesNewRoman',border:[false,false,true,false]}],
                        [{text: 'Terms and Conditions',bold:1,fontSize: 10,alignment:'left',border:[true,false,false,false],rowSpan:1},{text: 'Total ',margin:[20,0,0,0],alignment:'left',bold:1,fonts: 'TimesNewRoman',fontSize: 10,border:[true,false,false,false]},{text: currencySymbol + ' ' + json.netAmount,alignment:'right',fontSize: 10,bold:1,fonts: 'TimesNewRoman',border:[false,false,true,false]}],
                        [{text: 'Interest @18 % will be charged on all overdue Invoices',fontSize: 10,alignment:'left',border:[true,false,false,false]},{text: 'Payment Made ',margin:[20,0,0,0],alignment:'left',fonts: 'TimesNewRoman',fontSize: 10,border:[true,false,false,false]},{text: currencySymbol + ' ' + json.receiveAmt,alignment:'right',fontSize: 10,fonts: 'TimesNewRoman',border:[false,false,true,false]}],
                        [{text:'All disputes, claims and / Or any differences arising on any matter ralating to this invoice are subject to be exclusive jurisdiction of the courts at Mumbai.',fontSize: 10,alignment:'left',border:[true,false,false,false]},{text: 'Balance Due ',margin:[20,0,0,0],alignment:'left',bold:1,fonts: 'TimesNewRoman',fontSize: 10,border:[true,false,false,true]},{text:currencySymbol + ' ' + json.netamt,alignment:'right',fontSize: 10,bold:1,fonts: 'TimesNewRoman',border:[false,false,true,true]}],
                        [{text: 'Rejection or any discrepancies in the invoice to be intimated within a period on 15 days from date of invoice.',fontSize: 10,alignment:'left',border:[true,false,false,true]},{text: 'For Pest Mortem (India) Pvt. Ltd. ',alignment:'center',fonts: 'TimesNewRoman',fontSize: 10,border:[true,false,true,false],colSpan:2,rowSpan:1},{}],
                        [{text:'PLEASE REMIT FUNDS TO -  \nINTERMEDIARY BANK: JP MORGAN CHASE BANK, NEW YORK, USA.\n SWIFT CODE : CHASUS33 \n FEDWIRE ABA NO.021000021\nCHIPS ABA NO.002 CHIPS UID NO.354459 \n FAVOURING BENEFICIARY BANK : HDFC BANK LTD., \n INDIA SWIFT CODE: HDFCINBB\nHDFC BANK ACCOUNT NO. WITH JP MORGAN 001-1-406717\nBENEFICIARY NAME : Pest Mortem (India) Pvt.Ltd.\n BENEFICIARY ACCOUNT NO.50200017596497',border: [true, false, true, false],alignment:'left',fontSize: 9,fonts: 'TimesNewRoman'},{text: '',width: 100,height: 100,margin:[40,-20,0,0],border: [false, false, true, false],colSpan:2,rowSpan:1},{}],
                        [{id:'closingParagraph',text:'Notes : ' + json.notes,margin:[0,0,0,3],alignment:'left',fontSize: 9,fonts: 'TimesNewRoman',border: [true, false, false, true]},{text: 'Authorised Signatory ',alignment:'center',fonts: 'TimesNewRoman',fontSize: 10,border:[true,false,true,true],colSpan:2},{}],

                      ]
                    },layout: {
                        hLineColor: function (i, node) {
                        return (i === 0 || i === node.table.body.length) ? 'black' : 'black';
                        },
                        vLineColor: function (i, node) {
                        return (i === 0 || i === node.table.widths.length) ? 'black' : 'black';
                        },
                    }
              });

            } else if (json.currency == 'USD' && json.notes == '' && json.addTerm == '' ) {
              
              content.push({
                id:'signature',
                table: {
                      widths: [300,'*','*'],
                      body:[
                        [{text: 'Total In Words\n' + currencyFullFom + ' ' + convertNumberToWords(json.netAmount),alignment:'left',border:[true,true,false,true],fontSize: 10,rowSpan:2},{text: 'Sub Total ',margin:[20,0,0,0],alignment:'left',fonts: 'TimesNewRoman',fontSize: 10,border:[true,true,false,false]},{text: subTotal,alignment:'right',fontSize: 10,fonts: 'TimesNewRoman',border:[false,true,true,false]}],
                        [{},{text: 'Rounded Off ',margin:[20,0,0,0],alignment:'left',fonts: 'TimesNewRoman',fontSize: 10,border:[true,false,false,false]},{text: json.roundOff,alignment:'right',fontSize: 10,fonts: 'TimesNewRoman',border:[false,false,true,false]}],
                        [{text: 'Terms and Conditions',bold:1,fontSize: 10,alignment:'left',border:[true,false,false,false],rowSpan:1},{text: 'Total ',margin:[20,0,0,0],alignment:'left',bold:1,fonts: 'TimesNewRoman',fontSize: 10,border:[true,false,false,false]},{text: currencySymbol + ' ' + json.netAmount,alignment:'right',fontSize: 10,bold:1,fonts: 'TimesNewRoman',border:[false,false,true,false]}],
                        [{text: 'Interest @18 % will be charged on all overdue Invoices',fontSize: 10,alignment:'left',border:[true,false,false,false]},{text: 'Payment Made ',margin:[20,0,0,0],alignment:'left',fonts: 'TimesNewRoman',fontSize: 10,border:[true,false,false,false]},{text: currencySymbol + ' ' + json.receiveAmt,alignment:'right',fontSize: 10,fonts: 'TimesNewRoman',border:[false,false,true,false]}],
                        [{text:'All disputes, claims and / Or any differences arising on any matter ralating to this invoice are subject to be exclusive jurisdiction of the courts at Mumbai.',fontSize: 10,alignment:'left',border:[true,false,false,false]},{text: 'Balance Due ',margin:[20,0,0,0],alignment:'left',bold:1,fonts: 'TimesNewRoman',fontSize: 10,border:[true,false,false,true]},{text:currencySymbol + ' ' + json.netamt,alignment:'right',fontSize: 10,bold:1,fonts: 'TimesNewRoman',border:[false,false,true,true]}],
                        [{text: 'Rejection or any discrepancies in the invoice to be intimated within a period on 15 days from date of invoice.',fontSize: 10,alignment:'left',border:[true,false,false,true]},{text: 'For Pest Mortem (India) Pvt. Ltd. ',alignment:'center',fonts: 'TimesNewRoman',fontSize: 10,border:[true,false,true,false],colSpan:2,rowSpan:1},{}],
                        [{text:'PLEASE REMIT FUNDS TO -  \nINTERMEDIARY BANK: JP MORGAN CHASE BANK, NEW YORK, USA.\n SWIFT CODE : CHASUS33 \n FEDWIRE ABA NO.021000021\nCHIPS ABA NO.002 CHIPS UID NO.354459 \n FAVOURING BENEFICIARY BANK : HDFC BANK LTD., \n INDIA SWIFT CODE: HDFCINBB\nHDFC BANK ACCOUNT NO. WITH JP MORGAN 001-1-406717\nBENEFICIARY NAME : Pest Mortem (India) Pvt.Ltd.\n BENEFICIARY ACCOUNT NO.50200017596497',border: [true, false, true, true],alignment:'left',fontSize: 9,fonts: 'TimesNewRoman',rowSpan:2},{text: '',width: 100,height: 100,margin:[40,-20,0,0],border: [false, false, true, false],colSpan:2,rowSpan:1},{}],
                        [{},{id:'closingParagraph',text: 'Authorised Signatory ',alignment:'center',fonts: 'TimesNewRoman',fontSize: 10,border:[true,false,true,true],colSpan:2},{}],

                      ]
                    },layout: {
                        hLineColor: function (i, node) {
                        return (i === 0 || i === node.table.body.length) ? 'black' : 'black';
                        },
                        vLineColor: function (i, node) {
                        return (i === 0 || i === node.table.widths.length) ? 'black' : 'black';
                        },
                    }
              });

            }

        dd = {

          pageSize: 'A4',

          pageOrientation: 'portrait',

          pageMargins: [40, 30, 30, 35],

          pageBreakBefore: function(currentNode, followingNodesOnPage, nodesOnNextPage, previousNodesOnPage) {
            if (currentNode.id === 'signature' && (currentNode.pageNumbers.length != 1 || currentNode.pageNumbers[0] != currentNode.pages)) {
              
              borders = [true,true,false,false];

              return true;
            }
            else if (currentNode.id === 'closingParagraph' && currentNode.pageNumbers.length != 1) {
              borders = [true,false,false,false];
              return true;

            }
            return false;
          },    

          footer: function (currentPage, pageCount) {

            return {

              margin: 10,

              columns: [{

                fontSize: 9,

                text: [{

                    text: '--------------------------------------------------------------------------' +
                      '\n',
                    margin: [0, 20]

                  },
                  {

                    text: [{text:'© Pest Mortem (India) Pvt. Ltd. | '},{text: ' REGD OFFICE : ',bold:1},{text: 'G-2, Sunder Tower, T. J. Road, Sewree (West), Mumbai,400015 | PAGE ' + currentPage.toString() + ' of ' + pageCount}],

                  }

                ],

                alignment: 'center'

              }]


            };

          },

          content,
          
          styles: {

            tablfont: {

              fontSize: 9,
              fonts: 'TimesNewRoman'
            }

          }

        }

        var win = window.open('', '_blank');
        pdfMake.createPdf(dd).open({}, win);


      }

    }

  })

}



$(document).ready(function(){
  $('[data-toggle="tooltip"]').tooltip();   
});

function TriggerOutlook(e) {

  var email        = $(e).attr('data-emailId');
  var invoice_bill = $(e).attr('data-invoice_bill');
  var branchId     = $(e).attr('data-branchid');
  var category     = $(e).attr('data-category');


  var valid = true;

  var regemail = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;

  if (!regemail.test(email)) {

    valid = valid * false;

    alert('Pls Check Customer Email id');

  } else {

    valid = valid * true;

  }

  //var body = href = "http://pestmorten.local/process/invoice/email.php?invoice_bill=" + btoa(invoice_bill)+''+branchId;
  var body = href = "http://pestmortem.co.in/process/invoice/email.php?invoice_bill=" + btoa(invoice_bill)+''+branchId;

  var subject = "Pest Mortem (India) Pvt. Ltd.";
  var TO      = email;

  if (valid) {

    window.location.href = "mailto: " + TO + "?body=Pls Click Link And Download This Purchase Order To Given Below %0D%0A%0D%0A" + body + "&subject=" + subject;

    window.location.reload();

  }
  
}


$(document).ready(function () {

  $('.select-js1').select2();

});

function submit() {

  var invoiceBill = $("#invoiceBill").val();

  var customerid  = $('#customerid').val();

  var fromdate    = $("#fromdate").val();

  var todate      = $('#todate').val();

  var selectLimit = $('#selectLimit').val();
  
  window.location = "pestcontrolInvoice.php?customerid=" + customerid + "&invoiceBill=" + invoiceBill+ "&fromdate=" + fromdate + "&todate=" + todate + "&selectLimit=" + selectLimit;

}

</script>