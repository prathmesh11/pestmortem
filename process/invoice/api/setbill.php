<?php

    $base = '../../../';

    include($base.'_in/connect.php');

    header('content-type: application/json; charset=utf-8');

    header("access-control-allow-origin: *");

    if(isset($_POST['data'])){

      $con = _connect();
    
      if (session_status() == PHP_SESSION_NONE) { session_start(); }

      $created_by = $_SESSION['employeeid'];

      $branchid   = $_SESSION['branchid'];

      $data = get_object_vars(json_decode($_POST["data"]));

      $category     = $data['category'];

      $branchshortname = mysqli_fetch_assoc(mysqli_query($con,"SELECT branchshortname x FROM branchmaster WHERE branchid='$branchid'"))['x'];

      $year1 = 0;
      $year2 = 0;

      if ( date('m') > 3 ) {

          $year1 = date('y');

          $year2 = date('y')+1;

      } else {

          $year1 = date('y')-1;

          $year2 = date('y');
      }

      if ( date('m') > 6 ) { //financial year

        $year111 = date('Y');

        $year11 = date('Y') + 1;

      } else {
        $year11 = date('Y') - 1;

        $year111 = date('Y');
      }

      $first = $year111.'-04-01';

      $last  = $year11.'-03-31'; 
      
      
      if ($category == 'proformaBill') {

        $invoice_bill = mysqli_fetch_assoc(mysqli_query($con,"SELECT count(invoice_bill) x FROM `billing` WHERE invoice_bill<>'' AND '$today' BETWEEN '$first' AND '$last' AND category='proformaBill' "))['x'];
        
      } else {

        $invoice_bill = mysqli_fetch_assoc(mysqli_query($con,"SELECT COUNT(invoice_bill) x FROM `billing` WHERE invoice_bill<>'' AND '$today' BETWEEN '$first' AND '$last' AND category<>'proformaBill' "))['x'];
        
      }

            
      // $n2 = str_pad($invoice_bill + 12345, 5, 0, STR_PAD_LEFT);

      // echo $n2;

      if ($category == 'proformaBill') {

        if($invoice_bill!=''){

          $invoice_bill++;
      
          $invoice_bill = 'PMI/'.$branchshortname.'/INV/P'.$invoice_bill.'/'.$year1.'-'.$year2;
      
        } else {
      
          $invoice_bill = 'PMI/'.$branchshortname.'/INV/P1/'.$year1.'-'.$year2;
      
        }
        # code...
      } else {

        if($invoice_bill!=''){

          $invoice_bill++;
      
          $invoice_bill = 'PMI/'.$branchshortname.'/INV/'.$invoice_bill.'/'.$year1.'-'.$year2;
      
        } else {
      
          $invoice_bill = 'PMI/'.$branchshortname.'/INV/1/'.$year1.'-'.$year2;
      
        }
        # code...
      }


        
                
      $customername = $data['customerid'];

      $int = (int) filter_var($customername, FILTER_SANITIZE_NUMBER_INT);
      
      if ($int == '') {

        $customerid=$data['customerid'];

      } else {

        $customerid=$int;

      }

      $create=mysqli_query($con,"INSERT INTO billing (branchid,customerid ,invoice_bill ,category ,enquiry_time ,enquiry_by) VALUES ('$branchid','$customerid' ,'$invoice_bill' ,'$category' ,'$CURRENT_MILLIS' ,'$created_by')");

      if($create){

          echo '{"status":"success","category":"'.$category.'"}';

      } else {

          echo '{"status":"falid"}';

      }
        
      _close($con);

    } else {

        echo '{"status":"falid"}';

     }
?>