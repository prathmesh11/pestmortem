<?php

    $base = '../../../';

    include($base.'_in/connect.php');

    header('content-type: application/json; charset=utf-8');

    header("access-control-allow-origin: *");

    if (isset($_POST['invoice_bill'])) {

        $con=_connect();
        
        $branchId       = $_POST["branchId"];

        $invoice_bill = $_POST["invoice_bill"];

        $select = mysqli_fetch_assoc(mysqli_query($con,"SELECT branchmaster.branchjson, billing.* FROM billing LEFT JOIN branchmaster ON branchmaster.branchid = billing.branchid WHERE billing.branchid='$branchId' AND billing.invoice_bill='$invoice_bill'"));

        if($select){

            $address         = get_object_vars(json_decode($select['branchjson']))['address'];  

            $gstnumber       = get_object_vars(json_decode($select['branchjson']))['gstnumber'];  

            $brachphone      = get_object_vars(json_decode($select['branchjson']))['phone'];  

            $brachemail      = get_object_vars(json_decode($select['branchjson']))['email'];  

            $branchStateCode = get_object_vars(json_decode($select['branchjson']))['state']; 

            $branchLutno     = get_object_vars(json_decode($select['branchjson']))['lutno']; 


            $branchStatename = mysqli_fetch_assoc(mysqli_query($con,"SELECT statename x FROM statemaster WHERE statecode='$branchStateCode'"))['x'];

            $customerid = $select['customerid'];
            
            $cifo       = mysqli_fetch_assoc(mysqli_query($con,"SELECT customerjson x FROM customermaster WHERE customerid='$customerid'"))['x'];
            
            if ($cifo != '') {
               
               $custStateCode =  get_object_vars(json_decode($cifo))['state'];

               $custStatename = mysqli_fetch_assoc(mysqli_query($con,"SELECT statename x FROM statemaster WHERE statecode='$custStateCode'"))['x'];
                
            }

            $custjson = mysqli_fetch_assoc(mysqli_query($con,"SELECT addressjson,chajson FROM customermaster WHERE customerid='$customerid'"));

            $chajson  = $custjson['chajson'];

            if ($chajson == "") {
    
              $chajson = '[]';
    
            }
    
            $addressjson = $custjson['addressjson'];
    
            if ($addressjson == "") {
    
              $addressjson = '[]';
    
            }

            $receiveAmt = mysqli_fetch_assoc(mysqli_query($con,"SELECT SUM(receiveAmt) x FROM invoicepayment WHERE branchid='$branchId' AND invoiceNo='$invoice_bill'"))['x'];
            
            if ($receiveAmt == '') {
               
                $receiveAmts = 0;

            } else {

                $receiveAmts = number_format((float)$receiveAmt, 2, '.', '');

            }

            $customer_json = json_decode($select['customer_json']);

            $invoiceDate = get_object_vars($customer_json[0])['invoiceDate'];
            $dueDate     = get_object_vars($customer_json[0])['dueDate'];
            $shipto      = get_object_vars($customer_json[0])['shipto'];
            $cha         = get_object_vars($customer_json[0])['cha'];
            $custName    = get_object_vars($customer_json[0])['custName'];
            $custAddress = get_object_vars($customer_json[0])['custAddress'];
            $cityName    = get_object_vars($customer_json[0])['cityName'];
            $personName  = get_object_vars($customer_json[0])['personName'];
            $custPhone   = get_object_vars($customer_json[0])['custPhone'];
            $custMobile  = get_object_vars($customer_json[0])['custMobile'];
            $gstIn       = get_object_vars($customer_json[0])['gstIn'];
            $email       = get_object_vars($customer_json[0])['email'];
            $account     = get_object_vars($customer_json[0])['account'];
            $bankName    = get_object_vars($customer_json[0])['bankName'];
            $branchName  = get_object_vars($customer_json[0])['branchName'];
            $ifscCode    = get_object_vars($customer_json[0])['ifscCode'];
            $accountNo   = get_object_vars($customer_json[0])['accountNo'];
            $emptybox    = get_object_vars($customer_json[0])['emptybox'];
            $notes       = get_object_vars($customer_json[0])['notes'];
            $addTerm     = get_object_vars($customer_json[0])['addTerm'];
            $accountName = get_object_vars($customer_json[0])['accountName'];
            $format      = get_object_vars($customer_json[0])['format'];




            $accountselect    = mysqli_fetch_assoc(mysqli_query($con,"SELECT  * FROM customermaster WHERE customerid='$account' AND branchid='$branchId'"));
            
            if ($accountselect) {
            
                $consineeName      = $accountselect['customername'];
            
                $accountaddress   = get_object_vars(json_decode($accountselect['customerjson']))['address'];
            
                $accountgst       = get_object_vars(json_decode($accountselect['customerjson']))['gstnumber'];
            
                $accountemail     = get_object_vars(json_decode($accountselect['customerjson']))['emailId'];
            
                $accountlandline  = get_object_vars(json_decode($accountselect['contactjson'])[0])['landline'];
            
                $accountStateCode =  get_object_vars(json_decode($accountselect['customerjson']))['state'];
            
                $accountStatename = mysqli_fetch_assoc(mysqli_query($con,"SELECT statename x FROM statemaster WHERE statecode='$custStateCode'"))['x'];
            
            }
            
            $netamt =$select['contractamt']-$receiveAmts;
        
            $json = '{"category":"'.$select['category'].'","custName":"'.$customerid.'","sales_person_name":"'.$select['sales_person_name'].'","notes":"'.$notes.'","addTerm":"'.$addTerm.'","accountName":"'.$accountName.'","format":"'.$format.'","emptybox":"'.$emptybox.'","receiveAmt":"'.$receiveAmts.'","netamt":"'.$netamt.'.00","invoice_bill":"'.$invoice_bill.'","brachaddress":"'.$address.'","brachgstnumber":"'.$gstnumber.'","brachemail":"'.$brachemail.'","branchStatename":"'.$branchStatename.'","branchStateCode":"'.$branchStateCode.'","branchlutno":"'.$branchLutno.'","brachphone":"'.$brachphone.'","service_json":'.$select['service_json'].',"subTotal":"'.$select['total_amount'].'","cgstAmt":"'.$select['cgstamt'].'","sgstAmt":"'.$select['sgstamt'].'","igstAmt":"'.$select['igstamt'].'","totalGst":"'.$select['totalGst'].'","currency":"'.$select['currency'].'","roundOff":"'.$select['roundOff'].'","netAmount":"'.$select['contractamt'].'","invoiceDate":"'.$invoiceDate.'","dueDate":"'.$dueDate.'","shipto":"'.$shipto.'","cha":"'.$cha.'","customerName":"'.$custName.'","custAddress":"'.$select['custAddress'].'","cityName":"'.$select['cityName'].'","statecode":"'.$custStateCode.'","statename":"'.$custStatename.'","personName":"'.$personName.'","custPhone":"'.$custPhone.'","custMobile":"'.$custMobile.'","gstIn":"'.$gstIn.'","email":"'.$email.'","account":"'.$account.'","accountaddress":"'.$accountaddress.'","consineeName":"'.$consineeName.'","accountgst":"'.$accountgst.'","accountemail":"'.$accountemail.'","accountlandline":"'.$accountlandline.'","accountStateCode":"'.$accountStateCode.'","accountStatename":"'.$accountStatename.'","bankName":"'.$bankName.'","branchName":"'.$branchName.'","ifscCode":"'.$ifscCode.'","accountNo":"'.$accountNo.'","chajson":'.$chajson.',"addressjson":'.$addressjson.'}'; 

            echo '{"status":"success","json":['.$json.']}';   

        } else {

            echo '{"status":"falid1"}';

        }


        _close($con);

    } else {

        echo '{"status":"falid"}';
        
    }
?>