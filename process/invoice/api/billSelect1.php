<?php

    $base = '../../../';

    include($base.'_in/connect.php');

    header('content-type: application/json; charset=utf-8');

    header("access-control-allow-origin: *");

    if (isset($_POST['invoice_bill'])) {

        $con=_connect();
        
        $branchId       = $_POST["branchId"];

        if ($branchId=='') {

            $invoice_bill   = $_POST["invoice_bill"];
            $invoice_pieces = explode("/", $invoice_bill);
            $branchId       = $invoice_pieces[3];
            
        } else {

            $branchId = $_POST["branchId"];

        }

        $invoice_bill   = $_POST["invoice_bill"];

        $select = mysqli_fetch_assoc(mysqli_query($con,"SELECT branchmaster.branchjson, billing.* FROM billing LEFT JOIN branchmaster ON branchmaster.branchid = billing.branchid WHERE billing.branchid='$branchId' AND billing.invoice_bill='$invoice_bill'"));

        if($select){

            $address    = get_object_vars(json_decode($select['branchjson']))['address'];  
            $gstnumber  = get_object_vars(json_decode($select['branchjson']))['gstnumber'];  
            $brachemail = get_object_vars(json_decode($select['branchjson']))['email'];  


            $customer_json = json_decode($select['customer_json']);

            $issuedate        = get_object_vars($customer_json[0])['issuedate'];
            $poNo             = get_object_vars($customer_json[0])['poNo'];
            $contractNo       = get_object_vars($customer_json[0])['contractNo'];
            $contractDate     = get_object_vars($customer_json[0])['contractDate'];
            $custName         = get_object_vars($customer_json[0])['custName'];
            $premisesName     = get_object_vars($customer_json[0])['premisesName'];
            $custAddress      = get_object_vars($customer_json[0])['custAddress'];
            $premisesAddress  = get_object_vars($customer_json[0])['premisesAddress'];
            $personName       = get_object_vars($customer_json[0])['personName'];
            $premisesLandLine = get_object_vars($customer_json[0])['premisesLandLine'];
            $custPhone        = get_object_vars($customer_json[0])['custPhone'];
            $custMobile       = get_object_vars($customer_json[0])['custMobile'];
            $premisesMobile   = get_object_vars($customer_json[0])['premisesMobile'];
            $account          = get_object_vars($customer_json[0])['account'];
            $gstIn            = get_object_vars($customer_json[0])['gstIn'];
            $account1         = get_object_vars($customer_json[0])['account1'];
            $email            = get_object_vars($customer_json[0])['email'];
            $account2         = get_object_vars($customer_json[0])['account2'];
            $bankName         = get_object_vars($customer_json[0])['bankName'];
            $branchName       = get_object_vars($customer_json[0])['branchName'];
            $ifscCode         = get_object_vars($customer_json[0])['ifscCode'];
            $accountNo        = get_object_vars($customer_json[0])['accountNo'];

            $json = '{"invoice_bill":"'.$invoice_bill.'","brachaddress":"'.$address.'","brachgstnumber":"'.$gstnumber.'","brachemail":"'.$brachemail.'","service_json":'.$select['service_json'].',"amount":"'.$select['total_amount'].'","cgstAmt":"'.$select['cgstamt'].'","sgstAmt":"'.$select['sgstamt'].'","igstAmt":"'.$select['igstamt'].'","netAmount":"'.$select['contractamt'].'","issuedate":"'.$issuedate.'","poNo":"'.$poNo.'","contractNo":"'.$contractNo.'","contractDate":"'.$contractDate.'","custName":"'.$custName.'","premisesName":"'.$premisesName.'","custAddress":"'.$custAddress.'","premisesAddress":"'.$premisesAddress.'","personName":"'.$personName.'","premisesLandLine":"'.$premisesLandLine.'","custPhone":"'.$custPhone.'","custMobile":"'.$custMobile.'","premisesMobile":"'.$premisesMobile.'","account":"'.$account.'","gstIn":"'.$gstIn.'","account1":"'.$account1.'","email":"'.$email.'","account2":"'.$account2.'","bankName":"'.$bankName.'","branchName":"'.$branchName.'","ifscCode":"'.$ifscCode.'","accountNo":"'.$accountNo.'"}'; 

            echo '{"status":"success","json":['.$json.']}';   

        } else {

            echo '{"status":"falid1"}';

        }


        _close($con);

    } else {

        echo '{"status":"falid"}';
        
    }

    //UPDATE `billing` SET custAddress = '', cityName = '' WHERE customerid = 
    //SELECT * FROM `billing` WHERE customerid =

    //update billing as t1 inner join ( select customeraddress,customercity from customermultiaddress where customerid = 118) as t2 set t1.custAddress = t2.customeraddress, t1.cityName = t2.customercity where t1.customerid = 118
?>