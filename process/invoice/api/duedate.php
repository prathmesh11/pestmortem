<?php

    $base = '../../../';

    include($base.'_in/connect.php');

    header('content-type: application/json; charset=utf-8');

    header("access-control-allow-origin: *");

    if(isset($_POST['invoiceDate']) && isset($_POST['customerId'])){

      $con = _connect();
    
      $invoiceDate = $_POST['invoiceDate'];

      $customerId = $_POST['customerId'];

      $customerjson=mysqli_fetch_assoc(mysqli_query($con,"SELECT customerjson x FROM customermaster WHERE customerid='$customerId'"))['x'];

      if ($customerjson) {

        $creditdays       = get_object_vars(json_decode($customerjson))['creditdays'];

        $onedayTimestamp  = 86400000;

        $invoice          = (strtotime($invoiceDate) * 1000);

        $dueDateTimestamp = ($onedayTimestamp*$creditdays)+$invoice;

        $dueDate          =  date('Y-m-d',  $dueDateTimestamp/1000);
          
        echo '{"status":"success","dueDate":"'.$dueDate.'"}';

      } else {

        $dueDate = $_POST['invoiceDate'];

        echo '{"status":"success","dueDate":"'.$dueDate.'"}';


      }
        _close($con);

    } else {

        echo '{"status":"falid"}';

     }
?>