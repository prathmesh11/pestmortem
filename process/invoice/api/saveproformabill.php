<?php

    $base = '../../../';

    include($base.'_in/connect.php');

    header('content-type: application/json; charset=utf-8');

    header("access-control-allow-origin: *");

    if (isset($_POST['data']) && isset($_POST['branchId']) && isset($_POST['invoice_bill'])) {

        $con=_connect();

        if (session_status() == PHP_SESSION_NONE) { session_start(); }

        $created_by = $_SESSION['employeeid'];

        $branchId       = $_POST["branchId"];

        $invoice_bill   = $_POST["invoice_bill"];

        $serviceDetails = $_POST["serviceDetails"];

        $customerId     = $_POST["customerId"];

        $category       = $_POST["category"];

        $copy           = $_POST["copy"];

        $edit           = $_POST["edit"];


        $data = get_object_vars(json_decode($_POST["data"]));

        //print_r($data);

        $invoiceDate       = $data['invoiceDate'];

        $dueDate           = $data['dueDate'];

        $shipto            = $data['shipto'];

        $cha               = $data['cha'];

        $customerids       = $data['custName'];

        $custName          = mysqli_fetch_assoc(mysqli_query($con,"SELECT customername x FROM customermaster WHERE customerid='$customerids' AND branchid='$branchId'"))['x'];

        $custAddress       = $data['custAddress'];

        $cityName          = $data['cityName'];

        $personName        = $data['personName'];

        $custPhone         = $data['custPhone'];

        $custMobile        = $data['custMobile'];

        $gstIn             = $data['gstIn'];

        $email             = $data['email'];

        $account           = $data['account'];

        $sales_person_name = $data['sales_person_name'];

        $currency          = $data['currency'];

        $subTotal          = $data['subTotal'];

        $cgstAmt           = $data['cgstAmt'];

        $sgstAmt           = $data['sgstAmt'];

        $igstAmt           = $data['igstAmt'];

        $totalGst          = $cgstAmt+$sgstAmt+$igstAmt;

        $roundOff          = $data['roundOff'];

        $netAmount         = $data['netAmount'];

        $bankName          = $data['bankName'];

        $branchName        = $data['branchName'];

        $ifscCode          = $data['ifscCode'];

        $accountNo         = $data['accountNo'];

        $emptybox          = $data['emptybox'];

        $notes             = $data['notes'];

        $addTerm           = $data['addTerm'];

        $accountName       = $data['accountName'];

        $format            = $data['format'];

        $json = ',{"format":"'.$format.'","accountName":"'.$accountName.'","notes":"'.$notes.'","addTerm":"'.$addTerm.'","emptybox":"'.$emptybox.'","invoiceDate":"'.$invoiceDate.'","dueDate":"'.$dueDate.'","shipto":"'.$shipto.'","cha":"'.$cha.'","custName":"'.$custName.'","custAddress":"'.$custAddress.'","cityName":"'.$cityName.'","personName":"'.$personName.'","custPhone":"'.$custPhone.'","custMobile":"'.$custMobile.'","gstIn":"'.$gstIn.'","email":"'.$email.'","account":"'.$account.'","bankName":"'.$bankName.'","branchName":"'.$branchName.'","ifscCode":"'.$ifscCode.'","accountNo":"'.$accountNo.'"}';

        $json          = substr($json,1);
        
        $customer_json = '['.$json.']';

        if ($copy == "true") {


            $invoice_bills = mysqli_fetch_assoc(mysqli_query($con,"SELECT invoice_bill x FROM billing WHERE invoice_bill='$invoice_bill'"))['x'];

            if ($invoice_bills) {

                $new_invoice_bill = newProformaBillNo($con,$branchid);

                $doneEntry = mysqli_query($con,"INSERT INTO billing (branchid,category,customerid,sales_person_name,invoiceDate,dueDate,customer_json,service_json,total_amount,cgstamt,sgstamt,igstamt,totalGst,currency,roundOff,contractamt,enquiry_by,enquiry_time,created_by,created_time,invoice_bill,custAddress,cityName) VALUES('$branchId','$category','$customerids','$sales_person_name','$invoiceDate','$dueDate','$customer_json','$serviceDetails','$subTotal','$cgstAmt','$sgstAmt','$igstAmt','$totalGst','$currency','$roundOff','$netAmount','$created_by','$CURRENT_MILLIS','$created_by','$CURRENT_MILLIS','$new_invoice_bill','$custAddress','$cityName')");

            } else {
                
                $doneEntry = mysqli_query($con,"INSERT INTO billing (branchid,category,customerid,sales_person_name,invoiceDate,dueDate,customer_json,service_json,total_amount,cgstamt,sgstamt,igstamt,totalGst,currency,roundOff,contractamt,enquiry_by,enquiry_time,created_by,created_time,invoice_bill,custAddress,cityName) VALUES('$branchId','$category','$customerids','$sales_person_name','$invoiceDate','$dueDate','$customer_json','$serviceDetails','$subTotal','$cgstAmt','$sgstAmt','$igstAmt','$totalGst','$currency','$roundOff','$netAmount','$created_by','$CURRENT_MILLIS','$created_by','$CURRENT_MILLIS','$invoice_bill','$custAddress','$cityName')");

            }
            
        } else if ($edit == "true") {
              
            $doneEntry = mysqli_query($con,"UPDATE billing SET customerid = '$customerids',sales_person_name='$sales_person_name',invoiceDate='$invoiceDate',dueDate='$dueDate',customer_json='$customer_json',service_json='$serviceDetails',total_amount='$subTotal',cgstamt='$cgstAmt',sgstamt='$sgstAmt',igstamt='$igstAmt',totalGst='$totalGst',currency='$currency',roundOff='$roundOff',contractamt='$netAmount',created_by='$created_by',created_time='$CURRENT_MILLIS', custAddress ='$custAddress', cityName='$cityName' WHERE branchid='$branchId' AND invoice_bill='$invoice_bill'");

        } else {


            $invoice_bills = mysqli_fetch_assoc(mysqli_query($con,"SELECT invoice_bill x FROM billing WHERE invoice_bill='$invoice_bill'"))['x'];

            if ($invoice_bills) {

                $new_invoice_bill = newProformaBillNo($con,$branchid);

                $doneEntry = mysqli_query($con,"INSERT INTO billing (branchid,category,customerid,sales_person_name,invoiceDate,dueDate,customer_json,service_json,total_amount,cgstamt,sgstamt,igstamt,totalGst,currency,roundOff,contractamt,enquiry_by,enquiry_time,created_by,created_time,invoice_bill,custAddress,cityName) VALUES('$branchId','$category','$customerids','$sales_person_name','$invoiceDate','$dueDate','$customer_json','$serviceDetails','$subTotal','$cgstAmt','$sgstAmt','$igstAmt','$totalGst','$currency','$roundOff','$netAmount','$created_by','$CURRENT_MILLIS','$created_by','$CURRENT_MILLIS','$new_invoice_bill','$custAddress','$cityName')");

            } else {
                
                $doneEntry = mysqli_query($con,"INSERT INTO billing (branchid,category,customerid,sales_person_name,invoiceDate,dueDate,customer_json,service_json,total_amount,cgstamt,sgstamt,igstamt,totalGst,currency,roundOff,contractamt,enquiry_by,enquiry_time,created_by,created_time,invoice_bill,custAddress,cityName) VALUES('$branchId','$category','$customerids','$sales_person_name','$invoiceDate','$dueDate','$customer_json','$serviceDetails','$subTotal','$cgstAmt','$sgstAmt','$igstAmt','$totalGst','$currency','$roundOff','$netAmount','$created_by','$CURRENT_MILLIS','$created_by','$CURRENT_MILLIS','$invoice_bill','$custAddress','$cityName')");

            }
            

        }

        if ($doneEntry) {

            echo '{"status":"success"}';

        } else {

            echo '{"status":"falid1"}';
            
        }

        _close($con);

    } else {

        echo '{"status":"falid"}';
        
    }



?>