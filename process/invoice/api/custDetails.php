<?php

    $base = '../../../';

    include($base.'_in/connect.php');

    header('content-type: application/json; charset=utf-8');

    header("access-control-allow-origin: *");

    if(isset($_POST['custName']) && isset($_POST['branchid'])){

      $con = _connect();
    
      if (session_status() == PHP_SESSION_NONE) { session_start(); }

      $customerid   = $_POST['custName'];

      $branchid     = $_POST['branchid'];

      $customerjson=mysqli_fetch_assoc(mysqli_query($con,"SELECT * FROM customermaster WHERE customerid='$customerid' AND branchid='$branchid'"));

      if($customerjson){

        $customeradd = get_object_vars(json_decode($customerjson['customerjson']))['address'];

        $gst         = get_object_vars(json_decode($customerjson['customerjson']))['gstnumber'];

        $email       = get_object_vars(json_decode($customerjson['customerjson']))['emailId'];

        $mobile      = get_object_vars(json_decode($customerjson['contactjson'])[0])['mobile'];

        $name        = get_object_vars(json_decode($customerjson['contactjson'])[0])['name'];

        $landline    = get_object_vars(json_decode($customerjson['contactjson'])[0])['landline'];

        $gstOnBill   = get_object_vars(json_decode($customerjson['customerjson']))['gstOnBill'];

        $currency    = get_object_vars(json_decode($customerjson['customerjson']))['westageallowed'];
        
        $chajson     = $customerjson['chajson'];

        if ($chajson == "") {

          $chajson = '[]';

        }

        $addressjson = $customerjson['addressjson'];

        if ($addressjson == "") {

          $addressjson = '[]';

        }

        echo '{"status":"success","gstOnBill":"'.$gstOnBill.'","customeradd":"'.$customeradd.'","gst":"'.$gst.'","email":"'.$email.'","mobile":"'.$mobile.'","name":"'.$name.'","landline":"'.$landline.'","currency":"'.$currency.'","chajson":'.$chajson.',"addressjson":'.$addressjson.'}';

      } else {

        echo '{"status":"falid1"}';

      }
        
        _close($con);

    } else {

        echo '{"status":"falid"}';

    }

?>