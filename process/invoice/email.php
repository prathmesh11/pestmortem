<?php
 $base    = '../../';
 include($base.'_in/connect.php');
 $con = _connect();
 $invoice_bill = $_GET['invoice_bill'];
  if(preg_match_all('/\d+/', $invoice_bill, $numbers))
  $branchid = end($numbers[0]);
  $invoice_bills = substr($invoice_bill, 0, -1);;
  $signature = mysqli_fetch_assoc(mysqli_query($con,"SELECT signature x FROM branchmaster WHERE branchid='$branchid'"))['x'];

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Pest Mortem (India) Pvt. Ltd.</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="/js/pdfmake.min.js"></script>
<script src="/js/vfs_fonts.js"></script>
</head>
<body>
<div class="container-fluid">
<img style="display:none;" id="imageid" src="<?php echo $base;?>img/imageonline-co-blackandwhiteimage.png">
<img style="display:none;" id="taxinvoice" src="../../img/tax-invoice1.png">
<img style="display:none;" id="signature" src="<?php echo '../../'.$signature;?>">



<center>
<h1>Thank You For Download..!!</h1>
<h3>(Pest Mortem (India) Pvt. Ltd.)</h3>
</center>

<script>


function withDecimal(s) {
  n=Math.round(s * 100) / 100;
    var nums = n.toString().split('.')
    var whole = convertNumberToWords(nums[0])
    if (nums.length == 2) {
      var res1 = nums[1].slice(0, 1);
      var res2 = nums[1].slice(1, 2);
        var fraction = convertNumberToWords(res1)+''+convertNumberToWords(res2)
        return whole+'point '+fraction;
    } else {
        return whole;
    }
}

function getBase64Image(img) {
  var canvas = document.createElement("canvas");
  canvas.width = img.width;
  canvas.height = img.height;
  var ctx = canvas.getContext("2d");
  ctx.drawImage(img, 0, 0);
  var dataURL = canvas.toDataURL("image/png");
  return dataURL.replace('/^data:image\/(png|jpg);base64,/', "");
}

function printer() {


    var invoice_bill = atob("<?php echo $invoice_bills;?>");

    var branchId = "<?php echo $branchid;?>";



    $.ajax({

    type: "POST",

    data: "branchId=" + branchId + "&invoice_bill=" + invoice_bill,

    url: 'api/billSelect.php',

    cache: false,

    success: function (res) {

      if (res.status == 'success') {


        let content = [];

        let json = res.json[0];

        let base64 = getBase64Image(document.getElementById("imageid"));

        let signature = getBase64Image(document.getElementById("signature"));

        let taxinvoice = getBase64Image(document.getElementById("taxinvoice"));

      
               window.pdfMake.vfs["Times-New-Roman-Regular.ttf"] = "BASE 64 HERE";

        pdfMake.fonts = {
            // Default font should still be available
            Roboto: {
                normal: 'Roboto-Regular.ttf',
                bold: 'Roboto-Medium.ttf',
                italics: 'Roboto-Italic.ttf',
                bolditalics: 'Roboto-Italic.ttf'
            },
            // Make sure you define all 4 components - normal, bold, italics, bolditalics - (even if they all point to the same font file)
            TimesNewRoman: {
                normal: 'Times-New-Roman-Regular.ttf',
                bold: 'Times-New-Roman-Bold.ttf',
                italics: 'Times-New-Roman-Italics.ttf',
                bolditalics: 'Times-New-Roman-Italics.ttf'
            }
        };


        content.push({
            layout: 'noBorders',
            table: {
                widths: ['*'],
                body: [
                    [{text:'CIN NO. : U24211MH1980PTC023338\n\n',bold:1,fontSize: 10,alignment:'center',fonts: 'TimesNewRoman'}],
                ]
            }

        });


        content.push({
            layout: 'noBorders',
            table: {
                widths: ['*', 325],
                body: [
                    [{image:base64,
                            width: 150,
                            height:80,
                            margin:[0,20]}, 
                            //{text:[{text:'PEST MORTEM (INDIA) PRIVATE LIMITED \n',fontSize: 12},{text: json.brachaddress+'\nState : ' + json.branchStatename + ', StateCode : ' + json.branchStateCode +'\n'+json.brachphone+'\n'+json.brachemail+'\nwww.pestmortem.com\nGST No : ' + json.brachgstnumber + ' \nPAN No : AACCP2255R\n\n',fontSize: 10,}],bold:1,alignment:'right',fonts: 'TimesNewRoman'}],
                            {text:[{text:'PEST MORTEM (INDIA) PRIVATE LIMITED \n',fontSize: 13},{text: json.brachaddress+'\n'+json.brachphone+'\n'+json.brachemail+'\nwww.pestmortem.com\nGST No : ' + json.brachgstnumber +'\nState : ' + json.branchStatename + ', StateCode : ' + json.branchStateCode + ' \nPAN No : AACCP2255R\n\n',fontSize: 10,}],bold:1,alignment:'right',fonts: 'TimesNewRoman'}],
                            
                ]
            }

        });


        if (json.shipto!='' && json.consineeName != '' && json.emptybox != '' && json.accountName != '') {

          if (json.totalGst =="0.00") {

            content.push({

              table: {
                widths: [200, 'auto'],
                 //heights: [0, 10],
                body:[
                  [{image:taxinvoice,width: 204,height:140,border:[true,true,true,true]},{text: [{text:'Bill To : \n',alignment:'left',fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: json.customerName+'\nA/C. : ' + json.accountName+'\n',border:[false,false,false,false],bold:1,alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text:json.custAddress,bold:1,alignment:'left',border:[false,false,false,false],fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nPhone  : ' + json.custPhone,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nEmail    : ' + json.email,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nGSTIN  : ' + json.gstIn,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nState    : ' + json.statename + '   State Code : ' + json.statecode,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\n'+json.emptybox,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nShip To : \n',alignment:'left',fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: json.consineeName,bold:1,alignment:'left',fontSize: 10,fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: '\nConsinee : ' + json.accountaddress,bold:1,alignment:'left',fontSize: 10,fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: '\nPhone  : ' + json.accountlandline,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nEmail    : ' + json.accountemail + '\nGSTIN  : ' + json.accountgst + '\nState    : ' + json.accountStatename + '   State Code : ' + json.accountStateCode + '\n\nPlace Of Supply : ' + json.shipto,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'}],rowSpan:5}],
                  [{text:'Invoice No      :   ' +  json.invoice_bill,fontSize: 10,bold: 1,absolutePosition: {x: 42, y: 303},color: '#000000',fonts: 'TimesNewRoman'},{}],
                  [{text:'Invoice Date   :   '   +  convertDate(json.invoiceDate),margin: [2,-7,0,0],alignment: 'left',fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],
                  [{text:'Due Date         :   '   +  convertDate(json.dueDate),margin: [2,5,0,0],alignment: 'left',fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],
                  [{text:'Lut No              :   ' + json.branchlutno,alignment: 'left',margin: [2,5,0,0],fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],


                ]

              },			layout: 'noBorders'


            })
            
          } else {

            content.push({

              table: {
                widths: [200, 'auto'],
                //  heights: [0, 10],
                body:[
                  [{image:taxinvoice,width: 204,height:140,border:[true,true,true,true]},{text: [{text:'Bill To : \n',alignment:'left',fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: json.customerName+'\nA/C. : ' + json.accountName+'\n',border:[false,false,false,false],bold:1,alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text:json.custAddress,bold:1,alignment:'left',border:[false,false,false,false],fontSize: 9,fonts: 'TimesNewRoman'},{text: '\nPhone  : ' + json.custPhone,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nEmail    : ' + json.email,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nGSTIN  : ' + json.gstIn,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nState    : ' + json.statename + '   State Code : ' + json.statecode,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\n'+json.emptybox,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nShip To : \n',alignment:'left',fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: json.consineeName,bold:1,alignment:'left',fontSize: 10,fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: '\nConsinee : ' + json.accountaddress,bold:1,alignment:'left',fontSize: 10,fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: '\nPhone  : ' + json.accountlandline,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nEmail    : ' + json.accountemail + '\nGSTIN  : ' + json.accountgst + '\nState    : ' + json.accountStatename + '   State Code : ' + json.accountStateCode + '\n\nPlace Of Supply : ' + json.shipto,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'}],rowSpan:4}],
                  [{text:'Invoice No      :   ' +  json.invoice_bill,fontSize: 10,bold: 1,absolutePosition: {x: 42, y: 303},color: '#000000',fonts: 'TimesNewRoman'},{}],
                  [{text:'Invoice Date   :   ' + convertDate(json.invoiceDate),margin: [2,-7,0,0],alignment: 'left',fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],
                  [{text:'Due Date         :   ' + convertDate(json.dueDate),margin: [2,5,0,0],alignment: 'left',fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],

                ]

              },			layout: 'noBorders'


            })

          }

        } else if (json.shipto!='' && json.consineeName != '' && json.emptybox != '' && json.accountName == '') {

          if (json.totalGst =="0.00") {

            content.push({

              table: {
                widths: [200, 'auto'],
                 //heights: [0, 10],
                body:[
                  [{image:taxinvoice,width: 204,height:140,border:[true,true,true,true]},{text: [{text:'Bill To : \n',alignment:'left',fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: json.customerName+'\n',border:[false,false,false,false],bold:1,alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text:json.custAddress,bold:1,alignment:'left',border:[false,false,false,false],fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nPhone  : ' + json.custPhone,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nEmail    : ' + json.email,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nGSTIN  : ' + json.gstIn,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nState    : ' + json.statename + '   State Code : ' + json.statecode,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\n'+json.emptybox,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nShip To : \n',alignment:'left',fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: json.consineeName,bold:1,alignment:'left',fontSize: 10,fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: '\nConsinee : ' + json.accountaddress,bold:1,alignment:'left',fontSize: 10,fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: '\nPhone  : ' + json.accountlandline,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nEmail    : ' + json.accountemail + '\nGSTIN  : ' + json.accountgst + '\nState    : ' + json.accountStatename + '   State Code : ' + json.accountStateCode + '\n\nPlace Of Supply : ' + json.shipto,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'}],rowSpan:5}],
                  [{text:'Invoice No      :   ' +  json.invoice_bill,fontSize: 10,bold: 1,absolutePosition: {x: 42, y: 303},color: '#000000',fonts: 'TimesNewRoman'},{}],
                  [{text:'Invoice Date   :   '   +  convertDate(json.invoiceDate),margin: [2,-7,0,0],alignment: 'left',fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],
                  [{text:'Due Date         :   ' + convertDate(json.dueDate),margin: [2,5,0,0],alignment: 'left',fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],
                  [{text:'Lut No              :   ' + json.branchlutno,alignment: 'left',margin: [2,5,0,0],fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],


                ]

              },			layout: 'noBorders'


            })
            
          } else {

            content.push({

              table: {
                widths: [200, 'auto'],
                //  heights: [0, 10],
                body:[
                  [{image:taxinvoice,width: 204,height:140,border:[true,true,true,true]},{text: [{text:'Bill To : \n',alignment:'left',fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: json.customerName+'\n',border:[false,false,false,false],bold:1,alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text:json.custAddress,bold:1,alignment:'left',border:[false,false,false,false],fontSize: 9,fonts: 'TimesNewRoman'},{text: '\nPhone  : ' + json.custPhone,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nEmail    : ' + json.email,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nGSTIN  : ' + json.gstIn,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nState    : ' + json.statename + '   State Code : ' + json.statecode,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\n'+json.emptybox,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nShip To : \n',alignment:'left',fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: json.consineeName,bold:1,alignment:'left',fontSize: 10,fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: '\nConsinee : ' + json.accountaddress,bold:1,alignment:'left',fontSize: 10,fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: '\nPhone  : ' + json.accountlandline,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nEmail    : ' + json.accountemail + '\nGSTIN  : ' + json.accountgst + '\nState    : ' + json.accountStatename + '   State Code : ' + json.accountStateCode + '\n\nPlace Of Supply : ' + json.shipto,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'}],rowSpan:4}],
                  [{text:'Invoice No      :   ' +  json.invoice_bill,fontSize: 10,bold: 1,absolutePosition: {x: 42, y: 303},color: '#000000',fonts: 'TimesNewRoman'},{}],
                  [{text:'Invoice Date   :   '   +  convertDate(json.invoiceDate),margin: [2,-7,0,0],alignment: 'left',fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],
                  [{text:'Due Date         :   ' + convertDate(json.dueDate),margin: [2,5,0,0],alignment: 'left',fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],

                ]

              },			layout: 'noBorders'


            })

          }

        } else if (json.shipto!='' && json.consineeName != '' && json.emptybox == '' && json.accountName != '') {

          if (json.totalGst =="0.00") {

            content.push({

              table: {
                widths: [200, 'auto'],
                // heights: [85, 0],
                body:[
                  [{image:taxinvoice,width: 204,height:140,border:[true,true,true,true]},{text: [{text:'Bill To : \n',alignment:'left',fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: json.customerName+'\nA/C. : ' + json.accountName+'\n',border:[false,false,false,false],bold:1,alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text:json.custAddress,bold:1,alignment:'left',border:[false,false,false,false],fontSize: 9,fonts: 'TimesNewRoman'},{text: '\nPhone  : ' + json.custPhone,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nEmail    : ' + json.email,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nGSTIN  : ' + json.gstIn,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nState    : ' + json.statename + '   State Code : ' + json.statecode,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nShip To : \n',alignment:'left',fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: json.consineeName,bold:1,alignment:'left',fontSize: 10,fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: '\nConsinee : ' + json.accountaddress,bold:1,alignment:'left',fontSize: 10,fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: '\nPhone  : ' + json.accountlandline,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nEmail    : ' + json.accountemail + '\nGSTIN  : ' + json.accountgst + '\nState    : ' + json.accountStatename + '   State Code : ' + json.accountStateCode + '\n\nPlace Of Supply : ' + json.shipto,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'}],rowSpan:5}],
                  [{text:'Invoice No      :   ' +  json.invoice_bill,fontSize: 10,bold: 1,absolutePosition: {x: 42, y: 303},color: '#000000',fonts: 'TimesNewRoman'},{}],
                  [{text:'Invoice Date   :   '   +  convertDate(json.invoiceDate),margin: [2,-7,0,0],alignment: 'left',fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],
                  [{text:'Due Date         :   ' + convertDate(json.dueDate),margin: [2,5,0,0],alignment: 'left',fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],
                  [{text:'Lut No              :   ' + json.branchlutno,alignment: 'left',margin: [2,5,0,0],fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],


                ]

              },			layout: 'noBorders'


            })
            
          } else {

            content.push({

              table: {
                widths: [200, 'auto'],
                // heights: [85, 0],
                body:[
                  [{image:taxinvoice,width: 204,height:140,border:[true,true,true,true]},{text: [{text:'Bill To : \n',alignment:'left',fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: json.customerName+'\nA/C. : ' + json.accountName+'\n',border:[false,false,false,false],bold:1,alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text:json.custAddress,bold:1,alignment:'left',border:[false,false,false,false],fontSize: 9,fonts: 'TimesNewRoman'},{text: '\nPhone  : ' + json.custPhone,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nEmail    : ' + json.email,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nGSTIN  : ' + json.gstIn,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nState    : ' + json.statename + '   State Code : ' + json.statecode,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nShip To : \n',alignment:'left',fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: json.consineeName,bold:1,alignment:'left',fontSize: 10,fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: '\nConsinee : ' + json.accountaddress,bold:1,alignment:'left',fontSize: 10,fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: '\nPhone  : ' + json.accountlandline,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nEmail    : ' + json.accountemail + '\nGSTIN  : ' + json.accountgst + '\nState    : ' + json.accountStatename + '   State Code : ' + json.accountStateCode + '\n\nPlace Of Supply : ' + json.shipto,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'}],rowSpan:4}],
                  [{text:'Invoice No      :   ' +  json.invoice_bill,fontSize: 10,bold: 1,absolutePosition: {x: 42, y: 303},color: '#000000',fonts: 'TimesNewRoman'},{}],
                  [{text:'Invoice Date   :   '   +  convertDate(json.invoiceDate),margin: [2,-7,0,0],alignment: 'left',fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],
                  [{text:'Due Date         :   ' + convertDate(json.dueDate),margin: [2,5,0,0],alignment: 'left',fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],
                ]

              },			layout: 'noBorders'


            })

          }

        } else if (json.shipto!='' && json.consineeName == '' && json.emptybox != '' && json.accountName != '') {

          if (json.totalGst =="0.00") {

            content.push({

              table: {
                widths: [200, 'auto'],
                // heights: [85, 0],
                body:[
                  [{image:taxinvoice,width: 204,height:140,border:[true,true,true,true]},{text: [{text:'Bill To : \n',alignment:'left',fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: json.customerName+'\nA/C. : ' + json.accountName+'\n',border:[false,false,false,false],bold:1,alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text:json.custAddress,bold:1,alignment:'left',border:[false,false,false,false],fontSize: 9,fonts: 'TimesNewRoman'},{text: '\nPhone  : ' + json.custPhone,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nEmail    : ' + json.email,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nGSTIN  : ' + json.gstIn,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nState    : ' + json.statename + '   State Code : ' + json.statecode,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\n'+json.emptybox,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text:'\n\nPlace Of Supply : ' + json.shipto,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'}],rowSpan:5}],
                  [{text:'Invoice No      :   ' +  json.invoice_bill,fontSize: 10,bold: 1,absolutePosition: {x: 42, y: 303},color: '#000000',fonts: 'TimesNewRoman'},{}],
                  [{text:'Invoice Date   :   '   +  convertDate(json.invoiceDate),margin: [2,-7,0,0],alignment: 'left',fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],
                  [{text:'Due Date         :   ' + convertDate(json.dueDate),margin: [2,5,0,0],alignment: 'left',fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],
                  [{text:'Lut No              :   ' + json.branchlutno,alignment: 'left',margin: [2,5,0,0],fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],


                ]

              },			layout: 'noBorders'


            })
            
          } else {

            content.push({

              table: {
                widths: [200, 'auto'],
                // heights: [85, 0],
                body:[
                  [{image:taxinvoice,width: 204,height:140,border:[true,true,true,true]},{text: [{text:'Bill To : \n',alignment:'left',fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: json.customerName+'\nA/C. : ' + json.accountName+'\n',border:[false,false,false,false],bold:1,alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text:json.custAddress,bold:1,alignment:'left',border:[false,false,false,false],fontSize: 9,fonts: 'TimesNewRoman'},{text: '\nPhone  : ' + json.custPhone,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nEmail    : ' + json.email,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nGSTIN  : ' + json.gstIn,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nState    : ' + json.statename + '   State Code : ' + json.statecode,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\n'+json.emptybox,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text:'\n\nPlace Of Supply : ' + json.shipto,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'}],rowSpan:4}],
                  [{text:'Invoice No      :   ' +  json.invoice_bill,fontSize: 10,bold: 1,absolutePosition: {x: 42, y: 303},color: '#000000',fonts: 'TimesNewRoman'},{}],
                  [{text:'Invoice Date   :   '   +  convertDate(json.invoiceDate),margin: [2,-7,0,0],alignment: 'left',fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],
                  [{text:'Due Date         :   ' + convertDate(json.dueDate),margin: [2,5,0,0],alignment: 'left',fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],

                ]

              },layout: 'noBorders'


            })

          }

        } else if (json.shipto=='' && json.consineeName != '' && json.emptybox != '' && json.accountName != '') {

          if (json.totalGst =="0.00") {

            content.push({

              table: {
                widths: [200, 'auto'],
                 //heights: [0, 10],
                body:[
                  [{image:taxinvoice,width: 204,height:140,border:[true,true,true,true]},{text: [{text:'Bill To : \n',alignment:'left',fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: json.customerName+'\nA/C. : ' + json.accountName+'\n',border:[false,false,false,false],bold:1,alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text:json.custAddress,bold:1,alignment:'left',border:[false,false,false,false],fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nPhone  : ' + json.custPhone,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nEmail    : ' + json.email,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nGSTIN  : ' + json.gstIn,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nState    : ' + json.statename + '   State Code : ' + json.statecode,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\n'+json.emptybox,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nShip To : \n',alignment:'left',fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: json.consineeName,bold:1,alignment:'left',fontSize: 10,fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: '\nConsinee : ' + json.accountaddress,bold:1,alignment:'left',fontSize: 10,fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: '\nPhone  : ' + json.accountlandline,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nEmail    : ' + json.accountemail + '\nGSTIN  : ' + json.accountgst + '\nState    : ' + json.accountStatename + '   State Code : ' + json.accountStateCode,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'}],rowSpan:5}],
                  [{text:'Invoice No      :   ' +  json.invoice_bill,fontSize: 10,bold: 1,absolutePosition: {x: 42, y: 303},color: '#000000',fonts: 'TimesNewRoman'},{}],
                  [{text:'Invoice Date   :   '   +  convertDate(json.invoiceDate),margin: [2,-7,0,0],alignment: 'left',fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],
                  [{text:'Due Date         :   ' + convertDate(json.dueDate),margin: [2,5,0,0],alignment: 'left',fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],
                  [{text:'Lut No              :   ' + json.branchlutno,alignment: 'left',margin: [2,5,0,0],fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],


                ]

              },			layout: 'noBorders'


            })
            
          } else {

            content.push({

              table: {
                widths: [200, 'auto'],
                //  heights: [0, 10],
                body:[
                  [{image:taxinvoice,width: 204,height:140,border:[true,true,true,true]},{text: [{text:'Bill To : \n',alignment:'left',fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: json.customerName+'\nA/C. : ' + json.accountName+'\n',border:[false,false,false,false],bold:1,alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text:json.custAddress,bold:1,alignment:'left',border:[false,false,false,false],fontSize: 9,fonts: 'TimesNewRoman'},{text: '\nPhone  : ' + json.custPhone,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nEmail    : ' + json.email,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nGSTIN  : ' + json.gstIn,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nState    : ' + json.statename + '   State Code : ' + json.statecode,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\n'+json.emptybox,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nShip To : \n',alignment:'left',fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: json.consineeName,bold:1,alignment:'left',fontSize: 10,fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: '\nConsinee : ' + json.accountaddress,bold:1,alignment:'left',fontSize: 10,fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: '\nPhone  : ' + json.accountlandline,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nEmail    : ' + json.accountemail + '\nGSTIN  : ' + json.accountgst + '\nState    : ' + json.accountStatename + '   State Code : ' + json.accountStateCode,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'}],rowSpan:4}],
                  [{text:'Invoice No      :   ' +  json.invoice_bill,fontSize: 10,bold: 1,absolutePosition: {x: 42, y: 303},color: '#000000',fonts: 'TimesNewRoman'},{}],
                  [{text:'Invoice Date   :   '   +  convertDate(json.invoiceDate),margin: [2,-7,0,0],alignment: 'left',fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],
                  [{text:'Due Date         :   ' + convertDate(json.dueDate),margin: [2,5,0,0],alignment: 'left',fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],

                ]

              },			layout: 'noBorders'


            })

          }

        } else if (json.shipto!='' && json.consineeName != '' && json.emptybox == '' && json.accountName == '') {

          if (json.totalGst =="0.00") {

            content.push({

              table: {
                widths: [200, 'auto'],
                // heights: [85, 0],
                body:[
                  [{image:taxinvoice,width: 204,height:140,border:[true,true,true,true]},{text: [{text:'Bill To : \n',alignment:'left',fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: json.customerName+'\n',border:[false,false,false,false],bold:1,alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text:json.custAddress,bold:1,alignment:'left',border:[false,false,false,false],fontSize: 9,fonts: 'TimesNewRoman'},{text: '\nPhone  : ' + json.custPhone,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nEmail    : ' + json.email,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nGSTIN  : ' + json.gstIn,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nState    : ' + json.statename + '   State Code : ' + json.statecode,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nShip To : \n',alignment:'left',fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: json.consineeName,bold:1,alignment:'left',fontSize: 10,fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: '\nShip To : \n',alignment:'left',fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: json.consineeName,bold:1,alignment:'left',fontSize: 10,fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: '\nConsinee : ' + json.accountaddress,bold:1,alignment:'left',fontSize: 10,fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: '\nPhone  : ' + json.accountlandline,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nEmail    : ' + json.accountemail + '\nGSTIN  : ' + json.accountgst + '\nState    : ' + json.accountStatename + '   State Code : ' + json.accountStateCode + '\n\nPlace Of Supply : ' + json.shipto,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'}],rowSpan:5}],
                  [{text:'Invoice No      :   ' +  json.invoice_bill,fontSize: 10,bold: 1,absolutePosition: {x: 42, y: 303},color: '#000000',fonts: 'TimesNewRoman'},{}],
                  [{text:'Invoice Date   :   '   +  convertDate(json.invoiceDate),margin: [2,-7,0,0],alignment: 'left',fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],
                  [{text:'Due Date         :   ' + convertDate(json.dueDate),margin: [2,5,0,0],alignment: 'left',fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],
                  [{text:'Lut No              :   ' + json.branchlutno,alignment: 'left',margin: [2,5,0,0],fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],


                ]

              },			layout: 'noBorders'


            })
            
          } else {

            content.push({

              table: {
                widths: [200, 'auto'],
                // heights: [85, 0],
                body:[
                  [{image:taxinvoice,width: 204,height:140,border:[true,true,true,true]},{text: [{text:'Bill To : \n',alignment:'left',fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: json.customerName+'\n',border:[false,false,false,false],bold:1,alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text:json.custAddress,bold:1,alignment:'left',border:[false,false,false,false],fontSize: 9,fonts: 'TimesNewRoman'},{text: '\nPhone  : ' + json.custPhone,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nEmail    : ' + json.email,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nGSTIN  : ' + json.gstIn,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nState    : ' + json.statename + '   State Code : ' + json.statecode,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nShip To : \n',alignment:'left',fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: json.consineeName,bold:1,alignment:'left',fontSize: 10,fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: '\nConsinee : ' + json.accountaddress,bold:1,alignment:'left',fontSize: 10,fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: '\nPhone  : ' + json.accountlandline,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nEmail    : ' + json.accountemail + '\nGSTIN  : ' + json.accountgst + '\nState    : ' + json.accountStatename + '   State Code : ' + json.accountStateCode + '\n\nPlace Of Supply : ' + json.shipto,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'}],rowSpan:4}],
                  [{text:'Invoice No      :   ' +  json.invoice_bill,fontSize: 10,bold: 1,absolutePosition: {x: 42, y: 303},color: '#000000',fonts: 'TimesNewRoman'},{}],
                  [{text:'Invoice Date   :   '   +  convertDate(json.invoiceDate),margin: [2,-7,0,0],alignment: 'left',fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],
                  [{text:'Due Date         :   ' + convertDate(json.dueDate),margin: [2,5,0,0],alignment: 'left',fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],
                ]

              },			layout: 'noBorders'


            })

          }

        } else if (json.shipto!='' && json.consineeName == '' && json.emptybox != '' && json.accountName == '') {

          if (json.totalGst =="0.00") {

            content.push({

              table: {
                widths: [200, 'auto'],
                // heights: [85, 0],
                body:[
                  [{image:taxinvoice,width: 204,height:140,border:[true,true,true,true]},{text: [{text:'Bill To : \n',alignment:'left',fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: json.customerName+'\n',border:[false,false,false,false],bold:1,alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text:json.custAddress,bold:1,alignment:'left',border:[false,false,false,false],fontSize: 9,fonts: 'TimesNewRoman'},{text: '\nPhone  : ' + json.custPhone,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nEmail    : ' + json.email,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nGSTIN  : ' + json.gstIn,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nState    : ' + json.statename + '   State Code : ' + json.statecode,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\n'+json.emptybox,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text:'\n\nPlace Of Supply : ' + json.shipto,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'}],rowSpan:5}],
                  [{text:'Invoice No      :   ' +  json.invoice_bill,fontSize: 10,bold: 1,absolutePosition: {x: 42, y: 303},color: '#000000',fonts: 'TimesNewRoman'},{}],
                  [{text:'Invoice Date   :   '   +  convertDate(json.invoiceDate),margin: [2,-7,0,0],alignment: 'left',fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],
                  [{text:'Due Date         :   ' + convertDate(json.dueDate),margin: [2,5,0,0],alignment: 'left',fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],
                  [{text:'Lut No              :   ' + json.branchlutno,alignment: 'left',margin: [2,5,0,0],fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],


                ]

              },			layout: 'noBorders'


            })
            
          } else {

            content.push({

              table: {
                widths: [200, 'auto'],
                // heights: [85, 0],
                body:[
                  [{image:taxinvoice,width: 204,height:140,border:[true,true,true,true]},{text: [{text:'Bill To : \n',alignment:'left',fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: json.customerName+'\n',border:[false,false,false,false],bold:1,alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text:json.custAddress,bold:1,alignment:'left',border:[false,false,false,false],fontSize: 9,fonts: 'TimesNewRoman'},{text: '\nPhone  : ' + json.custPhone,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nEmail    : ' + json.email,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nGSTIN  : ' + json.gstIn,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nState    : ' + json.statename + '   State Code : ' + json.statecode,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\n'+json.emptybox,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text:'\n\nPlace Of Supply : ' + json.shipto,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'}],rowSpan:4}],
                  [{text:'Invoice No      :   ' +  json.invoice_bill,fontSize: 10,bold: 1,absolutePosition: {x: 42, y: 303},color: '#000000',fonts: 'TimesNewRoman'},{}],
                  [{text:'Invoice Date   :   '   +  convertDate(json.invoiceDate),margin: [2,-7,0,0],alignment: 'left',fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],
                  [{text:'Due Date         :   ' + convertDate(json.dueDate),margin: [2,5,0,0],alignment: 'left',fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],

                ]

              },layout: 'noBorders'


            })

          }

        } else if (json.shipto!='' && json.consineeName == '' && json.emptybox == '' && json.accountName != '') {

          if (json.totalGst =="0.00") {

            content.push({

              table: {
                widths: [200, 'auto'],
                // heights: [85, 0],
                body:[
                  [{image:taxinvoice,width: 204,height:140,border:[true,true,true,true]},{text: [{text:'Bill To : \n',alignment:'left',fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: json.customerName+'\nA/C. : ' + json.accountName+'\n',border:[false,false,false,false],bold:1,alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text:json.custAddress,bold:1,alignment:'left',border:[false,false,false,false],fontSize: 9,fonts: 'TimesNewRoman'},{text: '\nPhone  : ' + json.custPhone,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nEmail    : ' + json.email,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nGSTIN  : ' + json.gstIn,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nState    : ' + json.statename + '   State Code : ' + json.statecode,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text:'\n\nPlace Of Supply : ' + json.shipto,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'}],rowSpan:5}],
                  [{text:'Invoice No      :   ' +  json.invoice_bill,fontSize: 10,bold: 1,absolutePosition: {x: 42, y: 303},color: '#000000',fonts: 'TimesNewRoman'},{}],
                  [{text:'Invoice Date   :   '   +  convertDate(json.invoiceDate),margin: [2,-7,0,0],alignment: 'left',fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],
                  [{text:'Due Date         :   ' + convertDate(json.dueDate),margin: [2,5,0,0],alignment: 'left',fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],
                  [{text:'Lut No              :   ' + json.branchlutno,alignment: 'left',margin: [2,5,0,0],fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],


                ]

              },			layout: 'noBorders'


            })
            
          } else {

            content.push({

              table: {
                widths: [200, 'auto'],
                // heights: [85, 0],
                body:[
                  [{image:taxinvoice,width: 204,height:140,border:[true,true,true,true]},{text: [{text:'Bill To : \n',alignment:'left',fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: json.customerName+'\nA/C. : ' + json.accountName+'\n',border:[false,false,false,false],bold:1,alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text:json.custAddress,bold:1,alignment:'left',border:[false,false,false,false],fontSize: 9,fonts: 'TimesNewRoman'},{text: '\nPhone  : ' + json.custPhone,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nEmail    : ' + json.email,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nGSTIN  : ' + json.gstIn,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nState    : ' + json.statename + '   State Code : ' + json.statecode,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text:'\n\nPlace Of Supply : ' + json.shipto,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'}],rowSpan:4}],
                  [{text:'Invoice No      :   ' +  json.invoice_bill,fontSize: 10,bold: 1,absolutePosition: {x: 42, y: 303},color: '#000000',fonts: 'TimesNewRoman'},{}],
                  [{text:'Invoice Date   :   '   +  convertDate(json.invoiceDate),margin: [2,-7,0,0],alignment: 'left',fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],
                  [{text:'Due Date         :   ' + convertDate(json.dueDate),margin: [2,5,0,0],alignment: 'left',fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],

                ]

              },layout: 'noBorders'


            })

          }

        } else if (json.shipto =='' && json.consineeName != '' && json.emptybox != '' && json.accountName == '') {

          if (json.totalGst =="0.00") {

            content.push({

              table: {
                widths: [200, 'auto'],
                 //heights: [0, 10],
                body:[
                  [{image:taxinvoice,width: 204,height:140,border:[true,true,true,true]},{text: [{text:'Bill To : \n',alignment:'left',fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: json.customerName+'\n',border:[false,false,false,false],bold:1,alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text:json.custAddress,bold:1,alignment:'left',border:[false,false,false,false],fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nPhone  : ' + json.custPhone,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nEmail    : ' + json.email,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nGSTIN  : ' + json.gstIn,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nState    : ' + json.statename + '   State Code : ' + json.statecode,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\n'+json.emptybox,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nShip To : \n',alignment:'left',fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: json.consineeName,bold:1,alignment:'left',fontSize: 10,fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: '\nConsinee : ' + json.accountaddress,bold:1,alignment:'left',fontSize: 10,fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: '\nPhone  : ' + json.accountlandline,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nEmail    : ' + json.accountemail + '\nGSTIN  : ' + json.accountgst + '\nState    : ' + json.accountStatename + '   State Code : ' + json.accountStateCode,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'}],rowSpan:5}],
                  [{text:'Invoice No      :   ' +  json.invoice_bill,fontSize: 10,bold: 1,absolutePosition: {x: 42, y: 303},color: '#000000',fonts: 'TimesNewRoman'},{}],
                  [{text:'Invoice Date   :   '   +  convertDate(json.invoiceDate),margin: [2,-7,0,0],alignment: 'left',fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],
                  [{text:'Due Date         :   ' + convertDate(json.dueDate),margin: [2,5,0,0],alignment: 'left',fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],
                  [{text:'Lut No              :   ' + json.branchlutno,alignment: 'left',margin: [2,5,0,0],fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],


                ]

              },			layout: 'noBorders'


            })
            
          } else {

            content.push({

              table: {
                widths: [200, 'auto'],
                //  heights: [0, 10],
                body:[
                  [{image:taxinvoice,width: 204,height:140,border:[true,true,true,true]},{text: [{text:'Bill To : \n',alignment:'left',fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: json.customerName+'\n',border:[false,false,false,false],bold:1,alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text:json.custAddress,bold:1,alignment:'left',border:[false,false,false,false],fontSize: 9,fonts: 'TimesNewRoman'},{text: '\nPhone  : ' + json.custPhone,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nEmail    : ' + json.email,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nGSTIN  : ' + json.gstIn,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nState    : ' + json.statename + '   State Code : ' + json.statecode,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\n'+json.emptybox,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nShip To : \n',alignment:'left',fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: json.consineeName,bold:1,alignment:'left',fontSize: 10,fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: '\nConsinee : ' + json.accountaddress,bold:1,alignment:'left',fontSize: 10,fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: '\nPhone  : ' + json.accountlandline,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nEmail    : ' + json.accountemail + '\nGSTIN  : ' + json.accountgst + '\nState    : ' + json.accountStatename + '   State Code : ' + json.accountStateCode,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'}],rowSpan:4}],
                  [{text:'Invoice No      :   ' +  json.invoice_bill,fontSize: 10,bold: 1,absolutePosition: {x: 42, y: 303},color: '#000000',fonts: 'TimesNewRoman'},{}],
                  [{text:'Invoice Date   :   '   +  convertDate(json.invoiceDate),margin: [2,-7,0,0],alignment: 'left',fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],
                  [{text:'Due Date         :   ' + convertDate(json.dueDate),margin: [2,5,0,0],alignment: 'left',fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],

                ]

              },			layout: 'noBorders'


            })

          }

        } else if (json.shipto=='' && json.consineeName != '' && json.emptybox == '' && json.accountName != '') {


          if (json.totalGst =="0.00") {

            content.push({

              table: {
                widths: [200, 'auto'],
                // heights: [85, 0],
                body:[
                  [{image:taxinvoice,width: 204,height:140,border:[true,true,true,true]},{text: [{text:'Bill To : \n',alignment:'left',fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: json.customerName+'\nA/C. : ' + json.accountName+'\n',border:[false,false,false,false],bold:1,alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text:json.custAddress,bold:1,alignment:'left',border:[false,false,false,false],fontSize: 9,fonts: 'TimesNewRoman'},{text: '\nPhone  : ' + json.custPhone,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nEmail    : ' + json.email,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nGSTIN  : ' + json.gstIn,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nState    : ' + json.statename + '   State Code : ' + json.statecode,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nShip To : \n',alignment:'left',fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: json.consineeName,bold:1,alignment:'left',fontSize: 10,fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: '\nConsinee : ' + json.accountaddress,bold:1,alignment:'left',fontSize: 10,fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: '\nPhone  : ' + json.accountlandline,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nEmail    : ' + json.accountemail + '\nGSTIN  : ' + json.accountgst + '\nState    : ' + json.accountStatename + '   State Code : ' + json.accountStateCode,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'}],rowSpan:5}],
                  [{text:'Invoice No      :   ' +  json.invoice_bill,fontSize: 10,bold: 1,absolutePosition: {x: 42, y: 303},color: '#000000',fonts: 'TimesNewRoman'},{}],
                  [{text:'Invoice Date   :   '   +  convertDate(json.invoiceDate),margin: [2,-7,0,0],alignment: 'left',fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],
                  [{text:'Due Date         :   ' + convertDate(json.dueDate),margin: [2,5,0,0],alignment: 'left',fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],
                  [{text:'Lut No              :   ' + json.branchlutno,alignment: 'left',margin: [2,5,0,0],fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],


                ]

              },			layout: 'noBorders'


            })
            
          } else {

            content.push({

              table: {
                widths: [200, 'auto'],
                // heights: [85, 0],
                body:[
                  [{image:taxinvoice,width: 204,height:140,border:[true,true,true,true]},{text: [{text:'Bill To : \n',alignment:'left',fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: json.customerName+'\nA/C. : ' + json.accountName+'\n',border:[false,false,false,false],bold:1,alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text:json.custAddress,bold:1,alignment:'left',border:[false,false,false,false],fontSize: 9,fonts: 'TimesNewRoman'},{text: '\nPhone  : ' + json.custPhone,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nEmail    : ' + json.email,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nGSTIN  : ' + json.gstIn,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nState    : ' + json.statename + '   State Code : ' + json.statecode,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nShip To : \n',alignment:'left',fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: json.consineeName,bold:1,alignment:'left',fontSize: 10,fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: '\nConsinee : ' + json.accountaddress,bold:1,alignment:'left',fontSize: 10,fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: '\nPhone  : ' + json.accountlandline,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nEmail    : ' + json.accountemail + '\nGSTIN  : ' + json.accountgst + '\nState    : ' + json.accountStatename + '   State Code : ' + json.accountStateCode,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'}],rowSpan:4}],
                  [{text:'Invoice No      :   ' +  json.invoice_bill,fontSize: 10,bold: 1,absolutePosition: {x: 42, y: 303},color: '#000000',fonts: 'TimesNewRoman'},{}],
                  [{text:'Invoice Date   :   '   +  convertDate(json.invoiceDate),margin: [2,-7,0,0],alignment: 'left',fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],
                  [{text:'Due Date         :   ' + convertDate(json.dueDate),margin: [2,5,0,0],alignment: 'left',fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],
                ]

              },			layout: 'noBorders'


            })

          }

        } else if (json.shipto=='' && json.consineeName == '' && json.emptybox != '' && json.accountName != '') {
          console.log('abbbbbbbbbbbbbbbbbb');

          if (json.totalGst =="0.00") {

            content.push({

              table: {
                widths: [200, 'auto'],
                // heights: [85, 0],
                body:[
                  [{image:taxinvoice,width: 204,height:140,border:[true,true,true,true]},{text: [{text:'Bill To : \n',alignment:'left',fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: json.customerName+'\nA/C. : ' + json.accountName+'\n',border:[false,false,false,false],bold:1,alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text:json.custAddress,bold:1,alignment:'left',border:[false,false,false,false],fontSize: 9,fonts: 'TimesNewRoman'},{text: '\nPhone  : ' + json.custPhone,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nEmail    : ' + json.email,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nGSTIN  : ' + json.gstIn,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nState    : ' + json.statename + '   State Code : ' + json.statecode,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\n'+json.emptybox,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'}],rowSpan:5}],
                  [{text:'Invoice No      :   ' +  json.invoice_bill,fontSize: 10,bold: 1,absolutePosition: {x: 42, y: 303},color: '#000000',fonts: 'TimesNewRoman'},{}],
                  [{text:'Invoice Date   :   '   +  convertDate(json.invoiceDate),margin: [2,-7,0,0],alignment: 'left',fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],
                  [{text:'Due Date         :   ' + convertDate(json.dueDate),margin: [2,5,0,0],alignment: 'left',fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],
                  [{text:'Lut No              :   ' + json.branchlutno,alignment: 'left',margin: [2,5,0,0],fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],


                ]

              },			layout: 'noBorders'


            })
            
          } else {

            content.push({

              table: {
                widths: [200, 'auto'],
                // heights: [85, 0],
                body:[
                  [{image:taxinvoice,width: 204,height:140},{text: [{text:'Bill To : \n',alignment:'left',fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: json.customerName+'\nA/C. : ' + json.accountName+'\n',border:[false,false,false,false],bold:1,alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text:json.custAddress,bold:1,alignment:'left',border:[false,false,false,false],fontSize: 9,fonts: 'TimesNewRoman'},{text: '\nPhone  : ' + json.custPhone,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nEmail    : ' + json.email,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nGSTIN  : ' + json.gstIn,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nState    : ' + json.statename + '   State Code : ' + json.statecode,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\n'+json.emptybox,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'}],rowSpan:4}],
                  [{text:'Invoice No      :   ' +  json.invoice_bill,fontSize: 10,bold: 1,absolutePosition: {x: 42, y: 303},color: '#000000',fonts: 'TimesNewRoman'},{}],
                  [{text:'Invoice Date   :   '   +  convertDate(json.invoiceDate),margin: [2,-7,0,0],alignment: 'left',fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],
                  [{text:'Due Date         :   ' + convertDate(json.dueDate),margin: [2,5,0,0],alignment: 'left',fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],

                ]

              },layout: 'noBorders'


            })

          }

        } else if (json.shipto!='' && json.consineeName == '' && json.emptybox == '' && json.accountName == '') {

          if (json.totalGst =="0.00") {

            content.push({

              table: {
                widths: [200, 'auto'],
                 //heights: [0, 10],
                body:[
                  [{image:taxinvoice,width: 204,height:140,border:[true,true,true,true]},{text: [{text:'Bill To : \n',alignment:'left',fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: json.customerName+'\n',border:[false,false,false,false],bold:1,alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text:json.custAddress,bold:1,alignment:'left',border:[false,false,false,false],fontSize: 9,fonts: 'TimesNewRoman'},{text: '\nPhone  : ' + json.custPhone,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nEmail    : ' + json.email,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nGSTIN  : ' + json.gstIn,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nState    : ' + json.statename + '   State Code : ' + json.statecode,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text:'\n\nPlace Of Supply : ' + json.shipto,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'}],rowSpan:5}],
                  [{text:'Invoice No      :   ' +  json.invoice_bill,fontSize: 10,bold: 1,absolutePosition: {x: 42, y: 303},color: '#000000',fonts: 'TimesNewRoman'},{}],
                  [{text:'Invoice Date   :   '   +  convertDate(json.invoiceDate),margin: [2,-7,0,0],alignment: 'left',fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],
                  [{text:'Due Date         :   ' + convertDate(json.dueDate),margin: [2,5,0,0],alignment: 'left',fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],
                  [{text:'Lut No              :   ' + json.branchlutno,alignment: 'left',margin: [2,5,0,0],fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],


                ]

              },			layout: 'noBorders'


            })
            
          } else {

            content.push({

              table: {
                widths: [200, 'auto'],
                //  heights: [0, 10],
                body:[
                  [{image:taxinvoice,width: 204,height:140,border:[true,true,true,true]},{text: [{text:'Bill To : \n',alignment:'left',fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: json.customerName+'\n',border:[false,false,false,false],bold:1,alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text:json.custAddress,bold:1,alignment:'left',border:[false,false,false,false],fontSize: 9,fonts: 'TimesNewRoman'},{text: '\nPhone  : ' + json.custPhone,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nEmail    : ' + json.email,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nGSTIN  : ' + json.gstIn,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nState    : ' + json.statename + '   State Code : ' + json.statecode,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text:'\n\nPlace Of Supply : ' + json.shipto,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'}],rowSpan:4}],
                  [{text:'Invoice No      :   ' +  json.invoice_bill,fontSize: 10,bold: 1,absolutePosition: {x: 42, y: 303},color: '#000000',fonts: 'TimesNewRoman'},{}],
                  [{text:'Invoice Date   :   '   +  convertDate(json.invoiceDate),margin: [2,-7,0,0],alignment: 'left',fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],
                  [{text:'Due Date         :   ' + convertDate(json.dueDate),margin: [2,5,0,0],alignment: 'left',fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],
                ]

              },			layout: 'noBorders'


            })

          }


        } else if (json.shipto=='' && json.consineeName != '' && json.emptybox == '' && json.accountName =='') {

          console.log('ppppppppppppppppppppppppp');

          if (json.totalGst =="0.00") {

            content.push({

              table: {
                widths: [200, 'auto'],
                // heights: [85, 0],
                body:[
                  [{image:taxinvoice,width: 204,height:140,border:[true,true,true,true]},{text: [{text:'Bill To : \n',alignment:'left',fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: json.customerName +'\n',border:[false,false,false,false],bold:1,alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text:json.custAddress,bold:1,alignment:'left',border:[false,false,false,false],fontSize: 9,fonts: 'TimesNewRoman'},{text: '\nPhone  : ' + json.custPhone,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nEmail    : ' + json.email,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nGSTIN  : ' + json.gstIn,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nState    : ' + json.statename + '   State Code : ' + json.statecode,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nShip To : \n',alignment:'left',fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: json.consineeName,bold:1,alignment:'left',fontSize: 10,fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: '\nConsinee : ' + json.accountaddress,bold:1,alignment:'left',fontSize: 10,fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: '\nPhone  : ' + json.accountlandline,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nEmail    : ' + json.accountemail + '\nGSTIN  : ' + json.accountgst + '\nState    : ' + json.accountStatename + '   State Code : ' + json.accountStateCode,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'}],rowSpan:5}],
                  [{text:'Invoice No      :   ' +  json.invoice_bill,fontSize: 10,bold: 1,absolutePosition: {x: 42, y: 303},color: '#000000',fonts: 'TimesNewRoman'},{}],
                  [{text:'Invoice Date   :   '   +  convertDate(json.invoiceDate),margin: [2,-7,0,0],alignment: 'left',fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],
                  [{text:'Due Date         :   ' + convertDate(json.dueDate),margin: [2,5,0,0],alignment: 'left',fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],
                  [{text:'Lut No              :   ' + json.branchlutno,alignment: 'left',margin: [2,5,0,0],fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],
                ]

              },			layout: 'noBorders'


            })


          } else {

            content.push({

              table: {
                widths: [200, 'auto'],
                // heights: [85, 0],
                body:[
                  [{image:taxinvoice,width: 204,height:140,border:[true,true,true,true]},{text: [{text:'Bill To : \n',alignment:'left',fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: json.customerName +'\n',border:[false,false,false,false],bold:1,alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text:json.custAddress,bold:1,alignment:'left',border:[false,false,false,false],fontSize: 9,fonts: 'TimesNewRoman'},{text: '\nPhone  : ' + json.custPhone,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nEmail    : ' + json.email,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nGSTIN  : ' + json.gstIn,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nState    : ' + json.statename + '   State Code : ' + json.statecode,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nShip To : \n',alignment:'left',fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: json.consineeName,bold:1,alignment:'left',fontSize: 10,fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: '\nConsinee : ' + json.accountaddress,bold:1,alignment:'left',fontSize: 10,fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: '\nPhone  : ' + json.accountlandline,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nEmail    : ' + json.accountemail + '\nGSTIN  : ' + json.accountgst + '\nState    : ' + json.accountStatename + '   State Code : ' + json.accountStateCode,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'}],rowSpan:4}],
                  [{text:'Invoice No      :   ' +  json.invoice_bill,fontSize: 10,bold: 1,absolutePosition: {x: 42, y: 303},color: '#000000',fonts: 'TimesNewRoman'},{}],
                  [{text:'Invoice Date   :   '   +  convertDate(json.invoiceDate),margin: [2,-7,0,0],alignment: 'left',fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],
                  [{text:'Due Date         :   ' + convertDate(json.dueDate),margin: [2,5,0,0],alignment: 'left',fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],
                ]

              },			layout: 'noBorders'


            })

          }


        } else if (json.shipto=='' && json.consineeName == '' && json.emptybox != '' && json.accountName == '') {
          if (json.totalGst =="0.00") {

            content.push({

              table: {
                widths: [200, 'auto'],
                // heights: [85, 0],
                body:[
                  [{image:taxinvoice,width: 204,height:140,border:[true,true,true,true]},{text: [{text:'Bill To : \n',alignment:'left',fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: json.customerName+'\n',border:[false,false,false,false],bold:1,alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text:json.custAddress,bold:1,alignment:'left',border:[false,false,false,false],fontSize: 9,fonts: 'TimesNewRoman'},{text: '\nPhone  : ' + json.custPhone,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nEmail    : ' + json.email,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nGSTIN  : ' + json.gstIn,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nState    : ' + json.statename + '   State Code : ' + json.statecode,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\n'+json.emptybox,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'}],rowSpan:5}],
                  [{text:'Invoice No      :   ' +  json.invoice_bill,fontSize: 10,bold: 1,absolutePosition: {x: 42, y: 303},color: '#000000',fonts: 'TimesNewRoman'},{}],
                  [{text:'Invoice Date   :   '   +  convertDate(json.invoiceDate),margin: [2,-7,0,0],alignment: 'left',fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],
                  [{text:'Due Date         :   ' + convertDate(json.dueDate),margin: [2,5,0,0],alignment: 'left',fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],
                  [{text:'Lut No              :   ' + json.branchlutno,alignment: 'left',margin: [2,5,0,0],fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],


                ]

              },			layout: 'noBorders'


            })
            
          } else {

            content.push({

              table: {
                widths: [200, 'auto'],
                // heights: [85, 0],
                body:[
                  [{image:taxinvoice,width: 204,height:140,border:[true,true,true,true]},{text: [{text:'Bill To : \n',alignment:'left',fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: json.customerName+'\n',border:[false,false,false,false],bold:1,alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text:json.custAddress,bold:1,alignment:'left',border:[false,false,false,false],fontSize: 9,fonts: 'TimesNewRoman'},{text: '\nPhone  : ' + json.custPhone,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nEmail    : ' + json.email,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nGSTIN  : ' + json.gstIn,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nState    : ' + json.statename + '   State Code : ' + json.statecode,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\n'+json.emptybox,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'}],rowSpan:4}],
                  [{text:'Invoice No      :   ' +  json.invoice_bill,fontSize: 10,bold: 1,absolutePosition: {x: 42, y: 303},color: '#000000',fonts: 'TimesNewRoman'},{}],
                  [{text:'Invoice Date   :   '   +  convertDate(json.invoiceDate),margin: [2,-7,0,0],alignment: 'left',fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],
                  [{text:'Due Date         :   ' + convertDate(json.dueDate),margin: [2,5,0,0],alignment: 'left',fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],

                ]

              },layout: 'noBorders'


            })

          }

        } else if (json.shipto=='' && json.consineeName == '' && json.emptybox == '' && json.accountName != '') {

          if (json.totalGst =="0.00") {

            content.push({

              table: {
                widths: [200, 'auto'],
                // heights: [85, 0],
                body:[
                  [{image:taxinvoice,width: 204,height:140,border:[true,true,true,true]},{text: [{text:'Bill To : \n',alignment:'left',fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: json.customerName+'\nA/C. : ' + json.accountName+'\n',border:[false,false,false,false],bold:1,alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text:json.custAddress,bold:1,alignment:'left',border:[false,false,false,false],fontSize: 9,fonts: 'TimesNewRoman'},{text: '\nPhone  : ' + json.custPhone,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nEmail    : ' + json.email,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nGSTIN  : ' + json.gstIn,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nState    : ' + json.statename + '   State Code : ' + json.statecode,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'}],rowSpan:5}],
                  [{text:'Invoice No      :   ' +  json.invoice_bill,fontSize: 10,bold: 1,absolutePosition: {x: 42, y: 303},color: '#000000',fonts: 'TimesNewRoman'},{}],
                  [{text:'Invoice Date   :   '   +  convertDate(json.invoiceDate),margin: [2,-7,0,0],alignment: 'left',fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],
                  [{text:'Due Date         :   ' + convertDate(json.dueDate),margin: [2,5,0,0],alignment: 'left',fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],
                  [{text:'Lut No              :   ' + json.branchlutno,alignment: 'left',margin: [2,5,0,0],fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],


                ]

              },			layout: 'noBorders'


            })
            
          } else {

            content.push({

              table: {
                widths: [200, 'auto'],
                // heights: [85, 0],
                body:[
                  [{image:taxinvoice,width: 204,height:140,border:[true,true,true,true]},{text: [{text:'Bill To : \n',alignment:'left',fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: json.customerName+'\nA/C. : ' + json.accountName+'\n',border:[false,false,false,false],bold:1,alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text:json.custAddress,bold:1,alignment:'left',border:[false,false,false,false],fontSize: 9,fonts: 'TimesNewRoman'},{text: '\nPhone  : ' + json.custPhone,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nEmail    : ' + json.email,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nGSTIN  : ' + json.gstIn,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nState    : ' + json.statename + '   State Code : ' + json.statecode,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'}],rowSpan:4}],
                  [{text:'Invoice No      :   ' +  json.invoice_bill,fontSize: 10,bold: 1,absolutePosition: {x: 42, y: 303},color: '#000000',fonts: 'TimesNewRoman'},{}],
                  [{text:'Invoice Date   :   '   +  convertDate(json.invoiceDate),margin: [2,-7,0,0],alignment: 'left',fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],
                  [{text:'Due Date         :   ' + convertDate(json.dueDate),margin: [2,5,0,0],alignment: 'left',fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],

                ]

              },layout: 'noBorders'


            })

          }

        } else {


          if (json.totalGst =="0.00") {

            content.push({

              table: {
                widths: [200, 'auto'],
                // heights: [85, 0],
                body:[
                  [{image:taxinvoice,width: 204,height:140,border:[true,true,true,true]},{text: [{text:'Bill To : \n',alignment:'left',fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: json.customerName+'\n',border:[false,false,false,false],bold:1,alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text:json.custAddress,bold:1,alignment:'left',border:[false,false,false,false],fontSize: 9,fonts: 'TimesNewRoman'},{text: '\nPhone  : ' + json.custPhone,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nEmail    : ' + json.email,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nGSTIN  : ' + json.gstIn,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nState    : ' + json.statename + '   State Code : ' + json.statecode,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'}],rowSpan:5}],
                  [{text:'Invoice No      :   ' +  json.invoice_bill,fontSize: 10,bold: 1,absolutePosition: {x: 42, y: 303},color: '#000000',fonts: 'TimesNewRoman'},{}],
                  [{text:'Invoice Date   :   '   +  convertDate(json.invoiceDate),margin: [2,-7,0,0],alignment: 'left',fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],
                  [{text:'Due Date         :   ' + convertDate(json.dueDate),margin: [2,5,0,0],alignment: 'left',fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],
                  [{text:'Lut No              :   ' + json.branchlutno,alignment: 'left',margin: [2,5,0,0],fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],


                ]

              },			layout: 'noBorders'


            })
            
          } else {

            content.push({

              table: {
                widths: [200, 'auto'],
                // heights: [85, 0],
                body:[
                  [{image:taxinvoice,width: 204,height:140,border:[true,true,true,true]},{text: [{text:'Bill To : \n',alignment:'left',fonts: 'TimesNewRoman',border:[false,false,false,false]},{text: json.customerName+'\n',border:[false,false,false,false],bold:1,alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text:json.custAddress,bold:1,alignment:'left',border:[false,false,false,false],fontSize: 9,fonts: 'TimesNewRoman'},{text: '\nPhone  : ' + json.custPhone,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nEmail    : ' + json.email,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nGSTIN  : ' + json.gstIn,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'},{text: '\nState    : ' + json.statename + '   State Code : ' + json.statecode,bold:1,border:[false,false,false,false],alignment:'left',fontSize: 10,fonts: 'TimesNewRoman'}],rowSpan:4}],
                  [{text:'Invoice No      :   ' +  json.invoice_bill,fontSize: 10,bold: 1,absolutePosition: {x: 42, y: 303},color: '#000000',fonts: 'TimesNewRoman'},{}],
                  [{text:'Invoice Date   :   '   +  convertDate(json.invoiceDate),margin: [2,-7,0,0],alignment: 'left',fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],
                  [{text:'Due Date         :   ' + convertDate(json.dueDate),margin: [2,5,0,0],alignment: 'left',fontSize: 10,bold: 1,border:[false,false,false,false],fonts: 'TimesNewRoman'},{}],

                ]

              },layout: 'noBorders'


            })

          }


        }
         
                

        content.push({
            text: '\n'
        });

        var currencySymbol = '';
        var currencyFullFom ='';
        if (json.currency == 'Rs') {

          currencySymbol = '₹';
          currencyFullFom = 'Rupees';


        } else if(json.currency == 'USD'){

          currencySymbol = '$';
          currencyFullFom = 'USD';

        }

        table1 = {
          widths: [],
          dontBreakRows: true,
          body: []
        };
        table1['widths'].push('auto', '*', 40, 'auto', 40, 40,'auto','auto', 'auto', 50);

        if (json.category == "productSaleBill") {

          table1['body'].push([{text:'#',border: [true, true, true, false],bold:1,fillColor: '#bdc3c7'}, {text:'Item & Description',border: [true, true, true, false],bold:1,alignment:'center',fillColor: '#bdc3c7'},{text:'HSN',border: [true, true, true, false],bold:1,alignment:'center',fillColor: '#bdc3c7'}, {text:'Qty',border: [true, true, true, false],bold:1,alignment:'center',fillColor: '#bdc3c7'},{text:'Rate',border: [true, true, true, false],bold:1,alignment:'center',fillColor: '#bdc3c7'},{text:'Taxable Value',border: [true, true, true, false],bold:1,alignment:'center',fillColor: '#bdc3c7'}, {text:'Tax Type',border: [true, true, true, false],bold:1,alignment:'center',fillColor: '#bdc3c7'},{text:'Tax Rate',border: [true, true, true, false],bold:1,alignment:'center',fillColor: '#bdc3c7'},{text:'Tax Amount',border: [true, true, true, false],bold:1,alignment:'center',fillColor: '#bdc3c7'}, {text:'Amount',border: [true, true, true, false],bold:1,alignment:'center',fillColor: '#bdc3c7'}]);

        } else {

          table1['body'].push([{text:'#',border: [true, true, true, false],bold:1,fillColor: '#bdc3c7'}, {text:'Item & Description',border: [true, true, true, false],bold:1,alignment:'center',fillColor: '#bdc3c7'},{text:'SAC',border: [true, true, true, false],bold:1,alignment:'center',fillColor: '#bdc3c7'}, {text:'Qty',border: [true, true, true, false],bold:1,alignment:'center',fillColor: '#bdc3c7'},{text:'Rate',border: [true, true, true, false],bold:1,alignment:'center',fillColor: '#bdc3c7'},{text:'Taxable Value',border: [true, true, true, false],bold:1,alignment:'center',fillColor: '#bdc3c7'}, {text:'Tax Type',border: [true, true, true, false],bold:1,alignment:'center',fillColor: '#bdc3c7'},{text:'Tax Rate',border: [true, true, true, false],bold:1,alignment:'center',fillColor: '#bdc3c7'},{text:'Tax Amount',border: [true, true, true, false],bold:1,alignment:'center',fillColor: '#bdc3c7'}, {text:'Amount',border: [true, true, true, false],bold:1,alignment:'center',fillColor: '#bdc3c7'}]);

        }

        table1['body'].push([{text:'',border: [true, false, true, false],bold:1,fillColor: '#bdc3c7'}, {text:'',border: [true, false, true, false],bold:1,alignment:'center',fillColor: '#bdc3c7'},{text:'',border: [true, false, true, false],bold:1,alignment:'center',fillColor: '#bdc3c7'}, {text:'',border: [true, false, true, false],bold:1,alignment:'center',fillColor: '#bdc3c7'},{text:currencySymbol,border: [true, true, true, false],bold:1,alignment:'center',fillColor: '#bdc3c7'},{text:currencySymbol,border: [true, true, true, false],bold:1,alignment:'center',fillColor: '#bdc3c7'}, {text:'',border: [true, false, true, false],bold:1,alignment:'center',fillColor: '#bdc3c7'},{text:'',border: [true, false, true, false],bold:1,alignment:'center',fillColor: '#bdc3c7'},{text:currencySymbol,border: [true, true, true, false],bold:1,alignment:'center',fillColor: '#bdc3c7'}, {text:currencySymbol,border: [true, true, true, false],bold:1,alignment:'center',fillColor: '#bdc3c7'}]);

        var serviceJson = json.service_json;

        

        var couter = 1;

        for (var i in serviceJson) {
          var arr = [];
          let regExpr = /[`^]/gi;

          var conditionandscope = serviceJson[i].itemDescription.replace(regExpr, '\n');

          if (json.branchStateCode==json.statecode) {
     
            arr.push(couter, {text:[{text:serviceJson[i].servicename +'\n',bold:1}, {text:conditionandscope,fontSize: 7}]}, {text:serviceJson[i].sac,alignment: 'center'}, {text:serviceJson[i].qty +'\n'+ serviceJson[i].qty1,alignment: 'center'}, {text: serviceJson[i].rate,alignment: 'center'},{text: serviceJson[i].amount,alignment: 'center'},{text:'CGST \n SGST',alignment: 'center'},{text: serviceJson[i].cgstper+'% \n'+ serviceJson[i].sgstper+'%',alignment: 'center'},{text: serviceJson[i].cgstamount+'\n' +  serviceJson[i].sgstamount,alignment: 'center'},{text: serviceJson[i].totalamount,alignment: 'center'});

          } else {

            arr.push(couter, {text:[{text:serviceJson[i].servicename +'\n',bold:1}, {text:conditionandscope,fontSize: 7}]}, {text:serviceJson[i].sac,alignment: 'center'}, {text:serviceJson[i].qty +'\n'+ serviceJson[i].qty1,alignment: 'center'}, {text: serviceJson[i].rate,alignment: 'center'},{text: serviceJson[i].amount,alignment: 'center'},{text:'IGST',alignment: 'center'},{text: serviceJson[i].igstper+'%',alignment: 'center'},{text: serviceJson[i].igstamount,alignment: 'center'},{text: serviceJson[i].totalamount,alignment: 'center'});

          }
          table1['body'].push(arr);
          couter++;

        }
        content.push({
            style: 'tablfont',
            table: table1,
            layout: {
            hLineColor: function (i, node) {
              return (i === 0 || i === node.table.body.length) ? 'gray' : 'gray';
            },
            vLineColor: function (i, node) {
              return (i === 0 || i === node.table.widths.length) ? 'gray' : 'gray';
            },
          },
        });

        content.push({
            text: '\n'
        });

        let borders = '';


        let subTotal =  parseFloat(json.subTotal)+parseFloat(json.totalGst);

            if (json.currency == 'Rs' && json.notes != '' && json.addTerm != '' ) {
              
              content.push({
                id:'signature',
                table: {
                      widths: [300,'*','*'],
                      body:[
                        [{text: 'Total In Words\n' + currencyFullFom + ' ' + convertNumberToWords(json.netAmount),alignment:'left',border:borders,fontSize: 10,rowSpan:2},{text: 'Sub Total ',margin:[20,0,0,0],alignment:'left',fonts: 'TimesNewRoman',fontSize: 10,border:[true,true,false,false]},{text: subTotal,alignment:'right',fontSize: 10,fonts: 'TimesNewRoman',border:[false,true,true,false]}],
                        [{},{text: 'Rounded Off ',margin:[20,0,0,0],alignment:'left',fonts: 'TimesNewRoman',fontSize: 10,border:[true,false,false,false]},{text: json.roundOff,alignment:'right',fontSize: 10,fonts: 'TimesNewRoman',border:[false,false,true,false]}],
                        [{text: 'Terms and Conditions',bold:1,fontSize: 10,alignment:'left',border:[true,false,false,false],rowSpan:1},{text: 'Total ',margin:[20,0,0,0],alignment:'left',bold:1,fonts: 'TimesNewRoman',fontSize: 10,border:[true,false,false,false]},{text: currencySymbol + ' ' + json.netAmount,alignment:'right',fontSize: 10,bold:1,fonts: 'TimesNewRoman',border:[false,false,true,false]}],
                        [{text: 'Interest @18 % will be charged on all overdue Invoices',fontSize: 10,alignment:'left',border:[true,false,false,false]},{text: 'Payment Made ',margin:[20,0,0,0],alignment:'left',fonts: 'TimesNewRoman',fontSize: 10,border:[true,false,false,false]},{text: currencySymbol + ' ' + json.receiveAmt,alignment:'right',fontSize: 10,fonts: 'TimesNewRoman',border:[false,false,true,false]}],
                        [{text:'All disputes, claims and / Or any differences arising on any matter ralating to this invoice are subject to be exclusive jurisdiction of the courts at Mumbai.',fontSize: 10,alignment:'left',border:[true,false,false,false]},{text: 'Balance Due ',margin:[20,0,0,0],alignment:'left',bold:1,fonts: 'TimesNewRoman',fontSize: 10,border:[true,false,false,true]},{text:currencySymbol + ' ' + json.netamt,alignment:'right',fontSize: 10,bold:1,fonts: 'TimesNewRoman',border:[false,false,true,true]}],
                        [{text: 'Rejection or any discrepancies in the invoice to be intimated within a period on 15 days from date of invoice.',fontSize: 10,alignment:'left',border:[true,false,false,false]},{text: 'For Pest Mortem (India) Pvt. Ltd. ',alignment:'center',fonts: 'TimesNewRoman',fontSize: 10,border:[true,false,true,false],colSpan:2,rowSpan:2},{}],
                        [{text: json.addTerm,fontSize: 10,alignment:'left',border:[true,false,false,true]},{},{}],
                        [{text:'Company Name : Pest Mortem (India) Pvt. Ltd.  ',border: [true, false, true, false],alignment:'left',fontSize: 9,fonts: 'TimesNewRoman'},{text: '',width: 100,height: 50,margin:[40,-20,0,0],border: [false, false, true, false],colSpan:2,rowSpan:4},{}],
                        [{text:'Bank Name     : '+ json.bankName  + ' , ' + json.branchName ,border: [true, false, true, false],alignment:'left',fontSize: 9,fonts: 'TimesNewRoman'},{},{}],
                        [{text:'Account No     : '+ json.accountNo,border: [true, false, true, false],alignment:'left',fontSize: 9,fonts: 'TimesNewRoman'},{},{}],
                        [{text:'RTGS/NEFT/IFSC Code : '+ json.ifscCode+'\n',border: [true, false, true, false],alignment:'left',fontSize: 9,fonts: 'TimesNewRoman'},{},{}],
                        [{id:'closingParagraph',text:'Notes : ' + json.notes,margin:[0,0,0,3],alignment:'left',fontSize: 9,fonts: 'TimesNewRoman',border: [true, false, false, true]},{text: 'Authorised Signatory ',alignment:'center',fonts: 'TimesNewRoman',fontSize: 10,border:[true,false,true,true],colSpan:2},{}],

                      ],

                    },layout: {
                        hLineColor: function (i, node) {
                        return (i === 0 || i === node.table.body.length) ? 'black' : 'black';
                        },
                        vLineColor: function (i, node) {
                        return (i === 0 || i === node.table.widths.length) ? 'black' : 'black';
                        },
                    },
                    
                   
                    
                                      
              });

            } else if (json.currency == 'Rs' && json.notes == '' && json.addTerm != '') {

              content.push({
                id:'signature',
                table: {
                      widths: [300,'*','*'],
                      body:[
                        [{text: 'Total In Words\n' + currencyFullFom + ' ' + convertNumberToWords(json.netAmount),alignment:'left',border:borders,fontSize: 10,rowSpan:2},{text: 'Sub Total ',margin:[20,0,0,0],alignment:'left',fonts: 'TimesNewRoman',fontSize: 10,border:[true,true,false,false]},{text: subTotal,alignment:'right',fontSize: 10,fonts: 'TimesNewRoman',border:[false,true,true,false]}],
                        [{},{text: 'Rounded Off ',margin:[20,0,0,0],alignment:'left',fonts: 'TimesNewRoman',fontSize: 10,border:[true,false,false,false]},{text: json.roundOff,alignment:'right',fontSize: 10,fonts: 'TimesNewRoman',border:[false,false,true,false]}],
                        [{text: 'Terms and Conditions',bold:1,fontSize: 10,alignment:'left',border:[true,false,false,false],rowSpan:1},{text: 'Total ',margin:[20,0,0,0],alignment:'left',bold:1,fonts: 'TimesNewRoman',fontSize: 10,border:[true,false,false,false]},{text: currencySymbol + ' ' + json.netAmount,alignment:'right',fontSize: 10,bold:1,fonts: 'TimesNewRoman',border:[false,false,true,false]}],
                        [{text: 'Interest @18 % will be charged on all overdue Invoices',fontSize: 10,alignment:'left',border:[true,false,false,false]},{text: 'Payment Made ',margin:[20,0,0,0],alignment:'left',fonts: 'TimesNewRoman',fontSize: 10,border:[true,false,false,false]},{text: currencySymbol + ' ' + json.receiveAmt,alignment:'right',fontSize: 10,fonts: 'TimesNewRoman',border:[false,false,true,false]}],
                        [{text:'All disputes, claims and / Or any differences arising on any matter ralating to this invoice are subject to be exclusive jurisdiction of the courts at Mumbai.',fontSize: 10,alignment:'left',border:[true,false,false,false]},{text: 'Balance Due ',margin:[20,0,0,0],alignment:'left',bold:1,fonts: 'TimesNewRoman',fontSize: 10,border:[true,false,false,true]},{text:currencySymbol + ' ' + json.netamt,alignment:'right',fontSize: 10,bold:1,fonts: 'TimesNewRoman',border:[false,false,true,true]}],
                        [{text: 'Rejection or any discrepancies in the invoice to be intimated within a period on 15 days from date of invoice.',fontSize: 10,alignment:'left',border:[true,false,false,false]},{text: 'For Pest Mortem (India) Pvt. Ltd. ',alignment:'center',fonts: 'TimesNewRoman',fontSize: 10,border:[true,false,true,false],colSpan:2,rowSpan:2},{}],
                        [{text: json.addTerm,fontSize: 10,alignment:'left',border:[true,false,false,true]},{},{}],
                        [{text:'Company Name : Pest Mortem (India) Pvt. Ltd.  ',border: [true, false, true, false],alignment:'left',fontSize: 9,fonts: 'TimesNewRoman'},{text: '',width: 100,height: 50,margin:[40,-20,0,0],border: [false, false, true, false],colSpan:2,rowSpan:4},{}],
                        [{text:'Bank Name     : '+ json.bankName  + ' , ' + json.branchName ,border: [true, false, true, false],alignment:'left',fontSize: 9,fonts: 'TimesNewRoman'},{},{}],
                        [{text:'Account No     : '+ json.accountNo,border: [true, false, true, false],alignment:'left',fontSize: 9,fonts: 'TimesNewRoman'},{},{}],
                        [{text:'RTGS/NEFT/IFSC Code : '+ json.ifscCode+'\n',border: [true, false, true, true],alignment:'left',fontSize: 9,fonts: 'TimesNewRoman',rowSpan:2},{},{}],
                        [{},{text: 'Authorised Signatory ',alignment:'center',fonts: 'TimesNewRoman',fontSize: 10,border:[true,false,true,true],colSpan:2},{}],



                      ],

                    },layout: {
                        hLineColor: function (i, node) {
                        return (i === 0 || i === node.table.body.length) ? 'black' : 'black';
                        },
                        vLineColor: function (i, node) {
                        return (i === 0 || i === node.table.widths.length) ? 'black' : 'black';
                        },
                    }
              });


            } else if (json.currency == 'Rs' && json.notes != '' && json.addTerm == '') {

              content.push({
                id:'signature',
                table: {
                      widths: [300,'*','*'],
                      body:[
                        [{text: 'Total In Words\n' + currencyFullFom + ' ' + convertNumberToWords(json.netAmount),alignment:'left',border:borders,fontSize: 10,rowSpan:2},{text: 'Sub Total ',margin:[20,0,0,0],alignment:'left',fonts: 'TimesNewRoman',fontSize: 10,border:[true,true,false,false]},{text: subTotal,alignment:'right',fontSize: 10,fonts: 'TimesNewRoman',border:[false,true,true,false]}],
                        [{},{text: 'Rounded Off ',margin:[20,0,0,0],alignment:'left',fonts: 'TimesNewRoman',fontSize: 10,border:[true,false,false,false]},{text: json.roundOff,alignment:'right',fontSize: 10,fonts: 'TimesNewRoman',border:[false,false,true,false]}],
                        [{text: 'Terms and Conditions',bold:1,fontSize: 10,alignment:'left',border:[true,false,false,false],rowSpan:1},{text: 'Total ',margin:[20,0,0,0],alignment:'left',bold:1,fonts: 'TimesNewRoman',fontSize: 10,border:[true,false,false,false]},{text: currencySymbol + ' ' + json.netAmount,alignment:'right',fontSize: 10,bold:1,fonts: 'TimesNewRoman',border:[false,false,true,false]}],
                        [{text: 'Interest @18 % will be charged on all overdue Invoices',fontSize: 10,alignment:'left',border:[true,false,false,false]},{text: 'Payment Made ',margin:[20,0,0,0],alignment:'left',fonts: 'TimesNewRoman',fontSize: 10,border:[true,false,false,false]},{text: currencySymbol + ' ' + json.receiveAmt,alignment:'right',fontSize: 10,fonts: 'TimesNewRoman',border:[false,false,true,false]}],
                        [{text:'All disputes, claims and / Or any differences arising on any matter ralating to this invoice are subject to be exclusive jurisdiction of the courts at Mumbai.',fontSize: 10,alignment:'left',border:[true,false,false,false]},{text: 'Balance Due ',margin:[20,0,0,0],alignment:'left',bold:1,fonts: 'TimesNewRoman',fontSize: 10,border:[true,false,false,true]},{text:currencySymbol + ' ' + json.netamt,alignment:'right',fontSize: 10,bold:1,fonts: 'TimesNewRoman',border:[false,false,true,true]}],
                        [{text: 'Rejection or any discrepancies in the invoice to be intimated within a period on 15 days from date of invoice.',fontSize: 10,alignment:'left',border:[true,false,false,true]},{text: 'For Pest Mortem (India) Pvt. Ltd. ',alignment:'center',fonts: 'TimesNewRoman',fontSize: 10,border:[true,false,true,false],colSpan:2,rowSpan:1},{}],
                        [{text:'Company Name : Pest Mortem (India) Pvt. Ltd.  ',border: [true, false, true, false],alignment:'left',fontSize: 9,fonts: 'TimesNewRoman'},{text: '',width: 100,height: 50,margin:[40,-10,0,0],border: [false, false, true, false],colSpan:2,rowSpan:4},{}],
                        [{text:'Bank Name     : '+ json.bankName  + ' , ' + json.branchName ,border: [true, false, true, false],alignment:'left',fontSize: 9,fonts: 'TimesNewRoman'},{},{}],
                        [{text:'Account No     : '+ json.accountNo,border: [true, false, true, false],alignment:'left',fontSize: 9,fonts: 'TimesNewRoman'},{},{}],
                        [{text:'RTGS/NEFT/IFSC Code : '+ json.ifscCode+'\n',border: [true, false, true, false],alignment:'left',fontSize: 9,fonts: 'TimesNewRoman'},{},{}],
                        [{id:'closingParagraph',text:'Notes : ' + json.notes,margin:[0,0,0,3],alignment:'left',fontSize: 9,fonts: 'TimesNewRoman',border: [true, false, false, true]},{text: 'Authorised Signatory ',alignment:'center',fonts: 'TimesNewRoman',fontSize: 10,border:[true,false,true,true],colSpan:2},{}],



                      ],

                    },layout: {
                        hLineColor: function (i, node) {
                        return (i === 0 || i === node.table.body.length) ? 'black' : 'black';
                        },
                        vLineColor: function (i, node) {
                        return (i === 0 || i === node.table.widths.length) ? 'black' : 'black';
                        },
                    }
              });

            } else if (json.currency == 'Rs' && json.notes == '' && json.addTerm == '') {

              content.push({
                id:'signature',
                table: {
                      widths: [300,'*','*'],
                      body:[
                        [{text: 'Total In Words\n' + currencyFullFom + ' ' + convertNumberToWords(json.netAmount),alignment:'left',border:borders,fontSize: 10,rowSpan:2},{text: 'Sub Total ',margin:[20,0,0,0],alignment:'left',fonts: 'TimesNewRoman',fontSize: 10,border:[true,true,false,false]},{text: subTotal,alignment:'right',fontSize: 10,fonts: 'TimesNewRoman',border:[false,true,true,false]}],
                        [{},{text: 'Rounded Off ',margin:[20,0,0,0],alignment:'left',fonts: 'TimesNewRoman',fontSize: 10,border:[true,false,false,false]},{text: json.roundOff,alignment:'right',fontSize: 10,fonts: 'TimesNewRoman',border:[false,false,true,false]}],
                        [{text: 'Terms and Conditions',bold:1,fontSize: 10,alignment:'left',border:[true,false,false,false],rowSpan:1},{text: 'Total ',margin:[20,0,0,0],alignment:'left',bold:1,fonts: 'TimesNewRoman',fontSize: 10,border:[true,false,false,false]},{text: currencySymbol + ' ' + json.netAmount,alignment:'right',fontSize: 10,bold:1,fonts: 'TimesNewRoman',border:[false,false,true,false]}],
                        [{text: 'Interest @18 % will be charged on all overdue Invoices',fontSize: 10,alignment:'left',border:[true,false,false,false]},{text: 'Payment Made ',margin:[20,0,0,0],alignment:'left',fonts: 'TimesNewRoman',fontSize: 10,border:[true,false,false,false]},{text: currencySymbol + ' ' + json.receiveAmt,alignment:'right',fontSize: 10,fonts: 'TimesNewRoman',border:[false,false,true,false]}],
                        [{text:'All disputes, claims and / Or any differences arising on any matter ralating to this invoice are subject to be exclusive jurisdiction of the courts at Mumbai.',fontSize: 10,alignment:'left',border:[true,false,false,false]},{text: 'Balance Due ',margin:[20,0,0,0],alignment:'left',bold:1,fonts: 'TimesNewRoman',fontSize: 10,border:[true,false,false,true]},{text:currencySymbol + ' ' + json.netamt,alignment:'right',fontSize: 10,bold:1,fonts: 'TimesNewRoman',border:[false,false,true,true]}],
                        [{text: 'Rejection or any discrepancies in the invoice to be intimated within a period on 15 days from date of invoice.',fontSize: 10,alignment:'left',border:[true,false,false,true]},{text: 'For Pest Mortem (India) Pvt. Ltd. ',alignment:'center',fonts: 'TimesNewRoman',fontSize: 10,border:[true,false,true,false],colSpan:2,rowSpan:1},{}],
                        [{text:'Company Name : Pest Mortem (India) Pvt. Ltd.  ',border: [true, false, true, false],alignment:'left',fontSize: 9,fonts: 'TimesNewRoman'},{text: '',width: 100,height: 50,margin:[40,-10,0,0],border: [false, false, true, false],colSpan:2,rowSpan:4},{}],
                        [{text:'Bank Name     : '+ json.bankName  + ' , ' + json.branchName ,border: [true, false, true, false],alignment:'left',fontSize: 9,fonts: 'TimesNewRoman'},{},{}],
                        [{text:'Account No     : '+ json.accountNo,border: [true, false, true, false],alignment:'left',fontSize: 9,fonts: 'TimesNewRoman'},{},{}],
                        [{text:'RTGS/NEFT/IFSC Code : '+ json.ifscCode+'\n',border: [true, false, true, true],alignment:'left',fontSize: 9,fonts: 'TimesNewRoman',rowSpan:2},{},{}],
                        [{},{id:'closingParagraph',text: 'Authorised Signatory ',alignment:'center',fonts: 'TimesNewRoman',fontSize: 10,border:[true,false,true,true],colSpan:2},{}],

                      ],

                    },layout: {
                        hLineColor: function (i, node) {
                        return (i === 0 || i === node.table.body.length) ? 'black' : 'black';
                        },
                        vLineColor: function (i, node) {
                        return (i === 0 || i === node.table.widths.length) ? 'black' : 'black';
                        },
                    }
              });

            }else if (json.currency == 'USD' && json.notes != '' && json.addTerm != '' ) {
              
              content.push({
                id:'signature',
                table: {
                      widths: [300,'*','*'],
                      body:[
                        [{text: 'Total In Words\n' + currencyFullFom + ' ' + convertNumberToWords(json.netAmount),alignment:'left',border:[true,true,false,true],fontSize: 10,rowSpan:2},{text: 'Sub Total ',margin:[20,0,0,0],alignment:'left',fonts: 'TimesNewRoman',fontSize: 10,border:[true,true,false,false]},{text: subTotal,alignment:'right',fontSize: 10,fonts: 'TimesNewRoman',border:[false,true,true,false]}],
                        [{},{text: 'Rounded Off ',margin:[20,0,0,0],alignment:'left',fonts: 'TimesNewRoman',fontSize: 10,border:[true,false,false,false]},{text: json.roundOff,alignment:'right',fontSize: 10,fonts: 'TimesNewRoman',border:[false,false,true,false]}],
                        [{text: 'Terms and Conditions',bold:1,fontSize: 10,alignment:'left',border:[true,false,false,false],rowSpan:1},{text: 'Total ',margin:[20,0,0,0],alignment:'left',bold:1,fonts: 'TimesNewRoman',fontSize: 10,border:[true,false,false,false]},{text: currencySymbol + ' ' + json.netAmount,alignment:'right',fontSize: 10,bold:1,fonts: 'TimesNewRoman',border:[false,false,true,false]}],
                        [{text: 'Interest @18 % will be charged on all overdue Invoices',fontSize: 10,alignment:'left',border:[true,false,false,false]},{text: 'Payment Made ',margin:[20,0,0,0],alignment:'left',fonts: 'TimesNewRoman',fontSize: 10,border:[true,false,false,false]},{text: currencySymbol + ' ' + json.receiveAmt,alignment:'right',fontSize: 10,fonts: 'TimesNewRoman',border:[false,false,true,false]}],
                        [{text:'All disputes, claims and / Or any differences arising on any matter ralating to this invoice are subject to be exclusive jurisdiction of the courts at Mumbai.',fontSize: 10,alignment:'left',border:[true,false,false,false]},{text: 'Balance Due ',margin:[20,0,0,0],alignment:'left',bold:1,fonts: 'TimesNewRoman',fontSize: 10,border:[true,false,false,true]},{text:currencySymbol + ' ' + json.netamt,alignment:'right',fontSize: 10,bold:1,fonts: 'TimesNewRoman',border:[false,false,true,true]}],
                        [{text: 'Rejection or any discrepancies in the invoice to be intimated within a period on 15 days from date of invoice.',fontSize: 10,alignment:'left',border:[true,false,false,false]},{text: 'For Pest Mortem (India) Pvt. Ltd. ',alignment:'center',fonts: 'TimesNewRoman',fontSize: 10,border:[true,false,true,false],colSpan:2,rowSpan:2},{}],
                        [{text: json.addTerm,fontSize: 10,alignment:'left',border:[true,false,false,true]},{},{}],
                        [{text:'PLEASE REMIT FUNDS TO -  \nINTERMEDIARY BANK: JP MORGAN CHASE BANK, NEW YORK, USA.\n SWIFT CODE : CHASUS33 \n FEDWIRE ABA NO.021000021\nCHIPS ABA NO.002 CHIPS UID NO.354459 \n FAVOURING BENEFICIARY BANK : HDFC BANK LTD., \n INDIA SWIFT CODE: HDFCINBB\nHDFC BANK ACCOUNT NO. WITH JP MORGAN 001-1-406717\nBENEFICIARY NAME : Pest Mortem (India) Pvt.Ltd.\n BENEFICIARY ACCOUNT NO.50200017596497',border: [true, false, true, false],alignment:'left',fontSize: 9,fonts: 'TimesNewRoman'},{text: '',width: 100,height: 100,margin:[40,-20,0,0],border: [false, false, true, false],colSpan:2,rowSpan:1},{}],
                        [{id:'closingParagraph',text:'Notes : ' + json.notes,margin:[0,0,0,3],alignment:'left',fontSize: 9,fonts: 'TimesNewRoman',border: [true, false, false, true]},{text: 'Authorised Signatory ',alignment:'center',fonts: 'TimesNewRoman',fontSize: 10,border:[true,false,true,true],colSpan:2},{}],

                      ]
                    },layout: {
                        hLineColor: function (i, node) {
                        return (i === 0 || i === node.table.body.length) ? 'black' : 'black';
                        },
                        vLineColor: function (i, node) {
                        return (i === 0 || i === node.table.widths.length) ? 'black' : 'black';
                        },
                    }
              });

            } else if (json.currency == 'USD' && json.notes == '' && json.addTerm != '' ) {
              
              content.push({
                id:'signature',
                table: {
                      widths: [300,'*','*'],
                      body:[
                        [{text: 'Total In Words\n' + currencyFullFom + ' ' + convertNumberToWords(json.netAmount),alignment:'left',border:[true,true,false,true],fontSize: 10,rowSpan:2},{text: 'Sub Total ',margin:[20,0,0,0],alignment:'left',fonts: 'TimesNewRoman',fontSize: 10,border:[true,true,false,false]},{text: subTotal,alignment:'right',fontSize: 10,fonts: 'TimesNewRoman',border:[false,true,true,false]}],
                        [{},{text: 'Rounded Off ',margin:[20,0,0,0],alignment:'left',fonts: 'TimesNewRoman',fontSize: 10,border:[true,false,false,false]},{text: json.roundOff,alignment:'right',fontSize: 10,fonts: 'TimesNewRoman',border:[false,false,true,false]}],
                        [{text: 'Terms and Conditions',bold:1,fontSize: 10,alignment:'left',border:[true,false,false,false],rowSpan:1},{text: 'Total ',margin:[20,0,0,0],alignment:'left',bold:1,fonts: 'TimesNewRoman',fontSize: 10,border:[true,false,false,false]},{text: currencySymbol + ' ' + json.netAmount,alignment:'right',fontSize: 10,bold:1,fonts: 'TimesNewRoman',border:[false,false,true,false]}],
                        [{text: 'Interest @18 % will be charged on all overdue Invoices',fontSize: 10,alignment:'left',border:[true,false,false,false]},{text: 'Payment Made ',margin:[20,0,0,0],alignment:'left',fonts: 'TimesNewRoman',fontSize: 10,border:[true,false,false,false]},{text: currencySymbol + ' ' + json.receiveAmt,alignment:'right',fontSize: 10,fonts: 'TimesNewRoman',border:[false,false,true,false]}],
                        [{text:'All disputes, claims and / Or any differences arising on any matter ralating to this invoice are subject to be exclusive jurisdiction of the courts at Mumbai.',fontSize: 10,alignment:'left',border:[true,false,false,false]},{text: 'Balance Due ',margin:[20,0,0,0],alignment:'left',bold:1,fonts: 'TimesNewRoman',fontSize: 10,border:[true,false,false,true]},{text:currencySymbol + ' ' + json.netamt,alignment:'right',fontSize: 10,bold:1,fonts: 'TimesNewRoman',border:[false,false,true,true]}],
                        [{text: 'Rejection or any discrepancies in the invoice to be intimated within a period on 15 days from date of invoice.',fontSize: 10,alignment:'left',border:[true,false,false,false]},{text: 'For Pest Mortem (India) Pvt. Ltd. ',alignment:'center',fonts: 'TimesNewRoman',fontSize: 10,border:[true,false,true,false],colSpan:2,rowSpan:2},{}],
                        [{text: json.addTerm,fontSize: 10,alignment:'left',border:[true,false,false,true]},{},{}],
                        [{text:'PLEASE REMIT FUNDS TO -  \nINTERMEDIARY BANK: JP MORGAN CHASE BANK, NEW YORK, USA.\n SWIFT CODE : CHASUS33 \n FEDWIRE ABA NO.021000021\nCHIPS ABA NO.002 CHIPS UID NO.354459 \n FAVOURING BENEFICIARY BANK : HDFC BANK LTD., \n INDIA SWIFT CODE: HDFCINBB\nHDFC BANK ACCOUNT NO. WITH JP MORGAN 001-1-406717\nBENEFICIARY NAME : Pest Mortem (India) Pvt.Ltd.\n BENEFICIARY ACCOUNT NO.50200017596497',border: [true, false, true, true],alignment:'left',fontSize: 9,fonts: 'TimesNewRoman',rowSpan:2},{text: '',width: 100,height: 100,margin:[40,-20,0,0],border: [false, false, true, false],colSpan:2,rowSpan:1},{}],
                        [{},{id:'closingParagraph',text: 'Authorised Signatory ',alignment:'center',fonts: 'TimesNewRoman',fontSize: 10,border:[true,false,true,true],colSpan:2},{}],
                      ]
                    },layout: {
                        hLineColor: function (i, node) {
                        return (i === 0 || i === node.table.body.length) ? 'black' : 'black';
                        },
                        vLineColor: function (i, node) {
                        return (i === 0 || i === node.table.widths.length) ? 'black' : 'black';
                        },
                    }
              });

            } else if (json.currency == 'USD' && json.notes != '' && json.addTerm == '' ) {
              
              content.push({
                id:'signature',
                table: {
                      widths: [300,'*','*'],
                      body:[
                        [{text: 'Total In Words\n' + currencyFullFom + ' ' + convertNumberToWords(json.netAmount),alignment:'left',border:[true,true,false,true],fontSize: 10,rowSpan:2},{text: 'Sub Total ',margin:[20,0,0,0],alignment:'left',fonts: 'TimesNewRoman',fontSize: 10,border:[true,true,false,false]},{text: subTotal,alignment:'right',fontSize: 10,fonts: 'TimesNewRoman',border:[false,true,true,false]}],
                        [{},{text: 'Rounded Off ',margin:[20,0,0,0],alignment:'left',fonts: 'TimesNewRoman',fontSize: 10,border:[true,false,false,false]},{text: json.roundOff,alignment:'right',fontSize: 10,fonts: 'TimesNewRoman',border:[false,false,true,false]}],
                        [{text: 'Terms and Conditions',bold:1,fontSize: 10,alignment:'left',border:[true,false,false,false],rowSpan:1},{text: 'Total ',margin:[20,0,0,0],alignment:'left',bold:1,fonts: 'TimesNewRoman',fontSize: 10,border:[true,false,false,false]},{text: currencySymbol + ' ' + json.netAmount,alignment:'right',fontSize: 10,bold:1,fonts: 'TimesNewRoman',border:[false,false,true,false]}],
                        [{text: 'Interest @18 % will be charged on all overdue Invoices',fontSize: 10,alignment:'left',border:[true,false,false,false]},{text: 'Payment Made ',margin:[20,0,0,0],alignment:'left',fonts: 'TimesNewRoman',fontSize: 10,border:[true,false,false,false]},{text: currencySymbol + ' ' + json.receiveAmt,alignment:'right',fontSize: 10,fonts: 'TimesNewRoman',border:[false,false,true,false]}],
                        [{text:'All disputes, claims and / Or any differences arising on any matter ralating to this invoice are subject to be exclusive jurisdiction of the courts at Mumbai.',fontSize: 10,alignment:'left',border:[true,false,false,false]},{text: 'Balance Due ',margin:[20,0,0,0],alignment:'left',bold:1,fonts: 'TimesNewRoman',fontSize: 10,border:[true,false,false,true]},{text:currencySymbol + ' ' + json.netamt,alignment:'right',fontSize: 10,bold:1,fonts: 'TimesNewRoman',border:[false,false,true,true]}],
                        [{text: 'Rejection or any discrepancies in the invoice to be intimated within a period on 15 days from date of invoice.',fontSize: 10,alignment:'left',border:[true,false,false,true]},{text: 'For Pest Mortem (India) Pvt. Ltd. ',alignment:'center',fonts: 'TimesNewRoman',fontSize: 10,border:[true,false,true,false],colSpan:2,rowSpan:1},{}],
                        [{text:'PLEASE REMIT FUNDS TO -  \nINTERMEDIARY BANK: JP MORGAN CHASE BANK, NEW YORK, USA.\n SWIFT CODE : CHASUS33 \n FEDWIRE ABA NO.021000021\nCHIPS ABA NO.002 CHIPS UID NO.354459 \n FAVOURING BENEFICIARY BANK : HDFC BANK LTD., \n INDIA SWIFT CODE: HDFCINBB\nHDFC BANK ACCOUNT NO. WITH JP MORGAN 001-1-406717\nBENEFICIARY NAME : Pest Mortem (India) Pvt.Ltd.\n BENEFICIARY ACCOUNT NO.50200017596497',border: [true, false, true, false],alignment:'left',fontSize: 9,fonts: 'TimesNewRoman'},{text: '',width: 100,height: 100,margin:[40,-20,0,0],border: [false, false, true, false],colSpan:2,rowSpan:1},{}],
                        [{id:'closingParagraph',text:'Notes : ' + json.notes,margin:[0,0,0,3],alignment:'left',fontSize: 9,fonts: 'TimesNewRoman',border: [true, false, false, true]},{text: 'Authorised Signatory ',alignment:'center',fonts: 'TimesNewRoman',fontSize: 10,border:[true,false,true,true],colSpan:2},{}],

                      ]
                    },layout: {
                        hLineColor: function (i, node) {
                        return (i === 0 || i === node.table.body.length) ? 'black' : 'black';
                        },
                        vLineColor: function (i, node) {
                        return (i === 0 || i === node.table.widths.length) ? 'black' : 'black';
                        },
                    }
              });

            } else if (json.currency == 'USD' && json.notes == '' && json.addTerm == '' ) {
              
              content.push({
                id:'signature',
                table: {
                      widths: [300,'*','*'],
                      body:[
                        [{text: 'Total In Words\n' + currencyFullFom + ' ' + convertNumberToWords(json.netAmount),alignment:'left',border:[true,true,false,true],fontSize: 10,rowSpan:2},{text: 'Sub Total ',margin:[20,0,0,0],alignment:'left',fonts: 'TimesNewRoman',fontSize: 10,border:[true,true,false,false]},{text: subTotal,alignment:'right',fontSize: 10,fonts: 'TimesNewRoman',border:[false,true,true,false]}],
                        [{},{text: 'Rounded Off ',margin:[20,0,0,0],alignment:'left',fonts: 'TimesNewRoman',fontSize: 10,border:[true,false,false,false]},{text: json.roundOff,alignment:'right',fontSize: 10,fonts: 'TimesNewRoman',border:[false,false,true,false]}],
                        [{text: 'Terms and Conditions',bold:1,fontSize: 10,alignment:'left',border:[true,false,false,false],rowSpan:1},{text: 'Total ',margin:[20,0,0,0],alignment:'left',bold:1,fonts: 'TimesNewRoman',fontSize: 10,border:[true,false,false,false]},{text: currencySymbol + ' ' + json.netAmount,alignment:'right',fontSize: 10,bold:1,fonts: 'TimesNewRoman',border:[false,false,true,false]}],
                        [{text: 'Interest @18 % will be charged on all overdue Invoices',fontSize: 10,alignment:'left',border:[true,false,false,false]},{text: 'Payment Made ',margin:[20,0,0,0],alignment:'left',fonts: 'TimesNewRoman',fontSize: 10,border:[true,false,false,false]},{text: currencySymbol + ' ' + json.receiveAmt,alignment:'right',fontSize: 10,fonts: 'TimesNewRoman',border:[false,false,true,false]}],
                        [{text:'All disputes, claims and / Or any differences arising on any matter ralating to this invoice are subject to be exclusive jurisdiction of the courts at Mumbai.',fontSize: 10,alignment:'left',border:[true,false,false,false]},{text: 'Balance Due ',margin:[20,0,0,0],alignment:'left',bold:1,fonts: 'TimesNewRoman',fontSize: 10,border:[true,false,false,true]},{text:currencySymbol + ' ' + json.netamt,alignment:'right',fontSize: 10,bold:1,fonts: 'TimesNewRoman',border:[false,false,true,true]}],
                        [{text: 'Rejection or any discrepancies in the invoice to be intimated within a period on 15 days from date of invoice.',fontSize: 10,alignment:'left',border:[true,false,false,true]},{text: 'For Pest Mortem (India) Pvt. Ltd. ',alignment:'center',fonts: 'TimesNewRoman',fontSize: 10,border:[true,false,true,false],colSpan:2,rowSpan:1},{}],
                        [{text:'PLEASE REMIT FUNDS TO -  \nINTERMEDIARY BANK: JP MORGAN CHASE BANK, NEW YORK, USA.\n SWIFT CODE : CHASUS33 \n FEDWIRE ABA NO.021000021\nCHIPS ABA NO.002 CHIPS UID NO.354459 \n FAVOURING BENEFICIARY BANK : HDFC BANK LTD., \n INDIA SWIFT CODE: HDFCINBB\nHDFC BANK ACCOUNT NO. WITH JP MORGAN 001-1-406717\nBENEFICIARY NAME : Pest Mortem (India) Pvt.Ltd.\n BENEFICIARY ACCOUNT NO.50200017596497',border: [true, false, true, true],alignment:'left',fontSize: 9,fonts: 'TimesNewRoman',rowSpan:2},{text: '',width: 100,height: 100,margin:[40,-20,0,0],border: [false, false, true, false],colSpan:2,rowSpan:1},{}],
                        [{},{id:'closingParagraph',text: 'Authorised Signatory ',alignment:'center',fonts: 'TimesNewRoman',fontSize: 10,border:[true,false,true,true],colSpan:2},{}],

                      ]
                    },layout: {
                        hLineColor: function (i, node) {
                        return (i === 0 || i === node.table.body.length) ? 'black' : 'black';
                        },
                        vLineColor: function (i, node) {
                        return (i === 0 || i === node.table.widths.length) ? 'black' : 'black';
                        },
                    }
              });

            }

        dd = {

          pageSize: 'A4',

          pageOrientation: 'portrait',

          pageMargins: [40, 30, 30, 35],

          pageBreakBefore: function(currentNode, followingNodesOnPage, nodesOnNextPage, previousNodesOnPage) {
            if (currentNode.id === 'signature' && (currentNode.pageNumbers.length != 1 || currentNode.pageNumbers[0] != currentNode.pages)) {
              
              borders = [true,true,false,true];

              return true;
            }
            else if (currentNode.id === 'closingParagraph' && currentNode.pageNumbers.length != 1) {
              borders = [true,false,false,true]
              return true;

            }
            return false;
          },    

          footer: function (currentPage, pageCount) {

            return {

              margin: 10,

              columns: [{

                fontSize: 9,

                text: [{

                    text: '--------------------------------------------------------------------------' +
                      '\n',
                    margin: [0, 20]

                  },
                  {

                    text: [{text:'© Pest Mortem (India) Pvt. Ltd. | '},{text: ' REGD OFFICE : ',bold:1},{text: 'G-2, Sunder Tower, T. J. Road, Sewree (West), Mumbai,400015 | PAGE ' + currentPage.toString() + ' of ' + pageCount}],

                  }

                ],

                alignment: 'center'

              }]


            };

          },

          content,
          
          styles: {

            tablfont: {

              fontSize: 9,
              fonts: 'TimesNewRoman'
            }

          }

        }

              //  var win = window.open('', '_blank');

              //  pdfMake.createPdf(dd).open({}, win);
              pdfMake.createPdf(dd).download(invoice_bill + 'pdf');


            }

        }

    });

}

function convertNumberToWords(amount) {
    var words = new Array();
    words[0] = '';
    words[1] = 'One';
    words[2] = 'Two';
    words[3] = 'Three';
    words[4] = 'Four';
    words[5] = 'Five';
    words[6] = 'Six';
    words[7] = 'Seven';
    words[8] = 'Eight';
    words[9] = 'Nine';
    words[10] = 'Ten';
    words[11] = 'Eleven';
    words[12] = 'Twelve';
    words[13] = 'Thirteen';
    words[14] = 'Fourteen';
    words[15] = 'Fifteen';
    words[16] = 'Sixteen';
    words[17] = 'Seventeen';
    words[18] = 'Eighteen';
    words[19] = 'Nineteen';
    words[20] = 'Twenty';
    words[30] = 'Thirty';
    words[40] = 'Forty';
    words[50] = 'Fifty';
    words[60] = 'Sixty';
    words[70] = 'Seventy';
    words[80] = 'Eighty';
    words[90] = 'Ninety';
    amount = amount.toString();
    var atemp = amount.split(".");
    var number = atemp[0].split(",").join("");
    var n_length = number.length;
    var words_string = "";
    if (n_length <= 9) {
        var n_array = new Array(0, 0, 0, 0, 0, 0, 0, 0, 0);
        var received_n_array = new Array();
        for (var i = 0; i < n_length; i++) {
            received_n_array[i] = number.substr(i, 1);
        }
        for (var i = 9 - n_length, j = 0; i < 9; i++, j++) {
            n_array[i] = received_n_array[j];
        }
        for (var i = 0, j = 1; i < 9; i++, j++) {
            if (i == 0 || i == 2 || i == 4 || i == 7) {
                if (n_array[i] == 1) {
                    n_array[j] = 10 + parseInt(n_array[j]);
                    n_array[i] = 0;
                }
            }
        }
        value = "";
        for (var i = 0; i < 9; i++) {
            if (i == 0 || i == 2 || i == 4 || i == 7) {
                value = n_array[i] * 10;
            } else {
                value = n_array[i];
            }
            if (value != 0) {
                words_string += words[value] + " ";
            }
            if ((i == 1 && value != 0) || (i == 0 && value != 0 && n_array[i + 1] == 0)) {
                words_string += "Crores ";
            }
            if ((i == 3 && value != 0) || (i == 2 && value != 0 && n_array[i + 1] == 0)) {
                words_string += "Lakhs ";
            }
            if ((i == 5 && value != 0) || (i == 4 && value != 0 && n_array[i + 1] == 0)) {
                words_string += "Thousand ";
            }
            if (i == 6 && value != 0 && (n_array[i + 1] != 0 && n_array[i + 2] != 0)) {
                words_string += "Hundred and ";
            } else if (i == 6 && value != 0) {
                words_string += "Hundred ";
            }
        }
        words_string = words_string.split("  ").join(" ");
    }
    return words_string;
}

function convertDate(inputFormat){

var d = new Date(inputFormat);

var n = d.getMonth()+1;

if (n < 10){

  var month = '0' + n;

} else{

  var month = n;

}

var year = d.getFullYear();

var da = d.getDate();

if (da < 10){

  var date = '0' + da;

} else{

  var date = da;

}

return  date + '/' + month + '/' + year;

}
printer();
</script>
</div>
</body>
</html>
