<?php 
    
    $base    = '../../';
    $navenq1 = 'background:#1B1464;';

    include('header.php');

    if (session_status() == PHP_SESSION_NONE) { session_start(); }
    $sessionby = $_SESSION['employeeid'];
    $branchid  = $_SESSION['branchid'];

    $branchshortname = mysqli_fetch_assoc(mysqli_query($con,"SELECT branchshortname x FROM branchmaster WHERE branchid='$branchid'"))['x'];

?>

<br>
<img style="display:none;" id=imageid src="<?php echo $base;?>img/PestMortemLogo.jpg">

<div class="table-ui container-fluid">

    <div class="tr row">

        <div class="col-sm-2 th" style="word-wrap:break-word;">Bill No.</div>

        <div class="col-sm-4 th">Customer Info</div>

        <!-- <div class="col-sm-5 th">Bill Description</div> -->

        <div class="col-sm-4 th">Payment info</div>

        <div class="col-sm-2 th">Action</div>

    </div>


    <div class="row tr">

        <?php

            $result = mysqli_query($con,"SELECT * FROM billing WHERE branchid='$branchid' AND deleted_time=0  AND category='Fumigation' ");

            while ($row = mysqli_fetch_assoc($result)) {

                $customerid = $row['customerid'];

                $custEmail = get_object_vars(json_decode($row['customer_json'])[0])['email'];

                $billDate = strtotime(get_object_vars(json_decode($row['customer_json'])[0])['invoiceDate'])*1000;

                $dueDate = strtotime(get_object_vars(json_decode($row['customer_json'])[0])['dueDate'])*1000;


                $lastfollowup=$billDate;

                $followUpStatusFlag = '';

                if($lastfollowup!=0){

                  if($START_OF_THE_DAY<$lastfollowup && $END_OF_THE_DAY<=$lastfollowup){

                    $followUpStatusFlag='#3F51B5';

                  }

                  if($START_OF_THE_DAY>$lastfollowup && $END_OF_THE_DAY>$lastfollowup){

                    $followUpStatusFlag='#F44336';

                  }
                  
                  if($START_OF_THE_DAY<=$lastfollowup && $END_OF_THE_DAY>$lastfollowup ){

                    $followUpStatusFlag='#4CAF50';

                  }

                } else {

                  $followUpStatusFlag='#ff9800';

                }
                    
                $cifo      = mysqli_fetch_assoc(mysqli_query($con,"SELECT * FROM customermaster WHERE customerid='$customerid'"));

        ?>

                <div class="col-sm-2 td" style="word-wrap:break-word;">

                  <?php echo $row['invoice_bill']; ?>

                  <?php if ($billDate != '') {

                    echo '<br>Bill Date : <p style="background:'.$followUpStatusFlag.';color:#fff;">'.date("d-m-Y",$billDate/1000).'</p>';
                    echo '<br>Due Date : <p style="background:Red;color:#fff;">'.date("d-m-Y",$dueDate/1000).'</p>'; 

                  } ?>

                </div>

                <div class="col-sm-4 td">

                    <?php 

                      if ($cifo == '') {

                        echo 'Company : '.$customerid;
                        
                      } else {

                        echo 'Company : '.$cifo['customername']; 
                        echo '<br>GSTIN : '.get_object_vars(json_decode($cifo['customerjson']))['gstnumber'];

                        if (get_object_vars(json_decode($row['customer_json'])[0])['shipto'] !='') {
                          echo '<br>Service Location : '.get_object_vars(json_decode($row['customer_json'])[0])['shipto'];
                        }
                        
                        if (get_object_vars(json_decode($row['customer_json'])[0])['account'] !='') {
                          echo '<br>Bill A/C : '.get_object_vars(json_decode($row['customer_json'])[0])['account'];
                        }
                        if (get_object_vars(json_decode($row['customer_json'])[0])['cha'] !='') {
                          echo '<br>Cha : '.get_object_vars(json_decode($row['customer_json'])[0])['cha'];
                        }
                      
                      }

                    ?>

                </div>


                <div class="col-sm-4 td" style="word-wrap:break-word;">

                  <?php
                  
                    if ($row['created_time']<>0) {


                        $totalAmt = $row['contractamt']-$row['totalGst'];

                      echo 'Contract Amount : ' .number_format($row['contractamt'], 2);


                      echo '<table class="table table-list">';

                          echo '<thead>';

                            echo '<tr>';

                              echo '<th>BILL AMT</th>';

                              echo '<th>Total GST Amt.</th>';


                            echo '</tr>';

                          echo '</thead>';

                          echo '<tbody>';

                            echo '<tr>';

                              echo '<td align="center">'.number_format($totalAmt, 2).'</td>';

                              echo '<td align="center">'.number_format($row['totalGst'], 2).'</td>';
                              
                             echo '</tr>';

                          echo '</tbody>';

                      echo '</table>';


                    }  


                  ?>

                </div>

                <div class="col-sm-2 td" >

                    <?php

                        if ($row['category']=='Fumigation') {

                          if ($row['created_time']==0) {

                    ?>

                            <a href="bill.php?invoice_bill=<?php echo $row['invoice_bill'];?>&customerid=<?php echo $customerid;?>&category=<?php echo $row['category'];?>" class="btn btn-sm btn-primary btn-block">Fumigation Bill</a>


                    <?php
                          } else {
                    ?>

                            <a href="bill.php?invoice_bill=<?php echo $row['invoice_bill'];?>&customerid=<?php echo $customerid;?>&edit=true&category=<?php echo $row['category'];?>" class="btn btn-sm btn-primary btn-block">Edit</a>

                            <a href="bill.php?invoice_bill=<?php echo $row['invoice_bill'];?>&customerid=<?php echo $customerid;?>&copy=true&category=<?php echo $row['category'];?>" class="btn btn-sm btn-success btn-block">Copy</a>

                            <button data-invoice_bill="<?php echo $row['invoice_bill'];?>" data-branchid="<?php echo $row['branchid'];?>" class="btn btn-sm btn-info btn-block" onclick="printfumPdf(this);">Print Bill</button>

                            <button data-invoice_bill="<?php echo $row['invoice_bill'];?>" data-category="<?php echo $row['category'];?>" data-branchid="<?php echo $row['branchid'];?>" data-emailId="<?php echo $custEmail;?>" class="btn btn-sm btn-warning btn-block" onclick="TriggerOutlook(this)">Mail Pdf</a>

                    <?php
                          }

                        }
                    
                    ?>
                       

                        <button class="btn btn-sm btn-danger btn-block" data-BillNo="<?php echo $row['invoice_bill'];?>" data-branchid="<?php echo $row['branchid'];?>" onclick="deletebill(this)">Delete Bill</button>


                </div>

        <?php        
                
            }
        
        ?>

    </div>

</div>

<button class="btn" style="font-size:25px;border-radius:50px;width:60px;height:60px;position: fixed;bottom: 40px;
    right: 40px;background:#eb2f06;color:#fff;" data-toggle="modal" data-target="#myModal">+</button>


<div id="myModal" class="modal fade" role="dialog">

    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">

            <div class="modal-header">

                <h4 class="modal-title" align="center">New Bill</h4>

            </div>

            <div class="modal-body">

                <div class="form-horizontal">

                    <div class="form-group">

                        <label class="control-label col-sm-4" for="email">Select Customer :</label>

                        <div class="col-sm-8">

                            <input type="text" list="customerid"  data-name='customerid' data-role="text" class="form-control input-sm" />
            
                            <datalist id="customerid" >

                                <?php

                                    $result= mysqli_query($con,"SELECT customerid,customername  FROM customermaster ORDER by customerid ASC ");

                                    while($rows = mysqli_fetch_assoc($result)){

                                        echo '<option value="'.$rows['customerid'].' '.$rows['customername'].'"></option>';


                                    }
                                
                                ?>

                            </datalist>

                        </div>
                    </div>


                    <div class="form-group">

                        <label class="control-label col-sm-4" for="email">Select Category </label>

                        <div class="col-sm-8">

                            <select data-role="select" data-name='category' class="form-control input-sm" disabled>

                                <option value="Fumigation">Fumigation</option>

                                <option value="Pest Control">Pest Control</option>

                             </select>   
                        </div>

                    </div>


                    <div class="form-group">

                        <div class="col-sm-offset-2 col-sm-10">

                            <button class="btn btn-success btn-block btn-sm" id="newqneuiry" onclick="newBill()">Add
                                New Bill</button>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

</div>

<?php 
include('footer.php');
?>

<script>

function newBill() {

  var valid = true;

  if (checker('myModal') != false) {

    valid = valid * true;

  } else {

    alert('Please Select Customer');

    valid = valid * false;

  }

  if (valid) {

    var data = checker('myModal');

    var datastr = JSON.stringify(data);

    $.ajax({

      type: "POST",

      data: {

        data: datastr

      },

      url: 'api/setbill.php',

      cache: false,

      success: function (res) {

        if (res.status == 'success') {

          if (res.category=='Fumigation') {

            location.href = '/process/invoice/fumigationInvoice.php';

          } else {

            location.href = '/process/invoice/pestcontrolInvoice.php';

          }

        }

      }

    })

  }

}


function deletebill(e) {

  var BillNo = $(e).attr('data-BillNo');

  var branchid = $(e).attr('data-branchid');


  $.ajax({

    type: "POST",

    data: 'invoice_bill=' + BillNo + '&branchid=' + branchid,

    url: 'api/deleteBill.php',

    cache: false,

    success: function (res) {

      if (res.status == 'success') {

        window.location.reload();

      }

    }

  })

}

function getBase64Image(img) {
  var canvas = document.createElement("canvas");
  canvas.width = img.width;
  canvas.height = img.height;
  var ctx = canvas.getContext("2d");
  ctx.drawImage(img, 0, 0);
  var dataURL = canvas.toDataURL("image/png");
  return dataURL.replace('/^data:image\/(png|jpg);base64,/', "");
}

function printfumPdf(e) {

  var invoice_bill = $(e).attr('data-invoice_bill');

  var branchId = $(e).attr('data-branchid');

  $.ajax({

    type: "POST",

    data: "branchId=" + branchId + "&invoice_bill=" + invoice_bill,

    url: 'api/billSelect.php',

    cache: false,

    success: function (res) {

      if (res.status == 'success') {


        let content = [];

        let json = res.json[0];

        let base64 = getBase64Image(document.getElementById("imageid"));
        window.pdfMake.vfs["Times-New-Roman-Regular.ttf"] = "BASE 64 HERE";

        pdfMake.fonts = {
            // Default font should still be available
            Roboto: {
                normal: 'Roboto-Regular.ttf',
                bold: 'Roboto-Medium.ttf',
                italics: 'Roboto-Italic.ttf',
                bolditalics: 'Roboto-Italic.ttf'
            },
            // Make sure you define all 4 components - normal, bold, italics, bolditalics - (even if they all point to the same font file)
            TimesNewRoman: {
                normal: 'Times-New-Roman-Regular.ttf',
                bold: 'Times-New-Roman-Bold.ttf',
                italics: 'Times-New-Roman-Italics.ttf',
                bolditalics: 'Times-New-Roman-Italics.ttf'
            }
        };


       
        content.push({
            layout: 'noBorders',
            table: {
                widths: ['*', '*'],
                body: [
                    [{}, {text:[{text:'PEST MORTEM (INDIA) PRIVATE LIMITED \n',fontSize: 12},{text: 'CIN NO. : U24211MH1980PTC023338\n+91 22 24191400\nmumbai@pestmortem.com\nwww.pestmortem.com\n'+json.brachaddress+'\nState : ' + json.branchStatename + ', StateCode : ' + json.branchStateCode + '\nGST No : ' + json.brachgstnumber + ' \nPAN No : AACCP2255R\n\n',fontSize: 10,}],bold:1,alignment:'right',fonts: 'TimesNewRoman'}],
                ]
            }

        });
        
        var shipto = json.shipto;

        if (shipto!='') { //check Service Location not blank

          if (json.account != '') { //check account not blank

            if (json.cha != '') {//check Cha. not blank


              content.push({
                layout: 'noBorders',
                table: {
                    widths: [200, '*'],
                    body: [
                        [{text:'Tax Invoice',bold:1, margin: [0, 50, 0, 0],fillColor: '#000000',color: '#ffffff',fontSize: 30,alignment:'center',rowSpan: 8,fonts: 'TimesNewRoman'}, {text:'Bill To : ',margin: [10, 0, 0, 0],alignment:'left',fonts: 'TimesNewRoman'}],

                        [{}, {text:json.custName,bold:1,alignment:'left',margin: [12, 0, 0, 0],fontSize: 10,fonts: 'TimesNewRoman'}],

                        [{}, {text:json.custAddress,bold:1,alignment:'left',margin: [12, 0, 0, 0],fontSize: 9,fonts: 'TimesNewRoman'}],

                        [{}, {text:json.personName,bold:1,alignment:'left',margin: [12, 0, 0, 0],fontSize: 10,fonts: 'TimesNewRoman'}],

                        [{}, {text: 'Phone     : ' + json.custPhone,bold:1,alignment:'left',margin: [12, 0, 0, 0],fontSize: 10,fonts: 'TimesNewRoman'}],

                        [{}, {text: 'Mobile    : ' + json.custMobile,bold:1,alignment:'left',margin: [12, 0, 0, 0],fontSize: 10,fonts: 'TimesNewRoman'}],

                        [{}, {text: 'GSTIN     : ' + json.gstIn,bold:1,alignment:'left',margin: [12, 0, 0, 0],fontSize: 10,fonts: 'TimesNewRoman'}],

                        [{}, {text: 'email       : ' + json.email,bold:1,alignment:'left',margin: [12, 0, 0, 0],fontSize: 10,fonts: 'TimesNewRoman'}],

                        [{text:'Invoice No # ' + json.invoice_bill,heights: [ 10, 15 ],fontSize: 10,margin: [0, 15, 0, 5],fillColor: '#7f8c8d',color: '#ffffff',alignment:'center',rowSpan: 3,fonts: 'TimesNewRoman'}, {text: 'account  : ' + json.account,bold:1,alignment:'left',margin: [12, 0, 0, 0],fontSize: 10,fonts: 'TimesNewRoman'}],

                        [{}, {text: 'Cha          : ' + json.cha,bold:1,alignment:'left',margin: [12, 0, 0, 0],fontSize: 10,fonts: 'TimesNewRoman'}],

                        [{}, {text: 'Place Of Service : ' + json.shipto,bold:1,alignment:'left',margin: [12, 0, 0, 0],fontSize: 10,fonts: 'TimesNewRoman'}],


                    ]
                }

              });


            }else{//check Cha. blank

              content.push({
                layout: 'noBorders',
                table: {
                    widths: [200, '*'],
                    body: [
                        [{text:'Tax Invoice',bold:1, margin: [0, 40, 0, 0],fillColor: '#000000',color: '#ffffff',fontSize: 30,alignment:'center',rowSpan: 7,fonts: 'TimesNewRoman'}, {text:'Bill To : ',margin: [10, 0, 0, 0],alignment:'left',fonts: 'TimesNewRoman'}],

                        [{}, {text:json.custName,bold:1,alignment:'left',margin: [12, 0, 0, 0],fontSize: 10,fonts: 'TimesNewRoman'}],

                        [{}, {text:json.custAddress + ', State : ' + json.statename + ', State Code : ' + json.statecode,bold:1,alignment:'left',margin: [12, 0, 0, 0],fontSize: 9,fonts: 'TimesNewRoman'}],

                        [{}, {text:json.personName,bold:1,alignment:'left',margin: [12, 0, 0, 0],fontSize: 10,fonts: 'TimesNewRoman'}],

                        [{}, {text: 'Phone     : ' + json.custPhone,bold:1,alignment:'left',margin: [12, 0, 0, 0],fontSize: 10,fonts: 'TimesNewRoman'}],

                        [{}, {text: 'Mobile    : ' + json.custMobile,bold:1,alignment:'left',margin: [12, 0, 0, 0],fontSize: 10,fonts: 'TimesNewRoman'}],

                        [{}, {text: 'GSTIN     : ' + json.gstIn,bold:1,alignment:'left',margin: [12, 0, 0, 0],fontSize: 10,fonts: 'TimesNewRoman'}],

                        [{text:'Invoice No # ' + json.invoice_bill,fontSize: 10,margin: [0, 15, 0, 5],fillColor: '#7f8c8d',color: '#ffffff',alignment:'center',rowSpan: 3,fonts: 'TimesNewRoman'}, {text: 'email       : ' + json.email,bold:1,alignment:'left',margin: [12, 0, 0, 0],fontSize: 10,fonts: 'TimesNewRoman'}],

                        [{}, {text: 'account   : ' + json.account,bold:1,alignment:'left',margin: [12, 0, 0, 0],fontSize: 10,fonts: 'TimesNewRoman'}],
                        
                        [{}, {text: 'Place Of Service : ' + json.shipto,bold:1,alignment:'left',margin: [12, 0, 0, 0],fontSize: 10,fonts: 'TimesNewRoman'}],

                    ]
                }

              });


            }


          } else { //check account blank


            if (json.cha != '') { //check Cha. not blank

              content.push({
                layout: 'noBorders',
                table: {
                    widths: [200, '*'],
                    body: [
                        [{text:'Tax Invoice',bold:1, margin: [0, 40, 0, 0],fillColor: '#000000',color: '#ffffff',fontSize: 30,alignment:'center',rowSpan: 7,fonts: 'TimesNewRoman'}, {text:'Bill To : ',margin: [10, 0, 0, 0],alignment:'left',fonts: 'TimesNewRoman'}],

                        [{}, {text:json.custName,bold:1,alignment:'left',margin: [12, 0, 0, 0],fontSize: 10,fonts: 'TimesNewRoman'}],

                        [{}, {text:json.custAddress, bold:1,alignment:'left',margin: [12, 0, 0, 0],fontSize: 9,fonts: 'TimesNewRoman'}],

                        [{}, {text:json.personName,bold:1,alignment:'left',margin: [12, 0, 0, 0],fontSize: 10,fonts: 'TimesNewRoman'}],

                        [{}, {text: 'Phone     : ' + json.custPhone,bold:1,alignment:'left',margin: [12, 0, 0, 0],fontSize: 10,fonts: 'TimesNewRoman'}],

                        [{}, {text: 'Mobile    : ' + json.custMobile,bold:1,alignment:'left',margin: [12, 0, 0, 0],fontSize: 10,fonts: 'TimesNewRoman'}],

                        [{}, {text: 'GSTIN     : ' + json.gstIn,bold:1,alignment:'left',margin: [12, 0, 0, 0],fontSize: 10,fonts: 'TimesNewRoman'}],

                        [{text:'Invoice No # ' + json.invoice_bill,fontSize: 10,margin: [0, 15, 0, 5],fillColor: '#7f8c8d',color: '#ffffff',alignment:'center',rowSpan: 3,fonts: 'TimesNewRoman'}, {text: 'email       : ' + json.email,bold:1,alignment:'left',margin: [12, 0, 0, 0],fontSize: 10,fonts: 'TimesNewRoman'}],
                        
                        [{}, {text: 'Cha          : ' + json.cha,bold:1,alignment:'left',margin: [12, 0, 0, 0],fontSize: 10,fonts: 'TimesNewRoman'}],

                        [{}, {text: 'Place Of Service : ' + json.shipto,bold:1,alignment:'left',margin: [12, 0, 0, 0],fontSize: 10,fonts: 'TimesNewRoman'}],

                    ]
                }

              });


            } else { //check Cha. blank
              
              content.push({
                layout: 'noBorders',
                table: {
                    widths: [200,'*'],
                    body: [
                        [{text:'Tax Invoice',bold:1, margin: [0, 30, 0, 30],fillColor: '#000000',color: '#ffffff',fontSize: 30,alignment:'center',rowSpan: 6,fonts: 'TimesNewRoman'}, {text:'Bill To : ',margin: [10, 0, 0, 0],alignment:'left',fonts: 'TimesNewRoman'}],

                        [{}, {text:json.custName,bold:1,alignment:'left',margin: [12, 0, 0, 0],fontSize: 9,fonts: 'TimesNewRoman'}],

                        [{}, {text:json.custAddress,bold:1,alignment:'left',margin: [12, 0, 0, 0],fontSize: 10,fonts: 'TimesNewRoman'}],

                        [{}, {text:json.personName,bold:1,alignment:'left',margin: [12, 0, 0, 0],fontSize: 10,fonts: 'TimesNewRoman'}],

                        [{}, {text: 'Phone  : ' + json.custPhone,bold:1,alignment:'left',margin: [12, 0, 0, 0],fontSize: 10,fonts: 'TimesNewRoman'}],

                        [{}, {text: 'Mobile : ' + json.custMobile,bold:1,alignment:'left',margin: [12, 0, 0, 0],fontSize: 10,fonts: 'TimesNewRoman'}],

                        [{text:'Invoice No # ' + json.invoice_bill,fontSize: 10,margin: [0, 15, 0, 5],fillColor: '#7f8c8d',color: '#ffffff',alignment:'center',rowSpan: 3,fonts: 'TimesNewRoman'}, {text: 'GSTIN  : ' + json.gstIn,bold:1,alignment:'left',margin: [12, 0, 0, 0],fontSize: 10,fonts: 'TimesNewRoman'}],

                        [{}, {text: 'email   : ' + json.email,bold:1,alignment:'left',margin: [12, 0, 0, 0],fontSize: 10,fonts: 'TimesNewRoman'}],

                        [{}, {text: 'Place Of Service : ' + json.shipto,bold:1,alignment:'left',margin: [12, 0, 0, 0],fontSize: 10,fonts: 'TimesNewRoman'}],


                    ]
                }

              });


            }
            

          }  
        
        } else {

            //check Service Location is blank

            if (json.account != '') {

                //check account not blank

              if (json.cha != '') {
                  //check Cha. Not blank
                content.push({
                  layout: 'noBorders',
                  table: {
                      widths: [200, '*'],
                      body: [

                          [{text:'Tax Invoice',bold:1, margin: [0, 40, 0, 0],fillColor: '#000000',color: '#ffffff',fontSize: 30,alignment:'center',rowSpan: 7,fonts: 'TimesNewRoman'}, {text:'Bill To : ',margin: [10, 0, 0, 0],alignment:'left',fonts: 'TimesNewRoman'}],

                          [{}, {text:json.custName,bold:1,alignment:'left',margin: [12, 0, 0, 0],fontSize: 10,fonts: 'TimesNewRoman'}],

                          [{}, {text:json.custAddress,bold:1,alignment:'left',margin: [12, 0, 0, 0],fontSize: 9,fonts: 'TimesNewRoman'}],

                          [{}, {text:json.personName,bold:1,alignment:'left',margin: [12, 0, 0, 0],fontSize: 10,fonts: 'TimesNewRoman'}],

                          [{}, {text: 'Phone     : ' + json.custPhone,bold:1,alignment:'left',margin: [12, 0, 0, 0],fontSize: 10,fonts: 'TimesNewRoman'}],

                          [{}, {text: 'Mobile    : ' + json.custMobile,bold:1,alignment:'left',margin: [12, 0, 0, 0],fontSize: 10,fonts: 'TimesNewRoman'}],

                          [{}, {text: 'GSTIN     : ' + json.gstIn,bold:1,alignment:'left',margin: [12, 0, 0, 0],fontSize: 10,fonts: 'TimesNewRoman'}],

                          [{text:'Invoice No # ' + json.invoice_bill,fontSize: 10,margin: [0, 15, 0, 5],fillColor: '#7f8c8d',color: '#ffffff',alignment:'center',rowSpan: 3,fonts: 'TimesNewRoman'}, {text: 'email       : ' + json.email,bold:1,alignment:'left',margin: [12, 0, 0, 0],fontSize: 10,fonts: 'TimesNewRoman'}],
                        
                          [{}, {text: 'account  : ' + json.account,bold:1,alignment:'left',margin: [12, 0, 0, 0],fontSize: 10,fonts: 'TimesNewRoman'}],
                          
                          [{}, {text: 'Cha         : ' + json.cha,bold:1,alignment:'left',margin: [12, 0, 0, 0],fontSize: 10,fonts: 'TimesNewRoman'}],

                      ]
                  }

                });


              } else {

                //check Cha. blank

                content.push({
                  layout: 'noBorders',
                  table: {
                      widths: [200, '*'],
                      body: [
                          [{text:'Tax Invoice',bold:1, margin: [0, 30, 0, 30],fillColor: '#000000',color: '#ffffff',fontSize: 30,alignment:'center',rowSpan: 6,fonts: 'TimesNewRoman'}, {text:'Bill To : ',margin: [10, 0, 0, 0],alignment:'left',fonts: 'TimesNewRoman'}],

                          [{}, {text:json.custName,bold:1,alignment:'left',margin: [12, 0, 0, 0],fontSize: 10,fonts: 'TimesNewRoman'}],

                          [{}, {text:json.custAddress,bold:1,alignment:'left',margin: [12, 0, 0, 0],fontSize: 9,fonts: 'TimesNewRoman'}],

                          [{}, {text:json.personName,bold:1,alignment:'left',margin: [12, 0, 0, 0],fontSize: 10,fonts: 'TimesNewRoman'}],

                          [{}, {text: 'Phone    : ' + json.custPhone,bold:1,alignment:'left',margin: [12, 0, 0, 0],fontSize: 10,fonts: 'TimesNewRoman'}],

                          [{}, {text: 'Mobile   : ' + json.custMobile,bold:1,alignment:'left',margin: [12, 0, 0, 0],fontSize: 10,fonts: 'TimesNewRoman'}],

                          [{text:'Invoice No # ' + json.invoice_bill,fontSize: 10,margin: [0, 15, 0, 0],fillColor: '#7f8c8d',color: '#ffffff',alignment:'center',rowSpan: 3,fonts: 'TimesNewRoman'}, {text: 'GSTIN    : ' + json.gstIn,bold:1,alignment:'left',margin: [12, 0, 0, 0],fontSize: 10,fonts: 'TimesNewRoman'}],

                          [{}, {text: 'email      : ' + json.email,bold:1,alignment:'left',margin: [12, 0, 0, 0],fontSize: 10,fonts: 'TimesNewRoman'}],

                          [{}, {text: 'account  : ' + json.account,bold:1,alignment:'left',margin: [12, 0, 0, 0],fontSize: 10,fonts: 'TimesNewRoman'}],
                          
                      ]
                  }

                });

              }
            

            }  else {

              //check account blank

              if (json.cha != '') {

                //check Cha. Not blank

                content.push({
                  layout: 'noBorders',
                  table: {
                      widths: [200, '*'],
                      body: [
                          [{text:'Tax Invoice',bold:1, margin: [0, 30, 0, 30],fillColor: '#000000',color: '#ffffff',fontSize: 30,alignment:'center',rowSpan: 6,fonts: 'TimesNewRoman'}, {text:'Bill To : ',margin: [10, 0, 0, 0],alignment:'left',fonts: 'TimesNewRoman'}],

                          [{}, {text:json.custName,bold:1,alignment:'left',margin: [12, 0, 0, 0],fontSize: 10,fonts: 'TimesNewRoman'}],

                          [{}, {text:json.custAddress,bold:1,alignment:'left',margin: [12, 0, 0, 0],fontSize: 9,fonts: 'TimesNewRoman'}],

                          [{}, {text:json.personName,bold:1,alignment:'left',margin: [12, 0, 0, 0],fontSize: 10,fonts: 'TimesNewRoman'}],

                          [{}, {text: 'Phone    : ' + json.custPhone,bold:1,alignment:'left',margin: [12, 0, 0, 0],fontSize: 10,fonts: 'TimesNewRoman'}],

                          [{}, {text: 'Mobile   : ' + json.custMobile,bold:1,alignment:'left',margin: [12, 0, 0, 0],fontSize: 10,fonts: 'TimesNewRoman'}],

                          [{text:'Invoice No # ' + json.invoice_bill,fontSize: 10,margin: [0, 10, 0, 5],fillColor: '#7f8c8d',color: '#ffffff',alignment:'center',rowSpan: 3,fonts: 'TimesNewRoman'}, {text: 'GSTIN    : ' + json.gstIn,bold:1,alignment:'left',margin: [12, 0, 0, 0],fontSize: 10,fonts: 'TimesNewRoman'}],

                          [{}, {text: 'email      : ' + json.email,bold:1,alignment:'left',margin: [12, 0, 0, 0],fontSize: 10,fonts: 'TimesNewRoman'}],
                          
                          [{}, {text: 'Cha         : ' + json.cha,bold:1,alignment:'left',margin: [12, 0, 0, 0],fontSize: 10,fonts: 'TimesNewRoman'}],

                      ]
                  }

                });


              } else {

                //check Cha. blank

                content.push({
                  layout: 'noBorders',
                  table: {
                      widths: [200, '*'],
                      body: [
                          [{text:'Tax Invoice',bold:1, margin: [0, 30, 0, 30],fillColor: '#000000',color: '#ffffff',fontSize: 30,alignment:'center',rowSpan: 6,fonts: 'TimesNewRoman'}, {text:'Bill To : ',margin: [10, 0, 0, 0],alignment:'left',fonts: 'TimesNewRoman'}],

                          [{}, {text:json.custName,bold:1,alignment:'left',margin: [12, 0, 0, 0],fontSize: 10,fonts: 'TimesNewRoman'}],

                          [{}, {text:json.custAddress,bold:1,alignment:'left',margin: [12, 0, 0, 0],fontSize: 9,fonts: 'TimesNewRoman'}],

                          [{}, {text:json.personName,bold:1,alignment:'left',margin: [12, 0, 0, 0],fontSize: 10,fonts: 'TimesNewRoman'}],

                          [{}, {text: 'Phone  : ' + json.custPhone,bold:1,alignment:'left',margin: [12, 0, 0, 0],fontSize: 10,fonts: 'TimesNewRoman'}],

                          [{}, {text: 'Mobile : ' + json.custMobile,bold:1,alignment:'left',margin: [12, 0, 0, 0],fontSize: 10,fonts: 'TimesNewRoman'}],

                          [{text:'Invoice No # ' + json.invoice_bill,fontSize: 10,margin: [0, 10, 0, 5],fillColor: '#7f8c8d',color: '#ffffff',alignment:'center',rowSpan: 2,fonts: 'TimesNewRoman'}, {text: 'GSTIN  : ' + json.gstIn,bold:1,alignment:'left',margin: [12, 0, 0, 0],fontSize: 10,fonts: 'TimesNewRoman'}],

                          [{}, {text: 'email    : ' + json.email,bold:1,alignment:'left',margin: [12, 0, 0, 0],fontSize: 10,fonts: 'TimesNewRoman'}],

                      ]
                  }

                });

              }

            }
        }

        if (json.totalGst =="0.00") {
            content.push({
              layout: 'noBorders',
              text: '\nLUT No            :   ' + json.branchlutno,
              fontSize: 10,
              bold: 1,
              alignment: 'left',
              fonts: 'TimesNewRoman'
          });
        }
        content.push({
            layout: 'noBorders',
            text: '\nInvoice Date   :   '   +  convertDate(json.invoiceDate),
            fontSize: 10,
            bold: 1,
            alignment: 'left',
            fonts: 'TimesNewRoman'
        });
        content.push({
            layout: 'noBorders',
            text: '\nDue Date         :   '   +  convertDate(json.dueDate),
            fontSize: 10,
            bold: 1,
            alignment: 'left',
            fonts: 'TimesNewRoman'
        });

        content.push({
            text: '\n\n'
        });


        table1 = {
          widths: [],
          dontBreakRows: true,
          body: []
        };
        table1['widths'].push('auto', '*', 50, 'auto', 40, 'auto','auto','auto', 'auto', 50);
        table1['body'].push([{text:'#',border: [true, true, true, false],fillColor: '#dfe6e9',bold:1}, {text:'Item & Description',border: [true, true, true, false],fillColor: '#dfe6e9',bold:1,alignment:'center'},{text:'HSN/SAC',border: [true, true, true, false],fillColor: '#dfe6e9',bold:1,alignment:'center'}, {text:'Qty',border: [true, true, true, false],fillColor: '#dfe6e9',bold:1,alignment:'center'},{text:'Rate',border: [true, true, true, false],fillColor: '#dfe6e9',bold:1,alignment:'center'},{text:'Amount',border: [true, true, true, false],fillColor: '#dfe6e9',bold:1,alignment:'center'}, {text:'Tax Type',border: [true, true, true, false],fillColor: '#dfe6e9',bold:1,alignment:'center'},{text:'Tax Rate',border: [true, true, true, false],bold:1,fillColor: '#dfe6e9',alignment:'center'},{text:'Tax Amount',border: [true, true, true, false],bold:1,fillColor: '#dfe6e9',alignment:'center'}, {text:'Amount',border: [true, true, true, false],bold:1,fillColor: '#dfe6e9',alignment:'center'}]);

        var serviceJson = json.service_json;

        var couter = 1;

        for (var i in serviceJson) {
          var arr = [];
          if (serviceJson[i].igstper=='0') {
              
            arr.push(couter, {text:[{text:serviceJson[i].servicename +'\n'}, {text:serviceJson[i].itemDescription,fontSize: 8}]}, serviceJson[i].sac, {text:serviceJson[i].qty +'\n'+ serviceJson[i].qty1,alignment:'right'}, {text:serviceJson[i].rate,alignment:'right'},{text:serviceJson[i].amount,alignment:'right'},{text:'CGST \n SGST',alignment:'right'},{text: serviceJson[i].cgstper+'% \n'+serviceJson[i].sgstper+'%',alignment:'right'},{text: serviceJson[i].cgstamount+'\n'+serviceJson[i].sgstamount,alignment:'right'},{text:serviceJson[i].totalamount,alignment:'right'});

          } else {

            arr.push(couter, {text:[{text:serviceJson[i].servicename +'\n'}, {text:serviceJson[i].itemDescription,fontSize: 8}]}, serviceJson[i].sac, {text:serviceJson[i].qty +'\n'+ serviceJson[i].qty1,alignment:'right'}, {text:serviceJson[i].rate,alignment:'right'},{text:serviceJson[i].amount,alignment:'right'},{text:'IGST',alignment:'right'},{text: serviceJson[i].igstper+'%',alignment:'right'},{text: serviceJson[i].igstamount,alignment:'right'},{text:serviceJson[i].totalamount,alignment:'right'});
          }
          table1['body'].push(arr);
          couter++;

        }


            var tb4 = [];
            tb4.push({text: 'Total ', colSpan: 8,bold:1, alignment: 'left'}, '', '', '', '', '', '', '',{text: parseFloat(json.totalGst).toFixed(), bold: true, alignment: 'right'}, {text: parseFloat(json.netAmount).toFixed(), bold: true, alignment: 'right'});
            table1['body'].push(tb4);

            var tb5 = [];
            tb5.push({text: [{text:'Amount in Words : \n'},{text:convertNumberToWords(json.netAmount),margin: [12, 20, 0, 0]}],bold:1, colSpan: 10, alignment: 'left'}, '', '', '', '', '', '', '','','');
            table1['body'].push(tb5);

            content.push({
			        	style: 'tablfont',
                table: table1,
                layout: {
                hLineColor: function (i, node) {
                  return (i === 0 || i === node.table.body.length) ? 'gray' : 'gray';
                },
                vLineColor: function (i, node) {
                  return (i === 0 || i === node.table.widths.length) ? 'gray' : 'gray';
                },
              },
            });

            content.push({
                table: {
                    widths: [300, '*'],
                    body: [
                    [{text:'Bank Name     : '+ json.bankName,border: [true, false, false, false],alignment:'left',fontSize: 9,fonts: 'TimesNewRoman'}, {text:'For Pest Mortem(India) Pvt. Ltd.',border: [false, false, true, false],fontSize: 9,bold:1,alignment:'right'}],
                    [{text:'Branch Name  : '+ json.branchName,border: [true, false, false, false],alignment:'left',fontSize: 9,fonts: 'TimesNewRoman'}, {text:'',border: [false, false, true, false]}],
                    [{text:'IFSC Code       : '+ json.ifscCode,border: [true, false, false, false],alignment:'left',fontSize: 9,fonts: 'TimesNewRoman'}, {text:'',border: [false, false, true, false]}],
                    [{text:'Account No     : '+ json.accountNo,border: [true, false, false, true],alignment:'left',fontSize: 9,fonts: 'TimesNewRoman'}, {text:'Authorised Signatory',border: [false, false, true, true],fontSize: 9,bold:1,alignment:'right'}],

                    ]
                    
                },layout: {
                        hLineColor: function (i, node) {
                        return (i === 0 || i === node.table.body.length) ? 'gray' : 'gray';
                        },
                        vLineColor: function (i, node) {
                        return (i === 0 || i === node.table.widths.length) ? 'gray' : 'gray';
                        },
                    }

            });


        dd = {

          pageSize: 'A4',

          pageOrientation: 'portrait',

          pageMargins: [40, 30, 30, 35],

          footer: function (currentPage, pageCount) {

            return {

              margin: 10,

              columns: [{

                fontSize: 9,

                text: [{

                    text: '--------------------------------------------------------------------------' +
                      '\n',
                    margin: [0, 20]

                  },
                  {

                    text: '© Pest Mortem (India) PVT. LTD. | G-2, Sunder Tower, T. J. Road, Sewree (West), Mumbai,400015 | PAGE ' + currentPage.toString() + ' of ' + pageCount,

                  }

                ],

                alignment: 'center'

              }]


            };

          },

          content,
          styles: {

            tablfont: {

              fontSize: 9,
              fonts: 'TimesNewRoman'
            }

          }

        }

        var win = window.open('', '_blank');
        pdfMake.createPdf(dd).open({}, win);


      }

    }

  })

}






function TriggerOutlook(e) {

  var email        = $(e).attr('data-emailId');
  var invoice_bill = $(e).attr('data-invoice_bill');
  var branchId     = $(e).attr('data-branchid');
  var category     = $(e).attr('data-category');


  var valid = true;

  var regemail = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;

  if (!regemail.test(email)) {

    valid = valid * false;

    alert('Pls Check Customer Email id');

  } else {

    valid = valid * true;

  }

  

   // var body = href = "http://pestmorten.local/process/invoice/email.php?invoice_bill=" + invoice_bill;
    var body = href = "http://pestmortem.co.in/process/invoice/email.php?invoice_bill=" + invoice_bill;

  var subject = "Pest Mortem (India) Pvt. Ltd.";
  var TO      = email;

  if (valid) {

    window.location.href = "mailto: " + TO + "?body=Pls Click Link And Download This Purchase Order To Given Below %0D%0A%0D%0A" + body + "&subject=" + subject;

    window.location.reload();

  }
  
}

</script>