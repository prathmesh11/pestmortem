<?php

    $base    = '../../';

    if($_GET['category']){

        $category = $_GET['category'];

    }

    if ($category == 'proformaBill') {

        $navenq4 = 'background:#1B1464;';

    }


    include('header.php');

    if (!in_array('Proforma Bill Invoice', $arrysession)) {
   
        echo "<script>window.location.href='/process/dashboard.php';</script>";
       
        exit;
  
    }

    if (session_status() == PHP_SESSION_NONE) { session_start(); }
    $sessionby = $_SESSION['employeeid'];
    $branchid  = $_SESSION['branchid'];

    $branchjson=json_decode(mysqli_fetch_assoc(mysqli_query($con,"SELECT branchjson x FROM branchmaster WHERE branchid='$branchid'"))['x']);

    

    if($_GET['customerid']){

        $customerid = $_GET['customerid'];

    }

    if($_GET['edit']){

        $edit = $_GET['edit'];

    }

    if($_GET['copy']){

        $copy = $_GET['copy'];

    }

    if ($copy == 'true') {


      $branchshortname = mysqli_fetch_assoc(mysqli_query($con,"SELECT branchshortname x FROM branchmaster WHERE branchid='$branchid'"))['x'];

      $year1 = 0;
      $year2 = 0;

      if ( date('m') > 3 ) {

          $year1 = date('y');

          $year2 = date('y')+1;

      } else {

          $year1 = date('y')-1;

          $year2 = date('y');
      }

      if ( date('m') > 6 ) { //financial year

        $year111 = date('Y');

        $year11 = date('Y') + 1;

      } else {
        $year11 = date('Y') - 1;

        $year111 = date('Y');
      }

      $first  = $year11.'-03-31'; 
      
      $last = $year111.'-04-01'; 


      $sum = mysqli_fetch_assoc(mysqli_query($con,"SELECT COUNT(invoice_bill)+45 x FROM `billing` WHERE invoice_bill<>'' AND '$today' BETWEEN '$first' AND '$last' AND category='proformaBill' "))['x'];

    //   $sum = $invoice_bill1 + 1;

      $invoice_bill = $branchshortname.'/PF/'.$sum.'/'.$year1.'-'.$year2;

    } else if ($edit == 'true') {

        if($_GET['invoice_bill']){

            $invoice_bill = $_GET['invoice_bill'];
    
        }

    } else {
        
        $branchshortname = mysqli_fetch_assoc(mysqli_query($con,"SELECT branchshortname x FROM branchmaster WHERE branchid='$branchid'"))['x'];

      $year1 = 0;
      $year2 = 0;

      if ( date('m') > 3 ) {

          $year1 = date('y');

          $year2 = date('y')+1;

      } else {

          $year1 = date('y')-1;

          $year2 = date('y');
      }

      if ( date('m') > 6 ) { //financial year

        $year111 = date('Y');

        $year11 = date('Y') + 1;

      } else {
        $year11 = date('Y') - 1;

        $year111 = date('Y');
      }

      $first  = $year11.'-03-31'; 
      
      $last = $year111.'-04-01'; 


      $invoice_bill = mysqli_fetch_assoc(mysqli_query($con,"SELECT COUNT(invoice_bill) x FROM `billing` WHERE invoice_bill<>'' AND '$today' BETWEEN '$first' AND '$last' AND category='proformaBill' "))['x'];

      if($invoice_bill!=0){

        $invoice_bill = mysqli_fetch_assoc(mysqli_query($con,"SELECT COUNT(invoice_bill)+45 x FROM `billing` WHERE invoice_bill<>'' AND '$today' BETWEEN '$first' AND '$last' AND category='proformaBill' "))['x'];
    
        $invoice_bill = $branchshortname.'/PF/'.$invoice_bill.'/'.$year1.'-'.$year2;
    
      } else {
    
        $invoice_bill = $branchshortname.'/PF/45/'.$year1.'-'.$year2;
    
      }

    }
                    
    
    
   
?>

    <style>
    
    .table-bordered>tbody>tr>td, .table-bordered>tbody>tr>th, .table-bordered>tfoot>tr>td, .table-bordered>tfoot>tr>th, .table-bordered>thead>tr>td, .table-bordered>thead>tr>th {
        border: 2px solid #000000;
    }

    .form-control {

        font-weight: bold;
        
    }

    .table-list td,
	.table-list th {
		border: 1px solid #ddd;
		padding: 1px !important;
		font-size: 13px;
	}
	.table-list td{
		padding-top: 2px !important;
		margin:0;

	}

	.table-list tr:nth-child(even) {
		background-color: #f2f2f2;
	}

	.table-list th {
		padding-top: 5px;
		padding-bottom: 5px;
		text-align: center;
		background-color: #16a085;
		color: white;
	}
    </style>

    <br>

    <input type="hidden" value="<?php echo $customerid;?>" id="customerId">

    <input type="hidden" value="<?php echo $branchid;?>" id="branchId">

    <input type="hidden" value="<?php echo $_GET['invoice_bill'];?>" id="copyinvoiceBill">

    <input type="hidden" value="<?php echo $invoice_bill;?>" id="invoice_bill">

    <input type="hidden" value="<?php echo $edit;?>" id="edit">

    <input type="hidden" value="<?php echo $copy;?>" id="copy">

    <input type="hidden" value="<?php echo $_GET['category']?>" id="category">



    <div class="col-sm-1"></div>

    <div class="col-sm-10" style="background:#ddd;">

        <div class="row">

        
            <div class="col-sm-4" style="padding-top:5px;">
                
                <img src="<?php echo $base;?>img/PestMortemLogo.jpg" alt="No-imag" class='img-responsive img-thumbnail'>

            </div>

            <div class="col-sm-8" align="center">
                
                <h3><b>PEST MORTEM (INDIA) PRIVATE LIMITED</b></h3>

                <h6><b>(Approved by National Plant Protection Organization, Government of India)</b></h6>

                <h6><b>AN ISO 9001 : 2008 CERTIFIED COMPANY</b></h6>

                <h5><b><?php echo get_object_vars($branchjson)['address']; ?></b></h5>

                <h5><b><?php echo 'GST NO : ' .get_object_vars($branchjson)['gstnumber']; ?></b></h5>

            </div>

            <div class="col-sm-12" align="center">
                
                <h4><b>TAX INVOICE</b></h4>

                <h6><b>PEST CONTROL SERVICES AND FUMIGATION OF EXPORT CARGO, SHIPS, CONTAINERS</b></h6>

                <hr style="border:1px solid #000;">

            </div>

            <div class="row" id="pestBill">

                <div class="col-sm-12" >

                    <label class="control-label" style="margin-left:16px;"><b>Invoice No : <?php echo $invoice_bill;?> </b></label>

                </div> <br>

                <div class="form-group col-sm-6">

                    <label class="control-label col-sm-5" for="email"><b>Invoice Date: </b></label>

                    <div class="col-sm-7">
                        
                        <input type="date"  data-role="date" id="invoiceDate" data-name="invoiceDate" class="form-control" onchange="duedate()">

                    </div>

                </div>

                <div class="form-group col-sm-6">

                    <label class="control-label col-sm-5" for="email"><b>Due Date : </b></label>

                    <div class="col-sm-7">
                        
                        <input type="date" id="dueDate" data-role="date" data-name="dueDate" class="form-control">

                    </div>

                </div>
            
            </div>


            <div class="table-responsive col-sm-12" id="pestBill" style="margin-top:10px;">          
                
                <table class="table table-bordered">

                    <thead>

                        <tr>

                            <th style="border-top: 2px solid #000;">Bill To</th>
                            <th style="border-top: 2px solid #000;">Place Of Supply</th>

                        </tr>

                    </thead>

                    <tbody>

                        <tr>

                            <td>

                                <div class="form-group form-horizontal">

                                    <label class="control-label col-sm-4" for="name"><b>Name : </b></label>

                                    <div class="col-sm-8">

                                        <select class="form-control input-sm select-js1" id="custName" onchange="custDetails(this)" data-name="custName">

                                        <option value="Select">Select Customername</option>

                                        <?php

                                            $account = mysqli_query($con,"SELECT customerid,customername FROM customermaster WHERE branchid='$branchid' ORDER BY customername");

                                                while ($row = mysqli_fetch_assoc($account)) {

                                                    echo '<option value="'.$row['customerid'].'">'.$row['customername'].'</option>';

                                                }
                                        
                                        ?>

                                        </select>

                                    </div>

                                </div>

                            </td>

                            <td>
                            
                                <div class="form-group">

                                    <textarea type="text" data-name="shipto" onkeyup="uppercase(this)" onchange="removeSpecialChar(this)" class="form-control" id="shipto"></textarea>

                                </div>

                            </td>

                        </tr>

                        <tr>

                            <td>

                                <div class="form-group form-horizontal">

                                    <label class="control-label col-sm-4" for="name"><b>Address :</b></label>

                                    <div class="col-sm-8">

                                        <textarea type="text" data-name="custAddress" id="custAddress" onkeyup="uppercase(this)" data-role="text" class="form-control"></textarea>

                                    </div>

                                </div>

                            </td>


                            <td>
                            
                                <div class="form-group">

                                    <label for="name"><b>Add Info :</b></label>

                                    <textarea type="text" data-name="emptybox" onkeyup="uppercase(this)" onchange="removeSpecialChar(this)"  id="emptybox" class="form-control" ></textarea>

                                </div>

                            </td>


                        </tr>

                        <tr>

                            <td>

                                <div class="form-group form-horizontal">

                                    <label class="control-label col-sm-4" for="name"><b>Name Of Person :</b></label>

                                    <div class="col-sm-8">

                                        <input type="text" data-name="personName" onkeyup="uppercase(this)" id="personName" data-role="text" class="form-control">

                                    </div>

                                </div>

                            </td>

                             <td>
                            
                                <div class="form-group">

                                    <label for="name"><b>Notes</b></label>

                                    <textarea type="text" data-name="notes" onkeyup="uppercase(this)" onchange="removeSpecialChar(this)"  id="notes" class="form-control" ></textarea>

                                </div>

                            </td>


                        </tr>

                        <tr>

                            <td>

                                <div class="form-group form-horizontal">

                                    <label class="control-label col-sm-4" for="name"><b>Phone :</b></label>

                                    <div class="col-sm-8">

                                        <input type="text" data-name="custPhone" id="custPhone" class="form-control">

                                    </div>

                                </div>

                            </td>

                            <td>
                            
                                <div class="form-group">

                                    <label for="name"><b>Addtional term</b></label>

                                    <textarea type="text" onkeyup="uppercase(this)" onchange="removeSpecialChar(this)" data-name="addTerm"  id="addTerm" class="form-control" ></textarea>

                                </div>

                            </td>

                        </tr>

                        <tr>

                            <td>

                                <div class="form-group form-horizontal">

                                    <label class="control-label col-sm-4" for="name"><b>Mobile :</b></label>

                                    <div class="col-sm-8">

                                        <input type="text" data-name="custMobile" id="custMobile" data-role="text" class="form-control" >

                                    </div>

                                </div>

                            </td>

                            <td>
                            
                                <div class="form-group">

                                    <label for="name"><b>Account</b></label>

                                    <input type="accountName" data-name="accountName" onkeyup="uppercase(this)" onchange="removeSpecialChar(this)" id="accountName" class="form-control" ></textarea>

                                </div>

                            </td>

                        </tr>

                        <tr>

                            <td>

                                <div class="form-group form-horizontal">

                                    <label class="control-label col-sm-4" for="name"><b>GSTIN :</b></label>

                                    <div class="col-sm-8">

                                        <input type="text" data-name="gstIn" id="gstIn" data-role="" class="form-control">

                                    </div>

                                </div>

                            </td>

                            <td rowspan="4">
                            
                                <div class="form-group">

                                    <label for="name"><b>Sales Person</b></label>
                                    <br>
                                    <select class="form-control input-sm select-js1" id="salesPersonName" data-name="sales_person_name">

                                        <option value="Select">Select Sales Person</option>

                                        <?php

                                            $account = mysqli_query($con,"SELECT id,sales_person_name FROM salesperson WHERE branch_id='$branchid' ORDER BY sales_person_name");

                                            while ($row = mysqli_fetch_assoc($account)) {

                                                echo '<option value="'.$row['id'].'">'.$row['sales_person_name'].'</option>';

                                            }
                                            
                                        ?>

                                    </select>

                                </div>

                            </td>

                        </tr>

                        <tr>

                            <td>

                                <div class="form-group form-horizontal">

                                    <label class="control-label col-sm-4" for="name"><b>email :</b></label>

                                    <div class="col-sm-8">

                                        <input type="text" data-name="email" id="email" data-role="text" class="form-control">

                                    </div>

                                </div>

                            </td>

                        </tr>

                        <tr>

                            <td>

                                <div class="form-group form-horizontal">

                                    <label class="control-label col-sm-4" for="name"><b>Consinee Name :</b></label>

                                    <div class="col-sm-8">

                                    <select class="form-control input-sm select-js1" id="account" data-name="account">

                                        <option value="Select">Select Consinee Name</option>

                                        <?php

                                            $account = mysqli_query($con,"SELECT customerid,customername FROM customermaster WHERE branchid='$branchid' ORDER BY customername");

                                                while ($row = mysqli_fetch_assoc($account)) {

                                                    echo '<option value="'.$row['customerid'].'">'.$row['customername'].'</option>';

                                                }
                                        
                                        ?>

                                        </select>

                                    </div>

                                </div>

                            </td>

                        </tr>

                        <tr>

                            <td>

                                <div class="form-group form-horizontal">

                                    <label class="control-label col-sm-4" for="name"><b>Cha :</b></label>

                                    <div class="col-sm-8">

                                        <select class="form-control" id="cha" data-name="cha">

                                            <option value="Select">Select Cha</option>

                                        </select>

                                    </div>

                                </div>

                            </td>

                        </tr>

                    </tbody>

                </table>

            </div>



            <hr style="border:1px solid #000; width:95%;">
            
            <div class="col-sm-12" style="margin-top: 10px;">

                <table class="table table-list form-group" id="table-order">

                    <thead>

                        <th>#</th>

                        <th>Item & Description</th>

                        <th style="width:8%">HSN/SAC</th>

                        <th style="width:8%">Qty</th>

                        <th style="width:8%">Rate</th>

                        <th style="width:8%">Amount</th>

                        <th style="width:8%">CGST</th>

                        <th style="width:8%">SGST</th>

                        <th style="width:8%">IGST</th>

                        <th style="width:8%">Total Amount</th>

                        <th>Action</th>

                    </thead>

                    <tbody>
                        <tr>
                            
                            <td></td>

                            <td>

                                <textarea data-name="itemDescription" id="itemDescription" style="text-transform: uppercase" onchange="removeSpecialChar(this)" rows="4" cols="50" data-role="text" class="form-control input-sm" > </textarea>

                            </td>

                            <td>

                                <input data-name="sac" class="form-control input-sm" data-role="text" id="sac" type="text" />

                            </td>

                            <td>

                                <input data-name="qty" class="form-control input-sm" data-role="number" id="qty" type="number" onkeyup="totalamt()"/>

                                <input data-name="qty1" class="form-control input-sm" data-role="text" id="qty1" type="text" onkeyup="uppercase(this)" Placeholder="unit"/>

                            </td>

                            <td>

                                <textarea data-name="rate"  data-role="number" id="rate" class="form-control input-sm" type="number" onkeyup="totalamt()"></textarea>

                            </td>

                            <td>

                                <textarea data-name="amount"  data-role="number" id="amount" class="form-control input-sm" type="number" readonly ></textarea>

                            </td>

                            <td>

                                <input data-name="cgstper" data-role="number" id="cgstper" class="form-control input-sm" onkeyup="cgstrate()" type="number" />

                                <input data-name="cgstamount" data-role="number" id="cgstamount" class="form-control input-sm" type="number" readonly />

                            </td>

                            <td>

                                <input data-name="sgstper" data-role="number" id="sgstper" class="form-control input-sm" onkeyup="sgstrate()" type="number" />

                                <input data-name="sgstamount" data-role="number" id="sgstamount" class="form-control input-sm" type="number" readonly />

                            </td>

                            <td>

                                <input data-name="igstper" data-role="number" id="igstper" class="form-control input-sm" onkeyup="igstrate()" type="number" />

                                <input data-name="igstamount" data-role="number" id="igstamount" class="form-control input-sm" type="number" readonly />

                            </td>

                            <td>

                                <input data-name="totalamount" data-role="text" id="totalamount" class="form-control input-sm" type="text" readonly />

                            </td>

                            <td>

                                <button class="btn btn-sm btn-primary" id="btn-add" data-edit="" onclick="add(this)">Add</button>

                            </td>

                            

                        </tr>

                    </tbody>

                </table>

            </div>

            <div class="col-sm-12" id="pestBill">

                <table class="table table-bordered"  style="border: 2px solid #000;">

                    <tr>

                        <th style="text-align: center;font-weight:800;" colspan="9">PAYMENT DETAILS </th>

                    </tr>

                    <tr>
                        <td rowspan="7">

                            <div class="form-group form-horizontal">

                                <label class="control-label col-sm-4" for="name"><b>Total In Words :</b></label>

                                <div class="col-sm-8">

                                    <input type="text" data-role="text" data-name="totalWords" onchange="uppercase(this)" class="form-control input-sm" id="totalWords" readonly/>

                                </div>

                            </div>

                            <div class="form-group form-horizontal">

                                <label class="control-label col-sm-4" for="name"><b>Select Currency :</b></label>

                                <div class="col-sm-8">

                                    <select class="form-control" id="currency" data-role="select" data-name="currency">
                                    
                                        <option value="Select">Select</option>
                                        <option value="Rs">Rs</option>
                                        <option value="USD">USD</option>

                                    </select>

                                </div>

                            </div>

                             <div class="form-group form-horizontal">

                                <label class="control-label col-sm-4" for="name"><b>Select Format :</b></label>

                                <div class="col-sm-8">

                                    <select class="form-control" id="format" data-role="select" data-name="format">
                                    
                                        <option value="Select">Select</option>
                                        <option value="Cash Memo">Cash Memo</option>
                                        <option value="Proforma Invoice">Proforma Invoice</option>
                                        <option value="Bill Of Supply">Bill Of Supply</option>

                                    </select>

                                </div>

                            </div>
                                
                        </td>

                            <td colspan="2" style="text-align:center;"><b> SERVICE CHARGES </b></td>

                    </tr>

                    <tr>
                        

                        <td>

                            <b>Sub Total</b>

                        </td>

                        <td>

                            <input type="number" data-role="number" data-name="subTotal" class="form-control input-sm" id="subTotal"  readonly/>
                            
                        </td>

                    </tr>

                    <tr>
                        

                            <td>

                                <b>CGST AMT</b>

                            </td>

                            <td>

                                <input type="number" data-role="number" data-name="cgstAmt" class="form-control input-sm" id="cgstAmt"  readonly/>
                                
                            </td>

                    </tr>

                    <tr>
                        

                        <td>

                            <b>SGST AMT</b>

                        </td>

                        <td>

                            <input type="number" data-role="number" data-name="sgstAmt" class="form-control input-sm" id="sgstAmt"  readonly/>
                            
                        </td>

                    </tr>

                    <tr>
                        

                        <td>

                            <b>IGST AMT</b>

                        </td>

                        <td>

                            <input type="number" data-role="number" data-name="igstAmt" class="form-control input-sm" id="igstAmt"  readonly/>
                            
                        </td>

                    </tr>

                    <tr>

                        <td>

                            <div class="form-group form-horizontal">

                                <label class="control-label" for="name"><b>Round Off </b></label>

                            </div>

                        </td>

                        <td>

                            <input type="number" data-role="number" data-name="roundOff" class="form-control input-sm" id="roundOff"  readonly/>
                            
                        </td>

                    </tr>


                    <tr>

                        <td>

                            <div class="form-group form-horizontal">

                                <label class="control-label" for="name"><b>Net Amount </b></label>

                            </div>

                        </td>

                        <td>

                            <input type="number" data-role="number" data-name="netAmount" class="form-control input-sm" id="netAmount"  readonly/>
                            
                        </td>

                    </tr>


                </table> 
            
            </div>

            <hr style="border:1px solid #000; width:98%;">

            <table class="table table-bordered" style="border: 2px solid #000;">

                <tr>

                    <th style="text-align: center;font-weight:800;" colspan="9">BANK DETAILS </th>

                </tr>

            </table>

            <div class="col-sm-6" id="pestBill"> 

                <div class="form-group form-horizontal">

                    <label class="control-label col-sm-4" for="name"><b>Bank Name : </b></label>

                    <div class="col-sm-8">

                        <input type="text" data-role="text" data-name="bankName" class="form-control input-sm" value="<?php echo get_object_vars($branchjson)['bankname']; ?>"/>

                    </div>

                </div>

                <div class="form-group form-horizontal">

                    <label class="control-label col-sm-4" for="name"><b>Branch Name : </b></label>

                    <div class="col-sm-8">

                        <input type="text" data-role="text" data-name="branchName" class="form-control input-sm" value="<?php echo get_object_vars($branchjson)['bankbranchname']; ?>"/>

                    </div>

                </div>
            
            </div>

            <div class="col-sm-6" id="pestBill"> 

                <div class="form-group form-horizontal">

                    <label class="control-label col-sm-4" for="name"><b>IFSC Code  : </b></label>

                    <div class="col-sm-8">

                        <input type="text" data-role="text" data-name="ifscCode" class="form-control input-sm" value="<?php echo get_object_vars($branchjson)['ifsccode']; ?>"/>

                    </div>

                </div>

                <div class="form-group form-horizontal">

                    <label class="control-label col-sm-4" for="name"><b>Account No : </b></label>

                    <div class="col-sm-8">

                        <input type="text" data-role="text" data-name="accountNo" class="form-control input-sm" value="<?php echo get_object_vars($branchjson)['accountno']; ?>"/>

                    </div>

                </div>
            
            </div>

        </div>

    </div>

    <button class="btn btn-lg btn-primary" onclick="saveBill();" id="savecertificate" style="border-radius:0px;position:fixed;bottom:10px;right:10px;">Save Bill</button>


<?php 
include('footer.php');
?>

<script>

$(function () {
    $('#itemDescription').keyup(function (e) {
        if (e.keyCode == 13) {
            var curr = getCaret(this);
            var val = $(this).val();
            var end = val.length;

            $(this).val(val.substr(0, curr) + '^' + val.substr(curr, end));
        }

    })
});

function getCaret(el) {
    if (el.selectionStart) {
        return el.selectionStart;
    } else if (document.selection) {
        el.focus();

        var r = document.selection.createRange();
        if (r == null) {
            return 0;
        }

        var re = el.createTextRange(),
            rc = re.duplicate();
        re.moveToBookmark(r.getBookmark());
        rc.setEndPoint('EndToStart', re);

        return rc.text.length;
    }
    return 0;
}

function removeSpecialChar(e) {

    let removeChar = $(e).val();

    let regExpr = /[`~!#$&*_|+=?;:'"<>\{\}\[\]]/gi;

    if (!regExpr.test(removeChar)==false)   {


        alert('Special Characters Not Allow');

        $(e).val(removeChar.replace(regExpr, ""));


    }

}



<?php 
        $valid = 'true';
        if(isset($_GET['edit'])){
            $valid ='false';
        } 
        if(isset($_GET['copy'])){
            $valid ='false';
        }
?>
    var valid = <?php echo $valid; ?>;

function custDetails(e){

    if(valid){


        $.ajax({

        type: "POST",

        data: "custName=" + $(e).val() + "&branchid=" +$('#branchId').val(),

        url: 'api/custDetails.php',

        cache: false,

        success: function (res) {

            if (res.status == 'success') {

                $('#custAddress').val(res.customeradd).trigger("onkeyup");

                $('#personName').val(res.name).trigger("onkeyup");

                $('#custPhone').val(res.landline);

                $('#custMobile').val(res.mobile);

                $('#gstIn').val(res.gst);

                $('#email').val(res.email);

                if (res.gstOnBill == 'Yes') {

                    $('#gstIn').attr('data-role' , 'text');

                } else {

                    $('#gstIn').attr('data-role' , '');
                    $('#gstIn').css('border-bottom' , 'none');

                }

                let chajson = res.chajson;

                var str = '<option value="Select">Select Cha</option>';

                for (var i in chajson) {

                    str += '<option value="' + chajson[i].cha + '">' + chajson[i].cha + '</option>';

                }

                if (res.currency == 'RUPEES' || res.currency == 'INR' || res.currency == 'Rs' || res.currency == 'rs' || res.currency == 'RS') {

                    $('#currency').val('Rs');
                    
                } else if (res.currency == 'USD' || res.currency == 'usd' || res.currency == 'DOLLER' || res.currency == 'Doller' ){

                    $('#currency').val('USD');

                } else {
                    
                    $('#currency').val('Rs');

                }
                
                $('#cha').html(str);

                duedate();

            }


        }

    });
}else{
        valid=true;
    }

}


function duedate() {

    let invoiceDate = $('#invoiceDate').val();

    let customerId = $('#custName').val();

    if (invoiceDate != "") {

        $.ajax({

            type: "POST",

            data: "invoiceDate=" + invoiceDate + "&customerId="+customerId,

            url: 'api/duedate.php',

            cache: false,

            success: function (res) {

                if (res.status == 'success') {

                    $('#dueDate').val(res.dueDate);

                }


            }

        });

    } else {

        $('#dueDate').val("");

    }

}


function totalamt() {

    let qty = $('#qty').val();
    let rate = $('#rate').val();
    let amount = 0;
    amount = parseFloat(qty) * parseFloat(rate);
    console.log(amount);

    // if (isNaN(amount)) {

    //     amount = 0;

    // }

    $('#amount').val(amount.round(2));

    totalamount();
    cgstrate();
    sgstrate();
    igstrate();
    totalamount();
    //calculation();
}

function checkbox(){
    var valcgstper=$('#cgstper').val();
    var valsgstper=$('#sgstper').val();
    var valigstper=$('#igstper').val();
    if(valcgstper!='' && valsgstper!='' && valigstper==''){
        $('#cgstper').prop('readonly',false);
        $('#sgstper').prop('readonly',false);

        $('#igstper').val(0);
        $('#igstper').prop('readonly',true);

        
    } 
    if(valcgstper=='' && valsgstper=='' && valigstper!=''){
        $('#cgstper').val(0);
        $('#sgstper').val(0);
        $('#cgstper').prop('readonly',true);
        $('#sgstper').prop('readonly',true);
        $('#igstper').prop('readonly',false);
    }

}

function cgstrate() {
    checkbox();

    var cgstper = parseFloat($('#cgstper').val());
    var amounts = parseFloat($('#amount').val());
    var cgstamt = (amounts * cgstper) / 100;

    if (isNaN(cgstamt)) {

        cgstamt = 0;

    }
    $('#cgstamount').val(cgstamt.round(2));
    totalamount();

    //  calculation();
}

function sgstrate() {
    checkbox();

    var sgstper = parseFloat($('#sgstper').val());
    var amounts = parseFloat($('#amount').val());
    var sgstamt = (amounts * sgstper) / 100;
    if (isNaN(sgstamt)) {

        sgstamt = 0;

    }
    $('#sgstamount').val(sgstamt.round(2));
    totalamount();

    //  calculation();
}

function igstrate() {
    checkbox();

    var igstper = parseFloat($('#igstper').val());
    var amounts = parseFloat($('#amount').val());
    var igstamt = (amounts * igstper) / 100;
    if (isNaN(igstamt)) {

        igstamt = 0;

    }
    $('#igstamount').val(igstamt.round(2));
    totalamount();

    //  calculation();
}

function totalamount() {

    var cgstamount = parseFloat($('#cgstamount').val());
    var sgstamount = parseFloat($('#sgstamount').val());

    var igstamount = parseFloat($('#igstamount').val());

    var amounts = parseFloat($('#amount').val());
    var totalamount = 0;
    if (isNaN(cgstamount)) {

        cgstamount = 0;

    }
    if (isNaN(sgstamount)) {

        sgstamount = 0;

    }
    if (isNaN(igstamount)) {

        igstamount = 0;

    }
    var totalamount = amounts + cgstamount + sgstamount + igstamount;

    
    

    $('#totalamount').val(totalamount.round(2));
    //  calculation();
}


// add function start :
function add(e) {

    uppercasebyid('itemDescription');

	var chk = $(e).data('edit');
	var data = checker('table-order');
    var itemDescription = data.itemDescription;
	var sac = data.sac;
	var qty = data.qty;
    var qty1 = data.qty1;
	var rate = data.rate;
    var amount = data.amount;
	var cgstper = data.cgstper;
    var sgstper = data.sgstper;
	var igstper = data.igstper;
    var cgstamount = data.cgstamount;
    var sgstamount = data.sgstamount;
	var igstamount = data.igstamount;
    var totalamount = data.totalamount;


	var valid = true;

	if (checker('table-order') != false) {
		valid = valid * true;
	} else {
		valid = valid * false;
	}

	// $('#table-order .itemDescription').each(function () {
	// 	var itemDescription1 = $(this).text().trim();
	// 	if (itemDescription1 == itemDescription && chk == '') {
	// 		valid = valid * false;
	// 		alert('Dublicate Name');
	// 	}
	// });
	if (valid) {
		var chk = $(e).data('edit');
		if (chk == '') {
			var len = $('#table-order .srno').length + 1;
			fun_adder(len,itemDescription,sac,qty,qty1,rate,amount,cgstper,cgstamount,sgstper,sgstamount,igstper,igstamount,totalamount);
		} else {
			fun_adder_edit(chk,itemDescription,sac,qty,qty1,rate,amount,cgstper,cgstamount,sgstper,sgstamount,igstper,igstamount,totalamount);
		}
		modals.clear('table-order');
		$('#btn-add').data('edit', '');
	}
    //calculation();

}

// add function end :

// fun_adder function start :
function fun_adder(len,itemDescription,sac,qty,qty1,rate,amount,cgstper,cgstamount,sgstper,sgstamount,igstper,igstamount,totalamount) {
		var str = '<tr>';
		str += '<td align="center" class="srno">' + len + '</td>';
        str += '<td align="center" class="itemDescription">' + itemDescription + '</td>';
        str += '<td align="center" class="sac">' + sac + '</td>';
        str += '<td align="center">' + qty +  '<br>' +  qty1 +'</td>';
		str += '<td align="center" class="qty" style="display:none;">' + qty + '</td>';
        str += '<td align="center" class="qty1" style="display:none;">' + qty1 + '</td>';
		str += '<td align="center" class="rate">' + rate + '</td>';
        str += '<td align="center" class="amount">' + amount + '</td>';
        str += '<td align="center" >' + cgstper + '<br>' +  cgstamount + '</td>';
        str += '<td align="center" >' + sgstper + '<br>' +  sgstamount + '</td>';
        str += '<td align="center" >' + igstper + '<br>' +  igstamount + '</td>';
		str += '<td align="center" class="cgstper" style="display:none;">' + cgstper + '</td>';
        str += '<td align="center" class="cgstamount" style="display:none;">' + cgstamount + '</td>';
        str += '<td align="center" class="sgstper" style="display:none;">' + sgstper + '</td>';
        str += '<td align="center" class="sgstamount" style="display:none;">' + sgstamount + '</td>';
		str += '<td align="center" class="igstper" style="display:none;">' + igstper + '</td>';
		str += '<td align="center" class="igstamount" style="display:none;">' + igstamount + '</td>';
		str += '<td align="center" class="totalamount">' + totalamount + '</td>';
		str += '<td align="center"><button class="btn btn-sm btn-success" onclick="editor(' + len + ',this)">E</button>  <button class="btn btn-sm btn-danger remover" onclick="remover(this)">R</buuton></td>';
		str += '</tr>';
		$('#table-order > tbody').append(str);
        calculation();

}
// fun_adder function end :

// fun_adder_edit function start :
function fun_adder_edit(len,itemDescription,sac,qty,qty1,rate,amount,cgstper,cgstamount,sgstper,sgstamount,igstper,igstamount,totalamount) {
		var str = '<td align="center" class="srno">' + len + '</td>';
        str += '<td align="center" class="itemDescription">' + itemDescription + '</td>';
        str += '<td align="center" class="sac">' + sac + '</td>';
        str += '<td align="center">' + qty +  '<br>' +  qty1 +'</td>';
		str += '<td align="center" class="qty" style="display:none;">' + qty + '</td>';
        str += '<td align="center" class="qty1" style="display:none;">' + qty1 + '</td>';
		str += '<td align="center" class="rate">' + rate + '</td>';
        str += '<td align="center" class="amount">' + amount + '</td>';
        str += '<td align="center" >' + cgstper + '<br>' +  cgstamount + '</td>';
        str += '<td align="center" >' + sgstper + '<br>' +  sgstamount + '</td>';
        str += '<td align="center" >' + igstper + '<br>' +  igstamount + '</td>';
		str += '<td align="center" class="cgstper" style="display:none;">' + cgstper + '</td>';
        str += '<td align="center" class="cgstamount" style="display:none;">' + cgstamount + '</td>';
        str += '<td align="center" class="sgstper" style="display:none;">' + sgstper + '</td>';
        str += '<td align="center" class="sgstamount" style="display:none;">' + sgstamount + '</td>';
		str += '<td align="center" class="igstper" style="display:none;">' + igstper + '</td>';
		str += '<td align="center" class="igstamount" style="display:none;">' + igstamount + '</td>';
		str += '<td align="center" class="totalamount">' + totalamount + '</td>';
		str += '<td align="center"><button class="btn btn-sm btn-success" onclick="editor(' + len + ',this)">E</button>  <button class="btn btn-sm btn-danger remover" onclick="remover(this)">R</buuton></td>';

			$('#table-order .srno').each(function(){
			var srno=$(this).text().trim();
			if(srno==len){
				$(this).parent().html(str);
			}
        });
        
        totalamt();

        cgstrate();
        sgstrate();
        igstrate();
        calculation();

}
// fun_adder_edit function end :

//editor function start :
function editor(srno,e){
    var itemDescription = $(e).parent().parent().find('.itemDescription').text().trim().replace(/[`^]/gi, "\n^");

    $('#itemDescription').val(itemDescription);
	//  $('#itemDescription').val($(e).parent().parent().find('.itemDescription').text().trim());
     $('#sac').val($(e).parent().parent().find('.sac').text().trim());
	 $('#qty').val($(e).parent().parent().find('.qty').text().trim());
	 $('#qty1').val($(e).parent().parent().find('.qty1').text().trim());
	 $('#rate').val($(e).parent().parent().find('.rate').text().trim());
     $('#amount').val($(e).parent().parent().find('.amount').text().trim());
	 $('#cgstper').val($(e).parent().parent().find('.cgstper').text().trim());
     $('#cgstamount').val($(e).parent().parent().find('.cgstamount').text().trim());
	 $('#sgstper').val($(e).parent().parent().find('.sgstper').text().trim());
     $('#sgstamount').val($(e).parent().parent().find('.sgstamount').text().trim());
	 $('#igstper').val($(e).parent().parent().find('.igstper').text().trim());
	 $('#igstamount').val($(e).parent().parent().find('.igstamount').text().trim());
     $('#totalamount').val($(e).parent().parent().find('.totalamount').text().trim());

	$('#btn-add').data('edit',srno);
}


function calculation() {
    var subtotal  = 0;
	var netamount = 0;
	var totalcgst = 0;
	var totalsgst = 0;
	var totaligst = 0;
    var count     = 0;
    var roundOff  = 0;
    var round     = 0;

	$('#table-order > tbody > tr').each(function () {

        if(count!=0){

            subtotal  += parseFloat($(this).find('.amount').text().trim());
            netamount += parseFloat($(this).find('.totalamount').text().trim());
            totalcgst += parseFloat($(this).find('.cgstamount').text().trim());
            totalsgst += parseFloat($(this).find('.sgstamount').text().trim());
            totaligst += parseFloat($(this).find('.igstamount').text().trim());

        } else {

            count++;

        }


	})

 
    
    


    var totalGst =  totalcgst + totalsgst + totaligst;
    
    roundOff = netamount%1;


    if (0 < roundOff && roundOff < 0.50) {
            
        round = '-' + roundOff.round(2);

    } else if (0 < roundOff && roundOff > 0.50) {
            
        round = ((0.99 - roundOff)+0.01).round(2);
            
    } else if (roundOff == 0.50) {
        
        round = 0.50;

    } else  {

        round = 0;

    }

    $('#subTotal').val(subtotal.round(2));

    $('#cgstAmt').val(totalcgst.round(2));

    $('#sgstAmt').val(totalsgst.round(2));

    $('#igstAmt').val(totaligst.round(2));

    $('#roundOff').val(round);

	$('#netAmount').val(Math.round(netamount));

    $('#totalWords').val(convertNumberToWords(netamount.round(2))).trigger("onchange");
}


function remover(e) {

    $(e).parent().parent().remove();

    calculation();

}



function saveBill() {

    var arr = [];

    var valid = true;

    $('#table-order > tbody > tr').each(function () {

        var itemDescription = $(this).find('.itemDescription').text().trim();
        var sac = $(this).find('.sac').text().trim();
        var qty = $(this).find('.qty').text().trim();
        var qty1 = $(this).find('.qty1').text().trim();
        var rate = $(this).find('.rate').text().trim();
        var amount = $(this).find('.amount').text().trim();
        var cgstper = $(this).find('.cgstper').text().trim();
        var cgstamount = $(this).find('.cgstamount').text().trim();
        var sgstper = $(this).find('.sgstper').text().trim();
        var sgstamount = $(this).find('.sgstamount').text().trim();
        var igstper = $(this).find('.igstper').text().trim();
        var igstamount = $(this).find('.igstamount').text().trim();
        var totalamount = $(this).find('.totalamount').text().trim();

        if (itemDescription != '') {

            arr.push({

                "itemDescription":itemDescription,

                "sac": sac,

                "qty": qty,

                "qty1": qty1,

                "rate": rate,

                "amount":amount,

                "cgstper": cgstper,

                "cgstamount": cgstamount,

                "sgstper": sgstper,

                "sgstamount": sgstamount,

                "igstper": igstper,

                "igstamount": igstamount,

                "totalamount": totalamount


            });
            var valid = true;


        } else {
            var valid = false;

        }

    })

   
    if (checker('pestBill') != false) {

        valid = valid * true;

    } else {

        valid = valid * false;

    }

    let invoicebill = ''
    
    if ($('#copy').val() == 'true') {

        invoicebill = "<?php echo $invoice_bill;?>";
        
    } else if ($('#edit').val() == 'true') {

        invoicebill = $('#copyinvoiceBill').val();
        
    } else {

        invoicebill = $('#invoice_bill').val();
    }

    if (valid) {

        var data = checker('pestBill');
        var datastr = JSON.stringify(data);
        var serviceDetails = JSON.stringify(arr);

        $('#savecertificate').hide();

        $.ajax({

            type: "POST",

            data: {

                data: datastr,
                serviceDetails: serviceDetails,
                branchId: $('#branchId').val(),
                invoice_bill: invoicebill,
                customerId : $('#customerId').val(),
                copy : $('#copy').val(),
                edit : $('#edit').val(),
                category : $('#category').val()

            },

            url: 'api/saveproformabill.php',

            cache: false,

            success: function (res) {

                if (res.status == 'success') {

                    swal({

                        type: 'success',
                        title: 'Bill Create',
                        showConfirmButton: false,
                        timer: 1000

                    });

                    setTimeout(function () {

                        location.href = '/process/invoice/proformabill.php';


                    }, 1000);

                } else{

                    $('#savecertificate').show();

                }

            }

        })

    }

}


if ($('#branchId').val() != '' && $('#category').val() != '' && $('#edit').val() == 'true' || $('#copy').val() == 'true') {

    if ($('#copy').val() == 'true') {

        invoicebill = "<?php echo $_GET['invoice_bill'];?>";

    } else if ($('#edit').val() == 'true') {

        invoicebill = $('#copyinvoiceBill').val();

    } 

    $.ajax({

        type: "POST",

        data: "branchId=" + $('#branchId').val() + "&invoice_bill=" + invoicebill + "&category=" + $('#category').val(),

        url: 'api/billSelect.php',

        cache: false,

        success: function (res) {

            if (res.status == 'success') {

                modals.putvalue('pestBill', res.json);

                $('#savecertificate').attr('onclick', 'saveBill()');

                $('#savecertificate').text('Update Bill');

                var serviceJson = res.json[0].service_json;
                var couter = 1;
                for (var i in serviceJson) {

                    fun_adder(couter,serviceJson[i].itemDescription,serviceJson[i].sac,serviceJson[i].qty,serviceJson[i].qty1,serviceJson[i].rate,serviceJson[i].amount,serviceJson[i].cgstper,serviceJson[i].cgstamount,serviceJson[i].sgstper,serviceJson[i].sgstamount,serviceJson[i].igstper,serviceJson[i].igstamount,serviceJson[i].totalamount);

                    couter++;

                }
               // totalamt();cgstrate();sgstrate();igstrate();
             
                calculation();
            }

        }

    });

}

function uppercase(e) {
    
    let upperCase = $(e).val().toUpperCase();

    $(e).val(upperCase);

}


function uppercasebyid(id){

    let upperCase = $('#'+id).val().toUpperCase();
    
    $('#'+id).val(upperCase);

}

$(document).ready(function () {

    $('.select-js1').select2();

});


function validatepaste(e) {
    //var ctrldown =  e.ctrlKey ? e.ctrlKey : ((e.keyCode === 17) ? true : false);
    var pastedata = e.clipboardData.getData('text/plain');
    if (isNaN(pastedata)) {
        e.preventDefault();
        console.log("PASTE FAIL!");
        console.log(pastedata);
        return false;
    } else {
            console.log("PASTE!");
            console.log(pastedata); 
    }
}

function validate(e) {
    //getting key code of pressed key
    var keycode = (e.which) ? e.which : e.keyCode;
    var phn = document.getElementById('textarea');
    //comparing pressed keycodes
    if ((keycode < 48 || keycode > 57) && keycode !== 13) {
        e.preventDefault();
        console.log("FAIL");
        return false;
    } else {
        console.log("OK!");
    }
}

</script>