<?php

$css = '<link rel="stylesheet" href="'.$base.'css/classic.css">
<link rel="stylesheet" href="'.$base.'css/classic.date.css">
<link rel="stylesheet" href="'.$base.'css/classic.time.css">
<link href="'.$base.'/css/select2.min.css" rel="stylesheet" />
<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
<link rel="stylesheet" href="'.$base.'css/alart.css">
<link rel="stylesheet" href="'.$base.'css/grid.min.css">';

$js = '<script src="'.$base.'js/picker.js"></script>
<script src="'.$base.'js/picker.date.js"></script>
<script src="'.$base.'js/picker.time.js"></script>
<script src="'.$base.'js/pdfmake-new.min.js"></script>
<script src="'.$base.'js/vfs_fonts_times.js"></script>
<script src="'.$base.'js/vfs_fonts.js"></script>
<script src="'.$base.'/js/select2.min.js"></script>
<script src="'.$base.'../js/alart.js"></script>
<script src="'.$base.'js/fwork.js"></script>';


// <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.68/vfs_fonts.min.js"></script>
    include($base.'_in/header.php');
    include($base.'_in/connect.php');

    if (!in_array("Fumigation Invoice", $arrysession) && !in_array('Pestcontrol Invoice', $arrysession) && !in_array('Product Sale Bill Invoice', $arrysession) && !in_array('Proforma Bill Invoice', $arrysession) ) {
   
        echo "<script>window.location.href='/process/dashboard.php';</script>";
       
        exit;

    }
    


    $con = _connect();

 
    if (session_status() == PHP_SESSION_NONE) { session_start(); }
    $branchid = $_SESSION['branchid'];
    
?>
<style>
    .th{
        background:#8e44ad;
        color:#fff;
        text-align:center;
        padding-top:2px;
        padding-bottom:2px;
        border:1px solid #fff;
    }
    .td{
        border:1px solid #ddd;
    }
    .table-ui .btn{
        margin:3px;
    }
    #myModal1 .col-sm-4,.col-sm-8{
      margin-bottom:2px;
    }
    .picker__select--year{
        height:auto;
    }
    .picker__select--month{
        height:auto;
    }
    .table-list td,
   .table-list th {
       border: 1px solid #ddd;
       padding: 1px !important;
       font-size: 13px;
   }
   .table-list td{
	padding-top: 10px !important;
   }

   .table-list tr:nth-child(even) {
       background-color: #f2f2f2;
   }

   .table-list th {
       padding-top: 5px;
       padding-bottom: 5px;
       text-align: center;
       background-color: #16a085;
       color: white;
   }
</style>
<div class="container-fluid">
    <div id="div-content" class="content">
        <table width="100%">
            <tr>
                <td align="center" style="<?php echo $fumigationInvoice; ?>width:15%"><a
                        href="/process/invoice/fumigationInvoice.php"
                        style="border:1px solid blue;border-radius:0px;<?php echo $navenq1;?>"
                        class="btn btn-primary btn-block">Fumigation Invoice</td>
                <td align="center" style="<?php echo $pestcontrolInvoice; ?>width:15%"><a
                        href="/process/invoice/pestcontrolInvoice.php"
                        style="border:1px solid blue;border-radius:0px;<?php echo $navenq2;?>"
                        class="btn btn-primary btn-block">Pest Control Invoice</td>
                <td align="center" style="<?php echo $ProductSaleBillInvoice; ?>width:15%"><a
                        href="/process/invoice/productsalebill.php"
                        style="border:1px solid blue;border-radius:0px;<?php echo $navenq3;?>"
                        class="btn btn-primary btn-block">Product Sale Bill</td>
                <td align="center" style="<?php echo $ProformaBillInvoice; ?>width:15%"><a
                        href="/process/invoice/proformabill.php"
                        style="border:1px solid blue;border-radius:0px;<?php echo $navenq4;?>"
                        class="btn btn-primary btn-block">Proforma Bill</td>
            </tr>
        </table>
