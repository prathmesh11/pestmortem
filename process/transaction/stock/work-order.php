<?php 
$base='../../../';
$navenq7='background:#1B1464;';
  include('header.php');
?>
<br>
<style>
.table-list td,
   .table-list th {
       border: 1px solid #ddd;
       padding: 1px !important;
       font-size: 13px;
   }
   .table-list td{
	padding-top: 10px !important;
   }

   .table-list tr:nth-child(even) {
       background-color: #f2f2f2;
   }

   .table-list th {
       padding-top: 5px;
       padding-bottom: 5px;
       text-align: center;
       background-color: #16a085;
       color: white;
   }
</style>
<div class="table-ui container-fluid">
<div class="tr row">
<div class="col-sm-1 th">Enquiry No.</div>    
<div class="col-sm-3 th">Customer Info</div>
<div class="col-sm-8 th">Work Order Info</div>
</div>
<?php

$result=mysqli_query($con,"SELECT enquiryid,customerid,enquirystatus,quotation,followupnote,category,created_by,created_time FROM enqtable WHERE category='Product' AND enquirystatus<>0 AND quotation<>0 AND customerorder<>0 AND customerorderapproval<>0 AND profarma<>0  AND production=0 AND dispatch=0 AND cancel=0 ORDER BY created_time DESC");
while($rows = mysqli_fetch_assoc($result)){
    $ccode=$rows['customerid'];
    $cifo=mysqli_fetch_assoc(mysqli_query($con,"SELECT cname,ccontact,caddress,cgstno FROM mcustmast WHERE ccode='$ccode' ORDER BY created_time DESC"));
    $enquiryidbom=$rows['enquiryid'];

    $profarmapolicy=true;
    $tablejson=json_decode(mysqli_fetch_assoc(mysqli_query($con,"SELECT tablejson x FROM profarma WHERE enquiryid='$enquiryidbom'"))['x']);
   
    foreach($tablejson as $i){
      $policy=get_object_vars($i)['policy'];
      $status=get_object_vars($i)['status'];

      if($policy=='Advance' && $status==''){
        $profarmapolicy=$profarmapolicy*false;
      }else{
        $profarmapolicy=$profarmapolicy*true;
      }
    }

    if($profarmapolicy){
?>

<div class="row tr">
<div class="col-sm-1 td"><?php echo $rows['enquiryid']; ?></div>
<div class="col-sm-3 td">
<?php 

echo 'Company : '.$cifo['cname']; 
echo '<br>Address : '.$cifo['caddress']; 
echo '<br>Name  : '.get_object_vars(json_decode($cifo['ccontact'])[0])['table'][0];
echo '<br>Dept. : '.get_object_vars(json_decode($cifo['ccontact'])[0])['table'][1];
echo '<br>Mobile : <a href="tel:'.get_object_vars(json_decode($cifo['ccontact'])[0])['table'][3].'">'.get_object_vars(json_decode($cifo['ccontact'])[0])['table'][3].'</a>';
echo '<br>Landline'.get_object_vars(json_decode($cifo['ccontact'])[0])['table'][4];
echo '<br>Email : <a href="mailto:'.get_object_vars(json_decode($cifo['ccontact'])[0])['table'][5].'">'.get_object_vars(json_decode($cifo['ccontact'])[0])['table'][5].'</a>';
?>
</div>



<div class="col-sm-8 td">
<?php 


$custorder=mysqli_fetch_assoc(mysqli_query($con,"SELECT ppno,ppdate,saletype,tablejson,created_by,created_time FROM partypurchase WHERE enquiryid='$enquiryidbom'"));

echo 'Customer Order No : '.$custorder['ppno'].' || ';
echo 'Customer Order Date : '.date('d/m/Y',  strtotime($custorder['ppdate'])).' <br> ';
$created_by=$custorder['created_by'];
$created_by=mysqli_fetch_assoc(mysqli_query($con,"SELECT username x FROM admin WHERE aid='$created_by'"))['x'];
echo 'Created By : '.$created_by.' || ';
echo 'Created Date : '.date("d-m-Y",$custorder['created_time']/1000) .'<br>';
$profarmapolicy=mysqli_fetch_assoc(mysqli_query($con,"SELECT policy x FROM profarma WHERE enquiryid='$enquiryidbom'"))['x'];

?>
<table class="table-list table">
    <thead>
      <tr>
        <th>Product</th>
        <th>Customer Order Qty</th>
        <th>Work Order No.</th>
        <th>Work Order Qty</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
    <?php
    $tablejson=$custorder['tablejson'];
    $tablejson=json_decode($tablejson);
  
    foreach($tablejson as $i){
      $itemid=get_object_vars($i)['itemid']; 
      $partypono=$custorder['ppno'];

      $production=mysqli_fetch_assoc(mysqli_query($con,"SELECT workorderno x FROM workorderno WHERE enquiryid='$enquiryidbom' AND partypono='$partypono' AND itemid='$itemid' AND production<>'0'"))['x']; 
      if($production==''){
        
        $workorderno=mysqli_fetch_assoc(mysqli_query($con,"SELECT workorderno x FROM workorderno WHERE enquiryid='$enquiryidbom' AND partypono='$partypono' AND itemid='$itemid' "))['x'];  
        if($workorderno==''){
          echo '<tr>';
          echo '<td align="center">'.get_object_vars($i)['itemname'].'</td>';
          echo '<td align="center">'.get_object_vars($i)['qty'].'</td>';
          echo '<td align="center"></td>';
          echo '<td align="center"></td>';
          echo '<td align="center"><a href="/process/transaction/engineering-production/index.php?ccode='.$ccode.'&enq='.$rows['enquiryid'].'&custno='.$partypono.'&itemcode='.get_object_vars($i)['itemcode'].'" class="btn btn-sm btn-primary">Create</a></td>';
          echo '</tr>';
        }else{
          $workorderqty=mysqli_fetch_assoc(mysqli_query($con,"SELECT qty x FROM workorderno WHERE enquiryid='$enquiryidbom' AND partypono='$partypono' AND itemid='$itemid' "))['x'];  
          echo '<tr>';
          echo '<td align="center">'.get_object_vars($i)['itemname'].'</td>';
          echo '<td align="center">'.get_object_vars($i)['qty'].'</td>';  
          echo '<td align="center">'.$workorderno.'</td>';
          echo '<td align="center">'.$workorderqty.'</td>';
          echo '<td align="center"><button data-enquiryid="'.$rows['enquiryid'].'" data-workorderno="'.$workorderno.'" onclick="production(this)" class="btn btn-sm btn-primary">Send Production</button></td>';
          echo '</tr>';
        }
      }
    }
    ?>
    </body>
    </table>
<?php



$profarma=mysqli_fetch_assoc(mysqli_query($con,"SELECT tablejson x FROM profarma WHERE enquiryid='$enquiryidbom'"))['x'];
$profarma=json_decode($profarma);

?>


</div>


</div>
<?php
}
}
?>
</div>
</div>
</div>

<?php
    include($base.'_in/footer.php');
?>
<script>
  function closemodal(e){
    $('#'+e).modal('toggle');
  }

  
  function customerapproval(e){
  var enquiryid=$(e).data('enquiryid');
  $.ajax({
        type: "POST",
        data: 'enquiryid='+enquiryid,
        url: 'api/customerapproval.php',
        cache: false,
        success: function (res) {
          if (res.status == 'success') {
             window.location.reload();
          }
        }
    })
}

function production(e){
  var enquiryid=$(e).data('enquiryid');
  var workorderno=$(e).data('workorderno');
  $.ajax({
        type: "POST",
        data: 'enquiryid='+enquiryid+'&workorderno='+workorderno,
        url: 'api/production.php',
        cache: false,
        success: function (res) {
          if (res.status == 'success') {
             window.location.reload();
          }
        }
    })
}

$('.table-list').each(function(){
var len=$(this).find('tbody').find('tr').length;
console.log(len);
if(!len){
  $(this).parent().parent().remove();
}
});
  </script>