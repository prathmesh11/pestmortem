<?php 
$base='../../../../';
  include('../header.php');
?>

<style type="text/css">
.flex-container{
  
    width: 100%;
 

}

body{
    background:#eee;
}
.column{
    
    padding:40px;
    width: 65%;
    background-color: #fff;
    -moz-box-shadow:    inset 0 0 5px #000000;
   -webkit-box-shadow: inset 0 0 5px #000000;
   box-shadow:         inset 0 0 5px #000000;
}
.table-list{
    table-layout: fixed; width: 100%;
}

.table-list td,
.table-list th {
	border: 1px solid #ddd;
	padding: 1px !important;
	font-size: 13px;
}

.table-list td {
	padding-top: 10px !important;
    word-wrap: break-word;
}

.table-list tr:nth-child(even) {
	background-color: #f2f2f2;
}

.table-list th {
	padding-top: 5px;
	padding-bottom: 5px;
	text-align: center;
	background-color: #16a085;
	color: white;
}
@media print {
  body * {
    visibility: hidden;
  }
  #section-to-print, #section-to-print * {
    visibility: visible;
  }
  #section-to-print {
    position: absolute;
    left: 0;
    top: 0;
  }
}
</style>
<?php
$enquiryid=$_GET['enq'];
$ccode=mysqli_fetch_assoc(mysqli_query($con,"SELECT customerid x FROM enqtable WHERE enquiryid='$enquiryid'"))['x'];
$cifo=mysqli_fetch_assoc(mysqli_query($con,"SELECT cname,ccontact,caddress,cgstno FROM mcustmast WHERE ccode='$ccode'"));

?>
<h4 align="center">Product Quatation</h4>
 <div class="flex-container"  id="#section-to-print">
 <div class="col-sm-2"></div>
        <div class="column col-sm-8">
        <div class="row">
    <div class="col-sm-6" align="center">
      <img src="/img/icon/hiverlogo.jpg" style="width:80%;margin-top:40px;">
    </div>
    <div class="col-sm-6">
      <h3 style="color:#3742fa;margin-bottom:2px;"><b>HIVER Aircon Private Limited</b></h3>
      <p style="font-size:13px;font-family:Arial;">Address : Garnet Paladium, Unit No. 104, First Floor, Western
        Express Hwy, Near Oberoi Mall Junction, Pandit M. Nehru Road, Behind Express Zone Buss Park, Goregaon
        East (E), Mumbai - 400097, India <br>
        Tel : +91 22 28304470;28714470;9773159000<br>
        Email : shivajidarade@hiver.co.in, hiverindia@gmail.com,<br>
        Website : www.hiver.co.in</p>
    </div>

    <div class="col-sm-6" style="font-size:16px;">REF : <input type="text" style="width:340px;" data-role='text' data-name='ref' id="inp-ref"></div>
    <div class="col-sm-6" align="right" id="out-date" style="font-size:16px;">
    <input type="date" data-role='date' value="<?php echo $today; ?>" data-name='ref' id="inp-ref"></div>
     
    <div class="col-sm-12">

      <p><strong>To,</strong></p>
      <p><strong><?php echo $cifo['cname']; ?>,</strong></p>
      <p><strong><?php echo $cifo['caddress']; ?> </strong></p>
      <p><strong>Kind Attn : <input type="text" style="width:340px;" data-role='text' data-name='ref' id="inp-ref"></strong></p>
      <p><strong>Sub: <input type="text" style="width:800px;" data-role='text' data-name='ref' id="inp-ref"></strong></p>
      <p><strong>Ref: </strong>As per enquiry</p>
      <p>Dear Sir,</p>
      <p>We thank you for your valued enquiry and are pleased to offer as attached for your consideration, as per the
        terms and conditions mentioned.</p>
      <br>
      <p>We would like to introduce ourselves as a leading Manufacturer &amp; Exporter of all types of Cooling
        Equipment&rsquo;s:</p>
      <p style="color:#3742fa;"><strong>Air-Conditioning water Chillers, Low Temp Brine Chillers, Modular Chillers, Heat Pumps &amp; Industrial
          Chillers.</strong></p>
      <br>
      <p>Dear Sir,</p>
      <br>
     
      <p>Further to our meeting &amp; discussions we are pleased to submit our offer as under:-</p>
      <p>Please see below enlisted attachments in the following order:-</p>
      <br>
      <p>Annexure I : Technical Design</p>
      <p>Annexure II : Commercial</p>
      <p>Annexure III : Terms &amp; Conditions</p>
      <br>
      <p>Hope our offer matches best to your requirement.</p>
      <p>Should you require further more details please feel free to contact us.</p>
      <p>Thanking you and assuring of Genuine OEM Spare Parts, World Class Machines, Quality Installation and Excellent
        Services at all times.</p>
        <br>
      <p style="color:#3742fa;"><b>Yours Faithfully,</b></p>
      <p><strong>For Hiver Aircon Private Limited</strong></p>
      <div class="col-sm-6">
      <p>Sujaj G.</p>
      <p>HOD Sales & Design</p>
      <p>Mob: 07972077157</p>
      <p>Tel: 022 28304470 Ext. No. 202</p>
      <p>Email: <a href="mailto:sales01@hiver.co.in">sales01@hiver.co.in</a></p>
      </div>
      <div class="col-sm-6">
      <p>Shivaji Darade</p>
      <p>Managing Director</p>
      <p>Mob: 09773159000</p>
      <p>Tel: 022 28304470 Ext. No. 206</p>
      <p>Email: <a href="mailto:shivajidarade@hiver.co.in">shivajidarade@hiver.co.in</a></p>
      </div>
     <div style="clear:both;"></div>
      <br>
      
      <h4><b>Annexure I: Technical Design</b></h4>
      
      <table class="table-list col-sm-12 table1" id="table1">
        <thead>
        <tr>
          <th width="50"> No.</th>
          <th>Item</th>
          <th>Description</th>
          <th>Details/Make</th>
          <th  width="70">Action</th>
        </tr>
        </thead>
        <tbody>
        <tr>
          <td></td>
          <td><input type="text" data-role='text' id='inp-text-1' class="form-control input-sm"></td>
          <td><input type="text" data-role='text' id='inp-text-2' class="form-control input-sm"></td>
          <td><input type="text" data-role='text' id='inp-text-3' class="form-control input-sm"></td>
          <td><button class="btn btn-sm btn-success" onclick="btnannexure1()" style="border-radius:50px;">add +</button></td>
        </tr>
        </tbody>
      </table>

      <p><strong>Data Received from Customer:</strong></p>
      <p><strong>Work under Customer&rsquo;s Scope:</strong></p>
      <ul>
        <li>Piping between Chiller &amp; Process</li>
        <li>Cooling Tower Water for Condenser</li>
        <li>Hot Air Exhaust Ducting (if required)</li>
        <li>Incoming Power Supply to Chiller</li>
        <li>Specified Quantity &amp; Quality of Media</li>
        <li>Material Unloading, Unpacking, Handling &amp; Positioning</li>
        <li>Any Civil and/or Fabrication work</li>
      </ul>
      <p><strong>Important Notes:</strong></p>
      <ul>
        <li>Ensure proper ventilation</li>
        <li>Hot exhaust air from chiller should not short cycle back to chiller</li>
        <li style="color:red;">DO NOT use MS/GI piping</li>
        <li>Use only non-corrosive piping to avoid rust</li>
        <li>Chiller to be installed on a flat platform</li>
      </ul>
      <p><strong>*Optional:</strong></p>
      <ul>
        <li>Flow Switches</li>
        <li>Flow Meters</li>
        <li>Pressure Relief Valves</li>
        <li>&plusmn; 0.50C Temperature Stability</li>
        <li>Parallel Temperature Display</li>
        <li>Remote Control Box</li>
        <li>RS485 Communication Port or LAN Port</li>
        <li>Interlocking to Process Machine</li>
        <li>Interconnecting Piping</li>
        <li>Additional year warranties</li>
        <li>Comprehensive &amp; Non-comprehensive</li>
      </ul>
      <p>Service packages beyond warranty</p>

      <br>
    <h4><b>Annexure II: Commercial</b></h4>
    <table class="table-list col-sm-12 table2" id="table2" style="margin-left:5px;">
        <thead>
        <tr>
          <th width="50">No.</th>
          <th>Description</th>
          <th>Price</th>
          <th width="70">Action</th>
        </tr>
        </thead>
        <tbody>
        <tr>
          <td></td>
          <td><input type="text" data-role='text' id='inp-text-4' class="form-control input-sm"></td>
          <td><input type="text" data-role='text' id='inp-text-5' class="form-control input-sm"></td>
          <td><button class="btn btn-sm btn-success"  onclick="btnannexure2()" style="border-radius:50px;">add +</button></td>
        </tr>
        </tbody>
      </table>
   <div style="clear:both;"></div>
   <br>
      <table class=" table-list ">
        <thead>
        </thead>
        <tbody>
          <tr>
            <td>GST HSN CODE</td>
            <td><input type="text" data-role='text' data-name='goodreciptno' value="84158210" class="form-control input-sm"></td>
          </tr>
          <tr>
            <td>Ex-Works</td>
            <td><input type="text" data-role='text' value="Hiver Aircon Private Limited Plot No. A72/2, Opp. Legrand, Malegaon MIDC, Sinnar, Dist: Nashik - 422113 Maharashtra India" data-name='goodreciptno'  class="form-control input-sm"></td>
          </tr>
        </tbody>
      </table>
<br>
      <table class=" table-list ">
        <thead>
          <tr>
            <th>Packing Charges</th>
            <th><input type="text" data-role='text' value="3%" data-name='goodreciptno' class="form-control input-sm"></th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>PAN No</td>
            <td><input type="text" data-role='text' value="AADCH2014H" data-name='goodreciptno' class="form-control input-sm"></td>
          </tr>
          <tr>
            <td>GSTN</td>
            <td><input type="text" data-role='text' value="27AADCH2014H1Z0 (zero)" data-name='goodreciptno' class="form-control input-sm"></td>
          </tr>
          <tr>
            <td>CIN</td>
            <td><input type="text" data-role='text'  value="U31500MH2013PTC244687" data-name='goodreciptno' class="form-control input-sm"></td>
          </tr>
      
        </tbody>
      </table>

<br>
<table class=" table-list ">
        <thead>
          <tr>
            <th>Freight (Optional)</th>
            <th>Within Customer’s Scope</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>Local Transport Packings (GST HSN CODE : 4415)</td>
            <td><input type="text" data-role='text' value="Within Customer’s Scope"  data-name='goodreciptno' class="form-control input-sm"></td>
          </tr>
          <tr>
            <td>Seaworthy Packings (GST HSN CODE : 4415)</td>
            <td><input type="text" data-role='text' value="Within Customer’s Scope"  data-name='goodreciptno' class="form-control input-sm"></td>
          </tr>
          <tr>
            <td>Unpacking, Unloading & Positioning</td>
            <td><input type="text" data-role='text' value="Within Customer’s Scope" data-name='goodreciptno' class="form-control input-sm"></td>
          </tr>
      
        </tbody>
      </table>
      <br>
      <table class=" table-list ">
        <thead>
          <tr>
            <th>Warranty</th>
            <th>12 months from Date of Tax Invoice</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>2nd Year (Optional)</td>
            <td><input type="text" data-role='text' value="N.A." data-name='goodreciptno' class="form-control input-sm"></td>
          </tr>
          <tr>
            <td>3rd & 4th Year (Optional)</td>
            <td><input type="text" data-role='text' value="N.A." data-name='goodreciptno' class="form-control input-sm"></td>
          </tr>
          <tr>
            <td>4th & 5th Year (Optional)</td>
            <td><input type="text" data-role='text' value="N.A." data-name='goodreciptno' class="form-control input-sm"></td>
          </tr>
      
        </tbody>
      </table>
      <br>
      <table class=" table-list ">
        <thead>
          <tr>
            <th>Installation & Commissioning (Optional)</th>
            <th>Within Hiver’s Scope</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>Total Price in INR (Exclusive of GST @ 18.00 %)</td>
            <td><input type="text" data-role='text' data-name='goodreciptno' class="form-control input-sm"></td>
          </tr>
          <tr>
            <td>CGST @ 9% & SGST @ 9% / IGST @ 18%</td>
            <td><input type="text" data-role='text' data-name='goodreciptno' class="form-control input-sm"></td>
          </tr>
          <tr>
            <td>Total Price in INR (Inclusive of GST @ 18.00 %)</td>
            <td><input type="text" data-role='text' data-name='goodreciptno' class="form-control input-sm"></td>
          </tr>
          <tr>
            <td>GST SAC CODE</td>
            <td><input type="text" data-role='text' data-name='goodreciptno' value="995469" class="form-control input-sm"></td>
          </tr>
        </tbody>
      </table>
<br>
      <h4><strong>Annexure III: Terms &amp; Conditions:-</strong></h4>
      <p><strong>EX FACTORY DELIVERY: </strong>Delivery from the date of receipt of confirmed order. If any outside
        Inspection by Customer or Inspection Agency is involved, the time required for this Inspection/release of
        Inspection Note is to be added to the delivery time and any cost of Inspection will be to customer&rsquo;s
        account. Consignee particulars must be clearly and completely furnished and Consignee must deliver receipt of
        goods on the R.R./L.R. In case the goods are too bulky for transportation, they will be supplied in knocked down
        condition in suitable le package and the supply will be treated as of the equipment itself.</p>
      <p><strong>VALIDITY: </strong>Our offer is valid for your acceptance up to validity date and thereafter subject to
        our written confirmation. The contract shall not be binding unless accepted in writing by a Manager authorized
        by the Company.</p>
      <p><strong>EQUIPMENT SPECIFICATIONS: </strong>All equipment&rsquo;s, components and material enumerated under the
        &lsquo;Scope of Supply&rsquo; would be as per our design/selection. We reserve the right to supply the bought
        out items from the supplier of our choice/design, unless specifically agreed upon prior to the order. We also
        reserve the right to make changes at any time in material or design, which in our judgment, are necessary for
        the improvement of the specified equipment.</p>
      <p><strong>FACILITIES TO BE PROVIDED BY YOU: </strong>Suitable and secured premises for storage of equipment and
        tools free of charge.</p>
      <p><strong>DAMAGES: </strong>Damages arising out of this Contract and claimed will not cover working losses of
        profits, losses of stored/in transit goods or consequential damages.</p>
      <p><strong>FORCE MAJEURE : </strong>The Performance of the contract hereunder shall be subject to the conditions
        of Force Majeure Clause including acts of God like floods, earthquakes, etc., war, fire riots, civil commotion,
        strikes, lockouts, Governmental regulations and other circumstances beyond human control, which directly or
        indirectly affect the performance or obligations by the Vendor or their sub-vendors, who have undertaken to
        supply the bought out items to the contractor&rsquo;s requirement, to be used for the equipment&rsquo;s to be
        supplied under this contract. Provided the occurrence of any such event under any of the circumstances described
        hereinabove renders the performance of the obligations hereunder Incapable or impossible, then in that case the
        period of delivery under the contract will be automatically deemed to be extended by a corresponding period,
        until the occurrence of the events affecting the performance are removed or abated.</p>
      <p><strong>WARRANTY: </strong>Provided the plant is used in a proper manner, we undertake to remedy within
        reasonable time (subject to the under mentioned provisions) any defects arising from faulty design, material or
        workmanship, which may develop within a warranty period. Our responsibility is limited to repairs and/or
        replacement of such defective parts only. We cannot accept any responsibility for any consequential or resulting
        damage, liability or loss arising from such defects. The replaced parts shall be our property. The guarantee
        also does not include refrigerant. We supply only first charge of refrigerant as included in Scope of Supply.
        Any additional refrigerant required for any reason whatsoever would be to Customer&rsquo;s account. The warranty
        does not cover the supply of consumable items like oil, filters, etc. and also does not cover routine
        maintenance of the plant, cleaning of the condenser periodically, cleaning of the condenser or normal
        deterioration of the plant due to atmospheric conditions and deterioration of the plant on account of rusting,
        etc. due to idling or improper storage.</p>
      <p><strong> TRANSFER OF PROPERTY: </strong>The transfer of Property in the goods shall be deemed to have taken
        place from</p>
      <p> HIVER AIRCON PVT. LTD. to the Customer the moment they are delivered to a Carrier or to the Buyer or other
        bailey</p>
      <p> but this is subject to Buyer making full payment of the purchase price including taxes, duties,
        transportation,</p>
      <p> insurance charges, etc.</p>
      <br>
      <p><strong> CANCELLATION: </strong>In case, the Purchaser cancels the order fully or partially, 100% assessed
        cancellation</p>
      <p> charges will be applicable and charged to the purchaser.</p>
      <p><strong>SALES TAX: </strong>As applicable at the time of dispatch. Customer shall furnish appropriate Forms in
        support of any concessional Taxes/Duties along with the Purchase Order; otherwise the full rate will be charged
        and shall be paid by the Customer.</p>

        <div style="clear:both;"></div>
<hr>
    <button class="btn btn-primary btn-block" >Download</button>
    <hr>
    </div>

  </div>
  
        </div>
        
    </div>

<?php
    include($base.'_in/footer.php');
?>
<script>
function btnannexure1(){
  if($('#inp-text-1').val()!='' || $('#inp-text-2').val()!='' || $('#inp-text-3').val()!=''){
var len=($('#table1 > tbody > tr').length);
var str='<tr class="table1-'+len+'">';
str+='<td align="center">'+len+'</td>';
str+='<td>'+$('#inp-text-1').val()+'</td>';
str+='<td>'+$('#inp-text-2').val()+'</td>';
str+='<td>'+$('#inp-text-3').val()+'</td>';
str+='<td align="center"><button class="btn btn-sm btn-danger " data-len="table1-'+len+'" onclick="removes(this)" style="border-radius:50px;">R</button></td>';
str+='</tr>';
$('.table1 > tbody').append(str);
$('#inp-text-1').val('');
$('#inp-text-2').val('');
$('#inp-text-3').val('');
  }
}

function removes(e){
$('.'+$(e).data('len')).remove();
}

function btnannexure2(){
  if($('#inp-text-4').val()!='' || $('#inp-text-5').val()!=''){
  var len=($('#table2 > tbody > tr').length);
var str='<tr class="table2-'+len+'">';
str+='<td align="center">'+len+'</td>';
str+='<td>'+$('#inp-text-4').val()+'</td>';
str+='<td>'+$('#inp-text-5').val()+'</td>';
str+='<td align="center"><button class="btn btn-sm btn-danger" data-len="table2-'+len+'" onclick="removes(this)" style="border-radius:50px;">R</button></td>';
str+='</tr>';
$('.table2 > tbody').append(str);
$('#inp-text-4').val('');
$('#inp-text-5').val('');
  }
}


$('#inp-ref').on('keyup',function(){
  $('#out-ref').text($(this).val());
})

$('#inp-date').on('keyup',function(){
  $('#out-date').text($(this).val());
})

$('#inp-kind').on('keyup',function(){
  console.log($(this).val());
  
  $('#out-kind').text($(this).val());
})

$('#inp-sub').on('keyup',function(){
  $('#out-sub').text($(this).val());
})
</script>