<?php 
$base='../../../';
$navenq2='background:#1B1464;';
  include('header.php');
?>
<style>
.input-container {
 
  max-width: 300px;
  background-color: #EDEDED;
  border: 1px solid #DFDFDF;
  border-radius: 5px;
}

input[type='file'] {
  display: none;
}

.file-info {
  font-size: 0.9em;
}

.browse-btn {
  background: #03A595;
  color: #fff;
  min-height: 35px;
  padding: 10px;
  border: none;
  border-top-left-radius: 5px;
  border-bottom-left-radius: 5px;
}

.browse-btn:hover {
  background: #4ec0b4;
}

@media (max-width: 300px) {
  button {
    width: 100%;
    border-top-right-radius: 5px;
    border-bottom-left-radius: 0;
  }
  
  .file-info {
    display: block;
    margin: 10px 5px;
  }
}
</style>
<br>

<div class="table-ui container-fluid">
<div class="tr row">
<div class="col-sm-1 th">Enquiry No.</div>    
<div class="col-sm-3 th">Customer Info</div>
<div class="col-sm-2 th">Select BOM</div>
<div class="col-sm-4 th">Uploaded Costing</div>
<div class="col-sm-2 th">Action</div>
</div>
<?php

$result=mysqli_query($con,"SELECT extra,enquiryid,customerid,enquirystatus,followupnote,category,created_by,created_time FROM enqtable WHERE enquirystatus<>0 AND quotation=0 AND customerorder=0 AND customerorderapproval=0 AND profarma=0 AND payment=0 AND production=0 AND dispatch=0 AND cancel=0 ORDER BY created_time DESC");
while($rows = mysqli_fetch_assoc($result)){

    $ccode=$rows['customerid'];
    $cifo=mysqli_fetch_assoc(mysqli_query($con,"SELECT cname,ccontact,caddress,cgstno FROM mcustmast WHERE ccode='$ccode' ORDER BY created_time DESC"));
    $enquiryidbom=$rows['enquiryid'];
   
?>

<div class="row tr">
<div class="col-sm-1 td"><?php echo $rows['enquiryid']; ?></div>
<div class="col-sm-3 td">
<?php 

echo 'Company : '.$cifo['cname']; 
echo '<br>Address : '.$cifo['caddress']; 
echo '<br>Name  : '.get_object_vars(json_decode($cifo['ccontact'])[0])['table'][0];
echo '<br>Dept. : '.get_object_vars(json_decode($cifo['ccontact'])[0])['table'][1];
echo '<br>Mobile : <a href="tel:'.get_object_vars(json_decode($cifo['ccontact'])[0])['table'][3].'">'.get_object_vars(json_decode($cifo['ccontact'])[0])['table'][3].'</a>';
echo '<br>Landline'.get_object_vars(json_decode($cifo['ccontact'])[0])['table'][4];
echo '<br>Email : <a href="mailto:'.get_object_vars(json_decode($cifo['ccontact'])[0])['table'][5].'">'.get_object_vars(json_decode($cifo['ccontact'])[0])['table'][5].'</a>';
?>
</div>

<div class="col-sm-2 td"><?php
$bom=mysqli_query($con,"SELECT DISTINCTROW name FROM sheetmetal WHERE ccode='$ccode'");
$extra=json_decode('['.$rows['extra'].']');
echo '<select class="form-control input-sm">';
while($rows1 = mysqli_fetch_assoc($bom)){
  $bomname=$rows1['name'];
  $itemcode=mysqli_fetch_assoc(mysqli_query($con,"SELECT itemcode x FROM sheetmetal WHERE name='$bomname' ORDER BY id DESC LIMIT 1"))['x'];
  $checker=0;
  foreach($extra as $i){
    $itemcode2=get_object_vars($i)['itemcode'];
    if($itemcode2==$itemcode){
      $checker++;
    }
  }
  if($checker==0){
    echo '<option value="'.$itemcode.'">'.$rows1['name'].'</option>';
  }
}
echo '</select>';
echo '<button class="btn btn-sm btn-primary btn-block" data-enquiryid="'.$rows['enquiryid'].'" style="margin-top:1px;" onclick="addbom(this)">add</button>';


foreach($extra as $i){
 // get_object_vars($i)['itemcode']
  echo 'BOM Name :'.get_object_vars($i)['name'].'<br>';
  echo 'Drawing No. : '.get_object_vars($i)['drwingno'].'<br>';
  echo 'Rev No. : '.get_object_vars($i)['RevNo'].'<br>';
 echo '<hr style="border:1px solid #000;margin:1px;padding:0px;color:#000;">';
}

?>
</div>

<div class="col-sm-4 td">
<?php
foreach($extra as $i){
  // get_object_vars($i)['itemcode']
   echo 'BOM Name :'.get_object_vars($i)['name'].'<br>';
   echo '
   <div class="row">
   <div class="col-sm-6 input-container">
   <input type="file" class="real-input" name="quatation" accept="application/xlsx">
     <button class="browse-btn">
       Browse Files
     </button>
   <span class="file-info">Upload a file</span>
 </div> 
 <div class="col-sm-3" style="padding:4px;">
 <button class="btn btn-primary btn-sm btn-block" data-enquiryid="'.$rows['enquiryid'].'"  data-bomid="'.get_object_vars($i)['id'].'" onclick="upload(this)">Upload </button>
 </div>
 <div class="col-sm-3"style="padding:4px;" >
 <a class="btn btn-primary btn-sm  btn-block" href="/process/transaction/enquiry1/quatation/product.php?enq='.$rows['enquiryid'].'&bom='.get_object_vars($i)['id'].'" >Create Costing</a>
 </div>
   </div>
 
 <div style="clear:both;"></div>';
 $enquir=str_replace('/', '-',$rows['enquiryid']);
 $path="quatation/pdf/".$enquir.'-'.get_object_vars($i)['id'].'.xlsx';
 if(file_exists($path)){
 echo '<center><a href="'.$path.'" target="_blank">View xlsx</a></center>';
 }else{
   echo 'Upload excel file';
 }
 echo '<hr style="margin:1;padding:0px;border:1px solid #000;">
 ';

}


?>
</div>
<div class="col-sm-2 td">
<?php
if($category=='Product'){ echo '<a class="btn btn-warning btn-sm  btn-block" href="/process/create-master/hiverbom/index.php?enq='.$rows['enquiryid'].'" >Create BOM</a>'; }
?>
<?php
    $enquir=str_replace('/', '-',$rows['enquiryid']);
    $path="quatation/pdf/".$enquir.'.xlsx';
  if(file_exists($path)){
  ?>
  <button class="btn btn-success btn-sm  btn-block" data-enquiryid="<?php echo $rows['enquiryid']; ?>" onclick="confirmquatation(this)">Confirm </button>

  <?php
  }
?>
<button class="btn btn-sm btn-block btn-danger" data-enquiryid="<?php echo $rows['enquiryid']; ?>" onclick="create(this)">Create Work Contract</button>
<button class="btn btn-sm btn-block btn-danger" data-enquiryid="<?php echo $rows['enquiryid']; ?>" onclick="cancel(this)">Cancel</button>
</div>

</div>
<?php
}
?>
</div>
</div>
</div>

<?php
    include($base.'_in/footer.php');
?>
<script>

function addbom(e){
  var itemcode=$(e).parent().find('select').val();
  var enquiryid=$(e).data('enquiryid');
  $.ajax({
        type: "POST",
        data: 'enquiryid='+enquiryid+'&itemcode='+itemcode,
        url: 'api/itemcode.php',
        cache: false,
        success: function (res) {
          if (res.status == 'success') {
             window.location.reload();
          }
        }
    })
}
$('.browse-btn').on('click',function(){
  $(this).parent().find('input').click();
});

$('.real-input').on('change',function(){
  $(this).parent().find('span').text($(this).val().replace(/C:\\fakepath\\/i, ''));

});

  function closemodal(e){
    $('#'+e).modal('toggle');
  }

  
  function confirmquatation(e){
  var enquiryid=$(e).data('enquiryid');
  $.ajax({
        type: "POST",
        data: 'enquiryid='+enquiryid,
        url: 'quatation/confirmquatation.php',
        cache: false,
        success: function (res) {
          if (res.status == 'success') {
             window.location.reload();
          }
        }
    })
}

function serivcetype(e){
  var enquiryid=$(e).data('enquiryid');
  var extra=$(e).val();
  $.ajax({
        type: "POST",
        data: 'enquiryid='+enquiryid+'&extra='+extra,
        url: 'api/serivcetype.php',
        cache: false,
        success: function (res) {
          if (res.status == 'success') {
             window.location.reload();
          }
        }
    })
}

function upload(e){
var file=$(e).parent().parent().find('input')[0].files[0];
var enquiryid=$(e).data('enquiryid');
var bomid=$(e).data('bomid');
var formData = new FormData();

formData.append('quatation',file);
formData.append('enquiryid',enquiryid);
formData.append('bomid',bomid);
$.ajax({
        type: "POST",
        data: formData,
        url: 'quatation/pdf.php',
        cache: false,
        processData: false,
        contentType: false,
        success: function (res) {
        
             window.location.reload();
         
        }
    })
}



  </script>
  <script>
function cancel(e){
     var enquiryid=$(e).data('enquiryid');
  $.ajax({
        type: "POST",
        data: 'enquiryid='+enquiryid,
        url: 'api/cancel.php',
        cache: false,
        success: function (res) {
          if (res.status == 'success') {
             window.location.reload();
          }
        }
    })
}
</script>