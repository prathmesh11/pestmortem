<?php 
$base='../../../';
$navenq10='background:#1B1464;';
  include('header.php');
?>
<br>
<style>
.table-list td,
   .table-list th {
       border: 1px solid #ddd;
       padding: 1px !important;
       font-size: 13px;
   }
   .table-list td{
	padding-top: 10px !important;
   }

   .table-list tr:nth-child(even) {
       background-color: #f2f2f2;
   }

   .table-list th {
       padding-top: 5px;
       padding-bottom: 5px;
       text-align: center;
       background-color: #16a085;
       color: white;
   }
</style>
<div class="table-ui container-fluid">
<div class="tr row">
<div class="col-sm-1 th">Enquiry No.</div>    
<div class="col-sm-3 th">Customer Info</div>
<div class="col-sm-8 th">Order Status</div>
</div>
<?php

$result=mysqli_query($con,"SELECT enquiryid,customerid,enquirystatus,quotation,customerorder,customerorderapproval,profarma,payment,workorder,production,invoice,dispatch,cancel,followupnote,category,created_by,created_time FROM enqtable WHERE category='Product' AND enquirystatus<>0 AND quotation<>0 AND dispatch=0 AND cancel=0 ORDER BY created_time DESC");
while($rows = mysqli_fetch_assoc($result)){
    $ccode=$rows['customerid'];
    $cifo=mysqli_fetch_assoc(mysqli_query($con,"SELECT cname,ccontact,caddress,cgstno FROM mcustmast WHERE ccode='$ccode' ORDER BY created_time DESC"));
    $enquiryidbom=$rows['enquiryid'];
?>

<div class="row tr">
<div class="col-sm-1 td"><?php echo $rows['enquiryid']; ?></div>
<div class="col-sm-3 td">
<?php 

echo 'Company : '.$cifo['cname']; 
echo '<br>Address : '.$cifo['caddress']; 
echo '<br>Name  : '.get_object_vars(json_decode($cifo['ccontact'])[0])['table'][0];
echo '<br>Dept. : '.get_object_vars(json_decode($cifo['ccontact'])[0])['table'][1];
echo '<br>Mobile : <a href="tel:'.get_object_vars(json_decode($cifo['ccontact'])[0])['table'][3].'">'.get_object_vars(json_decode($cifo['ccontact'])[0])['table'][3].'</a>';
echo '<br>Landline'.get_object_vars(json_decode($cifo['ccontact'])[0])['table'][4];
echo '<br>Email : <a href="mailto:'.get_object_vars(json_decode($cifo['ccontact'])[0])['table'][5].'">'.get_object_vars(json_decode($cifo['ccontact'])[0])['table'][5].'</a>';
?>
</div>

<?php 
$quotation='';
$customerorder='';
$customerorderapproval='';
$profarma='';
$payment='';
$workorder='';
$production='';
$invoice='';
$dispatch='';
$cancel='';

if($rows['quotation']!=0){ $quotation=date("d-m-Y",$rows['quotation']/1000); }
if($rows['customerorder']!=0){ $customerorder=date("d-m-Y",$rows['customerorder']/1000); }
if($rows['customerorderapproval']!=0){ $customerorderapproval=date("d-m-Y",$rows['customerorderapproval']/1000); }
if($rows['profarma']!=0){ $profarma=date("d-m-Y",$rows['profarma']/1000); }
if($rows['payment']!=0){ $payment=date("d-m-Y",$rows['payment']/1000); }
if($rows['workorder']!=0){ $workorder=date("d-m-Y",$rows['workorder']/1000); }
if($rows['production']!=0){ $production=date("d-m-Y",$rows['production']/1000); }
if($rows['invoice']!=0){ $invoice=date("d-m-Y",$rows['invoice']/1000); }
if($rows['dispatch']!=0){ $dispatch=date("d-m-Y",$rows['dispatch']/1000); }
if($rows['cancel']!=0){ $cancel=date("d-m-Y",$rows['cancel']/1000); }

?>

<div class="col-sm-8 td">
<table class="table-list table">
<thead>
<tr>
<th style="width:20%">Quatation</th>
<th style="width:20%">Customer Order</th>
<th style="width:20%">Admin Approval</th>
<th style="width:20%">Profarma</th>
<th style="width:20%">Payment</th>

</tr>
</thead>
<tbody>
<?php 

echo '<tr>';
echo '<td align="center">'.$quotation.'</td>';
echo '<td align="center">'.$customerorder.'</td>';
echo '<td align="center">'.$customerorderapproval.'</td>';
echo '<td align="center">'.$profarma.'</td>';
echo '<td align="center">'.$payment.'</td>';

echo '</tr>';
?>
</tbody>
</table>

<table class="table-list table">
<thead>
<tr>
<th style="width:20%">Work Order</th>
<th style="width:20%">Production</th>
<th style="width:20%">Invoice</th>
<th style="width:20%">Cancel</th>
<th style="width:20%">Dispatch</th>
</tr>
</thead>
<tbody>
<?php 


echo '<tr>';
echo '<td align="center">'.$workorder.'</td>';
echo '<td align="center">'.$production.'</td>';
echo '<td align="center">';
if($invoice){
    echo $invoice.'<button class="btn btn-sm btn-warning" data-enquiryid="'.$rows['enquiryid'].'printdata(this)" onclick="">Print</button>';
}else{
    echo $invoice;
}

echo '</td>';
echo '<td align="center"><button class="btn btn-sm btn-block btn-danger" data-enquiryid="'.$rows['enquiryid'].'" onclick="cancel(this)">Cancel</button></td>';
$profarmapolicy=true;
    $tablejson=json_decode(mysqli_fetch_assoc(mysqli_query($con,"SELECT tablejson x FROM profarma WHERE enquiryid='$enquiryidbom'"))['x']);
   
    foreach($tablejson as $i){
      $policy=get_object_vars($i)['policy'];
      $status=get_object_vars($i)['status'];

      if($policy=='Advance' && $status==''){
        $profarmapolicy=$profarmapolicy*false;
      }else{
        $profarmapolicy=$profarmapolicy*true;
      }

      if($policy=='Before Dispatch' && $status==''){
        $profarmapolicy=$profarmapolicy*false;
      }else{
        $profarmapolicy=$profarmapolicy*true;
      } 
    }

if($profarmapolicy==true && $invoice!=''){
    echo '<td align="center"><button class="btn btn-sm btn-block btn-success" data-enquiryid="'.$rows['enquiryid'].'" onclick="dispatch(this)">Dispatch</button></td>';
}else{
    echo '<td align="center"></td>';   
}

echo '</tr>';
?>
</tbody>
</table>
</div>

</div>
<?php
}
?>
</div>
</div>
</div>

<?php
    include($base.'_in/footer.php');
?>

<script>
function cancel(e){
     var enquiryid=$(e).data('enquiryid');
  $.ajax({
        type: "POST",
        data: 'enquiryid='+enquiryid,
        url: 'api/cancel.php',
        cache: false,
        success: function (res) {
          if (res.status == 'success') {
             window.location.reload();
          }
        }
    })
}

function dispatch(e){
     var enquiryid=$(e).data('enquiryid');
  $.ajax({
        type: "POST",
        data: 'enquiryid='+enquiryid,
        url: 'api/dispatch.php',
        cache: false,
        success: function (res) {
          if (res.status == 'success') {
             window.location.reload();
          }
        }
    })
}

function printdata(e){
  var enquiryid=$(e).data('enquiryid');
  $.ajax({
        type: "POST",
        data: 'enquiryid='+enquiryid,
        url: 'api/printdata.php',
        cache: false,
        success: function (res) {
          if (res.status == 'success') {
            
          }
        }
    })
}
</script>