<?php 
$base='../../../../';

$navenq4='background:#1B1464;';
include('header.php');
$enquiryidnew=mysqli_fetch_assoc(mysqli_query($con,"SELECT id x FROM crmmaster ORDER BY id DESC"))['x'];

  $year1=0;
  $year2=0;

if ( date('m') > 3 ) {
    $year1 = date('y');
    $year2 = date('y')+1;
}else{
    $year1 = date('y')-1;
    $year2 = date('y');
} 

if (session_status() == PHP_SESSION_NONE) { session_start(); }
$sessionby = $_SESSION['employeeid'];
$branchid = $_SESSION['branchid'];

$branchshortname=mysqli_fetch_assoc(mysqli_query($con,"SELECT branchshortname x FROM branchmaster WHERE branchid='$branchid'"))['x'];

  if($enquiryidnew){
    $enquiryidnew++;
    $enquiryidnew=str_pad($enquiryidnew,6, '0', STR_PAD_LEFT);
    $enquiryidnew='PMI/'.$branchshortname.'/ENQ/'.$enquiryidnew.'/'.$year1.'-'.$year2;
  }else{
    $enquiryidnew='PMI/'.$year1.'-'.$year2.'/B'.$branchid.'/E'.$sessionby.'/1';
    $enquiryidnew='PMI/'.$branchshortname.'/ENQ/000001/'.$year1.'-'.$year2;
  }

?>
<br>
<style>
.input-container {
 
  max-width: 300px;
  background-color: #EDEDED;
  border: 1px solid #DFDFDF;
  border-radius: 5px;
}

input[type='file'] {
  display: none;
}

.file-info {
  font-size: 0.9em;
}

.browse-btn {
  background: #03A595;
  color: #fff;
  min-height: 35px;
  padding: 10px;
  border: none;
  border-top-left-radius: 5px;
  border-bottom-left-radius: 5px;
}

.browse-btn:hover {
  background: #4ec0b4;
}

@media (max-width: 300px) {
  button {
    width: 100%;
    border-top-right-radius: 5px;
    border-bottom-left-radius: 0;
  }
  
  .file-info {
    display: block;
    margin: 10px 5px;
  }
}
</style>
<div class="table-ui container-fluid">
<div class="tr row">

<div class="col-sm-1 th">Cert. No.</div>   
<div class="col-sm-3 th">Enquiry No.</div>    
 <div class="col-sm-3 th">Certificate Type</div>
<div class="col-sm-3 th">Certificate </div>
<div class="col-sm-2 th">Action</div>
</div>

<?php
//$result=mysqli_query($con,"SELECT enquiryid ,category,customerid,enquiryby ,enquirynote ,followupnote ,followuptime ,created_time ,created_by ,enquiry_time ,enquiry_by ,wo_time ,wo_by ,cert_time ,cert_by ,job_all_time ,job_all_by ,job_com_time ,job_com_by ,close_enquiry_time ,close_enquiry_by FROM crmmaster WHERE created_time<>0 AND created_by<>0 AND enquiry_time<>0 AND enquiry_by<>0 AND wo_time<>0 AND wo_by<>0 AND cert_time=0 AND cert_by=0 AND job_all_time=0 AND job_all_by=0 AND job_com_time=0 AND job_com_by=0 AND close_enquiry_time=0 AND close_enquiry_by=0 ORDER BY created_time DESC");
$result = mysqli_query($con,"SELECT * FROM certificatemaster WHERE created ='Y' ");
while($rows = mysqli_fetch_assoc($result)){

    $enquiryid=$rows['enquiryid'];

  //  $cifo=mysqli_fetch_assoc(mysqli_query($con,"SELECT * FROM customermaster WHERE customerid='$customerid'"));

?>

<div class="row tr">
<div class="col-sm-1 td text-center" style="word-wrap:break-word;"><?php echo $rows['id']; ?></div>
 
<div class="col-sm-3 td"><?php echo $rows['enquiryid']; ?></div>
 <div class="col-sm-3 td text-center"><?php echo $rows['certType']; ?></div>
 

<div class="col-sm-3 td">
<?php
/* 
    $enquir=str_replace('/', '-',$rows['enquiryid']);
    $path="api/certificate/".$enquir.'.pdf';

  if(file_exists($path)){
    echo '<br><center><a href="'.$path.'" target="_blank">View PDF</a></center>';
  }else{
    echo '<br>No Certificate created<BR>';
  }

  $result1=mysqli_query($con,"SELECT certType FROM certificatemaster WHERE enquiryid = '".$rows['enquiryid']."'");
while($rows1 = mysqli_fetch_assoc($result1)){
  $enquir=str_replace('/', '-',$rows1['enquiryid']);
    
    $path="api/certificate/".$enquir.'.pdf';

    echo '<a href="'.$path.'" target="_blank">'.$rows1['certType'].'</a><br>';

}
*/
?>
</div>

<div class="col-sm-2 td">
  
  <button class="btn" class="btn btn-sm btn-block btn-primary" data-toggle="modal" data-target="#myModal">Assign</button>
</div>

</div>
<?php
}
?>
</div>
</div>
</div>

 
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" align="center">Assign Job</h4>
      </div>
      <div class="modal-body">
          <div class="form-group row">

         <div class="col-sm-4">Select Employee</div>
       <div class="col-sm-6">
       <select data-role="select" data-name='empid'  id="enpid" class="form-control input-sm">
                <option value="Select">Select</option>
                <?php 
                  $result11=mysqli_query($con,"SELECT employeeid,username  FROM employeemaster ORDER by id ASC");
                  while($rows = mysqli_fetch_assoc($result11)){
                    echo '<option value="'.$rows['employeeid'].'">'.$rows['username'].'</option>';
                  }
                ?>
        </select>
       </div>

         
       <div class="col-sm-4"></div>
       <div class="col-sm-8"><button class="btn btn-success btn-block btn-sm" id="allotjobbtn" onclick="allotjob()">Allot Job</button></div>
        
      </div>
    </div>
  </div>
</div>
</div>

<?php 
include('footer.php');
?>
<script>

$('.browse-btn').on('click',function(){
  $(this).parent().find('input').click();
});

$('.real-input').on('change',function(){
  $(this).parent().find('span').text($(this).val().replace(/C:\\fakepath\\/i, ''));

});

function allotjob() {
  $('#myModal').hide();
  var valid = true;
  
  if (checker('myModal') != false) {
    valid = valid * true;
  } else {
    valid = valid * false;
  }
  if (valid) {
    var data=checker('myModal');
    var datastr = JSON.stringify(data);
     alert(datastr);
    $.ajax({
        type: "POST",
        data: {
          data: datastr
        },
        url: 'api/allotjob.php',
        cache: false,
        success: function (res) {
          if (res.status == 'success') {
         
              window.location.reload();
           
          }
        }
      })
    }
  }
  
function workcontract(){

}

function setidtomidal(e){
var enquiryid=$(e).data('enquiryid');
$('#inp-enquiryidset').val(enquiryid);
}



function upload(e){
var file=$(e).parent().parent().find('input')[0].files[0];
var enquiryid=$(e).data('enquiryid');
var formData = new FormData();

formData.append('offer',file);
formData.append('enquiryid',enquiryid);
$.ajax({
        type: "POST",
        data: formData,
        url: 'api/offer.php',
        cache: false,
        processData: false,
        contentType: false,
        success: function (res) {
             window.location.reload();
        }
    })
}


</script>