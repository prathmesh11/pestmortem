<?php 
$base='../../../../';

$navenq2='background:#1B1464;';
include('header.php');
$enquiryidnew=mysqli_fetch_assoc(mysqli_query($con,"SELECT id x FROM crmmaster ORDER BY id DESC"))['x'];

  $year1=0;
  $year2=0;

if ( date('m') > 3 ) {
    $year1 = date('y');
    $year2 = date('y')+1;
}else{
    $year1 = date('y')-1;
    $year2 = date('y');
}

if (session_status() == PHP_SESSION_NONE) { session_start(); }
$sessionby = $_SESSION['employeeid'];
$branchid = $_SESSION['branchid'];

$branchshortname=mysqli_fetch_assoc(mysqli_query($con,"SELECT branchshortname x FROM branchmaster WHERE branchid='$branchid'"))['x'];

  if($enquiryidnew){
    $enquiryidnew++;
    $enquiryidnew=str_pad($enquiryidnew,6, '0', STR_PAD_LEFT);
    $enquiryidnew='PMI/'.$branchshortname.'/ENQ/'.$enquiryidnew.'/'.$year1.'-'.$year2;
  }else{
    $enquiryidnew='PMI/'.$year1.'-'.$year2.'/B'.$branchid.'/E'.$sessionby.'/1';
    $enquiryidnew='PMI/'.$branchshortname.'/ENQ/000001/'.$year1.'-'.$year2;
  }

?>
<br>
<style>
.input-container {
 
  max-width: 300px;
  background-color: #EDEDED;
  border: 1px solid #DFDFDF;
  border-radius: 5px;
}

input[type='file'] {
  display: none;
}

.file-info {
  font-size: 0.9em;
}

.browse-btn {
  background: #03A595;
  color: #fff;
  min-height: 35px;
  padding: 10px;
  border: none;
  border-top-left-radius: 5px;
  border-bottom-left-radius: 5px;
}

.browse-btn:hover {
  background: #4ec0b4;
}

@media (max-width: 300px) {
  button {
    width: 100%;
    border-top-right-radius: 5px;
    border-bottom-left-radius: 0;
  }
  
  .file-info {
    display: block;
    margin: 10px 5px;
  }
}
</style>
<div class="table-ui container-fluid">
<div class="tr row">
<div class="col-sm-1 th">Enquiry No.</div>    
<div class="col-sm-3 th">Customer Info</div>
<div class="col-sm-1 th">Enquery For</div>
<div class="col-sm-2 th">Offer</div>
<div class="col-sm-2 th">Note</div>
<div class="col-sm-1 th">Work_Contract</div>
<div class="col-sm-2 th">Action</div>
</div>

<?php
$result=mysqli_query($con,"SELECT enquiryid ,category,customerid,enquiryby ,enquirynote ,followupnote ,followuptime ,created_time ,created_by ,enquiry_time ,enquiry_by ,wo_time ,wo_by ,cert_time ,cert_by ,job_all_time ,job_all_by ,job_com_time ,job_com_by ,close_enquiry_time ,close_enquiry_by FROM crmmaster WHERE created_time<>0 AND created_by<>0 AND enquiry_time<>0 AND enquiry_by<>0 AND wo_time=0 AND wo_by=0 AND cert_time=0 AND cert_by=0 AND job_all_time=0 AND job_all_by=0 AND job_com_time=0 AND job_com_by=0 AND close_enquiry_time=0 AND close_enquiry_by=0 ORDER BY created_time DESC");

while($rows = mysqli_fetch_assoc($result)){

    $customerid=$rows['customerid'];

    $cifo=mysqli_fetch_assoc(mysqli_query($con,"SELECT * FROM customermaster WHERE customerid='$customerid'"));

?>

<div class="row tr">
<div class="col-sm-1 td" style="word-wrap:break-word;"><?php echo $rows['enquiryid']; ?></div>
<div class="col-sm-3 td">
<?php
echo 'Company : '.$cifo['customername']; 
echo '<br>Address : '.get_object_vars(json_decode($cifo['customerjson']))['address']; 
echo '<br>Name  : '.get_object_vars(json_decode($cifo['contactjson'])[0])['name'];
echo '<br>Dept. : '.get_object_vars(json_decode($cifo['contactjson'])[0])['department'];
echo '<br>Mobile : <a href="tel:'.get_object_vars(json_decode($cifo['contactjson'])[0])['mobile'].'">'.get_object_vars(json_decode($cifo['contactjson'])[0])['mobile'].'</a>';
echo '<br>Landline'.get_object_vars(json_decode($cifo['contactjson'])[0])['landline'];
echo '<br>Email : <a href="mailto:'.get_object_vars(json_decode($cifo['contactjson'])[0])['email'].'">'.get_object_vars(json_decode($cifo['contactjson'])[0])['email'].'</a>';
?>
</div>
<div class="col-sm-1 td"><?php echo $rows['category']; ?></div>

<div class="col-sm-2 td">
<!-- offer  -->
<div class="input-container">
  <input type="file" class="real-input" name="quatation" accept="application/pdf">
    <button class="browse-btn">
      Browse Files
    </button>
  <span class="file-info">Upload a file</span>
</div>
<button class="btn btn-primary btn-sm btn-block" data-enquiryid="<?php echo $rows['enquiryid']; ?>" onclick="upload(this)">Upload </button>

<?php 
    $enquir=str_replace('/', '-',$rows['enquiryid']);
    $path="api/offer/".$enquir.'.pdf';
  if(file_exists($path)){
    echo '<br><center><a href="'.$path.'" target="_blank">View PDF</a></center>';
  }else{
    echo '<br>Please Upload .pdf File';
  }
?>
<!-- offer  -->
</div>

<div class="col-sm-2 td"><?php echo $rows['enquirynote']; ?></div>

<div class="col-sm-1 td"><?php 

?></div>

<div class="col-sm-2 td">
<button class="btn btn-success btn-sm btn-block" data-link1="enquiry_time" data-link2="enquiry_by" data-enquiryid="<?php echo $rows['enquiryid']; ?>" onclick="confirms(this)">Order Confirm</button>
<button class="btn btn-sm btn-block btn-danger" data-enquiryid="<?php echo $rows['enquiryid']; ?>" onclick="closes(this)">Order Close</button>
<a class="btn btn-sm btn-block btn-primary" href="work-contract/index.php?enq=<?php echo $rows['enquiryid']; ?>" >Work Contract</a>
</div>

</div>
<?php
}
?>
</div>
</div>
</div>

<button class="btn" style="font-size:25px;border-radius:50px;width:60px;height:60px;position: fixed;
    bottom: 40px;
    right: 40px;background:#eb2f06;color:#fff;" data-toggle="modal" data-target="#myModal">+</button>

<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" align="center">New Enquiry</h4>
      </div>
      <div class="modal-body">
          <div class="form-group row">
       <div class="col-sm-4">Enquiry No.</div>
       <div class="col-sm-8"><input type="text" data-name="enquiryid" value="<?php echo $enquiryidnew; ?>" class="form-control input-sm" readonly></div>
       <div class="col-sm-4">Select Customer</div>
       <div class="col-sm-6">
       <select data-role="select" data-name='customerid'  id="customeridnew" class="form-control input-sm">
                <option value="Select">Select</option>
                <?php 
                  $result11=mysqli_query($con,"SELECT customerid,customername  FROM customermaster ORDER by customerid ASC");
                  while($rows = mysqli_fetch_assoc($result11)){
                    echo '<option value="'.$rows['customerid'].'">'.$rows['customername'].'</option>';
                  }
                ?>
        </select>
       </div>
       <div class="col-sm-2">
        <a href="/process/create-master/customer-master/index.php" class="btn btn-block btn-success btn-sm">New</a>
        </div>
        
       <div class="col-sm-4" >Select Category</div>
       <div class="col-sm-8">
       <select data-role="select" data-name='category' class="form-control input-sm">
                <option value="Fumigation">Fumigation</option>
                <option value="Pest Control">Pest Control</option>
        </select>
       </div>

       <input type="hidden" data-name="workorder" value="workorder">
      
       <div class="col-sm-4">Note</div>
       <div class="col-sm-8"><input type="text" data-name="enquirynote" class="form-control input-sm"></div>
       <div class="col-sm-4"></div>
       <div class="col-sm-8"><button class="btn btn-success btn-block btn-sm" id="newqneuiry" onclick="newqneuiry()">Add New Enquery</button></div>
        </div>
        
      </div>
    </div>
  </div>
</div>


<?php 
include('footer.php');
?>
<script>

$('.browse-btn').on('click',function(){
  $(this).parent().find('input').click();
});

$('.real-input').on('change',function(){
  $(this).parent().find('span').text($(this).val().replace(/C:\\fakepath\\/i, ''));

});

function newqneuiry() {
  $('#newqneuiry').hide();
  var valid = true;
  if (checker('myModal') != false) {
    valid = valid * true;
  } else {
    valid = valid * false;
  }
  if (valid) {
    var data=checker('myModal');
    var datastr = JSON.stringify(data);
    $.ajax({
        type: "POST",
        data: {
          data: datastr
        },
        url: 'api/newqneuiry.php',
        cache: false,
        success: function (res) {
          if (res.status == 'success') {
         
              window.location.reload();
           
          }
        }
      })
    }
  }
  
function workcontract(){

}

function setidtomidal(e){
var enquiryid=$(e).data('enquiryid');
$('#inp-enquiryidset').val(enquiryid);
}



function upload(e){
var file=$(e).parent().parent().find('input')[0].files[0];
var enquiryid=$(e).data('enquiryid');
var formData = new FormData();

formData.append('offer',file);
formData.append('enquiryid',enquiryid);
$.ajax({
        type: "POST",
        data: formData,
        url: 'api/offer.php',
        cache: false,
        processData: false,
        contentType: false,
        success: function (res) {
             window.location.reload();
        }
    })
}


</script>