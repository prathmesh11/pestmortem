<?php 
$base='../../../../';

$navenq6='background:#1B1464;';
include('header.php');
$enquiryidnew=mysqli_fetch_assoc(mysqli_query($con,"SELECT id x FROM crmmaster ORDER BY id DESC"))['x'];

  $year1=0;
  $year2=0;

if ( date('m') > 3 ) {
    $year1 = date('y');
    $year2 = date('y')+1;
}else{
    $year1 = date('y')-1;
    $year2 = date('y');
} 

if (session_status() == PHP_SESSION_NONE) { session_start(); }
$sessionby = $_SESSION['employeeid'];
$branchid = $_SESSION['branchid'];

$branchshortname=mysqli_fetch_assoc(mysqli_query($con,"SELECT branchshortname x FROM branchmaster WHERE branchid='$branchid'"))['x'];

  if($enquiryidnew){
    $enquiryidnew++;
    $enquiryidnew=str_pad($enquiryidnew,6, '0', STR_PAD_LEFT);
    $enquiryidnew='PMI/'.$branchshortname.'/ENQ/'.$enquiryidnew.'/'.$year1.'-'.$year2;
  }else{
    $enquiryidnew='PMI/'.$year1.'-'.$year2.'/B'.$branchid.'/E'.$sessionby.'/1';
    $enquiryidnew='PMI/'.$branchshortname.'/ENQ/000001/'.$year1.'-'.$year2;
  }

?>
<br>
<style>
.input-container {
 
  max-width: 300px;
  background-color: #EDEDED;
  border: 1px solid #DFDFDF;
  border-radius: 5px;
}

input[type='file'] {
  display: none;
}

.file-info {
  font-size: 0.9em;
}

.browse-btn {
  background: #03A595;
  color: #fff;
  min-height: 35px;
  padding: 10px;
  border: none;
  border-top-left-radius: 5px;
  border-bottom-left-radius: 5px;
}

.browse-btn:hover {
  background: #4ec0b4;
}

@media (max-width: 300px) {
  button {
    width: 100%;
    border-top-right-radius: 5px;
    border-bottom-left-radius: 0;
  }
  
  .file-info {
    display: block;
    margin: 10px 5px;
  }
}
</style>
<div class="table-ui container-fluid">
<div class="tr row">
 
<div class="col-sm-2 th">Enquiry No.</div>   
<div class="col-sm-3 th">Customer Info</div> 

 <div class="col-sm-2 th">Bills</div> 
<div class="col-sm-2 th">Followup Date</div>
<div class="col-sm-2 th">Followup Note</div>

<div class="col-sm-1 th">Action</div>
</div>

<?php
$result=mysqli_query($con,"SELECT id,enquiryid ,category,customerid,enquiryby ,enquirynote ,followupnote ,followuptime ,created_time ,created_by ,enquiry_time ,enquiry_by ,wo_time ,wo_by ,cert_time ,cert_by ,job_all_time ,job_all_by ,job_com_time ,job_com_by ,close_enquiry_time ,close_enquiry_by FROM crmmaster WHERE created_time<>0 AND created_by<>0 AND enquiry_time<>0 AND enquiry_by<>0 AND wo_time<>0 AND wo_by<>0 AND cert_time<>0 AND cert_by<>0 AND job_all_time<>0 AND job_all_by<>0 AND job_com_time=0 AND job_com_by=0 AND close_enquiry_time=0 AND close_enquiry_by=0 ORDER BY created_time DESC");
//$result = mysqli_query($con,"SELECT * FROM certificatemaster WHERE created ='Y' ");
while($rows = mysqli_fetch_assoc($result)){

    $enquiryid=$rows['enquiryid'];
    $id=$rows['id'];
    // $cifo=mysqli_fetch_assoc(mysqli_query($con,"SELECT * FROM crmmaster WHERE enquiryid='$enquiryid'"));
    $customerid = $rows['customerid'];
     $cifo=mysqli_fetch_assoc(mysqli_query($con,"SELECT * FROM customermaster WHERE customerid='$customerid'"));
     $qry = mysqli_query($con,"SELECT * FROM workcontractbill WHERE enquiryid='$enquiryid'");
  $bifo=mysqli_fetch_assoc($qry);
  $rowcnt = mysqli_num_rows($qry);
  if($rowcnt ==0){
    $followupon = 0;
  }else{
    if($bifo['followuptime']==0){
        $followupon = 0;
    }else{
      $followupon = $bifo['followuptime'];
     
    }
   
  }
     
?>

<div class="row tr">
 
<div class="col-sm-2 td"><?php echo $rows['enquiryid']; ?></div>
<div class="col-sm-3 td">
<?php
echo 'Company : '.$cifo['customername']; 
echo '<br>Address : '.get_object_vars(json_decode($cifo['customerjson']))['address']; 
echo '<br>Mobile : <a href="tel:'.get_object_vars(json_decode($cifo['contactjson'])[0])['mobile'].'">'.get_object_vars(json_decode($cifo['contactjson'])[0])['mobile'].'</a>';
?>
</div>
<?php

  $qry = mysqli_query($con,"SELECT * FROM workcontractbill WHERE enquiryid='$enquiryid' AND paymentrecived_by<>0 AND followuptime<>0 ");
   $rowcnt1 = mysqli_num_rows($qry);
   /*if($rowcnt1 >=0){ 
    $followupon = $bifo['followuptime'];
   }else{  
     $followupon ="0";
   }  
   */
 ?>

 <div class="col-sm-2 td text-center">
 <?php 
  if($rowcnt != 0)
  { ?>
    <input type="radio" class="billtype_<?php echo $bifo['id']; ?>" name="billtype_<?php echo $bifo['id']; ?>" value="TAX INVOICE" checked>TAX INVOICE  
    <input type="radio" class="billtype_<?php echo $bifo['id']; ?>" name="billtype_<?php echo $bifo['id']; ?>" value="BILL">BILL
    
    <a class="btn btn-sm btn-block btn-success" onclick="printer(<?php echo $bifo['id']; ?>,'<?php echo $enquiryid; ?>')" >View PDF</a>
 <?php } ?>
 </div>
 

 
<div class="col-sm-2 td">

 <?php
 if($followupon !=0 || $followupon !=NULL ){ 
     echo date("d-m-Y",$followupon/1000);
  }

 ?>
</div>
<div class="col-sm-2 td">
<?php  echo $bifo['followupnote']; ?>
</div>
<div class="col-sm-1 td">
  
   <?php
   if($rowcnt ==0) { ?>
 <a class="btn btn-sm btn-block btn-primary" href="payment/createbill.php?id=<?php echo $rows['id']; ?>&enq=<?php echo $rows['enquiryid']; ?>" >Billing</a> 
 <?php }  
   else{ 
   ?>
 
  <button class="btn btn-sm btn-block btn-success"  data-toggle="modal" data-target="#myModal_<?php echo $rows['id']; ?>">Payment</button>
 
<!-- modal -->
<div id="myModal_<?php echo $rows['id']; ?>" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" align="center">Make Payment For <?php echo $rows['id']; ?></h4>
      </div>
      <div class="modal-body">
          <div class="form-group row">
 <input type="hidden" data-name="enquiryid" class="form-control" value="<?php echo $rows['enquiryid']; ?>">
       <div class="col-sm-4">Received By :</div>
       <div class="col-sm-6">
       <select data-role="select" data-name="empid"  id="empid" class="form-control input-sm">
                <option value="Select">Select</option>
                <option value="0">NONE</option>
                <?php 
                  $result11=mysqli_query($con,"SELECT employeeid,username  FROM employeemaster WHERE branchid = '$branchid' ORDER by id ASC");
                  while($rows1 = mysqli_fetch_assoc($result11)){
                    echo '<option value="'.$rows1['employeeid'].'">'.$rows1['username'].'</option>';
                  }
                ?>
        </select>
       </div>
           <br> <br> <br> 
        <!--<div class="col-sm-4" >Status </div>
       <div class="col-sm-6">
       <select data-role="select" data-name="status" id="status" class="form-control input-sm">
                
                <option value="">Select</option>
                <option value="Clear">Clear</option>
                <option value="Followup">Follow up</option>
                
        </select>
       </div>
      
    <br> <br> <br>-->
     <div class="col-sm-4">Followup On</div>
           <div class="col-sm-6">
            <input type="text" data-name="followupon1" id="inp-enq-date" class="form-control">
            <input type="hidden" data-name="followupon" id="inp-enq-date-stamp">
       
           </div>
           <br> <br> <br> 
       <div class="col-sm-4">Followup Note</div>
           <div class="col-sm-6">
            <input type="text" data-name="followupnote" class="form-control">
           </div>
           <br> <br> <br> 
       <div class="col-sm-4"></div>
       <div class="col-sm-8"><button class="btn btn-success btn-block btn-sm" id="addcertificate" onclick="addpayment(<?php echo $rows['id']; ?>);">Save</button></div>
        
      </div>
    </div>
  </div>
</div>
</div>
<!--   modal end-->

 <?php  } ?>
 
</div>

</div>
<?php
}
?>
</div>
</div>
</div>

 

<?php 
include('footer.php');
?>
<script>


$("#inp-enq-date").pickadate({
        selectYears:true,
        selectMonths:true,
        min:true,
        onClose: function(){
            try{
                var dateStamp = this.get('select')['pick'];
                $("#inp-enq-date-stamp").val(dateStamp);
                alert(dateStamp);
            }catch(err){

            }
        }
    });



$('.browse-btn').on('click',function(){
  $(this).parent().find('input').click();
});

$('.real-input').on('change',function(){
  $(this).parent().find('span').text($(this).val().replace(/C:\\fakepath\\/i, ''));

});
 
function addpayment(id) {
  var mid = '#myModal_'+id;
 $(mid).hide();
  var valid = true;

 
  if (checker('myModal_'+id) != false) {
    valid = valid * true;
  } else {
    valid = valid * false;
  }

  if (valid) {
    var data=checker('myModal_'+id);
    var datastr = JSON.stringify(data);
     //alert(datastr);
    $.ajax({
        type: "POST",
        data: {
          data: datastr
        },
        url: 'api/addpayment.php',
        cache: false,
        success: function (res) {
          //alert(res.status);
          if (res.status == 'success') {
         
              window.location.reload();
           
          }
        }
      })
    }
  }
function confirmcertificate(e) {
     
        var id = $(e).data('id');
        var enquiryid = $(e).data('enquiryid');
        $.ajax({
            type: "POST",
            data: 'enquiryid=' + enquiryid + '&id=' + id ,
            url: 'api/billing.php',
            cache: false,
            success: function (res) {
                 if (res.status == 'success') {
                    window.location = "job-allotment.php";
                }
            }
        })
    
}
function printer(id,enqno) {
 var billidname = "billtype_"+id;
 //var billtype = $(billidname).val();
 var billtype = $("input[name='billtype_"+id+"']:checked").val();

// alert(billtype);

   var jsondata = $.parseJSON($.ajax({
         type: "POST",
        data: {
          id: id
        },
        url: 'api/getbilljson.php',
        cache: false,
        success: function (res) {
        //jsondata =JSON.stringify(res.json);
         },
        dataType: "json", 
        async: false
    }).responseText);
   

jsondata =JSON.stringify(jsondata);
data1 = JSON.parse(jsondata);  
json1 = data1.json;     
contractid = data1.contractid;
billdate = data1.billdate; 

 //get customer info

var jsondata = $.parseJSON($.ajax({
         type: "POST",
        data: {
          enqno: enqno
        },
        url: 'api/getcustjson.php',
        cache: false,
        success: function (res) {
        //jsondata =JSON.stringify(res.json);
         },
        dataType: "json", 
        async: false
    }).responseText);
   jsondata =JSON.stringify(jsondata);
data2 = JSON.parse(jsondata);  
json2= data2.json; 
customername = data2.customername;
address = data2.address;
state = data2.state;
gstno = data2.gstno;
 
 //get contract info

var jsondata = $.parseJSON($.ajax({
         type: "POST",
        data: {
          enqno: enqno
        },
        url: 'api/getwcjson.php',
        cache: false,
        success: function (res) {
          //alert(res.status);
        //jsondata =JSON.stringify(res.json);
         },
        dataType: "json", 
        async: false
    }).responseText);

jsondata =JSON.stringify(jsondata);
data3 = JSON.parse(jsondata);  
//alert(jsondata);
itemjson= data3.itemjson; 
servicejson= data3.servicejson; 
contractdate = data3.contractdate;

contractstart = data3.contractstart;
contractend = data3.contractend;
netamount = data3.netamount;

billamt = json1.qty * json1.rate;
 
balamt = netamount - billamt;
 
if(billtype!= 'TAX INVOICE'){
  sgst=0;igst=0;cgst=0;freight=0;

}else{
  sgst=json1.sgst;
  igst=json1.igst;
  cgst=json1.cgst;
  freight=0;

}
igstamt = (billamt*igst)/100;
cgstamt = (billamt*cgst)/100;
sgstamt = (billamt*sgst)/100;

grandtotal = billamt+igstamt+cgstamt+sgstamt;
totalamtinwords = convertNumberToWords(grandtotal)+ " Only";


 //get branch info

var jsondata = $.parseJSON($.ajax({
         type: "POST",
        data: {
          enqno: enqno
        },
        url: 'api/getbrjson.php',
        cache: false,
        success: function (res) {
          //alert(res.status);
        //jsondata =JSON.stringify(res.json);
         },
        dataType: "json", 
        async: false
    }).responseText);

jsondata =JSON.stringify(jsondata);
data4 = JSON.parse(jsondata); 

branchjson= data4.branchjson; 
branchshortname= data3.branchshortname; 
branchname = data3.branchname;




serviceinfo = "\nSERVICE : "+servicejson.servicename+"\nCONTRACT PERIOD : "+contractstart+" TO "+contractend+"\nP.O. No. : "+json1.pono+" DTD : "+contractdate+"\n\n";
balanceinfo ="\nRs. "+netamount+": TOTAL CONTRACT AMOUNT\nRs."+billamt+": 1ST QUARTER BILL\n ----------------\nRs."+balamt+" : BALANCE";

paymentinfo = "Rupees : "+totalamtinwords+"\n\nPayment should be made within 30 days from the date of invoice.\n Interest @18% will be charged on overdue Invoices.\n GSTIN: "+gstno+"\n Category of Service : Cleaning Service\n\n E.P.F. A/c No. : MH/40228\n E.S.I.C. Code No. : 31-480-101\n PAN NO. : AACCP25\n\nFumigation - Accreditation No. \n\n Bank Name : "+branchjson.bankname+" \nBranch Name :"+branchjson.bankbranchname+"\n Account holder's Name : M/s. Pest Mortem (India) Pvt. Ltd.\n AC Number : C.A.No. "+branchjson.accountno+"\n RTGS/NEFT IFSC CODE : "+branchjson.ifsccode;
totalinfo = "TOTAL     : "+billamt+"\n()FREIGHT     : "+freight+"\nCGST@ "+cgst+"%      : "+cgstamt+"\nSGST@ "+sgst+"%      : "+sgstamt+"\nIGST@ "+igst+" %     : "+igstamt+"\nGRAND TOTAL      : "+grandtotal+"\n\n\n\n\n For Pest Mortem(India) Pvt. Ltd.\n\n\n\nAuthorised Signatory";

var fullDate = new Date();
var twoDigitMonth = ((fullDate.getMonth().length + 1) === 1) ? (fullDate.getMonth() + 1) : '' + (fullDate.getMonth() + 1);
var currentDate = fullDate.getDate() + "/" + twoDigitMonth + "/" + fullDate.getFullYear();

//var billdate = new Date(billdate);

//billdate = billdate.getDate()+ "/" + (billdate.getMonth()+1) + "/" + billdate.getFullYear();

  

 var content=[]; 
 //json1 =  JSON.parse(json);
     //  content.push({ text:'\n( Sr. No.'+jsondata.srno+' )',alignment:'center' });
         
                  dd = {
                        

                    pageSize: 'A4',
                    pageMargins: [40, 20, 20, 40],
                    footer: function (currentPage, pageCount) {
                        return {
                            margin: 10,
                            columns: [{
                                fontSize: 8,
                                text: [{
                                        text: '--------------------------------------------------------------------------' +
                                            '\n',
                                        margin: [0, 20]
                                    },
                                    {
                                        text: '©Pest Mortem (India) PVT. LTD. || Printed On ' +currentDate ,
                                    }
                                ],
                                alignment: 'center'
                            }]
                        };

                       },

                    content: [
                            {
                                text: billtype,
                                 fontSize: 10,
                                      bold: true,
                                      alignment: 'center',
                            },
                            {
                                text: 'SUBJECT TO MUMBAI JURIDICTION',
                                 fontSize: 10,
                                      bold: true,
                                      alignment: 'center',
                            },
                             {
                                text: 'PEST MORTEM (INDIA) PRIVATE LIMITED',
                                 fontSize: 16,
                                      bold: true,
                                      alignment: 'center',
                            },
                            {
                                    text: '(Estd. 1980)',
                                     fontSize: 12,
                                      bold: true,
                                      alignment: 'center',
                                },
                              '\n',
                              {
                                    text: 'Regd. Office : '+branchjson.address+'\nTel : '+branchjson.phone+' Email : '+branchjson.email ,
                                     fontSize: 8,
                                      bold: true,
                                      alignment: 'center',
                                },
                                {
                                     border : 0,
                                    bold: true,
                                     fontSize: 8,
                                    margin: [0, 15, 0, 15],
                                    table: {
                                         widths: [50, 250, 100, '*'],
                                        height:400,
                                        body: [
                                            ['M/S:', address, 'INVOICE No. :',id],
                                            ['GST no.:', gstno, 'INVOICE Date. :',billdate],
                                            ['State:', state, 'CONTRACT NO. :',contractid],
                                            ['A/C.:', '', 'CONTRACT DATE :',contractdate]
                                          
                                        ],

                                    },
                                        layout: 'noBorders'
                                },
                                {
                                     border : 0,
                                    bold: true,
                                     fontSize: 8,
                                    margin: [0, 15, 0, 15],
                                    table: {
                                         widths: [300, 20,20, 50, '*'],
                                   
                                        body: [
                                            ['PARTICULARS', 'SAC', 'QTY','RATE','TOTAL'],
                                            [itemjson.itemname, itemjson.SAC, json1.qty,json1.rate,(json1.qty*json1.rate)],
                                            [serviceinfo, '', '','',''],
                                            [balanceinfo, '', '','',''],

                                        ],

                                    },

                                         
                                },
                                
                                {
                                    border : 0,
                                    bold: true,
                                     fontSize: 8,
                                    margin: [0, 15,],
                                    table: {
                                         widths: [400,  '*'],
                                   
                                        body: [
                                            // [paymentinfo,  {text: totalinfo,alignment: 'right'}],
                                          [paymentinfo,  {
                                                               border : 0,
                                                              bold: true,
                                                               fontSize: 8,
                                                              margin: [0, 15, ],
                                                              table: {
                                                                   widths: [80, '*'],
                                                             
                                                                  body: [
                                                                      ['TOTAL  ', ':'+billamt ],
                                                                      ['()FREIGHT', ':'+freight ],       
                                                                      ['CGST '+cgst+'%', ':'+cgstamt ],
                                                                      ['SGST '+sgst+' %', ':'+sgstamt ],  
                                                                      ['IGST '+igst+' %', ':'+igstamt ],
                                                                      ['GRAND TOTAL ', ':'+grandtotal ],       
                                                                           
                                                                                                                                     

                                                                  ],

                                                                }, layout: 'noBorders'

                                                      }],
                                                     ], 
                                    },  
                                     layout: 'noBorders'

                                         
                                },

                    ]
               }
                         // totalinfo = "TOTAL     : "+billamt+"\n()FREIGHT     : "+freight+"\nCGST@ 0.00      : "+cgstamt+"\nSGST@ 0.00      : "+sgstamt+"\nIGST@ 18.00      : "+igstamt+"\nGRAND TOTAL      : "+grandtotal+"\n\n\n\n\n For Pest Mortem(India) Pvt. Ltd.\n\n\n\nAuthorised Signatory";
    
               pdfMake.createPdf(dd).download("billno_"+id+".pdf");
            //  pdfMake.createPdf(dd).open();

}

function convertNumberToWords(amount) {
    var words = new Array();
    words[0] = '';
    words[1] = 'One';
    words[2] = 'Two';
    words[3] = 'Three';
    words[4] = 'Four';
    words[5] = 'Five';
    words[6] = 'Six';
    words[7] = 'Seven';
    words[8] = 'Eight';
    words[9] = 'Nine';
    words[10] = 'Ten';
    words[11] = 'Eleven';
    words[12] = 'Twelve';
    words[13] = 'Thirteen';
    words[14] = 'Fourteen';
    words[15] = 'Fifteen';
    words[16] = 'Sixteen';
    words[17] = 'Seventeen';
    words[18] = 'Eighteen';
    words[19] = 'Nineteen';
    words[20] = 'Twenty';
    words[30] = 'Thirty';
    words[40] = 'Forty';
    words[50] = 'Fifty';
    words[60] = 'Sixty';
    words[70] = 'Seventy';
    words[80] = 'Eighty';
    words[90] = 'Ninety';
    amount = amount.toString();
    var atemp = amount.split(".");
    var number = atemp[0].split(",").join("");
    var n_length = number.length;
    var words_string = "";
    if (n_length <= 9) {
        var n_array = new Array(0, 0, 0, 0, 0, 0, 0, 0, 0);
        var received_n_array = new Array();
        for (var i = 0; i < n_length; i++) {
            received_n_array[i] = number.substr(i, 1);
        }
        for (var i = 9 - n_length, j = 0; i < 9; i++, j++) {
            n_array[i] = received_n_array[j];
        }
        for (var i = 0, j = 1; i < 9; i++, j++) {
            if (i == 0 || i == 2 || i == 4 || i == 7) {
                if (n_array[i] == 1) {
                    n_array[j] = 10 + parseInt(n_array[j]);
                    n_array[i] = 0;
                }
            }
        }
        value = "";
        for (var i = 0; i < 9; i++) {
            if (i == 0 || i == 2 || i == 4 || i == 7) {
                value = n_array[i] * 10;
            } else {
                value = n_array[i];
            }
            if (value != 0) {
                words_string += words[value] + " ";
            }
            if ((i == 1 && value != 0) || (i == 0 && value != 0 && n_array[i + 1] == 0)) {
                words_string += "Crores ";
            }
            if ((i == 3 && value != 0) || (i == 2 && value != 0 && n_array[i + 1] == 0)) {
                words_string += "Lakhs ";
            }
            if ((i == 5 && value != 0) || (i == 4 && value != 0 && n_array[i + 1] == 0)) {
                words_string += "Thousand ";
            }
            if (i == 6 && value != 0 && (n_array[i + 1] != 0 && n_array[i + 2] != 0)) {
                words_string += "Hundred and ";
            } else if (i == 6 && value != 0) {
                words_string += "Hundred ";
            }
        }
        words_string = words_string.split("  ").join(" ");
    }
    return words_string;
}
</script>