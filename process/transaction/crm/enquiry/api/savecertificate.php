<?php
    $base='../../../../../';
    include($base.'_in/connect.php');
    header('content-type: application/json; charset=utf-8');
    header("access-control-allow-origin: *");
    if(isset($_POST['data'])){
      $con=_connect();
      $data=get_object_vars(json_decode($_POST["data"]));

      if (session_status() == PHP_SESSION_NONE) { session_start(); }
      $created_by = $_SESSION['employeeid'];
      $branchid = $_SESSION['branchid'];
                 $certid=$data['certid'];
               
                $enquiryid=$data['enquiryid'];
                $fumdate=$data['fumdate'];
                $fumplace=$data['fumplace'];
                $fumdosage=$data['fumdosage'];
                $humidity=$data['humidity'];

                $fumdurationdays=$data['fumdurationdays'];
                $fumdurationhrs=$data['fumdurationhrs'];
                $fumperformed=$data['fumperformed'];
                
                $issuedate=$data['issuedate'];
                $mintemp=$data['mintemp'];
                $containerno=$data['containerno'];
                $exporter=$data['exporter'];
                $consignee=$data['consignee'];
                $notifiedparty=$data['notifiedparty'];
                $cargo=$data['cargo'];
                $qty=$data['qty'];
                $description=$data['description'];
                $vesselname=$data['vesselname'];
                $loading=$data['loading'];
                $shipingmark=$data['shipingmark'];
                $entryport=$data['entryport'];
                $additional=$data['additional'];
                $destinationcon = $data['destinationcon'];

                //MBR
                $conpressure = $data['conpressure'];
                $confreeair = $data['confreeair'];
                $ventilation = $data['ventilation'];
                $enclosure = $data['enclosure'];
                $commodify = $data['commodify'];
                $plasticwrappingused = $data['plasticwrappingused'];
                $fumigated = $data['fumigated'];
                $plasticwrapping = $data['plasticwrapping'];
                $thick = $data['thick'];
                $dimension = $data['dimension'];
                
                //AFAS
                $fumtarget = $data['fumtarget'];
                $consignment = $data['consignment'];
                $countryorg = $data['countryorg'];
                $importer = $data['importer'];
                $daffdosage = $data['daffdosage'];
                $fumconducted = $data['fumconducted'];
                $targetfum = $data['targetfum'];
                 

                $certType=$data['certType'];
                $create=false;
                $create=mysqli_query($con,"UPDATE certificatemaster SET fumdate='$fumdate',fumplace='$fumplace' ,fumdosage='$fumdosage',fumperformed ='$fumperformed',humidity='$humidity' ,fumdurationdays='$fumdurationdays',fumdurationhrs='$fumdurationhrs',issuedate='$issuedate',mintemp='$mintemp',containerno='$containerno',destinationcon='$destinationcon',exporter='$exporter',consignee='$consignee',notifiedparty='$notifiedparty',cargo='$cargo',qty='$qty',description='$description',vesselname='$vesselname',loading='$loading',shipingmark='$shipingmark',entryport='$entryport',conpressure='$conpressure',confreeair='$confreeair',ventilation='$ventilation',enclosure='$enclosure',commodify='$commodify',plasticwrappingused='$plasticwrappingused',fumigated='$fumigated',plasticwrapping='$plasticwrapping',thick='$thick',dimension='$dimension',fumtarget='$fumtarget',consignment='$consignment',countryorg='$countryorg',importer='$importer',daffdosage='$daffdosage',fumconducted='$fumconducted',targetfum='$targetfum',additional='$additional',created='Y'  WHERE id = '$certid' ");
                
                 $create=mysqli_query($con,"UPDATE crmmaster SET cert_time='$CURRENT_MILLIS' , cert_by='$created_by' WHERE enquiryid = '$enquiryid'");


                if($create){
                    echo '{"status":"success"}';
                }else{
                    echo '{"status":"falid"}';
                }
            _close($con);
     }else{
        echo '{"status":"falid"}';
     }
?>