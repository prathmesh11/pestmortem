<?php
    include($base.'_in/footer.php');
?>
<script>
function confirms(e) {
    if (confirm("Are You Sure TO Confirm Order?")) {
        var link1 = $(e).data('link1');
        var link2 = $(e).data('link2');
        var enquiryid = $(e).data('enquiryid');
        $.ajax({
            type: "POST",
            data: 'enquiryid=' + enquiryid + '&link1=' + link1 + '&link2=' + link2,
            url: 'api/confirm.php',
            cache: false,
            success: function (res) {
                if (res.status == 'success') {
                    window.location.reload();
                }
            }
        })
    }
}

function closes(e) {
    console.log(e);
    
    if (confirm("Are You Sure TO close Order?")) {
        var enquiryid = $(e).data('enquiryid');
        $.ajax({
            type: "POST",
            data: 'enquiryid=' + enquiryid,
            url: 'api/close.php',
            cache: false,
            success: function (res) {
                if (res.status == 'success') {
                    window.location.reload();
                }
            }
        })
    }
}

</script>
