<?php 
$base='../../../../../';

$navenq3='background:#1B1464;';
include('../header.php');

$enqno='0';
if($_GET['enq']){
    $enqno=$_GET['enq'];
}
if($_GET['id']){
    $certid=$_GET['id'];
}
 $enqno=str_replace('ENQ', 'AFAS',$enqno);

if (session_status() == PHP_SESSION_NONE) { session_start(); }
$sessionby = $_SESSION['employeeid'];
$branchid = $_SESSION['branchid'];

$branchjson=json_decode(mysqli_fetch_assoc(mysqli_query($con,"SELECT branchjson x FROM branchmaster WHERE branchid='$branchid'"))['x']);
$customerid=mysqli_fetch_assoc(mysqli_query($con,"SELECT customerid x FROM crmmaster WHERE created_time<>0 AND created_by<>0 AND enquiry_time<>0 AND enquiry_by<>0 AND wo_time<>0 AND wo_by<>0 AND cert_time=0 AND cert_by=0 AND job_all_time=0 AND job_all_by=0 AND job_com_time=0 AND job_com_by=0 AND close_enquiry_time=0 AND close_enquiry_by=0 AND enquiryid='$enqno' ORDER BY created_time DESC"))['x'];

?>
<style>
.table-list td,
   .table-list th {
       border: 1px solid #ddd;
       padding: 1px !important;
       font-size: 13px;
   }
   .table-list td{
	padding-top: 10px !important;
   }

   .table-list tr:nth-child(even) {
       background-color: #f2f2f2;
   }

   .table-list th {
       padding-top: 5px;
       padding-bottom: 5px;
       text-align: center;
       background-color: #16a085;
       color: white;
   }
</style>
<br>
<div class="col-sm-2"></div>
<div class="col-sm-8" style="background:#fff;">
<div class="row">
<div class="col-sm-12" align="center">
<h3><b>PEST MORTEM (INDIA) PRIVATE LIMITED</b></h3>
<h4><b>(Approved by National Plant Protection Organization, Government of India)</b></h4>
<h4><?php echo get_object_vars($branchjson)['address']; ?></h4>
 <h4><b>AFAS-METHYL BROMIDE FUMIGATION CERTIFICATE</b></h4>
 </div>
<div id="alpcert">
<input type="hidden" data-name="enquiryid" value="<?php echo $enqno; ?>">

<div class="col-sm-6"><b>CERTIFICATE NO. : <?php echo $enqno; ?></b></div>

<div class="col-sm-6"  align="right"><b>AEI No. : IN 0012 MB</b></div>
 
 <div class="col-sm-12">
 
 <BR>
<div class="col-sm-12" align="center"><b>TARGET OF FUMIGATION DETAILS</b></div>

 <hr style="border:1px solid #000;">
 <div class="col-sm-12">
<div class="col-sm-3"> Target of Fumigation :  </div>
<div class="col-sm-9">
<input type="radio" data-name="fumtarget" >Commodify &nbsp;
<input type="radio" data-name="fumtarget" >Packing &nbsp;
<input type="radio" data-name="fumtarget" >Both Commodify and Packing
</div>
</div>
<div class="col-sm-12">
<div class="col-sm-3"> Commodify :  </div><div class="col-sm-3"><input type="text" data-name="commodify" class="form-control"></div>
<div class="col-sm-3"> Quantity : </div><div class="col-sm-3"><input type="text" data-name="qty" class="form-control"></div>
</div>
<div class="col-sm-12">
<div class="col-sm-3"> Consignment Link :  </div><div class="col-sm-3"><input type="text" data-name="consignment" class="form-control"></div>
<div class="col-sm-3"> Country of origin :  </div><div class="col-sm-3"><input type="text" data-name="countryorg" class="form-control"></div>

</div>
<div class="col-sm-12">
<div class="col-sm-3"> Port of loading :  </div><div class="col-sm-3"><input type="text" data-name="loading" class="form-control"></div>
<div class="col-sm-3"> Country of destination : </div><div class="col-sm-3"><input type="text" data-name="destinationcon" class="form-control"></div>
</div>
<br>
 <hr style="border:1px solid #000;">
 <div class="col-sm-12">
<div class="col-sm-6"> Name and Address of exporter : <textarea rows="5" cols="25" data-name="exporter" class="form-control"></textarea> </div>


<div class="col-sm-6"> Name and Address of importer : <textarea rows="5" cols="25" data-name="importer" class="form-control"></textarea> </div>
</div>
 <hr style="border:1px solid #000;">
<div class="col-sm-12" align="center"><b>TREATMENT DETAILS</b></div>

<div class="col-sm-12">
<div class="col-sm-3"> Date of fumigation completed : </div><div class="col-sm-3"><input type="date" data-name="fumdate" class="form-control"></div>
<div class="col-sm-3"> Place of fumigation : </div><div class="col-sm-3"><input type="text" data-name="fumplace" class="form-control"></div>
</div>


<div class="col-sm-12">
<div class="col-sm-3"> DAFF Prescribed dose rate(g/m<sup>3</sup>) : </div><div class="col-sm-3"><input type="text" data-name="daffdosage" class="form-control"></div>
<div class="col-sm-3"> Exposure Period(hrs) : </div><div class="col-sm-3"><input type="text" data-name="fumdurationhrs" class="form-control"></div>
</div>



<div class="col-sm-12">
<div class="col-sm-3">Forecast minimum temp(&#8451;):</div><div class="col-sm-3"><input type="text" data-name="mintemp" class="form-control"></div>
<div class="col-sm-3"> Applied dose rate(g/m<sup>3</sup>) :</div><div class="col-sm-3"><input type="text" data-name="fumdosage" class="form-control"></div>
</div>

<BR>
<BR>
<hr style="border:1px solid #000;">

<div class="col-sm-12">
<div class="col-sm-4"> How was the fumigation conducted ?   </div>
<div class="col-sm-8">
<input type="radio" data-name="fumconducted" value="US">Un-sheeted Container &nbsp;
<input type="radio" data-name="fumconducted" value="S" >Sheeted Container/s &nbsp;
<input type="radio" data-name="fumconducted" value="C" >Chember &nbsp;<br>
<input type="radio" data-name="fumconducted" value="PT" >Pressure Tested Container &nbsp;
<input type="radio" data-name="fumconducted" value="SS" >Sheeted Stack &nbsp;


</div>
</div>
<div class="col-sm-12">
<div class="col-sm-4">Container Number/s(Where applicable):</div><div class="col-sm-8"><input type="text" data-name="containerno" class="form-control"></div>
 
</div>

<hr style="border:1px solid #000;">

<div class="col-sm-12">
<div class="col-sm-6"> Does the target of the fumigation conform to the DAFF plastic wrapping, Impervious surface and timber thickness requirements at the time of fumgation?   </div>
<div class="col-sm-6">
<input type="radio" data-name="targetfum" value="Y" >YES &nbsp;
<input type="radio" data-name="targetfum" value="N">NO &nbsp;
 

</div>
</div>

<hr style="border:1px solid #000;">
<div class="col-sm-12">
<div class="col-sm-4">Ventilation:</div><div class="col-sm-8"><input type="text" data-name="ventilation" class="form-control"></div>
 
</div>

<hr style="border:1px solid #000;">
<div class="col-sm-12" align="center"><b>DECLARATION</b></div>
By signing below, I, the AFAS accredited fumigator responsible, declare the these details are true and correct and the Fumigation has been carried out in accordance with all the requirements in the DAFF Methyl Bromide Fumigation Standard.
<hr style="border:1px solid #000;">
 <div class="col-sm-12">
<div class="col-sm-4"><b> Additional Declaration : </b></div><div class="col-sm-8"><textarea rows="3" cols="25" data-name="additional" class="form-control"></textarea> </div>
</div> 
 

<BR><BR>
<button onclick="savecertificate();" class="btn btn-success btn-sm ">Save Certificate</button>
 
<input type="hidden" data-name="certid" value="<?php echo $certid; ?>">
<input type="hidden" data-name="enquiryid" value="<?php echo $enqno1; ?>">

</div>
<?php
include('../footer.php');
?>
<script>
$('body').css('background','#eee');
function funtb(e){
    var arr=[];
    $('.tds').each(function (){
        var serv=$(this).find('.serv').text().trim();
        var yesno=$(this).find('.yesno').val();
        var frq=$(this).find('.frq').val();
        if(yesno!='NO' && frq!='Nil'){
            arr.push({
                "serv":serv,
                "yesno":yesno,
                "frq":frq
            });
            var str='<table class="table-list table">';
            str+='<thead>';
            str+='<tr>';
            str+='<th>Service</th>'; 
            str+='<th>Freq.</th>';
            str+='<th>Start Date</th>';
            str+='</tr>';
            str+='</thead>';  
            str+='<tbody>'; 
            str+='<tr>';
            str+='<td>';
            for(var i in arr){
            str+=arr[i].serv+'<br>';
            }
            str+='</td>';
            str+='<td><input type="number" onkeyup="coter(this)"></td>';
            str+='<td id="coter">';

            str+='</td>';
            str+='</tr>';
            str+='</tbody>'; 
            str+='</table>'; 
            $('#table-ui').html(str);
        }

    })
}
function coter(e){
var cot=$(e).val().trim();
if(cot!=''){
cot=parseInt(cot);
var str='<table class="table">';
str+='<thead>';
str+='<tr>';
str+='<th>Sr. No.</th>';
str+='<th>Service Date</th>';
str+='</tr>';
str+='</thead>';
str+='<tbody>';
for(var i=1;i<=cot;i++){
    str+='<tr>';
    str+='<td>'+i+'</td>';
    str+='<td><input type="date"></td>';
    str+='</tr>';
}
str+='</tbody>';
str+='</table>';
console.log(str);

$('#coter').html(str);
}
}
function savecertificate() {
  //$('#newqneuiry').hide();
  var valid = true;
  if (checker('alpcert') != false) {
    valid = valid * true;
  } else {
    valid = valid * false;
  }
  if (valid) {
    var data=checker('alpcert');
    var datastr = JSON.stringify(data);
    //alert(datastr);
  
    $.ajax({
        type: "POST",
        data: {
          data: datastr
        },
        url: '../api/savecertificate.php',
        cache: false,
        success: function (res) {
          if (res.status == 'success') {
         
             window.location= "../job-allotment.php";
           
          }
        }
      })
    }
  }
  
</script>