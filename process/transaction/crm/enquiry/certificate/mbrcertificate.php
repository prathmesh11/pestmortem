<?php 
$base='../../../../../';

$navenq3='background:#1B1464;';
include('../header.php');

$enqno='0';
if($_GET['enq']){
    $enqno=$_GET['enq'];
}
if($_GET['id']){
    $certid=$_GET['id'];
}

$enqno1 = $enqno;
 $enqno=str_replace('ENQ', 'MBR',$enqno);

if (session_status() == PHP_SESSION_NONE) { session_start(); }
$sessionby = $_SESSION['employeeid'];
$branchid = $_SESSION['branchid'];

$branchjson=json_decode(mysqli_fetch_assoc(mysqli_query($con,"SELECT branchjson x FROM branchmaster WHERE branchid='$branchid'"))['x']);
$customerid=mysqli_fetch_assoc(mysqli_query($con,"SELECT customerid x FROM crmmaster WHERE created_time<>0 AND created_by<>0 AND enquiry_time<>0 AND enquiry_by<>0 AND wo_time<>0 AND wo_by<>0 AND cert_time=0 AND cert_by=0 AND job_all_time=0 AND job_all_by=0 AND job_com_time=0 AND job_com_by=0 AND close_enquiry_time=0 AND close_enquiry_by=0 AND enquiryid='$enqno' ORDER BY created_time DESC"))['x'];

?>
<style>
.table-list td,
   .table-list th {
       border: 1px solid #ddd;
       padding: 1px !important;
       font-size: 13px;
   }
   .table-list td{
	padding-top: 10px !important;
   }

   .table-list tr:nth-child(even) {
       background-color: #f2f2f2;
   }

   .table-list th {
       padding-top: 5px;
       padding-bottom: 5px;
       text-align: center;
       background-color: #16a085;
       color: white;
   }
</style>
<br>
<div class="col-sm-2"></div>
<div class="col-sm-8" style="background:#fff;">
<div class="row">
<div class="col-sm-12" align="center">
<h3><b>PEST MORTEM (INDIA) PRIVATE LIMITED</b></h3>
<h4><b>(Approved by National Plant Protection Organization, Government of India)</b></h4>
<h4><?php echo get_object_vars($branchjson)['address']; ?></h4>
 <h4><b>FUMIGATION CERTIFICATE</b></h4>
 <hr style="border:1px solid #000;">
</div>
<div id="alpcert">
<input type="hidden" data-name="enquiryid" value="<?php echo $enqno1; ?>">
<?php 
  $enqstr = explode("/",$enqno);
  $srno = $enqstr[3];   
?>
<div class="col-sm-6"><b>Sr. No. : <?php echo $certid; ?></b></div>

<div class="col-sm-6"  align="right"><b>Certificate No. : <?php echo $enqno; ?></b></div>
<div class="col-sm-6"><b>Dte. PPQS Regd. No.:  030/ALP Dated:</b></div>

<div class="col-sm-3" align="right"><b>Date of issue : </b></div><div class="col-sm-3"><input type="date" data-name="issuedate" class="form-control"></div>
 <div class="col-sm-12">
<hr style="border:1px solid #000;">
 <I>
     This is to certify that following regulated articles have been fumigated according to the appropriate procedures to conform to the current phytosanitary regulations of the importing country.

 </I>
 </div>
  <br> <br>
 <hr style="border:1px solid #000;">
 
<div class="col-sm-12"><b>Details of Goods</b></div>
  <div class="col-sm-12">
 <div class="col-sm-4">Description of goods : </div><div class="col-sm-8"><textarea rows="3" cols="25" data-name="description" class="form-control"></textarea></div>
 </div>
 <div class="col-sm-12">
<div class="col-sm-4">Quantity declared : </div><div class="col-sm-8"><textarea rows="3" cols="25" data-name="qty" class="form-control"></textarea> </div>
</div>
<div class="col-sm-12">
<div class="col-sm-4">Distinguishing marks: </div><div class="col-sm-8"><textarea rows="3" cols="25" data-name="shipingmark" class="form-control"></textarea> </div>
</div>
<div class="col-sm-12">
<div class="col-sm-4">Consignment link/ Container No.: </div><div class="col-sm-8"><input type="text" data-name="containerno" class="form-control"></div>
</div>
 
<div class="col-sm-12">
<div class="col-sm-4">  Port & country loading :  </div><div class="col-sm-8"><input type="text" data-name="loading" class="form-control"> </div>
</div>

<div class="col-sm-12">
<div class="col-sm-4"> Country of destination:  </div><div class="col-sm-8"><input type="text" data-name="destinationcon" class="form-control"> </div>
</div>
<div class="col-sm-12">
<div class="col-sm-4">  Name of Vessel : </div><div class="col-sm-8"><input type="text" data-name="vesselname" class="form-control"> </div>
</div>
<div class="col-sm-12">
<div class="col-sm-4">  Port of entry : </div><div class="col-sm-8"><input type="text" data-name="entryport" class="form-control"> </div>
</div>
 
 <div class="col-sm-12">
<div class="col-sm-4">  Name and Address of exporter :  </div><div class="col-sm-8"><textarea rows="3" cols="25" data-name="exporter" class="form-control"></textarea> </div>
</div>
<div class="col-sm-12">
<div class="col-sm-4"> Name and Address of consignee :  </div><div class="col-sm-8"><textarea rows="3" cols="25" data-name="consignee" class="form-control"></textarea> </div>
</div>

 <br> <br>
<div class="col-sm-12"><b>Details of Treatment</b></div>
<div class="col-sm-12">
<div class="col-sm-3">  Name of fumigant :  </div><div class="col-sm-3">METHYL BROMIDE</div>
<div class="col-sm-3">  Date of fumigation :  </div><div class="col-sm-3"><input type="date" data-name="fumdate" class="form-control"></div>
</div>
<div class="col-sm-12">
<div class="col-sm-3">  Place of fumigation :  </div><div class="col-sm-3"><input type="text" data-name="fumplace" class="form-control"></div>
<div class="col-sm-3">  Dosage of fumigant :  </div><div class="col-sm-3"><input type="text" data-name="fumdosage" class="form-control"></div>
</div>
<div class="col-sm-12">
<div class="col-sm-3"> Duration of fumigation :  </div><div class="col-sm-2">
 
<select data-name="fumdurationhrs"  >
<?php 
for($i=1;$i<=100;$i++){
?>
<option value="<?php echo $i;?>"><?php echo $i;?></option> 
<?php } ?>
</select>HRS</div> 
 <div class="col-sm-1"></div>
<div class="col-sm-3">  Mini. air Temp. : (&#8451;) </div><div class="col-sm-3"><input type="text" data-name="mintemp" class="form-control"></div>
</div>
<div class="col-sm-12">
<div class="col-sm-6">Fumigation performed in under gas tight sheet</div>
<div class="col-sm-3">
  <select data-name="fumperformed"><option value="NO">NO</option><option value="YES">YES</option></select></div>
</div>
 
<div class="col-sm-12">
<div class="col-sm-6">Container pressure test conducted</div>
<div class="col-sm-3">
  <select data-name="conpressure"><option value="NA">NA</option><option value="NO">NO</option><option value="YES">YES</option></select></div>
</div>

<div class="col-sm-12">
<div class="col-sm-6">Container has 200mm. free air space at the top of container</div>
<div class="col-sm-3">
  <select data-name="confreeair"><option value="NA">NA</option><option value="NO">NO</option><option value="YES">YES</option></select></div>
</div>

<div class="col-sm-12">
<div class="col-sm-6">In transit fumigation-Needs ventilation at port of discharge</div>
<div class="col-sm-3">
  <select data-name="ventilation"><option value="NA">NA</option><option value="NO">NO</option><option value="YES">YES</option></select></div>
</div>

<div class="col-sm-12">
<div class="col-sm-6">Enclosure has been ventilated to below 5 ppm v/v Methyl Bromide</div>
<div class="col-sm-3">
  <select data-name="enclosure"><option value="NA">NA</option><option value="NO">NO</option><option value="YES">YES</option></select></div>
</div>
 <br> <br>
<div class="col-sm-12"><b>Wrapping And Timber</b></div>
<div class="col-sm-12">
<div class="col-sm-6">Has the commodify been fumigated prior to lacquering, vanishing, painting or wrapping</div>
<div class="col-sm-3">
  <select data-name="commodify"><option value="NA">NA</option><option value="NO">NO</option><option value="YES">YES</option></select></div>
</div>

<div class="col-sm-12">
<div class="col-sm-6">Has plastic wrapping been used in the consignment ?</div>
<div class="col-sm-3">
  <select data-name="plasticwrappingused"><option value="NA">NA</option><option value="NO">NO</option><option value="YES">YES</option></select></div>
</div>

<div class="col-sm-12">
<div class="col-sm-6"> * If yes, has the Consignment been fumigated prior to wrapping ?</div>
<div class="col-sm-3">
  <select data-name="fumigated"><option value="NA">NA</option><option value="NO">NO</option><option value="YES">YES</option></select></div>
</div>

<div class="col-sm-12">
<div class="col-sm-6"> * Or has the plastic wrapping been slashed, opened, or perforated in accordance with the wrapping and perforation Standard ?</div>
<div class="col-sm-3">
  <select data-name="plasticwrapping"><option value="NA">NA</option><option value="NO">NO</option><option value="YES">YES</option></select></div>
</div>

<div class="col-sm-12">
<div class="col-sm-6">Is the Timber in this consignment less than 200mm thick in one dimension and correctly spaced every 200mm in height?</div>
<div class="col-sm-3">
  <select data-name="thick"><option value="NA">NA</option><option value="NO">NO</option><option value="YES">YES</option></select></div>
</div>
 

<div class="col-sm-12">
<div class="col-sm-4"><b> Additional Declaration : </b></div>
<div class="col-sm-10"><textarea rows="3" cols="25" data-name="additional" class="form-control"></textarea> </div>
 </div>

 I declare that these details are true & correct and the fumigation has been carried out in accordance with the  NSPM-12.

<BR><BR>
<button onclick="savecertificate();" class="btn btn-success btn-sm ">Save Certificate</button>
<input type="hidden" data-name="certType" value="MBR">
<input type="hidden" data-name="certid" value="<?php echo $certid; ?>">
<input type="hidden" data-name="enquiryid" value="<?php echo $enqno1; ?>">


</div>
<?php
include('../footer.php');
?>
<script>
$('body').css('background','#eee');
function funtb(e){
    var arr=[];
    $('.tds').each(function (){
        var serv=$(this).find('.serv').text().trim();
        var yesno=$(this).find('.yesno').val();
        var frq=$(this).find('.frq').val();
        if(yesno!='NO' && frq!='Nil'){
            arr.push({
                "serv":serv,
                "yesno":yesno,
                "frq":frq
            });
            var str='<table class="table-list table">';
            str+='<thead>';
            str+='<tr>';
            str+='<th>Service</th>'; 
            str+='<th>Freq.</th>';
            str+='<th>Start Date</th>';
            str+='</tr>';
            str+='</thead>';  
            str+='<tbody>'; 
            str+='<tr>';
            str+='<td>';
            for(var i in arr){
            str+=arr[i].serv+'<br>';
            }
            str+='</td>';
            str+='<td><input type="number" onkeyup="coter(this)"></td>';
            str+='<td id="coter">';

            str+='</td>';
            str+='</tr>';
            str+='</tbody>'; 
            str+='</table>'; 
            $('#table-ui').html(str);
        }

    })
}
function coter(e){
var cot=$(e).val().trim();
if(cot!=''){
cot=parseInt(cot);
var str='<table class="table">';
str+='<thead>';
str+='<tr>';
str+='<th>Sr. No.</th>';
str+='<th>Service Date</th>';
str+='</tr>';
str+='</thead>';
str+='<tbody>';
for(var i=1;i<=cot;i++){
    str+='<tr>';
    str+='<td>'+i+'</td>';
    str+='<td><input type="date"></td>';
    str+='</tr>';
}
str+='</tbody>';
str+='</table>';
console.log(str);

$('#coter').html(str);
}
}
function savecertificate() {
  //$('#newqneuiry').hide();
  var valid = true;
  if (checker('alpcert') != false) {
    valid = valid * true;
  } else {
    valid = valid * false;
  }
  if (valid) {
    var data=checker('alpcert');
    var datastr = JSON.stringify(data);
    //alert(datastr);
  
    $.ajax({
        type: "POST",
        data: {
          data: datastr
        },
        url: '../api/savecertificate.php',
        cache: false,
        success: function (res) {
          if (res.status == 'success') {
         
              window.location= "../job-allotment.php";
           
          }
        }
      })
    }
  }
  
</script>