<?php 
$base='../../../../../';

$navenq2='background:#1B1464;';
include('../header.php');

$enqno='0';
if($_GET['enq']){
    $enqno=$_GET['enq'];
}

if (session_status() == PHP_SESSION_NONE) { session_start(); }
$sessionby = $_SESSION['employeeid'];
$branchid = $_SESSION['branchid'];

$branchjson=json_decode(mysqli_fetch_assoc(mysqli_query($con,"SELECT branchjson x FROM branchmaster WHERE branchid='$branchid'"))['x']);
$customerid=mysqli_fetch_assoc(mysqli_query($con,"SELECT customerid x FROM crmmaster WHERE created_time<>0 AND created_by<>0 AND enquiry_time<>0 AND enquiry_by<>0 AND wo_time=0 AND wo_by=0 AND cert_time=0 AND cert_by=0 AND job_all_time=0 AND job_all_by=0 AND job_com_time=0 AND job_com_by=0 AND close_enquiry_time=0 AND close_enquiry_by=0 AND enquiryid='$enqno' ORDER BY created_time DESC"))['x'];

?>
<style>
.table-list td,
   .table-list th {
       border: 1px solid #ddd;
       padding: 1px !important;
       font-size: 13px;
   }
   .table-list td{
	padding-top: 10px !important;
   }

   .table-list tr:nth-child(even) {
       background-color: #f2f2f2;
   }

   .table-list th {
       padding-top: 5px;
       padding-bottom: 5px;
       text-align: center;
       background-color: #16a085;
       color: white;
   }
</style>
<br>
<div class="col-sm-2"></div>
<div class="col-sm-8" style="background:#fff;">
<div class="row">
<div class="col-sm-12" align="center">
<h3><b>PEST MORTEM (INDIA) PRIVATE LIMITED</b></h3>
<h4><b>(Approved by National Plant Protection Organization, Government of India)</b></h4>
<h4><?php echo get_object_vars($branchjson)['address']; ?></h4>
<h4>AN ISO 9001 : 2008 CERTIFIED COMPANY</h4>
<h4><b>AN ISO 9001 : 2008 CERTIFIED COMPANY</b></h4>
<h4><b>EXPERT IN</b></h4>
<h5>PEST CONTROL SERVICES AND FUMIGATION OF EXPORT CARGO, SHIPS, CONTAINERS</h5>
<h4><b>WORK CONTRACT</b></h4>
<hr style="border:1px solid #000;">
</div>
<div class="col-sm-6"><b>Contract No. : <?php echo $enqno; ?></b></div>
<div class="col-sm-6" align="right"><b>Dated : <?php echo date("d/m/Y"); ?></b></div>
<div class="col-sm-6"><b>Period : </b><input type="date"> TO <input type="date"></div>
<div class="col-sm-6"></div>
<div class="col-sm-12">
<hr style="border:1px solid #000;">
In consultation and as agreed upon a sum of Rs <input type="number"> + GST <input type="number"> % Rs. <input type="number" readonly> = Rs. <input type="number"> (Rupees Twenty Thousand only)
<br>
to be paid By <span id="customername"></span> (herein after called the owners)<br>
<span id="customeraddress"></span><br>
<span id="contractperson"></span><br>
to M/S. Pest Control Mortem (India) Pvt. Ltd. <?php echo get_object_vars($branchjson)['companybranchname'];?> (herein after called the contractor).<br>
<b>The Contractor Shall Carry out the Following pest control serices in the premises of owners viz.</b>
<hr style="border:1px solid #000;">
<?php
                    $counter=1;
                    $result4=mysqli_query($con,"SELECT servicename FROM servicemaster ORDER BY id ASC" );
                     while($rows3 = mysqli_fetch_assoc($result4)){
                       
                            echo '<div class="col-sm-12 tds">'; 
                            if($counter==1){
                                echo '<div class="col-sm-6"><b>Type of Services</b></div>';
                                echo '<div class="col-sm-3"><b>Yes/No</b></div>';
                                echo '<div class="col-sm-3"><b>Frequency</b></div>';
                             
                            }
                            echo '<div class="col-sm-6"><span class="serv">'. $rows3['servicename'].'</span></div>';
                            echo '<div class="col-sm-3 "><select class="yesno" onchange="funtb(this)"><option value="NO">NO</option><option value="YES">YES</option></select></div>';
                            echo '<div class="col-sm-3 "><select class="frq" onchange="funtb(this)"><option value="Nil">Nil</option><option value="Daily">Daily</option><option value="Monthly">Monthly</option><option value="Quarterly">Quarterly</option><option value="Half Yearly">Half Yearly</option><option value="Yearly">Yearly</option></select></div>';
                            echo '</div>';
                            $counter++;
                    
                    }
    ?>
    <hr style="border:1px solid #000;">
    <div id="table-ui"></div>
    <hr style="border:1px solid #000;">
    <b>Installation of Equipment</b><br>
    <?php
                    $result4=mysqli_query($con,"SELECT machinename FROM machinemaster WHERE asset='Equipment' ORDER BY id ASC" );
                     while($rows3 = mysqli_fetch_assoc($result4)){              
                            echo '<div class="col-sm-6">'; 
                            echo '<div class="col-sm-6">'. $rows3['machinename'].'</div>';
                            echo '<div class="col-sm-3"><select><option value="NO">NO</option><option value="YES">YES</option></select></div>';
                            echo '<div class="col-sm-3"><select><option value="Nil">Nil</option><option value="Monthly">Monthly</option><option value="Quarterly">Quarterly</option><option value="Half Yearly">Half Yearly</option><option value="Yearly">Yearly</option></select></div>';
                            echo '</div>';
                    }
    ?>
    <div style="clear:both;"></div>
    <hr style="border:1px solid #000;">
   <b> Service Locations</b> <br>
    <input type="text">
    <hr style="border:1px solid #000;">
    <b>Additional Declaration (if any)</b><br>
     <input type="text">
    <hr style="border:1px solid #000;">
    <b>Payment acceptable by</b><br>
    Mode of payment :  <input type="text"><br>
    No Refund will be considered under any circumstances.<br>
    No libility attaches to or assumed by the company, its Director or its representatives in respect of this contract.<br>
    <b>Please see the Terms and conditions overleaf.</b>
    Please sign and return copy of the contract as token of acceptance. 
    <br><br>
    <div class="col-sm-6">
    <b>FOR PEST MORTEM (INDIA) PVT. LTD</b></div>
    <div class="col-sm-6" align="right"><b>FOR OWNERS</b><br>Name:<br>Mob. No. <br>Tel. No.</div>

</div>
</div>
<br>
<br>
</div>
<br>
<br>

<?php
include('../footer.php');
?>
<script>
$('body').css('background', '#eee');

function funtb(e) {
    var arr = [];
    $('.tds').each(function () {
        var serv = $(this).find('.serv').text().trim();
        var yesno = $(this).find('.yesno').val();
        var frq = $(this).find('.frq').val();
        if (yesno != 'NO' && frq != 'Nil') {
            arr.push({
                "serv": serv,
                "yesno": yesno,
                "frq": frq
            });
            var str = '<table class="table-list table">';
            str += '<thead>';
            str += '<tr>';
            str += '<th>Service</th>';
            str += '<th>Freq.</th>';
            str += '<th>Start Date</th>';
            str += '</tr>';
            str += '</thead>';
            str += '<tbody>';
            str += '<tr>';
            str += '<td>';
            for (var i in arr) {
                str += arr[i].serv + '<br>';
            }
            str += '</td>';
            str += '<td><input type="number" onkeyup="coter(this)"></td>';
            str += '<td id="coter">';

            str += '</td>';
            str += '</tr>';
            str += '</tbody>';
            str += '</table>';
            $('#table-ui').html(str);
        }

    })
}

function coter(e) {
    var cot = $(e).val().trim();
    if (cot != '') {
        cot = parseInt(cot);
        var str = '<table class="table">';
        str += '<thead>';
        str += '<tr>';
        str += '<th>Sr. No.</th>';
        str += '<th>Service Date</th>';
        str += '</tr>';
        str += '</thead>';
        str += '<tbody>';
        for (var i = 1; i <= cot; i++) {
            str += '<tr>';
            str += '<td>' + i + '</td>';
            str += '<td><input type="date"></td>';
            str += '</tr>';
        }
        str += '</tbody>';
        str += '</table>';
        console.log(str);

        $('#coter').html(str);
    }
}

</script>