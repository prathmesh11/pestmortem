<?php

$css = '<link rel="stylesheet" href="'.$base.'css/classic.css">
<link rel="stylesheet" href="'.$base.'css/classic.date.css">
<link rel="stylesheet" href="'.$base.'css/classic.time.css">
<link rel="stylesheet" href="'.$base.'css/grid.min.css">';

$js = '<script src="'.$base.'js/picker.js"></script>
<script src="'.$base.'js/picker.date.js"></script>
<script src="'.$base.'js/picker.time.js"></script>
<script src="'.$base.'js/pdfmake.min.js"></script><script src="'.$base.'js/vfs_fonts.js"></script>
<script src="'.$base.'js/fwork.js"></script>';

    include($base.'_in/header.php');
    include($base.'_in/connect.php');
    $con = _connect();
   
?>
<style>
    .th{
        background:#8e44ad;
        color:#fff;
        text-align:center;
        padding-top:2px;
        padding-bottom:2px;
        border:1px solid #fff;
    }
    .td{
        border:1px solid #ddd;
    }
    .table-ui .btn{
        margin:3px;
    }
    #myModal1 .col-sm-4,.col-sm-8{
      margin-bottom:2px;
    }
    .picker__select--year{
        height:auto;
    }
    .picker__select--month{
        height:auto;
    }
    </style>
<div class="container-fluid">
    <div id="div-content" class="content">
<table width="100%" >
<tr>
<td align="center" style="width:10%"><a href="/process/transaction/crm/enquiry/index.php" style="border:1px solid blue;border-radius:0px;<?php echo $navenq1;?>" class="btn btn-primary btn-block">Enquiry & FollowUp</td>
<td align="center" style="width:10%"><a href="/process/transaction/crm/enquiry/workorder.php" style="border:1px solid blue;border-radius:0px;<?php echo $navenq2;?>" class="btn btn-primary btn-block">Work Contract</td>
<td align="center" style="width:10%"><a href="/process/transaction/crm/enquiry/certification.php" style="border:1px solid blue;border-radius:0px;<?php echo $navenq3;?>" class="btn btn-primary btn-block">Certification</td>
<td align="center" style="width:10%"><a href="/process/transaction/crm/enquiry/job-allotment.php" style="border:1px solid blue;border-radius:0px;<?php echo $navenq4;?>" class="btn btn-primary btn-block">Job Allotment</td>
<td align="center" style="width:10%"><a href="/process/transaction/crm/enquiry/job-completed.php" style="border:1px solid blue;border-radius:0px;<?php echo $navenq5;?>" class="btn btn-primary btn-block">Job Completed</td>
<td align="center" style="width:10%"><a href="/process/transaction/crm/enquiry/close-enquiry.php" style="border:1px solid blue;border-radius:0px;<?php echo $navenq6;?>" class="btn btn-primary btn-block">Close Enquiry</td>
</tr>
</table>
