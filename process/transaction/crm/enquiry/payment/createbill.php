<?php 
$base='../../../../../';

$navenq6='background:#1B1464;';
include('../header.php');

$enqno='0';
if($_GET['enq']){
    $enqno=$_GET['enq'];
}

if (session_status() == PHP_SESSION_NONE) { session_start(); }
$sessionby = $_SESSION['employeeid'];
$branchid = $_SESSION['branchid'];

$branchjson=json_decode(mysqli_fetch_assoc(mysqli_query($con,"SELECT branchjson x FROM branchmaster WHERE branchid='$branchid'"))['x']);

$customerid=mysqli_fetch_assoc(mysqli_query($con,"SELECT customerid x FROM crmmaster WHERE enquiryid='$enqno' ORDER BY created_time DESC"))['x'];
$contactjson=json_decode(mysqli_fetch_assoc(mysqli_query($con,"SELECT contactjson x FROM customermaster WHERE customerid='$customerid'"))['x'])[0];

$mobilecontact=get_object_vars($contactjson)['mobile'];
$landlinecontact=get_object_vars($contactjson)['landline'];
$emailcontact=get_object_vars($contactjson)['email'];

//$custjson=json_decode(mysqli_fetch_assoc(mysqli_query($con,"SELECT customerjson x FROM customermaster WHERE customerid='$customerid'"))['x'])[0];
$cifo=mysqli_fetch_assoc(mysqli_query($con,"SELECT * FROM customermaster WHERE customerid='$customerid'"));
$namecontact=get_object_vars(json_decode($cifo['customerjson']))['customername'];

$addresscontact=get_object_vars(json_decode($cifo['customerjson']))['address'];
$gstno = $cifo['gstnumber'];

$wcifo=mysqli_fetch_assoc(mysqli_query($con,"SELECT * FROM workcontract WHERE enquiryid='$enqno'"));

$servicename=get_object_vars(json_decode($wcifo['servicejson']))['servicename'];

$itemname=get_object_vars(json_decode($wcifo['itemjson']))['itemname'];
$rate=get_object_vars(json_decode($wcifo['itemjson']))['rate'];
$SAC=get_object_vars(json_decode($wcifo['itemjson']))['SAC'];
$qty = 1;
$total = $qty * $rate;
$contractdate = date("d-m-Y",$wcifo['contractdate']/1000);
$contractstart = date("d-m-Y",$wcifo['contractstart']/1000);
$contractend = date("d-m-Y",$wcifo['contractend']/1000);
$contractid = $wcifo['contractid'];
         
$contractamt = $wcifo['netamount'];

$wcbifo=mysqli_fetch_assoc(mysqli_query($con,"SELECT * FROM workcontractbill order by id desc limit 1"));
$billid = $wcbifo['id']+1;
?>
<style>
.table-list td,
   .table-list th {
       border: 1px solid #ddd;
       padding: 1px !important;
       font-size: 13px;
   }
   .table-list td{
	padding-top: 10px !important;
   }

   .table-list tr:nth-child(even) {
       background-color: #f2f2f2;
   }

   .table-list th {
       padding-top: 5px;
       padding-bottom: 5px;
       text-align: center;
       background-color: #16a085;
       color: white;
   }
</style>
<br>
<div class="col-sm-2"></div>
<div class="col-sm-8" style="background:#fff;">
<div class="row">
<div class="col-sm-12" align="center">
<h3><b>PEST MORTEM (INDIA) PRIVATE LIMITED</b></h3>
<h4><b>(Estd. 1980)</b></h4>
<h5><?php echo "Regd. Office. :".get_object_vars($branchjson)['address']; ?></h5>
<h5><?php echo "Tel. :".get_object_vars($branchjson)['phone'] ."  Email :".get_object_vars($branchjson)['email'] ; ?></h5>
   
<hr style="border:1px solid #000;">
</div>
<?php 
$WOC=str_replace('ENQ', 'WOC',$enqno);
?> 
<div class="col-sm-12">
 <table width="800">
   <tr >
    <td width="500">
    <table>
    <tr>
      <td width="100">
      <b>  M/S.: </b>
    </td>
    <td><?php echo $addresscontact; ?></td>
      </tr>
      <tr>
      <td>

       <b> GSTIN : </b>
    </td>
    <td><?php echo $gstno; ?></td>
    </tr>
    <tr>
      <td>
       <b> A/C :  </b>
    </td>
    <td>  </td>
    </tr>
    <tr>
      <td>
       <b> STATE: </b>
    </td>
    <td> </td>
    </tr>
    </table>


    </td>
<td width="300">
<table>
<tr>
      <td>
       <b>INVOICE NO. : </b>
    </td>
    <td><?php echo $billid;?></td>
    </tr>
    <tr>
      <td>
        <b>INVOICE DATE: </b>
    </td>
    <td><?php echo date("d-m-Y");?></td>
    </tr>
    <tr>
      <td>
       <b> CONTRACT NO. :  </b>
    </td>
    <td> <?php echo $contractid;  ?></td>
    </tr>
    <tr>
      <td>
        <b> CONTRACT DATE : </b>
    </td>
    <td> <?php echo $contractdate; ?></td>
    </tr>
    </table>

</td>

</tr>
</table>
    
</div>
<input type="hidden" name="henqno" id="henqno" value="<?php  echo $enqno; ?>">
<div class="col-sm-12">
  <table border = "1" width="800" height="400">
  <td width="300" align= "center"><b> P A R T I C U L A R S  </b></td>
  <td width="50"  align= "center"> <b>SAC</b></td>
  <td width="50"  align= "center"> <b>QTY</b></td>
  <td width="100" align= "center"> <b>RATE</b></td>
  <td width="100" align= "center"> <b>TOTAL</b></td>
  <tr>
  <td >
   
    <?php echo strtoupper($itemname);?>
<BR><BR>
     SERVICE : <?php echo $servicename; ?> <br>
  CONTRACT PERIOD : <?php echo $contractstart; ?> TO <?php echo $contractend; ?><BR>
  PO NO. : <input type="text" size = '2' name="pono" id="pono"> DATED : <?php echo $contractdate; ?> <BR>
 
<BR><BR>
 <input type="text" size = '8' name="contractamt" id="contractamt" value="<?php echo number_format($contractamt,2); ?>" readonly > : TOTAL CONTRACT AMOUNT<br>
  <input type="hidden" id="hnet" value="<?php echo $contractamt; ?>">
  <input type="text" size = '8' name="quarterly" id="quarterly" readonly> : 1ST QUARTER BILL  <br>
  <input type="hidden" size = '8' name="hquarterly" id="hquarterly"  >
  <hr>
  <input type="text"  size = '8' name="balance" id="balance" readonly> : BALANCE AMOUNT <br>

         
   </td>


  <td align= "center"><?php echo $SAC; ?></td>
  <td align= "center"><input type="text" size = '2' name="qty" id="qty" onkeyup="calc();"></td>
  <td align= "center"><input type="text" size = '5' name="rate" id="rate" onkeyup="calc();" value="<?php echo ($rate); ?>"></td>

  <td align= "center"><input type="text" size = '5' name="total" id="total" value="<?php echo number_format($total, 2); ?>" readonly ></td>
  
  </tr>
  <!--
  <tr>
   <td>
  SERVICE : <?php echo $servicename; ?> <br>
  CONTRACT PERIOD : <?php echo $contractstart; ?> TO <?php echo $contractend; ?><BR>
  PO NO. : <input type="text" size = '2' name="pono" id="pono"> DATED : <?php echo $contractdate; ?> <BR>
 
   </td><td></td><td></td><td></td><td></td>
  </tr>
  <tr>
  <td>
   <input type="text" size = '8' name="contractamt" id="contractamt" value="<?php echo number_format($contractamt,2); ?>" readonly > : TOTAL CONTRACT AMOUNT<br>
  <input type="hidden" id="hnet" value="<?php echo $contractamt; ?>">
  <input type="text" size = '8' name="quarterly" id="quarterly" readonly> : 1ST QUARTER BILL  <br>
  <input type="hidden" size = '8' name="hquarterly" id="hquarterly"  >
  <hr>
  <input type="text"  size = '8' name="balance" id="balance" readonly> : BALANCE AMOUNT <br>
  </td>
  <td>  </td>
  <td> </td>
  <td> </td>
  <td> </td>

  </tr> -->
  </table>

</div>
 <div class="col-sm-12">
   <table>
   <td width="500">
   <br>
   <b>Rupees :<span id="inword"></span> Only</b><br>
   <b>Payment should be made within 30 days from the date of invoice.<br>
   Interest @18% will be charged on all overdue invoices.<br>
   GSTIN : <?php echo $gstno; ?><br>
   Category of service : Cleaning services<br>
   </b><br>
   E.P.F. A/c No. MH/40228<br> 
   E.S.I.C. Code No. : 31-48076-101<br> 
   PAN No. AACCP 2255R<br> 
   Fumigation - Accreditation No.<br><br>
   <b>
   Bank Name : <?php echo get_object_vars($branchjson)['bankname']; ?> <br>
   Branch Name : <?php echo get_object_vars($branchjson)['bankbranchname']; ?><br>
   Account Holder's Name : M/s. Pest Mortem(India) Pvt. Ltd.<br>
   AC Number : C.A. No.  <?php echo get_object_vars($branchjson)['accountno']; ?><br>
   RTGS/ NEFT IFSC CODE :  <?php echo get_object_vars($branchjson)['ifsccode']; ?><br>
   </b>

   </td>
   <td width="300">
   <b>
   <?php
    $gstamt=($total)*(18)/100;
        ?>
   <table> 
    <tr><td width="200"> Total  </td> <td width="50"><input type="text" id="totalamt1" size= "12" value="" readonly></td></tr>
   <tr><td>()FREIGHT</td><td><input type="text" id="freight" value="0" size= "12" readonly></td></tr>
   
   <tr><td>CGST@ <input type="text"  size="1" id="cgst"  name="cgst" value="0" onkeyup="calc();">%</td><td><input type="text" id="cgstamt" value="0.00" size= "12" readonly></td></tr>
   
   <tr><td>SGST@ <input type="text"  size="1" id="sgst" name="sgst"  value="0" onkeyup="calc();">%</td> <td><input type="text" id="sgstamt" value="0.00" size= "12" readonly></td></tr>

   
  <tr><td>IGST@  &nbsp;<input type="text" size="1" id="igst" name="igst" value="18" onkeyup="calc();">%</td> <td><input type="text" id="igstamt" value="0.00" size= "12" readonly></td></tr>
   

   <tr><td>GRAND TOTAL </td><td><input type="text" id="grandtotal" size= "12" value="0.00" readonly></td></tr>
    </table
</b>
   <br>
   For Pest Mortem(India) Pvt. Ltd.
   <BR><BR><BR><BR>
   Authorised Signatory
   </td>
   </table>

 </div> 
<button class="btn btn-lg btn-primary" onclick="savebill(this)" style="border-radius:0px;position:fixed;bottom:10px;right:10px;">Save Bill</button>
<?php
include('../footer.php');
?>
<script>
$('body').css('background','#eee');
function funtb(e){
    var arr=[];
    $('.tds').each(function (){
        var serv=$(this).find('.serv').text().trim();
        var yesno=$(this).find('.yesno').val();
        var frq=$(this).find('.frq').val();
        if(yesno!='NO' && frq!='Nil'){
            arr.push({
                "serv":serv,
                "yesno":yesno,
                "frq":frq
            });
            var str='<table class="table-list table">';
            str+='<thead>';
            str+='<tr>';
            str+='<th>Service</th>'; 
            str+='<th>Frequency (In No.)</th>';
            str+='<th>Start Date</th>';
            str+='</tr>';
            str+='</thead>';  
            str+='<tbody>'; 
            str+='<tr>';
            str+='<td>';
            for(var i in arr){
            str+=arr[i].serv+'<br>';
            }
            str+='</td>';
            str+='<td><input type="number" class="form-control input-sm" onkeyup="coter(this)"></td>';
            str+='<td id="coter">';

            str+='</td>';
            str+='</tr>';
            str+='</tbody>'; 
            str+='</table>'; 
            $('#table-ui').html(str);
        }
    })
}
function coter(e){
var cot=$(e).val().trim();
if(cot!=''){
cot=parseInt(cot);
var str='<table class="table">';
str+='<thead>';
str+='<tr>';
str+='<th>Sr. No.</th>';
str+='<th>Service Date</th>';
str+='</tr>';
str+='</thead>';
str+='<tbody>';
for(var i=1;i<=cot;i++){
    str+='<tr>';
    str+='<td>'+i+'</td>';
    str+='<td><input type="date" class="form-control input-sm"></td>';
    str+='</tr>';
}
str+='</tbody>';
str+='</table>';
console.log(str);

$('#coter').html(str);
}
}
 function calc(){
    var totalamt=0;
     var rate=$('#rate').val();
 
    var qty=$('#qty').val();
    if(rate!='' && qty!=''){
        totalamt = rate * qty;

    $('#total').val(totalamt.toFixed(2) );
    $('#quarterly').val(totalamt.toFixed(2) );
    $('#totalamt1').val(totalamt.toFixed(2) );
    $('#hquarterly').val(totalamt);
    
    calc1();
      }
}
function calc1(){
    var totalamt=0;
     var netamt=$('#hnet').val();
     var igst=$('#igst').val();
     var cgst=$('#cgst').val();
     var sgst=$('#sgst').val();
     var freight=$('#freight').val();
     //alert(sgst);
     var grandtotal = 0; 
    // alert(quarterly);
   var billamt = parseInt($('#hquarterly').val());
    if(netamt!='' && billamt!=''){
        totalamt = netamt - billamt;
        cgst = (billamt*cgst)/100;
        sgst = (billamt*sgst)/100;
        igst = (billamt*igst)/100;
        grandtotal = billamt + igst + cgst + sgst;
       // alert(igst);
    $('#balance').val(totalamt.toFixed(2) );
  // $('#totalamt1').val(billamt.toFixed(2));
   $('#cgstamt').val(cgst.toFixed(2));
   $('#sgstamt').val(sgst.toFixed(2));
   $('#igstamt').val(igst.toFixed(2));
   
   $('#grandtotal').val(grandtotal.toFixed(2));
    $('#inword').text(convertNumberToWords(grandtotal.toFixed(2) ));
   
   // $('#inword').text(convertNumberToWords(totalamt.toFixed(2) ));
    }
}
function savebill(e) {
    
        var qty = $('#qty').val();
        var rate = $('#rate').val();
        var pono = $('#pono').val();
        var cgst = $('#cgst').val();
        var sgst = $('#sgst').val();
        var igst = $('#igst').val();
        
        var enquiryid = $('#henqno').val();
         $.ajax({
            type: "POST",
            data: 'enquiryid=' + enquiryid + '&qty=' + qty +'&rate=' + rate +'&pono=' + pono +'&cgst=' + cgst +'&sgst=' + sgst +'&igst=' + igst   ,
            url: '../api/savebill.php',
            cache: false,
            success: function (res) {
               if (res.status == 'success') {
                    window.location = "../payment.php";
                }
            }
        })
    
}
function convertNumberToWords(amount) {
    var words = new Array();
    words[0] = '';
    words[1] = 'One';
    words[2] = 'Two';
    words[3] = 'Three';
    words[4] = 'Four';
    words[5] = 'Five';
    words[6] = 'Six';
    words[7] = 'Seven';
    words[8] = 'Eight';
    words[9] = 'Nine';
    words[10] = 'Ten';
    words[11] = 'Eleven';
    words[12] = 'Twelve';
    words[13] = 'Thirteen';
    words[14] = 'Fourteen';
    words[15] = 'Fifteen';
    words[16] = 'Sixteen';
    words[17] = 'Seventeen';
    words[18] = 'Eighteen';
    words[19] = 'Nineteen';
    words[20] = 'Twenty';
    words[30] = 'Thirty';
    words[40] = 'Forty';
    words[50] = 'Fifty';
    words[60] = 'Sixty';
    words[70] = 'Seventy';
    words[80] = 'Eighty';
    words[90] = 'Ninety';
    amount = amount.toString();
    var atemp = amount.split(".");
    var number = atemp[0].split(",").join("");
    var n_length = number.length;
    var words_string = "";
    if (n_length <= 9) {
        var n_array = new Array(0, 0, 0, 0, 0, 0, 0, 0, 0);
        var received_n_array = new Array();
        for (var i = 0; i < n_length; i++) {
            received_n_array[i] = number.substr(i, 1);
        }
        for (var i = 9 - n_length, j = 0; i < 9; i++, j++) {
            n_array[i] = received_n_array[j];
        }
        for (var i = 0, j = 1; i < 9; i++, j++) {
            if (i == 0 || i == 2 || i == 4 || i == 7) {
                if (n_array[i] == 1) {
                    n_array[j] = 10 + parseInt(n_array[j]);
                    n_array[i] = 0;
                }
            }
        }
        value = "";
        for (var i = 0; i < 9; i++) {
            if (i == 0 || i == 2 || i == 4 || i == 7) {
                value = n_array[i] * 10;
            } else {
                value = n_array[i];
            }
            if (value != 0) {
                words_string += words[value] + " ";
            }
            if ((i == 1 && value != 0) || (i == 0 && value != 0 && n_array[i + 1] == 0)) {
                words_string += "Crores ";
            }
            if ((i == 3 && value != 0) || (i == 2 && value != 0 && n_array[i + 1] == 0)) {
                words_string += "Lakhs ";
            }
            if ((i == 5 && value != 0) || (i == 4 && value != 0 && n_array[i + 1] == 0)) {
                words_string += "Thousand ";
            }
            if (i == 6 && value != 0 && (n_array[i + 1] != 0 && n_array[i + 2] != 0)) {
                words_string += "Hundred and ";
            } else if (i == 6 && value != 0) {
                words_string += "Hundred ";
            }
        }
        words_string = words_string.split("  ").join(" ");
    }
    return words_string;
}
</script>