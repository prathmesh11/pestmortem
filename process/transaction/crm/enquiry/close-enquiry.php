<?php 
$base='../../../../';

$navenq1='background:#1B1464;';
include('header.php');
$enquiryidnew=mysqli_fetch_assoc(mysqli_query($con,"SELECT id x FROM crmmaster ORDER BY id DESC"))['x'];

  $year1=0;
  $year2=0;

if ( date('m') > 3 ) {
    $year1 = date('y');
    $year2 = date('y')+1;
}else{
    $year1 = date('y')-1;
    $year2 = date('y');
}

if (session_status() == PHP_SESSION_NONE) { session_start(); }
$sessionby = $_SESSION['employeeid'];
$branchid = $_SESSION['branchid'];

$branchshortname=mysqli_fetch_assoc(mysqli_query($con,"SELECT branchshortname x FROM branchmaster WHERE branchid='$branchid'"))['x'];

  if($enquiryidnew){
    $enquiryidnew++;
    $enquiryidnew=str_pad($enquiryidnew,6, '0', STR_PAD_LEFT);
    $enquiryidnew='PMI/'.$branchshortname.'/ENQ/'.$enquiryidnew.'/'.$year1.'-'.$year2;
  }else{
    $enquiryidnew='PMI/'.$year1.'-'.$year2.'/B'.$branchid.'/E'.$sessionby.'/1';
    $enquiryidnew='PMI/'.$branchshortname.'/ENQ/000001/'.$year1.'-'.$year2;
  }

?>
<br>
<style>
.input-container {
 
  max-width: 300px;
  background-color: #EDEDED;
  border: 1px solid #DFDFDF;
  border-radius: 5px;
}

input[type='file'] {
  display: none;
}

.file-info {
  font-size: 0.9em;
}

.browse-btn {
  background: #03A595;
  color: #fff;
  min-height: 35px;
  padding: 10px;
  border: none;
  border-top-left-radius: 5px;
  border-bottom-left-radius: 5px;
}

.browse-btn:hover {
  background: #4ec0b4;
}

@media (max-width: 300px) {
  button {
    width: 100%;
    border-top-right-radius: 5px;
    border-bottom-left-radius: 0;
  }
  
  .file-info {
    display: block;
    margin: 10px 5px;
  }
}
</style>
<div class="table-ui container-fluid">
<div class="tr row">
<div class="col-sm-1 th">Enquiry No.</div>    
<div class="col-sm-3 th">Customer Info</div>
<div class="col-sm-2 th">Enquery For</div>
<div class="col-sm-2 th">Close Date</div>
<div class="col-sm-2 th">Close By</div>
<div class="col-sm-2 th">Action</div>
</div>

<?php
$result=mysqli_query($con,"SELECT enquiryid,customerid,category,close_enquiry_reason,close_enquiry_time ,close_enquiry_by FROM crmmaster WHERE close_enquiry_time<>0 AND close_enquiry_by<>0 ORDER BY created_time DESC");

while($rows = mysqli_fetch_assoc($result)){

    $customerid=$rows['customerid'];

    $cifo=mysqli_fetch_assoc(mysqli_query($con,"SELECT * FROM customermaster WHERE customerid='$customerid'"));

?>

<div class="row tr">
<div class="col-sm-1 td" style="word-wrap:break-word;"><?php echo $rows['enquiryid']; ?></div>
<div class="col-sm-3 td">
<?php 

echo 'Company : '.$cifo['customername']; 
echo '<br>Address : '.get_object_vars(json_decode($cifo['customerjson']))['address']; 
echo '<br>Name  : '.get_object_vars(json_decode($cifo['contactjson'])[0])['name'];
echo '<br>Dept. : '.get_object_vars(json_decode($cifo['contactjson'])[0])['department'];
echo '<br>Mobile : <a href="tel:'.get_object_vars(json_decode($cifo['contactjson'])[0])['mobile'].'">'.get_object_vars(json_decode($cifo['contactjson'])[0])['mobile'].'</a>';
echo '<br>Landline'.get_object_vars(json_decode($cifo['contactjson'])[0])['landline'];
echo '<br>Email : <a href="mailto:'.get_object_vars(json_decode($cifo['contactjson'])[0])['email'].'">'.get_object_vars(json_decode($cifo['contactjson'])[0])['email'].'</a>';
?>
</div>
<div class="col-sm-2 td"><?php echo $rows['category']; ?></div>
<div class="col-sm-2 td">

<?php
echo date("d-m-Y",$rows['close_enquiry_time']/1000);
?>

</div>
<div class="col-sm-2 td">
<?php
$close_enquiry_by=$rows['close_enquiry_by'];
$username=mysqli_fetch_assoc(mysqli_query($con,"SELECT username x FROM employeemaster WHERE employeeid='$close_enquiry_by'"))['x'];
echo $username;
 ?>

</div>

<!-- <div class="col-sm-3 td">

<?php //echo $rows['close_enquiry_reason']; ?>

</div> -->

<div class="col-sm-2 td">
<button class="btn btn-sm btn-block btn-success" data-enquiryid="<?php echo $rows['enquiryid']; ?>" onclick="recovers(this)">Recover Order</button>

</div>

</div>
<?php
}
?>
</div>
</div>
</div>

<button class="btn" style="font-size:25px;border-radius:50px;width:60px;height:60px;position: fixed;
    bottom: 40px;
    right: 40px;background:#eb2f06;color:#fff;" data-toggle="modal" data-target="#myModal">+</button>

<?php 
include('footer.php');
?>
<script>
function recovers(e) {
   
    if (confirm("Are You Sure TO recover Order?")) {
        var enquiryid = $(e).data('enquiryid');
        $.ajax({
            type: "POST",
            data: 'enquiryid=' + enquiryid,
            url: 'api/recover.php',
            cache: false,
            success: function (res) {
                if (res.status == 'success') {
                    window.location.reload();
                }
            }
        })
    }
}
</script>
