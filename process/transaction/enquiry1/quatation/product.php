<?php 
$navenq2='background:#1B1464;';
$base='../../../../';
  include('../header.php');
?>

<style type="text/css">
.flex-container{
  
    width: 100%;
 

}

body{
    background:#eee;
}
.column{
    
    padding:40px;
    width: 65%;
    background-color: #fff;
    -moz-box-shadow:    inset 0 0 5px #000000;
   -webkit-box-shadow: inset 0 0 5px #000000;
   box-shadow:         inset 0 0 5px #000000;
}
.table-list{
    table-layout: fixed; width: 100%;
}

.table-list td,
.table-list th {
	border: 1px solid #ddd;
	padding: 1px !important;
	font-size: 13px;
}

.table-list td {
	padding-top: 10px !important;
    word-wrap: break-word;
}

.table-list tr:nth-child(even) {
	background-color: #f2f2f2;
}

.table-list th {
	padding-top: 5px;
	padding-bottom: 5px;
	text-align: center;
	background-color: #16a085;
	color: white;
}
@media print {
  body * {
    visibility: hidden;
  }
  #section-to-print, #section-to-print * {
    visibility: visible;
  }
  #section-to-print {
    position: absolute;
    left: 0;
    top: 0;
  }
}
.iframe-container {
  overflow: hidden;
  padding-top: 56.25%;
  position: relative;
} 

.iframe-container iframe {
  position: absolute;
  top: 0;
  left: 0;
  border: 0;
  width: 100%;
  height: 600px;
}
</style>
<?php
$bomid=$_GET['bom'];

$enquiryid=$_GET['enq'];
$bom=mysqli_fetch_assoc(mysqli_query($con,"SELECT drwingno,RevNo,name,ccode,pallet,bom,nesting,hardware FROM sheetmetal WHERE id='$bomid'"));
$ccode=$bom['ccode'];
$cname=mysqli_fetch_assoc(mysqli_query($con,"SELECT cname x FROM mcustmast WHERE ccode='$ccode'"))['x'];

$json1='';
$hardware=json_decode($bom['hardware']);
$count=1;
foreach($hardware as $i){
  $itemname=get_object_vars($i)['ItemName'];
  $quantity=get_object_vars($i)['Quantity'];
  $itemcode1=mysqli_fetch_assoc(mysqli_query($con,"SELECT itemcode x FROM mitemmaster WHERE itemname='$itemname' "))['x'];

  $msirate=(mysqli_query($con,"SELECT itemcode,rate FROM mcirate WHERE ccode='$ccode' "));
   $rate=0;  
  while($rows = mysqli_fetch_assoc($msirate)){
    $itemcode=$rows["itemcode"];
   
    if($itemcode1==$itemcode){
      $rate=$rows["rate"];
    }
   }
   $amount=$rate*$quantity;
   $json1.=',{"srno":"'.$count.'","itemname":"'.$itemname.'","quantity":"'.$quantity.'","rate":"'.$rate.'","amount":"'.$amount.'"}';
   $count++;
}

$json1=substr($json1,1);
$json1='['.$json1.']';

?>

<input type="hidden" id="cname" value='<?php echo $cname; ?>'/>
<input type="hidden" id="drwingno" value='<?php echo $bom['drwingno'] ; ?>'/>
<input type="hidden" id="name" value='<?php echo $bom['name'] ; ?>'/>
<input type="hidden" id="item-json1" value='<?php echo $bom['bom']; ?>'/>
<input type="hidden" id="item-json2" value='<?php echo $json1; ?>'/>
<input type="hidden" id="item-json3" value='<?php echo $bom['pallet']; ?>'/>
<script>

var json1 = JSON.parse($("#item-json1").val());
var json2 = JSON.parse($("#item-json2").val());
var json3 = JSON.parse($("#item-json3").val());

console.log(json3);

var hardware=[
  ['HARDWARE BROGHTOUT ITEMS','','','',''],
  ['SR.NO','DESCRIPTION','QTY/SET','COST/PCS','TOTAL COST']
];

var pallet=[
  ['Pallet Details','','','','','',''],
  ['SR.NO','DESCRIPTION','Length - mm','Width - mm','Thk - mm','Qty','Cubic Ft']
];

var cubicfeet=0;
var countsr1=1;
for(var i in json3){
  var arr1=[countsr1,json3[i].desc,json3[i].length,json3[i].width,json3[i].think,json3[i].quanitiy,json3[i].cubicfeet];
  pallet.push(arr1);
  cubicfeet+=parseFloat(json3[i].cubicfeet);
  countsr1++;
}

pallet1=[];

pallet1.push(['Pallet Cost Caculation','','']);
 pallet1.push(['Total Cubic Ft','',cubicfeet]);
 pallet1.push(['Rate per Cubic Ft - Rs','','0']);
 pallet1.push(['Total','','=AE15*AE16']);
pallet1.push(['Cost For Runner Cutout','','0']);
pallet1.push(['Cost For Fumigation','','0']);
pallet1.push(['Total Pallet Cost','','=AE17+AE18+AE19']);

console.log(pallet1);


console.log(pallet);

var amounttotal=0;
for(var i in json2){
  var arr2=[json2[i].srno,json2[i].itemname,json2[i].quantity,json2[i].rate,json2[i].amount];
  hardware.push(arr2);
  amounttotal+=parseFloat(json2[i].amount);
}
hardware.push(['','','','TOTAL',amounttotal]);
var arr=[];
var totalnet=0;
var totalarea=0;

if(json1!=''){
  for(var i in json1){
  var srno='';
  if(json1[i].subno==''){
    srno=json1[i].srno;
  }
  var blweight=parseFloat(json1[i].NetWeight)/parseFloat(json1[i].QtySets);
var arr1=[srno,json1[i].subno,json1[i].DrawingNo,json1[i].RevNo,json1[i].PartDescription,json1[i].MaterialType,json1[i].QtySets,json1[i].Length,json1[i].Width,json1[i].Thick,blweight,json1[i].NetWeight,json1[i].AreaOfPC];
arr.push(arr1);
totalnet+=parseFloat(json1[i].NetWeight);
totalarea+=parseFloat(json1[i].AreaOfPC);
}
}

var bomtable=[];

var aa=['1','SHEET BLANK WT.','IN KG/P.CAB.',totalnet];
var bb=['','ADD : WASTAGE','IN KG/P.CAB.','=5*'+totalnet+'/100'];
var cc=['','TOTAL WT.','IN KG/P.CAB.','=AJ5+AJ6'];
var dd=['','RATE','P.KG','0'];
var ee=['','CHARGES.(Punching,Bending & Fab.)','P.KG','0'];
var ff=['','TOTAL (Rate+P/B/F)','','=AJ8+AJ9'];
var gg=['','FAB COST','','=AJ7+AJ10'];
var hh=['','','',''];
var ii=['','TOTAL FAB. COST','','=AJ11'];
var jj=['','','',''];
var kk=['2','CHARGES.(Powder Coating)','','0'];
var ll=['','TOTAL AREA','',totalarea];
var mm=['','TOTAL P/C COST','','=AJ15*AJ16'];
var nn=['','','',''];
var oo=['A','SUB- COST(1+2)','RS/P.CAB','=AJ13+AJ17'];
var pp=['','','',''];
var qq=['B','HARDWARE COST','',amounttotal];
var rr=['C','PALLET COST','','=AE20'];
var ss=['D','Add: 7% H.C. ON H/W','','=(AJ22+AJ24)*0.07'];
var tt=['E','ASSBLY CHGS.','',''];
var uu=['F','PKG. CHGS.','','=1.2*AJ5'];
var vv=['G','FORWARDING COST','','=AE12'];
var ww=['','','',''];
var xx=['','TOTAL','A+B+C+D+E+F+G','=AJ19+AJ20+AJ21+AJ22+AJ23+AJ24+AJ25+AJ26'];
var yy=['','','DEVELOPMENT COST',''];
var zz=['','','TOTAL COST OF CABINET','=AJ28+AJ29'];

bomtable.push(aa,bb,cc,dd,ee,ff,gg,hh,ii,jj,kk,ll,mm,nn,oo,pp,qq,rr,ss,tt,uu,vv,ww,xx,yy,zz);

var transport=[];
var aaa=['Transportation Cost Calculation','',''];
var bbb=['','length of Pallet','Width of pallet'];
var ccc=['','',''];
var ddd=['','',''];
var eee=['','',''];
var fff=['','',''];
var ggg=['Total Qty per truck','',''];
var hhh=['Transportation','',''];

transport.push(aaa,bbb,ccc,ddd,eee,fff,ggg,hhh);


var totalamount=['','','','','','','','','','TOTAL','',totalnet,totalarea];
arr.push(totalamount);

  var cname=$('#cname').val();
  var drwingno=$('#drwingno').val();
  var name=$('#name').val();
  
    function makeApiCall1() {
      var params = {
        spreadsheetId: '15Vgig43CBJbm8bSkX5D4XTt22KgRhMiSVEmvhWndFw0',
      };
      var batchUpdateValuesRequestBody = {
        valueInputOption: 'USER_ENTERED',
        data: [{
          range: 'Sheet2!E2:E5',
          majorDimension: 'ROWS',
          values: [
            [cname.toUpperCase()],
            [drwingno],
            [''],
            [name]
          ]
        }],
      };
      var request = gapi.client.sheets.spreadsheets.values.batchUpdate(params, batchUpdateValuesRequestBody);
      request.then(function (response) {
        makeApiCall2();
        console.log(response.result);
      }, function (reason) {
        console.error('error: ' + reason.result.error.message);
      });
    }

    function makeApiCall2() {
      var params = {
        spreadsheetId: '15Vgig43CBJbm8bSkX5D4XTt22KgRhMiSVEmvhWndFw0',
      };

      var batchClearValuesRequestBody = {
        ranges: ['Sheet2!A7:AJ200'],
      };

      var request = gapi.client.sheets.spreadsheets.values.batchClear(params, batchClearValuesRequestBody);
      request.then(function (response) {
        makeApiCall3()
        console.log(response.result);
      }, function (reason) {
        console.error('error: ' + reason.result.error.message);
      });
    }

    function makeApiCall3() {
      var params = {
        spreadsheetId: '15Vgig43CBJbm8bSkX5D4XTt22KgRhMiSVEmvhWndFw0',
      };
      var batchUpdateValuesRequestBody = {
        valueInputOption: 'USER_ENTERED',
        data: [{
          range: 'Sheet2!A7:M' + (arr.length + 8),
          majorDimension: 'ROWS',
          values: arr
        }],
      };
      var request = gapi.client.sheets.spreadsheets.values.batchUpdate(params, batchUpdateValuesRequestBody);
      request.then(function (response) {
        makeApiCall4();
        console.log(response.result);
      }, function (reason) {
        console.error('error: ' + reason.result.error.message);
      });
    }

    function makeApiCall4() {
      var params = {
        spreadsheetId: '15Vgig43CBJbm8bSkX5D4XTt22KgRhMiSVEmvhWndFw0',
      };
      var batchUpdateValuesRequestBody = {
        valueInputOption: 'USER_ENTERED',
        data: [{
          range: 'Sheet2!O5:T' + (hardware.length+8),
          majorDimension: 'ROWS',
          values: hardware
        }],
      };
      var request = gapi.client.sheets.spreadsheets.values.batchUpdate(params, batchUpdateValuesRequestBody);
      request.then(function (response) {
        makeApiCall5();
        console.log(response.result);
      }, function (reason) {
        console.error('error: ' + reason.result.error.message);
      });
    }

    function makeApiCall5() {
      var params = {
        spreadsheetId: '15Vgig43CBJbm8bSkX5D4XTt22KgRhMiSVEmvhWndFw0',
      };
      var batchUpdateValuesRequestBody = {
        valueInputOption: 'USER_ENTERED',
        data: [{
          range: 'Sheet2!U5:AA' + (pallet.length+8),
          majorDimension: 'ROWS',
          values: pallet
        }],
      };
      var request = gapi.client.sheets.spreadsheets.values.batchUpdate(params, batchUpdateValuesRequestBody);
      request.then(function (response) {
        makeApiCall6();
        console.log(response.result);
      }, function (reason) {
        console.error('error: ' + reason.result.error.message);
      });
    }

    function makeApiCall6() {
      var params = {
        spreadsheetId: '15Vgig43CBJbm8bSkX5D4XTt22KgRhMiSVEmvhWndFw0',
      };
      var batchUpdateValuesRequestBody = {
        valueInputOption: 'USER_ENTERED',
        data: [{
          range: 'Sheet2!AC5:AE'+(transport.length+4),
          majorDimension: 'ROWS',
          values: transport
        }],
      };

      var request = gapi.client.sheets.spreadsheets.values.batchUpdate(params, batchUpdateValuesRequestBody);
      request.then(function (response) {
        makeApiCall7();
        console.log(response.result);
      }, function (reason) {
        console.error('error: ' + reason.result.error.message);
      });
    }

    function makeApiCall7() {
      var params = {
        spreadsheetId: '15Vgig43CBJbm8bSkX5D4XTt22KgRhMiSVEmvhWndFw0',
      };
      var batchUpdateValuesRequestBody = {
        valueInputOption: 'USER_ENTERED',
        data: [{
          range: 'Sheet2!AC14:AE'+(pallet1.length+15),
          majorDimension: 'ROWS',
          values: pallet1
        }],
      };
      var request = gapi.client.sheets.spreadsheets.values.batchUpdate(params, batchUpdateValuesRequestBody);
      request.then(function (response) {
        makeApiCall8();
        console.log(response.result);
      }, function (reason) {
        console.error('error: ' + reason.result.error.message);
      });
    }

    function makeApiCall8() {
      var params = {
        spreadsheetId: '15Vgig43CBJbm8bSkX5D4XTt22KgRhMiSVEmvhWndFw0',
      };
      var batchUpdateValuesRequestBody = {
        valueInputOption: 'USER_ENTERED',
        data: [{
          range: 'Sheet2!AG5:AL32',
          majorDimension: 'ROWS',
          values: bomtable
        }],
      };
      var request = gapi.client.sheets.spreadsheets.values.batchUpdate(params, batchUpdateValuesRequestBody);
      request.then(function (response) {
        console.log(response.result);
      }, function (reason) {
        console.error('error: ' + reason.result.error.message);
      });
    }





  

    function initClient() {
      var API_KEY = 'AIzaSyBVe7HthlidHKDU20N99EGJRV2GbsLXj9A';
      var CLIENT_ID = '14890618428-t27mdv4qgmb9gc4dseql91ep8si7q1k5.apps.googleusercontent.com';
      var SCOPE = 'https://www.googleapis.com/auth/spreadsheets';

      gapi.client.init({
        'apiKey': API_KEY,
        'clientId': CLIENT_ID,
        'scope': SCOPE,
        'discoveryDocs': ['https://sheets.googleapis.com/$discovery/rest?version=v4'],
      }).then(function() {
        gapi.auth2.getAuthInstance().isSignedIn.listen(updateSignInStatus);
        updateSignInStatus(gapi.auth2.getAuthInstance().isSignedIn.get());
      });
    }

    function handleClientLoad() {
      gapi.load('client:auth2', initClient);
    }

    function updateSignInStatus(isSignedIn) {
      if (isSignedIn) {
        makeApiCall1();      
        $('#signin-button').hide();
        $('#iframsheet').show();
      }
    }

    function handleSignInClick(event) {
      gapi.auth2.getAuthInstance().signIn();
    }

    function handleSignOutClick(event) {
      gapi.auth2.getAuthInstance().signOut();
    }
    </script>
    <script async defer src="https://apis.google.com/js/api.js"
      onload="this.onload=function(){};handleClientLoad()"
      onreadystatechange="if (this.readyState === 'complete') this.onload()">
    </script>
    <center>
    <button id="signin-button" class="btn btn-primary btn-lg" onclick="handleSignInClick()">Sign in</button>
    </center>
    <div class="iframe-container">
<iframe id="iframsheet" style="display:none;" src="https://docs.google.com/spreadsheets/d/15Vgig43CBJbm8bSkX5D4XTt22KgRhMiSVEmvhWndFw0/edit#gid=413457796" >
</iframe>
</div>
<?php
    include($base.'_in/footer.php');
?>