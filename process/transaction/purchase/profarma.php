<?php 
$base='../../../';
$navenq5='background:#1B1464;';
  include('header.php');
?>
<br>

<div class="table-ui container-fluid">
<div class="tr row">
<div class="col-sm-1 th">Enquiry No.</div>    
<div class="col-sm-3 th">Customer Info</div>
<div class="col-sm-6 th">Customer Order Info</div>
<div class="col-sm-2 th">Action</div>
</div>
<?php

$result=mysqli_query($con,"SELECT enquiryid,customerid,enquirystatus,quotation,followupnote,category,created_by,created_time FROM enqtable WHERE category='Product' AND enquirystatus<>0 AND quotation<>0 AND customerorder<>0 AND customerorderapproval<>0 AND profarma=0 AND payment=0 AND production=0 AND dispatch=0 AND cancel=0 ORDER BY created_time DESC");
while($rows = mysqli_fetch_assoc($result)){
    $ccode=$rows['customerid'];
    $cifo=mysqli_fetch_assoc(mysqli_query($con,"SELECT cname,ccontact,caddress,cgstno FROM mcustmast WHERE ccode='$ccode' ORDER BY created_time DESC"));
    $enquiryidbom=$rows['enquiryid'];
?>

<div class="row tr">
<div class="col-sm-1 td"><?php echo $rows['enquiryid']; ?></div>
<div class="col-sm-3 td">
<?php 

echo 'Company : '.$cifo['cname']; 
echo '<br>Address : '.$cifo['caddress']; 
echo '<br>Name  : '.get_object_vars(json_decode($cifo['ccontact'])[0])['table'][0];
echo '<br>Dept. : '.get_object_vars(json_decode($cifo['ccontact'])[0])['table'][1];
echo '<br>Mobile : <a href="tel:'.get_object_vars(json_decode($cifo['ccontact'])[0])['table'][3].'">'.get_object_vars(json_decode($cifo['ccontact'])[0])['table'][3].'</a>';
echo '<br>Landline'.get_object_vars(json_decode($cifo['ccontact'])[0])['table'][4];
echo '<br>Email : <a href="mailto:'.get_object_vars(json_decode($cifo['ccontact'])[0])['table'][5].'">'.get_object_vars(json_decode($cifo['ccontact'])[0])['table'][5].'</a>';
?>
</div>



<div class="col-sm-6 td">
<?php 


$custorder=mysqli_fetch_assoc(mysqli_query($con,"SELECT ppno,ppdate,saletype,tablejson,created_by,created_time FROM partypurchase WHERE enquiryid='$enquiryidbom'"));

echo 'Customer Order No : '.$custorder['ppno'].' || ';
echo 'Customer Order Date : '.date('d/m/Y',  strtotime($custorder['ppdate'])).' <br> ';
$created_by=$custorder['created_by'];
$created_by=mysqli_fetch_assoc(mysqli_query($con,"SELECT username x FROM admin WHERE aid='$created_by'"))['x'];
echo 'Created By : '.$created_by.' || ';
echo 'Created Date : '.date("d-m-Y",$custorder['created_time']/1000) .'<br>';
?>
<table class="table-ui">
    <thead>
      <div class="row tr">
        <div class="col-sm-6 th" >Product</div>
        <div class="col-sm-1 th" >Qty</div>
        <div class="col-sm-1 th" >Rate</div>
        <div class="col-sm-1 th" >Amt</div>
        <div class="col-sm-1 th" >GST</div>
        <div class="col-sm-2 th" >Total</div>
      </div>
    </thead>
    <tbody>
    <?php
    $tablejson=$custorder['tablejson'];
    $tablejson=json_decode($tablejson);
  
    foreach($tablejson as $i){
      
        echo '<div class="row tr">';
        echo '<div class="col-sm-6 td">'.get_object_vars($i)['itemname'].'</div>';
        echo '<div class="col-sm-1 td">'.get_object_vars($i)['qty'].'</div>';
        echo '<div class="col-sm-1 td">'.get_object_vars($i)['rate'].'</div>';
        echo '<div class="col-sm-1 td">'.get_object_vars($i)['amount'].'</div>';
        echo '<div class="col-sm-1 td">'.(get_object_vars($i)['cgstamt']+get_object_vars($i)['sgstamt']+get_object_vars($i)['igstamt']).'</div>';
        echo '<div class="col-sm-2 td">'.get_object_vars($i)['netamt'].'</div>';
        echo '</div>';
    }
    ?>
    </body>
    </table>
<?php


?>
</div>
<div class="col-sm-2 td">


<a class="btn btn-primary btn-sm btn-block" href="<?php echo '/process/transaction/profarma/index.php?enq='.$rows['enquiryid']; ?>" >Profarma </a>
<button class="btn btn-sm btn-block btn-danger" data-enquiryid="<?php echo $rows['enquiryid']; ?>" onclick="cancel(this)">Cancel</button>
</div>

</div>
<?php
}
?>
</div>
</div>
</div>

<?php
    include($base.'_in/footer.php');
?>
<script>
  function closemodal(e){
    $('#'+e).modal('toggle');
  }

  
  function customerapproval(e){
  var enquiryid=$(e).data('enquiryid');
  $.ajax({
        type: "POST",
        data: 'enquiryid='+enquiryid,
        url: 'api/customerapproval.php',
        cache: false,
        success: function (res) {
          if (res.status == 'success') {
             window.location.reload();
          }
        }
    })
}

  </script>

<script>
function cancel(e){
     var enquiryid=$(e).data('enquiryid');
  $.ajax({
        type: "POST",
        data: 'enquiryid='+enquiryid,
        url: 'api/cancel.php',
        cache: false,
        success: function (res) {
          if (res.status == 'success') {
             window.location.reload();
          }
        }
    })
}
</script>