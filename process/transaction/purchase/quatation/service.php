<?php 
$base='../../../../';
  include('../header.php');
  
?>
<h4 align="center">Service Quatation<h4>
<style>

.split {
  height: 100%;
  width: 50%;
  position: fixed;
  z-index: 1;
  top: 20%;
  overflow-x: hidden;
  border-top:1px solid #000;
  
}

.left {
  left: 0;
  background-color: #EEE;
  border-right:2px solid #000;
}

.right {
  right: 0;
  background-color: #FFF;
}

.table-bordered{
    border:1px solid #000;
    font-size:12px;
}

.table-bordered th{
background:#eee;

}

.table-bordered tr, td, th{
    border:1px solid #000;
}

</style>
</head>
<body>

<div class="split left">
</div>
<div class="split right" style="padding-top:30px;padding-left:30px;padding-right:30px;padding-bottom:400px;">
    <div class="row">
        <div class="col-sm-6" align="center">
            <img src="/img/icon/hiverlogo.jpg" style="width:80%;margin-top:40px;">
        </div>
        <div class="col-sm-6">
            <h3 style="color:#3742fa;margin-bottom:2px;"><b>HIVER Aircon Private Limited</b></h3>
            <p style="font-size:13px;font-family:Arial;">Address : Garnet Paladium, Unit No. 104, First Floor, Western
                Express Hwy, Near Oberoi Mall Junction, Pandit M. Nehru Road, Behind Express Zone Buss Park, Goregaon
                East (E), Mumbai - 400097, India <br>
                Tel : +91 22 28304470;28714470;9773159000<br>
                Email : shivajidarade@hiver.co.in, hiverindia@gmail.com,<br>
                Website : www.hiver.co.in</p>
        </div>

        <div class="col-sm-6" style="font-size:16px;">REF: HAPL/MUM/ECR/Q-2019/SD/0580</div>
        <div class="col-sm-6" align="right" style="font-size:16px;">20-03-2019</div>
        <div class="col-sm-12">
            <h4 align="center"><b>ANNUAL MAINTENANCE CONTRACT</b></h4>

            <p align="justify" style="font-size:15px;font-family:Tahoma">This Maintenance Agreement is made and entered
                into as of this 07th March of 2019 by and between <b> Emcure Pharmaceuticals Limited, (Sterile Product
                    Division), Plot No: P 1, I.T. - B.T. Park, Phase - 2, M.I.D.C. Hinjwadi, Pune - 411 057 & Hiver
                    Aircon Private Limited, </b> having an Office Address at <b> Garnet Paladium, Unit No. 104, First
                    Floor, Western Express Hwy, Near Oberoi Mall Juction, Behind Express Zone Buss Park, Pandit M.
                    Nehru Road, Goregaon East, Mumbai - 400 097.</b></p>

            <p style="font-size:16px;font-family:Tahoma"><b><u>Details of Services:</u></b></p>
            <p style="font-size:15px;font-family:Tahoma">Hiver Aircon will provide the services stated below on the
                following Equipment, in accordance with the terms set in the attached Maintenance Services Agreement
                and below:</p>
            <p style="font-size:15px;font-family:Tahoma"><b><u>Service Plan</u> – <u>Labour Annual Maintenance Contract
                        (LAMC)</u>:-</b></p>
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>Sr. No.</th>
                        <th>Quantity</th>
                        <th>Description</th>
                        <th>Make & Model #</th>
                        <th>Serial #</th>
                        <th>Location</th>
                        <th>Type of Service</th>
                        <th>Term Beginning</th>
                        <th>Term Duration</th>
                        <th>Service Fee [currency]</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>

            <p style="font-size:15px;font-family:Tahoma"><b>Payment Terms :</b> Half yearly in Advance against Performa
                Invoice.
                The Service Fee shall be Rupees Three Lakhs Sixty Seven Thousand and Five Hundred + GST as applicable
                for the term of One (1) year.</p>
            <p style="font-size:15px;font-family:Tahoma"><b>Proposal Validity :</b> This offer is valid for 90 days
                from the Date of Submission.
                The price of Rs. 3,67,500 + applicable GST will remain fixed till 31st March 2020. For any subsequent
                one year terms following 31st March 2020, the contract can be renewed as mutually agreed. </p>

            <div style="font-size:15px;font-family:Tahoma">
                <h4><b>Our Maintenance Plan shall consist of the following:-</b></h4>
                <p>1)<b>Annual Maintenance</b> - This service will be
                    performed at the start of the maintenance by service term. All manufacturers&rsquo;
                    recommended yearly maintenance procedures on the unit(s) will be conducted. The inspection
                    will be conducted on a mutually agreeable date (1 visit).</p>
                <p>2)<b>Preventative Maintenance</b> - This service provides
                    for regularly scheduled preventative maintenance inspections. There will be four Preventative
                    Maintenance visits per year, at approximately equal intervals.</p>
                <p>3)<b>Emergency Service</b> - Includes emergency calls
                    between inspections as required for the purpose of diagnosis of trouble, adjustment, minor repair,
                    or resetting of controls. Calls will be dealt with 24 x 7 basis.</p>
                <p>4)<b>Written Reports</b> - A report will be provided at
                    the end of each inspection with recommendations of necessary repairs and maintenance procedures.</p>
                    <br>  <br>
                <p>1)<b>Annual Maintenance</b> - Once a year a thorough
                    maintenance procedure will be performed including the following:</p>
                <ol>
                    <li>
                        <p>A) Motor and Starters</p>
                    </li>
                </ol>
                <ul>
                    <li>
                        <p>Clean the starter cabinet and starter components.</p>
                    </li>
                    <li>
                        <p>Inspect wiring and connections for tightness and signs of overheating and discoloration.</p>
                    </li>
                    <li>
                        <p>Check the condition of the contacts for wear and pitting.</p>
                    </li>
                    <li>
                        <p>Check contactors for free and smooth operation.</p>
                    </li>
                    <li>
                        <p>Check all mechanical linkages for wear, security and clearances.</p>
                    </li>
                    <li>
                        <p>Verify tightness of the motor terminal connections.</p>
                    </li>
                    <li>
                        <p>Meg the motor and record readings.</p>
                    </li>
                    <li>
                        <p>Verify the operation of the electrical interlocks.</p>
                    </li>
                    <li>
                        <p>Measure voltage and record. Voltage should be nominal voltage &plusmn;10%.</p>
                    </li>
                </ul>
                <ol>
                    <li>
                        <p>B) Check unit thoroughly for refrigerant leaks.</p>
                    </li>
                </ol>
                <ul>
                    <li>
                        <p>Inspect for leaks and report leak check result.</p>
                    </li>
                    <li>
                        <p>Repair minor leaks as required (e.g. valve packing, flare nuts).</p>
                    </li>
                    <li>
                        <p>Check the Condenser &amp; Evaporator scaling condition if required de-scaling then will
                            inform the same to you for de-scaling.</p>
                    </li>
                </ul>
                <ol>
                    <li>
                        <p>C) Lubrication System</p>
                    </li>
                </ol>
                <ul>
                    <li>
                        <p>Verify the operation of the oil heaters</p>
                    </li>
                    <li>
                        <p>Verify the oil pressure drop across the both Hot &amp; Cold oil filters.</p>
                    </li>
                </ul>
                <ol>
                    <li>
                        <p>D) Controls and Safeties</p>
                    </li>
                </ol>
                <ul>
                    <li>
                        <p>Inspect the control panel for cleanliness.</p>
                    </li>
                    <li>
                        <p>Inspect wiring and connections for tightness and signs of overheating and discoloration.</p>
                    </li>
                    <li>
                        <p>Verify the working condition of all alarm at Dynaview display.</p>
                    </li>
                    <li>
                        <p>Test oil pressure safety device (as required). Calibrate and record setting.</p>
                    </li>
                    <li>
                        <p>Test the operation of the chilled water pump starter auxiliary contacts, if applicable.</p>
                    </li>
                </ul>
                <br>
             
                <p><b>2)Preventative Maintenance - </b></p>
                <p>Regularly scheduled preventative maintenance inspection will include the following:</p>
                <ul>
                    <li>
                        <p>Check the operation of the lubrication system.</p>
                    </li>
                    <li>
                        <p>Check the general operation of the unit.</p>
                    </li>
                    <li>
                        <p>Check the operation of the control circuit.</p>
                    </li>
                    <li>
                        <p>Log the operating temperatures, pressures, voltages, and amperages.</p>
                    </li>
                    <li>
                        <p>Review operating procedures with operating personnel.</p>
                    </li>
                    <li>
                        <p>Check the operation of the motor and starter.</p>
                    </li>
                    <li>
                        <p>Analyse the recorded data. Compare the data to the original design
                            conditions.</p>
                    </li>
                    <li>
                        <p>Provide a written report of completed work, operation log and indicate any uncorrected
                            deficiencies detected.</p>
                    </li>
                </ul>
                <p></p>
                <h4><b><u>Additions and Exceptions:-</u></b></h4>
                <ol>
                    <li>
                        <p><b><u>A) Customer shall:</u></b></p>
                    </li>
                </ol>
                <ul>
                    <li>
                        <p>Operate the equipment with manufacturer&rsquo;s recommended instructions, including normal
                            starting and stopping of the unit, and maintaining of equipment log.</p>
                    </li>
                    <li>
                        <p>Provide Hiver Aircon reasonable and safe access to all equipment covered by this service;
                            reimburse Hiver Aircon for repairs, replacements, and emergency calls occasioned by any
                            cause beyond Hiver Aircon&rsquo;s control. Such reimbursement shall be at current /
                            overtime / holiday rates for labour, and current price levels for materials unless
                            otherwise agreed and may at Hiver Aircon&rsquo;s option be subject to a separate written
                            agreement prior to its undertaking such work.</p>
                    </li>
                    <li>
                        <p>Notify Hiver Aircon of any unusual performance of equipment covered by this service; and</p>
                    </li>
                    <li>
                        <p>Permit only Hiver Aircon personnel to repair or adjust equipment and / or controls during
                            the period of this service.</p>
                    </li>
                </ul>
                <ol>
                    <li>
                        <p><u><b>B) Repairs to be provided under this agreement, unless otherwise specified, shall not
                            include any of the following:</b></u></p>
                    </li>
                </ol>
                <ul>
                    <li>
                        <p>Problems caused by utility service during the operation of the equipment or damage sustained
                            by equipment or systems not specifically included in this service.</p>
                    </li>
                    <li>
                        <p>Failure or inadequacy of any structure or foundation supporting or surrounding the equipment
                            or any portion thereof.</p>
                    </li>
                    <li>
                        <p>Furnishing any items of equipment, labour or material, or performing special tests
                            recommended or required by insurance companies or federal, state, or local governments
                            unless specifically included in the scope of work of this service.</p>
                    </li>
                    <li>
                        <p>Any damage or malfunction resulting from freezing, corrosion or erosion on the water side of
                            the equipment or caused by scale or sludge on internal tubes except where water treatment
                            protection services are provided by Hiver Aircon as part of this service.</p>
                    </li>
                    <li>
                        <p>Building access alterations which might be necessary to repair or replace Customer&rsquo;s
                            existing equipment.</p>
                    </li>
                    <li>
                        <p>Work necessitated by adjustments, repairs or alterations by parties not authorised by Hiver
                            Aircon; or</p>
                    </li>
                    <li>
                        <p>Any design defect in the equipment, or any defect or malfunction which is due to faulty
                            materials or workmanship in manufacture or which in Hiver Aircon&rsquo;s opinion has arisen
                            as a result of:</p>
                        <ul>
                            <li>
                                <p>Electrical work external to the equipment;</p>
                            </li>
                            <li>
                                <p>Transportation or relocation of the equipment not performed by or on behalf of Hiver
                                    Aircon;</p>
                            </li>
                            <li>
                                <p>Any error or omission relating to the operation of the equipment;</p>
                            </li>
                            <li>
                                <p>Any modification, adjustment or repair to the equipment made by a third party
                                    without the written consent of Hiver Aircon; or</p>
                            </li>
                            <li>
                                <p>The subjection of the equipment by the Customer to unusual physical or electrical
                                    stress, the neglect or misuse of the equipment or any failure or fluctuation of
                                    electrical power, air conditioning, humidity control or other environmental
                                    controls.</p>
                            </li>
                            <li>
                                <p>Any other work, that cannot be deemed to be included in the normal Service scope</p>
                            </li>
                        </ul>
                    </li>
                </ul>
                <p>If on investigation Hiver Aircon reasonably determines that any defect in or malfunction of the
                    equipment is the result of any of the matters referred to above, the Customer shall be liable for
                    all costs incurred by Hiver Aircon in making the investigation and determining its cause.</p>
                <ol>
                    <li>
                        <p>C) The responsibility for insuring the equipment rests solely with the Customer.</p>
                    </li>
                </ol>
                <p></p>
               <b> <p><u>Maintenance &amp; Services Included &ndash; Service Plan &ndash; Labour Amc:</u></p>
                <ul>
                    <li>
                        <p>Annual Maintenance &ndash; Yes</p>
                    </li>
                    <li>
                        <p>Preventive Maintenance Quarterly &ndash; Yes</p>
                    </li>
                    <li>
                        <p>Emergency Service &ndash; Yes</p>
                    </li>
                    <li>
                        <p>Unlimited Breakdown Calls &ndash; Yes</p>
                    </li>
                    <li>
                        <p>Spares Parts Coverage &ndash; No</p>
                    </li>
                    <li>
                        <p>Refrigerant &amp; Oil Top up &ndash; No</p>
                    </li>
                    <li>
                        <p>Condenser De-Scaling &ndash; No</p>
                    </li>
                    <li>
                        <p>Evaporator De-Scaling &ndash; No</p>
                    </li>
                </ul>
                </b>
                <p></p>
                <ol>
                    <li>
                        <p>Spare Parts: Hiver Aircon has no obligation to provide spare or replacement parts, and the
                            fees for the Service Plan do not include the cost of spare or replacement parts. If Hiver
                            Aircon provides any spare or replacement parts to the Customer, Hiver Aircon shall invoice
                            the Customer for such items per Hiver Aircon&rsquo;s prevailing price list and terms and
                            conditions, and the Customer shall promptly pay for the invoiced items per the terms and
                            conditions set out in such invoice.</p>
                    </li>
                    <li>
                        <p>Fees for Additional Services: In addition to the foregoing compensation, the Customer shall
                            compensate Hiver Aircon for any additional services requested by the Customer and performed
                            by Hiver Aircon that are outside the scope of this Agreement on terms and conditions to be
                            agreed upon by the Customer and Hiver Aircon, or in the absence of an express agreement, as
                            per Hiver Aircon&rsquo;s terms and conditions applicable at the moment of invoicing to the
                            Customer.</p>
                    </li>
                    <li>
                        <p>The Service Fee: The Customer shall pay to Hiver Aircon a service fee in the amount and as
                            per the payment terms set.</p>
                    </li>
                </ol>
                <p align="center">MUTUAL INDEMNIFICATION</p>
                <p> Either party hereby
                    agrees to indemnify, defend and hold the others harmless from
                 and against any and
                    all liability, loss, expense (including court costs and attorneys&rsquo;
                 fees) and claims of
                    any nature whatsoever arising out of or resulting from (a) non-
                 performance or breach
                    of the obligations under this Agreement or (b) any injury to
                 or death of any
                    employees or agents of either parties occurring in connection with
                 the services provided
                    under this Agreement.
            </p>
                <p align="center">TERMS AND TERMINATION</p>
                <p> Term of
                    Agreement: This Agreement shall become effective on the Effective Date</p>
                         and shall continue in
                 full force and effect through the expiration of the term set,
                         unless sooner
                 terminated by mutual written consent or by either of the Parties
                         Pursuant. If at the
                 end of any term set or any successive term, neither Party has
                         notified the other of
                 its desire to discontinue the relevant Service Plan for the
                         referenced equipment,
                 the Service Plan shall continue subject to the terms of this
                         Agreement for
                 successive terms of the same period of time as the Original Term. In
                         the case of a
                 continuation, however, Hiver Aircon shall mutually, at any time, have
                         the right to change
                 the terms and conditions of the Services and the Service Fees
                         upon thirty (30) days
                    prior notice in writing to the Customer.</p>
                <p> Termination:
                    Either Party may terminate this Agreement after giving written notice
                     to the other Party in
             the event:</p><p>
                     (a)Recurrence of
             Breach: If a breach or default is cured within the period but recurs
                     within forty-five
             (45) days after the date on which the original notice of breach or
                     default was given,
             the non-breaching Party may terminate this Agreement upon
                     Forty (40) days prior
             written notice to the breaching Party, without further
                     opportunity to cure.
                     (b)Material Breach:
             Of a material breach or material default under this Agreement
                     by the
             non-terminating Party; provided, however, that if such material breach or
                     default is subject to
             cure, the non-terminating Party shall have a period of Forty
                     (40) working days
             following notice by the terminating Party of such breach or default
                     in which to cure the
             breach or default. If the breach or default is cured within such
                     period to the
             reasonable satisfaction of the terminating Party, this Agreement shall
                     instead continue in
                    full force and effect.
                <p> (c)Termination of
                    Existence: This Agreement shall terminate immediately if either
                     Party ceases to exist
             or commences proceedings for its liquidation, insolvency or
                     reorganization
             (unless such Party has assigned or transferred its rights and
                     obligations under
             this Agreement).</p>
                     (d)Without Cause:
             Either Party may terminate this Agreement without cause upon
                     ninety (90) days
             prior written notice to the non-terminating Party; provided,
                     however, that if a
             Party terminates pursuant after such Party has received a notice of
                     material breach or
             material default pursuant but prior to curing such breach or
                     default, then such
             termination shall be deemed for the purposes of this as
                     termination resulting
             from material breach or material default by such Party
                     pursuant.</p>
                
                <p align="center">EFFECTS OF TERMINATION</p>
                <p> 1)Liability for
                    Termination: Neither Party shall have any liability to the other Party 
                 solely as a result of
         termination in accordance with or for non-renewal of this
                 Agreement.
         Termination of this Agreement for any reason shall not relieve any Party
                 for any liability or
         obligation, which may have arisen hereunder prior to such
                 termination, nor
         shall any such termination relieve any Party of any claim for
                 damages or other
         liabilities arising as a consequence of any breach by such Party of
                 the terms of this
                    Agreement.</p>
                <p> 2)Payments of Amounts
                    Due: Upon termination of this Agreement by either Party in
                 accordance with or
                    otherwise, all amounts due by the Customer pursuant to this
                 Agreement and unpaid
                    shall accrue and become immediately due and payable to
                 Hiver Aircon.</p>
                
                <p align="center">CUSTOMER&rsquo;S OBLIGATIONS</p>
                <p> The Customer shall:</p>
                <ul>
                    <li>
                        <p>At all times keep the Equipment in the condition recommended by the</p>
                    </li>
                </ul>
                <p >manufacturer of the Equipment;</p>
                <ul>
                    <li>
                        <p>Not move the Equipment from its present location without obtaining the prior</p>
                    </li>
                </ul>
                <p>written consent of Hiver Aircon;</p>
                <ul>
                    <li>
                        <p>Use and operate the Equipment only in accordance with such instructions and</p>
                    </li>
                </ul>
                <p>recommendations relating to the care and operation of the Equipment as may be issued by the
                    manufacturer of the Equipment or as may from time to time be advised in writing by Hiver Aircon;
                    and</p>
                <ul>
                    <li>
                        <p>Not allow any person other than Hiver Aircon&rsquo;s representatives to adjust,</p>
                    </li>
                </ul>
                <p>maintain, repair, replace or remove any part of the Equipment.</p>
                <ul>
                    <li>
                        <p>The Customer shall ensure that Hiver Aircon&rsquo;s employees and/or representatives</p>
                    </li>
                </ul>
                <p>have full and free access to the Equipment and to any records of its use kept by the Customer in
                    order to enable Hiver Aircon to perform its duties hereunder.</p>
                <ul>
                    <li>
                        <p>The Customer shall provide Hiver Aircon with such information concerning the</p>
                    </li>
                </ul>
                <p> Equipment, its application, use, location and environment as
                    Hiver Aircon may</p>
                <p> reasonably request in order to enable Hiver Aircon to perform
                    its duties </p>
                <p> hereunder.</p>
                <ul>
                    <li>
                        <p>The Customer shall take all such steps as may be necessary to ensure the safety of Hiver
                            Aircon&rsquo;s employees and/or representatives that visit any premises of the Customer.</p>
                    </li>
                </ul>
                <p></p>
                <p align="center">LIMITATION OF LIABILITY</p>
                <p>1 Liability: No liability whatsoever for
                    direct damages shall attach to Hiver Aircon until the Service have been paid for in full and, then
                    said liability shall not exceed the Service Fees charged by Hiver Aircon for the Services rendered
                    in relation to the specific task or project performed.</p>
                <p>2 Hiver Aircon shall be under no liability for
                    any damage to the Equipment caused by the operation of the Equipment outside the Equipment&rsquo;s
                    designed use and conditions of use.</p>
                <p>3 Hiver Aircon shall be under no liability to
                    Customer for any indirect or consequential damage resulting from delay, trading loss or damage, or
                    for any other indirect or consequential damage.</p>
                <p></p>
                <p align="center"> GOVERNING LAW AND DISPUTE RESOLUTION</p>
                <p>1 Governing Law: This Agreement shall be
                    interpreted, construed and enforced in accordance with the laws and regulations of India.</p>
                <p>2 Negotiations: In the event that a
                    dispute arises between the Parties with respect to this Agreement, the Parties shall negotiate in
                    good faith in an attempt to resolve such dispute.</p>
                <p>3 Parties submit to the exclusive jurisdiction
                    of the competent Courts of Mumbai, India.</p>
                <p>4 Notice: Each Party agrees that any notice
                    relating to an arbitration proceeding hereunder shall be sufficiently served if delivered according
                    to the terms.</p>
                <p align="center">FORCE MAJEURE</p>
                <p>1 Force majeure events: Neither Party shall be
                    responsible for total or partial non-performance due to circumstances beyond its reasonable
                    control, including, without limitation, Acts of God, accidents, compliance with any law, regulation
                    or other government order (whether or not valid), war or national emergency, riots, fire,
                    industrial action, shortages of labour, restraints or delays affecting carriers or inability or
                    delay in obtaining supplies of adequate or suitable materials.</p>
                <p>2 Notification and termination: The Party
                    invoking any such events shall promptly notify the other Party, giving an estimate as to the
                    foreseeable duration thereof. Should the event last for a continuous period in excess of 3 (three)
                    months, either Party shall be entitled to terminate the affected order or the
                    Agreement.</p>
                <p></p>
                <p align="center">MISCELLANEOUS</p>
                <p>Notice: Any notice, consent, request, instruction, approval or other communication required or
                    permitted under this Agreement or any other document or instrument delivered in connection herewith
                    shall be deemed to have been validly given, made or served if in writing and delivered by hand, or
                    transmitted by facsimile, or mailed by certified mail, return receipt requested, postage prepaid
                    and properly addressed, or sent by express courier service, to the respective Party to whom such
                    notice, consent, request, instruction, approval or other communication relates at the following
                    address or email:</p>
                <div class="col-sm-6">If to Hiver Aircon:<br>
                <h4><b>Hiver Aircon Private Ltd. </b> </h4>                         
   Unit No. 104, 01st Floor <br>
   Garnet Paladium, WE Hwy,      <br>                       
   Nr Oberoi Mall, Malad (East), <br>
   Mumbai - 400 097.                  <br>                                   
   Attn: Mr. Shivaji Darade    <br>
   Tel: 022 28304470	            <br>              
   Mob: 9773159000               <br>                        
   Email: shivajidarade@hiver.co.in                 

                </div>
                <div class="col-sm-6"> If to the Customer:
               <h4><b> Emcure Pharmaceuticals Ltd.</b></h4>
                            Plot No - P1, I.T.B.T Park <br>
                            Phase – 2, MIDC, Hinjwadi, <br>
                            Pune - 411057.India <br>
                            Attn: Mr. Sachin Phalke  <br>                                             	                   
                            Tel:  02039821300 <br>
                            Mob: 9422523214 <br>
	                        Email: Sachin.Phalke@emcure.co.in 
                </div>
                <p> All such notices shall be
                    deemed given on the date of receipt, as evidenced by email confirmation, return receipt, courier
                    record or otherwise. A Party may change its address or facsimile number for purposes by
                    notice to the other Party at least five (5) days prior to the effectiveness of such change.</p>
                <p>2 Headings: The captions or headings in this
                    Agreement are made for convenience and general reference only and shall not be construed to
                    describe, define or limit the scope or intent of the provisions of this Agreement.</p>
                <p>3 Assignment: Customer shall not transfer,
                    assign or otherwise dispose of this Agreement, in whole or in part, to any persons, firm or company
                    without express prior written consent of Hiver Aircon.</p>
                <p>4 Independent Contractors: The
                    relationship of the Parties under this Agreement is that of independent contractors. Nothing
                    contained in this Agreement is intended or is to be construed so as to constitute the Parties as
                    partners, joint ventures or any Party as an agent or employee of the other. Neither Party has
                    any express or implied right under this Agreement to assume or create any obligation on behalf of
                    or in the name of the other, or to bind the other Party to any contract, agreement or undertaking
                    with any third Party, nor shall no conduct of the Parties be deemed to infer such right.</p>
                <p>5 Entire Agreement; Amendment: This Agreement
                    including the exhibits hereto contains the entire agreement of the Parties and supersedes any and
                    all prior agreements between the Parties, written or oral, with respect to the transactions
                    contemplated hereby. Any modification or amendment of any provision of this Agreement shall only be
                    valid if made in writing and so executed by the Parties.</p>
                <p>6 Counterparts: This Agreement may be
                    executed in several counterparts, each of which, when so executed, shall be deemed to be an
                    original, and such counterparts shall, together, constitute and be one and the same instrument.</p>
                <p>7 Binding Effect: This Agreement shall
                    be binding upon and shall inure to the benefit of the Parties hereto, and their respective
                    successors and permitted assigns, and no other person shall have any right under or by virtue of
                    this Agreement.</p>
                <p>8 Waiver: Failure or delay by Hiver Aircon in
                    enforcing or partially enforcing any provision of this Agreement will not be construed as a waiver
                    of any of its rights under this Agreement. Any waiver by Hiver Aircon of any breach of, or any
                    default under, any provision of this Agreement by Customer will not be deemed a waiver of any
                    subsequent breach or default and will in no way affect the other terms of this Agreement.</p>
                <p>9 Calendar: All dates, years and time
                    periods referenced in this Agreement and the schedules hereto shall be determined using the
                    Gregorian calendar, unless specifically stated otherwise herein.</p>
                <p>10 Remedies: Each right or remedy under this
                    Agreement is without prejudice to any other right or remedy whether under the Agreement or not.</p>
                <p>11 Survival: If any provision of this Agreement is or
                    shall become wholly or partly void, it shall to the extent of such voidness be deemed severable and
                    the remaining provisions of this Agreement and the remainder of such provision shall continue in
                    full force and effect. To the void provision shall be substituted a valid provision which comes as
                    close as possible to the economic purpose of the void provision.</p>
                <p>12 Language: The language of this Agreement and of
                    all communications between the Parties shall be English.</p>
                <p></p>
                <p>The both Parties have duly signed and executed this Agreement as of the Effective Date.</p>
                <div class="col-sm-6">For and on behalf<br>Emcure Pharmaceuticals Limited.<br>By: _____________________ <br>Name: Mr. Sachin Phalke <br>Title: Sr. Manager - Engineering </div>
                
                <div class="col-sm-6">For and on behalf of<br>Hiver Aircon Private Limited.<br>By: _____________________<br>Name : Mr. Shivaji Darade<br>Title: Managing Director</div>

           
            </div>
        </div>

    </div>
</div>



<?php
    include($base.'_in/footer.php');
?>
<script>
