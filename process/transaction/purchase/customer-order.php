<?php 
$base='../../../';
$navenq3='background:#1B1464;';
  include('header.php');
?>
<br>

<div class="table-ui container-fluid">
<div class="tr row">
<div class="col-sm-1 th">Enquiry No.</div>    
<div class="col-sm-3 th">Customer Info</div>
<div class="col-sm-2 th">Enquery For</div>
<div class="col-sm-1 th">Enquiry confirm</div>
<div class="col-sm-1 th">Costing confirm</div>
<div class="col-sm-2 th">Costing Excel</div>
<div class="col-sm-2 th">Action</div>
</div>
<?php

$result=mysqli_query($con,"SELECT extra,enquiryid,customerid,enquirystatus,quotation,followupnote,category,created_by,created_time FROM enqtable WHERE category='Product' AND enquirystatus<>0 AND quotation<>0 AND customerorder=0 AND customerorderapproval=0 AND profarma=0 AND payment=0 AND production=0 AND dispatch=0 AND cancel=0 ORDER BY created_time DESC");
while($rows = mysqli_fetch_assoc($result)){
    $ccode=$rows['customerid'];
    $cifo=mysqli_fetch_assoc(mysqli_query($con,"SELECT cname,ccontact,caddress,cgstno FROM mcustmast WHERE ccode='$ccode' ORDER BY created_time DESC"));
    $enquiryidbom=$rows['enquiryid'];
?>

<div class="row tr">
<div class="col-sm-1 td"><?php echo $rows['enquiryid']; ?></div>
<div class="col-sm-3 td">
<?php 

echo 'Company : '.$cifo['cname']; 
echo '<br>Address : '.$cifo['caddress']; 
echo '<br>Name  : '.get_object_vars(json_decode($cifo['ccontact'])[0])['table'][0];
echo '<br>Dept. : '.get_object_vars(json_decode($cifo['ccontact'])[0])['table'][1];
echo '<br>Mobile : <a href="tel:'.get_object_vars(json_decode($cifo['ccontact'])[0])['table'][3].'">'.get_object_vars(json_decode($cifo['ccontact'])[0])['table'][3].'</a>';
echo '<br>Landline'.get_object_vars(json_decode($cifo['ccontact'])[0])['table'][4];
echo '<br>Email : <a href="mailto:'.get_object_vars(json_decode($cifo['ccontact'])[0])['table'][5].'">'.get_object_vars(json_decode($cifo['ccontact'])[0])['table'][5].'</a>';
?>
</div>
<div class="col-sm-2 td"><?php

$extra=json_decode('['.$rows['extra'].']');

foreach($extra as $i){
 // get_object_vars($i)['itemcode']
  echo 'BOM Name :'.get_object_vars($i)['name'].'<br>';
  echo 'Drawing No. : '.get_object_vars($i)['drwingno'].'<br>';
  echo 'Rev No. : '.get_object_vars($i)['RevNo'].'<br>';
 echo '<hr style="border:1px solid #000;margin:1px;padding:0px;color:#000;">';
}

?>
</div>
<div class="col-sm-1 td"><?php 
$lastfollowup=$rows['enquirystatus'];
$lastfollowup=date("d-m-Y",$lastfollowup/1000); 
echo '<p style="background:#27ae60;color:#fff;">'.$lastfollowup.'</p>'; 
$category=$rows['category'];
$link='#';
  if($category=='Product'){ $link='/process/transaction/party-purchase-order/index.php?enq='.$rows['enquiryid']; }
  if($category=='Services'){ $link='/process/transaction/enquiry1/quatation/service.php'; }
  if($category=='HVC Part'){ $link='/process/transaction/enquiry1/quatation/hvc-part.php'; }
  if($category=='Rental Chillers'){ $link='/process/transaction/enquiry1/quatation/rental.php'; }

?></div>
<div class="col-sm-1 td">
<?php
$quotation=$rows['quotation'];
$quotation=date("d-m-Y",$quotation/1000);
echo '<p style="background:#27ae60;color:#fff;">'.$quotation.'</p>'; 
?>
</div>
<div class="col-sm-2 td">
<?php 
foreach($extra as $i){
  // get_object_vars($i)['itemcode']
   echo 'BOM Name :'.get_object_vars($i)['name'].'<br>';
 $enquir=str_replace('/', '-',$rows['enquiryid']);
 $path="quatation/pdf/".$enquir.'-'.get_object_vars($i)['id'].'.xlsx';
 if(file_exists($path)){
 echo '<center><a href="'.$path.'" target="_blank">View xlsx</a></center>';
 }else{
   echo 'Upload excel file';
 }
 echo '<hr style="margin:1;padding:0px;border:1px solid #000;">
 ';

}
?>
</div>
<div class="col-sm-2 td">
<a class="btn btn-primary btn-sm btn-block" href="<?php echo $link;?>" onclick="createquatation(this)">Create Customer Order</a>
<button class="btn btn-sm btn-block btn-danger" data-enquiryid="<?php echo $rows['enquiryid']; ?>" onclick="cancel(this)">Cancel</button>
</div>

</div>
<?php
}
?>
</div>
</div>
</div>

<?php
    include($base.'_in/footer.php');
?>
<script>
  function closemodal(e){
    $('#'+e).modal('toggle');
  }

  
  function confirmquatation(e){
  var enquiryid=$(e).data('enquiryid');
  $.ajax({
        type: "POST",
        data: 'enquiryid='+enquiryid,
        url: 'quatation/confirmquatation.php',
        cache: false,
        success: function (res) {
          if (res.status == 'success') {
             window.location.reload();
          }
        }
    })
}

  </script>
  <script>
function cancel(e){
     var enquiryid=$(e).data('enquiryid');
  $.ajax({
        type: "POST",
        data: 'enquiryid='+enquiryid,
        url: 'api/cancel.php',
        cache: false,
        success: function (res) {
          if (res.status == 'success') {
             window.location.reload();
          }
        }
    })
}
</script>