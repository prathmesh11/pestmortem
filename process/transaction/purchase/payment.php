<?php 
$base='../../../';
$navenq6='background:#1B1464;';
  include('header.php');
?>
<br>
<style>
.table-list td,
   .table-list th {
       border: 1px solid #ddd;
       padding: 1px !important;
       font-size: 13px;
   }
   .table-list td{
	padding-top: 10px !important;
   }

   .table-list tr:nth-child(even) {
       background-color: #f2f2f2;
   }

   .table-list th {
       padding-top: 5px;
       padding-bottom: 5px;
       text-align: center;
       background-color: #16a085;
       color: white;
   }
</style>
<div class="table-ui container-fluid">
<div class="tr row">
<div class="col-sm-1 th">Enquiry No.</div>    
<div class="col-sm-3 th">Customer Info</div>
<div class="col-sm-1 th">Costing</div>
<div class="col-sm-6 th">Customer Order Info</div>
<div class="col-sm-1 th">Follow Up</div>
</div>
<?php

$result=mysqli_query($con,"SELECT extra,followuptime,followupnote,enquiryid,customerid,enquirystatus,quotation,followupnote,category,created_by,created_time FROM enqtable WHERE category='Product' AND enquirystatus<>0 AND quotation<>0 AND customerorder<>0 AND customerorderapproval<>0 AND profarma<>0 AND payment=0 AND dispatch=0 AND cancel=0 ORDER BY created_time DESC");
while($rows = mysqli_fetch_assoc($result)){
    $ccode=$rows['customerid'];
    $cifo=mysqli_fetch_assoc(mysqli_query($con,"SELECT cname,ccontact,caddress,cgstno FROM mcustmast WHERE ccode='$ccode' ORDER BY created_time DESC"));
    $enquiryidbom=$rows['enquiryid'];
?>

<div class="row tr">
<div class="col-sm-1 td"><?php echo $rows['enquiryid']; ?></div>
<div class="col-sm-3 td">
<?php 

echo 'Company : '.$cifo['cname']; 
echo '<br>Address : '.$cifo['caddress']; 
echo '<br>Name  : '.get_object_vars(json_decode($cifo['ccontact'])[0])['table'][0];
echo '<br>Dept. : '.get_object_vars(json_decode($cifo['ccontact'])[0])['table'][1];
echo '<br>Mobile : <a href="tel:'.get_object_vars(json_decode($cifo['ccontact'])[0])['table'][3].'">'.get_object_vars(json_decode($cifo['ccontact'])[0])['table'][3].'</a>';
echo '<br>Landline'.get_object_vars(json_decode($cifo['ccontact'])[0])['table'][4];
echo '<br>Email : <a href="mailto:'.get_object_vars(json_decode($cifo['ccontact'])[0])['table'][5].'">'.get_object_vars(json_decode($cifo['ccontact'])[0])['table'][5].'</a>';
?>
</div>


<div class="col-sm-1 td">
<?php 
$extra=json_decode('['.$rows['extra'].']');
$quotation=$rows['quotation'];
$quotation=date("d-m-Y",$quotation/1000);
echo '<p style="background:#27ae60;color:#fff;">'.$quotation.'</p>'; 
foreach($extra as $i){
  // get_object_vars($i)['itemcode']
   echo 'BOM Name :'.get_object_vars($i)['name'].'<br>';
 $enquir=str_replace('/', '-',$rows['enquiryid']);
 $path="quatation/pdf/".$enquir.'-'.get_object_vars($i)['id'].'.xlsx';
 if(file_exists($path)){
 echo '<center><a href="'.$path.'" target="_blank">View xlsx</a></center>';
 }else{
   echo 'Upload excel file';
 }
 echo '<hr style="margin:1;padding:0px;border:1px solid #000;">
 ';

}
?>
</div>
<div class="col-sm-6 td">
<?php 


$custorder=mysqli_fetch_assoc(mysqli_query($con,"SELECT ppno,ppdate,saletype,tablejson,created_by,created_time FROM partypurchase WHERE enquiryid='$enquiryidbom'"));

echo 'Customer Order No : '.$custorder['ppno'].' || ';
echo 'Customer Order Date : '.date('d/m/Y',  strtotime($custorder['ppdate'])).' <br> ';
$created_by=$custorder['created_by'];
$created_by=mysqli_fetch_assoc(mysqli_query($con,"SELECT username x FROM admin WHERE aid='$created_by'"))['x'];
echo 'Created By : '.$created_by.' || ';
echo 'Created Date : '.date("d-m-Y",$custorder['created_time']/1000) .'<br>';
$profarmapolicy=mysqli_fetch_assoc(mysqli_query($con,"SELECT policy x FROM profarma WHERE enquiryid='$enquiryidbom'"))['x'];

$lastfollowup=$rows['followuptime'];
$followUpStatusFlag='';

if($lastfollowup!=0){
  if($START_OF_THE_DAY<$lastfollowup && $END_OF_THE_DAY<=$lastfollowup){
      $followUpStatusFlag='#3F51B5';
  }

  if($START_OF_THE_DAY>$lastfollowup && $END_OF_THE_DAY>$lastfollowup){
      $followUpStatusFlag='#F44336';
  }
  
  if($START_OF_THE_DAY<=$lastfollowup && $END_OF_THE_DAY>$lastfollowup ){
      $followUpStatusFlag='#4CAF50';
  }
}else{
  $followUpStatusFlag='#ff9800';
}
 if($lastfollowup==0){ $lastfollowup='NO Followup'; }else{ $lastfollowup=date("d-m-Y",$lastfollowup/1000); }
echo '<p style="background:'.$followUpStatusFlag.';color:#fff;">Follow Up :'.$lastfollowup.'<br>Note : '.$rows['followupnote'].'</p>'; 

?>
<table class="table-list table">
    <thead>
      <tr>
        <th>Product</th>
        <th>Qty</th>
        <th>Rate</th>
        <th>Amt</th>
        <th>GST</th>
        <th>Total</th>
      </tr>
    </thead>
    <tbody>
    <?php
    $tablejson=$custorder['tablejson'];
    $tablejson=json_decode($tablejson);
  
    foreach($tablejson as $i){
      
        echo '<tr>';
        echo '<td>'.get_object_vars($i)['itemname'].'</td>';
        echo '<td>'.get_object_vars($i)['qty'].'</td>';
        echo '<td>'.get_object_vars($i)['rate'].'</td>';
        echo '<td>'.get_object_vars($i)['amount'].'</td>';
        echo '<td>'.(get_object_vars($i)['cgstamt']+get_object_vars($i)['sgstamt']+get_object_vars($i)['igstamt']).'</td>';
        echo '<td>'.get_object_vars($i)['netamt'].'</td>';
        echo '</tr>';
    }
    ?>
    </tbody>
    </table>
<?php



$profarma=mysqli_fetch_assoc(mysqli_query($con,"SELECT tablejson x FROM profarma WHERE enquiryid='$enquiryidbom'"))['x'];
$profarma=json_decode($profarma);

?>


<table class="table-list table">
<thead>
<tr>
<th>Payment No.</th>
<th>Payment % </th>
<th>Payment Amount</th>
<th>Payment Policy</th>
<th>Payment Status</th>
<th>Action </th>
</tr>
</thead>
<tbody>
<?php 
foreach($profarma as $i){
    $btn='Paid';
    $status='Pending';
    if(get_object_vars($i)['status']){
        $status=date("d-m-Y",get_object_vars($i)['status']/1000);
    }else{
        $btn='<button class="btn btn-sm btn-primary" data-enquiryid="'.$rows['enquiryid'].'" data-no="'.get_object_vars($i)['no'].'" data-amt="'.get_object_vars($i)['amt'].'" onclick="payment(this)" >Paid</button>'; 
    }
    echo '<tr>';
    echo '<td>'.get_object_vars($i)['no'].'</td>';
    echo '<td>'.get_object_vars($i)['per'].'</td>';
    echo '<td>'.get_object_vars($i)['amt'].'</td>';
    echo '<td>'.get_object_vars($i)['policy'].'</td>';
    echo '<td>'.$status.'</td>';
    echo '<td>'.$btn.' <button class="btn btn-sm btn-warning" data-enquiryid="'.$rows['enquiryid'].'" data-no="'.get_object_vars($i)['no'].'" onclick="profarmaprint(this)" >Print</button></td>';
    echo '</tr>';
}
?>
</tbody>
</table>
</div>
<div class="col-sm-1 td">

<button class="btn btn-success btn-sm btn-block" data-toggle="modal" data-target="#myModal2" data-enquiryid="<?php echo $rows['enquiryid']; ?>" onclick="setidtomidal(this)">Set Follow Up</button>
<button class="btn btn-sm btn-block btn-danger" data-enquiryid="<?php echo $rows['enquiryid']; ?>" onclick="cancel(this)">Cancel</button>

</div>

</div>
<?php
}
?>
</div>
</div>
</div>
<div id="myModal2" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" align="center">Payment Followup</h4>
      </div>
      <div class="modal-body">
          <div class="form-group row">
       <div class="col-sm-4">Set New Followup</div>
       <input type="hidden" data-name="enquiryid" id="inp-enquiryidset">
       <div class="col-sm-8"><input type="text" id="inp-enq-olddate" class="form-control input-sm">
            <input type="hidden" id="inp-enq-olddate-stamp" data-name="followuptime">
       </div>
       <div class="col-sm-4">Note</div>
       <div class="col-sm-8"><input type="text" data-name="followupnote" class="form-control input-sm"></div>
       <div class="col-sm-4"></div>
       <div class="col-sm-8"><button class="btn btn-success btn-block btn-sm" onclick="setqneuiry()">Add New FollowUp</button></div>
        </div>
        
      </div>
    </div>
  </div>
</div>
<textarea style="display:none;" id='base64'></textarea>
<?php
    include($base.'_in/footer.php');
?>
<script>
function getBase64FromImageUrl(url) {
        var img = new Image();

        img.setAttribute('crossOrigin', 'anonymous');

        img.onload = function () {
            var canvas = document.createElement("canvas");
            canvas.width = this.width;
            canvas.height = this.height;
            var ctx = canvas.getContext("2d");
            ctx.drawImage(this, 0, 0);
            var dataURL = canvas.toDataURL("image/png");
            var base64 = dataURL.replace(/^data:image\/(png|jpg);base64,/, "");
            $('#base64').val(base64);
        };
        img.src = url;
    }
getBase64FromImageUrl('/img/icon/hiverlogo.jpg');

function profarmaprint(e) {
  var enquiryid = $(e).data('enquiryid');
  var no = $(e).data('no');
  $.ajax({
    type: "POST",
    data: 'enquiryid=' + enquiryid + '&no=' + no,
    url: 'api/profarmaprint.php',
    cache: false,
    success: function (res) {
      if (res.status == 'success') {
        var json = res.json;
       
        var content = [];

        var table1 = {
          widths: ['auto', '*', '*', '*'],
          dontBreakRows: true,
          body: []
        };
        var base64 = 'data:image/png;base64,' + $('#base64').val();
        content.push({
          layout: 'noBorders',
          table: {
            widths: ['*', '*'],
            body: [
              ['', {
                text: "HIVER Aircon Private Limited\n",
                color: 'blue',
                fontSize: 16,
                bold: true
              }],
              [{
                image: base64,
                width: 180,
                height: 70
              }, {
                text: 'Address : Garnet Paladium, Unit No. 104, First Floor, Western Express Hwy, Near Oberoi Mall Junction, Pandit M. Nehru Road, Behind Express Zone Buss Park, Goregaon East (E), Mumbai - 400097, India \nTel : +91 22 28304470;28714470;9773159000 \nEmail : shivajidarade@hiver.co.in, hiverindia@gmail.com, \nWebsite : www.hiver.co.in',
                alignment: 'justify',
                fontSize: 9
              }],
            ]
          }
        });
  
  content.push({
    text: '\n PRO-FORMA INVOICE \n',
    bold: true,
    fontSize: 14,
    alignment:'center'
  });

  content.push({text: 'Customer :'+json.custname, bold: true, fontSize: 11 });
  content.push({text: 'Address  :'+json.address, bold: true, fontSize: 11 });
  content.push({text: 'GSTN No. :'+json.cgstno, bold: true, fontSize: 11 });
  content.push({ text: '\n'});

var d = new Date();
var month = d.getMonth()+1;
var day = d.getDate();

var output = d.getFullYear() + '-' +
    (month<10 ? '0' : '') + month + '-' +
    (day<10 ? '0' : '') + day;

  content.push({
    style: 'tablfont1',
    table: {
      widths: ['*','*','*','*'],
      body: [
        [{text:'Pro-forma No.:'+json.no},{text:'Pro-forma Date:'+reversedate(output)},{text:'Enquiry No.:'+json.enquiryid},{text:''}],
        [{text:'Customer PO.No:'+json.custpono+'\nPO.Date:'+reversedate(json.custpodate)}, {text:'Date Shipped:'}, {text:'Delivery Method:'},{text:'Delivery Terms Ex- site:'}],
        [{text:'Order Ref No:'+json.ppno}, {text:'Date Requested:'+reversedate(json.ppdate)}, {text:'Our Reference: Mr. Shivaji Darade'},{text:'Customer Reference:'+json.custref}],
      ]
    }
  });
  content.push({ text: '\n'});
  content.push({text: 'GOODS & SERVICE CHARGES:', bold: true, fontSize: 11, decoration: 'underline' });

var table1={
  widths: [30,'*',50,150],
  body:[]
};
  var header=[{text:'SR.NO.',alignment:'center', bold: true},{text:'DESCRIPTION',alignment:'center', bold: true},{text:'QTY',alignment:'center', bold: true},{text:'PRICE INR',alignment:'center', bold: true}];
  table1['body'].push(header);

  var tablejson=json.tablejson;
  var count=1;
var totalamount=0;
var cgst=0;
var sgst=0;
var igst=0;
var cgstperc=0;
var sgstperc=0;
var igstperc=0;
var totalamountgst=0;
  for(var i in tablejson){
  var arr=[];

var amount=parseFloat(tablejson[i].amount);
var itemname=tablejson[i].itemname;
var netamt=parseFloat(tablejson[i].netamt);
var qty=parseFloat(tablejson[i].qty);
var cgstamt=parseFloat(tablejson[i].cgstamt);
var sgstamt=parseFloat(tablejson[i].sgstamt);
var igstamt=parseFloat(tablejson[i].igstamt);

cgstperc=tablejson[i].cgstperc;
sgstperc=tablejson[i].sgstperc;
igstperc=tablejson[i].igstperc;

totalamount+=amount;
cgst+=cgstamt;
sgst+=sgstamt;
igst+=igstamt;
totalamountgst+=netamt;

arr.push({text:count,alignment:'center'},{text:itemname,alignment:'center'},{text:qty,alignment:'center'},{text:amount,alignment:'center'});
table1['body'].push(arr);
count++;
  }

  var header0=['',{colSpan:2,text:'Total Price (Excluding of GST @ '+json.gst+' %)',alignment:'right', bold: true},'',{text:totalamount,alignment:'center'}];
  table1['body'].push(header0);
  var header1=['',{colSpan:2,text:'CGST @ '+cgstperc+' %',alignment:'right', bold: true},'',{text:cgst,alignment:'center'}];
  table1['body'].push(header1);
  var header2=['',{colSpan:2,text:'SGST @ '+sgstperc+' %',alignment:'right', bold: true},'',{text:sgst,alignment:'center'}]; 
  table1['body'].push(header2);
  var header3=['',{colSpan:2,text:'IGST @ '+igstperc+' %',alignment:'right'},'',{text:igst,alignment:'center'}];
  table1['body'].push(header3);
  var header4=['',{colSpan:2,text:'Total Price (Including of GST @ '+json.gst+' %)',alignment:'right', bold: true},'',{text:totalamountgst,alignment:'center'}];
  table1['body'].push(header4);
  var header5=[{colSpan:4,text:'.'},'','',''];
  table1['body'].push(header5);
  var header6=[{colSpan:3,text:'Total '+json.per+' % '+json.policy+' Price (Including GST) INR',alignment:'right', bold: true},'','',{text:json.amount,alignment:'center'}];
  table1['body'].push(header6);

  content.push({
    style: 'tablfont1',
    table: table1
  });

  content.push({ text: '\n'});
  content.push({text:'For Hiver Aircon Private Limited', bold: true, fontSize: 11, alignment:'right'});
  content.push({text:'\n\n(Authorized Signatory)', bold: true, fontSize: 10, alignment:'right'});
  content.push({ text: '\n'});
  content.push({
    style: 'tablfont1',
    table: {
      widths: ['*','*','auto'],
      body: [
        ['PAN: AADCH2014H','GSTIN: 27AADCH2014H1Z0','CIN: U31500MH2013PTC244687'],
        [{colSpan:3,text:'RTGS/ NEFT DETAILS:'}, '', ''],
        ['Bank Name: HDFC Bank Ltd.', {colSpan:2,text:'Branch: Dindoshi, Goregaon (E), Mumbai - 400063'}, ''],
        ['Current Account No: 50200035204989', {colSpan:2,text:'IFSC Code:HDFC0000212'}, '']
      ]
    }
  });

                                                      

        dd = {
    pageSize: 'A4',
    pageOrientation: 'portrait',
    pageMargins: [40, 30, 30, 35],
    footer: function (currentPage, pageCount) {
      return {
        margin: 10,
        columns: [{
          fontSize: 8,
          text: [{
              text: '--------------------------------------------------------------------------' +
                '\n',
              margin: [0, 20]
            },
            {
              text: '© HIVER AIRCON PRIVATE LIMITED | PAGE ' + currentPage.toString() + ' of ' + pageCount,
            }
          ],
          alignment: 'center'
        }]
      };

    },
    content,
    styles: {
      tablfont: {
        fontSize: 7
      },
      tablfont1: {
        fontSize: 9
      }
    }
  }
  var win = window.open('', '_blank');
  pdfMake.createPdf(dd).open({}, win);

      }
    }
  })
}

function reversedate(stringdate){
	var d=new Date(stringdate);
	var month = d.getUTCMonth() + 1; //months from 1-12
	var day = d.getUTCDate();
	var year = d.getUTCFullYear();
	return day+'-'+month+'-'+year;
}

function payment(e){
  var no=$(e).data('no');
  var amt=$(e).data('amt');
  var enquiryid=$(e).data('enquiryid');
  $.ajax({
        type: "POST",
        data: 'enquiryid='+enquiryid+'&no='+no+'&amt='+amt,
        url: 'api/payment.php',
        cache: false,
        success: function (res) {
          if (res.status == 'success') {
             window.location.reload();
          }
        }
    })
}

  </script>
  <script>
function cancel(e){
     var enquiryid=$(e).data('enquiryid');
  $.ajax({
        type: "POST",
        data: 'enquiryid='+enquiryid,
        url: 'api/cancel.php',
        cache: false,
        success: function (res) {
          if (res.status == 'success') {
             window.location.reload();
          }
        }
    })
}
$("#inp-enq-olddate").pickadate({
        selectYears:true,
        selectMonths:true,
        min:true,
        onClose: function(){
            try{
                var dateStamp = this.get('select')['pick'];
                $("#inp-enq-olddate-stamp").val(dateStamp);
            }catch(err){

            }
        }
    });

    function setidtomidal(e){
var enquiryid=$(e).data('enquiryid');
$('#inp-enquiryidset').val(enquiryid);
}
    function setqneuiry(){
    var valid = true;
  if (checker('myModal2') != false) {
    valid = valid * true;
  } else {
    valid = valid * false;
  }
  if (valid) {
    var data=checker('myModal2');
    var datastr = JSON.stringify(data);
    $.ajax({
        type: "POST",
        data: {
          data: datastr
        },
        url: 'api/paymentfollowup.php',
        cache: false,
        success: function (res) {
          if (res.status == 'success') {
             window.location.reload();
          }
        }
      })
    }
  }


</script>