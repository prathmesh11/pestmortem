<?php 
$base='../../../';
$navenq1='background:#1B1464;';
  include('header.php');
  $enquiryidnew=mysqli_fetch_assoc(mysqli_query($con,"SELECT id x FROM enqtable ORDER BY enquiryid DESC"))['x'];

  $year1=0;
  $year2=0;

if ( date('m') > 3 ) {
    $year1 = date('y');
    $year2 = date('y')+1;
}else{
    $year1 = date('y')-1;
    $year2 = date('y');
}

if (session_status() == PHP_SESSION_NONE) { session_start(); }
$sessionby = $_SESSION['aid'];
   

  if($enquiryidnew){
    $enquiryidnew++;
    $enquiryidnew='CH/'.$year1.'-'.$year2.'/'.$sessionby.'/'.$enquiryidnew;
  }else{
    $enquiryidnew='CH/'.$year1.'-'.$year2.'/'.$sessionby.'/1';
  }

  $json1='';
  $result11=mysqli_query($con,"SELECT customerid,customername  FROM customermaster ORDER by customerid ASC");
  while($rows = mysqli_fetch_assoc($result11)){
   $customerid=$rows["customerid"];
   $customername=$rows["customername"];
   $json1.=',{"customerid":"'.$customerid.'","customername":"'.$customername.'"}';   
  }
  $json1=substr($json1,1);
  $json1='['.$json1.']';
?>
<br>
<input type="hidden" id="item-json1" value='<?php echo str_replace("'"," &#39;",$json1); ?>'/>

<div class="table-ui container-fluid">
<div class="tr row">
<div class="col-sm-1 th">Enquiry No.</div>    
<div class="col-sm-3 th">Customer Info</div>
<div class="col-sm-2 th">Enquery For</div>
<div class="col-sm-2 th">Note</div>
<div class="col-sm-2 th">Follow Up Status</div>
<div class="col-sm-2 th">Action</div>
</div>
<?php
$result=mysqli_query($con,"SELECT enquiryid,customerid,followuptime,followupnote,category,created_by,created_time FROM enqtable WHERE enquirystatus=0 AND quotation=0 AND customerorder=0 AND customerorderapproval=0 AND profarma=0 AND payment=0 AND production=0 AND dispatch=0 AND cancel=0 ORDER BY created_time DESC");

while($rows = mysqli_fetch_assoc($result)){

    $customerid=$rows['customerid'];
    $cifo=mysqli_fetch_assoc(mysqli_query($con,"SELECT customername,contactjson,customerjson,gstnumber FROM customermaster WHERE customerid='$customerid' ORDER BY created_time DESC"));
?>

<div class="row tr">
<div class="col-sm-1 td"><?php echo $rows['enquiryid']; ?></div>
<div class="col-sm-3 td">
<?php 

echo 'Company : '.$cifo['customername']; 
echo '<br>Address : '.$cifo['contactjson']; 
echo '<br>Name  : '.get_object_vars(json_decode($cifo['ccontact'])[0])['table'][0];
echo '<br>Dept. : '.get_object_vars(json_decode($cifo['ccontact'])[0])['table'][1];
echo '<br>Mobile : <a href="tel:'.get_object_vars(json_decode($cifo['ccontact'])[0])['table'][3].'">'.get_object_vars(json_decode($cifo['ccontact'])[0])['table'][3].'</a>';
echo '<br>Landline'.get_object_vars(json_decode($cifo['ccontact'])[0])['table'][4];
echo '<br>Email : <a href="mailto:'.get_object_vars(json_decode($cifo['ccontact'])[0])['table'][5].'">'.get_object_vars(json_decode($cifo['ccontact'])[0])['table'][5].'</a>';
?>
</div>
<div class="col-sm-2 td"><?php echo $rows['category']; ?></div>
<div class="col-sm-2 td"><?php echo $rows['followupnote']; ?></div>
<div class="col-sm-2 td"><?php 

$lastfollowup=$rows['followuptime'];
$followUpStatusFlag='';

if($lastfollowup!=0){
  if($START_OF_THE_DAY<$lastfollowup && $END_OF_THE_DAY<=$lastfollowup){
      $followUpStatusFlag='#3F51B5';
  }

  if($START_OF_THE_DAY>$lastfollowup && $END_OF_THE_DAY>$lastfollowup){
      $followUpStatusFlag='#F44336';
  }
  
  if($START_OF_THE_DAY<=$lastfollowup && $END_OF_THE_DAY>$lastfollowup ){
      $followUpStatusFlag='#4CAF50';
  }
}else{
  $followUpStatusFlag='#ff9800';
}
 if($lastfollowup==0){ $lastfollowup='NO Followup'; }else{ $lastfollowup=date("d-m-Y",$lastfollowup/1000); }
echo '<p style="background:'.$followUpStatusFlag.';color:#fff;">'.$lastfollowup.'</p>'; 

?></div>

<div class="col-sm-2 td">
<button class="btn btn-primary btn-sm btn-block" data-toggle="modal" data-target="#myModal2" data-enquiryid="<?php echo $rows['enquiryid']; ?>" onclick="setidtomidal(this)">Followup</button>
<button class="btn btn-warning btn-sm btn-block" data-enquiryid="<?php echo $rows['enquiryid']; ?>" onclick="Offer(this)">Offer</button>
<button class="btn btn-success btn-sm btn-block" data-enquiryid="<?php echo $rows['enquiryid']; ?>" onclick="confirmenquiry(this)">Order Confirm</button>
<button class="btn btn-sm btn-block btn-danger" data-enquiryid="<?php echo $rows['enquiryid']; ?>" onclick="cancel(this)">Order Close</button>

</div>

</div>
<?php
}
?>
</div>
</div>
</div>

<button class="btn" style="font-size:25px;border-radius:50px;width:60px;height:60px;position: fixed;
    bottom: 40px;
    right: 40px;background:#eb2f06;color:#fff;" data-toggle="modal" data-target="#myModal">+</button>
<!-- Trigger the modal with a button -->


<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" align="center">Select Type Of Enquiry</h4>
      </div>
      <div class="modal-body">
          <div class="row">
       <div class="col-sm-6"><a style="border-radius:0px;" href="/process/create-master/customer-master/index.php?enquiry=new" class="btn btn-block btn-primary">New Customer Enquiry</a></div>
       <div class="col-sm-6"><a style="border-radius:0px;" onclick="closemodal('myModal')" data-toggle="modal" data-target="#myModal1" class="btn btn-block btn-primary">Old Customer Enquiry</a></div>
        </div>
      </div>
    </div>
  </div>
</div>


<div id="myModal1" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" align="center">New Enquiry</h4>
      </div>
      <div class="modal-body">
          <div class="form-group row">
       <div class="col-sm-4">Enquiry No.</div>
       <div class="col-sm-8"><input type="text" data-name="enquiryid" value="<?php echo $enquiryidnew; ?>" class="form-control input-sm" readonly></div>
       <div class="col-sm-4">Select Customer</div>
       <div class="col-sm-8">
       <select data-role="select" data-name='customerid'  id="customeridnew" class="form-control input-sm">
                <option value="Select">Select</option>
        </select>
       </div>
       <div class="col-sm-4" >Select Category</div>
       <div class="col-sm-8">
       <select data-role="select" data-name='category' class="form-control input-sm">
                <option value="Fumigation">Fumigation</option>
                <option value="Pest Control">Pest Control</option>
        </select>
       </div>
       <div class="col-sm-4">Set New Followup</div>
       <div class="col-sm-8"><input type="text" id="inp-enq-date" class="form-control input-sm">
       <input type="hidden" data-name="followuptime" id="inp-enq-date-stamp">
       </div>
       <div class="col-sm-4">Note</div>
       <div class="col-sm-8"><input type="text" data-name="followupnote" class="form-control input-sm"></div>
       <div class="col-sm-4"></div>
       <div class="col-sm-8"><button class="btn btn-success btn-block btn-sm" id="newqneuiry" onclick="newqneuiry()">Add New Enquery</button></div>
        </div>
        
      </div>
    </div>
  </div>
</div>

<div id="myModal2" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" align="center">New FollowUp</h4>
      </div>
      <div class="modal-body">
          <div class="form-group row">
       <div class="col-sm-4">Set New Followup</div>
       <input type="hidden" data-name="enquiryid" id="inp-enquiryidset">
       <div class="col-sm-8"><input type="text" id="inp-enq-olddate" class="form-control input-sm">
            <input type="hidden" id="inp-enq-olddate-stamp" data-name="followuptime">
       </div>
       <div class="col-sm-4">Note</div>
       <div class="col-sm-8"><input type="text" data-name="followupnote" class="form-control input-sm"></div>
       <div class="col-sm-4"></div>
       <div class="col-sm-8"><button class="btn btn-success btn-block btn-sm" onclick="setqneuiry()">Add New FollowUp</button></div>
        </div>
        
      </div>
    </div>
  </div>
</div>

<?php
    include($base.'_in/footer.php');
?>
<script>
var json1 = JSON.parse($("#item-json1").val());
var str='';
console.log(json1);

for (var i in json1) {  
        str += '<option value="' + json1[i].customerid  + '">' + json1[i].customername  + '</option>';
}
$('#customerid').append(str);

function setidtomidal(e){
var enquiryid=$(e).data('enquiryid');
$('#inp-enquiryidset').val(enquiryid);
}

$('#customeridnew').append(str);
  function closemodal(e){
    $('#'+e).modal('toggle');
  }

  $("#inp-enq-date").pickadate({
        selectYears:true,
        selectMonths:true,
        min:true,
        onClose: function(){
            try{
                var dateStamp = this.get('select')['pick'];
                $("#inp-enq-date-stamp").val(dateStamp);
            }catch(err){

            }
        }
    });

    $("#inp-enq-olddate").pickadate({
        selectYears:true,
        selectMonths:true,
        min:true,
        onClose: function(){
            try{
                var dateStamp = this.get('select')['pick'];
                $("#inp-enq-olddate-stamp").val(dateStamp);
            }catch(err){

            }
        }
    });

function newqneuiry() {
  $('#newqneuiry').hide();
  var valid = true;
  if (checker('myModal1') != false) {
    valid = valid * true;
  } else {
    valid = valid * false;
  }
  if (valid) {
    var data=checker('myModal1');
    var datastr = JSON.stringify(data);
    $.ajax({
        type: "POST",
        data: {
          data: datastr
        },
        url: 'api/newqneuiry.php',
        cache: false,
        success: function (res) {
          if (res.status == 'success') {
         
              window.location.reload();
           
          }
        }
      })
    }
  }

  function setqneuiry(){
    var valid = true;
  if (checker('myModal2') != false) {
    valid = valid * true;
  } else {
    valid = valid * false;
  }
  if (valid) {
    var data=checker('myModal2');
    var datastr = JSON.stringify(data);
    $.ajax({
        type: "POST",
        data: {
          data: datastr
        },
        url: 'api/setqneuiry.php',
        cache: false,
        success: function (res) {
          if (res.status == 'success') {
             window.location.reload();
          }
        }
      })
    }
  }


function confirmenquiry(e){
  var enquiryid=$(e).data('enquiryid');
  $.ajax({
        type: "POST",
        data: 'enquiryid='+enquiryid,
        url: 'api/confirmenquiry.php',
        cache: false,
        success: function (res) {
          if (res.status == 'success') {
             window.location.reload();
          }
        }
    })
}

function reset(e){
  var enquiryid=$(e).data('enquiryid');
  $.ajax({
        type: "POST",
        data: 'enquiryid='+enquiryid,
        url: 'api/reset.php',
        cache: false,
        success: function (res) {
          if (res.status == 'success') {
             window.location.reload();
          }
        }
      })
}

  </script>