<?php

    $base = '../../../';
    
    $js   = '<script src="'.$base.'js/fwork.js"></script>
            <script src="'.$base.'js/alart.js"></script>
            <script src="'.$base.'/js/select2.min.js"></script>
            <script src="'.$base.'js/list.min.js"></script>';
    
    $css  = '<link rel="stylesheet" href="'.$base.'css/fwork.css">
            <link rel="stylesheet" href="'.$base.'css/grid.min.css">
            <link href="'.$base.'/css/select2.min.css" rel="stylesheet" />
            <link rel="stylesheet" href="'.$base.'css/alart.css">';

    include($base.'_in/header.php');
    
    include($base.'_in/connect.php');

    if (!in_array("Commission Master Agent", $arrysession)) {
    
        echo "<script>window.location.href='/process/dashboard.php';</script>";
    
        exit;

    }

    if (session_status()==PHP_SESSION_NONE) { session_start(); }

    $branchid   = $_SESSION['branchid'];

    $con = _connect();

?>

<style>

    hr.style-hr {
        border: 0;
        height: 1px;
        background-image: linear-gradient(to right, rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.75), rgba(0, 0, 0, 0));
    }

    .table-list td,
    .table-list th {
        border: 1px solid #ddd;
        padding: 2px;
        font-size: 13px;
    }

    .table-list tr:nth-child(even) {
        background-color: #f2f2f2;
    }

    .table-list th {
        padding-top: 12px;
        padding-bottom: 12px;
        text-align: left;
        background-color: #16a085;
        color: white;
    }

    .sidenav1 {
        height: 100%;
        width: 0;
        position: fixed;
        z-index: 1;
        top: 0;
        left: 0;
        background-color: #111;
        overflow-x: hidden;
        transition: 0.5s;
        padding-top: 60px;
    }

    .sidenav1 a {
        padding: 8px 8px 8px 32px;
        text-decoration: none;
        font-size: 25px;
        color: #818181;
        display: block;
        transition: 0.3s;
    }

    .sidenav1 a:hover {
        color: #f1f1f1;
    }

    .sidenav1 .closebtn1 {
        position: absolute;

        right: 25px;
        font-size: 36px;
        margin-left: 50px;
    }

    @media screen and (max-height: 450px) {
        .sidenav1 {
            padding-top: 15px;
        }

        .sidenav1 a {
            font-size: 18px;
        }
    }

    .list {
        padding: 0px;
    }

    .list li span {
        display: inline;
    }

    .list li {
        text-decoration: none;
        display: block;
        padding: 2px;
        background: #FFF;
        border: 1px solid #333;
        color: #000;
    }

</style>

<div class="container-fluid">
    
    <div class=" content" id="section">

        <span style="font-size:24px;cursor:pointer" onclick="openNav1()">&#9776; Search</span>
       
        <div id="mysidenav1" class="sidenav1">

            <div id="test-list" style="padding:5px;">
                
                <a href="javascript:void(0)" class="closebtn1" onclick="closeNav1()">&times;</a>
             
                <br><br><br>
             
                <div class="form-group">
             
                    <input type="text" placeholder="Search" class="form-control input-sm fuzzy-search" style="border-radius:3px;">
                
                </div>
                
                <ul class="list">
                    
                    <?php 

                        $result1=mysqli_query($con, "SELECT * FROM `commissionmaster` WHERE branchid = '$branchid' ORDER BY agentname");
                        
                        while($rows=mysqli_fetch_assoc($result1)) {
                           
                            echo '<li><span class="agentname">' .$rows['agentname']. '<span><button style="margin-left:2px;" data-id="' .$rows['id']. '" onclick="edit(this)" class="btn btn-primary btn-sm"><span class="fa fa-edit"></span> E</button></i>';
                        
                        }
                    ?>

                </ul>

            </div>

        </div>

        <h2 align="center" style="margin-top:0px;">Commission Master</h2>

        <div style="clear:both;"></div>
        
        <hr class="style-hr">

        <div class="col-sm-6">
            
            <center> Agent Details</center>
            
            <hr class="style-hr">

            <div class="col-sm-5">Agent Name</div>
            
            <div class="col-sm-7">
                
                <div class="form-group">
                    
                    <input type="text" data-role='text' data-name='agentname' class="form-control input-sm">
                    
                    <input type="hidden" data-role="text" data-name = 'branchid' value ="<?php echo $branchid; ?>" id='branchid'>
                    
                    <input type="hidden" data-name = 'agentid'  id='agentid'>

                </div>

            </div>
            
            <div class="col-sm-5">Address</div>
            
            <div class="col-sm-7">

                <div class="form-group">
                    
                    <textarea data-role='textarea' data-name='agent_address' class="form-control input-sm"> </textarea>
                
                </div>

            </div>

            <div class="col-sm-5">Contact No.</div>
            
            <div class="col-sm-7">

                <div class="form-group">
                    
                    <input type="number" data-role='number' data-name='contactno' id="inp-phone" class="form-control input-sm">
                
                </div>
            
            </div>

            <div class="col-sm-5">Email id</div>

            <div class="col-sm-7">

                <div class="form-group">
                    
                    <input type="text" data-role='text' data-name='email' class="form-control input-sm">
                
                </div>
            
            </div>

            <div class="col-sm-5">Country</div>

            <div class="col-sm-7">

                <div class="form-group">

                    <select class="form-control input-sm" data-role="select" data-name="country">
                        
                        <option value="IN">India</option>
                        
                        <?php 
                        
                            $result1=mysqli_query($con, "SELECT * FROM `countrymaster`");
                                
                            while($rows=mysqli_fetch_assoc($result1)) {
                            
                                echo '<option value="'.$rows['countrycode'].'">'.$rows['countryname'].'</option>';
                            
                            }
                        
                        ?>

                    </select>

                </div>

            </div>

            <div class="col-sm-5">State</div>

            <div class="col-sm-7">

                <div class="form-group">

                    <select class="form-control input-sm" data-role="select" id="state" data-name="state">
                        
                        <option value="Select">Select</option>
                        
                        <?php 
                            
                            $result1 = mysqli_query($con, "SELECT * FROM `statemaster`");
                            
                            while($rows=mysqli_fetch_assoc($result1)) {
                                
                                echo '<option value="'.$rows['statecode'].'">'.$rows['statename'].'</option>';
                            }
                        
                        ?>
                        
                        <option value="00">OTHER</option>
                    
                    </select>
                
                </div>
          
            </div>

            <div class="col-sm-5">Category</div>

            <div class="col-sm-7">

                <div class="form-group">

                    <select class="form-control input-sm" data-role="select" data-name="category" onchange="customerShow(this)">

                        <option value="Select">Select Category</option>

                        <option value="customer">Customer</option>
                        
                        <option value="cha">Cha</option>

                        <option value="ppq">PPQ</option>

                        <option value="other">Other</option>

                    </select>

                </div>

            </div>
           
            <div class="col-sm-5 customerClass" style="display:none;">Customer Name</div>

            <div class="col-sm-7 customerClass" style="display:none;">

                <select class="form-control input-sm select-js1" id="custName" data-role="" data-name="custName">

                    <option value="Select">Select Customername</option>

                    <?php

                        $account = mysqli_query($con,"SELECT customerid,customername,city FROM customermaster WHERE branchid='$branchid' ORDER BY customername");

                            while ($row = mysqli_fetch_assoc($account)) {

                                echo '<option value="'.$row['customerid'].'">'.$row['customername'].'</option>';

                            }
                                    
                    ?>

                </select>

            </div>
           
        </div>
       
        <div class="col-sm-6">
                
            <center>Bank Details</center>
            
            <hr class="style-hr">

            <div class="col-sm-5">Bank Name</div>
                
            <div class="col-sm-7">
            
                <div class="form-group">

                    <input type="text" data-role='text' data-name='bankname' class="form-control input-sm">
                
                </div>
            
            </div>

            <div class="col-sm-5">Branch Name</div>
                
            <div class="col-sm-7">
                
                <div class="form-group">
                    
                    <input type="text" data-role='text' data-name='bankbranchname' class="form-control input-sm">
                
                </div>
            
            </div>
            
            <div class="col-sm-5">Branch Address</div>
            
            <div class="col-sm-7">
                
                <div class="form-group">
                    
                    <textarea data-role='textarea' data-name='bankaddress' class="form-control input-sm"> </textarea>
                
                </div>
            
            </div>
            
            <div class="col-sm-5">IFSC Code</div>
            
            <div class="col-sm-7">
                
                <div class="form-group">
                    
                    <input type="text" data-role='text' data-name='ifsccode' class="form-control input-sm">
                
                </div>
            
            </div>
            
            <div class="col-sm-5">Account Number</div>
            
            <div class="col-sm-7">
                
                <div class="form-group">
                    
                    <input type="text" data-role='text' data-name='accountno' class="form-control input-sm">
                
                </div>
            
            </div>
                
        </div>


        <div class="col-sm-12">
                
            <div style="clear:both;"></div>
            
            <hr class="style-hr">

            <div class="col-sm-3"> </div>
                
            <div class="col-sm-2">
                
                <button class="btn btn-success btn-sm btn-block" id="btn-submit" onclick="submit()" style="margin:5px;">Submit</button>
            
            </div>
            
            <div class="col-sm-2">
                
                <button class="btn btn-danger btn-sm btn-block" id="btn-reset" style="margin:5px;">Reset</button>
            
            </div>

            <div class="col-sm-2">
                
                <a href="/process/commission-master/commssion-management/index.php" class="btn btn-info btn-sm btn-block" id="btn-reset" style="margin:5px;">Back</a>
            
            </div>
               
            <div class="col-sm-3"></div>
        
        </div>

    </div>

</div>

<?php
    
    include($base.'_in/footer.php');

?>

<script>

function openNav1() {

    document.getElementById("mysidenav1").style.width = "300px";

}

function closeNav1() {

    document.getElementById("mysidenav1").style.width = "0";

}

var monkeyList = new List('test-list', {

    valueNames: ['agentname']

});


$(document).ready(function () {

    
    $('.select-js1').select2({width: '100%', tags: true});

});

function customerShow(e) {

    let customerDisplay = $(e).val();

    console.log(customerDisplay);

    if (customerDisplay == 'customer' || customerDisplay == 'cha') {

        $('#custName').attr('data-role','select');

        $('.customerClass').show();
        
    } else {

        $('#custName').attr('data-role','');

        $('.customerClass').hide();

    }
    
}

function submit() {

    var valid = true;

    if (checker('section') != false) {

        valid = valid * true;

    } else {

        valid = valid * false;

    }

    if (valid) {

        var data1 = checker('section');
           
            data1 = JSON.stringify(data1);

        $.ajax({
            
            type: "POST",
            
            data: {

                data: data1,

            },

            url: 'insert.php',
            
            cache: false,
            
            success: function (res) {
            
                if (res.status == 'success') {
            
                    swal({
                        
                        type: 'success',
                       
                        title: 'Commission Agent Add',
                       
                        showConfirmButton: false,
                       
                        timer: 3000
                    
                    });
                   
                    setTimeout(function () {
                        
                        window.location.reload();
                    
                    }, 3000);
                
                }

                if (res.status == 'falid1') {

                    swal('conatct No Or Email Id Duplicate', '', 'error');
                   
                    $('#inp-phone').css('border-bottom', '1px solid red');

                }

            }
        
        });
    
    }

}


function edit(e) {

    let agentid = $(e).attr('data-id');

    $('#agentid').val(agentid);

    $.ajax({

        type: "POST",
        
        data: 'agentid=' + agentid,
        
        url: 'select.php',
        
        cache: false,
        
        success: function (res) {
        
            if (res.status == 'success') {

                $('#btn-submit').attr('onclick', 'update()');
                
                $('#btn-submit').text('Update');

                modals.putvalue('section', res.json);
             
                closeNav1();
            
            }
        
        }
    
    });

}

function update() {

    var valid = true;

    if (checker('section') != false) {

        valid = valid * true;

    } else {

        valid = valid * false;

    }

    if (valid) {

        var data1 = checker('section');
        
            data1 = JSON.stringify(data1);

        $.ajax({
            type: "POST",
            
            data: {

                data: data1,

            },

            url: 'update.php',
            
            cache: false,
            
            success: function (res) {
            
                if (res.status == 'success') {
            
                    swal({
                        
                        type: 'success',
                    
                        title: 'Commission Agent update',
                    
                        showConfirmButton: false,
                    
                        timer: 3000
                    
                    });
                
                    setTimeout(function () {
                        
                        window.location.reload();
                    
                    }, 3000);
                
                }
//http://pestmorten.local/process/commission-master/commssion-management/index.php
                if (res.status == 'falid1') {

                    swal('conatct No Or Email Id Duplicate', '', 'error');
                
                    $('#inp-phone').css('border-bottom', '1px solid red');

                }

            }
        
        });

    }

}

$('#btn-reset').on('click', function () {
    window.location.reload();
});


</script>