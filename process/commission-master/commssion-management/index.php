<?php 

  $base = '../../../';

  $navenq1 = 'background:#1B1464;';
  
  include('header.php');

  if (!in_array("Commission Request", $arrysession)) {
   
      echo "<script>window.location.href='/process/dashboard.php';</script>";
     
      exit;

  }

  if (session_status() == PHP_SESSION_NONE) { session_start(); }
  
  $sessionby = $_SESSION['employeeid'];
  
  $branchid  = $_SESSION['branchid'];

?>
<br>
<style>

.th {
        background: #8e44ad;
        color: #fff;
        text-align: center;
        padding-top: 2px;
        padding-bottom: 2px;
        border: 1px solid #fff;
    }

    .td {
        border: 1px solid #ddd;
    }

    .table-ui .btn {
        margin: 3px;
    }

    #myModal1 .col-sm-4,
    .col-sm-8 {
        margin-bottom: 2px;
    }

    .picker__select--year {
        height: auto;
    }

    .picker__select--month {
        height: auto;
    }

    .table-list td,
    .table-list th {
        border: 1px solid #ddd;
        padding: 1px !important;
        font-size: 13px;
    }

    .table-list td {
        padding-top: 10px !important;
    }

    .table-list tr:nth-child(even) {
        background-color: #f2f2f2;
    }

    .table-list th {
        padding-top: 5px;
        padding-bottom: 5px;
        text-align: center;
        background-color: #16a085;
        color: white;
    }

</style>

<img class="hidden" id=imageid src="../../../../img/PestMortemLogo.jpg">

<div class="container-fluid">


    <br>

    <div class="table-ui container-fluid">

      <div class="tr row">

        <div class="col-sm-2 th">Customer Name</div>

        <div class="col-sm-4 th">Service Details</div>

        <div class="col-sm-4 th">Agent Details</div>

        <div class="col-sm-2 th">Action</div>

      </div>

      <?php
        
        $result=mysqli_query($con,"SELECT DISTINCT commissionid,customerid FROM `commisionrequest` WHERE branchid='$branchid' AND send_to_approval_time = 0 ORDER BY id DESC");
          
        while($rows = mysqli_fetch_assoc($result)){

          $commissionid = $rows['commissionid'];

          $customerid   = $rows['customerid'];

          $customername = mysqli_fetch_assoc(mysqli_query($con,"SELECT customername x FROM customermaster WHERE customerid = '$customerid' "))['x'];

      ?>

      <div class="row tr">
          
        <div class="col-sm-2 td">

          <?php 
            
            echo $customername;
            
          ?>

        </div>

        <div class="col-sm-4 td" style="word-wrap:break-word;">

          <?php
          
            echo '<table class="table table-list form-group">';
            
              echo '<thead>';
                  
                echo '<tr>';
                      
                  echo '<th width="300">Service Name</th>';
                                    
                  echo '<th width="80">	Service Rate</th>';
                                                                    
                echo '</tr>';
              
              echo '</thead>';
              
              echo '<tbody>';

              $total = 0;

              $result1 = mysqli_query($con,"SELECT * FROM comssionservice WHERE commissionid='$commissionid' AND branchid='$branchid' AND customerid = '$customerid'  ");

              while($rowss=mysqli_fetch_assoc($result1)){

                $serviceid   = $rowss['serviceid'];
               
                $servicename = mysqli_fetch_assoc(mysqli_query($con,"SELECT servicename x FROM servicemaster WHERE serviceid = '$serviceid' "))['x'];

                echo '<tr>';
                                
                  echo '<td>'.$servicename.'</td>';

                  echo '<td>'.$rowss['servicerate'].'</td>';

                echo '</tr>';
              
              }

              echo '</tbody>';

            echo '</table>';

          ?>

        </div>

        <div class="col-sm-4 td" style="word-wrap:break-word;">

          <?php
          
            echo '<table class="table table-list form-group">';
            
              echo '<thead>';
                  
                echo '<tr>';
                      
                  echo '<th width="300">Service Name</th>';

                  echo '<th width="300">Agent Name</th>';
                  
                  echo '<th>Type</th>';
                                    
                  echo '<th>Commision Amt</th>';
                                                  
                echo '</tr>';
              
              echo '</thead>';
              
              echo '<tbody>';

              $total = 0;

              $result1 = mysqli_query($con,"SELECT * FROM commisionrequest WHERE commissionid='$commissionid' AND branchid='$branchid' AND customerid = '$customerid'  ");

              while($rowsss=mysqli_fetch_assoc($result1)){

                $serviceid   = $rowsss['serviceid'];
               
                $servicename = mysqli_fetch_assoc(mysqli_query($con,"SELECT servicename x FROM servicemaster WHERE serviceid = '$serviceid' "))['x'];

                echo '<tr>';
                              
                  echo '<td>'.$servicename.'</td>';

                  echo '<td>'.$rowsss['agentname'].'</td>';

                  echo '<td>'.$rowsss['agenttype'].'</td>';

                  echo '<td>'.$rowsss['commissionamount'].'</td>';

                  $total+=$rowsss['commissionamount'];

                echo '</tr>';
              
              }

              echo '</tbody>';

            echo '</table>';

            echo 'Total Commission Amount : '.$total;


          ?>

        </div>

        <div class="col-sm-2 td">

          <button class="btn btn-sm btn-block btn-danger" data-commissionid="<?php echo $commissionid; ?>"  onclick="closes(this)">Send To Approval</button>

          <a href="commission.php?commissionid=<?php echo $commissionid;?>&edit=true" class="btn btn-info btn-sm btn-block">Edit</a>
          
        </div>
        
      </div>

      <?php } ?>

    </div>

  <!-- </div> -->
    
</div>


<a class="btn" id="commissionMaster" style="font-size:20px;border-radius:50px;width:60px;height:60px;position: fixed;
    bottom: 200px;
    right: 40px;background:#eb4d4b;color:#fff; display:none;" data-toggle="tooltip" title="Commssion Agent Master" href="/process/commission-master/commssion/index.php">
    
    <i class="fa fa-user" aria-hidden="true" style="padding-top:12px;"></i>
</a>

<a class="btn" id="commissionRequest" style="font-size:20px;border-radius:50px;width:60px;height:60px;position: fixed;
    bottom: 120px;
    right: 40px;background:#ffa801;color:#fff; display:none;" data-toggle="tooltip" title="Commssion Request" href="/process/commission-master/commssion-management/commission.php" >
    
    <i class="fa fa-money"  aria-hidden="true" style="padding-top:12px;"></i>
</a>

<a class="btn show" style="font-size:20px;border-radius:50px;width:60px;height:60px;position: fixed;
    bottom: 40px;
    right: 40px;background:#eb2f06;color:#fff;" data-toggle="tooltip" title="Show"  href="javascript:void(0);" onclick ="openIcon(this)">
    
    <i class="fa fa-plus" id="plusSign" aria-hidden="true" style="padding-top:12px;"></i>

</a>

<?php 

  include($base.'_in/footer.php');

?>

<script>

  function openIcon(x) {

    $(x).find('i').toggleClass('fa-plus fa-times');

    let showIcon = x.classList.toggle("hide");

    if (showIcon == true) {

      $(x).attr('title', 'Close') .tooltip('fixTitle').tooltip('show');

      $("#commissionMaster").show(1000);

      $("#commissionRequest").show(1000);

    } else {

      $(x).attr('title', 'Show') .tooltip('fixTitle').tooltip('show');
      
      $("#commissionMaster").hide(1000);

      $("#commissionRequest").hide(1000);

    }

  }

function closes(e) {

  let commissionid = $(e).attr('data-commissionid');;

  if (commissionid != '') {

    $.ajax({

      type: "POST",

      data: "commissionid=" + commissionid ,

      url: 'api/commissionSendToApproval.php',

      cache: false,

      success: function (res) {

        if (res.status == 'success') {

          swal({
              
            type: 'success',
            
            title: 'Commission Send To Approval',
            
            showConfirmButton: false,
            
            timer: 3000
          
          });
          
          setTimeout(function () {
              
            location.href = 'index.php';
          
          }, 3000);

        }

      }

    })

  } else {

    alert('Somthing Went Wrong');

  }

}

$(document).ready(function(){

  $('[data-toggle="tooltip"]').tooltip();   

});


</script>