<!DOCTYPE html>

<html>

    <head>
        
        <meta charset="UTF-8" />
    
        <title>modal_search</title>
        
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        
        <style>

            .highlight {
                
                background: red;
                
                padding: 1px;
                
                border: #00CC00 dotted 1px;
            }

        </style>
  
    </head>

    <body id="textsearch_p">
        
        <input class="search_text" id='p'>
        
        <input type="button" value="search" onClick="serachText();" />

        <div>
            
            <p>
                
                The ACM Digital Library, a part of the ACM Portal, contains a comprehensive archive of the organization's journals, magazines, and conference proceedings. Online services include a forum called Ubiquity and Tech News digest.
    
                ACM requires the copyright of all submissions to be assigned to the organization as a condition of publishing the work.[2] Authors may post the documents on their own websites, but they are required to link back to the digital library's reference page for the paper. Though authors are not allowed to charge for access to copies of their work, downloading a copy from the ACM site requires a paid subscription.
            
            </p>

            <p>
                
                The ACM Digital Library, a part of the ACM Portal, contains a comprehensive archive of the organization's journals, magazines, and conference proceedings. Online services include a forum called Ubiquity and Tech News digest.
    
                ACM requires the copyright of all submissions to be assigned to the organization as a condition of publishing the work.[2] Authors may post the documents on their own websites, but they are required to link back to the digital library's reference page for the paper. Though authors are not allowed to charge for access to copies of their work, downloading a copy from the ACM site requires a paid subscription.
            
            </p>

        </div>

        <div>
            
            <p>
                
                The ACM Digital Library, a part of the ACM Portal, contains a comprehensive archive of the organization's journals, magazines, and conference proceedings. Online services include a forum called Ubiquity and Tech News digest.
    
                ACM requires the copyright of all submissions to be assigned to the organization as a condition of publishing the work.[2] Authors may post the documents on their own websites, but they are required to link back to the digital library's reference page for the paper. Though authors are not allowed to charge for access to copies of their work, downloading a copy from the ACM site requires a paid subscription.
            
            </p>

            <p>
                
                The ACM Digital Library, a part of the ACM Portal, contains a comprehensive archive of the organization's journals, magazines, and conference proceedings. Online services include a forum called Ubiquity and Tech News digest.
    
                ACM requires the copyright of all submissions to be assigned to the organization as a condition of publishing the work.[2] Authors may post the documents on their own websites, but they are required to link back to the digital library's reference page for the paper. Though authors are not allowed to charge for access to copies of their work, downloading a copy from the ACM site requires a paid subscription.
            
            </p>

        </div>

    </body>

</html>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    
<script>

function serachText() {

    $("body").find(".highlight").removeClass("highlight");

    $(".search_text").each(function () {

        var textModal = $('#textsearch_' + this.id),

                 html = textModal.html(),

                 reg  = new RegExp($(this).val() || "&fakeEntity;", 'gi');

        textModal.html(html.replace(reg, function (str, index) {

            var t        = html.slice(0, index + 1),
                
                lastLt   = t.lastIndexOf("<"),
                
                lastGt   = t.lastIndexOf(">"),
                
                lastAmp  = t.lastIndexOf("&"),
                
                lastSemi = t.lastIndexOf(";");
            
            if (lastLt > lastGt) return str; // inside a tag
            
            if (lastAmp > lastSemi) return str; // inside an entity
            
            return "<span class='highlight'>" + str + "</span>";
                    
                         
        
        }));

    })

}


    </script>