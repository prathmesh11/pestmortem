<?php

    $base='../../../';

    include($base.'_in/connect.php');

    header('content-type: application/json; charset=utf-8');

    header("access-control-allow-origin: *");

    if(isset($_POST['data']) && isset($_POST['serviceDetails']) && isset($_POST['agentDetails'])){

        $con  = _connect();

        $data = get_object_vars(json_decode($_POST["data"]));

        if (session_status()==PHP_SESSION_NONE) { session_start(); }

        $update_by             = $_SESSION['employeeid'];

        $branchid              = $data['branchid'];

        $customerid            = $data["customerid"];

        $category              = $data["category"];

        $commissionid          = $data["commissionid"];

        $creatDetails          = mysqli_fetch_assoc(mysqli_query($con,"SELECT created_by,created_time,send_to_approval_by,send_to_approval_time FROM commisionrequest WHERE commissionid='$commissionid'"));

        $created_by            = $creatDetails['created_by'];

        $created_time          = $creatDetails['created_time'];

        $send_to_approval_by   = $creatDetails['send_to_approval_by'];

        $send_to_approval_time = $creatDetails['send_to_approval_time'];

        $deleteService         = mysqli_query($con,"DELETE FROM comssionservice WHERE commissionid = '$commissionid' ");

        $serviceDetails        = json_decode($_POST["serviceDetails"]);

        foreach ($serviceDetails as $i) {

            $srno         = get_object_vars($i)['srno'];
            
            $serviceid    = get_object_vars($i)['serviceid'];
            
            $servicename  = get_object_vars($i)['servicename'];
                        
            $servicerate  = get_object_vars($i)['servicerate'];
                        
            mysqli_query($con,"INSERT INTO comssionservice (branchid, commissionid, customerid, category, sr_no, serviceid, servicerate, created_by, created_time, update_by, update_time) VALUES ('$branchid', '$commissionid', '$customerid', '$category', '$srno', '$serviceid',  '$servicerate', '$created_by','$created_time', '$update_by','$CURRENT_MILLIS')");

        }

        $deleteagent  = mysqli_query($con,"DELETE FROM commisionrequest WHERE commissionid = '$commissionid' ");

        $agentDetails = json_decode($_POST["agentDetails"]);

        foreach ($agentDetails as $i) {

            $serviceid        = get_object_vars($i)['serviceid'];

            $srno             = get_object_vars($i)['srno'];
            
            $agentid          = get_object_vars($i)['agentid'];

            $agentname        = get_object_vars($i)['agentname'];
            
            $agenttype        = get_object_vars($i)['agenttype'];
            
            $contactno        = get_object_vars($i)['contactno'];
            
            $paidtype         = get_object_vars($i)['paidtype'];

            $bankname         = get_object_vars($i)['bankname'];
            
            $bankaddress      = get_object_vars($i)['bankaddress'];
            
            $ifsccode         = get_object_vars($i)['ifsccode'];
            
            $accountno        = get_object_vars($i)['accountno'];
            
            $commissionamount = get_object_vars($i)['commissionamount'];

            if ($serviceid != '') {

                $create = mysqli_query($con,"INSERT INTO commisionrequest (branchid, commissionid, customerid, category, serviceid, agentid, agentname, agenttype, contactno, paidtype, bankname, bankaddress, ifsccode, accountno, commissionamount, created_by, created_time, update_by, update_time,send_to_approval_by,send_to_approval_time) VALUES ('$branchid', '$commissionid', '$customerid', '$category','$serviceid', '$agentid', '$agentname', '$agenttype', '$contactno', '$paidtype', '$bankname', '$bankaddress', '$ifsccode', '$accountno', '$commissionamount','$created_by','$created_time', '$update_by','$CURRENT_MILLIS','$send_to_approval_by','$send_to_approval_time')");
            
            }

        }


        if($create){

            echo '{"status":"success"}';


        } else {

            echo '{"status":"falid2"}';

        }

        _close($con);

    } else {

        echo '{"status":"falid"}';

    }

?>