<?php 

  $base = '../../../';

  $navenq4 = 'background:#1B1464;';
  
  include('header.php');

  if (!in_array("Bill Paid", $arrysession)) {
   
    echo "<script>window.location.href='/process/dashboard.php';</script>";
    
    exit;

  }

  if (session_status() == PHP_SESSION_NONE) { session_start(); }
  
  $sessionby = $_SESSION['employeeid'];
  
  $branchid  = $_SESSION['branchid'];

?>

<br>
<style>

.th {
        background: #130f40;
        color: #fff;
        text-align: center;
        padding-top: 2px;
        padding-bottom: 2px;
        border: 1px solid #fff;
    }

    .td {
        border: 1px solid #ddd;
    }

    .table-ui .btn {
        margin: 3px;
    }

    #myModal1 .col-sm-4,
    .col-sm-8 {
        margin-bottom: 2px;
    }

    .picker__select--year {
        height: auto;
    }

    .picker__select--month {
        height: auto;
    }

    .table-list td,
    .table-list th {
        border: 1px solid #ddd;
        padding: 1px !important;
        font-size: 13px;
    }

    .table-list td {
        padding-top: 10px !important;
    }

    .table-list tr:nth-child(even) {
        background-color: #f2f2f2;
    }

    .table-list th {
        padding-top: 5px;
        padding-bottom: 5px;
        text-align: center;
        background-color: #16a085;
        color: white;
    }

</style>

<img class="hidden" id=imageid src="../../../../img/PestMortemLogo.jpg">

<input type="hidden" id="inp-branchId" value="<?php echo $branchid;?>">

<div class="container-fluid">

  <div class="row">

    <div class="col-sm-4">

      <b>Select Agent Name </b>

      <div class="form-group">

        <select class="select-js form-control input-sm agentid" data-role="select" id="agentid">';

          <option value="Select">Select Agent Name</option>

          <?php
                                                
            $select = mysqli_query($con,"SELECT id,agentname FROM commissionmaster WHERE branchid ='$branchid'  ORDER BY agentname ");

            while($rows = mysqli_fetch_assoc($select)){
                                                                                                            
              echo '<option value='.$rows['id'].' >'.$rows['agentname'].'</option>"';
                                            
            } 
                                            
          ?>
                                

        </select>  

      </div>

    </div>

    <div class="col-sm-2">
      <br>         
      <button class="btn btn-success btn-sm btn-block" id="btn-agent" onclick="agentPaymentDetails()">Add</button>

    </div>

  </div>


  <div class="table-ui container-fluid">

    <div class="tr row">

      <div class="col-sm-3 th">Bill No</div>

      <div class="col-sm-3 th">Bill Amount</div>

      <div class="col-sm-2 th">Bill Status</div>

      <div class="col-sm-2 th">Commission</div>

      <div class="col-sm-2 th">Commission Status</div>

    </div>

  </div>
    
</div>



<?php 

  include($base.'_in/footer.php');

?>

<script>


function agentPaymentDetails(){

    let valid = true;

    let agentid = $('#agentid').val();

  if (agentid == 'Select') {

    alert('Pls Select Agent');

    valid = valid * false;

  } else {

    $('#agentid').css('border', '2px solid green');

    valid = valid * true;

  }

  if (valid) {

    $.ajax({

      type: "POST",
            
      data: 'branchid='+$('#inp-branchId').val() + '&agentid=' + agentid,

      url: 'api/fetchagentpaymentdetails.php',
            
      cache: false,
            
      success: function (res) {
            
        if (res.status == 'success') {
            
          let json = res.json;

          let str = '';

            str+='<div class="tr row">';

              str+='<div class="col-sm-3 th">Bill No</div>';

              str+='<div class="col-sm-3 th">Bill Amount</div>';

              str+='<div class="col-sm-2 th">Bill Status</div>';

              str+='<div class="col-sm-2 th">Commission</div>';

              str+='<div class="col-sm-2 th">Commission Status</div>';

            str+='</div>';

          for (let i in json) {
            
            str+="<div class='tr row'>";

              str+='<div class="col-sm-3 td">';

                str+= json[i].billno;

              str+='</div>';

              str+='<div class="col-sm-3 td">';

                  str+= json[i].billamount;

              str+='</div>';

              str+='<div class="col-sm-2 td">';

                  str+= json[i].billStatus;

              str+='</div>';

              str+='<div class="col-sm-2 td">';

                  str+= json[i].commissionamount;

              str+='</div>';

              str+='<div class="col-sm-2 td">';

                str+= json[i].paidStatus;

              str+='</div>';

            str+="</div>";
            
          }

          str+='<div class="tr row">';

            str+='<div class="col-sm-8 td">Outstanding</div>';

            str+='<div class="col-sm-4 td">'+res.outStanding+'</div>';

          str+='</div>';

           str+='<div class="tr row">';

            str+='<div class="col-sm-8 td">Due</div>';

            str+='<div class="col-sm-4 td">'+res.due+'</div>';

           str+='</div>';

          $('.table-ui').html(str);
                
        }

      }
        
    });
    
  }

}



$(document).ready(function() {

  $('.select-js').select2({width: '100%', tags: true});

  $('.select').attr('style','width:100%!important;');

});

</script>