<?php 

  $base = '../../../';

  $navenq3 = 'background:#1B1464;';
  
  include('header.php');

  if (!in_array("Bill Paid", $arrysession)) {
   
    echo "<script>window.location.href='/process/dashboard.php';</script>";
    
    exit;

  }

  if (session_status() == PHP_SESSION_NONE) { session_start(); }
  
  $sessionby = $_SESSION['employeeid'];
  
  $branchid  = $_SESSION['branchid'];

?>
<br>
<style>

.th {
        background: #8e44ad;
        color: #fff;
        text-align: center;
        padding-top: 2px;
        padding-bottom: 2px;
        border: 1px solid #fff;
    }

    .td {
        border: 1px solid #ddd;
    }

    .table-ui .btn {
        margin: 3px;
    }

    #myModal1 .col-sm-4,
    .col-sm-8 {
        margin-bottom: 2px;
    }

    .picker__select--year {
        height: auto;
    }

    .picker__select--month {
        height: auto;
    }

    .table-list td,
    .table-list th {
        border: 1px solid #ddd;
        padding: 1px !important;
        font-size: 13px;
    }

    .table-list td {
        padding-top: 10px !important;
    }

    .table-list tr:nth-child(even) {
        background-color: #f2f2f2;
    }

    .table-list th {
        padding-top: 5px;
        padding-bottom: 5px;
        text-align: center;
        background-color: #16a085;
        color: white;
    }

</style>

<img class="hidden" id=imageid src="../../../../img/PestMortemLogo.jpg">

<div class="container-fluid">


    <br>

    <div class="table-ui container-fluid">

      <div class="tr row">

        <div class="col-sm-2 th">Customer Name</div>

        <div class="col-sm-4 th">Service Details</div>

        <div class="col-sm-4 th">Agent Details</div>

        <div class="col-sm-2 th">Action</div>

      </div>

      <?php
        
        $result=mysqli_query($con,"SELECT DISTINCT commissionid,customerid,check_time FROM `commisionrequest` WHERE branchid='$branchid' AND send_to_approval_time <> 0 AND approve_time <> 0 AND paid_time = 0 AND bill_status_confrim_time = 0 ORDER BY id DESC");
          
        while($rows = mysqli_fetch_assoc($result)){

          $commissionid = $rows['commissionid'];

          $customerid   = $rows['customerid'];

          $customername = mysqli_fetch_assoc(mysqli_query($con,"SELECT customername x FROM customermaster WHERE customerid = '$customerid' "))['x'];

          $paidid       = mysqli_fetch_assoc(mysqli_query($con,"SELECT paidid x FROM comssionbilldetails WHERE commissionid = '$commissionid' "))['x'];

          $countId      = mysqli_fetch_assoc(mysqli_query($con,"SELECT COUNT(id) x FROM link_billno WHERE commissionid = '$commissionid' "))['x'];

          $serviceid    = mysqli_fetch_assoc(mysqli_query($con,"SELECT serviceid x FROM commisionrequest WHERE commissionid = '$commissionid' "))['x'];

          $category     = mysqli_fetch_assoc(mysqli_query($con,"SELECT category x FROM commisionrequest WHERE commissionid = '$commissionid' "))['x'];


      ?>

      <div class="row tr">
          
        <div class="col-sm-2 td">

          <?php 
            
            echo $customername;
            
          ?>

        </div>

        <div class="col-sm-4 td" style="word-wrap:break-word;">

          <?php
          
            echo '<table class="table table-list form-group">';
            
              echo '<thead>';
                  
                echo '<tr>';
                      
                  echo '<th width="300">Service Name</th>';
                                    
                  echo '<th width="80">	Service Rate</th>';

                  echo '<th width="80">	Status</th>';
                                                                    
                echo '</tr>';
              
              echo '</thead>';
              
              echo '<tbody>';

              $total = 0;

              $result1 = mysqli_query($con,"SELECT * FROM comssionservice WHERE commissionid='$commissionid' AND branchid='$branchid' AND customerid = '$customerid'  ");

              while($rowss=mysqli_fetch_assoc($result1)){

                $serviceid   = $rowss['serviceid'];

                $bill_status = $rowss['bill_status'];

                if ($bill_status != '') {
                  
                  $status = "Bill Created";

                } else {

                  $status = "";


                }

               
                $servicename = mysqli_fetch_assoc(mysqli_query($con,"SELECT servicename x FROM servicemaster WHERE serviceid = '$serviceid' "))['x'];

                echo '<tr>';
                                
                  echo '<td>'.$servicename.'</td>';

                  echo '<td>'.$rowss['servicerate'].'</td>';

                  echo '<td>'.$status.'</td>';

                echo '</tr>';
              
              }

              echo '</tbody>';

            echo '</table>';


          ?>

        </div>

        <div class="col-sm-4 td" style="word-wrap:break-word;">

          <?php
          
            echo '<table class="table table-list form-group">';
            
              echo '<thead>';
                  
                echo '<tr>';
                      
                  echo '<th width="300">Service Name</th>';

                  echo '<th width="300">Agent Name</th>';
                  
                  echo '<th>Type</th>';
                                    
                  echo '<th>Commision Amt</th>';
                                                  
                echo '</tr>';
              
              echo '</thead>';
              
              echo '<tbody>';

              $total = 0;

              $result1 = mysqli_query($con,"SELECT * FROM commisionrequest WHERE commissionid='$commissionid' AND branchid='$branchid' AND customerid = '$customerid'  ");

              while($rowsss=mysqli_fetch_assoc($result1)){

                $serviceid   = $rowsss['serviceid'];
               
                $servicename = mysqli_fetch_assoc(mysqli_query($con,"SELECT servicename x FROM servicemaster WHERE serviceid = '$serviceid' "))['x'];

                echo '<tr>';
                                
                  echo '<td>'.$servicename.'</td>';

                  echo '<td>'.$rowsss['agentname'].'</td>';

                  echo '<td>'.$rowsss['agenttype'].'</td>';

                  echo '<td>'.$rowsss['commissionamount'].'</td>';

                  $total+=$rowsss['commissionamount'];

                echo '</tr>';
              
              }

              echo '</tbody>';

            echo '</table>';

            echo 'Total Commission Amount : '.$total;


          ?>

        </div>

        <div class="col-sm-2 td">

          <!-- <button class="btn btn-sm btn-block btn-success" data-commissionid="<?php echo $commissionid; ?>"  onclick="approval(this)">Paid</button> -->          
          <?php
          
            if ($rows['check_time']!=0) {
            
          ?>

            <button class="btn btn-sm btn-block btn-warning" data-commissionid="<?php echo $commissionid; ?>" data-branchid="<?php echo $branchid; ?>"  onclick="confirm(this)">Confirm</button>

          <?php
          
            }               
            if ($countId != 0) {
              
          ?>
            
            <a href="commissionpaid.php?commissionid=<?php echo $commissionid;?>&edit=true&customerid=<?php echo $customerid;?>&paidid=<?php echo $paidid;?>" class="btn btn-info btn-sm btn-block" id="billStatus">Bill Status</a>
          
          <?php } ?>

            <a href="/process/invoice/invoicelink.php?commissionid=<?php echo $commissionid;?>&customerid=<?php echo $customerid;?>&category=<?php echo $category;?>&comefrom=commission" class="btn btn-success btn-sm btn-block">Raise Invoice</a>

            <button data-commissionid="<?php echo $commissionid;?>" data-customerid="<?php echo $customerid;?>" data-branchid = "<?php echo $branchid;?>"  data-toggle="modal" data-target="#myModal3" class="btn btn-danger btn-sm btn-block" onclick="linkInvoice(this)">Link Invoice</button>
          
        </div>
        
      </div>

      <?php } ?>

    </div>

  <!-- </div> -->
    
</div>

<!---------------------- Link Invoice Model Start-------------------->

<div id="myModal3" class="modal fade" role="dialog">

  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">

      <div class="modal-header">

        <button type="button" class="close" data-dismiss="modal">&times;</button>

        <h4 class="modal-title" align="center">Link Invoice</h4>

      </div>

      <div class="modal-body">

        <div class="form-group row">
            
          <div id="myModalLinkInvoice">
            
            <input type="hidden" data-name="branchid" id="inp-branchId">

            <input type="hidden" data-name="commissionid" id="inp-commissionId">

            <input type="hidden" data-name="customerId" id="inp-customerId">
            
          </div>

          <div class="col-sm-12">

            <div class="table-responsive" id="serviceTable">

              <table class="table table-list form-group" id="table-order">

                <thead>

                  <th width="2%">#</th>

                  <th width="20%">Bill No</th>
                                            
                  <th width="5%">Action</th>

                </thead>

                <tbody>

                  <tr>
                            
                    <td></td>

                    <td>

                      <div class="form-group">

                        <select class="select-js form-control input-sm" data-role='select' id="invoiceNo" data-name="billno">

                          <option value="Select">Select</option>

                        </select>

                      </div>
                          
                    </td>

                    <td>

                      <button class="btn btn-success btn-sm center-block" id="btn-add" data-edit="" onclick="add(this)">Add</button>

                    </td>

                  </tr>

                </tbody>
                    
              </table>   

            </div>       
                
          </div>
        
        </div>

      </div>

      <div class="modal-footer">

        <button class="btn btn-success btn-block btn-sm" id="LinkBill" onclick="submit()">Link Bill</button>

      </div>

    </div>

  </div>

</div>

<!---------------------- Link Invoice Model End-------------------->

<?php 

  include($base.'_in/footer.php');

?>

<script>

function linkInvoice(e) {

  $('#inp-commissionId').val($(e).attr('data-commissionid'));

  $('#inp-customerId').val($(e).attr('data-customerid'));

  $('#inp-branchId').val($(e).attr('data-branchid'));

  if ($(e).attr('data-customerid') != '' && $(e).attr('data-commissionid') != '' && $(e).attr('data-branchid')) {

    $.ajax({

      type: "POST",

      data: "customerid=" + $(e).attr('data-customerid') + "&commissionid=" + $(e).attr('data-commissionid') + "&branchid=" + $(e).attr('data-branchid'),

      url: 'api/billnolink.php',

      cache: false,

      success: function (res) {

        if (res.status == 'success') {

          
          let json = res.json;

          let str  = '<option = Select>Select Bill No </option>';

          for(let i in json){

            str+='<option = '+json[i].billno+'>'+json[i].billno+'</option>';

          }

          $('#invoiceNo').html(str);

          let jsonLinkBillNo = res.jsonLinkBillNo;

          if (Object.keys(jsonLinkBillNo).length != 0) {

            let count = 1;

            for (let i  in jsonLinkBillNo) {

              fun_adder(count,jsonLinkBillNo[i].billno);
              
              count++;

            }
            
            $('#LinkBill').attr('onclick', 'update()');

            $('#LinkBill').text('Update Link Bill');

          } else {

            $('#LinkBill').attr('onclick', 'submit()');

            $('#LinkBill').text('Link Bill');

          }

        } else {

          $('#invoiceNo').html('<option = Select>Select Bill No </option>');

        }

      }

    })
    
  } else {

    $('#invoiceNo').html('<option = Select>Select Bill No </option>');

  }
    
}


//--------------------------Add Multiple Services start-------------------------------------//

function add(e) {

  let chk = $(e).attr('data-edit');

  let data = checker('table-order');

  let billno = data.billno;

  let valid = true;

  if (checker('table-order') != false) {

    valid = valid * true;

  } else {

    valid = valid * false;

  }

  $('#table-order .billno').each(function () {

    let billno1 = $(this).text().trim();

    if (billno1 == billno && chk == '') {

      valid = valid * false;

      alert('Dublicate Bill No');

    }

  });

  if (valid) {

    let chk = $(e).attr('data-edit');

    if (chk == '') {

      let len = $('#table-order .srNo').length + 1;

      fun_adder(len, billno);

    } else {

      fun_adder_edit(chk, billno);

    }

    modals.clear('table-order');

    $('#btn-add').attr('data-edit', '');

  }

}


function fun_adder(len, billno) {

  let str = '<tr id="invoiceNo">';
    
    str += '<td align="center" class="srNo">' + len + '</td>';
    
    str += '<td align="center" class="billno">' + billno + '</td>';
    
    str += '<td align="center"><button class="btn btn-sm btn-danger remover" onclick="remover(this)">R</buuton></td>';
    
  str += '</tr>';

  $('#table-order > tbody').append(str);

}


function submit() {

  let valid = true;

  let arr = [];

  $("#table-order > tbody > tr").each(function () {

    let srNo   = $(this).find('.srNo').text().trim();

    let billno = $(this).find('.billno').text().trim();

    arr.push({

      "srno": srNo,

      "billno": billno

    });

  })

  arr.shift();
  
  if (arr == '') {

    $('#table-order').css('border', '2px solid red');

    valid = valid * false;

  } else {

    $('#table-order').css('border', '2px solid green');

    valid = valid * true;

  }

  if (valid) {

    var billDetails = JSON.stringify(arr);

    $.ajax({

      type: "POST",
            
      data: {

        branchid: $('#inp-branchId').val(),

        commissionid: $('#inp-commissionId').val(),
        
        customerid: $('#inp-customerId').val(),

        billDetails: billDetails,

      },

      url: 'api/insertlinkBill.php',
            
      cache: false,
            
      success: function (res) {
            
        if (res.status == 'success') {
            
          swal({
                        
            type: 'success',
                       
            title: 'Commission Invoice Link',
                       
            showConfirmButton: false,
                       
            timer: 3000
                    
          });
                   
          setTimeout(function () {
                        
            location.href = 'paid.php';
                    
          }, 3000);
                
        }

      }
        
    });
    
  }

}

function update() {

  let valid = true;

  let arr = [];

  $("#table-order > tbody > tr").each(function () {

    let srNo   = $(this).find('.srNo').text().trim();

    let billno = $(this).find('.billno').text().trim();

    arr.push({

      "srno": srNo,

      "billno": billno

    });

  })

  arr.shift();
  
  if (arr == '') {

    $('#table-order').css('border', '2px solid red');

    valid = valid * false;

  } else {

    $('#table-order').css('border', '2px solid green');

    valid = valid * true;

  }

  if (valid) {

    var billDetails = JSON.stringify(arr);

    $.ajax({

      type: "POST",
            
      data: {

        branchid: $('#inp-branchId').val(),

        commissionid: $('#inp-commissionId').val(),
        
        customerid: $('#inp-customerId').val(),

        billDetails: billDetails,

      },

      url: 'api/updatelinkBill.php',
            
      cache: false,
            
      success: function (res) {
            
        if (res.status == 'success') {
            
          swal({
                        
            type: 'success',
                       
            title: 'Commission Invoice Link',
                       
            showConfirmButton: false,
                       
            timer: 3000
                    
          });
                   
          setTimeout(function () {
                        
            location.href = 'paid.php';
                    
          }, 3000);
                
        }

      }
        
    });
    
  }

}

function confirm(e) {

  let commissionid = $(e).attr('data-commissionid');

  let branchid     = $(e).attr('data-branchid');

  if (commissionid != '' && branchid != '') {

    $.ajax({

      type: "POST",

      data: "commissionid=" + commissionid + "&branchid=" + branchid,

      url: 'api/commissionpaid.php',

      cache: false,

      success: function (res) {

        if (res.status == 'success') {

          swal({
              
            type: 'success',
            
            title: 'Commission Paid',
            
            showConfirmButton: false,
            
            timer: 3000
          
          });
          
          setTimeout(function () {
              
            location.href = 'paid.php';
          
          }, 3000);

        }

      }

    })

  } else {

    alert('Somthing Went Wrong');

  }

}


$('#myModal3').on('hidden.bs.modal', function (e) {

  $(this).find("input,textarea,select").val('').end();
  
  $('#table-order > tbody > tr#invoiceNo').remove();

  $('#LinkBill').attr('onclick', 'submit()');

  $('#LinkBill').text('Link Bill');

})

$(document).ready(function() {

  $('.select-js').select2({width: '100%', tags: true});

  $('.select').attr('style','width:100%!important;');

});

</script>