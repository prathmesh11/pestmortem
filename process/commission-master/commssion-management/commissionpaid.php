<?php


    $base = '../../../';
    
    $navenq3 = 'background:#1B1464;';
     
    include('header.php');
  
    if (!in_array("Bill Paid", $arrysession)) {
   
        echo "<script>window.location.href='/process/dashboard.php';</script>";
        
        exit;
    
    }
  
    if (session_status() == PHP_SESSION_NONE) { session_start(); }
    
    $sessionby    = $_SESSION['employeeid'];
    
    $branchid     = $_SESSION['branchid'];

    $commissionid = $_GET['commissionid'];

    $customerid   = $_GET['customerid'];


?>

<style>

    hr.style-hr {
        border: 0;
        height: 1px;
        background-image: linear-gradient(to right, rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.75), rgba(0, 0, 0, 0));
    }

    .table-list td,
    .table-list th {
        border: 1px solid #ddd;
        padding: 2px;
        font-size: 13px;
    }

    .table-list tr:nth-child(even) {
        background-color: #f2f2f2;
    }

    .table-list th {
        padding-top: 12px;
        padding-bottom: 12px;
        text-align: left;
        background-color: #16a085;
        color: white;
    }


</style>

<div class="container-fluid">

    <br>

    <h2 align="center" style="margin-top:0px;">Bill Paid</h2>

    <div style="clear:both;"></div>
    
    <hr class="style-hr">

    <div class="col-sm-12" id="section">
        
        <input type="hidden" data-role="text" data-name ='branchid' value ="<?php echo $branchid; ?>" id='branchid'>

        <input type="hidden" value="<?php echo $_GET['commissionid']; ?>" data-name ='commissionid' class="form-control input-sm" id="commissionid" />
        
        <input type="hidden" value="<?php echo $_GET['customerid']; ?>" data-name ='customerid' class="form-control input-sm" id="customerid" />

        <input type="hidden" value="<?php echo $_GET['edit']; ?>" class="form-control input-sm" id="edit" />

        <input type="hidden" value="<?php echo $_GET['paidid']; ?>" data-name ='paidid' class="form-control input-sm" id="paidid" />

        <div class="col-sm-5">

            <div class="form-group">

                <label for="">Customer Name</label>
                
                <select class="form-control input-sm" data-name="customerid" id="customerid" data-role="select" class="form-control input-sm" onchange="billNo()" readonly>

                    <?php

                        $customername = mysqli_fetch_assoc(mysqli_query($con,"SELECT customername x FROM customermaster WHERE customerid='$customerid' "))['x'];

                        echo '<option value="'.$customerid.'">'.$customername.'</option>';
                                    
                    ?>

                </select>

            </div>

        </div>
    
    </div>

    <h2 align="center" style="margin-top:0px;">Service Details</h2>

    <div style="clear:both;"></div>
    
    <hr class="style-hr">

    <div class="col-sm-12">

        <div class="table-responsive" id="serviceTable">

            <table class="table table-list form-group" id="table-order">

                <thead>

                    <th width="2%">#</th>

                    <th width="8%" style="text-align: center;">Bill No</th>

                    <th width="5%" style="text-align: center;">Bill Amount</th>
                    
                    <th width="5%" style="text-align: center;">Status</th>
                    
                    <th width="50%" colSpan="3" style="text-align: center;">Agent Commission Details</th>

                </thead>

                <tbody>

                    <?php

                        $count        = 1;

                        $commissionid = $_GET["commissionid"];

                        $select       = mysqli_query($con,"SELECT * FROM link_billno WHERE commissionid = '$commissionid'");

                        while ($row = mysqli_fetch_assoc($select)) {

                            $billamt     = $row['billamt'];

                            $billno      = $row['billno'];

                            $serviceid   = mysqli_fetch_assoc(mysqli_query($con,"SELECT serviceid x FROM comssionservice WHERE commissionid = '$commissionid' AND branchid ='$branchid' "))['x'];

                            $billStatus  = '';

                            $receiveAmt  = mysqli_fetch_assoc(mysqli_query($con,"SELECT sum(receiveAmt) x FROM invoicepayment WHERE invoiceNo='$billno' AND branchid ='$branchid' "))['x'];

                            if ($receiveAmt == $billamt) {
                                
                                $billStatus = "Paid";
                
                            } elseif ($receiveAmt != '') {
                                
                                $billStatus = "Part Paid";
                
                            } elseif($receiveAmt == '') {
                                
                                $billStatus = "Not Paid";
                
                            }

                            $service_json = mysqli_fetch_assoc(mysqli_query($con,"SELECT service_json x FROM billing WHERE invoice_bill = '$billno' AND branchid ='$branchid' "))['x'];

                            echo '<tr class="serviceDetails">';
                                    
                                echo '<td class = "srNo">'.$count.'</td>';

                                echo '<td class = "billNo">'.$row['billno'].'</td>';
                                
                                echo '<td class = "billamt">'.$row['billamt'].'</td>';

                                echo '<td class = "billstatus">'.$billStatus.'</td>';

                                echo '<td>';

                                    echo '<table class="table table-list form-group" id="table-agentDetails">';

                                        echo '<thead>';

                                            echo '<th width="10%">Service Name</th>';

                                            echo '<th width="5%">Qty</th>';
                                                        
                                            echo '<th width="10%">Agent Name</th>';

                                            echo '<th width="5%">Agent Commission</th>';                                                
                                            
                                        echo '</thead>';

                                        echo '<tbody>';
                                            
                                        $service_json = json_decode($service_json);
                                        
                                        foreach ($service_json as $i){
                                        
                                            $servicecode = get_object_vars($i)['servicecode'];
                                            
                                            $commisionrequest = mysqli_query($con,"SELECT * FROM commisionrequest WHERE commissionid = '$commissionid' AND branchid ='$branchid' AND serviceid='$servicecode'");
                                            
                                            while($row1=mysqli_fetch_assoc($commisionrequest)){
                                            
                                                $servicename = get_object_vars($i)['servicename'];
                                            
                                                $qty         = get_object_vars($i)['qty'];
                                            
                                                echo '<tr class="agentDetails">';
                                            
                                                    echo '<td>'.$servicename.'</td>';
                                            
                                                    echo '<td>'.$qty.'</td>';
                                            
                                                    echo '<td class = "agentid" style="display:none;">'.$row1['agentid'].'</td>';

                                                    echo '<td>'.$row1['agentname'].'</td>';
                                            
                                                    echo '<td class = "commissionamount">'.($qty*$row1['commissionamount']).'</td>';
                                            
                                                echo '</tr>';

                                            }

                                        }

                                        echo '</tbody>';

                                    echo '</table>';

                                echo '</td>';


                            echo '</tr>';

                                
                                $count++;
                        }
                        
                    ?>
                            
                </tbody>
                
            </table>   

        </div>       
            
    </div>

    <h2 align="center" style="margin-top:0px;">Commision Agent Details</h2>

    <div style="clear:both;"></div>
    
    <hr class="style-hr">

    <div class="col-sm-12">

        <div class="table-responsive" id="agentTable">

            <table class="table table-list form-group" id="table-agent">

                <thead>

                    <th width="2%">#</th>

                    <th width="10%">Agent Name</th>

                    <th width="8%">Type</th>
                    
                    <th width="8%">Service Name</th>
                    
                    <th width="5%">Commision_Amt</th>

                </thead>

                <tbody>
                        
                    <?php

                        $count = 1;

                        $total = 0;

                        $result1 = mysqli_query($con,"SELECT * FROM commisionrequest WHERE commissionid='$commissionid' AND branchid='$branchid' AND customerid = '$customerid'  ");

                        while($rowsss=mysqli_fetch_assoc($result1)){
        
                        $serviceid   = $rowsss['serviceid'];
                        
                        $servicename = mysqli_fetch_assoc(mysqli_query($con,"SELECT servicename x FROM servicemaster WHERE serviceid = '$serviceid' "))['x'];
        
                        echo '<tr class="item">';

                            echo '<td class="count">'.$count.'</td>';

                            echo '<td class="agentid" style="display:none;">'.$rowsss['agentid'].'</td>';

                            echo '<td class="agentname">'.$rowsss['agentname'].'</td>';
        
                            echo '<td class="agenttype">'.$rowsss['agenttype'].'</td>';
        
                            echo '<td class="contactno">'.$servicename.'</td>';
        
                            echo '<td><input type="text" class="commissionamount1" id="contactno" data-name="contactno" data-role="text" value="'.$rowsss['commissionamount'].'" onkeyup="changeCommissionAmt()" class="form-control input-sm" style="background-color: #eee; opacity: 1;" readonly></td>';
                    
                        echo '</tr>';

                        $count++;

                        
                        }
                        
                        ?>

                    </tr>
                        
                </tbody>
                
            </table>   

        </div>       
            
    </div>
    
    <div class="col-sm-12">
            
        <div style="clear:both;"></div>
        
        <hr class="style-hr">

        <div class="col-sm-4"> </div>
            
        <div class="col-sm-3">
            
            <button class="btn btn-success btn-sm btn-block" id="btn-submit" onclick="submit()" style="margin:5px;">Check</button>
        
        </div>
        
        <div class="col-sm-3"></div>
    
    </div>

</div>

<?php
    
    include($base.'_in/footer.php');

?>

<script>

$(document).ready(function() {

    $('.select-js').select2({width: '100%', tags: true});

    $('.select').attr('style','width:100%!important;');

});


function submit() {

    let valid = true;

    if (checker('section') != false) {

        valid = valid * true;

    } else {

        valid = valid * false;

    }
    
    var lastarray = [];

    $('#table-order > tbody > tr').each(function(){

        var arrmain   = {};
    
        var bill_id = $(this).find('.billNo').text().trim();
        
            arrmain['bill_id']=bill_id;
    
        var arr  = [];
        
        var arr1 = [];
    
        $(this).find('.form-group').find('tbody > tr').each(function(){
    
            var agentid = $(this).find('.agentid').text().trim();
    
            arr1.push(agentid);
    
        })
    
        var arr1 = arr1.filter((v, p) => arr1.indexOf(v) == p);
    
        for(var i=0 in arr1){
     
            var sum = 0;
       
            $(this).find('.form-group').find('tbody > tr').each(function(){
        
                var agentid = $(this).find('.agentid').text().trim();
     
                if(arr1[i]==agentid){
        
                    var commissionamount = parseFloat($(this).find('.commissionamount').text().trim());
        
                    sum=sum+commissionamount;
        
                }

            })

            arr.push({"agentid":arr1[i],"commissionamount":sum});

        }
 
        arrmain['agentinfo']=arr;
  
        lastarray.push(arrmain);

    })
    // arr.shift();

    if (valid) {

        let data1       = checker('section');
           
            data1       = JSON.stringify(data1);

            billDetails = JSON.stringify(lastarray);

        $.ajax({
            type: "POST",
            
            data: {

                data: data1,

                billDetails: billDetails,

            },

            url: 'api/insertcommission.php',
            
            cache: false,
            
            success: function (res) {
            
                if (res.status == 'success') {
            
                    swal({
                        
                        type: 'success',
                       
                        title: 'Commission Check',
                       
                        showConfirmButton: false,
                       
                        timer: 3000
                    
                    });
                   
                    setTimeout(function () {
                        
                        location.href = 'paid.php';
                    
                    }, 3000);
                
                }

                if (res.status == 'falid1') {

                    swal('Something Went Wrong', '', 'error');
                   
                    $('#inp-phone').css('border-bottom', '1px solid red');

                }

            }
        
        });
    
    }

}


if ($('#commissionid').val() != '' && $('#edit').val() == 'true' && $('#paidid').val() != '') {

    $.ajax({

        type: "POST",

        data: "commissionid=" + $('#commissionid').val() + "&paidid=" + $('#paidid').val(),

        url: 'api/selectcommission.php',

        cache: false,

        success: function (res) {

            if (res.status == 'success') {

                $('#btn-submit').attr('onclick', 'update()');

                $('#btn-submit').text('Update');

                var billJson = res.json;

                var serial = 1;

                for (var i in billJson) {

                    fun_adder(serial, billJson[i].billno, billJson[i].billamount, billJson[i].status, billJson[i].paidtype, billJson[i].commissionamt, billJson[i].description, billJson[i].paiddate);

                    serial++;

                }

            }

        }

    });

}

//--------------------------Edit Function End----------------------//

function update() {

    let valid = true;

    if (checker('section') != false) {

        valid = valid * true;

    } else {

        valid = valid * false;

    }

    let arr = [];

    $("#table-agent > tbody > tr.item").each(function () {

        let srNo             = $(this).find('.count').text().trim();

        let agentid          = $(this).find('.agentid').text().trim();
        
        let agentname        = $(this).find('.agentname').text().trim();
        
        let agenttype        = $(this).find('.agenttype').text().trim();
        
        let contactno        = $(this).find('.contactno').text().trim();
        
        let commissionamount = $(this).find('input.commissionamount1').val();
        
        arr.push({

            "srno": srNo,

            "agentid": agentid,

            "agentname": agentname,

            "agenttype": agenttype,

            "contactno": contactno,

            "commissionamount": commissionamount

        });

    })

    
    // arr.shift();

    if (arr == '') {

        $('#table-agent').css('border', '2px solid red');

        valid = valid * false;

    } else {

        $('#table-agent').css('border', '2px solid green');

        valid = valid * true;

    }

    let arr1 = [];

    $('#table-order > tbody > tr').each(function () {

        let srNo          = $(this).find('.srNo').text().trim();

        let billno        = $(this).find('.billno').text().trim();
        
        let billamount    = $(this).find('.billamount').text().trim();
        
        let status        = $(this).find('.status').text().trim();
        
        let paidtype      = $(this).find('.paidtype').text().trim();
        
        let commissionamt = $(this).find('.commissionamt').text().trim();
        
        let description   = $(this).find('.description').text().trim();
        
        let paiddate      = $(this).find('.paiddate').text().trim();
        
        arr1.push({

            "srno": srNo,

            "billno": billno,

            "billamount": billamount,

            "status": status,

            "paidtype": paidtype,

            "commissionamt": commissionamt,

            "description": description,

            "paiddate": paiddate

        });

    })

    
    arr1.shift();

    if (arr1 == '') {

        $('#table-order').css('border', '2px solid red');

        valid = valid * false;

    } else {

        $('#table-order').css('border', '2px solid green');

        valid = valid * true;

    }

    if (valid) {

        let data1       = checker('section');
           
            data1       = JSON.stringify(data1);

            billDetails = JSON.stringify(arr1);

            agentDetails = JSON.stringify(arr);

        $.ajax({
            type: "POST",
            
            data: {

                data: data1,

                billDetails: billDetails,

                agentDetails: agentDetails,

            },

            url: 'api/updatecommission.php',
            
            cache: false,
            
            success: function (res) {
            
                if (res.status == 'success') {
            
                    swal({
                        
                        type: 'success',
                       
                        title: 'Commission Add',
                       
                        showConfirmButton: false,
                       
                        timer: 3000
                    
                    });
                   
                    setTimeout(function () {
                        
                        location.href = 'paid.php';
                    
                    }, 3000);
                
                }

                if (res.status == 'falid1') {

                    swal('conatct No Or Email Id Duplicate', '', 'error');
                   
                    $('#inp-phone').css('border-bottom', '1px solid red');

                }

            }
        
        });
    
    }

}

$('#btn-reset').on('click', function () {
    window.location.reload();
});


</script>