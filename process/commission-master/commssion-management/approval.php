<?php 

  $base = '../../../';

  $navenq2 = 'background:#1B1464;';
  
  include('header.php');

   if (!in_array("Commission Approval", $arrysession)) {
   
      echo "<script>window.location.href='/process/dashboard.php';</script>";
     
      exit;

  }

  if (session_status() == PHP_SESSION_NONE) { session_start(); }
  
  $sessionby = $_SESSION['employeeid'];
  
  $branchid  = $_SESSION['branchid'];

?>
<br>
<style>

.th {
        background: #8e44ad;
        color: #fff;
        text-align: center;
        padding-top: 2px;
        padding-bottom: 2px;
        border: 1px solid #fff;
    }

    .td {
        border: 1px solid #ddd;
    }

    .table-ui .btn {
        margin: 3px;
    }

    #myModal1 .col-sm-4,
    .col-sm-8 {
        margin-bottom: 2px;
    }

    .picker__select--year {
        height: auto;
    }

    .picker__select--month {
        height: auto;
    }

    .table-list td,
    .table-list th {
        border: 1px solid #ddd;
        padding: 1px !important;
        font-size: 13px;
    }

    .table-list td {
        padding-top: 10px !important;
    }

    .table-list tr:nth-child(even) {
        background-color: #f2f2f2;
    }

    .table-list th {
        padding-top: 5px;
        padding-bottom: 5px;
        text-align: center;
        background-color: #16a085;
        color: white;
    }

</style>

<img class="hidden" id=imageid src="../../../../img/PestMortemLogo.jpg">

<div class="container-fluid">

<?php   
  
  $admin_branch = '';

  if($_SESSION['role']=='admin'){

    $branchid_admin = '';

    echo '<select onchange="branchadmin(this)" class="form-control input-sm" style="margin:2px;">';

    if($_COOKIE['branchid']!=''){

      $branchid_admin = $_COOKIE['branchid'];

      $branchname1    = mysqli_fetch_assoc(mysqli_query($con,"SELECT branchname x FROM branchmaster WHERE branchid='$branchid_admin'"))['x'];

      echo '<option value="'.$branchid_admin.'">'.$branchname1.'</option>';

    }
      
    echo '<option value="allbranch">All Branches</option>';

    $select1 = mysqli_query($con,"SELECT branchid, branchname FROM branchmaster WHERE branchid<>'$branchid_admin'");

    while($rows = mysqli_fetch_assoc($select1)){
        
      echo '<option value="'.$rows['branchid'].'">'.$rows['branchname'].'</option>';
      
    }
    
    echo '</select>';
  
  }
  
?>

    <br>

    <div class="table-ui container-fluid">

      <div class="tr row">

        <div class="col-sm-2 th">Customer Name</div>

        <div class="col-sm-4 th">Service Details</div>

        <div class="col-sm-4 th">Agent Details</div>

        <div class="col-sm-2 th">Action</div>

      </div>

      <?php

        if($branchid_admin==''){

          $result=mysqli_query($con,"SELECT DISTINCT commissionid,customerid FROM `commisionrequest` WHERE send_to_approval_time <> 0 AND approve_time = 0 ORDER BY id DESC");

        } else {

          $result=mysqli_query($con,"SELECT DISTINCT commissionid,customerid FROM `commisionrequest` WHERE branchid='$branchid_admin' AND send_to_approval_time <> 0 AND approve_time = 0 ORDER BY id DESC");

        }
        
        while($rows = mysqli_fetch_assoc($result)){

          $commissionid = $rows['commissionid'];

          $customerid   = $rows['customerid'];

          $customername = mysqli_fetch_assoc(mysqli_query($con,"SELECT customername x FROM customermaster WHERE customerid = '$customerid' "))['x'];

      ?>

      <div class="row tr">
          
        <div class="col-sm-2 td">

          <?php 
            
            echo $customername;
            
          ?>

        </div>

        <div class="col-sm-4 td" style="word-wrap:break-word;">

          <?php
          
            echo '<table class="table table-list form-group">';
            
              echo '<thead>';
                  
                echo '<tr>';
                      
                  echo '<th width="300">Service Name</th>';
                  
                  echo '<th>Service Qty</th>';
                  
                  echo '<th width="80">	Service Rate</th>';
                  
                  echo '<th>Total</th>';
                                                  
                echo '</tr>';
              
              echo '</thead>';
              
              echo '<tbody>';

              $total = 0;

              $result1 = mysqli_query($con,"SELECT * FROM comssionservice WHERE commissionid='$commissionid' AND branchid='$branchid' AND customerid = '$customerid'  ");

              while($rowss=mysqli_fetch_assoc($result1)){

                $serviceid   = $rowss['serviceid'];
               
                $servicename = mysqli_fetch_assoc(mysqli_query($con,"SELECT servicename x FROM servicemaster WHERE serviceid = '$serviceid' "))['x'];

                echo '<tr>';
                                
                  echo '<td>'.$servicename.'</td>';

                  echo '<td>'.$rowss['serviceqty'].'</td>';

                  echo '<td>'.$rowss['servicerate'].'</td>';

                  echo '<td>'.$rowss['servicetotal'].'</td>';

                echo '</tr>';
              
              }

              echo '</tbody>';

            echo '</table>';

          ?>

        </div>

        <div class="col-sm-4 td" style="word-wrap:break-word;">

          <?php
          
            echo '<table class="table table-list form-group">';
            
              echo '<thead>';
                  
                echo '<tr>';
                      
                  echo '<th width="300">Agent Name</th>';
                  
                  echo '<th>Type</th>';
                  
                  echo '<th width="80">Contact No</th>';
                  
                  echo '<th>Commision Amt</th>';
                                                  
                echo '</tr>';
              
              echo '</thead>';
              
              echo '<tbody>';

              $total = 0;

              $result1 = mysqli_query($con,"SELECT * FROM commisionrequest WHERE commissionid='$commissionid' AND branchid='$branchid' AND customerid = '$customerid'  ");

              while($rowsss=mysqli_fetch_assoc($result1)){

                $serviceid   = $rowss['serviceid'];
               
                $servicename = mysqli_fetch_assoc(mysqli_query($con,"SELECT servicename x FROM servicemaster WHERE serviceid = '$serviceid' "))['x'];

                echo '<tr>';
                                
                  echo '<td>'.$rowsss['agentname'].'</td>';

                  echo '<td>'.$rowsss['agenttype'].'</td>';

                  echo '<td>'.$rowsss['contactno'].'</td>';

                  echo '<td>'.$rowsss['commissionamount'].'</td>';

                  $total+=$rowsss['commissionamount'];

                echo '</tr>';
              
              }

              echo '</tbody>';

            echo '</table>';

            echo 'Total Commission Amount : '.$total;


          ?>

        </div>

        <div class="col-sm-2 td">

          <button class="btn btn-sm btn-block btn-success" data-commissionid="<?php echo $commissionid; ?>"  data-branchid="<?php echo $branchid; ?>" onclick="approval(this)">Approval</button>

          <a href="commission.php?commissionid=<?php echo $commissionid;?>&edit=true&send=approval" class="btn btn-info btn-sm btn-block">Edit</a>

          <button class="btn btn-sm btn-block btn-danger" data-commissionid="<?php echo $commissionid; ?>"  onclick="back(this)">Back</button>
          
        </div>
        
      </div>

      <?php } ?>

    </div>

  <!-- </div> -->
    
</div>


<?php 

    include($base.'_in/footer.php');
?>
<script>

function approval(e) {

  let commissionid = $(e).attr('data-commissionid');

  let branchid     = $(e).attr('data-branchid');

  if (commissionid != '') {

    $.ajax({

      type: "POST",

      data: "commissionid=" + commissionid + "&branchid=" + branchid,

      url: 'api/commissionapproval.php',

      cache: false,

      success: function (res) {

        if (res.status == 'success') {

          swal({
              
            type: 'success',
            
            title: 'Commission Approval',
            
            showConfirmButton: false,
            
            timer: 3000
          
          });
          
          setTimeout(function () {
              
            location.href = 'approval.php';
          
          }, 3000);

        }

      }

    })

  } else {

    alert('Somthing Went Wrong');

  }

}


function back(e) {

  let commissionid = $(e).attr('data-commissionid');;

  if (commissionid != '') {

    $.ajax({

      type: "POST",

      data: "commissionid=" + commissionid ,

      url: 'api/commissionback.php',

      cache: false,

      success: function (res) {

        if (res.status == 'success') {

          swal({
              
            type: 'success',
            
            title: 'Commission Send To Commission Request',
            
            showConfirmButton: false,
            
            timer: 3000
          
          });
          
          setTimeout(function () {
              
            location.href = 'index.php';
          
          }, 3000);

        }

      }

    })

  } else {

    alert('Somthing Went Wrong');

  }

}

function branchadmin(e){

  var branchid=$(e).val();

  if(branchid=='allbranch'){

    setCookie('branchid','', '');

    location.reload();

  }else{

    setCookie('branchid',branchid,1);

    location.reload();

  }

}

function setCookie(cname, cvalue, exdays) {

  var d = new Date();

  d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));

  var expires = "expires="+d.toUTCString();

  document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";

}


</script>