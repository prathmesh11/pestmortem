<?php

    $base = '../../../../';

    include($base.'_in/connect.php');

    header('content-type: application/json; charset=utf-8');

    header("access-control-allow-origin: *");

    if(isset($_POST['data']) && isset($_POST['paymentDetails'])){

        $con = _connect();

        if (session_status() == PHP_SESSION_NONE) { session_start(); }

        $created_by         = $_SESSION['employeeid'];

        $data               = get_object_vars(json_decode($_POST["data"]));

        $branchid           = $data['branchid'];

        $agentid            = $data['agentid'];

        $agentamount        = $data['agentamount'];

        $paymentdate        = $data['paymentdate'];

        $paymentdescription = $data['paymentdescription'];
 
        $paidid             = $data['paidid'];

        $update             = mysqli_query($con,"UPDATE commission_check SET paidid = '' WHERE branchid = '$branchid' AND agentid = '$agentid' AND paidid = '$paidid' ");

        $delete             = mysqli_query($con,"DELETE FROM commissionpaid WHERE branchid = '$branchid' AND agentid = '$agentid' AND paidid = '$paidid' ");

        $paymentDetails     = json_decode($_POST["paymentDetails"]);

        foreach ($paymentDetails as $i) {

            $billno           = get_object_vars($i)['billno'];

            $commissionamount = get_object_vars($i)['commissionamount'];

            $commissionid     = get_object_vars($i)['commissionid'];

            $check            = get_object_vars($i)['check'];

            if ($check == true) {
                
                $checkradio = 1;

            }

            $create = mysqli_query($con, "INSERT INTO commissionpaid (branchid,commissionid,paidid,agentid,agentamount,paymentdate,paymentdescription,billno,commissionamount,checkradio,paid_by,paid_time) VALUES ('$branchid', '$commissionid', '$paidid', '$agentid', '$agentamount', '$paymentdate', '$paymentdescription', '$billno', '$commissionamount', '$checkradio','$created_by', '$CURRENT_MILLIS')");
              
            $update = mysqli_query($con,"UPDATE commission_check SET paidid = '$paidid' WHERE branchid = '$branchid' AND billno = '$billno' AND commissionid = '$commissionid' AND agentid = '$agentid' ");

        }
        
        if($create){

            echo '{"status":"success"}';

        } else {

            echo '{"status":"falid1"}';

        }

        _close($con);

    } else {

        echo '{"status":"falid"}';

    }

?>