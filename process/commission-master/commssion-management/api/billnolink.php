<?php

    $base='../../../../';

    include($base.'_in/connect.php');

    header('content-type: application/json; charset=utf-8');

    header("access-control-allow-origin: *");

    if(isset($_POST['customerid']) && isset($_POST['commissionid']) && isset($_POST['branchid'])){
        
        $arrayprint   = array();
        
        $con          = _connect();

        $customerid   = _clean($con,$_POST["customerid"]);

        $commissionid = _clean($con,$_POST["commissionid"]);

        $branchid     = _clean($con,$_POST["branchid"]);

        $select       = mysqli_query($con, "SELECT * FROM billing WHERE customerid = '$customerid' AND branchid = '$branchid' AND commissionid = 0 ");

        while($row = mysqli_fetch_assoc($select)){

            $serviceJson = json_decode($row['service_json']);

            foreach ($serviceJson as $i) {
                
                $servicecode = get_object_vars($i)['servicecode'];
                
                $serviceid   = mysqli_fetch_assoc(mysqli_query($con,"SELECT serviceid x FROM commisionrequest WHERE commissionid = '$commissionid' AND branchid ='$branchid' AND serviceid='$servicecode'"))['x'];
                          
                if ($serviceid) {

                    array_push($arrayprint,$row['invoice_bill']);

                }
              
            }
       
        }

        $jsonarr = (array_unique(($arrayprint)));
        
        $json    = '';
        
        foreach ($jsonarr as $key => $value) {
       
            $json.= ',{"billno":"'.$value.'"}';
    
        }
    
        $json = substr($json,1);

        $json = '['.$json.']';

        $selectLinkBill = mysqli_query($con, "SELECT billno FROM link_billno WHERE customerid = '$customerid' AND branchid = '$branchid' AND commissionid = '$commissionid' ");

        while($row = mysqli_fetch_assoc($selectLinkBill)){

            $jsonLinkBillNo.= ',{"billno":"'.$row['billno'].'"}';
            
        }

        $jsonLinkBillNo = substr($jsonLinkBillNo,1);

        $jsonLinkBillNo = '['.$jsonLinkBillNo.']';

        if($select){            

            echo '{"status":"success","json":'.$json.',"jsonLinkBillNo":'.$jsonLinkBillNo.'}';

        } else {

            echo '{"status":"falid1"}';
    
        }
        
        _close($con);

    } else {

        echo '{"status":"falid"}';

    }

?>