<?php

    $base='../../../../';

    include($base.'_in/connect.php');

    header('content-type: application/json; charset=utf-8');

    header("access-control-allow-origin: *");

    if(isset($_POST['agentid']) && isset($_POST['branchid'])){

        $con      = _connect();

        $agentid  = $_POST['agentid'];

        $branchid = $_POST['branchid'];
    
        $select   = mysqli_query($con,"SELECT commission_check.billno,billing.contractamt,commission_check.commissionamount,commission_check.commissionpaid_time FROM commission_check INNER JOIN billing ON commission_check.billno = billing.invoice_bill WHERE commission_check.agentid = '$agentid' AND commission_check.branchid= '$branchid'");

        while ($row = mysqli_fetch_assoc($select)) {

            $billno         = $row['billno'];

            $billamt        = $row['contractamt'];

            $paidBillAmount = 0;

            $receiveAmt     = mysqli_fetch_assoc(mysqli_query($con,"SELECT sum(receiveAmt) x FROM invoicepayment WHERE invoiceNo='$billno' AND branchid ='$branchid' "))['x'];

            if ($receiveAmt == $billamt) {
                
                $billStatus     = "Paid";

                $paidBillAmount += $row['commissionamount'];

            } elseif ($receiveAmt != '') {
                
                $billStatus = "Part Paid";

            } elseif($receiveAmt == '') {
                
                $billStatus = "Not Paid";

            }

            $paidid = $row['commissionpaid_time'];

            if ($paidid != 0 ) {

                $paidStatus = "Commission Paid";

            } else {

                $paidStatus = "Commission Not Paid";

            }

            $json.= ',{"billno":"'.$billno.'","billamount":"'.$billamt.'","billStatus":"'.$billStatus.'","commissionamount":"'.$row['commissionamount'].'","paidStatus":"'.$paidStatus.'","paidBillAmount":"'.$paidBillAmount.'"}';
        }

        $json = substr($json,1);

        $json = '['.$json.']';

        $totalCommission = mysqli_fetch_assoc(mysqli_query($con,"SELECT sum(commissionamount) x FROM commission_check WHERE agentid = '$agentid' AND branchid= '$branchid' "))['x'];

        $paymentMade     = mysqli_fetch_assoc(mysqli_query($con,"SELECT sum(commissionamount) x FROM commission_check WHERE agentid = '$agentid' AND branchid= '$branchid' AND commissionpaid_time <>0 "))['x'];

        $outStanding     = $totalCommission-$paymentMade;

        $due             = $paidBillAmount - $paymentMade;

        if ($select) {

            echo '{"status":"success","json":'.$json.',"outStanding":"'.$outStanding.'","due":"'.abs($due).'"}';

        } else {
            
            echo '{"status":"falid1"}';

        }
       
        _close($con);

    } else {

        echo '{"status":"falid"}';

    }

?>