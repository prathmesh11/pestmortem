<?php

    $base='../../../../';

    include($base.'_in/connect.php');

    header('content-type: application/json; charset=utf-8');

    header("access-control-allow-origin: *");

    if(isset($_POST['data']) && isset($_POST['billDetails']) && isset($_POST['agentDetails'])){

        $con  = _connect();

        $data = get_object_vars(json_decode($_POST["data"]));

        if (session_status()==PHP_SESSION_NONE) { session_start(); }

        $created_by            = $_SESSION['employeeid'];

        $branchid              = $data['branchid'];

        $customerid            = $data["customerid"];

        $commissionid          = $data["commissionid"];

        $totalbillcommssionamt = $data['totalbillcommssionamt'];

        $totalcommissionamt    = $data['totalcommissionamt'];

        $paidid                = $data['paidid'];

        $billDetail            = $_POST["billDetails"];

        $billDetails           = json_decode($_POST["billDetails"]);

        mysqli_query($con,"DELETE FROM comssionbilldetails WHERE branchid = '$branchid' AND commissionid = '$commissionid' AND paidid = '$paidid'");

        $totalcommissionamt = 0;

        foreach ($billDetails as $i) {

            $srno          = get_object_vars($i)['srno'];
            
            $billno        = get_object_vars($i)['billno'];
            
            $billamount    = get_object_vars($i)['billamount'];
            
            $status        = get_object_vars($i)['status'];
            
            $paidtype      = get_object_vars($i)['paidtype'];
            
            $commissionamt = get_object_vars($i)['commissionamt'];

            $description   = get_object_vars($i)['description'];
            
            $paiddate      = get_object_vars($i)['paiddate'];

            $totalcommissionamt +=$commissionamt;
            
            mysqli_query($con,"INSERT INTO comssionbilldetails (branchid, commissionid, paidid, customerid, billno, billamount, status, paidtype, commissionamt, description, paiddate, created_by, created_time) VALUES ('$branchid', '$commissionid','$paidid' ,'$customerid', '$billno', '$billamount', '$status', '$paidtype', '$commissionamt','$description','$paiddate','$created_by','$CURRENT_MILLIS')");

        }

        $agentDetail  = $_POST["agentDetails"];

        $agentDetails = json_decode($_POST["agentDetails"]);

        $totalagentcommissionamt = 0;

        foreach ($agentDetails as $i) {

            $srno             = get_object_vars($i)['srno'];
            
            $agentid          = get_object_vars($i)['agentid'];

            $agentname        = get_object_vars($i)['agentname'];
            
            $agenttype        = get_object_vars($i)['agenttype'];
            
            $contactno        = get_object_vars($i)['contactno'];
            
            $commissionamount = get_object_vars($i)['commissionamount'];

            $totalagentcommissionamt +=$commissionamount;


            $create = mysqli_query($con,"UPDATE commisionrequest SET commissionamount='$commissionamount' WHERE agentid='$agentid' AND commissionid='$commissionid'");

        }


        if($create){

            mysqli_query($con,"UPDATE commissionpaid SET service_details='$billDetail' ,agent_details='$agentDetail',billamount='$totalcommissionamt', agentamount='$totalagentcommissionamt' WHERE paidid='$paidid' AND commissionid='$commissionid' AND branchid = '$branchid' ");

            echo '{"status":"success"}';

        } else {

            echo '{"status":"falid2"}';

        }

        _close($con);

    } else {

        echo '{"status":"falid"}';

    }

?>