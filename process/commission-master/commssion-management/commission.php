<?php


    $base = '../../../';

    if (isset($_GET['send'])=='approval') {

        $navenq2 = 'background:#1B1464;';
    
    } else {
    
        $navenq1 = 'background:#1B1464;';
    
    }  

    include('header.php');
  
    if (!in_array("Commission Request", $arrysession)) {
   
        echo "<script>window.location.href='/process/dashboard.php';</script>";
       
        exit;
  
    }
  
    if (session_status() == PHP_SESSION_NONE) { session_start(); }
    
    $sessionby = $_SESSION['employeeid'];
    
    $branchid  = $_SESSION['branchid'];
  

?>

<style>

    hr.style-hr {
        border: 0;
        height: 1px;
        background-image: linear-gradient(to right, rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.75), rgba(0, 0, 0, 0));
    }

    .table-list td,
    .table-list th {
        border: 1px solid #ddd;
        padding: 2px;
        font-size: 13px;
    }

    .table-list tr:nth-child(even) {
        background-color: #f2f2f2;
    }

    .table-list th {
        padding-top: 12px;
        padding-bottom: 12px;
        text-align: left;
        background-color: #16a085;
        color: white;
    }


</style>

<div class="container-fluid">

    <input type="hidden" value="<?php echo $_GET['send']; ?>" class="form-control input-sm" id="send" />

    <br>

    <h2 align="center" style="margin-top:0px;">Commission Request</h2>

    <div style="clear:both;"></div>
        
    <hr class="style-hr">

    <div class="col-sm-12" id="section">
            
        <input type="hidden" data-role="text" data-name ='branchid' value ="<?php echo $branchid; ?>" id='branchid'>

        <input type="hidden" value="<?php echo $_GET['commissionid']; ?>" data-name ='commissionid' class="form-control input-sm" id="commissionid" />
            
        <input type="hidden" value="<?php echo $_GET['edit']; ?>" class="form-control input-sm" id="edit" />

        <div class="col-sm-5">

            <div class="form-group">

                <label for="">Customer Name</label>
                
                <select class="form-control input-sm select-js" data-name="customerid" id="customerid" data-role="select" class="form-control input-sm">

                    <option value="Select">Select Customer Name</option>

                    <?php

                        $account = mysqli_query($con,"SELECT customerid,customername FROM customermaster WHERE branchid='$branchid' ORDER BY customername");

                        while ($row = mysqli_fetch_assoc($account)) {

                            echo '<option value="'.$row['customerid'].'">'.$row['customername'].'</option>';

                        }
                                        
                    ?>

                </select>

            </div>

        </div>

        <div class="col-sm-3">

            <div class="form-group">

                <label for="">Category</label>
                
                <select class="form-control input-sm " data-name="category" id="category" data-role="select" class="form-control input-sm" onchange="serviceName()">

                    <option value="Select">Select category</option>
                   
                    <option value="Pest Control">Pest Control</option>
                   
                    <option value="Fumigation">Fumigation</option>

                </select>

            </div>

        </div>
    
    </div>

    <h2 align="center" style="margin-top:0px;">Service Details</h2>

    <div style="clear:both;"></div>
        
    <hr class="style-hr">

    <div class="col-sm-12">

        <div class="table-responsive" id="serviceTable">

            <table class="table table-list form-group" id="table-order">

                <thead>

                    <th width="2%">#</th>

                    <th width="20%">Service Name</th>

                    <th width="5%">Service Rate</th>
                                            
                    <th width="5%">Action</th>

               </thead>

                <tbody>

                    <tr>
                            
                        <td></td>

                        <td>

                            <div class="form-group">

                                <select class="select-js form-control input-sm" data-role='select' id="serviceid" data-name="serviceid">

                                    <option value="Select">Select</option>

                                </select>

                            </div>
                            
                        </td>

                        <td>

                            <div class="form-group">

                                <input type="number" id="servicerate" data-name="servicerate" data-role="number" class="form-control input-sm">

                            </div>
                        
                        </td>


                        <td>

                            <button class="btn btn-success btn-sm center-block" id="btn-add" data-edit="" onclick="add(this)">Add</button>

                        </td>

                    </tr>
                            
                </tbody>
                    
            </table>   

        </div>       
                
    </div>

    <h2 align="center" style="margin-top:0px;">Commision Agent Details</h2>

    <div style="clear:both;"></div>
        
    <hr class="style-hr">

    <div class="col-sm-12">

        <div class="table-responsive" id="agentTable">

        </div>       
                
    </div>
        
    <div class="col-sm-12">
                
        <div style="clear:both;"></div>
            
        <hr class="style-hr">

        <div class="col-sm-3"> </div>
                
        <div class="col-sm-3">
                
            <button class="btn btn-success btn-sm btn-block" id="btn-submit" onclick="submit()" style="margin:5px;">Submit</button>
            
        </div>
            
        <div class="col-sm-3">
            
            <button class="btn btn-danger btn-sm btn-block" id="btn-reset" style="margin:5px;">Reset</button>
        
        </div>
               
        <div class="col-sm-3"></div>
        
    </div>


</div>

<?php
    
    include($base.'_in/footer.php');

?>

<script>


function serviceName() {

    let category = $('#category').val();

    if (category != 'Select') {
        
        $.ajax({

            type: "POST",

            data: "category=" + category,

            url: 'api/sevicename.php',

            cache: false,

            success: function (res) {

                if (res.status == 'success') {

                    let json = res.json;

                    str = '<option value= "Select">Select Services</option>';

                    for(let i in json){

                        str+='<option value = '+json[i].serviceid+'>'+json[i].servicename+'</option>'

                    }

                    $('#serviceid').html(str);

                } else {

                    $('#serviceid').html('<option value= "Select">Select Services</option>');

                }


            }

        })


    } else {

        $('#serviceid').html('<option value= "Select">Select Services</option>');

    }
    
}

//--------------------------Add Multiple Services start-------------------------------------//

function add(e) {

    let chk         = $(e).attr('data-edit');

    let data        = checker('table-order');

    let serviceid   = data.serviceid;

    let servicename = $('#serviceid').find(":selected").text();
    
    let servicerate = data.servicerate;
    
    let valid       = true;

    if (checker('table-order') != false) {

        valid = valid * true;

    } else {

        valid = valid * false;

    }

    $('#table-order .servicename').each(function () {

        let servicename1 = $(this).text().trim();

        if (servicename1 == servicename && chk == '') {

            valid = valid * false;

            alert('Dublicate Service Name');

        }

    });



    if (valid) {

        let chk = $(e).attr('data-edit');

        if (chk == '') {

            let len = $('#table-order .srNo').length + 1;

            fun_adder(len, serviceid, servicename, servicerate);

            fun_addAgentDetails(len,serviceid,servicename);

        } else {

            fun_adder_edit(chk, serviceid, servicename, servicerate);

            fun_addAgentDetails_edit(chk,serviceid,servicename);

        }

        modals.clear('table-order');

        $('#btn-add').attr('data-edit', '');

    }

}


function fun_adder(len, serviceid, servicename, servicerate) {

    let str  = '<tr id = '+len+'>';
        
        str += '<td align="center" class="srNo">' + len + '</td>';
        
        str += '<td align="center" class="serviceid" style="display:none;">' + serviceid + '</td>';
        
        str += '<td align="center" class="servicename">' + servicename + '</td>';
        
        str += '<td align="center" class="servicerate">' + servicerate + '</td>';
        
        str += '<td align="center"><button class="btn btn-sm btn-success" onclick="contactEditor(' + len + ',this) ">E</button>  <button class="btn btn-sm btn-danger remover" onclick="removers(' + len + ',this)">R</buuton></td>';
    
    str += '</tr>';
    
    $('#table-order > tbody').append(str);

    removehidden();
}

function fun_addAgentDetails(len,serviceid,servicename) {

    let str = '<table class="table table-list form-group" id="table-agent'+len+'">';

        str += '<thead>';
            
            str += '<tr>';
        
                str += '<th width="2%">#</th>';

                str += '<th width="8%">Service Name</th>';
        
                str += '<th width="8%">Agent Name</th>';

                str += '<th width="8%">Type</th>';
        
                str += '<th width="8%">contact No</th>';
        
                str += '<th width="8%">Paid Type</th>';

                str += '<th width="10%">Bank Name</th>';

                str += '<th width="10%">Bank Address</th>';

                str += '<th width="5%">IFSC Code</th>';

                str += '<th width="5%">Account No</th>';

                str += '<th width="5%">Commision_Amt</th>';

                str += '<th width="5%">Action</th>';

            str += '</tr>';

        str += '</thead>';

        str += '<tbody>';

            str += '<tr id="agent'+len+'">';
        
                str += '<td align="center" class="srNo" id="srNo'+len+'" rowspan>' + len + '</td>';
        
                str += '<td align="center" class="serviceid" id="serviceid'+len+'" style="display:none;">' + serviceid + '</td>';

                str += '<td align="center" class="servicename" id="servicename'+len+'" rowspan>' + servicename + '</td>';

                str+='<td>';

                    str+='<div class="form-group">';

                        str+='<select class="select-js form-control input-sm agentid" data-role="select" id="agentid'+len+'" data-name="agentid'+len+'" onchange="agentname(this)">';

                            str+='<option value="Select">Select Agent Name</option>';

                            <?php
                                                
                                $select = mysqli_query($con,"SELECT id,agentname FROM commissionmaster WHERE branchid ='$branchid'  ORDER BY agentname ");

                                while($rows = mysqli_fetch_assoc($select)){
                                                                        
                                echo "str+=";
                                    
                                    echo '"<option value=\''.$rows['id'].'\' >'.$rows['agentname'].'</option>"';
                                
                                echo ";";
            
                                } 
                                            
                            ?>
                                

                        str+='</select>';

                    str+='</div>';

                str+='</td>';

                str+='<td>';

                    str+='<div class="form-group">';

                        str+='<input type="text" id="agenttype'+len+'" data-name="agenttype'+len+'" data-role="text" class="form-control input-sm agenttypes" readonly>';
                    
                    str+='</div>';

                str+='</td>';

                str+='<td>';

                    str+='<div class="form-group">';

                        str+='<input type="number" id="contactno'+len+'" data-name="contactno'+len+'" data-role="number" class="form-control input-sm contactnos" readonly>';

                    str+='</div>';

                str+='</td>';

                str+='<td>';

                    str+='<div class="form-group">';

                        str+='<select class="form-control input-sm paidtype" data-role="select" id="paidtype'+len+'" data-name="paidtype'+len+'" onchange="paymentmode(this)">';

                            str+='<option value="Select">Payment Mode</option>';

                            str+='<option value="Cash">Cash</option>';
                
                            str+='<option value="Cheque">Cheque</option>';

                        str+='</select>';

                    str+='</div>';

                str+='</td>';

                str+='<td>';

                    str+='<div class="form-group">';

                        str+='<input type="text" id="bankname'+len+'" data-name="bankname'+len+'" data-role="text" value="-" class="form-control input-sm bankname" readonly>';

                    str+='</div>';
                                    
                str+='</td>';

                str+='<td>';

                    str+='<div class="form-group">';

                        str+='<input type="text" id="bankaddress'+len+'" data-name="bankaddress'+len+'" data-role="text" value="-" class="form-control input-sm bankaddress" readonly>';

                    str+='</div>';
                                    
                str+='</td>';

                str+='<td>';

                    str+='<div class="form-group">';

                        str+='<input type="text" id="ifsccode'+len+'" data-name="ifsccode'+len+'" data-role="text" class="form-control input-sm ifsccode" value="-" readonly>';

                    str+='</div>';
                                    
                str+='</td>';

                str+='<td>';

                    str+='<div class="form-group">';

                        str+='<input type="text" id="accountno'+len+'" data-name="accountno'+len+'" data-role="text" value="-" class="form-control input-sm accountno" readonly>';

                    str+='</div>';
                                    
                str+='</td>';


                str+='<td>';

                    str+='<div class="form-group">';

                        str+='<input type="number" id="commissionamount'+len+'" data-name="commissionamount'+len+'" data-role="number" class="form-control input-sm commissionamount">';

                    str+='</div>';
                                    
                str+='</td>';

                str+='<td>';

                    str+='<button class="btn btn-success btn-sm center-block" id="btn-agent'+len+'" data-tableSrNo='+len+'  data-editagent'+len+'="" onclick="addagent(this)">Add</button>';

                str+='</td>';
            
            str += '</tr>';

        str += '</tbody>';

    str += '<table>';

    $('#agentTable').append(str);

    $('.select-js').select2({

        width: '100%'

    });

    
}

function fun_adder_edit(len, serviceid, servicename, servicerate) {

    let str = '<td align="center" class="srNo">' + len + '</td>';
        
        str += '<td align="center" class="serviceid" style="display:none;">' + serviceid + '</td>';
    
        str += '<td align="center" class="servicename">' + servicename + '</td>';
        
        str += '<td align="center" class="servicerate">' + servicerate + '</td>';
        
        str += '<td align="center"><button class="btn btn-sm btn-success" onclick="contactEditor(' + len + ',this)">E</button>  <button class="btn btn-sm btn-danger remover" onclick="removers(' + len + ',this)">R</buuton></td>';

    $('#table-order .srNo').each(function () {
        
        let srNo = $(this).text().trim();
        
        if (srNo == len) {
        
            $(this).parent().html(str);
       
        }
   
    });

}


function fun_addAgentDetails_edit(len,serviceid,servicename) {

    $('tr#agent'+len).find('#serviceid'+len).text(serviceid);

    $('tr#agent'+len).find('#servicename'+len).text(servicename);

    $('#table-agent'+len+' > tbody > tr').each(function () {

        $(this).find('.serviceids').text(serviceid);
    
    });

}


function contactEditor(srNo, e) {

    let serviceid = $(e).parent().parent().find('.serviceid').text().trim();

    $('#serviceid').val(serviceid).trigger('change');
	
    $('#servicerate').val($(e).parent().parent().find('.servicerate').text().trim());

    $('#btn-add').attr('data-edit', srNo);

}

function removers(srNo,e) {

    $('tr#'+srNo).remove();

    $('#table-agent'+srNo).remove();

    removehidden();
}

//-----------------------Add Multiple Services End-------------------------//


function agentname(e) {

    let agentid   = $(e).val();

    let agentname = $(e).find(":selected").text();

    let edit      = $('#edit').val();

    if (!agentname) {

        agentname = '';

    }

    if (agentname != 'Select Agent Name' && agentname != '') {

        $.ajax({

            type: "POST",

            data: "agentid=" + agentid + "&agentname=" + agentname + "&edit=" + edit,

            url: 'api/agentdetails.php',

            cache: false,

            success: function (res) {

                if (res.status == 'success') {

                    $(e).parent().parent().parent().find('.agenttypes').val(res.json[0]['agenttype']);

                    $(e).parent().parent().parent().find('.contactnos').val(res.json[0]['contactno']);

                } else {

                    $(e).parent().parent().parent().find('.agenttypes').val(res.json[0]['agenttype']);

                    $(e).parent().parent().parent().find('.contactnos').val(res.json[0]['contactno']);

                }


            }

        })

    }

}



function paymentmode(e) {

    let agentid   = $(e).parent().parent().parent().find('.agentid').val();

    let agentname = $(e).parent().parent().parent().find('.agentid').find(":selected").text();

    let edit      = $('#edit').val();

    let paidtype  = $(e).val();

    if (agentname != 'Select Agent Name' && agentname != '' && paidtype !='Select' && paidtype !='Cash') {

        $.ajax({

            type: "POST",

            data: "agentid=" + agentid + "&agentname=" + agentname + "&edit=" + edit,

            url: 'api/bankdetails.php',

            cache: false,

            success: function (res) {

                if (res.status == 'success') {

                    $(e).parent().parent().parent().find('.bankname').val(res.json[0]['bankname']).attr("readonly", true);

                    $(e).parent().parent().parent().find('.bankaddress').val(res.json[0]['bankaddress']).attr("readonly", true);
                   
                    $(e).parent().parent().parent().find('.ifsccode').val(res.json[0]['ifsccode']).attr("readonly", true);
                   
                    $(e).parent().parent().parent().find('.accountno').val(res.json[0]['accountno']).attr("readonly", true);


                } else {

                    $(e).parent().parent().parent().find('.bankname').val(res.json[0]['bankname']).attr("readonly", true);

                    $(e).parent().parent().parent().find('.bankaddress').val(res.json[0]['bankaddress']).attr("readonly", true);
                   
                    $(e).parent().parent().parent().find('.ifsccode').val(res.json[0]['ifsccode']).attr("readonly", true);
                   
                    $(e).parent().parent().parent().find('.accountno').val(res.json[0]['accountno']).attr("readonly", true);

                }


            }

        })

    } else {

        $(e).parent().parent().parent().find('.bankname').val('-').attr("readonly", true);

        $(e).parent().parent().parent().find('.bankaddress').val('-').attr("readonly", true);
        
        $(e).parent().parent().parent().find('.ifsccode').val('-').attr("readonly", true);
        
        $(e).parent().parent().parent().find('.accountno').val('-').attr("readonly", true);

    }

}






//--------------------------Add Multiple Agent start-------------------------------------//

function addagent(e) {

    let tableSrNo        = $(e).attr('data-tableSrNo');

    let chk1             = $(e).attr('data-editagent'+tableSrNo);

    let data             = checker('table-agent'+tableSrNo);

    let agentid          = $('#agentid'+tableSrNo).val();

    let agentname        = $('#agentid'+tableSrNo).find(":selected").text();

    let agenttype        = $('#agenttype'+tableSrNo).val();
    
    let contactno        = $('#contactno'+tableSrNo).val();
    
    let paidtype         = $('#paidtype'+tableSrNo).val();

    let bankname         = $('#bankname'+tableSrNo).val();

    let bankaddress      = $('#bankaddress'+tableSrNo).val();

    let ifsccode         = $('#ifsccode'+tableSrNo).val();

    let accountno        = $('#accountno'+tableSrNo).val();

    let commissionamount = $('#commissionamount'+tableSrNo).val();

    let serviceid        = $(e).parent().parent().find('.serviceid').text();

    let valid            = true;

    if (checker('table-agent'+tableSrNo) != false) {

        valid = valid * true;

    } else {

        valid = valid * false;

    }

      $('#table-agent'+tableSrNo+' .agentname').each(function () {

        let agentname1 = $(this).text().trim();

        if (agentname1 == agentname && chk1 == '') {

            valid = valid * false;

            alert('Dublicate Service Name');

        }

    });


    if (valid) {

        let chk1 = $(e).attr('data-editagent'+tableSrNo);

        if (chk1 == '') {

            let len = $('#table-agent'+tableSrNo+' .srNos').length + 1;

            fun_adderagent(serviceid,tableSrNo,len, agentid, agentname, agenttype, contactno, paidtype, bankname, bankaddress, ifsccode, accountno, commissionamount);

        } else {

            fun_adder_edit_agent(serviceid,tableSrNo,chk1, agentid, agentname, agenttype, contactno, paidtype, bankname, bankaddress, ifsccode, accountno, commissionamount);

        }

        modals.clear('agent'+tableSrNo);

        $('#btn-agent'+tableSrNo).attr('data-editagent'+tableSrNo, '');

    }

}


function fun_adderagent(serviceid,tableSrNo,len, agentid, agentname, agenttype, contactno, paidtype, bankname, bankaddress, ifsccode, accountno, commissionamount) {

    let str = '<tr class="'+serviceid+'-'+agentid+'" id="'+tableSrNo+'">';

        str += '<td align="center" class="serviceids" style="display:none;">' + serviceid + '</td>';

        str += '<td align="center" class="srNos">' + len + '</td>';
        
        str += '<td align="center" class="agentid" style="display:none;">' + agentid + '</td>';
        
        str += '<td align="center" class="agentname">' + agentname + '</td>';
        
        str += '<td align="center" class="agenttype">' + agenttype + '</td>';
        
        str += '<td align="center" class="contactno">' + contactno + '</td>';
        
        str += '<td align="center" class="paidtype">' + paidtype + '</td>';
        
        str += '<td align="center" class="bankname">' + bankname + '</td>';
        
        str += '<td align="center" class="bankaddress">' + bankaddress + '</td>';
        
        str += '<td align="center" class="ifsccode">' + ifsccode + '</td>';
        
        str += '<td align="center" class="accountno">' + accountno + '</td>';
        
        str += '<td align="center" class="commissionamount">' + commissionamount + '</td>';
        
        str += '<td align="center"><button class="btn btn-sm btn-success" onclick="agentEditors(' + len + ',this,'+tableSrNo+') ">E</button>  <button class="btn btn-sm btn-danger remover" onclick="remover(this)">R</buuton></td>';
    
    str += '</tr>';

    $('#table-agent'+tableSrNo+' > tbody').append(str);

    //$('#table-agent'+tableSrNo+' tr#agent'+tableSrNo).after(str);


    $('tr#agent'+tableSrNo).find('#servicename'+tableSrNo+'').attr('rowspan',len+1);

}

function fun_adder_edit_agent(serviceid, tableSrNo, len, agentid, agentname, agenttype, contactno, paidtype, bankname, bankaddress, ifsccode, accountno, commissionamount) {

    let str = '<td align="center" class="serviceids" style="display:none;">' + serviceid + '</td>';

        str += '<td align="center" class="srNos">' + len + '</td>';
   
        str += '<td align="center" class="agentid" style="display:none;">' + agentid + '</td>';
        
        str += '<td align="center" class="agentname">' + agentname + '</td>';
        
        str += '<td align="center" class="agenttype">' + agenttype + '</td>';
        
        str += '<td align="center" class="contactno">' + contactno + '</td>';
        
        str += '<td align="center" class="paidtype">' + paidtype + '</td>';
        
        str += '<td align="center" class="bankname">' + bankname + '</td>';
        
        str += '<td align="center" class="bankaddress">' + bankaddress + '</td>';
        
        str += '<td align="center" class="ifsccode">' + ifsccode + '</td>';
        
        str += '<td align="center" class="accountno">' + accountno + '</td>';
        
        str += '<td align="center" class="commissionamount">' + commissionamount + '</td>';
        
        str += '<td align="center"><button class="btn btn-sm btn-success" onclick="agentEditors(' + len + ',this) ">E</button>  <button class="btn btn-sm btn-danger remover" onclick="remover(this)">R</buuton></td>';

    $('#table-agent'+tableSrNo+' .srNos').each(function () {
        
        let srNo = $(this).text().trim();
        
        if (srNo == len) {
        
            $(this).parent().html(str);
       
        }
   
    });


}

function agentEditors(srNo, e ,tableSrNo) {

    let agentid   = $(e).parent().parent().find('.agentid').text().trim();

    $('#agentid'+tableSrNo).val(agentid).trigger('change');

    $('#serviceid'+tableSrNo).val($(e).parent().parent().find('.serviceids').text().trim());

    $('#agenttype'+tableSrNo).val($(e).parent().parent().find('.agenttype').text().trim());

    $('#contactno'+tableSrNo).val($(e).parent().parent().find('.contactno').text().trim());

    $('#paidtype'+tableSrNo).val($(e).parent().parent().find('.paidtype').text().trim());
    
    $('#bankname'+tableSrNo).val($(e).parent().parent().find('.bankname').text().trim()).attr("readonly", true);
   
    $('#bankaddress'+tableSrNo).val($(e).parent().parent().find('.bankaddress').text().trim()).attr("readonly", true);
   
    $('#ifsccode'+tableSrNo).val($(e).parent().parent().find('.ifsccode').text().trim()).attr("readonly", true);
   
    $('#accountno'+tableSrNo).val($(e).parent().parent().find('.accountno').text().trim()).attr("readonly", true);

    $('#commissionamount'+tableSrNo).val($(e).parent().parent().find('.commissionamount').text().trim());

    $('#btn-agent'+tableSrNo).attr('data-editagent'+tableSrNo, srNo);

}

//-----------------------Add Multiple Agent End-------------------------// 


//-------------------------Remove Readonly Start ---------------------------//

function removehidden () {

    let tableOrderLenght = $('#table-order > tbody > tr').length - 1;

    if (tableOrderLenght == 0) {

        $('#category').prop('disabled', false);
        
    } else {

        $('#category').prop('disabled', 'disabled');

    }

}

//-------------------------Remove Readonly End ---------------------------//

//-------------------------Insert Start ---------------------------//

function submit() {

    let valid = true;

    if (checker('section') != false) {

        valid = valid * true;

    } else {

        valid = valid * false;

    }

    let arr = [];

    $('#table-order > tbody > tr').each(function () {

        let srNo         = $(this).find('.srNo').text().trim();

        let serviceid    = $(this).find('.serviceid').text().trim();
        
        let servicename  = $(this).find('.servicename').text().trim();
                
        let servicerate  = $(this).find('.servicerate').text().trim();
                
        arr.push({

            "srno": srNo,

            "serviceid": serviceid,

            "servicename": servicename,

            "servicerate": servicerate
            
        });

    })

    arr.shift();

    if (arr == '') {

        $('#table-order').css('border', '2px solid red');

        valid = valid * false;

    } else {

        $('#table-order').css('border', '2px solid green');

        valid = valid * true;

    }

    let tableLenght = $('.table-list').length-1;

    let arr1        = [];

    for (let i = 1; i <= tableLenght; i++) {
        
        $('#table-agent'+i+' > tbody > tr').each(function () {

            let serviceids       = $(this).find('.serviceids').text().trim();

            let srNo             = $(this).find('.srNos').text().trim();

            let agentid          = $(this).find('.agentid').text().trim();
        
            let agentname        = $(this).find('.agentname').text().trim();
        
            let agenttype        = $(this).find('.agenttype').text().trim();
        
            let contactno        = $(this).find('.contactno').text().trim();
        
            let paidtype         = $(this).find('.paidtype').text().trim();
        
            let bankname         = $(this).find('.bankname').text().trim();
        
            let bankaddress      = $(this).find('.bankaddress').text().trim();
        
            let ifsccode         = $(this).find('.ifsccode').text().trim();
        
            let accountno        = $(this).find('.accountno').text().trim();
        
            let commissionamount = $(this).find('.commissionamount').text().trim();
        
            arr1.push({

                "serviceid":serviceids,

                "srno": srNo,

                "agentid": agentid,

                "agentname": agentname,

                "agenttype": agenttype,

                "contactno": contactno,

                "paidtype": paidtype,

                "bankname": bankname,

                "bankaddress": bankaddress,

                "ifsccode": ifsccode,

                "accountno": accountno,

                "commissionamount": commissionamount

            });
                

        })
        
    }
    
    arr1.shift();

    // if (arr1 == '') {

    //     $('#table-order').css('border', '2px solid red');

    //     valid = valid * false;

    // } else {

    //     $('#table-order').css('border', '2px solid green');

    //     valid = valid * true;

    // }

    if (valid) {

        let data1          = checker('section');
           
            data1          = JSON.stringify(data1);

            serviceDetails = JSON.stringify(arr);

            agentDetails   = JSON.stringify(arr1);

        $.ajax({
            type: "POST",
            
            data: {

                data: data1,

                serviceDetails: serviceDetails,

                agentDetails: agentDetails,

            },

            url: 'insert.php',
            
            cache: false,
            
            success: function (res) {
            
                if (res.status == 'success') {
            
                    swal({
                        
                        type: 'success',
                       
                        title: 'Commission Add',
                       
                        showConfirmButton: false,
                       
                        timer: 3000
                    
                    });
                   
                    setTimeout(function () {
                        
                        location.href = 'index.php';
                    
                    }, 3000);
                
                }

                if (res.status == 'falid1') {

                    swal('conatct No Or Email Id Duplicate', '', 'error');
                   
                    $('#inp-phone').css('border-bottom', '1px solid red');

                }

            }
        
        });
    
    }

}


if ($('#commissionid').val() != '' && $('#edit').val() == 'true') {

    $.ajax({

        type: "POST",

        data: "commissionid=" + $('#commissionid').val(),

        url: 'select.php',

        cache: false,

        success: function (res) {

            if (res.status == 'success') {

                $("#customerid").val(res.customerid).trigger('change');

                $('#category').val(res.category).trigger('change');

                $('#btn-submit').attr('onclick', 'update()');

                $('#btn-submit').text('Update');

                var serviceJson = res.json1;

                var couter = 1;

                for (var i in serviceJson) {

                    fun_adder(couter, serviceJson[i].serviceid, serviceJson[i].servicename, serviceJson[i].servicerate);

                    fun_addAgentDetails(couter,serviceJson[i].serviceid,serviceJson[i].servicename);

                    couter++;

                }

                var agentJson = res.json;

                var serial = 1;

                for (var i in agentJson) {
                    
                    fun_adderagent(agentJson[i].serviceid,agentJson[i].tableSrNo,agentJson[i].count, agentJson[i].agentid, agentJson[i].agentname, agentJson[i].agenttype, agentJson[i].contactno, agentJson[i].paidtype, agentJson[i].bankname, agentJson[i].bankaddress, agentJson[i].ifsccode, agentJson[i].accountno, agentJson[i].commissionamount);

                    serial++;

                }

            }

        }

    });

}

//--------------------------Edit Function End----------------------//

function update() {

        let valid = true;

    if (checker('section') != false) {

        valid = valid * true;

    } else {

        valid = valid * false;

    }

    let arr = [];

    $('#table-order > tbody > tr').each(function () {

        let srNo         = $(this).find('.srNo').text().trim();

        let serviceid    = $(this).find('.serviceid').text().trim();
        
        let servicename  = $(this).find('.servicename').text().trim();
                
        let servicerate  = $(this).find('.servicerate').text().trim();
                
        arr.push({

            "srno": srNo,

            "serviceid": serviceid,

            "servicename": servicename,

            "servicerate": servicerate
            
        });

    })

    arr.shift();

    if (arr == '') {

        $('#table-order').css('border', '2px solid red');

        valid = valid * false;

    } else {

        $('#table-order').css('border', '2px solid green');

        valid = valid * true;

    }

    let tableLenght = $('.table-list').length-1;

    let arr1        = [];

    for (let i = 1; i <= tableLenght; i++) {
        
        $('#table-agent'+i+' > tbody > tr').each(function () {

            let serviceids       = $(this).find('.serviceids').text().trim();

            let srNo             = $(this).find('.srNos').text().trim();

            let agentid          = $(this).find('.agentid').text().trim();
        
            let agentname        = $(this).find('.agentname').text().trim();
        
            let agenttype        = $(this).find('.agenttype').text().trim();
        
            let contactno        = $(this).find('.contactno').text().trim();
        
            let paidtype         = $(this).find('.paidtype').text().trim();
        
            let bankname         = $(this).find('.bankname').text().trim();
        
            let bankaddress      = $(this).find('.bankaddress').text().trim();
        
            let ifsccode         = $(this).find('.ifsccode').text().trim();
        
            let accountno        = $(this).find('.accountno').text().trim();
        
            let commissionamount = $(this).find('.commissionamount').text().trim();
        
            arr1.push({

                "serviceid":serviceids,

                "srno": srNo,

                "agentid": agentid,

                "agentname": agentname,

                "agenttype": agenttype,

                "contactno": contactno,

                "paidtype": paidtype,

                "bankname": bankname,

                "bankaddress": bankaddress,

                "ifsccode": ifsccode,

                "accountno": accountno,

                "commissionamount": commissionamount

            });
                

        })
        
    }
    
    arr1.shift();

    // if (arr1 == '') {

    //     $('#table-order').css('border', '2px solid red');

    //     valid = valid * false;

    // } else {

    //     $('#table-order').css('border', '2px solid green');

    //     valid = valid * true;

    // }

    if (valid) {

        let data1          = checker('section');
           
            data1          = JSON.stringify(data1);

            serviceDetails = JSON.stringify(arr);

            agentDetails   = JSON.stringify(arr1);

        $.ajax({
            type: "POST",
            
            data: {

                data: data1,

                serviceDetails: serviceDetails,

                agentDetails: agentDetails,

            },

            url: 'update.php',
            
            cache: false,
            
            success: function (res) {
            
                if (res.status == 'success') {
            
                    swal({
                        
                        type: 'success',
                       
                        title: 'Commission Update',
                       
                        showConfirmButton: false,
                       
                        timer: 3000
                    
                    });
                   
                    setTimeout(function () {

                        if ($('#send').val()=='approval') {

                            location.href = 'approval.php';
                            
                        } else {

                            location.href = 'index.php';

                        }
                        
                    
                    }, 3000);
                
                }

                if (res.status == 'falid1') {

                    swal('conatct No Or Email Id Duplicate', '', 'error');
                   
                    $('#inp-phone').css('border-bottom', '1px solid red');

                }

            }
        
        });
    
    }

}

$('#btn-reset').on('click', function () {
    window.location.reload();
});

$(document).ready(function () {

    $('.select-js').select2({

        width: '100%'

    });

    $('.select').attr('style', 'width:100%!important;');

});

</script>