<?php 

  $base = '../../../';

  $navenq5 = 'background:#1B1464;';
  
  include('header.php');

  if (!in_array("Bill Paid", $arrysession)) {
   
    echo "<script>window.location.href='/process/dashboard.php';</script>";
    
    exit;

  }

  if (session_status() == PHP_SESSION_NONE) { session_start(); }
  
  $sessionby = $_SESSION['employeeid'];
  
  $branchid  = $_SESSION['branchid'];

?>

<br>
<style>

hr.style-hr {
    
        border: 0;
        height: 1px;
        background-image: linear-gradient(to right, rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.75), rgba(0, 0, 0, 0));

    }

.th {
        background: #130f40;
        color: #fff;
        text-align: center;
        padding-top: 2px;
        padding-bottom: 2px;
        border: 1px solid #fff;
    }

    .td {
        border: 1px solid #ddd;
    }

    .table-ui .btn {
        margin: 3px;
    }

    #myModal1 .col-sm-4,
    .col-sm-8 {
        margin-bottom: 2px;
    }

    .picker__select--year {
        height: auto;
    }

    .picker__select--month {
        height: auto;
    }

    .table-list td,
    .table-list th {
        border: 1px solid #ddd;
        padding: 1px !important;
        font-size: 13px;
    }

    .table-list td {
        padding-top: 10px !important;
    }

    .table-list tr:nth-child(even) {
        background-color: #f2f2f2;
    }

    .table-list th {
        padding-top: 5px;
        padding-bottom: 5px;
        text-align: center;
        background-color: #16a085;
        color: white;
    }

</style>

<img class="hidden" id=imageid src="../../../../img/PestMortemLogo.jpg">

<div class="container-fluid" id="section">

    <h2 align="center" style="margin-top:0px;">Payment</h2>
    
    <div style="clear:both;"></div>
        
    <hr class="style-hr">

    <div class="row">

        <div class="col-sm-6" id="section">

            <input type="hidden" id="inp-branchId" data-name="branchid" value="<?php echo $branchid;?>">

            <input type="hidden" id="inp-paidid" data-name="paidid" value="<?php echo $_GET['paidid'];?>">

            <input type="hidden" id="inp-edit" value="<?php echo $_GET['edit'];?>">


            <div class="col-sm-12">

                <b>Select Agent Name </b>

                <div class="form-group">

                    <select class="select-js form-control input-sm agentid" data-role="select" id="agentid" data-name="agentid" onchange="commissionDetails(this)">;

                        <option value="Select">Select Agent Name</option>

                        <?php
                                                    
                            $select = mysqli_query($con,"SELECT id,agentname FROM commissionmaster WHERE branchid ='$branchid'  ORDER BY agentname ");

                            while($rows = mysqli_fetch_assoc($select)){
                                                                                                                
                                echo '<option value='.$rows['id'].' >'.$rows['agentname'].'</option>"';
                                                
                            } 
                                                
                        ?>
                                    

                    </select>  

                </div>

            </div>

            <div class="col-sm-12">

                <b>Amount </b>

                <div class="form-group">

                    <input type="number" class="form-control input-sm agentamount" data-role="number" data-name="agentamount" id="agentAmount" readonly>

                </div>

            </div>

            <div class="col-sm-12">

                <b>Date </b>

                <div class="form-group">

                    <input type="date" class="form-control input-sm" data-role="date" data-name="paymentdate" id="agentDate">

                </div>

            </div>

            <div class="col-sm-12">

                <b>Description </b>

                <div class="form-group">

                    <textarea type="text" class="form-control input-sm" data-name="paymentdescription" data-role="text" id="description"></textarea>

                </div>

            </div>

            <div class="col-sm-12">

                <button class="btn btn-success btn-sm btn-block" id="btn-submit" onclick="submit()">Submit</button>

            </div>

        </div>

        <div class="col-sm-6">

            <div class="table-responsive" id="serviceTable">

                <table class="table table-list form-group" id="table-order">

                    <thead>

                        <th width="2%">#</th>

                        <th width="20%">Bill No</th>

                        <th width="20%">Commission Amount</th>
                                            
                        <th width="5%">Action</th>

                    </thead>

                    <tbody>
                
                    </tbody>
                    
                <table>

            </div>

        
        </div>

    </div>
    
</div>


<?php 

  include($base.'_in/footer.php');

?>

<script>


function commissionDetails(e) {
    
    let agentId = $(e).val();

    if (agentId != 'Select') {

         $.ajax({

            type: "POST",
            
            data: "agentId=" + agentId + "&branchId=" + $('#inp-branchId').val() + "&paidid=" + $('#inp-paidid').val() + "&edit="+$('#inp-edit').val(),

            url: 'api/agentcommissiondetails.php',
            
            cache: false,
            
            success: function (res) {
            
                if (res.status == 'success') {
            
                    let json = res.json;

                    let str = '';

                    let count = 1;

                    let sum   = 0;

                    for (let i in json) {

                        if (json[i].checkradio == 'checked') {

                            sum = sum + parseFloat(json[i].commissionamount);

                        }


                        str+='<tr>';

                            str+='<td class="srNo">'+count+'</td>';

                            str+='<td class="billno">'+json[i].billno+'</td>';

                            str+='<td class="commissionid" style="display:none;">'+json[i].commissionid+'</td>';

                            str+='<td class="commissionamount">'+json[i].commissionamount+'</td>';

                            str+='<td class="checkboxs"><div class="checkbox text-center"><label><input type="checkbox" value="'+json[i].commissionamount+'" '+json[i].checkradio+' onclick="putValue(this)" class="commissionAmountCheck"></label></div></td>';

                        str+='</tr>';
                        
                        count++;
                    }

                    $('#agentAmount').val(sum);

                    $('#table-order > tbody').html(str);

                } else {

                    $('#table-order > tbody').html('');

                    $('#agentAmount').val('');

                }

            }
        
        });
        
    } else {

        $('#table-order > tbody').html('');

        $('#agentAmount').val('');


    }

}

function putValue(e) {

    let commissionAMount = parseFloat($(e).val());

    let agentAmount      = parseFloat($('#agentAmount').val());

    let sum              = 0;

    if(e.checked) {

        sum = agentAmount + commissionAMount;

    } else {

        sum = agentAmount - commissionAMount;

    }

    $('#agentAmount').val(sum);
    
}

function submit(){

    let valid = true;

    if (checker('section') != false) {

        valid = valid * true;

    } else {

        valid = valid * false;

    }

    let arr = [];

    $("#table-order > tbody > tr").each(function () {

    let srNo             = $(this).find('.srNo').text().trim();

    let billno           = $(this).find('.billno').text().trim();

    let commissionid     = $(this).find('.commissionid').text().trim();

    let commissionamount = $(this).find('.commissionamount').text().trim();

    let check = $(this).find('.checkboxs input:checkbox').is(':checked');

        if (check == true) {

            arr.push({

                "srno": srNo,

                "billno": billno,

                "commissionid":commissionid,

                "commissionamount":commissionamount,

                "check":check

            });
            
        }

   

  })

    if (valid) {

        var data1 = checker('section');
           
        data1 = JSON.stringify(data1);

        paymentDetails = JSON.stringify(arr);


        $.ajax({

            type: "POST",
            
            data: {

                data: data1,

                paymentDetails : paymentDetails,

            },

            url: 'api/paidagentcommission.php',
            
            cache: false,
            
            success: function (res) {
            
                if (res.status == 'success') {
                    
                    swal({
                        
                        type: 'success',
                       
                        title: 'Commission Paid',
                       
                        showConfirmButton: false,
                       
                        timer: 3000
                    
                    });
                   
                    setTimeout(function () {

                        location.href = 'paymentdisplay.php';
                      
                    }, 3000);
                
                }

            }
        
        });
    
    }

}


if ($('#inp-paidid').val() != '' && $('#inp-edit').val() == 'true') {

    $.ajax({

        type: "POST",
            
        data: "paidid=" + $('#inp-paidid').val() + "&edit=" + $('#inp-edit').val() + "&branchId=" + $('#inp-branchId').val(),

        url: 'api/paidagentcommissionselect.php',
            
        cache: false,
            
        success: function (res) {
            
            if (res.status == 'success') {
                    
                modals.putvalue('section', res.json);

                $('#btn-submit').text('Update');

                $('#btn-submit').attr('onclick', 'update()');

            }

        }
        
    });
    
}


function update(){

    let valid = true;

    if (checker('section') != false) {

        valid = valid * true;

    } else {

        valid = valid * false;

    }

    let arr = [];

    $("#table-order > tbody > tr").each(function () {

        let srNo             = $(this).find('.srNo').text().trim();

        let billno           = $(this).find('.billno').text().trim();

        let commissionid     = $(this).find('.commissionid').text().trim();

        let commissionamount = $(this).find('.commissionamount').text().trim();

        let check = $(this).find('.checkboxs input:checkbox').is(':checked');

        if (check == true) {

            arr.push({

                "srno": srNo,

                "billno": billno,

                "commissionid":commissionid,

                "commissionamount":commissionamount,

                "check":check

            });
            
        }

    })

    if (valid) {

        var data1 = checker('section');
           
        data1 = JSON.stringify(data1);

        paymentDetails = JSON.stringify(arr);

        $.ajax({

            type: "POST",
            
            data: {

                data: data1,

                paymentDetails : paymentDetails,

            },

            url: 'api/paidagentcommissionupdate.php',
            
            cache: false,
            
            success: function (res) {
            
                if (res.status == 'success') {
                    
                    swal({
                        
                        type: 'success',
                       
                        title: 'Commission Paid',
                       
                        showConfirmButton: false,
                       
                        timer: 3000
                    
                    });
                   
                    setTimeout(function () {

                        location.href = 'paymentdisplay.php';
                      
                    }, 3000);
                
                }

            }
        
        });
    
    }

}

$(document).ready(function() {

  $('.select-js').select2({width: '100%', tags: true});

  $('.select').attr('style','width:100%!important;');

});

</script>