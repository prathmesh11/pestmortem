<?php 

  $base = '../../../';

  $navenq5 = 'background:#1B1464;';
  
  include('header.php');

  if (!in_array("Bill Paid", $arrysession)) {
   
    echo "<script>window.location.href='/process/dashboard.php';</script>";
    
    exit;

  }

  if (session_status() == PHP_SESSION_NONE) { session_start(); }
  
  $sessionby = $_SESSION['employeeid'];
  
  $branchid  = $_SESSION['branchid'];

?>
<br>
<style>

.th {
        background: #8e44ad;
        color: #fff;
        text-align: center;
        padding-top: 2px;
        padding-bottom: 2px;
        border: 1px solid #fff;
    }

    .td {
        border: 1px solid #ddd;
    }

    .table-ui .btn {
        margin: 3px;
    }

    #myModal1 .col-sm-4,
    .col-sm-8 {
        margin-bottom: 2px;
    }

    .picker__select--year {
        height: auto;
    }

    .picker__select--month {
        height: auto;
    }

    .table-list td,
    .table-list th {
        border: 1px solid #ddd;
        padding: 1px !important;
        font-size: 13px;
    }

    .table-list td {
        padding-top: 10px !important;
    }

    .table-list tr:nth-child(even) {
        background-color: #f2f2f2;
    }

    .table-list th {
        padding-top: 5px;
        padding-bottom: 5px;
        text-align: center;
        background-color: #16a085;
        color: white;
    }

</style>

<img class="hidden" id=imageid src="../../../../img/PestMortemLogo.jpg">

<div class="container-fluid">


    <br>

    <div class="table-ui container-fluid">

      <div class="tr row">

        <div class="col-sm-2 th">Agent Name</div>

        <div class="col-sm-4 th">Payment Details</div>

        <div class="col-sm-4 th">Agent Details</div>

        <div class="col-sm-2 th">Action</div>

      </div>

      <?php
        
        $result=mysqli_query($con,"SELECT DISTINCTROW paidid,agentid,agentamount,paymentdate,paymentdescription FROM `commissionpaid` WHERE branchid='$branchid' AND confirm_commssion_paid_time = 0 ORDER BY id DESC");
          
        while($rows = mysqli_fetch_assoc($result)){

          $paidid       = $rows['paidid'];

          $agentid      = $rows['agentid'];

          $agentname    = mysqli_fetch_assoc(mysqli_query($con,"SELECT agentname x FROM commissionmaster WHERE id = '$agentid' "))['x'];

      ?>

      <div class="row tr">
          
        <div class="col-sm-2 td">

          <?php 
            
            echo $agentname;
            
          ?>

        </div>

        <div class="col-sm-4 td" style="word-wrap:break-word;">

          <?php
          
            echo 'Total Amount Paid : ' .$rows['agentamount'];
           
            echo '<br>';

            echo 'Payment Date : ' .date("d-m-Y",strtotime($rows['paymentdate']));

            echo '<br>';

            echo 'Payment Description : ' .$rows['paymentdescription'];

          ?>

        </div>

        <div class="col-sm-4 td" style="word-wrap:break-word;">

          <?php
          
            echo '<table class="table table-list form-group">';
            
              echo '<thead>';
                  
                echo '<tr>';
                      
                  echo '<th width="300">Bill No</th>';

                  echo '<th width="300">Commission Amount</th>';
                                                  
                echo '</tr>';
              
              echo '</thead>';
              
              echo '<tbody>';

              $total = 0;

              $result1 = mysqli_query($con,"SELECT * FROM commissionpaid WHERE paidid='$paidid' AND branchid='$branchid' ");

              while($rowsss=mysqli_fetch_assoc($result1)){
               
                echo '<tr>';
                                
                  echo '<td>'.$rowsss['billno'].'</td>';

                  echo '<td>'.$rowsss['commissionamount'].'</td>';

                  $total+=$rowsss['commissionamount'];

                echo '</tr>';
              
              }

              echo '</tbody>';

            echo '</table>';

            echo 'Total Commission Amount : '.$total;


          ?>

        </div>

        <div class="col-sm-2 td">

          <a href="/process/commission-master/commssion-management/payment.php?paidid=<?php echo $paidid;?>&edit=true" class="btn btn-info btn-sm btn-block">Edit</a>

          <button data-paidid="<?php echo $paidid;?>" data-branchid = "<?php echo $branchid;?>"  class="btn btn-success btn-sm btn-block" onclick="confirm(this)">Confirm</button>
          
        </div>
        
      </div>

      <?php } ?>

    </div>

  <!-- </div> -->
    
</div>

<a class="btn show" style="font-size:20px;border-radius:50px;width:60px;height:60px;position: fixed;
    bottom: 40px;
    right: 40px;background:#eb2f06;color:#fff;" title="Show"  href="/process/commission-master/commssion-management/payment.php">
    
    <i class="fa fa-plus" id="plusSign" aria-hidden="true" style="padding-top:12px;"></i>

</a>

<?php 

  include($base.'_in/footer.php');

?>

<script>

function confirm(e) {

  let paidid   = $(e).attr('data-paidid');

  let branchid = $(e).attr('data-branchid');

  if (paidid != '' && branchid != '') {

    $.ajax({

      type: "POST",

      data: "paidid=" + paidid + "&branchid=" + branchid,

      url: 'api/commissionpaidconfirm.php',

      cache: false,

      success: function (res) {

        if (res.status == 'success') {

          swal({
              
            type: 'success',
            
            title: 'Commission Paid',
            
            showConfirmButton: false,
            
            timer: 3000
          
          });
          
          setTimeout(function () {
              
            location.href = 'paid.php';
          
          }, 3000);

        }

      }

    })

  } else {

    alert('Somthing Went Wrong');

  }

}

</script>