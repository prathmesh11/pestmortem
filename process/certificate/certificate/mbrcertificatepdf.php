<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Print Sample</title>
    <script src="../../../../../js/jquery-3.3.1.min.js"></script>
    <script src="../../../../../js/pdfmake.min.js"></script>
    <script src="../../../../../js/vfs_fonts.js"></script>
</head>
<body>

</body>
</html>
<script>
printer();
function printer() {
var fullDate = new Date()
var twoDigitMonth = ((fullDate.getMonth().length + 1) === 1) ? (fullDate.getMonth() + 1) : '' + (fullDate.getMonth() + 1);
var currentDate = fullDate.getDate() + "/" + twoDigitMonth + "/" + fullDate.getFullYear();
var content=[];
 
                 dd = {
                        

                    pageSize: 'A4',
                    pageMargins: [40, 20, 20, 40],
                    footer: function (currentPage, pageCount) {
                    return {
                        margin: 10,
                        columns: [{
                            fontSize: 8,
                            text: [{
                                    text: '--------------------------------------------------------------------------' +
                                        '\n',
                                    margin: [0, 20]
                                },
                                {
                                    text: '©Pest Mortem (India) PVT. LTD. || Printed On ' +currentDate ,
                                }
                            ],
                            alignment: 'center'
                        }]
                    };

                },
                    content: [
                            {
                                text: 'PEST MORTEM (INDIA) PRIVATE LIMITED',
                                style: 'header'
                            },
                            {
                                    text: '(Approved by National Plant Protection Organization, Government of India)',
                                    style: 'subheader'
                                },

                                '\n',
                                 {
                                    text: 'FUMIGATION CERTIFICATE',
                                    style: 'subheader1'
                                },
                                '\n',
                                {
                                    style: 'tableExample',
                                    table: {
                                         widths: [100, '*', 100, '*'],
                                   
                                        body: [
                                            ['Sr. No. :', '0072', 'Certificate Number:','PMI/MUM/ALP/722/18-19'],
                                            ['Dte. PPQS Regd. NO.:', '030/ALP', 'Date of Issue :','03/12/2018']
                                        ]
                                    },
                                        layout: 'noBorders'
                                },
                                 {
                                    text: ' This is to certify that following regulated articles have been fumigated according to the appropriate procedures to conform to the current phytosanitary regulations of the importing country.',
                                    style: 'declr'
                                },
                                {
                                    text: 'Description of Goods',
                                    style: 'subheader2'
                                },
                                 '\n',
                                
                                {
                                    style: 'tableExample1',
                                    table: {
                                         widths: [100, '*' ],
                                   
                                        body: [
                                          ['Description of goods', ': Parts & accesories for electricity Meter'],
                                            
                                          ['Quantity declared', ': 001 Wooden Pallet \n  Net weight 849.650 kgs'],
                                          ['Distinguishing marks', ': P M L made in india  '],
                                           
                                          ['Container No.', ': LCL'],
                                          ['Port & country Of Loading', ': Nhava Sheva, India'],
                                          ['Country Of Destination', ': Charleston'],
                                           
                                          ['Name and addres of exporter', ': Ruchi Soya Limited \n : Ruchi House, Royal Palms, Survey No. 169, Aarey Milk Colony,Goregoan (east),Mumbai-400065, INDIA'],
                                            ['Name and addres of consignee', ': 1. Mega Feed Industrial Co. Ltd., Ground Floor, \n : Both=ahtayung township,Myanmar '],
                                            
                                            
                                        ]
                                    },
                                        layout: 'noBorders'
                                },
                                '\n',
                                {
                                    text: 'Details of Treatment',
                                    style: 'subheader2'
                                },
                                {
                                    style: 'tableExample1',
                                    table: {
                                         widths: [100, '*', 100, '*'],
                                   
                                        body: [
                                            ['Name of fumigant', ': ALUMINIUM PHOSPHIDE', 'Date of fumigation',': 23/11/2018'],
                                            ['Place of fumigation', ': NAVKAR,Panvel', 'Duration of fumigant',': 24 Hours'],
                                            ['Dosage of fumigation', ': 48 gms/m3', 'Mini. Air temp(Deg. Cent.)',': 21'], 
                                               
                                        ]
                                    },
                                        layout: 'noBorders'
                                },
                               
                                  {
                                    style: 'tableExample1',
                                    table: {
                                         widths: [400, '*' ],
                                   
                                        body: [
                                            ['Fumigation performed in under gas tight sheet','NA'],
                                            ['Container pressure test conducted','YES'],
                                            ['Container has 200mm. free air space at the top of container','NA'],
                                            ['In transit fumigation-Needs ventilation at port of discharge','NA'],
                                            ['Enclosure has been ventilated to below 5 ppm v/v Methyl Bromide','YES'],
                                             
                                               
                                        ]
                                    },
                                        layout: 'noBorders'
                                },
                                 {
                                    text: 'Wrapping and Timber',
                                    style: 'subheader2'
                                },
                                {
                                    style: 'tableExample1',
                                    table: {
                                         widths: [400, '*' ],
                                   
                                        body: [
                                            ['Has the commodify been fumigated prior to lacquering, vanishing, painting or wrapping','NA'],
                                            ['Has plastic wrapping been used in the consignment ?','YES'],
                                            ['* If yes, has the Consignment been fumigated prior to wrapping ?','NA'],
                                            [' * Or has the plastic wrapping been slashed, opened, or perforated in accordance with the wrapping and perforation Standard ?','NA'],
                                            ['Is the Timber in this consignment less than 200mm thick in one \n dimension and correctly spaced every 200mm in height?','YES'],
                                             
                                               
                                        ]
                                    },
                                        layout: 'noBorders'
                                },
                                {
                                    text: 'Additional Declaration',
                                    style: 'subheader2'
                                },
                                '\n',
                                {text: 'FUMIGATION DONE AS PER ISPM-15 COMPLIANCE WITH IPPC LOGO.\N INVOICE NO. : HE/0421018-19 DT. 25/03/2019', style: 'para1'},
                                '\n',
                                
                                {
                                    text: ' I declare that these details are true & correct and the fumigation has been carried out in accordance with the  NSPM-12.',
                                    style: 'declr'
                                },
                                '\n','\n',
                                 {
                                    style: 'tableExample1',
                                    table: {
                                         widths: [300, '*' ],
                                   
                                        body: [
                                            ['Endorsed by Specified Officer of Dte. of','Name & sign of Accerdited Fumigation'],
                                            ['PPQS Name: sign & Office Seal','Operator with seal & date/Accerditation Number'],
                                            
                                            ]
                                       },
                                       layout: 'noBorders'
                                    },    

                            ],
                    styles: {
                            header: {
                            fontSize: 16,
                            bold: true,
                            alignment: 'center',
                          },
                            subheader: {
                            fontSize: 12,
                            bold: true,
                            alignment: 'center',
                          }, 
                          subheader1: {
                            fontSize: 14,
                            bold: true,
                            alignment: 'center',
                          },
                           subheader2: {
                            fontSize: 10,
                            bold: true,
                            
                          },
                           para1: {
                            fontSize: 8,
                             }, 
                          declr: {
                            fontSize: 10,
                            italics: true,
                            
                          },
                            tableExample: {
                                border : 0,
                                bold: true,
                                 fontSize: 10,
                                margin: [0, 5, 0, 15],
                            },
                             tableExample1: { 
                                bold: true,
                                 fontSize: 8,
                                margin: [0, 5, 0, 15],
                            },
                            tableHeader: {
                                bold: true,
                                fontSize: 13,
                                color: 'black',
                            },
                        tablfont: {
                            fontSize: 10
                        }
                    }
                }

                pdfMake.createPdf(dd).download('testprint.pdf');
            //  pdfMake.createPdf(dd).open();
            
     
}
</script>