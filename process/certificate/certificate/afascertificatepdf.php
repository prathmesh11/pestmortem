<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Print Sample</title>
    <script src="../../../../../js/jquery-3.3.1.min.js"></script>
    <script src="../../../../../js/pdfmake.min.js"></script>
    <script src="../../../../../js/vfs_fonts.js"></script>
</head>
<body>

</body>
</html>
<script>
printer();
function printer() {
var fullDate = new Date()
var twoDigitMonth = ((fullDate.getMonth().length + 1) === 1) ? (fullDate.getMonth() + 1) : '' + (fullDate.getMonth() + 1);
var currentDate = fullDate.getDate() + "/" + twoDigitMonth + "/" + fullDate.getFullYear();
var content=[];
 
                 dd = {
                        

                    pageSize: 'A4',
                    pageMargins: [40, 20, 20, 40],
                    footer: function (currentPage, pageCount) {
                    return {
                        margin: 10,
                        columns: [{
                            fontSize: 8,
                            text: [{
                                    text: '--------------------------------------------------------------------------' +
                                        '\n',
                                    margin: [0, 20]
                                },
                                {
                                    text: '©Pest Mortem (India) PVT. LTD. || Printed On ' +currentDate ,
                                }
                            ],
                            alignment: 'center'
                        }]
                    };

                },
                    content: [
                            {
                                text: 'PEST MORTEM (INDIA) PRIVATE LIMITED',
                                style: 'header'
                            },
                            {
                                    text: '(Approved by National Plant Protection Organization, Government of India)',
                                    style: 'subheader'
                                },

                                '\n',
                                 {
                                    text: 'AFAS-METHYL BROMIDE FUMIGATION CERTIFICATE',
                                    style: 'subheader1'
                                },
                                '\n',
                                {
                                    style: 'tableExample',
                                    table: {
                                         widths: [100, '*', 100, '*'],
                                   
                                        body: [
                                            ['Sr. No. :', '0072', 'Certificate Number:','PMI/MUM/ALP/722/18-19'],
                                            ['Dte. PPQS Regd. NO.:', '030/ALP', 'Date of Issue :','03/12/2018']
                                        ]
                                    },
                                        layout: 'noBorders'
                                },
                                  
                                {
                                    text: 'TARGET OF FUMIGATION DETAILS ',
                                    style: 'subheader3'
                                },
                                 '\n',
                                
                                {
                                    style: 'tableExample1',
                                    table: {
                                         widths: [100, '*' ,100,'*'],
                                   
                                        body: [
                                          ['Target of Fumigation', ': Commodify','',''],
                                            
                                          ['Commodify', ': Empty Container','Quantity',':One'],
                                          ['Consignment Link', ':12000027,028,032 & 150000008 ','Port of Loading','JNPT,India'],
                                         
                                          ['Country Of Origin', ': Nhava Sheva, India','Country Of Destination', ': Austrelia'],
                                           
                                            
                                        ]
                                    },
                                        layout: 'noBorders'
                                },
                                 
                                 {
                                    style: 'tableExample1',
                                    table: {
                                         widths: [300, '*'],
                                   
                                        body: [
                                            [{text: 'Name and address of exporter', style: 'tableHeader'}, {text: 'Name and address of importer', style: 'tableHeader'}],
                  
                                          ['M/S. PIX TRANSMISSION LIMITED \n PALS BUILDING, TPS IV,1ST ROAD,\n BANDRA(W),MUMBAI - 400050', 'M/S. FINER POWER TRANSMISSION P/I,\n 120,WILLIAMS ROAD,DANDENONG,AUSTRALIA'],
                                         
                                            
                                        ]
                                    },
                                        layout: 'noBorders'
                                },
                                
                                {
                                    text: 'TREATMENT DETAILS',
                                    style: 'subheader3'
                                },
                                {
                                    style: 'tableExample1',
                                    table: {
                                         widths: [100, '*', 100, '*'],
                                   
                                        body: [
                                            ['Date of fumigation', ': 22/04/2014', 'Place of fumigation',': NAGPUR'],
                                            ['DAFF Prescribes dose rate', ': 32', 'Exposure Period',': 24 Hours'],
                                            ['Mini. Air temp(Deg. Cent.)',': 26','Applied dose rate', ': 32' ], 
                                               
                                        ]
                                    },
                                        layout: 'noBorders'
                                },
                                 '\n',
                                {
                                    style: 'tableExample1',
                                    table: {
                                         widths: [200, '*'],
                                   
                                        body: [
                                            ['How was the fumigation conducted', ': Sheetd Containers'],
                                            ['Container number (where applicable)', ': 01x20 EMPTY CONTAINER NO. CMAU-0481575' ], 
                                               
                                        ]
                                    },
                                        layout: 'noBorders'
                                },
                                  '\n',
                                   {
                                    style: 'tableExample1',
                                    table: {
                                         widths: [300, '*'],
                                   
                                        body: [
                                            ['Does the target of the fumigation conform to the DAFF plastic wrapping, Impervious surface and timber thickness requirements at the time of fumgation? ', ': Yes'],
                                            ['Ventilation ', ': Final TLV reading (ppm)'],
                                            
                                               
                                        ]
                                    },
                                        layout: 'noBorders'
                                },
                                  '\n',
                                   {
                                    text: 'DECLARATION',
                                    style: 'subheader3'
                                  },
                                  '\n',
                                    {text: 'By signing below, I, the AFAS accredited fumigator responsible, declare the these details are true and correct and the Fumigation has been carried out in accordance with all the requirements in the DAFF Methyl Bromide Fumigation Standard.', style: 'para1'},
    
                                 
                                '\n',
                                {
                                    text: 'ADDITIONAL DECLARATION',
                                    style: 'subheader3'
                                },
                                '\n',
                                {text: 'FUMIGATION DONE AS PER ISPM-15 COMPLIANCE WITH IPPC LOGO.\N INVOICE NO. : HE/0421018-19 DT. 25/03/2019', style: 'para1'},
                                
                                '\n','\n',
                                 {
                                    style: 'tableExample1',
                                    table: {
                                         widths: [300, '*' ],
                                   
                                        body: [
                                            ['Endorsed by Specified Officer of Dte. of','Name & sign of Accerdited Fumigation'],
                                            ['PPQS Name: sign & Office Seal','Operator with seal & date/Accerditation Number'],
                                            
                                            ]
                                       },
                                       layout: 'noBorders'
                                    },    

                            ],
                    styles: {
                            header: {
                            fontSize: 16,
                            bold: true,
                            alignment: 'center',
                          },
                            subheader: {
                            fontSize: 12,
                            bold: true,
                            alignment: 'center',
                          }, 
                           para1: {
                            fontSize: 8,
                             }, 
                          subheader1: {
                            fontSize: 14,
                            bold: true,
                            alignment: 'center',
                          },
                           subheader2: {
                            fontSize: 10,
                            bold: true,
                            
                          },
                           subheader3: {
                            fontSize: 10,
                            bold: true,
                            alignment: 'center',
                          }, 
                          declr: {
                            fontSize: 10,
                            italics: true,
                            
                          },
                            tableExample: {
                                border : 0,
                                bold: true,
                                 fontSize: 10,
                                margin: [0, 5, 0, 15],
                            },
                             tableExample1: { 
                                bold: true,
                                 fontSize: 8,
                                margin: [0, 5, 0, 15],
                            },
                            tableHeader: {
                                bold: true,
                                fontSize: 10,
                                color: 'black',
                            },
                        tablfont: {
                            fontSize: 10
                        }
                    }
                }

                pdfMake.createPdf(dd).download('testprint.pdf');
              //pdfMake.createPdf(dd).open();
            
        
}
</script>