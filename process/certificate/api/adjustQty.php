<?php

    $base='../../../';

    include($base.'_in/connect.php');

    header('content-type: application/json; charset=utf-8');

    header("access-control-allow-origin: *");

    if(isset($_POST['WpmRef']) && isset($_POST['TotalQty']) && isset($_POST['TotalRemWpm']) && isset($_POST['transferbatch'])){
      
        $con = _connect();

        if (session_status()==PHP_SESSION_NONE) { session_start(); }

        $branchid      = $_SESSION['branchid'];

        $WpmRef        = _clean($con,$_POST['WpmRef']); 

        $TotalQty      = _clean($con,$_POST['TotalQty']); 

        $TotalRemWpm   = _clean($con,$_POST['TotalRemWpm']);

        $transferbatch = json_decode($_POST['transferbatch']);
        
        $NewTotalRemWpm = 0;

        foreach($transferbatch as $i){

            $transferqty += get_object_vars($i)['transferqty'];

            $transferqty1 = get_object_vars($i)['transferqty'];

            $certId       = get_object_vars($i)['certId'];

            $certNo       = get_object_vars($i)['certNo'];

            $certSrNo     = get_object_vars($i)['certSrNo'];


            if($transferqty1==0){
                
                mysqli_query($con,"INSERT INTO `deletecertificate`(`certId`, `branchid`, `enquiryid`, `certificateno`, `country`, `certType`, `fumdate`, `importExport`, `fumdate1`, `fumplace`, `fumdosage`, `daffdosage`, `fumdurationdays`, `fumdurationhrs`, `humidity`, `fumperformed`, `fumtarget`, `consignment`, `issuedate`, `mintemp`, `containerno1`, `containerno2`, `containerno3`, `containerno4`, `exporter1`, `exporter2`, `exporter3`, `exporter4`, `consignee4`, `consignee1`, `consignee2`, `consignee3`, `notifiedparty1`, `notifiedparty2`, `notifiedparty3`, `destinationcon`, `countryorg`, `cargo1`, `cargo2`, `cargo3`, `qty`, `qty1`, `qty2`, `description`, `description1`, `description2`, `vesselname`, `loading`, `shipingmark`, `shipingmark1`, `entryport`, `portofdischarge`, `fumconducted`, `targetfum`, `conpressure`, `confreeair`, `ventilation`, `enclosure`, `enclosure1`, `alpitemqty`, `alpcommodity`, `commodify`, `commodify1`, `plasticwrappingused`, `fumigated`, `plasticwrapping`, `thick`, `dimension`, `additional`, `additional1`, `additional2`, `created_by`, `created`, `confirm`, `mbrimportexport`, `mbrcommodity`, `mbrexportcountry`, `mbrquntity`, `srNo`, `consignment2`, `afasvessel`, `pressureinsec`, `performIn`, `noofcontainer`, `sizeofcontainer`, `calculated`, `chemicaljson`, `chemichalissued`, `confirmtime`, `qty3`, `accno`, `accname`, `description3`, `wpnrefno`, `wpninserted`, `totalwpm`, `remwpm`, `wpmcreated`, `isdraft`, `wpmCommodityJson`,`delect_by`,`delect_Time`) SELECT `id`, `branchid`, `enquiryid`, `certificateno`, `country`, `certType`, `fumdate`, `importExport`, `fumdate1`, `fumplace`, `fumdosage`, `daffdosage`, `fumdurationdays`, `fumdurationhrs`, `humidity`, `fumperformed`, `fumtarget`, `consignment`, `issuedate`, `mintemp`, `containerno1`, `containerno2`, `containerno3`, `containerno4`, `exporter1`, `exporter2`, `exporter3`, `exporter4`, `consignee4`, `consignee1`, `consignee2`, `consignee3`, `notifiedparty1`, `notifiedparty2`, `notifiedparty3`, `destinationcon`, `countryorg`, `cargo1`, `cargo2`, `cargo3`, `qty`, `qty1`, `qty2`, `description`, `description1`, `description2`, `vesselname`, `loading`, `shipingmark`, `shipingmark1`, `entryport`, `portofdischarge`, `fumconducted`, `targetfum`, `conpressure`, `confreeair`, `ventilation`, `enclosure`, `enclosure1`, `alpitemqty`, `alpcommodity`, `commodify`, `commodify1`, `plasticwrappingused`, `fumigated`, `plasticwrapping`, `thick`, `dimension`, `additional`, `additional1`, `additional2`, `created_by`, `created`, `confirm`, `mbrimportexport`, `mbrcommodity`, `mbrexportcountry`, `mbrquntity`, `srNo`, `consignment2`, `afasvessel`, `pressureinsec`, `performIn`, `noofcontainer`, `sizeofcontainer`, `calculated`, `chemicaljson`, `chemichalissued`, `confirmtime`, `qty3`, `accno`, `accname`, `description3`, `wpnrefno`, `wpninserted`, `totalwpm`, `remwpm`, `wpmcreated`, `isdraft`, `wpmCommodityJson`,'1','$CURRENT_MILLIS' FROM certificatemaster WHERE id='$certId' ");              

                mysqli_query($con,"DELETE FROM certificatemaster WHERE id='$certId'");
                    
            } else  {

                mysqli_query($con,"UPDATE certificatemaster SET totalwpm=$transferqty1 WHERE id='$certId' AND certificateno='$certNo' AND srNo='$certSrNo' AND branchid='$branchid' ");
               
            }

            $NewTotalRemWpm = $TotalQty - $transferqty;

        }

        $update =  mysqli_query($con,"UPDATE certificatemaster SET remwpm=$NewTotalRemWpm WHERE id='$WpmRef' AND branchid='$branchid' ");

        if ($update) {

            echo '{"status":"success"}';

        } else {

            echo '{"status":"falid1"}';

        }
              
        _close($con);

    } else {

        echo '{"status":"falid"}';

    }
?>