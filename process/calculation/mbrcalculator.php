<?php 
    $base='../../';
    $navenq6='background:#1B1464;';
    include('header.php');

    if (session_status() == PHP_SESSION_NONE) { session_start(); }
    $sessionby = $_SESSION['employeeid'];
    $branchid = $_SESSION['branchid'];
    if($_GET['id']){
        $certid=$_GET['id'];
    }
    $recal=$_GET['recal'];
    $result = mysqli_query($con,"SELECT * FROM certificatemaster WHERE id ='$certid' ");
    // UPDATE `mitemgroup` SET `inGrams` = '680.389' WHERE `mitemgroup`.`id` = 175; //  inGrams values for mbr calculation
    // UPDATE `mitemgroup` SET `inGrams` = '453.592' WHERE `mitemgroup`.`id` = 176;
    // UPDATE `mitemgroup` SET `inGrams` = '50000' WHERE `mitemgroup`.`id` = 177;
    // UPDATE `mitemgroup` SET `inGrams` = '64000' WHERE `mitemgroup`.`id` = 178;
    $rows = mysqli_fetch_assoc($result);
            $fumdosage=$rows['fumdosage'];
            $performIn = $rows['performIn'];
            $noofcontainer =$rows['noofcontainer'];
            $chemichalissued=$rows['chemichalissued'];
            $chemicaljson=$rows['chemicaljson'];
            $mbrcommodity=$rows['mbrcommodity'];
            $sizeofcontainer = $rows['sizeofcontainer'];
            //echo $performIn;
            $qty = $rows['qty'];
            if($sizeofcontainer==20){
                $valumeofcontainer=40;
            }elseif($sizeofcontainer==40){
                $valumeofcontainer=80;
            }else{}
        if($performIn=="container"){
            $chemicalreq=($noofcontainer*$valumeofcontainer*$fumdosage)/1000;
            $chemicalreq= round(($chemicalreq/0.98),3);
        }elseif($performIn=="container said to be loaded" || $performIn=="warehouse"){
            $multiplier = mysqli_fetch_assoc(mysqli_query($con,"SELECT multiplier x FROM `commoditymaster` WHERE commodityname='$mbrcommodity'"))['x'];
            $chemicalreq=($qty*$multiplier*$fumdosage)/1000;
            $chemicalreq= round(($chemicalreq/0.98),3);
        }elseif($performIn=="vessel"){
            $chemicalreq=(($qty*$fumdosage)/0.98)/1000;
            $chemicalreq= round($chemicalreq,3);
        }elseif($performIn=="other"){
            $chemicalreq=(($qty*$fumdosage)/0.98)/1000;
            $chemicalreq= round($chemicalreq,3);
        }elseif($performIn=="wpm"){

        }
     
?>
<div class="container-fluid">
    <div class="content" id="section">
        <input id="certid" class="hidden" value="<?php echo $certid; ?>">
        <input id="recal" class="hidden" value="<?php echo $recal; ?>">
        <h3 align="center">METHYL BROMIDE ISSUE</h3>
        <hr> 
        <?php if($performIn=="wpm"){ ?>  
        <div class="col-sm-12">
        <div class="col-sm-3"></div>
        <div class="col-sm-6">
        <p>Commodities: <?php echo $rows['mbrcommodity']?></p>
        </div>
        <div class="col-sm-3"></div>
        </div>
        <?php } ?>
        <div class="col-sm-12">
            <div class="col-sm-5"></div>
            <div class="col-sm-2"><p>Total Required</p></div>
            <div class="col-sm-2">
            <?php if($performIn=="wpm"){ ?>
                <input type="number" id="chemicalreq" data-name="chemicalreq" data-role="number" class="form-control" value="<?php echo $chemichalissued; ?>">   
            <?php }else{ ?>
                <input type="number" id="chemicalreq" data-name="chemicalreq" data-role="number" class="form-control" value="<?php echo $chemicalreq; ?>" readonly>   
            <?php } ?>
            </div>
            <div class="col-sm-3"></div>
        </div>
        <div class="col-sm-12">
                <div class="col-sm-2"></div>
                <div class="col-sm-4"><p>Available Types</p></div>
                <div class="col-sm-1"></div>
                <div class="col-sm-3"><p>Quntity (In KG)</p></div>
                <div class="col-sm-2"></div>
        </div>
        <?php
        if(!$recal){
            $mbrmaterial=mysqli_query($con,"SELECT groupName,inGrams FROM `mitemgroup` WHERE mainGroupName='MBR.'");
            while($rows = mysqli_fetch_assoc($mbrmaterial)){
                $itemname=$rows['groupName'];
                $inGrams=$rows['inGrams'];
                echo '<div class="col-sm-12">';
                    echo '<div class="col-sm-2"></div>';
                    echo '<div class="col-sm-3">';
                        echo '<p class="itemname">'.$itemname.'</p>';
                    echo '</div>';
                    echo'<div class="col-sm-4">';
                        echo '<div class="row">';
                            echo'<div class="col-sm-4">';
                                echo '<input type="number" class="form-control chemical" value="0" onkeyup="entry(this)">';
                                echo '<input class="inGrams hidden" value="'.$inGrams.'">';
                            echo '</div>';
                            echo '<div class="col-sm-8">';
                                echo '<input type="number" data-role="number" class="form-control cheinkg" value="0" readonly>';
                            echo '</div>';
                        echo '</div>';
                    echo '</div>';
                    echo'<div class="col-sm-2"></div>';
                echo '</div>';
            }
        }else{
            $array = json_decode($chemicaljson)[0];
            foreach ($array as $key => $value) {
                echo '<div class="col-sm-12">';
                    echo '<div class="col-sm-2"></div>';
                    echo '<div class="col-sm-3">';
                        echo '<p class="itemname">'.$key.'</p>';
                    echo '</div>';
                    echo'<div class="col-sm-4">';
                        echo '<div class="row">';
                            echo'<div class="col-sm-4">';
                                echo '<input type="number" class="form-control chemical" value="'.$value.'" onkeyup="entry(this)">';
                                $inGrams=(mysqli_fetch_assoc(mysqli_query($con,"SELECT inGrams x FROM `mitemgroup` WHERE groupName='$key'"))['x']);
                                echo '<input class="inGrams hidden" value="'.$inGrams.'">';
                            echo '</div>';
                            echo '<div class="col-sm-8">';
                                echo '<input type="number" data-role="number" class="form-control cheinkg" value="0" readonly>';
                            echo '</div>';
                        echo '</div>';
                    echo '</div>';
                    echo'<div class="col-sm-2"></div>';
                echo '</div>';
            }
           
        }
       ?>
        <div class="col-sm-12">
            <div class="col-sm-5"></div>
            <div class="col-sm-2"><p>Total </p></div>
            <div class="col-sm-2">
                <input type="number" id="mattotalissue" value="<?php echo $chemichalissued; ?>" data-name="mattotalissue" data-role="number" class="form-control" value="" readonly>   
            </div>
            <div class="col-sm-3"></div>
        </div>
        <div class="col-sm-12">
            <hr>
            <div class="col-sm-3"></div>
            <div class="col-sm-5">
            <!-- <button class="btn btn-block btn-warning">Reset</button> -->
            <button class="btn btn-block btn-success" onclick="submit()">Submit</button>
            </div>
            <div class="col-sm-4"></div>
        </div>
    </div>
</div>
<?php 
    include('footer.php');
?>        
<script>
    if(recal){
       $(".chemical").each(function(){
           var item=$(this).val();
           var inGrams=$(this).parent().find(".inGrams").val();
           var total=(item*inGrams)/1000;
           $(this).parent().parent().find('.cheinkg').val(total.toFixed(3));
           
       })
    }
    function entry(e){
        var item = $(e).val();
        var inGrams = $(e).parent().find(".inGrams").val();
        var total=(item * inGrams)/1000;
        $(e).parent().parent().find('.cheinkg').val(total.toFixed(3));
        var qty1=0;
        $('.cheinkg').each(function(){
            var qty=parseFloat($(this).parent().parent().find('.cheinkg').val());
            if(isNaN(qty1)){
                qty100=0;
                $(e).val(0);
            }
        qty1 = qty1 + qty;
        $("#mattotalissue").val(qty1.toFixed(3));
        })
    }
    function submit(){
        var str='';
        var essue = $("#mattotalissue").val();
        var total = $("#chemicalreq").val();
        $(".chemical").each(function(){
            var itemqty=$(this).val();
            var itemname=$(this).parent().parent().parent().parent().find('.itemname').text();
            str+=',"'+itemname+'":"'+itemqty+'"'; 
        })
      
        str='[{'+str.substring(1, str.length)+'}]';
        
        if(total>essue){
            alert('Something went wrong...!');
        }else{
            $.ajax({
                type: "POST",
                data: {
                    certid:$("#certid").val(),
                    total:essue,
                    json:str
                },
                url: 'api/calculator.php',
                cache: false,
                success: function (res) {
                if (res.status == 'success') {
                    window.location = "materialissue.php";
                }
                }
            }) 
        }
    }
</script>