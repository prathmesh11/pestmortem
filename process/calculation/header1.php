
<?php

    include('header.php');
    
?>
<style>
    .th{
        background:#8e44ad;
        color:#fff;
        text-align:center;
        padding-top:2px;
        padding-bottom:2px;
        border:1px solid #fff;
    }
    .td{
        border:1px solid #ddd;
    }
    .table-ui .btn{
        margin:3px;
    }
    #myModal1 .col-sm-4,.col-sm-8{
      margin-bottom:2px;
    }
    .picker__select--year{
        height:auto;
    }
    .picker__select--month{
        height:auto;
    }
    .table-list td,
   .table-list th {
       border: 1px solid #ddd;
       padding: 1px !important;
       font-size: 13px;
   }
   .table-list td{
	padding-top: 10px !important;
   }

   .table-list tr:nth-child(even) {
       background-color: #f2f2f2;
   }

   .table-list th {
       padding-top: 5px;
       padding-bottom: 5px;
       text-align: center;
       background-color: #16a085;
       color: white;
   }
</style>
<br>
    <!-- <div id="div-content" class="content"> -->
<table width="100%" >
<tr>
<td align="center" style="<?php echo $alp; ?>"><a href="/process/calculation/alp.php" style="border:1px solid blue;border-radius:0px;<?php echo $navenq8;?>" class="btn btn-primary btn-block">ALP Calculation</td>
<td align="center" style="<?php echo $mbr; ?>"><a href="/process/calculation/mbr.php" style="border:1px solid blue;border-radius:0px;<?php echo $navenq9;?>" class="btn btn-primary btn-block">MBR Calculation</td>
</tr>
</table>
<!-- </div>
</div> -->


