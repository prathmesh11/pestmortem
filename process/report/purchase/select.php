<?php
$base='../../../';
    include($base.'_in/connect.php');
    header('content-type: application/json; charset=utf-8');
    header("access-control-allow-origin: *");
    
    if(isset($_POST['branchid']) && isset($_POST['purchaseid']) && isset($_POST['supplierid']) && isset($_POST['fromdate']) && isset($_POST['todate']) && isset($_POST['status']) && isset($_POST['data'])) {
      $con=_connect();
      $branchid =_clean($con,$_POST["branchid"]);
      $purchaseid =_clean($con,$_POST["purchaseid"]);
      $supplierid =_clean($con,$_POST["supplierid"]);
      $fromdate =_clean($con,$_POST["fromdate"]);
      $todate =_clean($con,$_POST["todate"]);
      $status =_clean($con,$_POST["status"]);
      $data =json_decode($_POST['data']);
      $query='';

      if($branchid=='Select'){$branchid='';}
      if($purchaseid=='Select'){$purchaseid='';}
      if($supplierid=='Select'){$supplierid='';}
      if($status=='Select'){$status='';}
      //echo $branchid,$purchaseid,$supplierid,$fromdate,$todate,$status;
      switch ($purchaseid){
       case ($branchid=='' && $purchaseid=='' && $supplierid=='' && $fromdate=='' && $todate=='' && $status!=''):
       //00001
       if($status=='Purchase Order'){
          $query= "SELECT DISTINCT purchaseid FROM purchase WHERE created_time<>0 AND gateentry_time=0 AND quality_time=0  AND purchasebill_time=0 AND close_time=0";
          }elseif($status=='Gate Entry'){
            $query= "SELECT DISTINCT purchaseid FROM purchase WHERE created_time<>0 AND gateentry_time<>0 AND quality_time=0 AND purchasebill_time=0 AND close_time=0";
          }elseif($status=='Quality'){
            $query= "SELECT DISTINCT purchaseid FROM purchase WHERE created_time<>0 AND gateentry_time<>0 AND quality_time<>0 AND purchasebill_time=0 AND close_time=0";
          }elseif($status=='Purchase Bill'){
            $query= "SELECT DISTINCT purchaseid FROM purchase WHERE created_time<>0 AND gateentry_time<>0 AND quality_time<>0 AND purchasebill_time<>0 AND close_time=0";
          }elseif($status=='Closed'){
            $query= "SELECT DISTINCT purchaseid FROM purchase WHERE created_time<>0 AND gateentry_time<>0 AND quality_time<>0 AND purchasebill_time<>0 AND close_time<>0";
          }
        break;
        case ($branchid=='' && $purchaseid=='' && $supplierid=='' && $fromdate!='' && $todate!='' && $status==''):
          //00010
          $query="SELECT DISTINCT purchaseid FROM purchase WHERE podate BETWEEN '$fromdate' AND '$todate'";
        break;
        case ($branchid=='' && $purchaseid=='' && $supplierid=='' && $fromdate!='' && $todate!='' && $status!=''):
        //00011
        if($status=='Purchase Order'){
          $query= "SELECT DISTINCT purchaseid FROM purchase WHERE podate BETWEEN '$fromdate' AND '$todate' AND created_time<>0 AND gateentry_time=0 AND quality_time=0  AND purchasebill_time=0 AND close_time=0";
          }elseif($status=='Gate Entry'){
            $query= "SELECT DISTINCT purchaseid FROM purchase WHERE podate BETWEEN '$fromdate' AND '$todate' AND created_time<>0 AND gateentry_time<>0 AND quality_time=0 AND purchasebill_time=0 AND close_time=0";
          }elseif($status=='Quality'){
            $query= "SELECT DISTINCT purchaseid FROM purchase WHERE podate BETWEEN '$fromdate' AND '$todate' AND created_time<>0 AND gateentry_time<>0 AND quality_time<>0 AND purchasebill_time=0 AND close_time=0";
          }elseif($status=='Purchase Bill'){
            $query= "SELECT DISTINCT purchaseid FROM purchase WHERE podate BETWEEN '$fromdate' AND '$todate' AND created_time<>0 AND gateentry_time<>0 AND quality_time<>0 AND purchasebill_time<>0 AND close_time=0";
          }elseif($status=='Closed'){
            $query= "SELECT DISTINCT purchaseid FROM purchase WHERE podate BETWEEN '$fromdate' AND '$todate' AND created_time<>0 AND gateentry_time<>0 AND quality_time<>0 AND purchasebill_time<>0 AND close_time<>0";
          }
        break;
        case ($branchid=='' && $purchaseid=='' && $supplierid!='' && $fromdate=='' && $todate=='' && $status==''):
        //00100
        $query="SELECT DISTINCT purchaseid FROM purchase WHERE supplierid='$supplierid'";
        break;
        case ($branchid=='' && $purchaseid=='' && $supplierid!='' && $fromdate=='' && $todate=='' && $status!=''):
        //00101
          if($status=='Purchase Order'){
          $query= "SELECT DISTINCT purchaseid FROM purchase WHERE supplierid='$supplierid' AND created_time<>0 AND gateentry_time=0 AND quality_time=0  AND purchasebill_time=0 AND close_time=0";
          }elseif($status=='Gate Entry'){
            $query= "SELECT DISTINCT purchaseid FROM purchase WHERE supplierid='$supplierid' AND created_time<>0 AND gateentry_time<>0 AND quality_time=0 AND purchasebill_time=0 AND close_time=0";
          }elseif($status=='Quality'){
            $query= "SELECT DISTINCT purchaseid FROM purchase WHERE supplierid='$supplierid' AND created_time<>0 AND gateentry_time<>0 AND quality_time<>0 AND purchasebill_time=0 AND close_time=0";
          }elseif($status=='Purchase Bill'){
            $query= "SELECT DISTINCT purchaseid FROM purchase WHERE supplierid='$supplierid' AND created_time<>0 AND gateentry_time<>0 AND quality_time<>0 AND purchasebill_time<>0 AND close_time=0";
          }elseif($status=='Closed'){
            $query= "SELECT DISTINCT purchaseid FROM purchase WHERE supplierid='$supplierid' AND created_time<>0 AND gateentry_time<>0 AND quality_time<>0 AND purchasebill_time<>0 AND close_time<>0";
          }
        break;
        case ($branchid=='' && $purchaseid=='' && $supplierid!='' && $fromdate!='' && $todate!='' && $status==''):
        //00110
        $query="SELECT DISTINCT purchaseid FROM purchase WHERE supplierid='$supplierid' AND podate BETWEEN '$fromdate' AND '$todate'";
        break;
        case ($branchid=='' && $purchaseid=='' && $supplierid!='' && $fromdate!='' && $todate!='' && $status!=''):
          //00111
        if($status=='Purchase Order'){
          $query= "SELECT DISTINCT purchaseid FROM purchase WHERE supplierid='$supplierid' AND podate BETWEEN '$fromdate' AND '$todate' AND created_time<>0 AND gateentry_time=0 AND quality_time=0  AND purchasebill_time=0 AND close_time=0";
          }elseif($status=='Gate Entry'){
            $query= "SELECT DISTINCT purchaseid FROM purchase WHERE supplierid='$supplierid' AND podate BETWEEN '$fromdate' AND '$todate' AND created_time<>0 AND gateentry_time<>0 AND quality_time=0 AND purchasebill_time=0 AND close_time=0";
          }elseif($status=='Quality'){
            $query= "SELECT DISTINCT purchaseid FROM purchase WHERE supplierid='$supplierid' AND podate BETWEEN '$fromdate' AND '$todate' AND created_time<>0 AND gateentry_time<>0 AND quality_time<>0 AND purchasebill_time=0 AND close_time=0";
          }elseif($status=='Purchase Bill'){
            $query= "SELECT DISTINCT purchaseid FROM purchase WHERE supplierid='$supplierid' AND podate BETWEEN '$fromdate' AND '$todate' AND created_time<>0 AND gateentry_time<>0 AND quality_time<>0 AND purchasebill_time<>0 AND close_time=0";
          }elseif($status=='Closed'){
            $query= "SELECT DISTINCT purchaseid FROM purchase WHERE supplierid='$supplierid' AND podate BETWEEN '$fromdate' AND '$todate' AND created_time<>0 AND gateentry_time<>0 AND quality_time<>0 AND purchasebill_time<>0 AND close_time<>0";
          }
        break;
        case ($branchid=='' && $purchaseid!='' && $supplierid=='' && $fromdate=='' && $todate=='' && $status==''):
        //01000
         $query="SELECT DISTINCT purchaseid FROM purchase WHERE purchaseid='$purchaseid'";
        break;
        case ($branchid=='' && $purchaseid!='' && $supplierid=='' && $fromdate=='' && $todate=='' && $status!=''):
        //01001
        if($status=='Purchase Order'){
          $query= "SELECT DISTINCT purchaseid FROM purchase WHERE supplierid='$supplierid' AND purchaseid='$purchaseid' AND created_time<>0 AND gateentry_time=0 AND quality_time=0  AND purchasebill_time=0 AND close_time=0";
          }elseif($status=='Gate Entry'){
            $query= "SELECT DISTINCT purchaseid FROM purchase WHERE supplierid='$supplierid' AND purchaseid='$purchaseid' AND created_time<>0 AND gateentry_time<>0 AND quality_time=0 AND purchasebill_time=0 AND close_time=0";
          }elseif($status=='Quality'){
            $query= "SELECT DISTINCT purchaseid FROM purchase WHERE supplierid='$supplierid' AND purchaseid='$purchaseid' AND created_time<>0 AND gateentry_time<>0 AND quality_time<>0 AND purchasebill_time=0 AND close_time=0";
          }elseif($status=='Purchase Bill'){
            $query= "SELECT DISTINCT purchaseid FROM purchase WHERE supplierid='$supplierid' AND purchaseid='$purchaseid' AND created_time<>0 AND gateentry_time<>0 AND quality_time<>0 AND purchasebill_time<>0 AND close_time=0";
          }elseif($status=='Closed'){
            $query= "SELECT DISTINCT purchaseid FROM purchase WHERE supplierid='$supplierid' AND purchaseid='$purchaseid' AND created_time<>0 AND gateentry_time<>0 AND quality_time<>0 AND purchasebill_time<>0 AND close_time<>0";
          }
        break;
        case ($branchid=='' && $purchaseid!='' && $supplierid=='' && $fromdate!='' && $todate!='' && $status==''):
        //01010
        $query="SELECT DISTINCT purchaseid FROM purchase WHERE purchaseid='$purchaseid' AND podate BETWEEN '$fromdate' AND '$todate'";
        break;
        case ($branchid=='' && $purchaseid!='' && $supplierid=='' && $fromdate!='' && $todate!='' && $status!=''):
        //01011
        if($status=='Purchase Order'){
          $query= "SELECT DISTINCT purchaseid FROM purchase WHERE podate BETWEEN '$fromdate' AND '$todate' AND purchaseid='$purchaseid' AND created_time<>0 AND gateentry_time=0 AND quality_time=0  AND purchasebill_time=0 AND close_time=0";
          }elseif($status=='Gate Entry'){
            $query= "SELECT DISTINCT purchaseid FROM purchase WHERE podate BETWEEN '$fromdate' AND '$todate' AND purchaseid='$purchaseid' AND created_time<>0 AND gateentry_time<>0 AND quality_time=0 AND purchasebill_time=0 AND close_time=0";
          }elseif($status=='Quality'){
            $query= "SELECT DISTINCT purchaseid FROM purchase WHERE podate BETWEEN '$fromdate' AND '$todate' AND purchaseid='$purchaseid' AND created_time<>0 AND gateentry_time<>0 AND quality_time<>0 AND purchasebill_time=0 AND close_time=0";
          }elseif($status=='Purchase Bill'){
            $query= "SELECT DISTINCT purchaseid FROM purchase WHERE podate BETWEEN '$fromdate' AND '$todate' AND purchaseid='$purchaseid' AND created_time<>0 AND gateentry_time<>0 AND quality_time<>0 AND purchasebill_time<>0 AND close_time=0";
          }elseif($status=='Closed'){
            $query= "SELECT DISTINCT purchaseid FROM purchase WHERE podate BETWEEN '$fromdate' AND '$todate' AND purchaseid='$purchaseid' AND created_time<>0 AND gateentry_time<>0 AND quality_time<>0 AND purchasebill_time<>0 AND close_time<>0";
          }
        break;
        case ($branchid=='' && $purchaseid!='' && $supplierid!='' && $fromdate=='' && $todate=='' && $status==''):
        //01100 
        $query="SELECT DISTINCT purchaseid FROM purchase WHERE purchaseid='$purchaseid' AND supplierid='$supplierid'";
        break;
        case ($branchid=='' && $purchaseid!='' && $supplierid!='' && $fromdate=='' && $todate=='' && $status!=''):
        //01101
          if($status=='Purchase Order'){
          $query= "SELECT DISTINCT purchaseid FROM purchase WHERE purchaseid='$purchaseid' AND supplierid='$supplierid' AND created_time<>0 AND gateentry_time=0 AND quality_time=0  AND purchasebill_time=0 AND close_time=0";
          }elseif($status=='Gate Entry'){
            $query= "SELECT DISTINCT purchaseid FROM purchase WHERE purchaseid='$purchaseid' AND supplierid='$supplierid' AND created_time<>0 AND gateentry_time<>0 AND quality_time=0 AND purchasebill_time=0 AND close_time=0";
          }elseif($status=='Quality'){
            $query= "SELECT DISTINCT purchaseid FROM purchase WHERE purchaseid='$purchaseid' AND supplierid='$supplierid' AND created_time<>0 AND gateentry_time<>0 AND quality_time<>0 AND purchasebill_time=0 AND close_time=0";
          }elseif($status=='Purchase Bill'){
            $query= "SELECT DISTINCT purchaseid FROM purchase WHERE purchaseid='$purchaseid' AND supplierid='$supplierid' AND created_time<>0 AND gateentry_time<>0 AND quality_time<>0 AND purchasebill_time<>0 AND close_time=0";
          }elseif($status=='Closed'){
            $query= "SELECT DISTINCT purchaseid FROM purchase WHERE purchaseid='$purchaseid' AND supplierid='$supplierid' AND created_time<>0 AND gateentry_time<>0 AND quality_time<>0 AND purchasebill_time<>0 AND close_time<>0";
          }
        break;
        case ($branchid=='' && $purchaseid!='' && $supplierid!='' && $fromdate!='' && $todate!='' && $status==''):
        //01110
        $query= "SELECT DISTINCT purchaseid FROM purchase WHERE purchaseid='$purchaseid' AND supplierid='$supplierid' AND podate BETWEEN '$fromdate' AND '$todate'";
        break;
        case ($branchid=='' && $purchaseid!='' && $supplierid!='' && $fromdate!='' && $todate!='' && $status!=''):
        //01111
          if($status=='Purchase Order'){
          $query= "SELECT DISTINCT purchaseid FROM purchase WHERE podate BETWEEN '$fromdate' AND '$todate' AND purchaseid='$purchaseid' AND supplierid='$supplierid' AND created_time<>0 AND gateentry_time=0 AND quality_time=0  AND purchasebill_time=0 AND close_time=0";
          }elseif($status=='Gate Entry'){
            $query= "SELECT DISTINCT purchaseid FROM purchase WHERE podate BETWEEN '$fromdate' AND '$todate' AND purchaseid='$purchaseid' AND supplierid='$supplierid' AND created_time<>0 AND gateentry_time<>0 AND quality_time=0 AND purchasebill_time=0 AND close_time=0";
          }elseif($status=='Quality'){
            $query= "SELECT DISTINCT purchaseid FROM purchase WHERE  podate BETWEEN '$fromdate' AND '$todate' AND purchaseid='$purchaseid' AND supplierid='$supplierid' AND created_time<>0 AND gateentry_time<>0 AND quality_time<>0 AND purchasebill_time=0 AND close_time=0";
          }elseif($status=='Purchase Bill'){
            $query= "SELECT DISTINCT purchaseid FROM purchase WHERE  podate BETWEEN '$fromdate' AND '$todate' AND purchaseid='$purchaseid' AND supplierid='$supplierid' AND created_time<>0 AND gateentry_time<>0 AND quality_time<>0 AND purchasebill_time<>0 AND close_time=0";
          }elseif($status=='Closed'){
            $query= "SELECT DISTINCT purchaseid FROM purchase WHERE  podate BETWEEN '$fromdate' AND '$todate' AND purchaseid='$purchaseid' AND supplierid='$supplierid' AND created_time<>0 AND gateentry_time<>0 AND quality_time<>0 AND purchasebill_time<>0 AND close_time<>0";
          }
        break;
        case ($branchid!='' && $purchaseid=='' && $supplierid=='' && $fromdate=='' && $todate=='' && $status==''):
          //10000
          $query= "SELECT DISTINCT purchaseid FROM purchase WHERE branchid='$branchid'";
        break;
        case ($branchid!='' && $purchaseid=='' && $supplierid=='' && $fromdate!='' && $todate!='' && $status==''):
        //10010
        $query= "SELECT DISTINCT purchaseid FROM purchase WHERE branchid='$branchid' AND podate BETWEEN '$fromdate' AND '$todate'";
        break;
        case ($branchid!='' && $purchaseid=='' && $supplierid=='' && $fromdate!='' && $todate!='' && $status!=''):
        //10011
        if($status=='Purchase Order'){
          $query= "SELECT DISTINCT purchaseid FROM purchase WHERE podate BETWEEN '$fromdate' AND '$todate' AND created_time<>0 AND gateentry_time=0 AND quality_time=0  AND purchasebill_time=0 AND close_time=0";
          }elseif($status=='Gate Entry'){
            $query= "SELECT DISTINCT purchaseid FROM purchase WHERE podate BETWEEN '$fromdate' AND '$todate' AND created_time<>0 AND gateentry_time<>0 AND quality_time=0 AND purchasebill_time=0 AND close_time=0";
          }elseif($status=='Quality'){
            $query= "SELECT DISTINCT purchaseid FROM purchase WHERE podate BETWEEN '$fromdate' AND '$todate' AND created_time<>0 AND gateentry_time<>0 AND quality_time<>0 AND purchasebill_time=0 AND close_time=0";
          }elseif($status=='Purchase Bill'){
            $query= "SELECT DISTINCT purchaseid FROM purchase WHERE podate BETWEEN '$fromdate' AND '$todate' AND created_time<>0 AND gateentry_time<>0 AND quality_time<>0 AND purchasebill_time<>0 AND close_time=0";
          }elseif($status=='Closed'){
            $query= "SELECT DISTINCT purchaseid FROM purchase WHERE podate BETWEEN '$fromdate' AND '$todate' AND created_time<>0 AND gateentry_time<>0 AND quality_time<>0 AND purchasebill_time<>0 AND close_time<>0";
          }
        break;
        case ($branchid!='' && $purchaseid=='' && $supplierid!='' && $fromdate=='' && $todate=='' && $status==''):
        //10100
        $query= "SELECT DISTINCT purchaseid FROM purchase WHERE branchid='$branchid' AND supplierid='$supplierid'";
        break;
        case ($branchid!='' && $purchaseid=='' && $supplierid!='' && $fromdate=='' && $todate=='' && $status!=''):
        //10101
        if($status=='Purchase Order'){
          $query= "SELECT DISTINCT purchaseid FROM purchase WHERE branchid='$branchid' AND supplierid='$supplierid' AND created_time<>0 AND gateentry_time=0 AND quality_time=0  AND purchasebill_time=0 AND close_time=0";
          }elseif($status=='Gate Entry'){
            $query= "SELECT DISTINCT purchaseid FROM purchase WHERE branchid='$branchid' AND supplierid='$supplierid' AND created_time<>0 AND gateentry_time<>0 AND quality_time=0 AND purchasebill_time=0 AND close_time=0";
          }elseif($status=='Quality'){
            $query= "SELECT DISTINCT purchaseid FROM purchase WHERE branchid='$branchid' AND supplierid='$supplierid' AND created_time<>0 AND gateentry_time<>0 AND quality_time<>0 AND purchasebill_time=0 AND close_time=0";
          }elseif($status=='Purchase Bill'){
            $query= "SELECT DISTINCT purchaseid FROM purchase WHERE branchid='$branchid' AND supplierid='$supplierid' AND created_time<>0 AND gateentry_time<>0 AND quality_time<>0 AND purchasebill_time<>0 AND close_time=0";
          }elseif($status=='Closed'){
            $query= "SELECT DISTINCT purchaseid FROM purchase WHERE branchid='$branchid' AND supplierid='$supplierid' AND created_time<>0 AND gateentry_time<>0 AND quality_time<>0 AND purchasebill_time<>0 AND close_time<>0";
          }
        break;
        case ($branchid!='' && $purchaseid=='' && $supplierid!='' && $fromdate!='' && $todate!='' && $status==''):
        //10101
        $query= "SELECT DISTINCT purchaseid FROM purchase WHERE branchid='$branchid' AND supplierid='$supplierid' AND podate BETWEEN '$fromdate' AND '$todate'";
        break;
        case ($branchid!='' && $purchaseid=='' && $supplierid!='' && $fromdate!='' && $todate!='' && $status!=''):
        if($status=='Purchase Order'){
          $query= "SELECT DISTINCT purchaseid FROM purchase WHERE branchid='$branchid' AND supplierid='$supplierid' AND podate BETWEEN '$fromdate' AND '$todate' AND created_time<>0 AND gateentry_time=0 AND quality_time=0  AND purchasebill_time=0 AND close_time=0";
          }elseif($status=='Gate Entry'){
            $query= "SELECT DISTINCT purchaseid FROM purchase WHERE branchid='$branchid' AND supplierid='$supplierid' AND podate BETWEEN '$fromdate' AND '$todate' AND created_time<>0 AND gateentry_time<>0 AND quality_time=0 AND purchasebill_time=0 AND close_time=0";
          }elseif($status=='Quality'){
            $query= "SELECT DISTINCT purchaseid FROM purchase WHERE branchid='$branchid' AND supplierid='$supplierid' AND podate BETWEEN '$fromdate' AND '$todate' AND created_time<>0 AND gateentry_time<>0 AND quality_time<>0 AND purchasebill_time=0 AND close_time=0";
          }elseif($status=='Purchase Bill'){
            $query= "SELECT DISTINCT purchaseid FROM purchase WHERE branchid='$branchid' AND supplierid='$supplierid' AND podate BETWEEN '$fromdate' AND '$todate' AND created_time<>0 AND gateentry_time<>0 AND quality_time<>0 AND purchasebill_time<>0 AND close_time=0";
          }elseif($status=='Closed'){
            $query= "SELECT DISTINCT purchaseid FROM purchase WHERE branchid='$branchid' AND supplierid='$supplierid' AND podate BETWEEN '$fromdate' AND '$todate' AND created_time<>0 AND gateentry_time<>0 AND quality_time<>0 AND purchasebill_time<>0 AND close_time<>0";
          }
        break;
        case ($branchid!='' && $purchaseid!='' && $supplierid=='' && $fromdate=='' && $todate=='' && $status==''):
        //11000
          $query= "SELECT DISTINCT purchaseid FROM purchase WHERE branchid='$branchid' AND purchaseid='$purchaseid'";
        break;
        case ($branchid!='' && $purchaseid!='' && $supplierid=='' && $fromdate=='' && $todate=='' && $status!=''):
        if($status=='Purchase Order'){
          $query= "SELECT DISTINCT purchaseid FROM purchase WHERE branchid='$branchid' AND supplierid='$supplierid' AND podate BETWEEN '$fromdate' AND '$todate' AND created_time<>0 AND gateentry_time=0 AND quality_time=0  AND purchasebill_time=0 AND close_time=0";
          }elseif($status=='Gate Entry'){
            $query= "SELECT DISTINCT purchaseid FROM purchase WHERE branchid='$branchid' AND supplierid='$supplierid' AND podate BETWEEN '$fromdate' AND '$todate' AND created_time<>0 AND gateentry_time<>0 AND quality_time=0 AND purchasebill_time=0 AND close_time=0";
          }elseif($status=='Quality'){
            $query= "SELECT DISTINCT purchaseid FROM purchase WHERE branchid='$branchid' AND supplierid='$supplierid' AND podate BETWEEN '$fromdate' AND '$todate' AND created_time<>0 AND gateentry_time<>0 AND quality_time<>0 AND purchasebill_time=0 AND close_time=0";
          }elseif($status=='Purchase Bill'){
            $query= "SELECT DISTINCT purchaseid FROM purchase WHERE branchid='$branchid' AND supplierid='$supplierid' AND podate BETWEEN '$fromdate' AND '$todate' AND created_time<>0 AND gateentry_time<>0 AND quality_time<>0 AND purchasebill_time<>0 AND close_time=0";
          }elseif($status=='Closed'){
            $query= "SELECT DISTINCT purchaseid FROM purchase WHERE branchid='$branchid' AND supplierid='$supplierid' AND podate BETWEEN '$fromdate' AND '$todate' AND created_time<>0 AND gateentry_time<>0 AND quality_time<>0 AND purchasebill_time<>0 AND close_time<>0";
          }
        break;
        case ($branchid!='' && $purchaseid!='' && $supplierid=='' && $fromdate!='' && $todate!='' && $status==''):
        //11010
         $query= "SELECT DISTINCT purchaseid FROM purchase WHERE branchid='$branchid' AND purchaseid='$purchaseid' AND podate BETWEEN '$fromdate' AND '$todate'";
        break;
        case ($branchid!='' && $purchaseid!='' && $supplierid=='' && $fromdate!='' && $todate!='' && $status!=''):
        if($status=='Purchase Order'){
          $query= "SELECT DISTINCT purchaseid FROM purchase WHERE branchid='$branchid' AND purchaseid='$purchaseid' AND podate BETWEEN '$fromdate' AND '$todate' AND created_time<>0 AND gateentry_time=0 AND quality_time=0  AND purchasebill_time=0 AND close_time=0";
          }elseif($status=='Gate Entry'){
            $query= "SELECT DISTINCT purchaseid FROM purchase WHERE branchid='$branchid' AND purchaseid='$purchaseid' AND podate BETWEEN '$fromdate' AND '$todate' AND created_time<>0 AND gateentry_time<>0 AND quality_time=0 AND purchasebill_time=0 AND close_time=0";
          }elseif($status=='Quality'){
            $query= "SELECT DISTINCT purchaseid FROM purchase WHERE branchid='$branchid' AND purchaseid='$purchaseid' AND podate BETWEEN '$fromdate' AND '$todate' AND created_time<>0 AND gateentry_time<>0 AND quality_time<>0 AND purchasebill_time=0 AND close_time=0";
          }elseif($status=='Purchase Bill'){
            $query= "SELECT DISTINCT purchaseid FROM purchase WHERE branchid='$branchid' AND purchaseid='$purchaseid' AND podate BETWEEN '$fromdate' AND '$todate' AND created_time<>0 AND gateentry_time<>0 AND quality_time<>0 AND purchasebill_time<>0 AND close_time=0";
          }elseif($status=='Closed'){
            $query= "SELECT DISTINCT purchaseid FROM purchase WHERE branchid='$branchid' AND purchaseid='$purchaseid' AND podate BETWEEN '$fromdate' AND '$todate' AND created_time<>0 AND gateentry_time<>0 AND quality_time<>0 AND purchasebill_time<>0 AND close_time<>0";
          }
        break;
        case ($branchid!='' && $purchaseid!='' && $supplierid!='' && $fromdate=='' && $todate=='' && $status==''):
        //11000
        $query= "SELECT DISTINCT purchaseid FROM purchase WHERE branchid='$branchid' AND purchaseid='$purchaseid' AND supplierid='$supplierid'";
        break;
        case ($branchid!='' && $purchaseid!='' && $supplierid!='' && $fromdate=='' && $todate=='' && $status!=''):
        //11101
        if($status=='Purchase Order'){
          $query= "SELECT DISTINCT purchaseid FROM purchase WHERE branchid='$branchid' AND purchaseid='$purchaseid' AND supplierid='$supplierid' AND created_time<>0 AND gateentry_time=0 AND quality_time=0  AND purchasebill_time=0 AND close_time=0";
          }elseif($status=='Gate Entry'){
            $query= "SELECT DISTINCT purchaseid FROM purchase WHERE branchid='$branchid' AND purchaseid='$purchaseid' AND supplierid='$supplierid' AND created_time<>0 AND gateentry_time<>0 AND quality_time=0 AND purchasebill_time=0 AND close_time=0";
          }elseif($status=='Quality'){
            $query= "SELECT DISTINCT purchaseid FROM purchase WHERE branchid='$branchid' AND purchaseid='$purchaseid' AND supplierid='$supplierid' AND created_time<>0 AND gateentry_time<>0 AND quality_time<>0 AND purchasebill_time=0 AND close_time=0";
          }elseif($status=='Purchase Bill'){
            $query= "SELECT DISTINCT purchaseid FROM purchase WHERE branchid='$branchid' AND purchaseid='$purchaseid' AND supplierid='$supplierid' AND created_time<>0 AND gateentry_time<>0 AND quality_time<>0 AND purchasebill_time<>0 AND close_time=0";
          }elseif($status=='Closed'){
            $query= "SELECT DISTINCT purchaseid FROM purchase WHERE branchid='$branchid' AND purchaseid='$purchaseid' AND supplierid='$supplierid' AND created_time<>0 AND gateentry_time<>0 AND quality_time<>0 AND purchasebill_time<>0 AND close_time<>0";
          } 
        break;
        case ($branchid=='' && $purchaseid=='' && $supplierid=='' && $fromdate=='' && $todate=='' && $status==''):
          //00000
        break;
        case ($branchid!='' && $purchaseid!='' && $supplierid!='' && $fromdate!='' && $todate!='' && $status==''):
        //11110
        $query= "SELECT DISTINCT purchaseid FROM purchase WHERE branchid='$branchid' AND purchaseid='$purchaseid' AND supplierid='$supplierid' AND podate BETWEEN '$fromdate' AND '$todate'";
        break;
        case ($branchid!='' && $purchaseid!='' && $supplierid!='' && $fromdate!='' && $todate!='' && $status!=''):
        //11111
        if($status=='Purchase Order'){
          $query= "SELECT DISTINCT purchaseid FROM purchase WHERE branchid='$branchid' AND purchaseid='$purchaseid' AND supplierid='$supplierid' AND podate BETWEEN '$fromdate' AND '$todate' AND created_time<>0 AND gateentry_time=0 AND quality_time=0  AND purchasebill_time=0 AND close_time=0";
          }elseif($status=='Gate Entry'){
            $query= "SELECT DISTINCT purchaseid FROM purchase WHERE branchid='$branchid' AND purchaseid='$purchaseid' AND supplierid='$supplierid' AND podate BETWEEN '$fromdate' AND '$todate' AND created_time<>0 AND gateentry_time<>0 AND quality_time=0 AND purchasebill_time=0 AND close_time=0";
          }elseif($status=='Quality'){
            $query= "SELECT DISTINCT purchaseid FROM purchase WHERE branchid='$branchid' AND purchaseid='$purchaseid' AND supplierid='$supplierid' AND podate BETWEEN '$fromdate' AND '$todate' AND created_time<>0 AND gateentry_time<>0 AND quality_time<>0 AND purchasebill_time=0 AND close_time=0";
          }elseif($status=='Purchase Bill'){
            $query= "SELECT DISTINCT purchaseid FROM purchase WHERE branchid='$branchid' AND purchaseid='$purchaseid' AND supplierid='$supplierid' AND podate BETWEEN '$fromdate' AND '$todate' AND created_time<>0 AND gateentry_time<>0 AND quality_time<>0 AND purchasebill_time<>0 AND close_time=0";
          }elseif($status=='Closed'){
            $query= "SELECT DISTINCT purchaseid FROM purchase WHERE branchid='$branchid' AND purchaseid='$purchaseid' AND supplierid='$supplierid' AND podate BETWEEN '$fromdate' AND '$todate' AND created_time<>0 AND gateentry_time<>0 AND quality_time<>0 AND purchasebill_time<>0 AND close_time<>0";
          }
        break;
        default:
        //do nothing
        
        }
        if($branchid!='' && $purchaseid=='' && $supplierid=='' && $fromdate=='' && $todate=='' && $status==''){
          //10000
          $query= "SELECT DISTINCT purchaseid FROM purchase WHERE branchid='$branchid'";
        }
        
        if($branchid=='' && $purchaseid=='' && $supplierid!='' && $fromdate=='' && $todate=='' && $status==''){
        //00100
        $query="SELECT DISTINCT purchaseid FROM purchase WHERE supplierid='$supplierid'";
        }
        if($branchid=='' && $purchaseid=='' && $supplierid=='' && $fromdate!='' && $todate!='' && $status==''){
        //00010
        $query="SELECT DISTINCT purchaseid FROM purchase WHERE podate BETWEEN '$fromdate' AND '$todate'";
        }
        if($branchid=='' && $purchaseid=='' && $supplierid=='' && $fromdate=='' && $todate=='' && $status!=''){
       //00001
        if($status=='Purchase Order'){
          $query= "SELECT DISTINCT purchaseid FROM purchase WHERE created_time<>0 AND gateentry_time=0 AND quality_time=0  AND purchasebill_time=0 AND close_time=0";
          }elseif($status=='Gate Entry'){
            $query= "SELECT DISTINCT purchaseid FROM purchase WHERE created_time<>0 AND gateentry_time<>0 AND quality_time=0 AND purchasebill_time=0 AND close_time=0";
          }elseif($status=='Quality'){
            $query= "SELECT DISTINCT purchaseid FROM purchase WHERE created_time<>0 AND gateentry_time<>0 AND quality_time<>0 AND purchasebill_time=0 AND close_time=0";
          }elseif($status=='Purchase Bill'){
            $query= "SELECT DISTINCT purchaseid FROM purchase WHERE created_time<>0 AND gateentry_time<>0 AND quality_time<>0 AND purchasebill_time<>0 AND close_time=0";
          }elseif($status=='Closed'){
            $query= "SELECT DISTINCT purchaseid FROM purchase WHERE created_time<>0 AND gateentry_time<>0 AND quality_time<>0 AND purchasebill_time<>0 AND close_time<>0";
          }
        }
          if($branchid!='' && $purchaseid=='' && $supplierid=='' && $fromdate!='' && $todate!='' && $status==''){
          //10010
          $query= "SELECT DISTINCT purchaseid FROM purchase WHERE branchid='$branchid' AND podate BETWEEN '$fromdate' AND '$todate'";
          }
          if($branchid!='' && $purchaseid=='' && $supplierid!='' && $fromdate=='' && $todate=='' && $status==''){
        //10100
            $query= "SELECT DISTINCT purchaseid FROM purchase WHERE branchid='$branchid' AND supplierid='$supplierid'";
          }
      $select=mysqli_query($con,$query);
      $json2="";
      while($rows = mysqli_fetch_assoc($select)){
            $json="";
            $purchaseid=$rows['purchaseid'];
            $select2=mysqli_query($con,"SELECT * FROM purchase WHERE purchaseid='$purchaseid'");
            while($rowss = mysqli_fetch_assoc($select2)){
            $branchid=$rowss['branchid'];
            $poforbranch=$rowss['poforbranch'];
            $purchaseid=$rowss['purchaseid'];
            $podate=$rowss['podate'];
            $supplierid=$rowss['supplierid'];
            $sbranch=$rowss['sbranch'];
            $itemname=$rowss['itemname'];
            $itemcode=$rowss['itemcode'];
            $qty=$rowss['qty'];
            $unit=$rowss['unit'];
            $rate=$rowss['rate'];
            $descper=$rowss['descper'];
            $descamt=$rowss['descamt'];
            $amount=$rowss['amount'];
            $cgstper=$rowss['cgstper'];
            $sgstper=$rowss['sgstper'];
            $igstper=$rowss['igstper'];
            $cgstamt=$rowss['cgstamt'];
            $sgstamt=$rowss['sgstamt'];
            $igstamt=$rowss['igstamt'];
            $netamount=$rowss['netamount']; 
            $podate=$rowss['podate'];
            $json.=',{"itemname":"'.$itemname.'","itemcode":"'.$itemcode.'","qty":"'.$qty.'","unit":"'.$unit.'","rate":"'.$rate.'","descper":"'.$descper.'","descamt":"'.$descamt.'","amount":"'.$amount.'","cgstper":"'.$cgstper.'","sgstper":"'.$sgstper.'","igstper":"'.$igstper.'","cgstamt":"'.$cgstamt.'","sgstamt":"'.$sgstamt.'","igstamt":"'.$igstamt.'","netamount":"'.$netamount.'"}';
            }
            $json=substr($json,1);
            $json='['.$json.']';
            $branchname = mysqli_fetch_assoc(mysqli_query($con,"SELECT branchname X FROM branchmaster WHERE branchid='$poforbranch'"))['X']; 
            $suppliername= mysqli_fetch_assoc(mysqli_query($con, "SELECT suppliername X FROM `suppliermaster` WHERE supplierid='$supplierid'"))['X'];
            $json2.=',{"branchid":"'.$branchid.'","branchname":"'.$branchname.'","podate":"'.$podate.'","suppliername":"'.$suppliername.'","purchaseid":"'.$purchaseid.'","items":'.$json.'}';
      }
        $json2=substr($json2,1);
        $json2='['.$json2.']';

    echo '{"status":"success","json2":'.$json2.'}';
    _close($con);
    }else{
        echo '{"status":"falid"}';
     }
?>