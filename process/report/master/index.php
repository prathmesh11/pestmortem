<?php
$base='../../../';
$js='<script src="'.$base.'js/jquery.table2excel.min.js"></script>';  
include($base.'_in/header.php');
include($base.'_in/connect.php');
$con=_connect();
if (session_status() == PHP_SESSION_NONE) { session_start(); }
$branchid = $_SESSION['branchid'];

?>
<style>
    .table-list td,
    .table-list th {
        border: 1px solid #ddd;
        padding: 1px !important;
        font-size: 13px;
    }
    .table-list td {
        padding-top: 10px !important;
    }
    .table-list tr:nth-child(even) {
        background-color: #f2f2f2;
    }
    .table-list th {
        padding-top: 5px;
        padding-bottom: 5px;
        text-align: center;
        background-color: #16a085;
        color: white;
    }

    /* #section{
        margin:0px 1px 10px;
    } */
    h2{
        margin-left:650px;
    }
 
</style>
<input type="hidden" id="inp-branch" value='<?php echo $branchid; ?>' />
 <div class="container-fluid ">
 <div class="content">

    <div class="row">
        <div class="col-sm-10">
            <h2>Employee List</h2>
            <div class="row">
            <div class="col-sm-3">
                <label for="">Branch Name</label>
                <div class="form-group">
                    <select class="form-control input-sm" data-role="select" data-name="branchid" id="branchid">
                    <option value="Select">Select</option>
                        <?php 
                                $result1=mysqli_query($con, "SELECT * FROM `branchmaster`");
                                while($rows=mysqli_fetch_assoc($result1)) {
                                echo '<option value="'.$rows['branchid'].'">'.$rows['branchname'].'</option>';
                            }
                        ?>
                    </select>
                </div>
            </div>
            <div class="col-sm-3">
                <label for="">Company Name</label>
                <div class="form-group">
                    <select class="form-control input-sm" data-role="select" data-name="groupCode" id="groupCode">
                    <option value="Select">Select</option>
                        <?php 
                                $result2=mysqli_query($con, "SELECT groupName,groupCode FROM `additionalmaster` WHERE mainGroupName='COMPANY'");
                                while($rows=mysqli_fetch_assoc($result2)) {
                                echo '<option value="'.$rows['groupCode'].'">'.$rows['groupName'].'</option>';
                            }
                        ?>
                    </select>
                </div>
            </div>
            <div class="col-sm-3" style="margin-top:20px;">
                <button class="btn btn-success btn-block" id="btn-submit" onclick="submit()">Submit</button>
            </div>
         </div>
        </div>
        <div class="col-sm-2" style="margin-top:20px;">
            <button class="btn btn-sm btn-success exportToExcel">Download</button>
        </div>
    </div>

        <hr style="margin: 2px;">
        <table class="table-list table table2excel table2excel_with_colors hidden" id="table">
            <thead>
                <tr>
                    <th>Sr No.</th>
                    <th>Branch</th>
                    <th>Employee Name</th>
                    <th>Company</th>
                    <th>Department</th>
                    <th>Designation</th>
                    <th>Salary</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
 </div>
 </div>

<?php
include($base.'_in/footer.php');
?>
<script>
function submit(){
    var data=$('#branchid').val();
    var groupCode=$('#groupCode').val();
    $.ajax({
            type: "POST",
            data: 'branchid=' + data +'&groupCode='+groupCode,
            url: 'select.php',
            cache: false,
            success: function (res) {
                if (res.status == 'success') {
                    var json=res.json;
                    var count=0;
                    $('#table').removeClass('hidden');
                    $("#table tbody tr").remove();
                    for(var k in json){
                        count++;
                        $("#table").find('tbody').append($('<tr><td>'+count+'</td><td>'+json[k].branchname+'</td><td>'+json[k].username+'</td><td>'+json[k].company+'</td><td>'+json[k].department+'</td><td>'+json[k].designation+'</td><td>Basic:'+json[k].basic+'<br>DA:'+json[k].das+'<br>HRA:'+json[k].hra+'<br>Total Salary:'+json[k].totalsalary+'<br>Bonus:'+json[k].bonus+'<br>TOTAL CTC:'+json[k].totalctc+'<br></td></tr>'));
                    }
                }
            }
        });
}

$(function () {
    $(".exportToExcel").click(function (e) {
        var table = $(this).prev('.table2excel');
        console.log(table);
        $('.table2excel').table2excel({
            exclude: ".noExl",
            name: "Employee List",
            filename: "employeelist" + new Date().toISOString().replace(/[\-\:\.]/g, "") + ".xls",
            fileext: ".xls",
            exclude_img: true,
            exclude_links: true,
            exclude_inputs: true,
            preserveColors: true
        })
    })
})
</script>