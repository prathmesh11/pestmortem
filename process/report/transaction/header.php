<?php

$css = '<link rel="stylesheet" href="'.$base.'css/classic.css">
<link rel="stylesheet" href="'.$base.'css/classic.date.css">
<link rel="stylesheet" href="'.$base.'css/classic.time.css">
<link href="'.$base.'/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" href="'.$base.'css/grid.min.css">';

$js = '<script src="'.$base.'js/picker.js"></script>
<script src="'.$base.'js/picker.date.js"></script>
<script src="'.$base.'js/picker.time.js"></script>
<script src="'.$base.'js/pdfmake.min.js"></script><script src="'.$base.'js/vfs_fonts.js"></script>
<script src="'.$base.'/js/select2.min.js"></script>';
    include($base.'_in/header.php');
    include($base.'_in/connect.php');
    $con = _connect();

?>
<div class="container-fluid">
    <div id="div-content" class="content">
<table width="100%" >
<tr>
<td align="center" style="<?php echo $ALPCertification; ?>width:15%"><a href="/process/certificate/certificate.php?certPage=ALP" style="border:1px solid blue;border-radius:0px;<?php echo $navenq1;?>" class="btn btn-primary btn-block">ALP Certification</td>
<td align="center" style="<?php echo $MBRCertification; ?>width:15%"><a href="/process/certificate/certificate.php?certPage=MBR" style="border:1px solid blue;border-radius:0px;<?php echo $navenq2;?>" class="btn btn-primary btn-block">MBR Certification</td>
<td align="center" style="<?php echo $AFASCertification; ?>width:15%"><a href="/process/certificate/certificate.php?certPage=AFAS" style="border:1px solid blue;border-radius:0px;<?php echo $navenq3;?>" class="btn btn-primary btn-block">AFAS Certification</td>
<td align="center" style="<?php echo $WPMCertification; ?>width:15%"><a href="/process/certificate/certificate.php?certPage=WPM" style="border:1px solid blue;border-radius:0px;<?php echo $navenq4;?>" class="btn btn-primary btn-block">WPM Certification</td>
<td align="center" style="<?php echo $Draft; ?>width:10%"><a href="/process/certificate/draft.php" style="border:1px solid blue;border-radius:0px;<?php echo $navenq5;?>" class="btn btn-primary btn-block">Draft</td>
<td align="center" style="<?php echo $materialissue; ?>width:20%"><a href="/process/certificate/materialissue.php" style="border:1px solid blue;border-radius:0px;<?php echo $navenq6;?>" class="btn btn-primary btn-block">Calculation</td>
<td align="center" style="<?php echo $reports; ?>width:20%"><a href="/process/report/transaction/index.php" style="border:1px solid blue;border-radius:0px;<?php echo $navenq7;?>" class="btn btn-primary btn-block">Reports</td>
</tr>
</table>




