<?php
$base='../../../';
    include($base.'_in/connect.php');
    header('content-type: application/json; charset=utf-8');
    header("access-control-allow-origin: *");
    
    if(isset($_POST['country']) && isset($_POST['exporter1']) && isset($_POST['cargo1'])) {
      $con=_connect();
      $country =_clean($con,$_POST["country"]);
      $exporter1 =_clean($con,$_POST["exporter1"]);
      $cargo1 =_clean($con,$_POST["cargo1"]);
      $query='';

      if($country=='Select'){$country='';}
      if($exporter1=='Select'){$exporter1='';}
      if($cargo1=='Select'){$cargo1='';}

      if($country=='' && $exporter1=='' && $cargo1!=''){
        //001
        $query= "SELECT * FROM `certificatemaster` WHERE confirm='y' AND cargo1='$cargo1' ORDER BY id DESC";
      }
      if($country=='' && $exporter1!='' && $cargo1==''){
        //010
        $query= "SELECT * FROM `certificatemaster` WHERE confirm='y' AND exporter1='$exporter1' ORDER BY id DESC";
      }
      if($country=='' && $exporter1!='' && $cargo1!=''){
        //011
        $query= "SELECT * FROM `certificatemaster` WHERE confirm='y' AND exporter1='$exporter1' AND cargo1='$cargo1' ORDER BY id DESC";
      }
      if($country!='' && $exporter1=='' && $cargo1==''){
        //100
        $query= "SELECT * FROM `certificatemaster` WHERE confirm='y' AND country='$country' ORDER BY id DESC";
      }
      if($country!='' && $exporter1=='' && $cargo1!=''){
        //101
        $query= "SELECT * FROM `certificatemaster` WHERE confirm='y' AND country='$country' AND cargo1='$cargo1' ORDER BY id DESC";
      }
      if($country!='' && $exporter1!='' && $cargo1==''){
        //110
        $query= "SELECT * FROM `certificatemaster` WHERE confirm='y' AND country='$country' AND exporter1='$exporter1' ORDER BY id DESC";
      }
      if($country!='' && $exporter1!='' && $cargo1!=''){
        //111
        $query= "SELECT * FROM `certificatemaster` WHERE confirm='y' AND country='$country' AND exporter1='$exporter1' AND cargo1='$cargo1' ORDER BY id DESC";
      }
     

      //echo $query;
      $select=mysqli_query($con,$query);
      $json="";
      while($rows = mysqli_fetch_assoc($select)){
        $id=$rows['id'];
        $srNo=$rows['srNo'];
        $certType=$rows['certType'];
        $branchid=$rows['branchid'];
        $enquiryids=$rows['enquiryid'];
        $exporter=$rows['exporter1'];
        $importer=$rows['consignee'];
        $consignee=$rows['consignee1'];
        //$country=$rows['country'];
        $portofdischarge=$rows['loading'];
        $applieddose=$rows['fumdosage'];
        $issuedate=$rows['issuedate'];
        //$certificateno=$rows['certificateno'];
        $performIn=$rows['performIn'];
        if($performIn=='wpm'){
          $query2="SELECT certificateno,country FROM `certificatemaster` WHERE wpnrefno='$id'";
          $result2 = mysqli_query($con,$query2);
          $certificateno="";
          $country="";
          while($rowss = mysqli_fetch_assoc($result2)){
            $certificateno=$certificateno.''.$rowss['certificateno'].', ';
            $country=$country.''.$rowss['country'].', ';
          }
      }else{
          $certificateno=$rows['certificateno'];
          $country=$rows['country'];
         }

        $json.=',{"id":"'.$id.'","performIn":"'.$performIn.'","certificateno":"'.$certificateno.'","branchid":"'.$branchid.'","srNo":"'.$srNo.'","certType":"'.$certType.'","enquiryids":"'.$enquiryids.'","exporter":"'.$exporter.'","importer":"'.$importer.'","consignee":"'.$consignee.'","country":"'.$country.'","portofdischarge":"'.$portofdischarge.'","applieddose":"'.$applieddose.'","issuedate":"'.$issuedate.'"}';
      }
        $json=substr($json,1);
        $json='['.$json.']';
      echo '{"status":"success","json":'.$json.'}';
    _close($con);
    }else{
        echo '{"status":"falid"}';
     }
?>