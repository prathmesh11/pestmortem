<?php

$css = '<link rel="stylesheet" href="'.$base.'css/classic.css">
<link rel="stylesheet" href="'.$base.'css/classic.date.css">
<link rel="stylesheet" href="'.$base.'css/classic.time.css">
<link href="'.$base.'/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" href="'.$base.'css/alart.css">
<link rel="stylesheet" href="'.$base.'css/grid.min.css">';

$js = '<script src="'.$base.'js/picker.js"></script>
<script src="'.$base.'js/picker.date.js"></script>
<script src="'.$base.'js/picker.time.js"></script>
<script src="'.$base.'js/pdfmake.min.js"></script>
<script src="'.$base.'js/vfs_fonts.js"></script>
<script src="'.$base.'/js/select2.min.js"></script>
<script src="'.$base.'../js/alart.js"></script>
<script src="'.$base.'js/fwork.js"></script>';
    include($base.'_in/header.php');
    include($base.'_in/connect.php');
    $con = _connect();

 
    if (session_status() == PHP_SESSION_NONE) { session_start(); }
    $branchid = $_SESSION['branchid'];
  
    $Mbr_certificateno1   = mysqli_fetch_assoc(mysqli_query($con,"SELECT certificateno x  FROM `certificatemaster` WHERE certType='MBR' AND certificateno<>'' AND performIn<>'WPM'  AND wpmcreated<>'Y' AND  isdraft<>'Y' ORDER BY id DESC LIMIT 1"))['x'];
    $mbr_pieces = explode("/", $Mbr_certificateno1);
    $Mbr_certificateno=$mbr_pieces[3];


    $Wpm_certificateno1   = mysqli_fetch_assoc(mysqli_query($con,"SELECT certificateno x FROM `certificatemaster` WHERE certType='MBR' AND certificateno<>'' AND performIn='WPM' AND isdraft<>'Y' ORDER BY id DESC LIMIT 1"))['x'];
    $wpm_pieces = explode("/", $Wpm_certificateno1);
    $Wpm_certificateno=$wpm_pieces[3];


    $Alp_certificateno1   = mysqli_fetch_assoc(mysqli_query($con,"SELECT certificateno x  FROM `certificatemaster` WHERE certType='ALP' AND certificateno<>'' AND wpmcreated<>'Y' AND isdraft<>'Y'  ORDER BY id DESC LIMIT 1"))['x'];
    $Alp_pieces = explode("/", $Alp_certificateno1);
    $Alp_certificateno=$Alp_pieces[3];


    $Afas_certificateno1   = mysqli_fetch_assoc(mysqli_query($con,"SELECT certificateno x  FROM `certificatemaster` WHERE certType='AFAS' AND certificateno<>'' AND wpmcreated<>'Y' AND isdraft<>'Y'  ORDER BY id DESC LIMIT 1"))['x'];
    $Afas_pieces = explode("/", $Afas_certificateno1);
    $Afas_certificateno=$Afas_pieces[3];

    
?>
<style>
    .th{
        background:#8e44ad;
        color:#fff;
        text-align:center;
        padding-top:2px;
        padding-bottom:2px;
        border:1px solid #fff;
    }
    .td{
        border:1px solid #ddd;
    }
    .table-ui .btn{
        margin:3px;
    }
    #myModal1 .col-sm-4,.col-sm-8{
      margin-bottom:2px;
    }
    .picker__select--year{
        height:auto;
    }
    .picker__select--month{
        height:auto;
    }
    .table-list td,
   .table-list th {
       border: 1px solid #ddd;
       padding: 1px !important;
       font-size: 13px;
   }
   .table-list td{
	padding-top: 10px !important;
   }

   .table-list tr:nth-child(even) {
       background-color: #f2f2f2;
   }

   .table-list th {
       padding-top: 5px;
       padding-bottom: 5px;
       text-align: center;
       background-color: #16a085;
       color: white;
   }
</style>
<div class="container-fluid">
    <div id="div-content" class="content">
<table width="100%" >
<tr>
<!-- <td align="center" style="<?php echo $ALPCertification; ?>width:15%"><a href="/process/certificate/certificate.php?certPage=ALP" style="border:1px solid blue;border-radius:0px;<?php echo $navenq1;?>" class="btn btn-primary btn-block">ALP Certification <?php echo $Alp_certificateno;?></td>
<td align="center" style="<?php echo $MBRCertification; ?>width:15%"><a href="/process/certificate/certificate.php?certPage=MBR" style="border:1px solid blue;border-radius:0px;<?php echo $navenq2;?>" class="btn btn-primary btn-block">MBR Certification <?php echo $Mbr_certificateno;?></td>
<td align="center" style="<?php echo $AFASCertification; ?>width:15%"><a href="/process/certificate/certificate.php?certPage=AFAS" style="border:1px solid blue;border-radius:0px;<?php echo $navenq3;?>" class="btn btn-primary btn-block">AFAS Certification <?php echo $Afas_certificateno;?></td>
<td align="center" style="<?php echo $WPMCertification; ?>width:15%"><a href="/process/certificate/certificate.php?certPage=WPM" style="border:1px solid blue;border-radius:0px;<?php echo $navenq4;?>" class="btn btn-primary btn-block">WPM Certification <?php echo $Wpm_certificateno;?></td>
<td align="center" style="<?php echo $Draft; ?>width:10%"><a href="/process/certificate/draft.php" style="border:1px solid blue;border-radius:0px;<?php echo $navenq5;?>" class="btn btn-primary btn-block">Draft</td> -->
<td align="center" style="<?php echo $Calculation; ?>"><a href="/process/calculation/materialissue.php" style="border:1px solid blue;border-radius:0px;<?php echo $navenq6;?>" class="btn btn-primary btn-block">Calculation</td>
<td align="center" style="<?php echo $reports; ?>"><a href="/process/report/transaction/index.php" style="border:1px solid blue;border-radius:0px;<?php echo $navenq7;?>" class="btn btn-primary btn-block">Reports</td>
</tr>
</table>
