<?php
 $base='../../../';
 $navenq7='background:#1B1464;';
    include('header.php');
    if (session_status() == PHP_SESSION_NONE) { session_start(); }
    $branchid = $_SESSION['branchid'];
?>
<style>
    .table-list td,
    .table-list th {
        border: 1px solid #ddd;
        padding: 1px !important;
        font-size: 13px;
    }
    .table-list td {
        padding-top: 10px !important;
    }
    .table-list tr:nth-child(even) {
        background-color: #f2f2f2;
    }
    .table-list th {
        padding-top: 5px;
        padding-bottom: 5px;
        text-align: center;
        background-color: #16a085;
        color: white;
    }
    h2{
        margin-left:500px;
    }
    th {
    background: #8e44ad;
    color: #fff;
    text-align: center;
    padding-top: 2px;
    padding-bottom: 2px;
    border: 1px solid #fff;
    }
</style>
<input type="hidden" id="inp-branch" value='<?php echo $branchid; ?>' />
<div class="">
    <div class="">
  
        <hr class="style-hr">
        <div class="row">
            <div class="col-sm-2">
                <b style="padding-left:20px;">Branch Name :</b>
                    <div class="form-group">
                        <select class="form-control input-md select-js1"  data-role="select" data-name="branchid" id="branchid">
                            <option value="Select">Select</option>
                            <?php
                                    $result1=mysqli_query($con, "SELECT branchname,branchid FROM `branchmaster`");
                                    while($rows=mysqli_fetch_assoc($result1)) {
                                        echo '<option value="'.$rows['branchid'].'">'.$rows['branchname'].'</option>';
                                    }
                                ?> 
                        </select>
                    </div>
            </div>
            <div class="col-sm-2">
            <b style="padding-left:20px;">Certificate Number :</b>
                <div class="form-group">
                <select class="form-control input-md select-js1"  data-role="select" data-name="certificateno" id="certificateno">
                        <option value="Select">Select</option>
                        <?php
                                $result1=mysqli_query($con, "SELECT certificateno FROM `certificatemaster` WHERE confirm='y' AND calculated='y' ORDER BY id DESC");
                                while($rows=mysqli_fetch_assoc($result1)) {
                                    echo '<option value="'.$rows['certificateno'].'">'.$rows['certificateno'].'</option>';
                                }
                            ?> 
                    </select>
                </div>
            </div>
            <div class="col-sm-2">
                <b style="padding-left:20px;">From Date :</b>
                <div class="col-sm-12">
                    <div class="form-group"><input type="date" class="form-control" data-name="fromdate" id="fromdate">
                    </div>
                </div>
            </div>
            <div class="col-sm-2">
                <b style="padding-left:20px;">To Date :</b>
                <div class="col-sm-12">
                    <div class="form-group"><input type="date" class="form-control" data-name="todate" id="todate">
                    </div>
                </div>
            </div>
            <div class="col-sm-2">
                <div class="form-group">
                <b style="padding-left:20px;">Certificate Type :</b>
                    <select class="form-control input-md select-js1" data-role="select" data-name="certificatetype" id="certificatetype">
                        <option value="Select">Select</option>
                        <option value="ALP">ALP</option>
                        <option value="MBR">MBR</option>
                        <option value="AFAS">AFAS</option>
                    </select>
                </div>
            </div>
            <br>
            <div class="col-sm-2">
            <br>
                <a class="btn btn-primary btn-md btn-block" onclick="submit()">Submit</a>
                    <!-- "this.href='index.php?fromdate=' + document.getElementById('fromdate').value + '&todate=' + document.getElementById('todate').value + '&certificate=' + document.getElementById('certificate').value; return true" -->
                    
            </div>
        </div>
        <div class="row">
            <div class="col-sm-2">
                <b style="padding-left:20px;">Name of Country :</b>
                    <div class="form-group">
                        <select class="form-control input-md select-js1"  data-role="select" data-name="country" id="country">
                            <option value="Select">Select</option>
                            <?php
                                    $result1=mysqli_query($con, "SELECT DISTINCT country FROM `certificatemaster` WHERE country<>'' AND calculated='y' ORDER BY id DESC");
                                    while($rows=mysqli_fetch_assoc($result1)) {
                                        echo '<option value="'.$rows['country'].'">'.$rows['country'].'</option>';
                                    }
                                ?> 
                        </select>
                    </div>
            </div>
            <div class="col-sm-2">
            <b style="padding-left:20px;">Exporter Name :</b>
                <div class="form-group">
                <select class="form-control input-md select-js1"  data-role="select" data-name="exporter1" id="exporter1">
                        <option value="Select">Select</option>
                        <?php
                                $result1=mysqli_query($con, "SELECT DISTINCT exporter1 FROM `certificatemaster` WHERE exporter1<>'' AND calculated='y' ORDER BY id DESC");
                                while($rows=mysqli_fetch_assoc($result1)) {
                                    echo '<option value="'.$rows['exporter1'].'">'.$rows['exporter1'].'</option>';
                                }
                            ?> 
                    </select>
                </div>
            </div>
            <div class="col-sm-2">
                <div class="form-group">
                <b style="padding-left:20px;">Name of Cargo :</b>
                    <select class="form-control input-md select-js1" data-role="select" data-name="cargo1" id="cargo1">
                        <option value="Select">Select</option>
                        <?php
                                $result1=mysqli_query($con, "SELECT DISTINCT cargo1 FROM `certificatemaster` WHERE cargo1<>'' AND calculated='y' ORDER BY id DESC");
                                while($rows=mysqli_fetch_assoc($result1)) {
                                    echo '<option value="'.$rows['cargo1'].'">'.$rows['cargo1'].'</option>';
                                }
                            ?> 
                    </select>
                </div>
            </div>
            <br>
            <div class="col-sm-2">
            <br>
                <a class="btn btn-primary btn-md btn-block" onclick="submit2()">Submit</a>
                    <!-- "this.href='index.php?fromdate=' + document.getElementById('fromdate').value + '&todate=' + document.getElementById('todate').value + '&certificate=' + document.getElementById('certificate').value; return true" -->
                    
            </div>
        </div>
        <hr style="margin: 2px;">
        <table class="table" id="table" style="border: 1px solid #ddd;">
            <thead>
                <tr>
                <th width="125">Sr. No.</th>
                <th width="180">Certificate Type Sr. No.</th>
                <th class="text-center">Summary</th>
                <th width="150">Certificate Type</th>
                <th width="120">Action</th>
                </tr>
            </thead>
            <tbody>

            </tbody>
        </table>
    </div>
</div>


<?php
include($base.'_in/footer.php');
?>
<script>
$(document).ready(function() {
    $('.select-js1').select2({width: '100%'});
    $('.select').attr('style','width:100%!important;');
    $.ajax({
        type: "POST",
        data:{
            branchid:$('#inp-branch').val(),
        },
        url: 'select3.php',
        cache: false,
        success: function (res) {
            if(res.status=='success'){
                var json=res.json;
                var count=0;
               $("#table tbody").empty();
                for(var i in json){
                    var str='';
                    count++;
                    str+='<tr style="border: 1px solid #ddd;">';
                    str+='<td style="border: 1px solid #ddd;">'+json[i].id+'</td>';
                    str+='<td style="border: 1px solid #ddd;">Certificate No.<BR>'+json[i].certificateno+'<BR><BR>Sr. No. '+json[i].srNo+'</td>';
                    str+='<td style="border: 1px solid #ddd;">Name of Exporter:'+json[i].exporter +'<br>';
                    if(json[i].certType!="AFAS"){
                        str+='Name of Consignee:'+json[i].consignee +'<br>';
                    }else{
                        str+='Name of Importer:'+json[i].consignee +'<br>';
                    }
                    if(json[i].certType!="AFAS"){
                        str+='Country:'+json[i].country+'<br>';
                    }
                    str+='Port Of Discharge:'+json[i].portofdischarge +'<br>';
                    str+='Date Of Issue:'+json[i].issuedate;
                    if(json[i].certType=="AFAS" || json[i].certType=="MBR"){
                       str+='<BR>Performed In : '+json[i].performIn+'<br>';
                    }else{
                        str+='<BR>Using : '+json[i].performIn+'<br>';
                    }
                    str+='</td>';
                    str+='<td style="border: 1px solid #ddd;">'+json[i].certType+'</td>';
                    str+='<td style="border: 1px solid #ddd;">';
                    str+='<a class="btn btn-sm btn-block btn-primary" onclick="printer_'+json[i].certType+'('+json[i].id+','+json[i].branchid+')">View PDF</a>';
                    if(json[i].certType=="AFAS"){
                    str+='<a class="btn btn-sm  btn-block btn-success" href="../../certificate/certificate/afascertificate.php?id='+json[i].id+'&type='+json[i].certType+'&performIn='+json[i].performIn+'&confirmedit=true">Edit & recal</a>';
                    str+='<a class="btn btn-sm  btn-block btn-warning" href="../../certificate/certificate/afascertificate.php?id='+json[i].id+'&type='+json[i].certType+'&performIn='+json[i].performIn+'&copy=true" > Copy Certificate </a>';
                    }
                    if(json[i].certType=="ALP"){
                    str+='<a class="btn btn-sm  btn-block btn-success" href="../../certificate/certificate/alpcertificate.php?id='+json[i].id+'&type='+json[i].certType+'&performIn='+json[i].performIn+'&confirmedit=true"> Edit & recal </a>';
                    str+='<a class="btn btn-sm  btn-block btn-warning" href="../../certificate/certificate/alpcertificate.php?id='+json[i].id+'&type='+json[i].certType+'&performIn='+json[i].performIn+'&copy=true" > Copy Certificate </a>';
                    }
                    if(json[i].certType=="MBR"){
                        if(json[i].performIn=="wpm"){
                            //str+='<a class="btn btn-sm  btn-block btn-success" href="../../certificate/certificate/mbrcertificate.php?id='+json[i].id+'&type='+json[i].certType+'&performIn='+json[i].performIn+'"> Edit</a>';
                        }else{
                            str+='<a class="btn btn-sm  btn-block btn-success" href="../../certificate/certificate/mbrcertificate.php?id='+json[i].id+'&type='+json[i].certType+'&performIn='+json[i].performIn+'&confirmedit=true"> Edit & recal </a>';
                            str+='<a class="btn btn-sm  btn-block btn-warning" href="../../certificate/certificate/mbrcertificate.php?id='+json[i].id+'&type='+json[i].certType+'&performIn='+json[i].performIn+'&copy=true" > Copy Certificate </a>';
                        }
                    }
                    str+='</td>';
                    str+='</tr>'; 
                    str+='</tbody>'; 
                    str+='</table>';
                    $("#table > tbody").append(str);
                }
                    
               // window.location= "../purchase-order.php";
            }
        }
	});
            
});

function submit2(){
    $.ajax({
        type: "POST",
        data:{
            country:$('#country').val(),
            exporter1:$('#exporter1').val(),
            cargo1:$('#cargo1').val(),
        },
        url: 'select2.php',
        cache: false,
        success: function (res) {
            if(res.status=='success'){
                var json=res.json;
                var count=0;
               $("#table tbody").empty();
                for(var i in json){
                    var str='';
                    count++;
                    str+='<tr style="border: 1px solid #ddd;">';
                    str+='<td style="border: 1px solid #ddd;">'+json[i].id+'</td>';
                    str+='<td style="border: 1px solid #ddd;">Certificate No.<BR>'+json[i].certificateno+'<BR><BR>Sr. No. '+json[i].srNo+'</td>';
                    str+='<td style="border: 1px solid #ddd;">Name of Exporter:'+json[i].exporter +'<br>';
                    if(json[i].certType!="AFAS"){
                        str+='Name of Consignee:'+json[i].consignee +'<br>';
                    }else{
                        str+='Name of Importer:'+json[i].consignee +'<br>';
                    }
                    if(json[i].certType!="AFAS"){
                        str+='Country:'+json[i].country+'<br>';
                    }
                    str+='Port Of Discharge:'+json[i].portofdischarge +'<br>';
                    str+='Date Of Issue:'+json[i].issuedate+'</td>';
                    str+='<td style="border: 1px solid #ddd;">'+json[i].certType+'</td>';
                    str+='<td style="border: 1px solid #ddd;">';
                    str+='<a class="btn btn-sm btn-block btn-primary" onclick="printer_'+json[i].certType+'('+json[i].id+','+json[i].branchid+')">View PDF</a>';
                    if(json[i].certType=="AFAS"){
                    str+='<a class="btn btn-sm  btn-block btn-success" href="../../certificate/certificate/afascertificate.php?id='+json[i].id+'&type='+json[i].certType+'&performIn='+json[i].performIn+'&confirmedit=true">Edit & recal</a>';
                    str+='<a class="btn btn-sm  btn-block btn-warning" href="../../certificate/certificate/afascertificate.php?id='+json[i].id+'&type='+json[i].certType+'&performIn='+json[i].performIn+'&copy=true" > Copy Certificate </a>';
                    }
                    if(json[i].certType=="ALP"){
                    str+='<a class="btn btn-sm  btn-block btn-success" href="../../certificate/certificate/alpcertificate.php?id='+json[i].id+'&type='+json[i].certType+'&performIn='+json[i].performIn+'&confirmedit=true">Edit & recal</a>';
                    str+='<a class="btn btn-sm  btn-block btn-warning" href="../../certificate/certificate/alpcertificate.php?id='+json[i].id+'&type='+json[i].certType+'&performIn='+json[i].performIn+'&copy=true" > Copy Certificate </a>';
                    }
                    if(json[i].certType=="MBR"){
                        if(json[i].performIn=="wpm"){
                            //str+='<a class="btn btn-sm  btn-block btn-success" href="../../certificate/certificate/mbrcertificate.php?id='+json[i].id+'&type='+json[i].certType+'&performIn='+json[i].performIn+'"> Edit</a>';
                        }else{
                            str+='<a class="btn btn-sm  btn-block btn-success" href="../../certificate/certificate/mbrcertificate.php?id='+json[i].id+'&type='+json[i].certType+'&performIn='+json[i].performIn+'&confirmedit=true"> Edit & recal </a>';
                            str+='<a class="btn btn-sm  btn-block btn-warning" href="../../certificate/certificate/mbrcertificate.php?id='+json[i].id+'&type='+json[i].certType+'&performIn='+json[i].performIn+'&copy=true" > Copy Certificate </a>';
                        }
                    }
                    str+='</td>';
                    str+='</tr>'; 
                    str+='</tbody>'; 
                    str+='</table>';
                    $("#table > tbody").append(str);
                }
                    
               // window.location= "../purchase-order.php";
            }
        }
	});


 }


 function submit(){
    $.ajax({
        type: "POST",
        data:{
            branchid:$('#branchid').val(),
            certificateno:$('#certificateno').val(),
            fromdate:$('#fromdate').val(),
            todate:$('#todate').val(),
            certificatetype:$('#certificatetype').val(),
        },
        url: 'select.php',
        cache: false,
        success: function (res) {
            if(res.status=='success'){
                var json=res.json;
                var count=0;
               $("#table tbody").empty();
                for(var i in json){
                    var str='';
                    count++;
                    str+='<tr style="border: 1px solid #ddd;">';
                    str+='<td style="border: 1px solid #ddd;">'+json[i].id+'</td>';
                    str+='<td style="border: 1px solid #ddd;">Certificate No.<BR>'+json[i].certificateno+'<BR><BR>Sr. No. '+json[i].srNo+'</td>';
                    str+='<td style="border: 1px solid #ddd;">Name of Exporter:'+json[i].exporter +'<br>';
                    if(json[i].certType!="AFAS"){
                        str+='Name of Consignee:'+json[i].consignee +'<br>';
                    }else{
                        str+='Name of Importer:'+json[i].consignee +'<br>';
                    }
                    if(json[i].certType!="AFAS"){
                        str+='Country:'+json[i].country+'<br>';
                    }
                    str+='Port Of Discharge:'+json[i].portofdischarge +'<br>';
                    str+='Date Of Issue:'+json[i].issuedate+'</td>';
                    str+='<td style="border: 1px solid #ddd;">'+json[i].certType+'</td>';
                    str+='<td style="border: 1px solid #ddd;">';
                    str+='<a class="btn btn-sm btn-block btn-primary" onclick="printer_'+json[i].certType+'('+json[i].id+','+json[i].branchid+')">View PDF</a>';
                    if(json[i].certType=="AFAS"){
                    str+='<a class="btn btn-sm  btn-block btn-success" href="../../certificate/certificate/afascertificate.php?id='+json[i].id+'&type='+json[i].certType+'&performIn='+json[i].performIn+'&confirmedit=true">Edit & recal</a>';
                    str+='<a class="btn btn-sm  btn-block btn-warning" href="../../certificate/certificate/afascertificate.php?id='+json[i].id+'&type='+json[i].certType+'&performIn='+json[i].performIn+'&copy=true" > Copy Certificate </a>';
                    }
                    if(json[i].certType=="ALP"){
                    str+='<a class="btn btn-sm  btn-block btn-success" href="../../certificate/certificate/alpcertificate.php?id='+json[i].id+'&type='+json[i].certType+'&performIn='+json[i].performIn+'&confirmedit=true">Edit & recal</a>';
                    str+='<a class="btn btn-sm  btn-block btn-warning" href="../../certificate/certificate/alpcertificate.php?id='+json[i].id+'&type='+json[i].certType+'&performIn='+json[i].performIn+'&copy=true" > Copy Certificate </a>';
                    }
                    if(json[i].certType=="MBR"){
                        if(json[i].performIn=="wpm"){
                           // str+='<a class="btn btn-sm  btn-block btn-success" href="../../certificate/certificate/mbrcertificate.php?id='+json[i].id+'&type='+json[i].certType+'&performIn='+json[i].performIn+'"> Edit</a>';
                        }else{
                            str+='<a class="btn btn-sm  btn-block btn-success" href="../../certificate/certificate/mbrcertificate.php?id='+json[i].id+'&type='+json[i].certType+'&performIn='+json[i].performIn+'&confirmedit=true"> Edit & recal </a>';
                            str+='<a class="btn btn-sm  btn-block btn-warning" href="../../certificate/certificate/mbrcertificate.php?id='+json[i].id+'&type='+json[i].certType+'&performIn='+json[i].performIn+'&copy=true" > Copy Certificate </a>';
                        }
                    }
                    str+='</td>';
                    str+='</tr>'; 
                    str+='</tbody>'; 
                    str+='</table>';
                    $("#table > tbody").append(str);
                }
            }
        }
	});


 }
 function printer_ALP(certid,branchid) {
   var jsondata = $.parseJSON($.ajax({
         type: "POST",
        data: {
          certid: certid
        },
        url: '../../certificate/api/addjson.php',
        cache: false,
        success: function (res) {
        //jsondata =JSON.stringify(res.json);
         },
        dataType: "json", 
        async: false
    }).responseText);
    jsondata =JSON.stringify(jsondata);
    data1 = JSON.parse(jsondata);  
    json1 = data1.json;   

    var brjsondata = $.parseJSON($.ajax({
            type: "POST",
            data: {
              certid: certid,branchid:branchid
            },
            url: '../../certificate/api/getbrjson.php',
            cache: false,
            success: function (res) {
            //jsondata =JSON.stringify(res.json);
            },
            dataType: "json", 
            async: false
        }).responseText);
    brjsondata =JSON.stringify(brjsondata);
    brdata = JSON.parse(brjsondata);  
    brjson = brdata.branchjson;   


    //alert(data1.status);
    var fumdate = new Date(json1.fumdate);
    var issuedate = new Date(json1.issuedate);

    fumdate = ('0' + fumdate.getDate()).slice(-2) + "/" + ('0' + (fumdate.getMonth()+1)).slice(-2) + "/" + fumdate.getFullYear();
    issuedate = ('0' + issuedate.getDate()).slice(-2)+ "/" +  ('0' + (issuedate.getMonth()+1)).slice(-2) + "/" + issuedate.getFullYear();

    var fullDate = new Date();
    var twoDigitMonth = ((fullDate.getMonth().length + 1) === 1) ? (fullDate.getMonth() + 1) : '' + (fullDate.getMonth() + 1);
    var currentDate = fullDate.getDate() + "/" + twoDigitMonth + "/" + fullDate.getFullYear();
    var content=[]; 

    certno = 'PMI/'+brjson.branchshortname+'/ALP/'+json1.srno+'/'+fullDate.getFullYear().toString().substr(2,2)+'-'+((fullDate.getFullYear()+1).toString().substr(2,2));
                    dd = {
                    pageSize: 'A4',
                    pageMargins: [40, 30, 30, 40],
                    footer: function (currentPage, pageCount) {
                        return {
                            // margin: 10,
                            // columns: [{
                            //     fontSize: 8,
                            //     text: [{
                            //             text: '--------------------------------------------------------------------------' +
                            //                 '\n',
                            //             margin: [0, 20]
                            //         },
                            //         {
                            //             text: '©Pest Mortem (India) PVT. LTD. || Printed On ' +currentDate ,
                            //         }
                            //     ],
                            //     alignment: 'center'
                            // }]
                        };

                       },
    content: [
	    '\n',
            {
                text: 'PEST MORTEM (INDIA) PRIVATE LIMITED',fontSize: 14,bold: true,alignment: 'center',margin:[0,5,0,0]
            },
            {
                text: '(Approved by National Plant Protection Organization, Government of India)',
                 fontSize: 8, bold: true, alignment: 'center',
            },
            {
                text: 'AN ISO 9001:2008 CERTIFIED COMPANY',
                 fontSize: 8,alignment: 'center',margin:[0,0,0,10]
            },
            
	        {     margin:[0,-4,0,0],
                table:{
                widths:[510],
                body:[
                    [{margin:[-5,-3,-5,-3],
                      border:[0,0,0,0],
    	                table:{
    	                widths:["*"],
    	                body:[
    	                   [{text: 'FUMIGATION CERTIFICATE',fontSize: 12,bold: true,alignment: 'center',margin:[0,0,0,0],border:[0,0,0,0],},]
    	                    ]
        	            },
	            },
	            ],
    	        [{
    	            margin:[-5,-3,-5,-3],
    	            table:{
    	                widths:['auto',10,'auto','*'],
    	                body:[
        	            [
        	            {stack: [{text: [
                        {text:'PEST MORTEM (INDIA) PRIVATE LIMITED\n',style: 'left',bold:1},
                        {text:brjson.address+'\n',style:'left',bold:1},
                        {text:'Dte. PPQS Regd. No.\t030/ALP/DATED : 20.04.2012  ',style:'left',bold:1},
                        
                        ],}],border:[0,0,0,0]
        	            },
    	                {
    	                stack: [{text: [
                        {text:'\n',style:'left'},
                        {text:'\n',style:'left'},
                        {text:'\n',style:'left'},
                        ],}],border:[0,0,1,0]   
    	                },
    	                {
    	                stack: [{text: [
                        {text:'Treatment Cert. No. : \n',style: 'left',bold:1},
                        {text:'\n',style:'left'},
                        {text:'Date of Issue :\n',style: 'left',bold:1}
                        ],}],border:[0,0,0,0]   
    	                },
    	                {
    	                stack: [{text: [
                        {text:certno+'\n',style: 'left',bold:1},
                        {text:'\n',style:'left'},
                        {text:issuedate+'\n',style: 'left',bold:1}
                        ],}],border:[0,0,0,0]   
    	                },
    	                ]
    	                    ]
    	            },layout: {
        				hLineColor: function (i, node) {
        					return (i === 0 || i === node.table.body.length) ? '#778899' : '#778899';
        				},
        				vLineColor: function (i, node) {
        					return (i === 0 || i === node.table.widths.length) ? '#778899' : '#778899';
        				},
        	        }
    	        }],
    	        [
    	           {
    	                stack: [{text: [
    	                    {text:'\tThis is to certify that the goods described below were treated in accordance with the fumigation treatment requirements of importing country ',fontSize: 8,italics: true},
                            {text: json1.country.toUpperCase(), italics: false, fontSize: 9,bold:1},
                            {text:' and declared that the consignment has been verified free of impervious surfaces/layers such as plastic wrapping or laminated plastic films, lacquered or painted surfaces, aluminium foil, tarred or waxed papers etc. that may adversely effect the penetration of the fumigant, prior to fumigation.',fontSize: 8,italics: true},
                            {text:'\nThe Certificate is valid for the consignments shipped within 21 days from the date of completion of fumigation',style: '',color:'',fontSize: 8,},
                        ],}],border:[1,1,1,1]   
    	                }, 
    	       ],
    	        [{
    	            margin:[-5,-3,-5,-3],
    	            table:{
    	                widths:[165,'*'],
    	                
    	                body:[
    	                [{text:'Details of Treatment\n',style:'left',colSpan: 2,bold:1},{},],
    	                [{text:'Name of Fumigant\n',style:'left'},{text:'ALUMINIUM PHOSPHIDE\n',style:'left'},],
    	                [{text:'Date of Fumigation\n',style:'left'},{text:fumdate+'\n',style:'left'},],
    	                [{text:'Place of Fumigation\n',style:'left'},{text:json1.fumplace+'\n',style:'left'},],
    	                [{text:'Dosage of Fumigant\n',style:'left'},{text:json1.fumdosage+' GMS / M.T\n',style:'left'},],
    	                [{text:'Duration of Fumigation (in days)\n',style:'left'},{text:json1.fumdurationdays+' / '+json1.fumdurationhrs+'\n',style:'left'},],
    	                [{text:'Average ambient humidity during \nfumigation (RH)\n',style:'left'},{text:json1.humidity+' %\n',style:'left'},],
    	                [{text:'Fumigation performed under gas tight sheets\n',style:'left'},{text:json1.fumperformed+'\n',style:'left',bold:1},],
    	                [{text:'Description of Goods\n',style:'left',colSpan: 2,bold:1},{text:'\n',style:'left'},],
    	                [{text:'Container Number (or numerical link).\n/Seal Number\n',style:'left'},
    	                {stack: [{text: [
    	                    {text:json1.containerno1+'\n',style:'left'},
    	                    {text:json1.containerno2+'\n',style:'left',},
    	                    {text:json1.containerno3+'\n',style:'left',},
    	                    {text:json1.containerno4+'\n',style:'left',},
    	                    ]}]}
    	                ],
    	                [{text:'Name & Address of Exporter\n',style:'left'},
    	                {stack: [{text: [
    	                    {text:json1.exporter1+'\n',style:'left'},
    	                    {text:json1.exporter2+'\n',style:'left',},
    	                    {text:json1.exporter3+'\n',style:'left',},
    	                    ]}]}
    	                    ],
                      [{text:'Name and Address of Importer\n',style:'left'},
    	                 {stack: [{text: [
    	                    {text:json1.consignee1+'\n',style:'left'},
    	                    {text:json1.consignee2+'\n',style:'left',},
    	                    {text:json1.consignee3+'\n',style:'left',},
    	                    ]}]}
    	                ],
                      [{text:'Name and Address of notified party\n',style:'left'},
    	                 {stack: [{text: [
    	                    {text:json1.notifiedparty1+'\n',style:'left'},
    	                    {text:json1.notifiedparty2+'\n',style:'left',},
    	                    {text:json1.notifiedparty3+'\n',style:'left',},
    	                    ]}]}
    	                ],
    	                
    	                [{text:'Type and description of cargo\n',style:'left'},
    	                 {stack: [{text: [
    	                    {text:json1.cargo1+'\n',style:'left'},
    	                    {text:json1.cargo2+'\n',style:'left',},
    	                    {text:json1.cargo3+'\n',style:'left',},
    	                    ]}]}
    	                ],
    	                [{text:'Quantity (MTs)/ No of packages/No\n of pieces',style:'left'},
    	                {stack: [{text: [
    	                    {text:json1.qty+'\n',style:'left'},
                          {text:json1.qty2+'\n',style:'left'},
    	                    ]}]}
    	                ],
    	                [{text:'Description of packaging material\n',style:'left'},
    	                {stack: [{text: [
    	                    {text:json1.description+'\n',style:'left'},
    	                    {text:json1.description1+'\n',style:'left',},
    	                    ]}]}
    	                ],
        	            [{text:'Shipping mark or brand\n',style:'left'},
        	            {stack: [{text: [
    	                    {text:json1.shipingmark+'\n',style:'left'},
    	                    {text:json1.description2+'\n',style:'left',},
    	                    ]}]}
        	            ],
                      [{text:'Port & country Of Loading\n',style:'left'},
                      {text:json1.loading+'\n',style:'left'},],
                      [{text:'Port of Discharge\n',style:'left'},
                      {text:json1.entryport+'\n',style:'left'},],
                      [{text:'Name of the Vessel\n',style:'left'},
                      {text:json1.vesselname+'\n',style:'left'},],
                      [{text:'Additional Declaration\n',style:'left'},
        	            {stack: [{text: [
    	                    {text:json1.additional+'\n',style:'left'},
    	                    {text:json1.additional1+'\n',style:'left',},
                          {text:json1.additional2+'\n',style:'left',},
    	                    ]}]}
        	            ],
                      [{text:'Name &,Signature of Accredited\nFumigation Operator with seal & date/\nAccreditation Number\n',style:'left',bold:1},
                      {stack: [{text: [
                        {text:json1.accname+'\n',style:'left'},
    	                    {text:json1.accno+'\n',style:'left',},
                          {text:'Date:'+issuedate+'\n',style:'left',},
    	                    ]}]}
                          ],
    	                ],
    	            },layout: {
        				hLineColor: function (i, node) {
        					return (i === 0 || i === node.table.body.length) ? '#778899' : '#778899';
        				},
        				vLineColor: function (i, node) {
        					return (i === 0 || i === node.table.widths.length) ? '#778899' : '#778899';
        				},
        	        }
    	        }
    	       ],
    	       
	        ]
		},layout: {

				hLineColor: function (i, node) {
					return (i === 0 || i === node.table.body.length) ? '#778899' : '#778899';
				},
				vLineColor: function (i, node) {
					return (i === 0 || i === node.table.widths.length) ? '#778899' : '#778899';
				},
	        }
		
		},
		{text:'\n\n'},
		
		
		],
		styles: {
        left:{
            fontSize: 8,
    		alignment:'left',
        },
        right:{
            fontSize: 8,
    		alignment:'right',
        },
		centerbold: {
			fontSize: 8,
			alignment:'center',
			bold:1
		},
		center: {
			fontSize: 8,
			alignment:'center',
		},
		bn: {
			fontSize: 8,
			border:[0,0,0,0],
		},
    }

                               }
             pdfMake.createPdf(dd).open();
}
function printer_AFAS(certid,branchid) {
 
   var jsondata = $.parseJSON($.ajax({
         type: "POST",
        data: {
          certid: certid
        },
        url: '../../certificate/api/addjson.php',
        cache: false,
        success: function (res) {
        //jsondata =JSON.stringify(res.json);
         },
        dataType: "json", 
        async: false
    }).responseText);
  jsondata =JSON.stringify(jsondata);
  data1 = JSON.parse(jsondata);  
  json1 = data1.json;      
  //alert(data1.status);

  var brjsondata = $.parseJSON($.ajax({
          type: "POST",
          data: {
            certid: certid,branchid:branchid
          },
          url: '../../certificate/api/getbrjson.php',
          cache: false,
          success: function (res) {
          //jsondata =JSON.stringify(res.json);
          },
          dataType: "json", 
          async: false
      }).responseText);
  brjsondata =JSON.stringify(brjsondata);
  brdata = JSON.parse(brjsondata);  
  brjson = brdata.branchjson;   
  var fumdate = new Date(json1.fumdate);
  var issuedate = new Date(json1.issuedate);
  var fumdate1 = new Date(json1.fumdate1);
  fumdate1 = fumdate1.getDate()+ "/" + (fumdate1.getMonth()+1) + "/" + fumdate1.getFullYear();
  fumdate = ('0' + fumdate.getDate()).slice(-2) + "/" + ('0' + (fumdate.getMonth()+1)).slice(-2) + "/" + fumdate.getFullYear();
    issuedate = ('0' + issuedate.getDate()).slice(-2)+ "/" +  ('0' + (issuedate.getMonth()+1)).slice(-2) + "/" + issuedate.getFullYear();
  var fullDate = new Date();
  var twoDigitMonth = ((fullDate.getMonth().length + 1) === 1) ? (fullDate.getMonth() + 1) : '' + (fullDate.getMonth() + 1);
  var currentDate = fullDate.getDate() + "/" + twoDigitMonth + "/" + fullDate.getFullYear();
  var content=[]; 
  certno = 'PMI/'+brjson.branchshortname+'/AFAS/'+json1.srno+'/'+fullDate.getFullYear().toString().substr(2,2)+'-'+((fullDate.getFullYear()+1).toString().substr(2,2));
                  dd = {
                    pageSize: 'A4',
                    pageMargins: [50, 30, 30, 40],
                    footer: function (currentPage, pageCount) {
                        return {
                            // margin: 10,
                            // columns: [{
                            //     fontSize: 8,
                            //     text: [{
                            //             text: '--------------------------------------------------------------------------' +
                            //                 '\n',
                            //             margin: [0, 20]
                            //         },
                            //         {
                            //             text: '©Pest Mortem (India) PVT. LTD. || Printed On ' +currentDate ,
                            //         }
                            //     ],
                            //     alignment: 'center'
                            // }]
                        };

                       },

                    content: [
                            '\n',
                            {
                                text: 'PEST MORTEM (INDIA) PRIVATE LIMITED',
                                 fontSize: 14,
                                      bold: true,
                                      alignment: 'center',
                            },
                            {
                                    text: '(Approved by National Plant Protection Organization, Government of India)',
                                     fontSize: 8,
                                      bold: true,
                                      alignment: 'center',
                                },
  
                                {
                                    text: brjson.address,
                                     fontSize: 8,
                                       alignment: 'center',bold:1,
                                },
 
                                {
                                    text: 'AN ISO 9001:2008 CERTIFIED COMPANY',
                                     fontSize: 8,
                                      alignment: 'center',
                                },
 
                                 {
                                    text: 'AFAS-METHYL BROMIDE FUMIGATION CERTIFICATE',
                                    fontSize: 12,
                                      bold: true,
                                      alignment: 'center',
                                },
 
                                {
                                     border : 0,
                                    bold: true,
                                     fontSize: 8,
                                    margin: [0, 5, 0, 5],
                                    table: {
                                         widths: [80, 340, 100],
                                   
                                        body: [
                                            ['CERTIFICATE NO. :', certno.toUpperCase(), 'AEI NO : IN 0012 MB'],
                                        ]
                                    },
                                        layout: 'noBorders'
                                },
                                 {
                                    text: 'TARGET OF FUMIGATION DETAILS',
                                    fontSize: 8,
                                      bold: true,
                                      alignment: 'center',
                                },
                                {canvas: [{ type: 'line', x1: 0, y1: 5, x2: 595-2*40, y2: 5, lineWidth: 0.1 }]},
                                
                                
                                {
                                   border : 0, 
                                     fontSize: 8,
                                    margin: [0, 5, 0, 5],
                                    table: {
                                         widths: [81, 140 ,70,'*'],
                                   
                                        body: [
                                          ['Target of Fumigation  ',': '+ json1.fumtarget,'Commodity ', ': '+json1.commodify],
                                          ['Port of Loading',': '+ json1.loading,'',': '+ json1.commodify1],
                                          ['Country Of Origin  ', ': '+json1.countryorg,'Quantity ',': '+json1.qty1],
                                          ['Port of Discharge ', ': '+json1.portofdischarge,'',{text:': '+json1.qty2,border: [false, false, false, false]}],
                                          ['Country Of Destination',': '+ json1.destinationcon,'',{text:': '+json1.qty3,border: [false, false, false, false]}],
                                          ['Vessel',': '+json1.afasvessel,'Consignment Link ',': '+ json1.consignment],
                                          ['','','',': '+ json1.consignment2,],
                                           ]
                                     },
                                        layout: 'noBorders'
                                  },
                                  {
                                    fontSize: 8,
                                    margin: [0, 5, 0, 5],
                                    table: {
                                         widths: [250, '*'],
                                   
                                        body: [
                                            [
                                            { border : [false, true, true, false], 
                                            text: 'Name and address of exporter', fontSize: 8,bold:true
                                            },
                                             {
                                               border : [false, true, false, false], 
                                              text: 'Name and address of importer',  fontSize: 8,bold:true}],
                  
                                          [
                                          {
                                            border : [false, false, true, false],
                                            text: json1.exporter1, fontSize: 8
                                          },
                                            {
                                              border : [true, false, false, false],
                                              text: json1.consignee1, fontSize: 8
                                            }],
                                             [
                                          {
                                            border : [false, false, true, false],
                                            text: json1.exporter2, fontSize: 8
                                          },
                                            {
                                              border : [false, false, false, false],
                                              text: json1.consignee2, fontSize: 8
                                            }],
                                             [
                                          {
                                            border : [false, false, true, false],
                                            text: json1.exporter3, fontSize: 8
                                          },
                                            {
                                              border : [false, false, false, false],
                                              text: json1.consignee3, fontSize: 8
                                            }],
                                            [
                                          {
                                            border : [false, false, true, true],
                                            text: json1.exporter4, fontSize: 8
                                          },
                                            {
                                              border : [false, false, false, true],
                                              text: json1.consignee4, fontSize: 8
                                            }]
                                         
                                            
                                        ]
                                    },
                                          
                                },


                                  {
                                    text: 'TREATMENT DETAILS',
                                    fontSize :8,
                                    bold:true,
                                    alignment : 'center',
                                },
                                {
                                    border : 0, 
                                     fontSize: 8,
                                    margin: [0, 5, 0, 5],
                                    table: {
                                         widths: [120, '*', 100, '*'],
                                   
                                        body: [
                                            ['Date of fumigation complieted  ', ': '+fumdate1, 'Place of fumigation ',': '+json1.fumplace],
                                            ['DAFF Prescribes dose rate ',': '+ json1.daffdosage, 'Exposure Period ',': '+json1.fumdurationhrs+' HOURS'],
                                            ['Mini. Air temp(Deg. Cent.) ',': '+json1.mintemp,'Applied dose rate ',': '+ json1.fumdosage], 
                                               
                                        ]
                                    },
                                        layout: 'noBorders'
                                },
                                 {canvas: [{ type: 'line', x1: 0, y1: 5, x2: 595-2*40, y2: 5, lineWidth: 1 }]},
                                {
                                    border : 0, 
                                     fontSize: 8,
                                    margin: [0, 5, 0, 5],
                                    table: {
                                         widths: [200, '*'],
                                   
                                        body: [
                                            ['How was the fumigation conducted :', json1.fumconducted],
                                            ['Container number (where applicable) : ', json1.containerno1 ], 
                                               
                                        ]
                                    },
                                        layout: 'noBorders'
                                },
                                  {canvas: [{ type: 'line', x1: 0, y1: 5, x2: 595-2*40, y2: 5, lineWidth: 1 }]},
                                   {
                                    border : 0, 
                                     fontSize: 8,
                                    margin: [0, 5, 0, 5],
                                    table: {
                                         widths: [300, '*'],
                                   
                                        body: [
                                            ['Does the target of the fumigation conform to the DAFF plastic wrapping, Impervious surface and timber thickness requirements at the time of fumgation? : ', json1.targetfum],
                                             
                                            
                                               
                                        ]
                                    },
                                        layout: 'noBorders'
                                },
                                {canvas: [{ type: 'line', x1: 0, y1: 5, x2: 595-2*40, y2: 5, lineWidth: 1 }]},
                                '\n',
                                {
                                  text  : [{text:'Ventilation\t\t\t',fontSize: 8,bold:1},{text:'FINAL TLV READING (PPM)',fontSize: 8},{text:'\t\t\t\t\t\t'+json1.ventilation,fontSize: 8},{text:'\t\t\t\t\t\t\t(Not required for stack or pernanent chamber fumigation)',fontSize: 8}],
                                },
                                   
                                  {canvas: [{ type: 'line', x1: 0, y1: 5, x2: 595-2*40, y2: 5, lineWidth: 1 }]},
                                  '\n',
                                   {
                                    text: 'DECLARATION',
                                    fontSize :8,
                                    bold : true,
                                    alignment : 'center',
                                  },
                                  '\n',
                                    {text: 'By signing below, I, the AFAS accredited fumigator responsible, declare the these details are true and correct and the Fumigation has been carried out in accordance with all the requirements in the DAFF Methyl Bromide Fumigation Standard.',
                                     fontSize: 8,
                                  },
                                {canvas: [{ type: 'line', x1: 0, y1: 5, x2: 595-2*40, y2: 5, lineWidth: 1 }]},
                                '\n',
                                {
                                    text: 'ADDITIONAL DECLARATION',
                                    fontSize :8,
                                    bold : true,
                                    alignment : 'center',
                                },
                                {text: json1.additional,
                                 fontSize: 8,
                                },
                                {text: json1.additional1,
                                 fontSize: 8,
                                },
                                {text: json1.additional2,
                                 fontSize: 8,
                                },
                                '\n',
                                {canvas: [{ type: 'line', x1: 0, y1: 5, x2: 595-2*40, y2: 5, lineWidth: 1 }]},
                                '\n','\n','\n','\n',
                                 {
                                    bold: true,
                                 fontSize: 8,
                                margin: [0, 5, 0, 5],
                                    table: {
                                         widths: [120, 250,60],
                                   
                                        body: [
                                            [ {text:'Signature',alignment: 'center'},{text:'Date : '+issuedate,alignment: 'center'},'Company Stamp'],
                                            [ {text:json1.accname,alignment: 'center'},{text:json1.accno,alignment: 'center'},''],
                                            [{text:'Name of Accredited Fumigator',alignment: 'center'},{text:'AFAS-Accreditation Number',alignment: 'center'},''],
                                            ]
                                       },
                                       layout: 'noBorders'
                                    },    
 

                        ]
                     }
                              
            //  pdfMake.createPdf(dd).download("AFAScertificate_"+certid+".pdf");
             pdfMake.createPdf(dd).open();
}
function printer_MBR(certid,branchid) {
 
   var jsondata = $.parseJSON($.ajax({
         type: "POST",
        data: {
          certid: certid
        },
        url: '../../certificate/api/addjson.php',
        cache: false,
        success: function (res) {
       
         },
        dataType: "json", 
        async: false
    }).responseText);
  jsondata =JSON.stringify(jsondata);
  data1 = JSON.parse(jsondata);  
  json1 = data1.json;      

  var brjsondata = $.parseJSON($.ajax({
          type: "POST",
          data: {
            certid: certid,branchid:branchid
          },
          url: '../../certificate/api/getbrjson.php',
          cache: false,
          success: function (res) {
          //jsondata =JSON.stringify(res.json);
          },
          dataType: "json", 
          async: false
      }).responseText);
  brjsondata =JSON.stringify(brjsondata);
  brdata = JSON.parse(brjsondata);  
  brjson = brdata.branchjson;   


  var fumdate = new Date(json1.fumdate);
  var issuedate = new Date(json1.issuedate);

  fumdate = ('0' + fumdate.getDate()).slice(-2) + "/" + ('0' + (fumdate.getMonth()+1)).slice(-2) + "/" + fumdate.getFullYear();
    issuedate = ('0' + issuedate.getDate()).slice(-2)+ "/" +  ('0' + (issuedate.getMonth()+1)).slice(-2) + "/" + issuedate.getFullYear();

  var fullDate = new Date();
  var twoDigitMonth = ((fullDate.getMonth().length + 1) === 1) ? (fullDate.getMonth() + 1) : '' + (fullDate.getMonth() + 1);
  var currentDate = fullDate.getDate() + "/" + twoDigitMonth + "/" + fullDate.getFullYear();
  var content=[]; 
  

  certno = 'PMI/'+brjson.branchshortname+'/MBR/'+json1.srno+'/'+fullDate.getFullYear().toString().substr(2,2)+'-'+((fullDate.getFullYear()+1).toString().substr(2,2));
                 dd = {
                    pageSize: 'A4',
                    pageMargins: [40, 30, 30, 40],
                    //pageMargins: [40, 25, 20, 30],
                    footer: function (currentPage, pageCount) {
                        return {
                            // margin: 10,
                            // columns: [{
                            //     fontSize: 8,
                            //     text: [{
                            //             text: '--------------------------------------------------------------------------' +
                            //                 '\n',
                            //             margin: [0, 20]
                            //         },
                            //         {
                            //             text: '©Pest Mortem (India) PVT. LTD. || Printed On ' +currentDate ,
                            //         }
                            //     ],
                            //     alignment: 'center'
                            // }]
                        };

                       },
            content: [
            '\n',
            {
                text: 'PEST MORTEM (INDIA) PRIVATE LIMITED',fontSize: 14,bold: true,alignment: 'center',margin:[0,5,0,0]
            },
            {
                text: '(Approved by National Plant Protection Organization, Government of India)',
                 fontSize: 8, bold: true, alignment: 'center',
            },
            {
                text: 'AN ISO 9001:2008 CERTIFIED COMPANY',
                 fontSize: 8,alignment: 'center',margin:[0,0,0,10]
            },
	        {
                margin:[0,-4,0,0],
                table:{
                widths:[510],
                body:[
                    [{
                      margin:[-5,-3,-5,-3],
                      border:[0,0,0,0],
    	                table:{
                        
    	                widths:["*"],
    	                body:[
    	                   [{text: 'FUMIGATION CERTIFICATE',fontSize: 12,bold: true,alignment: 'center',margin:[0,0,0,0],border:[0,0,0,0],},]
    	                    ]
        	            },
	            },
	            ],
    	        [{
    	            margin:[-4,-3,-5,-3],
    	            table:{
    	                widths:['auto',10,'auto','*'],
    	                
    	                body:[
        	            [
        	            {stack: [{text: [
                        {text: 'PEST MORTEM (INDIA) PRIVATE LIMITED\n',style: 'left',bold:1},
                        {text:brjson.address+'\n',style:'left',bold:1},
                        {text:'Dte. PPQS Regd. No.\t012/MB/DATED : 25.07.2005  ',style:'left',bold:1},
                        
                        ],}],border:[0,0,0,0]
        	            },
    	                {
    	                stack: [{text: [
                        {text:'\n',style: 'left'},
                        {text:'\n',style:'left'},
                        {text:'\n',style:'left'},
                        ],}],border:[0,0,1,0]   
    	                },
    	                //{text:'',border:[0,0,0,0]},
    	                {
    	                stack: [{text: [
                        {text:'Treatment Cert. No. : \n',style: 'left',bold:1},
                        {text:'\n',style:'left'},
                        {text:'Date of Issue :\n',style: 'left',bold:1}
                        ],}],border:[0,0,0,0]   
    	                },
    	                {
    	                stack: [{text: [
                        {text:certno+'\n',style: 'left',bold:1},
                        {text:'\n',style:'left'},
                        {text:issuedate+'\n',style: 'left',bold:1}
                        ],}],border:[0,0,0,0]   
    	                },
    	                ]
    	                    ]
    	            },layout: {
        				hLineColor: function (i, node) {
        					return (i === 0 || i === node.table.body.length) ? '#778899' : '#778899';
        				},
        				vLineColor: function (i, node) {
        					return (i === 0 || i === node.table.widths.length) ? '#778899' : '#778899';
        				},
        	        }
    	        }],
    	        [
    	           {
    	                stack: [{text: [
    	                    {text:'\tThis is to certify that the goods described below were treated in accordance with the fumigation treatment requirements of importing country ',fontSize: 8,italics: true},
                            {text: json1.country.toUpperCase(), italics: false, fontSize: 9,bold:1},
                            {text:' and declared that the consignment has been verified free of impervious surfaces/layers such as plastic wrapping or laminated plastic films, lacquered or painted surfaces, aluminium foil, tarred or waxed papers etc. that may adversely effect the penetration of the fumigant, prior to fumigation.',fontSize: 8,italics: true},
                            {text:'\nThe Certificate is valid for the consignments shipped within 21 days from the date of completion of fumigation',style: '',color:'',fontSize: 8,},
                        ],}],border:[1,1,1,1]   
    	                }, 
    	       ],
    	        [{
    	            margin:[-5,-3,-5,-3],
    	            table:{
    	                widths:[165,'*'],
    	                
    	                body:[
    	                [{text:'Details of Treatment\n',style:'left',colSpan: 2,bold:1},{},],
    	                [{text:'Name of Fumigant\n',style:'left'},{text:'METHYL BROMIDE\n',style:'left'},],
    	                [{text:'Date of Fumigation\n',style:'left'},{text:fumdate+'\n',style:'left'},],
    	                [{text:'Place of Fumigation\n',style:'left'},{text:json1.fumplace+'\n',style:'left'},],
    	                [{text:'Dosage of Fumigant\n',style:'left'},{text:json1.fumdosage+' GMS/M3\n',style:'left'},],
    	                [{text:'Duration of Fumigation (in HRS)\n',style:'left'},{text:json1.fumdurationhrs+'\n',style:'left'},],
    	                [{text:'Average ambient temp during Fumigation(°C)\n',style:'left'},{text:json1.mintemp+' °C\n',style:'left'},],
    	                [{text:'Fumigation performed under gas tight sheets\n',style:'left'},{text:json1.fumperformed+'\n',style:'left',bold:1},],
                      [{text:'If containers are not fumigated under gastight\nsheets, pressure in seconds decay value (from\n200-100 Pascals in seconds)\n',style:'left'},{text:json1.pressureinsec+'\n',style:'left'},],
    	                [{text:'Description of Goods\n',style:'left',colSpan: 2,bold:1},{text:'\n',style:'left'},],
    	                [{text:'Container Number (or numerical link).\n/Seal Number\n',style:'left'},
    	                {stack: [{text: [
    	                    {text:json1.containerno1+'\n',style:'left'},
    	                    {text:json1.containerno2+'\n',style:'left',},
    	                    {text:json1.containerno3+'\n',style:'left',},
    	                    {text:json1.containerno4+'\n',style:'left',},
    	                    ]}]}
    	                ],
    	                [{text:'Name & Address of Exporter\n',style:'left'},
    	                {stack: [{text: [
    	                    {text:json1.exporter1+'\n',style:'left'},
    	                    {text:json1.exporter2+'\n',style:'left',},
    	                    {text:json1.exporter3+'\n',style:'left',},
    	                    ]}]}
    	                    ],
    	                [{text:'Name & Address of Importer\n',style:'left'},
    	                 {stack: [{text: [
    	                    {text:json1.consignee1+'\n',style:'left'},
    	                    {text:json1.consignee2+'\n',style:'left',},
    	                    {text:json1.consignee3+'\n',style:'left',},
    	                    ]}]}
    	                ],
                      [{text:'Name & Address of notified party\n',style:'left'},
    	                 {stack: [{text: [
    	                    {text:json1.notifiedparty1+'\n',style:'left'},
    	                    {text:json1.notifiedparty2+'\n',style:'left',},
    	                    {text:json1.notifiedparty3+'\n',style:'left',},
    	                    ]}]}
    	                ],
    	                [{text:'Type and description of cargo\n',style:'left'},
    	                 {stack: [{text: [
    	                    {text:json1.description+'\n',style:'left'},
    	                    {text:json1.description1+'\n',style:'left',},
    	                    ]}]}
    	                ],
    	                [{text:'Quantity (MTs)/ No of packages/No\n of pieces',style:'left'},
    	                {stack: [{text: [
    	                    {text:json1.qty1+'\n',style:'left'},
                          {text:json1.qty2,style:'left'},
    	                    ]}]}
    	                ],
    	                [{text:'Description of packaging material\n',style:'left'},
    	                {stack: [{text: [
    	                    {text:json1.description2+'\n',style:'left'},
    	                    {text:json1.description3+'\n',style:'left',},
    	                    ]}]}
    	                ],
        	            [{text:'Shipping mark or brand\n',style:'left'},
        	            {stack: [{text: [
    	                    {text:json1.shipingmark+'\n',style:'left'},
    	                    {text:json1.shipingmark1+'\n',style:'left',},
    	                    ]}]}
        	            ],
                      [{text:'Port & country Of Loading\n',style:'left'},
                      {text:json1.loading+'\n',style:'left'},],
                      [{text:'Port of Discharge\n',style:'left'},
                      {text:json1.entryport+'\n',style:'left'},],
                      [{text:'Name of the Vessel\n',style:'left'},
                      {text:json1.vesselname+'\n',style:'left'},],
                      [{text:'Additional Declaration\n',style:'left'},
        	            {stack: [{text: [
    	                    {text:json1.additional+'\n',style:'left'},
    	                    {text:json1.additional1+'\n',style:'left',},
                          {text:json1.additional2+'\n',style:'left',},
    	                    ]}]}
        	            ],
        	            [{text:'Name &,Signature of Accredited\nFumigation Operator with seal & date/\nAccreditation Number\n',style:'left',bold:1},
                      {stack: [{text: [
                        {text:json1.accname+'\n',style:'left'},
    	                    {text:json1.accno+'\n',style:'left',},
                          {text:'Date:'+issuedate+'\n',style:'left',},
    	                    ]}]}
                          ],
        	       
    	                ],
    	            },layout: {
        				hLineColor: function (i, node) {
        					return (i === 0 || i === node.table.body.length) ? '#778899' : '#778899';
        				},
        				vLineColor: function (i, node) {
        					return (i === 0 || i === node.table.widths.length) ? '#778899' : '#778899';
        				},
        	        }
    	        }
    	       ],
    	       
	        ]
		},layout: {

				hLineColor: function (i, node) {
					return (i === 0 || i === node.table.body.length) ? '#778899' : '#778899';
				},
				vLineColor: function (i, node) {
					return (i === 0 || i === node.table.widths.length) ? '#778899' : '#778899';
				},
	        }
		
		},
		{text:'\n\n'},
		
		
		],
		styles: {
        left:{
            fontSize: 8,
    		alignment:'left',
        },
        right:{
            fontSize: 8,
    		alignment:'right',
        },
		centerbold: {
			fontSize: 8,
			alignment:'center',
			bold:1
		},
		center: {
			fontSize: 8,
			alignment:'center',
		},
		bn: {
			fontSize: 8,
			border:[0,0,0,0],
		},
    }

           }
          

pdfMake.createPdf(dd).open();
}




$(function () {
    $(".exportToExcel").click(function (e) {
        var table = $(this).prev('.table2excel');
        console.log(table);
        $('.table2excel').table2excel({
            exclude: ".noExl",
            name: "Certificate Reports",
            filename: "certificatereports" + new Date().toISOString().replace(/[\-\:\.]/g, "") + ".xls",
            fileext: ".xls",
            exclude_img: true,
            exclude_links: true,
            exclude_inputs: true,
            preserveColors: true
        })
    })
})
</script>