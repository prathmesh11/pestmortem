<?php
$base='../../../';
    include($base.'_in/connect.php');
    header('content-type: application/json; charset=utf-8');
    header("access-control-allow-origin: *");
    
    if(isset($_POST['branchid']) && isset($_POST['certificateno']) && isset($_POST['fromdate']) && isset($_POST['todate']) && isset($_POST['certificatetype'])) {
      $con=_connect();
      $branchid =_clean($con,$_POST["branchid"]);
      $certificateno =_clean($con,$_POST["certificateno"]);
      $fromdate =_clean($con,$_POST["fromdate"]);
      $todate =_clean($con,$_POST["todate"]);
      $certificatetype =_clean($con,$_POST["certificatetype"]);
      $query='';

      if($branchid=='Select'){$branchid='';}
      if($certificateno=='Select'){$certificateno='';}
      if($certificatetype=='Select'){$certificatetype='';}

      if($branchid=='' && $certificateno=='' && $fromdate=='' && $todate=='' && $certificatetype!=''){
        //0001
        $query= "SELECT * FROM `certificatemaster` WHERE calculated='y' AND certType='$certificatetype' ORDER BY id DESC ";
      }
      if($branchid=='' && $certificateno=='' && $fromdate!='' && $todate!='' && $certificatetype==''){
        //0010
        $query= "SELECT * FROM `certificatemaster` WHERE calculated='y' AND issuedate BETWEEN '$fromdate' AND '$todate' ORDER BY id DESC ";
      }
      if($branchid=='' && $certificateno=='' && $fromdate!='' && $todate!='' && $certificatetype!=''){
        //0011
        $query= "SELECT * FROM `certificatemaster` WHERE calculated='y' AND issuedate BETWEEN '$fromdate' AND '$todate' AND certType='$certificatetype' ORDER BY id DESC ";
      }
      if($branchid=='' && $certificateno!='' && $fromdate=='' && $todate=='' && $certificatetype==''){
        //0100
        $query= "SELECT * FROM `certificatemaster` WHERE calculated='y' AND certificateno='$certificateno' ORDER BY id DESC ";
      }
      if($branchid=='' && $certificateno!='' && $fromdate=='' && $todate=='' && $certificatetype!=''){
        //0101
        $query= "SELECT * FROM `certificatemaster` WHERE calculated='y' AND certificateno='$certificateno' AND certType='$certificatetype' ORDER BY id DESC ";
      }
      if($branchid=='' && $certificateno!='' && $fromdate!='' && $todate!='' && $certificatetype!=''){
        //0111
        $query= "SELECT * FROM `certificatemaster` WHERE calculated='y' AND certificateno='$certificateno' AND certType='$certificatetype' AND issuedate BETWEEN '$fromdate' AND '$todate' ORDER BY id DESC ";
      }
      if($branchid!='' && $certificateno=='' && $fromdate=='' && $todate=='' && $certificatetype==''){
        //1000
        $query= "SELECT * FROM `certificatemaster` WHERE calculated='y' AND branchid='$branchid' ORDER BY id DESC ";
      }
      if($branchid!='' && $certificateno=='' && $fromdate=='' && $todate=='' && $certificatetype!=''){
        //1001
        $query= "SELECT * FROM `certificatemaster` WHERE calculated='y' AND branchid='$branchid' AND certType='$certificatetype' ORDER BY id DESC ";
      }
      if($branchid!='' && $certificateno=='' && $fromdate!='' && $todate!='' && $certificatetype!=''){
        //1011
        $query= "SELECT * FROM `certificatemaster` WHERE calculated='y' AND branchid='$branchid' AND certType='$certificatetype' AND issuedate BETWEEN '$fromdate' AND '$todate' ORDER BY id DESC ";
      }
      if($branchid!='' && $certificateno=='' && $fromdate!='' && $todate!='' && $certificatetype==''){
        //1010
        $query= "SELECT * FROM `certificatemaster` WHERE calculated='y' AND branchid='$branchid' AND issuedate BETWEEN '$fromdate' AND '$todate' ORDER BY id DESC ";
      }
      if($branchid!='' && $certificateno!='' && $fromdate=='' && $todate=='' && $certificatetype==''){
        //1100
        $query= "SELECT * FROM `certificatemaster` WHERE calculated='y' AND branchid='$branchid' AND certificateno='$certificateno' ORDER BY id DESC ";
      }
      if($branchid!='' && $certificateno!='' && $fromdate=='' && $todate=='' && $certificatetype!=''){
        //1101
        $query= "SELECT * FROM `certificatemaster` WHERE calculated='y' AND branchid='$branchid' AND certificateno='$certificateno' AND certType='$certificatetype' ORDER BY id DESC ";
      }
      if($branchid!='' && $certificateno!='' && $fromdate!='' && $todate!='' && $certificatetype==''){
        //1110
        $query= "SELECT * FROM `certificatemaster` WHERE calculated='y' AND branchid='$branchid' AND certificateno='$certificateno' AND issuedate BETWEEN '$fromdate' AND '$todate' ORDER BY id DESC ";
      }
      if($branchid=='' && $certificateno!='' && $fromdate!='' && $todate!='' && $certificatetype!=''){
        //1111
        $query= "SELECT * FROM `certificatemaster` WHERE calculated='y' AND branchid='$branchid' AND certificateno='$certificateno' AND certType='$certificatetype' AND issuedate BETWEEN '$fromdate' AND '$todate' ORDER BY id DESC ";
      }

      //echo $query;
      $select=mysqli_query($con,$query);
      $json="";
      while($rows = mysqli_fetch_assoc($select)){
        $id=$rows['id'];
        $srNo=$rows['srNo'];
        $certType=$rows['certType'];
        $branchid=$rows['branchid'];
        $enquiryids=$rows['enquiryid'];
        $exporter=$rows['exporter1'];
        $importer=$rows['consignee'];
        $consignee=$rows['consignee1'];
        //$country=$rows['country'];
        $portofdischarge=$rows['loading'];
        $applieddose=$rows['fumdosage'];
        $issuedate=$rows['issuedate'];
        //$certificateno=$rows['certificateno'];
        $performIn=$rows['performIn'];
        if($performIn=='wpm'){
          $query2="SELECT certificateno,country FROM `certificatemaster` WHERE wpnrefno='$id'";
          $result2 = mysqli_query($con,$query2);
          $certificateno="";
          $country="";
          while($rowss = mysqli_fetch_assoc($result2)){
            $certificateno=$certificateno.''.$rowss['certificateno'].', ';
            $country=$country.''.$rowss['country'].', ';
          }
      }else{
          $certificateno=$rows['certificateno'];
          $country=$rows['country'];
         }

        $json.=',{"id":"'.$id.'","performIn":"'.$performIn.'","certificateno":"'.$certificateno.'","branchid":"'.$branchid.'","srNo":"'.$srNo.'","certType":"'.$certType.'","enquiryids":"'.$enquiryids.'","exporter":"'.$exporter.'","importer":"'.$importer.'","consignee":"'.$consignee.'","country":"'.$country.'","portofdischarge":"'.$portofdischarge.'","applieddose":"'.$applieddose.'","issuedate":"'.$issuedate.'"}';
      }
        $json=substr($json,1);
        $json='['.$json.']';
      echo '{"status":"success","json":'.$json.'}';
    _close($con);
    }else{
        echo '{"status":"falid"}';
     }
?>