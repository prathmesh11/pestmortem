<?php
  $base='../../../../';
  $js='<script src="'.$base.'js/fwork.js"></script>
  <script src="'.$base.'js/alart.js"></script>
  <script src="'.$base.'js/pdfmake.min.js"></script>
  <script src="'.$base.'js/vfs_fonts.js"></script>
  <script src="'.$base.'js/list.min.js"></script>';
  $css='<link rel="stylesheet" href="'.$base.'css/fwork.css">
  <link rel="stylesheet" href="'.$base.'css/grid.min.css">
  <link rel="stylesheet" href="'.$base.'css/alart.css">';
  include($base.'_in/header.php');
  include($base.'_in/connect.php');
  $con=_connect();
  if (session_status() == PHP_SESSION_NONE) { session_start(); }
  $branchid = $_SESSION['branchid'];


?>

<input type="hidden" id="inp-branch" value='<?php echo $branchid; ?>' />
<input type="hidden" id="item-json1" value='<?php echo str_replace("'"," &#39;",$json1); ?>' />
<div class="container-fluid ">
    <div class="content" id="section">

        <div class="row">
            <div class="col-sm-12 text-center">
                <h2>SALARY SUMMARY</h2>
            </div>
        </div>
        <hr style="margin: 2px;">
        <div class="col-sm-12">
            <div class="col-sm-3"></div>
            <div class="col-sm-6">
                <div class="col-sm-5">From Date : </div>
                <div class="col-sm-7">
                    <div class="form-group"><input type="month" data-name='salarydate' class="form-control input-sm"
                            id="salarydate" value="<?php echo date("Y-m"); ?>"></div>
                </div>


                <div class="col-sm-5">Select Branch :</div>
                <div class="col-sm-7">
                    <div class="form-group">
                        <select class="form-control input-sm" data-role="select" data-name="branchid" id="branchid">
                            <option value="Select">Select</option>
                            <?php 
                                    $result1=mysqli_query($con, "SELECT * FROM `branchmaster`");
                                    while($rows=mysqli_fetch_assoc($result1)) {
                                    echo '<option value="'.$rows['branchid'].'">'.$rows['branchname'].'</option>';
                                }
                            ?>
                        </select>
                    </div>
                </div>

                <div class="col-sm-12">
                    <button class="btn btn-success btn-sm btn-block" id="btn-submit" onclick="submit()" style="margin:5px;">Submit</button>
                </div>
            </div>
        </div>
    </div>
</div>

<center>
    <button id="signin-button" class="btn btn-primary btn-lg" onclick="handleSignInClick()">Sign in</button>
</center>

<?php
include($base.'_in/footer.php');
?>

<script>


function submit() {
    var valid = true;
    if (checker('section') != false) {
        valid = valid * true;
    } else {
        valid = valid * false;
    }
    if (valid) {
        var data = checker('section');
        var datastr = JSON.stringify(data);
        $.ajax({
            type: "POST",
            data: {
                data: datastr
            },
            url: 'select.php',
            cache: false,
            success: function (res) {
                if (res.status == 'success') {
                    var json = res.json;
                    var json1 = json[0].json1[0];
                    var json2 = json[0].json2[0];
                    var content=[];
                    content.push(
                      {text: 'Pest Mortem (India) Pvt. Ltd.\nSalary register for Month of Jan-2019',fontSize:12, bold:1, alignment:'left' },
                      {
                          margin:[0,5,0,0],
                          table:{
                          dontBreakRows:true,
                          widths:[12,'*',35,35,45,35,32,35,45,35,35,35,35,35,42,45],
                          body:[
                                  [
                                    {
                                        margin:[-5,-3,-5,-3],
                                        table:{
                                        widths:['*'],
                                        body:[
                                            [{text:'Sr.',border:[0,0,0,0],style:'center'}],
                                            [{text:'No.',border:[0,0,0,0],style:'center'}],
                                            [{text:'',border:[0,0,0,0]}],[{text:'',border:[0,0,0,0]}]
                                            ]
                                        }
                                    },
                                    {text:'Full Name',border:[0,1,0,1],style:'center'},
                                    {
                                        margin:[-5,-3,-5,-3],
                                        table:{
                                        widths:['*'],
                                        body:[
                                            [{text:'Present',border:[0,0,0,1],style:'center'}],
                                            [{text:'P.L.',border:[0,0,0,1],style:'center'}],
                                            [{text:'P.H.',border:[0,0,0,1],style:'center'}],
                                            [{text:'W.off',border:[0,0,0,0],style:'center'}]
                                            ]
                                        },layout: {
                                                hLineColor: function (i, node) {
                                                    return (i === 0 || i === node.table.body.length) ? 'gray' : 'gray';
                                                },
                                            }
                                    },
                                    {
                                        margin:[-5,-3,-5,-3],
                                        table:{
                                        widths:['*'],
                                        body:[
                                            [{text:'Present',border:[0,0,0,0],style:'center'}],
                                            [{text:'Hrs',border:[0,0,0,1],style:'center'}],
                                            [{text:'O.T.',border:[0,0,0,0],style:'center'}],
                                            [{text:'Hrs',border:[0,0,0,0],style:'center'}]
                                            ]
                                        },layout: {
                                                hLineColor: function (i, node) {
                                                    return (i === 0 || i === node.table.body.length) ? 'gray' : 'gray';
                                                },
                                                
                                            }
                                    },
                                    {
                                        margin:[-5,-3,-5,-3],
                                        table:{
                                        widths:['*'],
                                        body:[
                                            [{text:'Basic',border:[0,0,0,0],style:'center'}],
                                            [{text:'+D.A.',border:[0,0,0,1],style:'center'}],
                                            [{text:'H.R.A.',border:[0,0,0,0],style:'center'}],
                                            [{text:'CONV.\nRs.',border:[0,0,0,0],style:'center'}]
                                            ]
                                        },layout: {
                                                hLineColor: function (i, node) {
                                                    return (i === 0 || i === node.table.body.length) ? 'gray' : 'gray';
                                                },
                                            }
                                    },
                                    {
                                        margin:[-5,-3,-5,-3],
                                        table:{
                                        widths:['*'],
                                        body:[
                                            [{text:'O.T.',border:[0,0,0,0],style:'center'}],
                                            [{text:'\n',border:[0,0,0,0],style:'center'}],
                                            [{text:'\n',border:[0,0,0,0],style:'center'}],
                                            [{text:'\nRs.',border:[0,0,0,0],style:'center'}]
                                            ]
                                        }
                                    },
                                    {
                                        margin:[-5,-3,-5,-3],
                                        table:{
                                        widths:['*'],
                                        body:[
                                            [{text:'OTH',border:[0,0,0,0],style:'center'}],
                                            [{text:'Income',border:[0,0,0,0],style:'center'}],
                                            [{text:'\n',border:[0,0,0,0],style:'center'}],
                                            [{text:'\nRs.',border:[0,0,0,0],style:'center'}]
                                            ]
                                        }
                                    },
                                    {
                                        margin:[-5,-3,-5,-3],
                                        table:{
                                        widths:['*'],
                                        body:[
                                            [{text:'TOTAL',border:[0,0,0,0],style:'center'}],
                                            [{text:'\n',border:[0,0,0,0],style:'center'}],
                                            [{text:'\n',border:[0,0,0,0],style:'center'}],
                                            [{text:'\nRs.',border:[0,0,0,0],style:'center' }]
                                            ]
                                        }
                                    },
                                    {
                                        margin:[-5,-3,-5,-3],colSpan:5,style:'center',
                                        table:{
                                        widths:[35,35,35,35,35],
                                        body:[
                                            [
                                            {colSpan:5,style:'center',border:[0,0,0,0],text:'Deduction'},{},{},{},{}],
                                            [{text:'P.F.',border:[0,1,0,0],style:'center'},
                                            {text:'P.T.',border:[1,1,0,0],style:'center'},
                                            {text:'ESIC',border:[1,1,0,0],style:'center'},
                                            {text:'Loan',border:[1,1,0,0],style:'center'},
                                            {text:'ADV.',border:[1,1,0,0],style:'center'},
                                            
                                            ],
                                            [{text:'\n\n',border:[0,0,0,0],style:'center'},{text:'\n\n',border:[1,0,0,0],style:'center'},
                                            {text:'\n\n',border:[1,0,0,0],style:'center'},{text:'\n\n',border:[1,0,0,0],style:'center'},
                                            {text:'\n\n',margin:[-3,-1,-5,-3],border:[1,0,0,0],style:'center'}],
                                            
                                            [{text:'Rs.',border:[0,0,0,0],style:'center'},{text:'Rs.',border:[1,0,0,0],style:'center'},
                                            {text:'Rs.',border:[1,0,0,0],style:'center'},{text:'Rs.',border:[1,0,0,0],style:'center'},
                                            {text:'Rs.',border:[1,0,0,0],style:'center'}],
                                            
                                            ]
                                    },layout: {
                                                hLineColor: function (i, node) {
                                                    return (i === 0 || i === node.table.body.length) ? 'gray' : 'gray';
                                                },
                                                vLineColor: function (i, node) {
                                                    return (i === 0 || i === node.table.widths.length) ? 'gray' : 'gray';
                                                },
                                            }
                                        
                                    },
                                    {text:'',},
                                    {text:'',},
                                    {text:'',},
                                    {text:'',},
                                    {
                                        margin:[-5,-3,-5,-3],
                                        table:{
                                        widths:['*'],
                                        body:[
                                            [{text:'TOTAL',border:[0,0,0,0],style:'center'}],
                                            [{text:'Deduction',border:[0,0,0,0],style:'center'}],
                                            [{text:'\n',border:[0,0,0,0],style:'center'}],
                                            [{text:'\nRs.',border:[0,0,0,0],style:'center'}]
                                            ]
                                        }
                                    },
                                    {
                                        margin:[-5,-3,-5,-3],
                                        table:{
                                        widths:['*'],
                                        body:[
                                            [{text:'Net',border:[0,0,0,0],style:'center'}],
                                            [{text:'Payable',border:[0,0,0,0],style:'center'}],
                                            [{text:'\n',border:[0,0,0,0],style:'center'}],
                                            [{text:'\nRs.',border:[0,0,0,0],style:'center'}]
                                            ]
                                        }
                                    },
                                ],

                                [
                                    {text:'',alignment:'center',fontSize: 9,bold:1},
                                    {text:'Worker\n',fontSize:18,bold:1,alignment:'center'},
                                    {
                                        stack:[
                                        {text:json2.presentdays+'\n',style:'right'},
                                        {text:json2.paidleave+'\n',style:'right'},
                                        {text:json2.casualleave+'\n',style:'right'},
                                        {text:json2.weekoffinmonth+'\n',style:'right'}
                                        ]},
                                    {
                                        stack:[
                                        {text:json2.empworkingovertimehrs+'\n',style:'right'},
                                        {text:'\n',style:'right'},
                                        {text:json2.overtimehrs+'\n',style:'right'},
                                        {text:'\n',style:'right'}
                                        ]},
                                    {
                                        stack:[
                                        {text:(parseFloat(json2.basicpayda)).toFixed(2)+'\n',style:'right'},
                                        {text:'\n',style:'right'},
                                        {text:(parseFloat(json2.hra)).toFixed(2)+'\n',style:'right'},
                                        {text:(parseFloat(json2.conveyance)).toFixed(2)+'\n',style:'right'},
                                        ]},
                                    {text:(parseFloat(json2.otamt)).toFixed(2)+'\n',style:'right'},
                                    {text:(parseFloat(json2.otherincome)).toFixed(2)+'\n',style:'right'},
                                    {text:(parseFloat(json2.totalearning)).toFixed(2)+'\n',style:'right'},
                                    {text:(parseFloat(json2.pfamt)).toFixed(2)+'\n',style:'right'},
                                    {text:(parseFloat(json2.ptamt)).toFixed(2)+'\n',style:'right'},
                                    {text:(parseFloat(json2.esicamt)).toFixed(2)+'\n',style:'right'},
                                    {text:(parseFloat(json2.loandeduc)).toFixed(2)+'\n',style:'right'},
                                    {text:(parseFloat(json2.advancededuction)).toFixed(2)+'\n',style:'right'},
                                    {text:(parseFloat(json2.totaldeduction)).toFixed(2)+'\n',style:'right'},
                                    {text:(parseFloat(json2.netpayment)).toFixed(2)+'\n',style:'right'},
                                ],
                                [
                                    {text:'',alignment:'center',fontSize: 9,bold:1},
                                    {text:'Staff\n',fontSize:18,bold:1,alignment:'center'},
                                    {
                                        stack:[
                                        {text:json1.presentdays+'\n',style:'right'},
                                        {text:json1.paidleave+'\n',style:'right'},
                                        {text:json1.casualleave+'\n',style:'right'},
                                        {text:json1.weekoffinmonth+'\n',style:'right'}
                                        ]},
                                    {
                                        stack:[
                                        {text:json1.empworkingovertimehrs+'\n',style:'right'},
                                        {text:'\n',style:'right'},
                                        {text:json1.overtimehrs+'\n',style:'right'},
                                        {text:'\n',style:'right'}
                                        ]},
                                    {
                                        stack:[
                                        {text:(parseFloat(json1.basicpayda)).toFixed(2)+'\n',style:'right'},
                                        {text:'\n',style:'right'},
                                        {text:(parseFloat(json1.hra)).toFixed(2)+'\n',style:'right'},
                                        {text:(parseFloat(json1.conveyance)).toFixed(2)+'\n',style:'right'},
                                       
                                        ]},
                                    {text:(parseFloat(json1.da)).toFixed(2)+'\n',style:'right'},
                                    {text:(parseFloat(json1.otamt)).toFixed(2)+'\n',style:'right'},
                                    {text:(parseFloat(json1.otherincome)).toFixed(2)+'\n',style:'right'},
                                    {text:(parseFloat(json1.totalearning)).toFixed(2)+'\n',style:'right'},
                                    {text:(parseFloat(json1.pfamt)).toFixed(2)+'\n',style:'right'},
                                    {text:(parseFloat(json1.ptamt)).toFixed(2)+'\n',style:'right'},
                                    {text:(parseFloat(json1.esicamt)).toFixed(2)+'\n',style:'right'},
                                    {text:(parseFloat(json1.loandeduc)).toFixed(2)+'\n',style:'right'},
                                    {text:(parseFloat(json1.advancededuction)).toFixed(2)+'\n',style:'right'},
                                    {text:(parseFloat(json1.totaldeduction)).toFixed(2)+'\n',style:'right'},
                                    {text:(parseFloat(json1.netpayment)).toFixed(2)+'\n',style:'right'},
                                ],
                            ]
                          },layout: {
                            fillColor: function (rowIndex) { 
                                if(rowIndex===0){
                                    return('#0074c1')
                                }
                                if(rowIndex % 2 === 0){
                                    return ('#FAEBD7');
                                }else{
                                    return ('#F5F5F5');
                                }
                                },
                                hLineColor: function (i, node) {
                                    return (i === 0 || i === node.table.body.length) ? 'gray' : 'gray';
                                },
                                vLineColor: function (i, node) {
                                    return (i === 0 || i === node.table.widths.length) ? 'gray' : 'gray';
                                },
                            }
                      }
                    )
                  dd = {
                    pageMargins: [ 40, 60, 40, 20 ],
                    pageOrientation:'landscape',
                    footer: function (currentPage, pageCount) { 
                      return { 
                        margin: 10, 
                        columns: [{ 
                          fontSize: 9, 
                          text: [{ 
                              text: '--------------------------------------------------------------------------' + 
                                '\n', 
                              margin: [0, 20] 
                            }, 
                            { 
                              text: '© Pest Mortem (India) PVT. LTD. | PAGE ' + currentPage.toString() + ' of ' + pageCount, 
                            } 
                          ], 
                          alignment: 'center' 
                        }] 
                      }; 

                    }, 
                    content, 
                    styles: {
                        center:{
                            fontSize: 9,
                            alignment:'center',
                            bold:1,
                            color:'white',
                        },
                        right:{
                            fontSize: 9, 
                            alignment:'right',
                            bold:1,
                            
                        },
                        left:{
                            fontSize: 9,
                            alignment:'left',
                            bold:1,
                            
                        },
                    }
                  } 
                  var win = window.open('', '_blank'); 
                  pdfMake.createPdf(dd).open({}, win); 

              }
              if (res.status == 'falid2') {

              }
            }
          });
        }
  }
</script>
 
     