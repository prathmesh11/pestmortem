<?php  
    $base='../../../../';  
    include($base.'_in/connect.php');  
    header('content-type: application/json; charset=utf-8');  
    header("access-control-allow-origin: *");  
    if(isset($_POST['data'])){  
        $con=_connect();  
        if (session_status()==PHP_SESSION_NONE) { session_start(); }  
        $created_by=$_SESSION['employeeid'];  
        $data=get_object_vars(json_decode($_POST["data"])); 
 
         
        $date=$data['salarydate']; 
        $branchid=$data['branchid'];;  
        $datetime1=strtotime($date);  
        $year=date('y',$datetime1);  
        $salarymonth=date("F", mktime(null, null, null, date('m',$datetime1)));  

        
  
        $result=mysqli_query($con,"SELECT * FROM `salaryprocess` WHERE salarydate='$date' AND branchid='$branchid'");  
                $totalearning;
                while($rows = mysqli_fetch_assoc($result)){ 
                    $employeeid=$rows['employeeid'];

                    $info4=mysqli_fetch_assoc(mysqli_query($con,"SELECT personaljson,employeeid,officialjson FROM salarymaster WHERE employeeid='$employeeid'"));  
                    $personaljson=$info4['personaljson'];  
                    $designation=get_object_vars(json_decode($personaljson))['designation'];
                    if($designation!='24'){
                        $employeeid=$rows['employeeid'];
                        $totalearning+=$rows['totalearning']; 
                        $totaldeduction+=$rows['totaldeduction']; 
                        $netpayment+=$rows['netpayment'];
                        $salarydate=$rows['salarydate'];  
                        $income=$rows['income']; 
                        $dayshrsinfo=$rows['dayshrsinfo'];
                        $deductions=$rows['deductions'];
                        $presentdays+=get_object_vars(json_decode($dayshrsinfo))['presentdays'];
                        $paidleave+=get_object_vars(json_decode($dayshrsinfo))['paidleave'];
                        $casualleave+=get_object_vars(json_decode($dayshrsinfo))['casualleave']; 
                        $weekoffinmonth+=get_object_vars(json_decode($dayshrsinfo))['weekoffinmonth'];
                        $info3=mysqli_fetch_assoc(mysqli_query($con,"SELECT sidejson FROM attendancemaster WHERE employeeid='$employeeid' AND attendancedate='$date'"));  
                        $sidejson=$info3['sidejson'];  
                        $empworkingovertimehrs+=get_object_vars(json_decode($sidejson))['empworkingovertimehrs'];
                        $overtimehrs+=get_object_vars(json_decode($sidejson))['overtimehrs'];
                        $basicpay+=get_object_vars(json_decode($income))['basicpay'];
                        $da+=get_object_vars(json_decode($income))['da'];
                        $conveyance+=get_object_vars(json_decode($income))['conveyance'];
                        $basicpayda+=$basicpay+$da;
                        $hra+=get_object_vars(json_decode($income))['hra'];
                        $otamt+=get_object_vars(json_decode($income))['otamt'];
                        $wash+=get_object_vars(json_decode($income))['wash'];
                        $medical+=get_object_vars(json_decode($income))['medical'];
                        if($wash==""){
                            $wash=0;
                          }
                          if($medical==""){
                            $medical=0;
                          }
                        $otherincome+=$wash+$medical;
                        $ptamt+=get_object_vars(json_decode($deductions))['ptamt'];
                        $pfamt+=get_object_vars(json_decode($deductions))['pfamt'];
                        $esicamt+=get_object_vars(json_decode($deductions))['esicamt'];
                        $advancededuction+=get_object_vars(json_decode($deductions))['advancededuction'];
                        $loandeduc+=get_object_vars(json_decode($deductions))['loandeduc'];
                    }else{
                        $salarydate2=$rows['salarydate']; 
                        $totalearning2+=$rows['totalearning']; 
                        $totaldeduction2+=$rows['totaldeduction']; 
                        $netpayment2+=$rows['netpayment'];
                        $income=$rows['income']; 
                        $dayshrsinfo=$rows['dayshrsinfo'];
                        $deductions=$rows['deductions'];
                        $presentdays2+=get_object_vars(json_decode($dayshrsinfo))['presentdays'];
                        $paidleave2+=get_object_vars(json_decode($dayshrsinfo))['paidleave'];
                        $casualleave2+=get_object_vars(json_decode($dayshrsinfo))['casualleave']; 
                        $weekoffinmonth2+=get_object_vars(json_decode($dayshrsinfo))['weekoffinmonth'];
                        $info3=mysqli_fetch_assoc(mysqli_query($con,"SELECT sidejson FROM attendancemaster WHERE employeeid='$employeeid' AND attendancedate='$date'"));  
                        $sidejson=$info3['sidejson'];  
                        $empworkingovertimehrs2+=get_object_vars(json_decode($sidejson))['empworkingovertimehrs'];
                        $overtimehrs2+=get_object_vars(json_decode($sidejson))['overtimehrs'];
                        $basicpay2+=get_object_vars(json_decode($income))['basicpay'];
                        $da2+=get_object_vars(json_decode($income))['da'];
                        $conveyance2+=get_object_vars(json_decode($income))['conveyance'];
                        $basicpayda2+=$basicpay2+$da2;
                        $hra2+=get_object_vars(json_decode($income))['hra'];
                        $otamt2+=get_object_vars(json_decode($income))['otamt'];
                        $wash2+=get_object_vars(json_decode($income))['wash'];
                        $medical2+=get_object_vars(json_decode($income))['medical'];
                        if($wash2==""){
                            $wash2=0;
                          }
                          if($medical2==""){
                            $medical2=0;
                          }
                        $otherincome2+=$wash2+$medical2;
                        $ptamt2+=get_object_vars(json_decode($deductions))['ptamt'];
                        $pfamt2+=get_object_vars(json_decode($deductions))['pfamt'];
                        $esicamt2+=get_object_vars(json_decode($deductions))['esicamt'];
                        $advancededuction2+=get_object_vars(json_decode($deductions))['advancededuction'];
                        $loandeduc2+=get_object_vars(json_decode($deductions))['loandeduc'];
                    }
                }  
                $json1.=',{"conveyance":"'.$conveyance.'","loandeduc":"'.$loandeduc.'","advancededuction":"'.$advancededuction.'","esicamt":"'.$esicamt.'","pfamt":"'.$pfamt.'","ptamt":"'.$ptamt.'","otherincome":"'.$otherincome.'","otamt":"'.$otamt.'","hra":"'.$hra.'","da":"'.$da.'","basicpayda":"'.$basicpayda.'","basicpay":"'.$basicpay.'","overtimehrs":"'.$overtimehrs.'","empworkingovertimehrs":"'.$empworkingovertimehrs.'","weekoffinmonth":"'.$weekoffinmonth.'","casualleave":"'.$casualleave.'","paidleave":"'.$paidleave.'","presentdays":"'.$presentdays.'","totalearning":"'.$totalearning.'","totaldeduction":"'.$totaldeduction.'","netpayment":"'.$netpayment.'"}'; 
                $json2.=',{"conveyance":"'.$conveyance2.'","loandeduc":"'.$loandeduc2.'","advancededuction":"'.$advancededuction2.'","esicamt":"'.$esicamt2.'","pfamt":"'.$pfamt2.'","ptamt":"'.$ptamt2.'","otherincome":"'.$otherincome2.'","otamt":"'.$otamt2.'","hra":"'.$hra2.'","da":"'.$da2.'","basicpayda":"'.$basicpayda2.'","basicpay":"'.$basicpay2.'","overtimehrs":"'.$overtimehrs2.'","empworkingovertimehrs":"'.$empworkingovertimehrs2.'","weekoffinmonth":"'.$weekoffinmonth2.'","casualleave":"'.$casualleave2.'","paidleave":"'.$paidleave2.'","presentdays":"'.$presentdays2.'","totalearning":"'.$totalearning2.'","totaldeduction":"'.$totaldeduction2.'","netpayment":"'.$netpayment2.'"}'; 
                $json1=substr($json1,1);  
                $json1='['.$json1.']'; 
                $json2=substr($json2,1);  
                $json2='['.$json2.']'; 
                $json=',{"json1":'.$json1.',"json2":'.$json2.'}'; 
                $json=substr($json,1);  
                $json='['.$json.']'; 
        echo '{"status":"success","json":'.$json.'}';  
    _close($con);  
    }else{  
        echo '{"status":"falid"}';  
    }  
  
?>  
