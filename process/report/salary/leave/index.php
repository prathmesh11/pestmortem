<?php
$base='../../../../';
$js='<script src="'.$base.'js/fwork.js"></script>
<script src="'.$base.'js/alart.js"></script>
<script src="'.$base.'js/pdfmake.min.js"></script>
<script src="'.$base.'js/vfs_fonts.js"></script>
<script src="'.$base.'js/list.min.js"></script>';
$css='<link rel="stylesheet" href="'.$base.'css/fwork.css">
<link rel="stylesheet" href="'.$base.'css/grid.min.css">
<link rel="stylesheet" href="'.$base.'css/alart.css">';
include($base.'_in/header.php');
include($base.'_in/connect.php');
$con=_connect();
if (session_status() == PHP_SESSION_NONE) { session_start(); }
$branchid = $_SESSION['branchid'];


?>
<div class="container-fluid ">
    <div class="content" id="section">

        <div class="row">
            <div class="col-sm-12 text-center">
                <h2>Leave Report</h2>
            </div>
        </div>
        <hr style="margin: 2px;">
        <div class="col-sm-12">
            <div class="col-sm-3"></div>
            <div class="col-sm-6">
                <div class="col-sm-5">Select Year : </div>
                <div class="col-sm-7">
                <div class="form-group"><?php 
                    $already_selected_value = 2019;
                    $earliest_year = 2000;
                    print '<select name="some_field" class="form-control input-sm" id="salarydate">';
                    foreach (range(date('Y'), $earliest_year) as $x) {
                        print '<option value="'.$x.'"'.($x === $already_selected_value ? ' selected="selected"' : '').'> Jan,'.$x.' - Dec,'.$x.'</option>';
                    }
                    print '</select>'; 
                ?>
                </div>
                </div>
                

                <div class="col-sm-12">
                    <button class="btn btn-success btn-sm btn-block" id="btn-submit" onclick="submit()"
                        style="margin:5px;">Submit</button>
                </div>
            </div>
            
        </div>
    </div>
</div>
<script>
function submit() {
    var date=$('#salarydate').val();
    
    console.log(date);
    var valid = true;
    var date = $("#salarydate").val();

    $.ajax({
            type: "POST",
            data: 'date=' + date,
            url: 'select.php',
            cache: false,
            success: function (res){
                var content = [];
                content.push(
                    {text:'Pest Mortem (India) Pvt. Ltd.,',style:'left',margin: [0, 0, 0, 7],bold:1},
		            {text:'Leave Balance As On :  Feb 2019,\n\n',style:'left',bold:1},
            
                        {	
                        widths:[],
                        table: {
                            dontBreakRows: true,
                            unbreackable:true,
                            widths:[12,'*',14,20,15,15,16,16,18,16,14,17,16,14,16,16,20],
                            body: [
                                [
                                    {text:'SN',alignment:'center',fontSize: 9,color:'white',rowSpan:2},
                                    {text:'Name',alignment:'center',fontSize: 10,color:'white',bold:1,rowSpan:2},
                                    {text:'',rowSpan:2,color:'white',fontSize: 10,},
                                    {text:'O.P.',alignment:'center',fontSize: 10,color:'white',bold:1,rowSpan:2},
                                    {text:'Leave Record',alignment:'center',fontSize: 10,color:'white',bold:1,colSpan:13},
                                    {},{},{},{},{},{},{},{},{},{},{},{}],
                                    [
                                    {},{},{},{},
                                    {text:'Jan',alignment:'center',color:'white',fontSize: 9,bold:1},
                                    {text:'Feb',alignment:'center',color:'white',fontSize: 9,bold:1},
                                    {text:'Mar',alignment:'center',color:'white',fontSize: 9,bold:1},
                                    {text:'Apr',alignment:'center',color:'white',fontSize: 9,bold:1},
                                    {text:'May',alignment:'center',color:'white',fontSize: 9,bold:1},
                                    {text:'Jun',alignment:'center',color:'white',fontSize: 9,bold:1},
                                    {text:'Jul',alignment:'center',color:'white',fontSize: 9,bold:1},
                                    {text:'Aug',alignment:'center',color:'white',fontSize: 9,bold:1},
                                    {text:'Sep',alignment:'center',color:'white',fontSize: 9,bold:1},
                                    {text:'Oct',alignment:'center',color:'white',fontSize: 9,bold:1},
                                    {text:'Nov',alignment:'center',color:'white',fontSize: 9,bold:1},
                                    {text:'Dec',alignment:'center',color:'white',fontSize: 9,bold:1},
                                    {text:'BAL',alignment:'center',color:'white',fontSize: 9,bold:1},
                                    ],
                                    [
                                    {text:'1',alignment:'center',fontSize: 10},
                                    {text:'Sfadsfd fadfadadfsfdas dfsafdasdf',alignment:'left',fontSize: 10},
                                    {
                                    colSpan:15,
                                    margin:[-5,-2,-5,-3],
                                    table:{
                                        widths:[14,20,15,15,16,16,18,16,14,17,16,14,16,16,20],
                                        body:[
                                            [
                                            {text:'PL',alignment:'center',fontSize: 10,border:[0,0,1,1]},
                                            {text:'320',alignment:'center',fontSize: 10,border:[1,0,1,1]},
                                            {text:'40',alignment:'center',fontSize: 10,border:[1,0,1,1]},
                                            {text:'50',alignment:'center',fontSize: 10,border:[1,0,1,1]},
                                            {text:'60',alignment:'center',fontSize: 10,border:[1,0,1,1]},
                                            {text:'70',alignment:'center',fontSize: 10,border:[1,0,1,1]},
                                            {text:'80',alignment:'center',fontSize: 10,border:[1,0,1,1]},
                                            {text:'90',alignment:'center',fontSize: 10,border:[1,0,1,1]},
                                            {text:'10',alignment:'center',fontSize: 10,border:[1,0,1,1]},
                                            {text:'11',alignment:'center',fontSize: 10,border:[1,0,1,1]},
                                            {text:'12',alignment:'center',fontSize: 10,border:[1,0,1,1]},
                                            {text:'13',alignment:'center',fontSize: 10,border:[1,0,1,1]},
                                            {text:'14',alignment:'center',fontSize: 10,border:[1,0,1,1]},
                                            {text:'15',alignment:'center',fontSize: 10,border:[1,0,1,1]},
                                            {text:'16',alignment:'center',fontSize: 10,border:[1,0,0,1]},
                                            ],
                                            [
                                            {text:'CL',alignment:'center',fontSize: 10,border:[0,0,1,1]},
                                            {text:'32',alignment:'center',fontSize: 10,border:[1,0,1,1]},
                                            {text:'40',alignment:'center',fontSize: 10,border:[1,0,1,1]},
                                            {text:'50',alignment:'center',fontSize: 10,border:[1,0,1,1]},
                                            {text:'60',alignment:'center',fontSize: 10,border:[1,0,1,1]},
                                            {text:'70',alignment:'center',fontSize: 10,border:[1,0,1,1]},
                                            {text:'80',alignment:'center',fontSize: 10,border:[1,0,1,1]},
                                            {text:'90',alignment:'center',fontSize: 10,border:[1,0,1,1]},
                                            {text:'10',alignment:'center',fontSize: 10,border:[1,0,1,1]},
                                            {text:'11',alignment:'center',fontSize: 10,border:[1,0,1,1]},
                                            {text:'12',alignment:'center',fontSize: 10,border:[1,0,1,1]},
                                            {text:'13',alignment:'center',fontSize: 10,border:[1,0,1,1]},
                                            {text:'14',alignment:'center',fontSize: 10,border:[1,0,1,1]},
                                            {text:'15',alignment:'center',fontSize: 10,border:[1,0,1,1]},
                                            {text:'16',alignment:'center',fontSize: 10,border:[1,0,0,1]},
                                            ],
                                            [
                                            {text:'SL',alignment:'center',fontSize: 10,border:[0,0,1,0]},
                                            {text:'32',alignment:'center',fontSize: 10,border:[0,1,1,0]},
                                            {text:'40',alignment:'center',fontSize: 10,border:[0,1,1,0]},
                                            {text:'50',alignment:'center',fontSize: 10,border:[0,1,1,0]},
                                            {text:'60',alignment:'center',fontSize: 10,border:[0,1,1,0]},
                                            {text:'70',alignment:'center',fontSize: 10,border:[0,1,1,0]},
                                            {text:'80',alignment:'center',fontSize: 10,border:[0,1,1,0]},
                                            {text:'90',alignment:'center',fontSize: 10,border:[0,1,1,0]},
                                            {text:'10',alignment:'center',fontSize: 10,border:[0,1,1,0]},
                                            {text:'11',alignment:'center',fontSize: 10,border:[0,1,1,0]},
                                            {text:'12',alignment:'center',fontSize: 10,border:[0,1,1,0]},
                                            {text:'13',alignment:'center',fontSize: 10,border:[0,1,1,0]},
                                            {text:'14',alignment:'center',fontSize: 10,border:[0,1,1,0]},
                                            {text:'15',alignment:'center',fontSize: 10,border:[0,1,1,0]},
                                            {text:'16',alignment:'center',fontSize: 10,border:[0,1,0,0]},
                                            ]
                                            ]
                                    },layout: {
                                    fillColor: function (rowIndex) { 
                                        if(rowIndex===0){
                                            return ('#FFFFE0');
                                        }
                                        if(rowIndex===1){
                                            return ('#F0F8FF');
                                        }
                                        if(rowIndex===2){
                                            return ('#DCDCDC');
                                        }
                                        },
                                        hLineColor: function (i, node) {
                                            return (i === 0 || i === node.table.body.length) ? 'gray' : 'gray';
                                        },
                                        vLineColor: function (i, node) {
                                            return (i === 0 || i === node.table.widths.length) ? 'gray' : 'gray';
                                        },
                                    }
                                    },
                                    ],
                                    [
                                    {text:'2',alignment:'center',fontSize: 10},
                                    {text:'Sfadsfd fadfadadfsfdas dfsafdasdf',alignment:'left',fontSize: 10},
                                    {
                                    colSpan:15,
                                    margin:[-5,-3,-5,-3],
                                    table:{
                                        widths:[14,20,15,15,16,16,18,16,14,17,16,14,16,16,20],
                                        body:[
                                            [
                                            {text:'PL',alignment:'center',fontSize: 10,border:[0,0,1,1]},
                                            {text:'320',alignment:'center',fontSize: 10,border:[1,0,1,1]},
                                            {text:'40',alignment:'center',fontSize: 10,border:[1,0,1,1]},
                                            {text:'50',alignment:'center',fontSize: 10,border:[1,0,1,1]},
                                            {text:'60',alignment:'center',fontSize: 10,border:[1,0,1,1]},
                                            {text:'70',alignment:'center',fontSize: 10,border:[1,0,1,1]},
                                            {text:'80',alignment:'center',fontSize: 10,border:[1,0,1,1]},
                                            {text:'90',alignment:'center',fontSize: 10,border:[1,0,1,1]},
                                            {text:'10',alignment:'center',fontSize: 10,border:[1,0,1,1]},
                                            {text:'11',alignment:'center',fontSize: 10,border:[1,0,1,1]},
                                            {text:'12',alignment:'center',fontSize: 10,border:[1,0,1,1]},
                                            {text:'13',alignment:'center',fontSize: 10,border:[1,0,1,1]},
                                            {text:'14',alignment:'center',fontSize: 10,border:[1,0,1,1]},
                                            {text:'15',alignment:'center',fontSize: 10,border:[1,0,1,1]},
                                            {text:'16',alignment:'center',fontSize: 10,border:[1,0,0,1]},
                                            ],
                                            [
                                            {text:'CL',alignment:'center',fontSize: 10,border:[0,0,1,1]},
                                            {text:'32',alignment:'center',fontSize: 10,border:[1,0,1,1]},
                                            {text:'40',alignment:'center',fontSize: 10,border:[1,0,1,1]},
                                            {text:'50',alignment:'center',fontSize: 10,border:[1,0,1,1]},
                                            {text:'60',alignment:'center',fontSize: 10,border:[1,0,1,1]},
                                            {text:'70',alignment:'center',fontSize: 10,border:[1,0,1,1]},
                                            {text:'80',alignment:'center',fontSize: 10,border:[1,0,1,1]},
                                            {text:'90',alignment:'center',fontSize: 10,border:[1,0,1,1]},
                                            {text:'10',alignment:'center',fontSize: 10,border:[1,0,1,1]},
                                            {text:'11',alignment:'center',fontSize: 10,border:[1,0,1,1]},
                                            {text:'12',alignment:'center',fontSize: 10,border:[1,0,1,1]},
                                            {text:'13',alignment:'center',fontSize: 10,border:[1,0,1,1]},
                                            {text:'14',alignment:'center',fontSize: 10,border:[1,0,1,1]},
                                            {text:'15',alignment:'center',fontSize: 10,border:[1,0,1,1]},
                                            {text:'16',alignment:'center',fontSize: 10,border:[1,0,1,1]},
                                            ],
                                            [
                                            {text:'SL',alignment:'center',fontSize: 10,border:[0,0,1,0]},
                                            {text:'32',alignment:'center',fontSize: 10,border:[0,1,1,0]},
                                            {text:'40',alignment:'center',fontSize: 10,border:[0,1,1,0]},
                                            {text:'50',alignment:'center',fontSize: 10,border:[0,1,1,0]},
                                            {text:'60',alignment:'center',fontSize: 10,border:[0,1,1,0]},
                                            {text:'70',alignment:'center',fontSize: 10,border:[0,1,1,0]},
                                            {text:'80',alignment:'center',fontSize: 10,border:[0,1,1,0]},
                                            {text:'90',alignment:'center',fontSize: 10,border:[0,1,1,0]},
                                            {text:'10',alignment:'center',fontSize: 10,border:[0,1,1,0]},
                                            {text:'11',alignment:'center',fontSize: 10,border:[0,1,1,0]},
                                            {text:'12',alignment:'center',fontSize: 10,border:[0,1,1,0]},
                                            {text:'13',alignment:'center',fontSize: 10,border:[0,1,1,0]},
                                            {text:'14',alignment:'center',fontSize: 10,border:[0,1,1,0]},
                                            {text:'15',alignment:'center',fontSize: 10,border:[0,1,1,0]},
                                            {text:'16',alignment:'center',fontSize: 10,border:[0,1,0,0]},
                                            ]
                                            ]
                                    },layout: {
                                    fillColor: function (rowIndex) { 
                                        if(rowIndex===0){
                                            return ('#FFFFE0');
                                        }
                                        if(rowIndex===1){
                                            return ('#F0F8FF');
                                        }
                                        if(rowIndex===2){
                                            return ('#DCDCDC');
                                        }
                                        },
                                        hLineColor: function (i, node) {
                                            return (i === 0 || i === node.table.body.length) ? 'gray' : 'gray';
                                        },
                                        vLineColor: function (i, node) {
                                            return (i === 0 || i === node.table.widths.length) ? 'gray' : 'gray';
                                        },
                                    }
                                    },
                                    ],
                                    [
                                    {text:'3',alignment:'center',fontSize: 10},
                                    {text:'Sfadsfd fadfadadfsfdas dfsafdasdf',alignment:'left',fontSize: 10},
                                    {
                                    colSpan:15,
                                    margin:[-5,-3,-5,-3],
                                    
                                    table:{
                                        widths:[14,20,15,15,16,16,18,16,14,17,16,14,16,16,20],
                                        body:[
                                            [
                                            {text:'PL',alignment:'center',fontSize: 10,border:[0,0,1,1]},
                                            {text:'323',alignment:'center',fontSize: 10,border:[1,0,1,1]},
                                            {text:'40',alignment:'center',fontSize: 10,border:[1,0,1,1]},
                                            {text:'50',alignment:'center',fontSize: 10,border:[1,0,1,1]},
                                            {text:'60',alignment:'center',fontSize: 10,border:[1,0,1,1]},
                                            {text:'70',alignment:'center',fontSize: 10,border:[1,0,1,1]},
                                            {text:'80',alignment:'center',fontSize: 10,border:[1,0,1,1]},
                                            {text:'90',alignment:'center',fontSize: 10,border:[1,0,1,1]},
                                            {text:'10',alignment:'center',fontSize: 10,border:[1,0,1,1]},
                                            {text:'11',alignment:'center',fontSize: 10,border:[1,0,1,1]},
                                            {text:'12',alignment:'center',fontSize: 10,border:[1,0,1,1]},
                                            {text:'13',alignment:'center',fontSize: 10,border:[1,0,1,1]},
                                            {text:'14',alignment:'center',fontSize: 10,border:[1,0,1,1]},
                                            {text:'15',alignment:'center',fontSize: 10,border:[1,0,1,1]},
                                            {text:'16',alignment:'center',fontSize: 10,border:[1,0,0,1]},
                                            ],
                                            [
                                            {text:'CL',alignment:'center',fontSize: 10,border:[0,0,1,1]},
                                            {text:'32',alignment:'center',fontSize: 10,border:[1,0,1,1]},
                                            {text:'40',alignment:'center',fontSize: 10,border:[1,0,1,1]},
                                            {text:'50',alignment:'center',fontSize: 10,border:[1,0,1,1]},
                                            {text:'60',alignment:'center',fontSize: 10,border:[1,0,1,1]},
                                            {text:'70',alignment:'center',fontSize: 10,border:[1,0,1,1]},
                                            {text:'80',alignment:'center',fontSize: 10,border:[1,0,1,1]},
                                            {text:'90',alignment:'center',fontSize: 10,border:[1,0,1,1]},
                                            {text:'10',alignment:'center',fontSize: 10,border:[1,0,1,1]},
                                            {text:'11',alignment:'center',fontSize: 10,border:[1,0,1,1]},
                                            {text:'12',alignment:'center',fontSize: 10,border:[1,0,1,1]},
                                            {text:'13',alignment:'center',fontSize: 10,border:[1,0,1,1]},
                                            {text:'14',alignment:'center',fontSize: 10,border:[1,0,1,1]},
                                            {text:'15',alignment:'center',fontSize: 10,border:[1,0,1,1]},
                                            {text:'16',alignment:'center',fontSize: 10,border:[1,0,1,1]},
                                            ],
                                            [
                                            {text:'SL',alignment:'center',fontSize: 10,border:[0,0,1,0]},
                                            {text:'32',alignment:'center',fontSize: 10,border:[0,1,1,0]},
                                            {text:'40',alignment:'center',fontSize: 10,border:[0,1,1,0]},
                                            {text:'50',alignment:'center',fontSize: 10,border:[0,1,1,0]},
                                            {text:'60',alignment:'center',fontSize: 10,border:[0,1,1,0]},
                                            {text:'70',alignment:'center',fontSize: 10,border:[0,1,1,0]},
                                            {text:'80',alignment:'center',fontSize: 10,border:[0,1,1,0]},
                                            {text:'90',alignment:'center',fontSize: 10,border:[0,1,1,0]},
                                            {text:'10',alignment:'center',fontSize: 10,border:[0,1,1,0]},
                                            {text:'11',alignment:'center',fontSize: 10,border:[0,1,1,0]},
                                            {text:'12',alignment:'center',fontSize: 10,border:[0,1,1,0]},
                                            {text:'13',alignment:'center',fontSize: 10,border:[0,1,1,0]},
                                            {text:'14',alignment:'center',fontSize: 10,border:[0,1,1,0]},
                                            {text:'15',alignment:'center',fontSize: 10,border:[0,1,1,0]},
                                            {text:'16',alignment:'center',fontSize: 10,border:[0,1,0,0]},
                                            ]
                                            ]
                                    },layout: {
                                    fillColor: function (rowIndex) { 
                                        if(rowIndex===0){
                                            return ('#FFFFE0');
                                        }
                                        if(rowIndex===1){
                                            return ('#F0F8FF');
                                        }
                                        if(rowIndex===2){
                                            return ('#DCDCDC');
                                        }
                                        },
                                        hLineColor: function (i, node) {
                                            return (i === 0 || i === node.table.body.length) ? 'gray' : 'gray';
                                        },
                                        vLineColor: function (i, node) {
                                            return (i === 0 || i === node.table.widths.length) ? 'gray' : 'gray';
                                        },
                                    }
                                    },
                                    ],
                                ]
                        },
                        layout: {
                    fillColor: function (rowIndex) { 
                        if(rowIndex===0||rowIndex===1){
                            return ('#0074c1');
                        }
                        if(rowIndex % 2 === 0){
                            return ('#FAEBD7');
                        }else{
                            return ('#F5F5F5');
                        }
                        
                        if(rowIndex===1) {
                            return ('#FFFFE0');
                        }else if(rowIndex===2){
                            return ('#F8F8FF');
                        }else{
                            return ('#F0F8FF');
                        }
                        },
                        hLineColor: function (i, node) {
                            return (i === 0 || i === node.table.body.length) ? 'gray' : 'gray';
                        },
                        vLineColor: function (i, node) {
                            return (i === 0 || i === node.table.widths.length) ? 'gray' : 'gray';
                        },
                    }
                    },
                    {text:'\n'}, 
                )
            dd = {
                pageSize: 'A4',
                pageMargins: [ 40, 60, 40, 60 ],    
                footer: function (currentPage, pageCount) {
                    return {
                        margin: 10,
                        columns: [{
                            fontSize: 9,
                            text: [{
                                    text: '--------------------------------------------------------------------------' +
                                        '\n',
                                    margin: [0, 20]
                                },
                                {
                                    text: '© Pest Mortem (India) PVT. LTD. | PAGE ' + currentPage.toString() + ' of ' + pageCount,
                                }
                            ],
                            alignment: 'center'
                        }]
                    };

                },
                content,
                styles: {
                        left:{
                            fontSize: 10,
                            alignment:'left',
                        }
                        }
            }
            var win = window.open('', '_blank');
            pdfMake.createPdf(dd).open({}, win);
 
    }
    });
                    
}
</script>