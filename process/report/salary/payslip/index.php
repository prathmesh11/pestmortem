<?php
$base='../../../../';
$js='<script src="'.$base.'js/fwork.js"></script>
<script src="'.$base.'js/alart.js"></script>
<script src="'.$base.'js/pdfmake.min.js"></script>
<script src="'.$base.'js/vfs_fonts.js"></script>
<script src="'.$base.'js/list.min.js"></script>';
$css='<link rel="stylesheet" href="'.$base.'css/fwork.css">
<link rel="stylesheet" href="'.$base.'css/grid.min.css">
<link rel="stylesheet" href="'.$base.'css/alart.css">';
include($base.'_in/header.php');
include($base.'_in/connect.php');
$con=_connect();
if (session_status() == PHP_SESSION_NONE) { session_start(); }
$branchid = $_SESSION['branchid'];


?>
<input type="hidden" id="inp-branch" value='<?php echo $branchid; ?>' />
<input type="hidden" id="item-json1" value='<?php echo str_replace("'"," &#39;",$json1); ?>' />
<img class="hidden" id=imageid src="../../../../img/PestMortemLogo.jpg">
<div class="container-fluid ">
    <div class="content" id="section">

        <div class="row">
            <div class="col-sm-12 text-center">
                <h2>SALARY SUMMARY</h2>
            </div>
        </div>
        <hr style="margin: 2px;">
        <div class="col-sm-12">
            <div class="col-sm-3"></div>
            <div class="col-sm-6">
                <div class="col-sm-5">From Date : </div>
                <div class="col-sm-7">
                    <div class="form-group"><input type="month" data-name='salarydate' class="form-control input-sm"
                            id="salarydate" value="<?php echo date("Y-m"); ?>"></div>
                </div>

                <div class="col-sm-12">
                    <button class="btn btn-success btn-sm btn-block" id="btn-submit" onclick="submit()"
                        style="margin:5px;">Submit</button>
                </div>
            </div>
            
        </div>
    </div>
</div>
<script>
function getBase64Image(img) {
  var canvas = document.createElement("canvas");
  canvas.width = img.width;
  canvas.height = img.height;
  var ctx = canvas.getContext("2d");
  ctx.drawImage(img, 0, 0);
  var dataURL = canvas.toDataURL("image/png");
  return dataURL.replace('/^data:image\/(png|jpg);base64,/', "");
}

function submit() {
    var branchid = $('#inp-branch').val();
    var date = $("#salarydate").val();

    $.ajax({
            type: "POST",
            data: 'date=' + date,
            url: 'select.php',
            cache: false,
            success: function (res){
                var content=[];
                var json=res.json;
                var base64 = getBase64Image(document.getElementById("imageid"));
                var netamt=0;
                var count=0;
                json.forEach(myFunction)
                function myFunction(item, index, arr) {
                var iterator = index;
                var cmpadd=item.branchadd;
                if(cmpadd.length>=80){
                    cmpadd = insert(cmpadd,80, "\n");
                }
                function insert(str, index, value) {
                    return str.substr(0, index) + value + str.substr(index);
                }
                if(count!=0){
                if(count%3=='0'){
                    content.push(
                        { text: '', fontSize: 14, bold: true, pageBreak: 'after', },
                        )
                    }
                }
                count++;
                netamt=(parseFloat(item.totalearning)-parseFloat(item.totaldeduction)).toFixed(2);
                var basicpay;
                var latecutting=item.latecutting;
                var itdeduction=item.deductions.itdeduction;
                var medical=item.income.medical;
                if(latecutting==""){
                    latecutting=0;
                }
                if(itdeduction==""){
                    itdeduction=0;
                }
                if(medical==""){
                    medical=0;
                }
                content.push({
                table:{
                widths:[490],
                body:[
                    [
    	            {   margin:[-5,-3,-5,-3],
    	                table:{
    	                widths:[80,'*'],
    	                body:[
    	                   [ 
    	                   {
                        stack: [{image:base64,
                            width: 60,
                            height:23,
                            margin:[5,5]
                            },],border:[0,0,0,0]
                            },
    	                    {margin:[0,0,55,0],
        	                stack: [{text: [
                            {text:'Pest Mortem (India) Pvt. Ltd.   '+item.branchname+'\n',style: 'centerbold'},
                            {text:cmpadd+' '+'\n',style:'centerbold'},
                            {text:'PAY-SLIP For the Month of   '+item.salarymonth+' - 20'+item.year+'\n',style:'centerbold'}
                            ]},],border:[0,0,0,0],alignment:'center'
    	                    },]
    	                    ]
    	            },
	            },],
    	        [{
    	            margin:[-5,-3,-5,-3],
    	            table:{
    	                widths:['auto','auto',10,'auto','*',"auto",'auto'],
    	                
    	                body:[
        	            [
        	            {stack: [{text: [
                        {text:'Employee Code \n',style: 'left'},
                        {text:'Department\n',style:'left'},
                        {text:'Designation\n',style:'left'},
                        {text:'PF/UAN No.\n',style: 'left'},
                        {text:'ESIC No. \n',style:'left'}
                        ],}],border:[0,0,0,0]
        	            },
    	                {
    	                stack: [{text: [
                        {text:item.employeeid+'\n',style: 'left'},
                        {text:item.department+'\n',style:'left'},
                        {text:item.designation+'\n',style:'left'},
                        {text:'\n',style: 'left'},
                        {text:item.employeresic+'\n',style:'left'}
                        ],}],border:[0,0,0,0]   
    	                },{text:'',border:[0,0,0,0]},
    	                {
    	                stack: [{text: [
                        {text:'Name \n',style: 'left'},
                        {text:'\n',style:'left'},
                        {text:'\n',style:'left'},
                        {text:'PAN No\n',style: 'left'}
                        ],}],border:[0,0,0,0]   
    	                },
    	                {
    	                stack: [{text: [
                        {text:item.employeename+'\n',style: 'left',bold:1},
                        {text:'\n',style:'left'},
                        {text:'\n',style:'left'},
                        {text:item.pan+'\n',style: 'left'}
                        ],}],border:[0,0,0,0]   
    	                },
    	                {
    	                stack: [{text: [
                        {text:'Present Days \n',style: 'left'},
                        {text:'Rem Hrs\n',style:'left'},
                        {text:'Paid Leave.\n',style:'left'},
                        {text:'Un-paid Leave\n',style: 'left'},
                        {text:'Late cutting days\n',style:'left'}
                        ],}],border:[0,0,0,0]   
    	                },
    	                {
    	                stack: [{text: [
                        {text:item.dayshrsinfo.presentdays+'\n',style: 'right',bold:1},
                        {text:item.remhrs+'\n',style:'right'},
                        {text:item.dayshrsinfo.paidleave+'\n',style:'right'},
                        {text:item.otherleave+'\n',style: 'right'},
                        {text:latecutting+'\n',style:'right'}
                        ],}],border:[0,0,0,0]   
    	                },
    	                ]
    	                    ]
    	            },
    	        }],
    	        [{
    	            margin:[2,-3,-5,-3],
    	            table:{
    	                widths:[95,85,70,105,85],
    	                
    	                body:[
        	            [
        	            {stack: [{text: [
                        {text:' E A R N I N G S  \n',style: 'left',italics:1, decoration: 'underline', bold:1},
                        {text:'BASIC + DA                   :\n',style:'left'},
                        {text:'CONVEYANCE              :\n',style:'left'},
                        {text:'D.A. Differance             :\n',style: 'left'},
                        {text:'Other                              :\n',style:'left'},
                        {text:'Medical allows             :\n',style:'left'},
                        {text:'H.R.A.                             :\n',style:'left'},
                        {text:'\n',style:'left',},
                        {text:'GROSS AMOUNT\n',style:'left',bold:1}
                        ],}],border:[0,0,0,0]
        	            },
    	                {
    	                stack: [{text: [
                        {text:'\n',style: 'left'},
                        {text:(parseFloat(item.income.basicpay)).toFixed(2)+'\n',style:'right'},
                        {text:(parseFloat(item.income.conveyance)).toFixed(2)+'\n',style:'right'},
                        {text:(parseFloat(item.income.da)).toFixed(2)+'\n',style: 'right'},
                        {text:(parseFloat(item.income.otamt)).toFixed(2)+'\n',style:'right'},
                        {text:medical+'\n',style:'right'},
                        {text:item.hra+'\n',style:'right'},
                        {text:'\n',style:'right',},
                        {text:(parseFloat(item.totalearning)).toFixed(2)+'\n',style:'right',bold:1}
                        ],}],border:[0,0,0,0]   
    	                },
    	                {text:'',border:[0,0,0,0]},
    	                {stack: [{text: [
                        {text:'  D E D U C T I O N S    \n',style: 'left',italics:1, decoration: 'underline', bold:1},
                        {text:'Prov.Fund                      :\n',style:'left'},
                        {text:'E.S.I.C.                           :\n',style:'left'},
                        {text:'Prof. Tax                        :\n',style: 'left'},
                        {text:'Advance                        :\n',style:'left'},
                        {text:'Loan                               :\n',style:'left'},
                        {text:'I.T.                                   :\n',style:'left'},
                        {text:'MLWF/Pently                :\n',style:'left'},
                        {text:'DEDUCTION AMOUNT\n',style:'left',bold:1}
                        ],}],border:[0,0,0,0]
        	            },
    	                {
    	                stack: [{text: [
                        {text:'\n',style: 'left'},
                        {text:(parseFloat(item.deductions.pfamt)).toFixed(2)+'\n',style:'right'},
                        {text:(parseFloat(item.deductions.esicamt)).toFixed(2)+'\n',style:'right'},
                        {text:(parseFloat(item.deductions.ptamt)).toFixed(2)+'\n',style: 'right'},
                        {text:(parseFloat(item.deductions.advancededuction)).toFixed(2)+'\n',style:'right'},
                        {text:(parseFloat(item.deductions.loandeduc)).toFixed(2)+'\n',style:'right'},
                        {text:itdeduction+'\n',style:'right'},
                        {text:'0\n',style:'right'},
                        {text:(parseFloat(item.totaldeduction)).toFixed(2)+'\n',style:'right',bold:1}
                        ],}],border:[0,0,0,0]   
    	                },
    	                ]
    	                ],
    	            },
    	        }
    	       ],
    	       [
    	           {
    	                stack: [{text: [
                        {text:'Net Payable Salary :     ',style: 'centerbold',color:'white',fontSize: 10,},
                        {text:netamt+'\n',style:'centerbold',color:'white',fontSize: 10,},
                        ],}],border:[1,1,1,1]   
    	                },
    	       ]
	        ]
		},layout: {
		        fillColor: function (rowIndex) { 
                  if(rowIndex===3){
                    return ('#0074c1');
                  }else if(rowIndex===1) {
                      return ('#FFFFE0');
                  }else if(rowIndex===2){
                      return ('#F8F8FF');
                  }else{
                      return ('#F0F8FF');
                  }
                },
				hLineColor: function (i, node) {
					return (i === 0 || i === node.table.body.length) ? 'gray' : 'gray';
				},
				vLineColor: function (i, node) {
					return (i === 0 || i === node.table.widths.length) ? 'gray' : 'gray';
				},
	        }
		
		},
		{text:'\n\n'},
                    )
                }






    dd = {
        pageSize: 'A4',
        pageMargins: [ 60, 45, 30, 45 ],
        footer: function (currentPage, pageCount) {
          return {
            margin: 10,
            columns: [{
              fontSize: 9,
              text: [{
                  text: '--------------------------------------------------------------------------' +
                    '\n',
                  margin: [0, 20]
                },
                {
                  text: '© Pest Mortem (India) PVT. LTD. | PAGE ' + currentPage.toString() + ' of ' + pageCount,
                }
              ],
              alignment: 'center'
            }]
          };

        },
        content,
        styles: {
        left:{
            fontSize: 9,
    		alignment:'left',
        },
        right:{
            fontSize: 9,
    		alignment:'right',
        },
		centerbold: {
			fontSize: 9,
			alignment:'center',
			bold:1
		},
		center: {
			fontSize: 9,
			alignment:'center',
		},
		bn: {
			fontSize: 9,
			border:[0,0,0,0],
		},
    	}
      }
      var win = window.open('', '_blank');
        pdfMake.createPdf(dd).open({}, win);

            }
    });
    
}

</script>
