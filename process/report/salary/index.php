<?php
$base='../../../';
$js='<script src="'.$base.'js/jquery.table2excel.min.js"></script>';  
include($base.'_in/header.php');
include($base.'_in/connect.php');
$con=_connect();
if (session_status() == PHP_SESSION_NONE) { session_start(); }
$branchid = $_SESSION['branchid'];

?>

<style>
hr.style-hr {
    border: 0;
    height: 1px;
    background-image: linear-gradient(to right, rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.75), rgba(0, 0, 0, 0));
}
.table-list td,
   .table-list th {
       border: 1px solid #ddd;
       padding: 1px !important;
       font-size: 13px;
   }
   .table-list td{
	padding-top: 10px !important;

   }

   .table-list tr:nth-child(even) {
       background-color: #f2f2f2;
   }

   .table-list th {
       padding-top: 5px;
       padding-bottom: 5px;
       text-align: center;
       background-color: #16a085;
       color: white;
   }
   .reportnav{
       border-radius:0px;
       text-align: center;
  -webkit-transition-duration: 0.4s; /* Safari */
  transition-duration: 0.4s;
  text-decoration: none;
  overflow: hidden;
  cursor: pointer;
   }
   .table-list td,
   .table-list th {
       border: 1px solid #ddd;
       padding: 1px !important;
       font-size: 13px;
   }
   .table-list td{
	padding-top: 10px !important;

   }

   .table-list tr:nth-child(even) {
       background-color: #f2f2f2;
   }

   .table-list th {
       padding-top: 5px;
       padding-bottom: 5px;
       text-align: center;
       background-color: #16a085;
       color: white;
   }

</style>

<div class="container-fluid">
    <div class=" content">
        <h2 align="center">SALARY REPORTS</h2>
        <div class="col-sm-12">
        <div class="col-sm-2" align="center"><a class="reportnav btn btn-block btn-lg btn-primary" href="/process/report/salary/bankletter/index.php">Bank Letter</a></div>
        <div class="col-sm-2" align="center"><a class="reportnav btn btn-block btn-lg btn-primary" href="/process/report/salary/salarystatement/index.php">Salary Statement</a></div>
        <div class="col-sm-2" align="center"><a class="reportnav btn btn-block btn-lg btn-primary" href="/process/report/salary/salarysummery/index.php">Salary Summary</a></div>
        <div class="col-sm-2" align="center"><a class="reportnav btn btn-block btn-lg btn-primary" href="/process/report/salary/salaryreports/index.php">Salary Reports</a></div>
        <div class="col-sm-2" align="center"><a class="reportnav btn btn-block btn-lg btn-primary" href="/process/report/salary/payslip/index.php">Pay Slip</a></div>
        <div class="col-sm-2" align="center"><a class="reportnav btn btn-block btn-lg btn-primary" href="/process/report/salary/leave/index.php">Leave Register</a></div>
        
        </div>
   </div>

      
   <?php
include($base.'_in/footer.php');
?>