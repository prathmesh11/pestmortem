<?php
    $base='../../../../';
    $js='<script src="'.$base.'js/fwork.js"></script>
    <script src="'.$base.'js/alart.js"></script>
    <script src="'.$base.'js/pdfmake.min.js"></script>
    <script src="'.$base.'js/vfs_fonts.js"></script>
    <script src="'.$base.'js/list.min.js"></script>';
    $css='<link rel="stylesheet" href="'.$base.'css/fwork.css">
    <link rel="stylesheet" href="'.$base.'css/grid.min.css">
    <link rel="stylesheet" href="'.$base.'css/alart.css">';
    include($base.'_in/header.php');
    include($base.'_in/connect.php');
    $con=_connect();
    if (session_status() == PHP_SESSION_NONE) { session_start(); }
    $branchid = $_SESSION['branchid'];
    $json1='';
    $result=mysqli_query($con,"SELECT * FROM branchmaster ORDER BY id ASC");
    while($rows = mysqli_fetch_assoc($result)){
        $bankname=get_object_vars(json_decode($rows['branchjson']))['bankname'];
        $bankaddress=get_object_vars(json_decode($rows['branchjson']))['bankaddress'];
        $accountno=get_object_vars(json_decode($rows['branchjson']))['accountno'];
    
        $json1.=',{"bankname":"'.$bankname.'","bankaddress":"'.$bankaddress.'","accountno":"'.$accountno.'","branchid":"'.$rows['branchid'].'"}';
    }
    $json1=substr($json1,1);
    $json1='['.$json1.']';

?>
<style>
    /* .input-field label {
        padding-left: 0px;
        font-weight:500;
        font-size:22px;
    }
    .input-field{
        margin-bottom: 0px;
        font-size:22px;
    }
    .container-fluid{
        margin-left:0px;
        margin-right:0px;
    } */

 
</style>
<input type="hidden" id="inp-branch" value='<?php echo $branchid; ?>' />
<input type="hidden" id="item-json1" value='<?php echo str_replace("'"," &#39;",$json1); ?>' />
<div class="container-fluid ">
    <div class="content" id="section">

        <div class="row">
            <div class="col-sm-12 text-center">
                <h2>Bank Letter</h2>
            </div>
        </div>
        <hr style="margin: 2px;">
        <div class="col-sm-12">
            <div class="col-sm-3"></div>
            <div class="col-sm-6">
              <div class="col-sm-5">From Date : </div>
                <div class="col-sm-7">
                    <div class="form-group"><input type="month"  data-name='salarydate'
                            class="form-control input-sm" id="salarydate" value="<?php echo date("Y-m"); ?>"></div>
                </div>

            
                <div class="col-sm-5">Select Branch :</div>
                <div class="col-sm-7">
                    <div class="form-group">
                        <select class="form-control input-sm" data-role="select" data-name="branchid" id="branchid" onchange="empdetails(this)">
                            <option value="Select">Select</option>
                            <?php 
                                    $result1=mysqli_query($con, "SELECT * FROM `branchmaster`");
                                    while($rows=mysqli_fetch_assoc($result1)) {
                                    echo '<option value="'.$rows['branchid'].'">'.$rows['branchname'].'</option>';
                                }
                            ?>
                        </select>
                    </div>
                </div>
                
                <div class="col-sm-5">Bank Name :</div>
                <div class="col-sm-7">
                    <div class="form-group"><input type="text" data-role='text' id="bankname"  data-name='bankname'
                            class="form-control input-sm"></div>
                </div>

                <div class="col-sm-5">Bank Address :</div>
                <div class="col-sm-7">
                <div class="form-group"><textarea id="address" data-role='text' id="address"  data-name='address'
                        class="form-control input-sm"> </textarea></div>
                </div>

                <div class="col-sm-5">Account No :</div>
                <div class="col-sm-7">
                <div class="form-group"><input type="text" data-role='text' id="accountno"  data-name='accountno'
                            class="form-control input-sm"></div>
                </div>

                <div class="col-sm-5">Cheque Number :</div>
                <div class="col-sm-7">
                    <div class="form-group"><input type="text" data-role='text'  data-name='chequeno'
                            class="form-control input-sm"></div>
                </div>

                <div class="col-sm-5">Cheque Date :</div>
                <div class="col-sm-7">
                    <div class="form-group"><input type="date"  data-name='chequedate'
                            class="form-control input-sm"></div>
                </div>


                <div class="col-sm-12">
                    <button class="btn btn-success btn-sm btn-block" id="btn-submit" onclick="submit()"
                        style="margin:5px;">Submit</button>
                </div>
            </div>
        </div>
    </div>
</div>



<?php
include($base.'_in/footer.php');
?>
<script>


function empdetails(e){
var employeeid=$(e).val();
var json1=JSON.parse($('#item-json1').val());
    for(var i in json1){

        if(json1[i].branchid==employeeid){
        $('#bankname').val(json1[i].bankname);
        $('#address').val(json1[i].bankaddress);
        $('#accountno').val(json1[i].accountno);
        }
    }
}


function submit() {
    var valid = true;
    if (checker('section') != false) {
        valid = valid * true;
    } else {
        valid = valid * false;
    }
    if (valid) {
        var data = checker('section');
        var datastr = JSON.stringify(data);
        $.ajax({
            type: "POST",
            data: {
                data: datastr
            },
            url: 'insert.php',
            cache: false,
            success: function (res) {
                if (res.status == 'success') {
                    console.log(res)
                    var content = [];                    
                    var amount=0;             
                    var json = res.json;
                    content.push(
                        {text:'To,',style:'left',margin: [0, 0, 0, 7]},
                        {text:'The Chief Manager,\n',style:'left',margin: [0, 0, 0, 7]},
                        {text:json[0].bankname+'',style:'left',margin: [0, 0, 0, 7]},
                        {text:json[0].address +'\n\n',style:'left'},
                        {text:'Sub :- Corporate payroll salary account scheme ',style:'left',margin: [30, 0, 0, 9]},
                        {text:'Ref. No.  Current Account No.'+json[0].accountno+'\n',style:'left',margin: [30, 0, 0, 15]},
                        {
                            table:{
                                widths:[60,'*'],
                                body:[
                                    [{text:'Dear Sir,\n\n',style:'left',border:[0,0,0,0],colSpan:2}],
                                ]
                                    
                            }
                        },
                    )
                    content.push(
                        {	
                        table: {
                            widths: [50, '*', 120,120],
                            dontBreakRows: true,
                            unbreackable:true,
                            body: [
                                [
                                {text:'Sr No',alignment:'center',fontSize: 10,color:'white',bold:1},
                                {text:'Name of The Employe',alignment:'left',fontSize: 10,color:'white',bold:1},
                                {text:'Account No',alignment:'center',fontSize: 10,color:'white',bold:1},
                                {text:'Amount',alignment:'center',fontSize: 10,color:'white',bold:1}
                                ],
                            ]
                        },
                        layout: {
                            fillColor: function (rowIndex) { 
                                if(rowIndex===0){
                                    return ('#0074c1');
                                }
                                if(rowIndex % 2 === 0){
                                    return ('#FFFFE0');
                                }else{
                                    return ('#F0F8FF');
                                }
                                },
                                hLineColor: function (i, node) {
                                    return (i === 0 || i === node.table.body.length) ? 'gray' : 'gray';
                                },
                                vLineColor: function (i, node) {
                                    return (i === 0 || i === node.table.widths.length) ? 'gray' : 'gray';
                                },
                            }
                    },
                    )
                    for(var i in json){
                            content[7].table.body.push(
                                [
                                {text:json[i].srno+'',alignment:'center',fontSize: 10},
                                {text:json[i].employeename+'',alignment:'left',fontSize: 10},
                                {text:json[i].accountnumber+'',alignment:'left',fontSize: 10},
                                {text:'₹'+(parseFloat(json[i].totalearning)).toFixed(2)+'',alignment:'right',fontSize: 10}
                                ],
                            )
                            amount+=parseFloat(json[i].totalearning);
                    }
                    var totalamtinwords = convertNumberToWords(amount)+ " Only";
                    content[7].table.body.push(
                        [
                        {text:'Total :',colSpan:3,alignment:'right',fontSize: 10,color:'',bold:1},{},{}, {text:'₹'+amount.toFixed(2)+'',alignment:'right',fontSize: 10}
                        ]
                    )
                    content[6].table.body.push(
                        [
                        {border:[0,0,0,0],colSpan:2,text:'With reference to above subject, please find enclosed here with Cheque No.' + json[0].chequeno + ' dtd ' + json[0].chequedate + ' for Rs. (Rupees ' + totalamtinwords + ') towards our employees salary for the month of ' + json[0].salarydate + '. Kindly transfer the amount to respective account  as per given below.',margin: [30, 0, 0, 9],fontSize:10,alignment: 'justify'}
                        ]
                    )
                    content.push(
                        {text:'\n'},
                        {
                            columns: [
                                {
                                margin: [10, 5, 0, 5],
                                fontSize: 10,
                                    table: {
                                        widths: [220],
                                        
                                        body:[
                                            [{text:'Kindly do the needful & oblige\n',fontsize:10,border:[0,0,0,0],margin: [30, 0, 0, 0]}],
                                            [{text:'Thanking you,',border:[0,0,0,0],margin: [60, 10, 0, 9]}],
                                            ],
                                            
                                    },layout: { 
                                            fillColor: function (rowIndex) { 
                                                if(rowIndex===1 || rowIndex===0){
                                                return ('#D3D3D3');
                                                }
                                            }
                                            
                                    },
                            },
                            {
                                fontSize: 10,
                                margin: [20, 5, 0, 15],
                                table: {
                                widths: [220],
                                body: [
                                    [
                                    {text:'Yours faithfully', border: [0, 0, 0, 0],alignment:'center'},
                                    
                                    ],
                                    [
                                    {text:'\nFor Pest Mortem (l) Pvt Ltd ', border: [0, 0, 0, 0],alignment:'center'},
                                
                                    ],
                                    [
                                    {text:'\nDirector', border: [0, 0, 0, 0],alignment:'center'}
                                    ]
                                    
                                    
                                ]
                                },
                                layout: { 
                                    fillColor: function (rowIndex) { 
                                        if(rowIndex===3){
                                            return ('#1E90FF');
                                        }else{
                                            if(rowIndex===0 || rowIndex===1 || rowIndex===2){
                                            return ('#D3D3D3');
                                            }
                                        }
                                        }
                                    } 
                            }
                        ]
                        },
                    )
                    dd = {
                        pageSize: 'A4',
                        pageOrientation: 'portrait',
                        pageMargins: [ 60, 150, 30, 20 ],
                        footer: function (currentPage, pageCount) {
                            return {
                                margin: 10,
                                columns: [{
                                    fontSize: 9,
                                    text: [{
                                            text: '--------------------------------------------------------------------------' +
                                                '\n',
                                            margin: [0, 20]
                                        },
                                        {
                                            text: '© Pest Mortem (India) PVT. LTD. | PAGE ' + currentPage.toString() + ' of ' + pageCount,
                                        }
                                    ],
                                    alignment: 'center'
                                }]
                            };

                        },
                        content,
                        styles: {
                                left:{
                                    fontSize: 10,
                                    alignment:'left',
                                }
                             }
                    }
                    var win = window.open('', '_blank');
                    pdfMake.createPdf(dd).open({}, win);
                }
                if (res.status == 'falid2') {

                }
            }
        });
    }
}

function convertNumberToWords(amount) {
    var words = new Array();
    words[0] = '';
    words[1] = 'One';
    words[2] = 'Two';
    words[3] = 'Three';
    words[4] = 'Four';
    words[5] = 'Five';
    words[6] = 'Six';
    words[7] = 'Seven';
    words[8] = 'Eight';
    words[9] = 'Nine';
    words[10] = 'Ten';
    words[11] = 'Eleven';
    words[12] = 'Twelve';
    words[13] = 'Thirteen';
    words[14] = 'Fourteen';
    words[15] = 'Fifteen';
    words[16] = 'Sixteen';
    words[17] = 'Seventeen';
    words[18] = 'Eighteen';
    words[19] = 'Nineteen';
    words[20] = 'Twenty';
    words[30] = 'Thirty';
    words[40] = 'Forty';
    words[50] = 'Fifty';
    words[60] = 'Sixty';
    words[70] = 'Seventy';
    words[80] = 'Eighty';
    words[90] = 'Ninety';
    amount = amount.toString();
    var atemp = amount.split(".");
    var number = atemp[0].split(",").join("");
    var n_length = number.length;
    var words_string = "";
    if (n_length <= 9) {
        var n_array = new Array(0, 0, 0, 0, 0, 0, 0, 0, 0);
        var received_n_array = new Array();
        for (var i = 0; i < n_length; i++) {
            received_n_array[i] = number.substr(i, 1);
        }
        for (var i = 9 - n_length, j = 0; i < 9; i++, j++) {
            n_array[i] = received_n_array[j];
        }
        for (var i = 0, j = 1; i < 9; i++, j++) {
            if (i == 0 || i == 2 || i == 4 || i == 7) {
                if (n_array[i] == 1) {
                    n_array[j] = 10 + parseInt(n_array[j]);
                    n_array[i] = 0;
                }
            }
        }
        value = "";
        for (var i = 0; i < 9; i++) {
            if (i == 0 || i == 2 || i == 4 || i == 7) {
                value = n_array[i] * 10;
            } else {
                value = n_array[i];
            }
            if (value != 0) {
                words_string += words[value] + " ";
            }
            if ((i == 1 && value != 0) || (i == 0 && value != 0 && n_array[i + 1] == 0)) {
                words_string += "Crores ";
            }
            if ((i == 3 && value != 0) || (i == 2 && value != 0 && n_array[i + 1] == 0)) {
                words_string += "Lakhs ";
            }
            if ((i == 5 && value != 0) || (i == 4 && value != 0 && n_array[i + 1] == 0)) {
                words_string += "Thousand ";
            }
            if (i == 6 && value != 0 && (n_array[i + 1] != 0 && n_array[i + 2] != 0)) {
                words_string += "Hundred and ";
            } else if (i == 6 && value != 0) {
                words_string += "Hundred ";
            }
        }
        words_string = words_string.split("  ").join(" ");
    }
    return words_string;
}

</script>