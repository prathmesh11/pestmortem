<?php
$base='../../../../';
$js='<script src="'.$base.'js/fwork.js"></script>
<script src="'.$base.'js/alart.js"></script>
<script src="'.$base.'js/pdfmake.min.js"></script>
<script src="'.$base.'js/vfs_fonts.js"></script>
<script src="'.$base.'js/list.min.js"></script>';
$css='<link rel="stylesheet" href="'.$base.'css/fwork.css">
<link rel="stylesheet" href="'.$base.'css/grid.min.css">
<link rel="stylesheet" href="'.$base.'css/alart.css">';
include($base.'_in/header.php');
include($base.'_in/connect.php');
$con=_connect();
if (session_status() == PHP_SESSION_NONE) { session_start(); }
$branchid = $_SESSION['branchid'];


?>
<input type="hidden" id="inp-branch" value='<?php echo $branchid; ?>' />
<input type="hidden" id="item-json1" value='<?php echo str_replace("'"," &#39;",$json1); ?>' />
<div class="container-fluid ">
    <div class="content" id="section">

        <div class="row">
            <div class="col-sm-12 text-center">
                <h2>SALARY REPORTS</h2>
            </div>
        </div>
        <hr style="margin: 2px;">
        <div class="col-sm-12">
            <div class="col-sm-3"></div>
            <div class="col-sm-6">
                <div class="col-sm-5">From Date : </div>
                <div class="col-sm-7">
                    <div class="form-group"><input type="month" data-name='salarydate' class="form-control input-sm"
                            id="salarydate" value="<?php echo date("Y-m"); ?>"></div>
                </div>


                <div class="col-sm-5">Select Branch :</div>
                <div class="col-sm-7">
                    <div class="form-group">
                        <select class="form-control input-sm" data-role="select" data-name="branchid" id="branchid">
                            <option value="Select">Select</option>
                            <?php 
                                    $result1=mysqli_query($con, "SELECT * FROM `branchmaster`");
                                    while($rows=mysqli_fetch_assoc($result1)) {
                                    echo '<option value="'.$rows['branchid'].'">'.$rows['branchname'].'</option>';
                                }
                            ?>
                        </select>
                    </div>
                </div>

                <div class="col-sm-12">
                    <button class="btn btn-success btn-sm btn-block" id="btn-submit" onclick="submit()"
                        style="margin:5px;">Submit</button>
                </div>
            </div>
        </div>
    </div>
</div>

<center>
    <button id="signin-button" class="btn btn-primary btn-lg" onclick="handleSignInClick()">Sign in</button>
</center>

<?php
include($base.'_in/footer.php');
?>

<script>


function submit() {
    var valid = true;
    if (checker('section') != false) {
        valid = valid * true;
    } else {
        valid = valid * false;
    }
    if (valid) {
        var data = checker('section');
        var datastr = JSON.stringify(data);
        $.ajax({
            type: "POST",
            data: {
                data: datastr
            },
            url: 'select.php',
            cache: false,
            success: function (res) {
                if (res.status == 'success') {
                    var json = res.json;
                    var content=[];  
                    var netamt=0; 
                    var count=0; 
                    content.push(
                        {
                            margin:[0,-8,0,0],
                            table:{
                            dontBreakRows:true,
                            widths:[12,'*',35,35,45,35,35,45,30,32,30,32,30,42,45,40],
                            body:[
                                    [
                                        {text:'1',alignment:'center',fontSize: 9,bold:1},
                                        {
                                            stack:[
                                            {text:''},
                                            {text:'Shubham Deshmukh \n',style:'left',},
                                            {text:'ID',style:'left',},
                                            {text:''}
                                            ]},
                                        {
                                            stack:[
                                            {text:'10.00\n',style:'right'},
                                            {text:'10.30\n',style:'right'},
                                            {text:'33.20\n',style:'right'},
                                            {text:'90.00\n',style:'right'}
                                            ]},
                                        {
                                            stack:[
                                            {text:'10.00\n',style:'right'},
                                            {text:'10.30\n',style:'right'},
                                            {text:'33.20\n',style:'right'},
                                            {text:'90.00\n',style:'right'}
                                            ]},
                                        {
                                            stack:[
                                            {text:'10,00000.00\n',style:'right'},
                                            {text:'10.30\n',style:'right'},
                                            {text:'33.20\n',style:'right'},
                                            {text:'90.00\n',style:'right'}
                                            ]},
                                        {text:'10000',style:'right'},
                                        {text:'10000',style:'right'},
                                        {text:'10000',style:'right'},
                                        {text:'10000',style:'right'},
                                        {text:'10000',style:'right'},
                                        {text:'10000',style:'right'},
                                        {text:'10000',style:'right'},
                                        {text:'10000',style:'right'},
                                        {text:'10000',style:'right'},
                                        {text:'10000',style:'right'},
                                        {text:'10000',style:'right'},
                                    ],
                                ]
                            },layout: {
                                fillColor: function (rowIndex) { 
                                    if(rowIndex % 2 === 0){
                                        return ('#FAEBD7');
                                    }else{
                                        return ('#F5F5F5');
                                    }
                                    },
                                    hLineColor: function (i, node) {
                                        return (i === 0 || i === node.table.body.length) ? 'gray' : 'gray';
                                    },
                                    vLineColor: function (i, node) {
                                        return (i === 0 || i === node.table.widths.length) ? 'gray' : 'gray';
                                    },
                                }
                        }
                    )
                
                    json.forEach(myFunction) 
                    function myFunction(item, index, arr) { 
                        var iterator = index; 
                        count++; 
                        var itdeduct = item.deductions.itdeduction;
                        if(itdeduct==""){
                        itdeduct=0;
                        }
                        var wash = item.income.wash;
                        var medical = item.income.medical;
                        if(wash==""){
                        wash=0;
                        }
                        if(medical==""){
                        medical=0;
                        }
                        var othincome = (parseFloat(wash+medical)).toFixed(2);
                        netamt=(parseFloat(item.totalearning)-parseFloat(item.totaldeduction)).toFixed(2); 
                        content[0].table.body.push(
                        [
                            {text:count+'\n',alignment:'center',fontSize: 9,bold:1},
                            {
                                stack:[
                                {text:''},
                                {text:item.employeename+'\n',style:'left',},
                                {text:'ID:'+item.employeeid,style:'left',},
                                {text:''}
                                ]},
                            {
                                stack:[
                                {text:item.dayshrsinfo.presentdays+'\n',style:'right'},
                                {text:item.dayshrsinfo.paidleave+'\n',style:'right'},
                                {text:item.dayshrsinfo.casualleave+'\n',style:'right'},
                                {text:item.dayshrsinfo.weekoffinmonth+'\n',style:'right'}
                                ]},
                            {
                                stack:[
                                {text:item.empworkingovertimehrs+'\n',style:'right'},
                                {text:'\n',style:'right'},
                                {text:item.dayshrsinfo.overtimehrs+'\n',style:'right'},
                                {text:'\n',style:'right'}
                                ]},
                            {
                                stack:[
                                {text:(parseFloat(item.income.da+item.income.basicpay)).toFixed(2)+'\n',style:'right'},
                                {text:'\n',style:'right'},
                                {text:(parseFloat(item.income.hra)).toFixed(2)+'\n',style:'right'},
                                {text:(parseFloat(item.income.conveyance)).toFixed(2)+'\n',style:'right'}
                                ]},
                            {text:(parseFloat(item.income.otamt)).toFixed(2)+'',style:'right'},
                            {text:othincome+'',style:'right'},
                            {text:(parseFloat(item.totalearning)).toFixed(2)+'',style:'right'},
                            {text:item.deductions.pfamt+'',style:'right'},
                            {text:(parseFloat(item.deductions.ptamt)).toFixed(2)+'',style:'right'},
                            {text:item.deductions.esicamt+'',style:'right'},
                            {text:(parseFloat(item.deductions.loandeduc)).toFixed(2)+'',style:'right'},
                            {text:(parseFloat(item.deductions.advancededuction)).toFixed(2)+'',style:'right'},
                            {text:(parseFloat(item.totaldeduction)).toFixed(2)+'',style:'right'},
                            {text:(parseFloat(item.netpayment)).toFixed(2)+'',style:'right'},
                            {text:'\n',},
                        ],
                        )
                        }
                        content[0].table.body.shift();

                        dd = {
                            pageMargins: [ 40, 150, 40, 20 ],
                            pageOrientation:'landscape',
                            header: function(currentPage, pageCount, pageSize) {
                            return [
                                {margin:[40,40,40,0],
                                text: 'Pest Mortem (India) Pvt. Ltd.\nSalary register for Month of  '+json[0].salarymonth+'-'+json[0].year,fontSize:12, bold:1, alignment:'left' },
                                {margin:[40,0,40,0],
                                table:{
                                    widths:[12,'*',35,35,45,35,35,45,30,32,30,32,30,42,45,40],
                                body:[
                                    [
                                        {
                                            margin:[-5,-3,-5,-3],
                                            table:{
                                            widths:['*'],
                                            body:[
                                                [{text:'Sr.',border:[0,0,0,0],style:'center'}],
                                                [{text:'No.',border:[0,0,0,0],style:'center'}],
                                                [{text:'',border:[0,0,0,0]}],[{text:'',border:[0,0,0,0]}]
                                                ]
                                            }
                                        },
                                        {text:'Full Name',border:[0,1,0,1],style:'center'},
                                        {
                                            margin:[-5,-3,-5,-3],
                                            table:{
                                            widths:['*'],
                                            body:[
                                                [{text:'Present',border:[0,0,0,1],style:'center'}],
                                                [{text:'P.L.',border:[0,0,0,1],style:'center'}],
                                                [{text:'P.H.',border:[0,0,0,1],style:'center'}],
                                                [{text:'W.off',border:[0,0,0,0],style:'center'}]
                                                ]
                                            },layout: {
                                                    hLineColor: function (i, node) {
                                                        return (i === 0 || i === node.table.body.length) ? 'white' : 'white';
                                                    },
                                                }
                                        },
                                        {
                                            margin:[-5,-3,-5,-3],
                                            table:{
                                            widths:['*'],
                                            body:[
                                                [{text:'Present',border:[0,0,0,0],style:'center'}],
                                                [{text:'Hrs',border:[0,0,0,1],style:'center'}],
                                                [{text:'O.T.',border:[0,0,0,0],style:'center'}],
                                                [{text:'Hrs',border:[0,0,0,0],style:'center'}]
                                                ]
                                            },layout: {
                                                    hLineColor: function (i, node) {
                                                        return (i === 0 || i === node.table.body.length) ? 'white' : 'white';
                                                    },
                                                    
                                                }
                                        },
                                        {
                                            margin:[-5,-3,-5,-3],
                                            table:{
                                            widths:['*'],
                                            body:[
                                                [{text:'Basic',border:[0,0,0,0],style:'center'}],
                                                [{text:'+D.A.',border:[0,0,0,1],style:'center'}],
                                                [{text:'H.R.A.',border:[0,0,0,0],style:'center'}],
                                                [{text:'CONV.\nRs.',border:[0,0,0,0],style:'center'}]
                                                ]
                                            },layout: {
                                                    hLineColor: function (i, node) {
                                                        return (i === 0 || i === node.table.body.length) ? 'white' : 'white';
                                                    },
                                                }
                                        },
                                        {
                                            margin:[-5,-3,-5,-3],
                                            table:{
                                            widths:['*'],
                                            body:[
                                                [{text:'O.T.',border:[0,0,0,0],style:'center'}],
                                                [{text:'\n',border:[0,0,0,0],style:'center'}],
                                                [{text:'\n',border:[0,0,0,0],style:'center'}],
                                                [{text:'\nRs.',border:[0,0,0,0],style:'center'}]
                                                ]
                                            }
                                        },
                                        {
                                            margin:[-5,-3,-5,-3],
                                            table:{
                                            widths:['*'],
                                            body:[
                                                [{text:'OTH',border:[0,0,0,0],style:'center'}],
                                                [{text:'Income',border:[0,0,0,0],style:'center'}],
                                                [{text:'\n',border:[0,0,0,0],style:'center'}],
                                                [{text:'\nRs.',border:[0,0,0,0],style:'center'}]
                                                ]
                                            }
                                        },
                                        {
                                            margin:[-5,-3,-5,-3],
                                            table:{
                                            widths:['*'],
                                            body:[
                                                [{text:'TOTAL',border:[0,0,0,0],style:'center'}],
                                                [{text:'\n',border:[0,0,0,0],style:'center'}],
                                                [{text:'\n',border:[0,0,0,0],style:'center'}],
                                                [{text:'\nRs.',border:[0,0,0,0],style:'center' }]
                                                ]
                                            }
                                        },
                                        {
                                            margin:[-5,-3,-5,-3],colSpan:5,style:'center',
                                            table:{
                                            widths:[30,32,30,32,30],
                                            body:[
                                                [
                                                {colSpan:5,style:'center',border:[0,0,0,0],text:'Deduction'},{},{},{},{}],
                                                [{text:'P.F.',border:[0,1,0,0],style:'center'},
                                                {text:'P.T.',border:[1,1,0,0],style:'center'},
                                                {text:'ESIC',border:[1,1,0,0],style:'center'},
                                                {text:'Loan',border:[1,1,0,0],style:'center'},
                                                {text:'ADV.',border:[1,1,0,0],style:'center'},
                                                
                                                ],
                                                [{text:'\n\n',border:[0,0,0,0],style:'center'},{text:'\n\n',border:[1,0,0,0],style:'center'},
                                                {text:'\n\n',border:[1,0,0,0],style:'center'},{text:'\n\n',border:[1,0,0,0],style:'center'},
                                                {text:'\n\n',margin:[-3,-1,-5,-3],border:[1,0,0,0],style:'center'}],
                                                
                                                [{text:'Rs.',border:[0,0,0,0],style:'center'},{text:'Rs.',border:[1,0,0,0],style:'center'},
                                                {text:'Rs.',border:[1,0,0,0],style:'center'},{text:'Rs.',border:[1,0,0,0],style:'center'},
                                                {text:'Rs.',border:[1,0,0,0],style:'center'}],
                                                
                                                ]
                                            },layout: {
                                                    hLineColor: function (i, node) {
                                                        return (i === 0 || i === node.table.body.length) ? 'white' : 'white';
                                                    },
                                                    vLineColor: function (i, node) {
                                                        return (i === 0 || i === node.table.widths.length) ? 'white' : 'white';
                                                    },
                                                }
                                            
                                        },
                                        {text:'',},
                                        {text:'',},
                                        {text:'',},
                                        {text:'',},
                                        {
                                            margin:[-5,-3,-5,-3],
                                            table:{
                                            widths:['*'],
                                            body:[
                                                [{text:'TOTAL',border:[0,0,0,0],style:'center'}],
                                                [{text:'Deduction',border:[0,0,0,0],style:'center'}],
                                                [{text:'\n',border:[0,0,0,0],style:'center'}],
                                                [{text:'\nRs.',border:[0,0,0,0],style:'center'}]
                                                ]
                                            }
                                        },
                                        {
                                            margin:[-5,-3,-5,-3],
                                            table:{
                                            widths:['*'],
                                            body:[
                                                [{text:'Net',border:[0,0,0,0],style:'center'}],
                                                [{text:'Payable',border:[0,0,0,0],style:'center'}],
                                                [{text:'\n',border:[0,0,0,0],style:'center'}],
                                                [{text:'\nRs.',border:[0,0,0,0],style:'center'}]
                                                ]
                                            }
                                        },
                                        {text:'Sign\n',style:'center'},
                                    ],
                                    
                                ]
                                },
                                layout: {
                                fillColor: function (rowIndex) { 
                                    if(rowIndex===0){
                                        return ('#0074c1');
                                    }
                                    },
                                    hLineColor: function (i, node) {
                                        return (i === 0 || i === node.table.body.length) ? 'white' : 'white';
                                    },
                                    vLineColor: function (i, node) {
                                        return (i === 0 || i === node.table.widths.length) ? 'white' : 'white';
                                    },
                                }
                                }
                                ]
                            }, 
                            footer: function (currentPage, pageCount) { 
                                return { 
                                margin: 10, 
                                columns: [{ 
                                    fontSize: 9, 
                                    text: [{ 
                                        text: '--------------------------------------------------------------------------' + 
                                        '\n', 
                                        margin: [0, 20] 
                                    }, 
                                    { 
                                        text: '© Pest Mortem (India) PVT. LTD. | PAGE ' + currentPage.toString() + ' of ' + pageCount, 
                                    } 
                                    ], 
                                    alignment: 'center' 
                                }] 
                                }; 

                            }, 
                            content, 
                            styles: {
                                center:{
                                    fontSize: 9,
                                    alignment:'center',
                                    bold:1,
                                    color:'white',
                                },
                                right:{
                                    fontSize: 9, 
                                    alignment:'right',
                                    bold:1,
                                    
                                },
                                left:{
                                    fontSize: 9,
                                    alignment:'left',
                                    bold:1,
                                    
                                },
                            }
                        } 
                        var win = window.open('', '_blank'); 
                        pdfMake.createPdf(dd).open({}, win); 

                    }
                if (res.status == 'falid2') {
                }
            }
        });
    }
}

</script>
    
    

