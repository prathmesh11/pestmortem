<?php 

$base='../../../../';
include($base.'_in/connect.php');
header('content-type: application/json; charset=utf-8');
header("access-control-allow-origin: *");
if(isset($_POST['data'])) {
    $con=_connect();
    if (session_status()==PHP_SESSION_NONE) { session_start(); }
     $data=get_object_vars(json_decode($_POST["data"]));

        $salarydate = $data['salarydate'];
        $branchid = $data['branchid'];
       
        
        $json1='';
        $branchjson=mysqli_fetch_assoc(mysqli_query($con,"SELECT branchjson x FROM branchmaster WHERE branchid='$branchid'"))['x'];
        
        $address=get_object_vars(json_decode($branchjson))['address'];
        

     $result1=mysqli_query($con,"SELECT salaryprocess.*, salarymaster.personaljson FROM salaryprocess Left Join salarymaster on salaryprocess.employeeid=salarymaster.employeeid  WHERE salaryprocess.branchid='$branchid' and salarydate='$salarydate'");

     while($row = mysqli_fetch_assoc($result1)){
        $count++;     
        $employeename=$row['employeename'];
        $employeeid=$row['employeeid'];
        $branchid=$row['branchid'];
        $presentdays=get_object_vars(json_decode($row['dayshrsinfo']))['presentdays'];
        $basicpay=get_object_vars(json_decode($row['income']))['basicpay'];
        $basicpay=round($basicpay, 2);
        
        
        $da=get_object_vars(json_decode($row['income']))['da'];
        $da=round($da, 2);
        

        $hra=get_object_vars(json_decode($row['income']))['hra'];
        $hra=round($hra, 2);
      
        $conveyance=get_object_vars(json_decode($row['income']))['conveyance'];
        $conveyance=round($conveyance, 2);
        
        $medical=get_object_vars(json_decode($row['income']))['medical'];
        $medical=round($medical, 2);
       
        $wash=get_object_vars(json_decode($row['income']))['wash'];
        $wash=round($wash, 2);
       
        $otamt=get_object_vars(json_decode($row['income']))['otamt'];
        $otamt=round($otamt, 2);
        
        
       $earnedallowance= $da+$conveyance+$medical+$wash+$otamt;
      

       $totalearning=$row['totalearning'];
       $totalearning=round($totalearning,2);
       

       $ptamt=get_object_vars(json_decode($row['deductions']))['ptamt'];
        $ptamt=round($ptamt, 2);

       $pfamt=get_object_vars(json_decode($row['deductions']))['pfamt'];
       $pfamt=round($pfamt, 2);
       
       $esicamt=get_object_vars(json_decode($row['deductions']))['esicamt'];
       $esicamt=round($esicamt, 2);

       $loandeduc=get_object_vars(json_decode($row['deductions']))['loandeduc'];
       $loandeduc=round($loandeduc, 2);

       $advancededuction=get_object_vars(json_decode($row['deductions']))['advancededuction'];
       $advancededuction=round($advancededuction, 2);

       $itdeduction=get_object_vars(json_decode($row['deductions']))['itdeduction'];
       $itdeduction=round($itdeduction, 2);

       
       $rateinmaster=$row['rateinmaster'];
       $rateinmaster=round($rateinmaster,2);

       $totaldeduction=$row['totaldeduction'];
       $totaldeduction=round($totaldeduction,2);

       $netpayment=$row['netpayment'];
       $netpayment=round($netpayment,2);

        

                   $json1.=',{
                    "srno":"'.$count.'"
                    ,"employeename":"'.$employeename.'"
                    ,"presentdays":"'.$presentdays.'"
                    ,"rateinmaster":"'.$rateinmaster.'"
                    ,"basicpay":"'.$basicpay.'"
                    ,"hra":"'.$hra.'"
                    ,"earnedallowance":"'.$earnedallowance.'"
                    ,"totalearning":"'.$totalearning.'"
                    ,"loandeduc":"'.$loandeduc.'"
                    ,"advancededuction":"'.$advancededuction.'"
                    ,"pfamt":"'.$pfamt.'"
                    ,"ptamt":"'.$ptamt.'"
                    ,"itdeduction":"'.$itdeduction.'"
                    ,"esicamt":"'.$esicamt.'"
                    ,"totaldeduction":"'.$totaldeduction.'"
                    ,"netpayment":"'.$netpayment.'"}';
                     

     }

     $json1=substr($json1,1);
     $json1='['.$json1.']';
     echo '{"status":"success","json":'.$json1.'}';
      _close($con);
    }
                   ?>

