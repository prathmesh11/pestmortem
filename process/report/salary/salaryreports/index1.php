<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script>

function makeApiCall0() {
      var params = {
        spreadsheetId: '1hdytz316smXVY_cfUwxpfPJw0ZknVpmZjluAI4Esevg',
      };

      var batchClearValuesRequestBody = {
        ranges: ['Sheet1!A10:AU600'],
      };

      var request = gapi.client.sheets.spreadsheets.values.batchClear(params, batchClearValuesRequestBody);
      request.then(function (response) {
     //   makeApiCall1();
        console.log(response.result);
      }, function (reason) {
        console.error('error: ' + reason.result.error.message);
      });
    }


function initClient() {
      var API_KEY = 'AIzaSyDteBm0ezHnONXI2TcCnOG1eZdoNwZea2w';
      var CLIENT_ID = '398149863948-k504fvm8mnded12dnt6utelssbmkrl91.apps.googleusercontent.com';
      var SCOPE = 'https://www.googleapis.com/auth/spreadsheets';

      gapi.client.init({
        'apiKey': API_KEY,
        'clientId': CLIENT_ID,
        'scope': SCOPE,
        'discoveryDocs': ['https://sheets.googleapis.com/$discovery/rest?version=v4'],
      }).then(function() {
        gapi.auth2.getAuthInstance().isSignedIn.listen(updateSignInStatus);
        updateSignInStatus(gapi.auth2.getAuthInstance().isSignedIn.get());
      });
    }

    function handleClientLoad() {
      gapi.load('client:auth2', initClient);
    }

    function updateSignInStatus(isSignedIn) {
      if (isSignedIn) {
        makeApiCall0();      
        $('#signin-button').hide();
        $('#iframsheet').show();
      }
    }

    function handleSignInClick(event) {
      gapi.auth2.getAuthInstance().signIn();
    }

    function handleSignOutClick(event) {
      gapi.auth2.getAuthInstance().signOut();
    }
    //----------------------------------------------
    </script>
    <script async defer src="https://apis.google.com/js/api.js"
      onload="this.onload=function(){};handleClientLoad()"
      onreadystatechange="if (this.readyState === 'complete') this.onload()">
    </script>
    <center>
    
    <button id="signin-button" class="btn btn-primary btn-lg" onclick="handleSignInClick()">Sign in</button>
    </center>
    <!-- <div class="iframe-container">
<iframe id="iframsheet" style="display:none;" src="https://docs.google.com/spreadsheets/d/1hdytz316smXVY_cfUwxpfPJw0ZknVpmZjluAI4Esevg/edit#gid=0" >
</iframe> -->