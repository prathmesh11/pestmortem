<?php 

    $base    = '../../../';

    $navenq1 = 'background:#1B1464;';

    include('../header.php');


    if (!in_array('Purchase Order', $arrysession)) {
   
        echo "<script>window.location.href='/process/dashboard.php';</script>";
       
        exit;

    }
?>

<br>

<style>

    body {
        background: #eee;
    }

    .table-list td,
    .table-list th {
        border: 1px solid #ddd;
        padding: 1px !important;
        font-size: 13px;
    }

    .table-list td {
        padding-top: 2px !important;
        margin: 0;

    }

    .table-list tr:nth-child(even) {
        background-color: #f2f2f2;
    }

    .table-list th {
        padding-top: 5px;
        padding-bottom: 5px;
        text-align: center;
        background-color: #16a085;
        color: white;
    }

</style>

    <input type="hidden" value="<?php echo $_POST['branchid']; ?>" id="inp-post-branchid">

    <input type="hidden" value="<?php echo $_POST['pono']; ?>" id="inp-post-pono">

    <input type="hidden" value="<?php echo $_POST['supplierid']; ?>" id="inp-post-supplierid">

    <input type="hidden" value='<?php echo $_POST['itemjson']; ?>' id="inp-post-itemjson">

    <input type="hidden" id="purchaseid" data-name="purchaseid" value='<?php echo $_GET['purchaseid']; ?>' />

    <input type="hidden" id="copypurchaseid" data-name="purchaseid" value='<?php echo $_GET['copypurchaseid']; ?>' />

    <input type="hidden" id="editgateentry" data-name="purchaseid" value='<?php echo $_GET['editgateentry']; ?>' />

    <input type="hidden" id="editbranchid" data-name="editbranchid" value='' />

    <input type="hidden" id="editsupplierid" data-name="editsupplierid" value='' />

    <input type="hidden" id="editsbranch" data-name="editsbranch" value='' />

    <input type="hidden" id="editspodate" data-name="editspodate" value='' />

    <input type="hidden" id="item-json1" />

<div class="container-fluid">

    <div class="row" id="div-select"
        style="border-radius:4px;border:1px solid #CCC;background:#FFF;padding-top:10px;-webkit-box-shadow: 2px 2px 3px 3px #ccc; -moz-box-shadow:2px 2px 3px 3px #ccc;box-shadow:2px 2px 3px 3px #ccc;">

        <div class="col-sm-3">

            <div class="col-sm-5">Order for Branch</div>

            <div class="col-sm-7">

                <div class="form-group">

                    <select class="form-control input-sm select-js1" onchange="getitem(this)" data-role="select"
                        data-name="branchid" id="branchid">

                        <option value="Select">Select</option>

                        <?php
                            $result1=mysqli_query($con, "SELECT branchname,branchid FROM `branchmaster`");

                                while($rows=mysqli_fetch_assoc($result1)) {

                                    echo '<option value="'.$rows['branchid'].'">'.$rows['branchname'].'</option>';

                                }

                        ?>

                    </select>

                </div>

            </div>

        </div>

        <div class="col-sm-3">

            <div class="col-sm-4">DATE</div>

            <div class="col-sm-8">

                <div class="form-group">
                        
                    <input type="date" id="podate" value="<?php echo $today;?>" class="form-control input-sm">

                </div>

            </div>

        </div>

        <div class="col-sm-3">

            <div class="col-sm-4">SUPPLIER</div>

            <div class="col-sm-8">

                <div class="form-group">

                    <select class="form-control input-sm select-js1" onchange="getitem(this)" data-role="select"
                        data-name="supplierid" id="supplierid">

                        <option value="Select">Select</option>

                        <?php
                            
                            $result1=mysqli_query($con, "SELECT supplierid,suppliername FROM `suppliermaster` ORDER BY suppliername ASC");

                                while($rows=mysqli_fetch_assoc($result1)) {

                                    echo '<option value="'.$rows['supplierid'].'">'.$rows['suppliername'].'</option>';

                                }

                        ?>

                    </select>

                </div>

            </div>

        </div>

        <!-- <div class="col-sm-3">
            <div class="col-sm-4">S BRANCH</div>
            <div class="col-sm-8">
                <div class="form-group">
                    <select class="form-control input-sm select-js1" onchange="setsbranch(this)" id="sbranch"
                        data-role="select" data-name="sbranch">
                        <option value="Select">Select</option>
                    </select>
                </div>
            </div>
        </div> -->

        <div class="col-sm-2">

            <button class="btn btn-sm btn-success btn-block" id="btn-insert" onclick="insert()">Submit</button>

            <button class="btn btn-sm btn-success btn-block hidden" id="btn-update" onclick="update()">Update</button>

        </div>

    </div>

    <br>

    <div class="row"
        style="border-radius:4px;border:1px solid #CCC;background:#FFF;padding-top:10px;-webkit-box-shadow: 2px 2px 3px 3px #ccc; -moz-box-shadow:2px 2px 3px 3px #ccc;box-shadow:2px 2px 3px 3px #ccc;">
        
        <table id="table-order" class="table table-list form-group">

            <thead>

                <tr>

                    <th rowspan="2" width="300">Item Name</th>

                    <th rowspan="2">Qty</th>

                    <th rowspan="2" width="80">Unit</th>

                    <th rowspan="2" style="display:none;">Rate</th>

                    <th colspan="2" style="display:none;">DISC.</th>

                    <th rowspan="2" style="display:none;">Amount</th>

                    <th colspan="2" style="display:none;">CGST</th>

                    <th colspan="2" style="display:none;">SGST</th>

                    <th colspan="2" style="display:none;">IGST</th>

                    <th rowspan="2" style="display:none;">Net Amount</th>

                    <th rowspan="2" width="80">Action</th>

                </tr>

                <tr>

                    <th style="display:none;">RATE %</th>

                    <th style="display:none;">AMOUNT</th>

                    <th style="display:none;">RATE %</th>

                    <th style="display:none;">AMOUNT</th>

                    <th style="display:none;">RATE %</th>

                    <th style="display:none;">AMOUNT</th>

                    <th style="display:none;">RATE %</th>

                    <th style="display:none;">AMOUNT</th>

                </tr>

            </thead>

            <tbody>

                <tr>

                    <td>

                        <select class="form-control input-sm select-js1" data-name="itemcode" data-role="select"
                            onchange="ratemaster(this)" id="itemcode" data-name="itemcode">

                            <option value="Select">Select</option>

                        </select>

                    </td>

                    <td>

                        <input type="text" class="form-control input-sm" data-name="qty" data-role="number" id="qty"
                            onkeyup="calc()" autocomplete='sameer'>

                    </td>

                    <td>
                        
                        <input type="text" class="form-control input-sm" data-name="unit" data-role="text" id="unit" readonly>
                    
                    </td>

                    <td style="display:none;">
                        
                        <input type="text" class="form-control input-sm" data-name="rate" data-role="number" onkeyup="calc()" id="rate" >
                    
                    </td>

                    <td style="display:none;">
                        
                        <input type="text" class="form-control input-sm" data-name="descper" data-role="number"
                            id="descper" onkeyup="desc1()" value="0" autocomplete='sameer' >
                        
                    </td>

                    <td style="display:none;">
                        
                        <input type="text" class="form-control input-sm" data-name="desamt" data-role="number"
                            id="desamt" onkeyup="desc2()" value="0" autocomplete='sameer' >
                        
                    </td>

                    <td style="display:none;">
                        
                        <input type="text" class="form-control input-sm" data-name="amount" data-role="number" id="amount" readonly > 
                        
                    </td>

                    <td style="display:none;">
                        
                        <input type="text" class="form-control input-sm" data-name="cgstper" data-role="number"
                            id="cgstper" onkeyup="calc()" >
                    
                    </td>

                    <td style="display:none;">
                        
                        <input type="text" class="form-control input-sm" data-name="cgstamt" data-role="number"
                            id="cgstamt" >
                        
                    </td>

                    <td style="display:none;">
                        
                        <input type="text" class="form-control input-sm" data-name="sgstper" data-role="number"
                            id="sgstper" onkeyup="calc()" >
                        
                    </td>


                    <td style="display:none;">
                        
                        <input type="text" class="form-control input-sm" data-name="sgstamt" data-role="number"
                            id="sgstamt" >
                        
                    </td>

                    <td style="display:none;">
                        
                        <input type="text" class="form-control input-sm" data-name="igstper" data-role="number"
                            id="igstper" onkeyup="calc()" >
                        
                    </td>

                    <td style="display:none;">
                        
                        <input type="text" class="form-control input-sm" data-name="igstamt" data-role="number" id="igstamt">
                        
                    </td>

                    <td style="display:none;">
                        
                        <input type="text" class="form-control input-sm" data-name="netamt" data-role="number"
                            id="netamt" readonly>
                        
                    </td>

                    <td align="center">
                        
                        <button class="btn btn-sm btn-success" id="btn-add"
                            onclick="adder()">Add</button>
                        
                    </td>

                </tr>

            </tbody>

        </table>
        
    </div>

    <br>

    <div class="row"
        style= "display:none;border-radius:4px;border:1px solid #CCC;background:#FFF;padding-top:10px;-webkit-box-shadow: 2px 2px 3px 3px #ccc; -moz-box-shadow:2px 2px 3px 3px #ccc;box-shadow:2px 2px 3px 3px #ccc;">

        <div class="col-sm-3">
            <div class="col-sm-4" align="right">DESC AMT</div>
            <div class="col-sm-8">
                <div class="form-group"><input type="text" class="form-control input-sm" data-name="totaldescamt"
                        id="totaldescamt" data-role="number" readonly></div>
            </div>
        </div>
        <div class="col-sm-3">
            <div class="col-sm-4" align="right">AMT</div>
            <div class="col-sm-8">
                <div class="form-group"><input type="text" class="form-control input-sm" data-name="totalnetamt"
                        id="totalnetamt" data-role="number" readonly></div>
            </div>
        </div>
        <div class="col-sm-3">
            <div class="col-sm-4" align="right">CGST AMT</div>
            <div class="col-sm-8">
                <div class="form-group"><input type="text" class="form-control input-sm" data-name="totalcgstamt"
                        id="totalcgstamt" data-role="number" readonly></div>
            </div>
        </div>
        <div class="col-sm-3">
            <div class="col-sm-4" align="right">SGST AMT</div>
            <div class="col-sm-8">
                <div class="form-group"><input type="text" class="form-control input-sm" data-name="totalsgstamt"
                        id="totalsgstamt" data-role="number" readonly></div>
            </div>
        </div>
        <div class="col-sm-3">
            <div class="col-sm-4" align="right">IGST AMT</div>
            <div class="col-sm-8">
                <div class="form-group"><input type="text" class="form-control input-sm" data-name="totaligstamt"
                        id="totaligstamt" data-role="number" readonly></div>
            </div>
        </div>
        <div class="col-sm-3" style="display:none;">
            <div class="col-sm-4" align="right">OTHER</div>
            <div class="col-sm-8">
                <div class="form-group"><input onkeyup=" totalcalc()" type="text" class="form-control input-sm"
                        data-name="totalother" id="totalother" data-role="number"></div>
            </div>
        </div>
        <div class="col-sm-3">
            <div class="col-sm-4" align="right">TOTAL</div>
            <div class="col-sm-8">
                <div class="form-group"><input type="text" class="form-control input-sm" data-name="totaltotal"
                        id="totaltotal" data-role="number" readonly></div>
            </div>
        </div>

    </div>
</div>
<?php 
include('../footer.php');
?>
<script>


var json2=[];
var purchaseid=$('#purchaseid').val();
    if(purchaseid!=""){
        edit();
        
    }
var copypurchaseid=$('#copypurchaseid').val();
if(copypurchaseid!=""){
        copy();
        
    }
var editgateentry=$('#editgateentry').val();
if(editgateentry!=""){
    $("#btn-insert").addClass('hidden');
    $("#btn-update").removeClass('hidden');
    var purchaseid=editgateentry;
        edit();
        
    }

function update(){
    var data=modals.tabletdbyclass('table-order');
    data.shift();
    $.ajax({
        type: "POST",
        data:{
            pobranchid:$('#branchid').val(),
            podate:$('#podate').val(),
            supplierid:$('#supplierid').val(),
            editgateentry:$('#editgateentry').val(),
            data:JSON.stringify(data)
        },
        url: 'update.php',
        cache: false,
        success: function (res) {
            if(res.status=='success'){
                window.location= "../gate-entry.php";
            }
        }
	});
}
function adder(){
    var valid = true;
    if (checker('table-order') != false) {
        valid = valid * true;
    } else {
        valid = valid * false;
    }
    if (valid) {
        data = checker('table-order');
        var str='<tr>';
        str+='<td align="center" class="itemname">'+ $('#itemcode').find(":selected").text() +'</td>'; 
        str+='<td align="center" class="itemcode" style="display:none;">'+data.itemcode+'</td>'; 
        str+='<td align="center" class="qty">'+data.qty+'</td>';
        str+='<td align="center" class="unit">'+data.unit+'</td>';
        str+='<td align="center" class="rate" style="display:none;">'+data.rate+'</td>';
        str+='<td align="center" class="descper" style="display:none;">'+data.descper+'</td>';
        str+='<td align="center" class="desamt" style="display:none;">'+data.desamt+'</td>';
        str+='<td align="center" class="amount" style="display:none;">'+data.amount+'</td>';
        str+='<td align="center" class="cgstper" style="display:none;">'+data.cgstper+'</td>';
        str+='<td align="center" class="cgstamt" style="display:none;">'+data.cgstamt+'</td>';
        str+='<td align="center" class="sgstper" style="display:none;">'+data.sgstper+'</td>';
        str+='<td align="center" class="sgstamt" style="display:none;">'+data.sgstamt+'</td>';
        str+='<td align="center" class="igstper" style="display:none;">'+data.igstper+'</td>';
        str+='<td align="center" class="igstamt" style="display:none;">'+data.igstamt+'</td>';
        str+='<td align="center" class="netamt" style="display:none;">'+data.netamt+'</td>';
        str+='<td align="center"><button class="btn btn-sm btn-success" onclick="editor('+data.itemcode+',this)">E</button><button class="btn btn-sm btn-danger remover" onclick="remover(this)">R</buuton></td>';
        str+='</tr>';
        $('#table-order > tbody').append(str);
        if(editremover!=""){
            editremover.click();
            $('#itemcode').prop('disabled', false);
            totalcalc();
        }
        modals.clear('table-order');
        $('#descper').val(0);
        $('#desamt').val(0);
    }
    $(".remover").click(function(){
        console.log("hello")
        totalcalc();
        calc();
    })
    totalcalc();
}

$(document).ready(function() {
    $('.select-js1').select2({width: '100%'});
    $('.select').attr('style','width:100%!important;');
    
});
function ratemaster(e){
    var json5=$("#item-json1").val();
    if(json5!=""){
    var itemcode=$(e).val();
    var json1 = JSON.parse($("#item-json1").val());
    for(var i in json1){
        if(itemcode==json1[i].itemcode){
            $('#unit').val(json1[i].unit);
            $('#rate').val(json1[i].rate);
            $('#amount').val(0);
            $('#cgstper').val(0);
            $('#sgstper').val(0);
            $('#igstper').val(0);
        }
        }
    }
    }
function calc(){
    var rate=parseFloat($('#rate').val());
    if(rate==0){
        var qty=parseFloat($('#qty').val());
        var desamt=parseFloat($('#desamt').val());
        var cgstper=parseFloat($('#cgstper').val());
        var sgstper=parseFloat($('#sgstper').val());
        var igstper=parseFloat($('#igstper').val());

        var amount=rate*qty;
        var afterdiscamt=amount-desamt;
        var cgstamt=cgstper*afterdiscamt/100;
        var sgstamt=sgstper*afterdiscamt/100;
        var igstamt=igstper*afterdiscamt/100;
        var netamt=afterdiscamt+cgstamt+sgstamt+igstamt;

        $('#amount').val(0);
        $('#cgstamt').val(0);
        $('#sgstamt').val(0);
        $('#igstamt').val(0);
        $('#netamt').val(0)
    }else{
    var qty=parseFloat($('#qty').val());
    var desamt=parseFloat($('#desamt').val());
    var cgstper=parseFloat($('#cgstper').val());
    var sgstper=parseFloat($('#sgstper').val());
    var igstper=parseFloat($('#igstper').val());

    var amount=rate*qty;
    var afterdiscamt=amount-desamt;
    var cgstamt=cgstper*afterdiscamt/100;
    var sgstamt=sgstper*afterdiscamt/100;
    var igstamt=igstper*afterdiscamt/100;
    var netamt=afterdiscamt+cgstamt+sgstamt+igstamt;

    $('#amount').val(afterdiscamt.round(2));
    $('#cgstamt').val(cgstamt.round(2));
    $('#sgstamt').val(sgstamt.round(2));
    $('#igstamt').val(igstamt.round(2));
    $('#netamt').val(netamt.round(2));
    }
 }
function desc1(){
    var descper=$('#descper').val();
    var desamt=$('#desamt').val();
    var qty=parseFloat($('#qty').val());
    var rate=parseFloat($('#rate').val());
    var amount=qty*rate;

        desamt=parseFloat(descper)*amount/100;
        $('#descper').val(parseFloat(descper).round(2));
        $('#desamt').val(parseFloat(desamt).round(2));
        calc();
}
function desc2(){
    var descper=$('#descper').val();
    var desamt=$('#desamt').val();
     var qty=parseFloat($('#qty').val());
    var rate=parseFloat($('#rate').val());
    var amount=qty*rate;

        descper=(parseFloat(desamt)/amount)*100;
        $('#descper').val(parseFloat(descper).round(2));
        $('#desamt').val(parseFloat(desamt).round(2));

        calc();
}
function getitem(e){
    var supplierid=$(e).val();
    var purchaseid=$('#purchaseid').val();
    //var sbranch=$(e).val();
        $.ajax({
                type: "POST",
                data:'supplierid='+supplierid+'&branchid='+$('#branchid').val(),
				url: 'showitem.php',
				cache: false,
				success: function (res) {
                    if(res.status=='success'){
                       $('#item-json1').val(JSON.stringify(res.json));
                       var json=res.json;
                       var str='<option value="Select">Select</option>';
                       for(var i in json){
                            str+='<option value="'+json[i].itemcode+'">'+json[i].itemname+'</option>';
                       }
                       $('#itemcode').html(str);
                       //$('#sbranch').prop('disabled', 'disabled');
                    }
				}
			});
        
    }
function totalcalc(){
    var amount=0;
    var desamt=0;
    var cgstamt=0;
    var sgstamt=0;
    var igstamt=0;
    var netamt=0;
    var count=0;
    var other=0;
    $('#table-order > tbody > tr').each(function(){
        if(count!=0){
            amount+=parseFloat($(this).find('.amount').text());
            desamt+=parseFloat($(this).find('.desamt').text());
            cgstamt+=parseFloat($(this).find('.cgstamt').text());
            sgstamt+=parseFloat($(this).find('.sgstamt').text());
            igstamt+=parseFloat($(this).find('.igstamt').text());
            netamt+=parseFloat($(this).find('.netamt').text());
        }else{
            count++;
        }
    });
    $('#totalnetamt').val(amount.round(2));
    $('#totaldescamt').val(desamt.round(2));
    $('#totalcgstamt').val(cgstamt.round(2));
    $('#totalsgstamt').val(sgstamt.round(2));
    $('#totaligstamt').val(igstamt.round(2));
    
    if($('#totalother').val()!=''){
        other=parseFloat($('#totalother').val());
    }
    var totaltotal=netamt+other;
    $('#totaltotal').val(totaltotal.round(2));

}
function insert(){
    var data=modals.tabletdbyclass('table-order');
    data.shift();
    $.ajax({
        type: "POST",
        data:{
            pobranchid:$('#branchid').val(),
            podate:$('#podate').val(),
            supplierid:$('#supplierid').val(),
            purchaseid:$('#purchaseid').val(),
            data1:$('#inp-post-itemjson').val(),
            data:JSON.stringify(data)
        },
        url: 'insert.php',
        cache: false,
        success: function (res) {
            if(res.status=='success'){
                window.location= "../purchase-order.php";
            }
        }
	});
}
var editremover='';
function editor(itemcode,e){
    if(purchaseid==""){
        var json1 = JSON.parse($("#item-json1").val());
        json1.forEach(myFunction)
        function myFunction(item, index, arr){
        if(itemcode==item.itemcode){
            var data=modals.tabletdbyclass('table-order');
            data.forEach(myFunction)
            function myFunction(item, index, arr){
            if(itemcode==item.itemcode){
                $('#itemcode').prop('disabled', 'disabled');
                $("#itemcode").append('<option value="'+item.itemcode+'" selected>'+item.itemcode+'</option>');
                $("#itemcode").val(item.itemcode).trigger("change");
                $("#qty").val(item.qty);
                $("#qty").val(item.qty).trigger("change");
                $("#unit").val(item.unit);
                $("#rate").val(item.rate);
                $("#rate").val(item.rate).trigger("change");
                $("#descper").val(item.descper);
                $("#descper").val(item.descper).trigger("change");
                $("#desamt").val(item.desamt);
                $("#desamt").val(item.desamt).trigger("change");
                $("#amount").val(item.amount);
                $("#amount").val(item.amount).trigger("change");
                $("#cgstper").val(item.cgstper);
                $("#cgstper").val(item.cgstper).trigger("change");
                $("#cgstamt").val(item.cgstamt);
                $("#cgstamt").val(item.cgstamt).trigger("change");
                $("#sgstamt").val(item.sgstamt);
                $("#sgstamt").val(item.sgstamt).trigger("change");
                $("#sgstper").val(item.sgstper);
                $("#sgstper").val(item.sgstper).trigger("change");
                $("#igstamt").val(item.igstamt);
                $("#igstamt").val(item.igstamt).trigger("change");
                $("#igstper").val(item.igstper);
                $("#igstper").val(item.igstper).trigger("change");
                $("#netamt").val(item.netamt);
                editremover=$(e).parent().find('.btn-danger');
            }
        }
        }
        }
    }
   json2.forEach(myFunction)
    function myFunction(item, index, arr) {
        if(itemcode==item.itemcode){
        $('#itemcode').prop('disabled', 'disabled');
         $("#itemcode").append('<option value="'+item.itemcode+'" selected>'+item.itemcode+'</option>');
         $("#itemcode").val(item.itemcode).trigger("change");
         $("#qty").val(item.qty);
         $("#qty").val(item.qty).trigger("change");
         $("#unit").val(item.unit);
         $("#rate").val(item.rate);
         $("#rate").val(item.rate).trigger("change");
         $("#descper").val(item.descper);
         $("#descper").val(item.descper).trigger("change");
         $("#desamt").val(item.descamt);
         $("#desamt").val(item.descamt).trigger("change");
         $("#amount").val(item.amount);
         $("#amount").val(item.amount).trigger("change");
         $("#cgstper").val(item.cgstper);
         $("#cgstper").val(item.cgstper).trigger("change");
         $("#cgstamt").val(item.cgstamt);
         $("#cgstamt").val(item.cgstamt).trigger("change");
         $("#sgstamt").val(item.sgstamt);
         $("#sgstamt").val(item.sgstamt).trigger("change");
         $("#sgstper").val(item.sgstper);
         $("#sgstper").val(item.sgstper).trigger("change");
         $("#igstamt").val(item.igstamt);
         $("#igstamt").val(item.igstamt).trigger("change");
         $("#igstper").val(item.igstper);
         $("#igstper").val(item.igstper).trigger("change");
         $("#netamt").val(item.netamount);
         editremover=$(e).parent().find('.btn-danger');
        }
    }
}

function copy(){
			$.ajax({
				type: "POST",
				data: 'purchaseid='+ copypurchaseid,
				url: 'select.php',
				cache: false,
				success: function (res) {
					if (res.status == 'success') {
                       var json=res.json;
                       json2=json;
                       var json1=res.json1;
                        $("#branchid").append('<option value="'+json1.poforbranch+'" selected>'+json1.branchname+'</option>');
                        $("#branchid").val(json1.poforbranch).trigger("change");
                        $("#supplierid").append('<option value="'+json1.supplierid+'" selected>'+json1.suppliername+'</option>');
                        $('#supplierid').prop('disabled', 'disabled');
                        json.forEach(myFunction)
                        function myFunction(item, index, arr) {
                            $("#itemcode").html('<option value="'+item.itemcode+'">'+item.itemname+'</option>');
                            $("#itemcode").val(item.itemcode).trigger("change");
                            $("#qty").val(item.qty);
                            $("#qty").val(item.qty).trigger("change");
                            $("#unit").val(item.unit);
                            $("#rate").val(item.rate);
                            $("#rate").val(item.rate).trigger("change");
                            $("#descper").val(item.descper);
                            $("#descper").val(item.descper).trigger("change");
                            $("#desamt").val(item.descamt);
                            $("#desamt").val(item.descamt).trigger("change");
                            $("#amount").val(item.amount);
                            $("#amount").val(item.amount).trigger("change");
                            $("#cgstper").val(item.cgstper);
                            $("#cgstper").val(item.cgstper).trigger("change");
                            $("#cgstamt").val(item.cgstamt);
                            $("#cgstamt").val(item.cgstamt).trigger("change");
                            $("#sgstamt").val(item.sgstamt);
                            $("#sgstamt").val(item.sgstamt).trigger("change");
                            $("#sgstper").val(item.sgstper);
                            $("#sgstper").val(item.sgstper).trigger("change");
                            $("#igstamt").val(item.igstamt);
                            $("#igstamt").val(item.igstamt).trigger("change");
                            $("#igstper").val(item.igstper);
                            $("#igstper").val(item.igstper).trigger("change");
                            $("#netamt").val(item.netamount);
                            $("#btn-add").click();
                        }
                    }
				}
            });
}
function edit(){
    $('#btn-submit').attr('onclick','update()');
    $('#btn-submit').text('Update');
    $('#purchaseid').val(purchaseid);
			$.ajax({
				type: "POST",
				data: 'purchaseid='+ purchaseid,
				url: 'select.php',
				cache: false,
				success: function (res) {
					if (res.status == 'success') {
                       var json=res.json;
                       json2=json;
                       var json1=res.json1;
                        $("#editbranchid").val(json1.poforbranch);
                        $("#editsupplierid").val(json1.supplierid);
                        $("#editsbranch").val(json1.sbranch); 
                        $("#branchid").append('<option value="'+json1.poforbranch+'" selected>'+json1.branchname+'</option>');
                        $("#branchid").val(json1.poforbranch).trigger("change");
                        $("#supplierid").append('<option value="'+json1.supplierid+'" selected>'+json1.suppliername+'</option>');
                        $('#supplierid').prop('disabled', 'disabled');
                        // $("#branchid").val(json1.poforbranch).trigger("change");
                        $("#sbranch").append('<option value="'+json1.sbranch+'" selected>'+json1.sbranch+'</option>');
                        $("#sbranch").val(json1.sbranch).trigger("change");
                        $('#sbranch').prop('disabled', 'disabled');
                        json.forEach(myFunction)
                        function myFunction(item, index, arr) {
                            $("#itemcode").html('<option value="'+item.itemcode+'">'+item.itemname+'</option>');
                            $("#itemcode").val(item.itemcode).trigger("change");
                            $("#qty").val(item.qty);
                            $("#qty").val(item.qty).trigger("change");
                            $("#unit").val(item.unit);
                            $("#rate").val(item.rate);
                            $("#rate").val(item.rate).trigger("change");
                            $("#descper").val(item.descper);
                            $("#descper").val(item.descper).trigger("change");
                            $("#desamt").val(item.descamt);
                            $("#desamt").val(item.descamt).trigger("change");
                            $("#amount").val(item.amount);
                            $("#amount").val(item.amount).trigger("change");
                            $("#cgstper").val(item.cgstper);
                            $("#cgstper").val(item.cgstper).trigger("change");
                            $("#cgstamt").val(item.cgstamt);
                            $("#cgstamt").val(item.cgstamt).trigger("change");
                            $("#sgstamt").val(item.sgstamt);
                            $("#sgstamt").val(item.sgstamt).trigger("change");
                            $("#sgstper").val(item.sgstper);
                            $("#sgstper").val(item.sgstper).trigger("change");
                            $("#igstamt").val(item.igstamt);
                            $("#igstamt").val(item.igstamt).trigger("change");
                            $("#igstper").val(item.igstper);
                            $("#igstper").val(item.igstper).trigger("change");
                            $("#netamt").val(item.netamount);
                            $("#btn-add").click();
                        }
                    }
				}
            });
}
var branchid=$('#inp-session-branchid').val();
$('#branchid').val(branchid);

var inppostbranchid=$('#inp-post-branchid').val();
if(inppostbranchid){
    $('#branchid').val(inppostbranchid).trigger('change');
    var supplierid=$('#inp-post-supplierid').val();    
    console.log(supplierid);
    $('#supplierid').val(supplierid).trigger('change');
    var itemjson=JSON.parse($('#inp-post-itemjson').val());
   
    var str='';
    var len=itemjson.length;
    var counter=0;
    console.log();
    console.log(itemjson);
   var inter= setInterval(function(){
        if(len>counter){
            $("#itemcode").val(itemjson[counter].itemcode).trigger('change');
            $("#qty").val(itemjson[counter].qty);
            $("#unit").val(itemjson[counter].unit);
            $("#rate").val(0);
            $("#descper").val(0);
            $("#desamt").val(0);
            $("#amount").val(0);
            $("#cgstper").val(0);
            $("#cgstamt").val(0);
            $("#sgstamt").val(0);
            $("#sgstper").val(0);
            $("#igstamt").val(0);
            $("#igstper").val(0);
            $("#netamt").val(0);  
            adder();
            counter++; 
        }else{
            clearInterval(inter);
        }
    }, 400);

    // for(var i in itemjson){
    //     $("#itemcode").val(itemjson[i].itemcode).trigger('change');
    //     $("#qty").val(itemjson[i].qty);
    //     $("#unit").val(itemjson[i].unit);
    //     $("#rate").val(0);
    //     $("#descper").val(0);
    //     $("#desamt").val(0);
    //     $("#amount").val(0);
    //     $("#cgstper").val(0);
    //     $("#cgstamt").val(0);
    //     $("#sgstamt").val(0);
    //     $("#sgstper").val(0);
    //     $("#igstamt").val(0);
    //     $("#igstper").val(0);
    //     $("#netamt").val(0);  
       
  //  }
}

</script>