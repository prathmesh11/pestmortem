

<?php 
  $base='../../';
  $navenq5='background:#1B1464;';
  include('header.php');

  if (!in_array("Purchase Requisition", $arrysession)) {
   
    echo "<script>window.location.href='/process/dashboard.php';</script>";
   
    exit;

  }

  if (session_status() == PHP_SESSION_NONE) { session_start(); }
  $sessionby = $_SESSION['employeeid'];
  $branchid = $_SESSION['branchid'];

?>
<style>
  .table-list td,
    .table-list th {
        border: 1px solid #ddd;
        padding: 1px !important;
        font-size: 13px;
    }
    .table-list td{
    padding-top: 2px !important;
      margin:0;

    }

    .table-list tr:nth-child(even) {
        background-color: #f2f2f2;
    }

    .table-list th {
        padding-top: 5px;
        padding-bottom: 5px;
        text-align: center;
        background-color: #16a085;
        color: white;
    }
</style>

<img class="hidden" id=imageid src="../../../../img/PestMortemLogo.jpg">

<?php   
   $admin_branch='';
  if($_SESSION['role']=='admin'){
      $branchid_admin='';
      echo '<select onchange="branchadmin(this)" class="form-control input-sm" style="margin:2px;">';
      if($_COOKIE['branchid']!=''){
        $branchid_admin=$_COOKIE['branchid'];
        $branchname1=mysqli_fetch_assoc(mysqli_query($con,"SELECT branchname x FROM branchmaster WHERE branchid='$branchid_admin'"))['x'];
        echo '<option value="'.$branchid_admin.'">'.$branchname1.'</option>';
      }
        echo '<option value="allbranch">All Branches</option>';
      $select1=mysqli_query($con,"SELECT branchid, branchname FROM branchmaster WHERE branchid<>'$branchid_admin'");
      while($rows = mysqli_fetch_assoc($select1)){
        echo '<option value="'.$rows['branchid'].'">'.$rows['branchname'].'</option>';
      }
    
  echo '</select>';
    }
?>

<div class="table-ui container-fluid">
  <div class="tr row">
    <div class="col-sm-3 th">PR NO.</div>
    <div class="col-sm-7 th">Item list</div>
    <div class="col-sm-2 th">Action</div>
  </div>
  <?php 
   if($_SESSION['role']=='admin'){
      if($branchid_admin==''){
        
          $select=mysqli_query($con,"SELECT DISTINCTROW pr_no,branchid FROM `purchase_req` WHERE rejected_time=0 AND close_pr_time=0");
      }else{
          $select=mysqli_query($con,"SELECT DISTINCTROW pr_no,branchid FROM `purchase_req` WHERE rejected_time=0 AND close_pr_time=0 AND branchid='$branchid_admin'");
      }
   }else{
    $select=mysqli_query($con,"SELECT DISTINCTROW pr_no,branchid FROM `purchase_req` WHERE rejected_time=0 AND branchid='$branchid' AND close_pr_time=0");
   }

    while($rows = mysqli_fetch_assoc($select)){
      $pr_no=$rows['pr_no'];
      $branchid=$rows['branchid'];

      //$branchid=mysqli_fetch_assoc(mysqli_query($con,"SELECT branchid x FROM `purchase_req` WHERE pr_no='$pr_no'"))['x'];
        ?>
<div class="tr row">
  <div class="col-sm-3 td">
      <?php  echo 'PRNo. : '.$pr_no.'<br>'; 
      $department=mysqli_fetch_assoc(mysqli_query($con,"SELECT department x FROM `purchase_req` WHERE pr_no='$pr_no'"))['x'];
      $groupName=mysqli_fetch_assoc(mysqli_query($con, "SELECT groupName x FROM `additionalmaster` WHERE groupCode='$department'"))['x'];
      echo 'DEPT. : '.$groupName.'<br>';
      $branchname=mysqli_fetch_assoc(mysqli_query($con, "SELECT branchname x FROM `branchmaster` WHERE branchid='$branchid'"))['x'];
      echo 'Branch : '.$branchname;
      ?>
  </div>
    <div class="col-sm-7 td">
      <div class="table-responsive">
        <table id="keywords" cellspacing="0" cellpadding="0" class="table table-list" data-name="item">
          <thead>
            <tr>
              <th><span>Item Name.</span></th>
              <th><span>DEC</span></th>
              <th><span>Qty</span></th>
              <th><span>Supplier</span></th>
              <th><span>Unit</span></th>
              <th><span>Remark</span></th>
            </tr>
          </thead>
          <tbody>
            <?php 
 
 

    $selecttable=mysqli_query($con,"SELECT * FROM `purchase_req` WHERE pr_no='$pr_no' AND branchid='$branchid' ");
    $approvard_time=0;
    while($rows1 = mysqli_fetch_assoc($selecttable)){
      $approvard_time=$rows1['approvard_time'];
      echo '<tr>
      <td><span>'.$rows1['itemname'].'</span></td>
      <td><span>'.$rows1['descip'].'</span></td>
      <td><span>'.$rows1['qty'].'</span></td>
      <td><span>'.$rows1['suppliername'].'</span></td>
      <td><span>'.$rows1['unit'].'</span></td>
      <td><span>'.$rows1['remark'].'</span></td>
    
    </tr>';
    }
        ?>
          </tbody>
        </table>
      </div>
    </div>

    <div class="col-sm-2 td">
<?php 

if($approvard_time==0){
?>
  <a class="btn btn-sm btn-block btn-primary" onclick="this.href='purchase-requisition/index.php?pr_no=' + '<?php echo $pr_no; ?>'; return true">Edit</a>
  <button class="btn btn-sm btn-block btn-warning" data-prno='<?php echo $pr_no; ?>' onclick="deletepr(this)">Remove</button>
<?php 
}
?>

  </div>
</div>
        <?php
    }

  ?>
  <a class="btn" href="purchase-requisition/index.php" style="font-size:25px;border-radius:50px;width:60px;height:60px;position: fixed;bottom: 40px;right: 40px;background:#eb2f06;color:#fff;" >+</a>

<?php 
include('footer.php');
?>
<script>



function deletepr(e) {
  if (confirm('are You Sure to Delete It')) {
    var prno = $(e).data('prno');
    console.log(prno);
    
    $.ajax({
        type: "POST",
        data: 'prno=' + prno,
        url: 'api/deletepr.php',
        cache: false,
        success: function (res) {
            if (res.status == 'success') {
               location.reload();
            }
        }
    });
  }
}

</script>