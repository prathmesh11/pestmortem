<?php 
    $base='../../';
    $navenq7='background:#1B1464;';
    include('header.php');

    if (!in_array('Reporting', $arrysession)) {
   
        echo "<script>window.location.href='/process/dashboard.php';</script>";
       
        exit;

    }
    if (session_status() == PHP_SESSION_NONE) { session_start(); }
    $sessionby = $_SESSION['employeeid'];
    $branchid = $_SESSION['branchid'];

?>
<style>
     .table-list td,
    .table-list th {
        border: 1px solid #ddd;
        padding: 1px !important;
        font-size: 13px;
    }
    .table-list td{
        padding-top: 2px !important;
        margin:0;

    }

    .table-list tr:nth-child(even) {
        background-color: #f2f2f2;
    }

    .table-list th {
        padding-top: 5px;
        padding-bottom: 5px;
        text-align: center;
        background-color: #16a085;
        color: white;
    }
</style>
<br>
<div class="row">
   <div class="col-sm-2">
   Select Branch : 
   <select class="form-control input-sm" id="branchid">
   <option value="Select">ALL BRANCHES</option>
   <?php
      $select1=mysqli_query($con,"SELECT branchid, branchname FROM branchmaster");
      while($rows = mysqli_fetch_assoc($select1)){
        echo '<option value="'.$rows['branchid'].'">'.$rows['branchname'].'</option>';
      }
?>
</select>
</div>
<div class="col-sm-2">
   Select Supplier : 
   <select class="form-control input-sm" id="supplierid">
   <option value="Select">ALL SUPPLIERS</option>
   <?php
      $select1=mysqli_query($con,"SELECT supplierid,suppliername FROM `suppliermaster`");
      while($rows = mysqli_fetch_assoc($select1)){
        echo '<option value="'.$rows['supplierid'].'">'.$rows['suppliername'].'</option>';
      }
?>
</select>
   </div>
   <div class="col-sm-2">
   Select Camical: 
   <select class="form-control input-sm" id="stockid">
   <option value="Select">ALL CAMICALS</option>
   <?php
      $select1=mysqli_query($con,"SELECT stockid,itemname FROM `stockmaster`");
      while($rows = mysqli_fetch_assoc($select1)){
        echo '<option value="'.$rows['stockid'].'">'.$rows['itemname'].'</option>';
      }
?>
</select>
   </div>

   <div class="col-sm-2">
   FROM date: 
      <input type="date" class="form-control input-sm" id="fromdate" />
   </div>
   <div class="col-sm-2">
   TO date : 
      <input type="date"class="form-control input-sm" id="todate" />
   </div>
   <div class="col-sm-2">.
  <button class="btn btn-sm btn-primary btn-block" onclick="search()">Search</button>
   </div>
</div>
<br>
<div class="row" style="padding:10px;">
    <table class="table table-list form-group">
    <thead>
    <tr>
    <th width="100">Branch</th>
    <th width="250">Supplier</th>
    <th width="200">PO.NO</th>
    <th width="600">Camical</th>
    <th width="100">Unit</th>
    <th width="100">Order Qty</th>
    <th width="100">Received Qty</th>
    <th width="100">Order Date</th>
    <th width="100">Received date</th>
    </tr>
    </thead>
    <tbody>
    </tbody>
    </table>
</div>
<?php 
include('footer.php');
?>
<script>
function search() {
    var branchid = $('#branchid').val(),
        supplierid = $('#supplierid').val(),
        stockid = $('#stockid').val(),
        fromdate = $('#fromdate').val(),
        todate = $('#todate').val();

    if (branchid == 'Select') {
        branchid = '';
    }
    if (supplierid == 'Select') {
        supplierid = '';
    }
    if (stockid == 'Select') {
        stockid = '';
    }
    $('.table-list > tbody > tr').remove();
    $.ajax({
        type: "POST",
        data: 'branchid=' + branchid + '&supplierid=' + supplierid + '&stockid=' + stockid + '&fromdate=' + fromdate + '&todate=' + todate,
        url: 'api/poreport.php',
        cache: false,
        success: function (res) {
            if (res.status == 'success') {
                var json=res.json;
                for(var i in json){
                    var str='';
                        str+='<tr>';
                        str+='<td class="branchname">'+json[i].branchname+'</td>';
                        str+='<td class="branchname">'+json[i].suppliername+'</td>';
                        str+='<td class="purchaseid">'+json[i].purchaseid+'</td>';
                        str+='<td class="itemname">'+json[i].itemname+'</td>';
                        str+='<td class="unit">'+json[i].unit+'</td>';
                        str+='<td class="orderqty">'+json[i].orderqty+'</td>';
                        str+='<td class="receivedqty">'+json[i].receivedqty+'</td>';
                        str+='<td class="podate">'+json[i].podate+'</td>';
                        str+='<td class="gateentrytime">'+json[i].gateentrytime+'</td>';
                        str+='</tr>';
                    $('.table-list > tbody').append(str);
                }
            }
        }
    });
}

</script>