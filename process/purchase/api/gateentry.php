<?php
    
    $base = '../../../';

    include($base.'_in/connect.php');

    header('content-type: application/json; charset=utf-8');

    header("access-control-allow-origin: *");

    if(isset($_POST['qty']) && isset($_POST['ids'])){

        $con = _connect();
      
        if (session_status()==PHP_SESSION_NONE) { session_start(); }

        $created_by = $_SESSION['employeeid'];

        $qty        = $_POST['qty'];

        $ids        = $_POST['ids'];

        $select     = mysqli_fetch_assoc(mysqli_query($con,"SELECT branchid, poforbranch, purchaseid, podate, qty,supplierid, itemname, itemcode, unit, rate, descper, descamt, amount, cgstper, sgstper, igstper, cgstamt, sgstamt, igstamt, netamount FROM purchase WHERE id='$ids'"));
                
            $branchid    = $select['branchid'];
            
            $poforbranch = $select['poforbranch'];
            
            $purchaseid  = $select['purchaseid'];
                
            $podate      = $select['podate'];

            $supplierid  = $select['supplierid'];

            $itemname    = $select['itemname'];

            $itemcode    = $select['itemcode'];

            $unit        = $select['unit'];

            $rate        = $select['rate'];

            $descper     = $select['descper'];

            $cgstper     = $select['cgstper'];

            $sgstper     = $select['sgstper'];

            $igstper     = $select['igstper'];

            $chkqty      = $select['qty'];

            $amountbefore = $qty*$rate;

            $descamt      = ($descper*$amountbefore)/100;

            $amount       = $amountbefore-$descamt;

            $cgstamt      = $cgstper*$amount/100;

            $sgstamt      = $sgstper*$amount/100;

            $igstamt      = $igstper*$amount/100;

            $netamount    = $amount+$cgstamt+$sgstamt+$igstamt;
            
            $selectbatch  = mysqli_query($con,"SELECT * FROM `batchnos` WHERE itemid=$itemcode");

            while($row = mysqli_fetch_assoc($selectbatch)){

                $id      = $row['id'];

                $itemid  = $row['itemid'];

                $batchno = $row['batchno'];

                $quntity = $row['quntity'];

                $json.=',{"batchno":"'.$batchno.'","quntity":"'.$quntity.'"}';

            }

            $json = substr($json,1);

            $json = '['.$json.']';

            $unit2value = mysqli_fetch_assoc(mysqli_query($con,"SELECT unit2value x FROM stockmaster WHERE stockid = '$itemcode' "))['x'];

            $stockQty =  $qty * $unit2value;

            //echo $stockQty;
                
            $create = mysqli_query($con,"INSERT INTO gateentry (branchid,poforbranch,purchaseid, podate, supplierid,itemname,itemcode, qty, unit, rate, descper, descamt, amount, cgstper, sgstper, igstper, cgstamt, sgstamt, igstamt, netamount, created_by, created_time) VALUES ('$branchid','$poforbranch','$purchaseid','$podate','$supplierid','$itemname','$itemcode','$qty','$unit','$rate','$descper','$descamt','$amount','$cgstper','$sgstper','$igstper','$cgstamt','$sgstamt','$igstamt','$netamount','$created_by','$CURRENT_MILLIS')");

   
                if($create){

                    $sumqty = mysqli_fetch_assoc(mysqli_query($con,"SELECT SUM(qty) x FROM gateentry WHERE purchaseid='$purchaseid' AND itemcode='$itemcode'"))['x'];

                    if($chkqty==$sumqty){

                        mysqli_query($con,"UPDATE purchase SET gateentry_by='$created_by', gateentry_time='$CURRENT_MILLIS' WHERE id='$ids'");
                        
                        $sel = mysqli_fetch_assoc(mysqli_query($con,"SELECT * FROM `livestockmaster` WHERE itemcode='$itemcode' AND branchid='$poforbranch' ORDER BY id DESC LIMIT 1"));

                        if($sel){

                            $opning       = $sel['opning'];

                            $quality      = $sel['quality']+$stockQty;

                            $issue        = $sel['issue'];

                            $adjust_plus  = $sel['adjust_plus'];

                            $adjust_minus = $sel['adjust_minus'];

                            $outqty       = $sel['outqty'];

                            $inqty        = $sel['inqty'];

                            $total        = ($opning+$quality+$adjust_plus+$inqty)-($issue+$adjust_minus+$outqty);

                        } else {

                            $opning       = 0;

                            $quality      = $stockQty;

                            $issue        = 0;

                            $adjust_plus  = 0;

                            $adjust_minus = 0;

                            $outqty       = 0;

                            $inqty        = 0;

                            $total        = ($opning+$quality+$adjust_plus+$inqty)-($issue+$adjust_minus+$outqty);

                        }
                         
                        mysqli_query($con,"INSERT into livestockmaster ( branchid,itemcode,opning,quality,issue,adjust_plus,adjust_minus,outbranch,outqty,inbranch,inqty,total,created_by,created_time) Values ('$poforbranch','$itemcode','$opning','$quality','$issue','$adjust_plus','$adjust_minus','$outbranch','$outqty','$inbranch','$inqty','$total','$created_by','$CURRENT_MILLIS')");

                        mysqli_query($con,"INSERT into livegovstockmaster ( branchid,itemcode,opning,quality,issue,adjust_plus,adjust_minus,outbranch,outqty,inbranch,inqty,total,created_by,created_time) Values ('$poforbranch','$itemcode','$opning','$quality','$issue','$adjust_plus','$adjust_minus','$outbranch','$outqty','$inbranch','$inqty','$total','$created_by','$CURRENT_MILLIS')");
                
                        echo '{"status":"success"}';

                    } else {

                        $sel = mysqli_fetch_assoc(mysqli_query($con,"SELECT * FROM `livestockmaster` WHERE itemcode='$itemcode' AND branchid='$poforbranch' ORDER BY id DESC LIMIT 1"));

                        if($sel){

                            $opning       = $sel['opning'];

                            $quality      = $sel['quality']+$stockQty;

                            $issue        = $sel['issue'];

                            $adjust_plus  = $sel['adjust_plus'];

                            $adjust_minus = $sel['adjust_minus'];

                            $outqty       = $sel['outqty'];

                            $inqty        = $sel['inqty'];

                            $total        = ($opning+$quality+$adjust_plus+$inqty)-($issue+$adjust_minus+$outqty);

                        } else {

                            $opning       = 0;

                            $quality      = $stockQty;

                            $issue        = 0;

                            $adjust_plus  = 0;

                            $adjust_minus = 0;

                            $outqty       = 0;

                            $inqty        = 0;

                            $total        = ($opning+$quality+$adjust_plus+$inqty)-($issue+$adjust_minus+$outqty);

                        }
                         
                        mysqli_query($con,"INSERT into livestockmaster ( branchid,itemcode,opning,quality,issue,adjust_plus,adjust_minus,outbranch,outqty,inbranch,inqty,total,created_by,created_time) Values ('$poforbranch','$itemcode','$opning','$quality','$issue','$adjust_plus','$adjust_minus','$outbranch','$outqty','$inbranch','$inqty','$total','$created_by','$CURRENT_MILLIS')");

                        mysqli_query($con,"INSERT into livegovstockmaster ( branchid,itemcode,opning,quality,issue,adjust_plus,adjust_minus,outbranch,outqty,inbranch,inqty,total,created_by,created_time) Values ('$poforbranch','$itemcode','$opning','$quality','$issue','$adjust_plus','$adjust_minus','$outbranch','$outqty','$inbranch','$inqty','$total','$created_by','$CURRENT_MILLIS')");

                        echo '{"status":"success"}';

                    }

                    
                } else {

                    echo '{"status":"falid"}';

                }

        _close($con);

    } else {

        echo '{"status":"falid"}';

    }
?>