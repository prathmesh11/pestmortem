<?php
    $base='../../../';
    include($base.'_in/connect.php');
    header('content-type: application/json; charset=utf-8');
    header("access-control-allow-origin: *");
    if(isset($_POST['qty']) && isset($_POST['ids'])){
      $con=_connect();

      if (session_status()==PHP_SESSION_NONE) { session_start(); }
      $created_by=$_SESSION['employeeid'];
                $qty=$_POST['qty'];
                $ids=$_POST['ids'];
                $select=mysqli_fetch_assoc(mysqli_query($con,"SELECT branchid, poforbranch, purchaseid, podate, qty,supplierid, itemname, itemcode, unit, rate, descper, descamt, amount, cgstper, sgstper, igstper, cgstamt, sgstamt, igstamt, netamount FROM gateentry WHERE id='$ids'"));
                $branchid=$select['branchid'];
                $poforbranch=$select['poforbranch'];
                $purchaseid=$select['purchaseid'];
                $podate=$select['podate'];
                $supplierid=$select['supplierid'];
                $itemname=$select['itemname'];
                $itemcode=$select['itemcode'];
                $unit=$select['unit'];
                $rate=$select['rate'];
                $descper=$select['descper'];
                $cgstper=$select['cgstper'];
                $sgstper=$select['sgstper'];
                $igstper=$select['igstper'];

                $chkqty=$select['qty'];

                $amountbefore=$qty*$rate;
                $descamt=($descper*$amountbefore)/100;
                $amount=$amountbefore-$descamt;
                $cgstamt=$cgstper*$amount/100;
                $sgstamt=$sgstper*$amount/100;
                $igstamt=$igstper*$amount/100;
                $netamount=$amount+$cgstamt+$sgstamt+$igstamt;
                
                $create=mysqli_query($con,"INSERT INTO quality (gateentryid,branchid, poforbranch, purchaseid, podate, supplierid,itemname,itemcode, qty, unit, rate, descper, descamt, amount, cgstper, sgstper, igstper, cgstamt, sgstamt, igstamt, netamount, created_by, created_time) VALUES ('$ids','$branchid','$poforbranch','$purchaseid','$podate','$supplierid','$itemname','$itemcode','$qty','$unit','$rate','$descper','$descamt','$amount','$cgstper','$sgstper','$igstper','$cgstamt','$sgstamt','$igstamt','$netamount','$created_by','$CURRENT_MILLIS')");
                mysqli_query($con,"UPDATE gateentry SET confirm_by='$created_by', confirm_time='$CURRENT_MILLIS' WHERE id='$ids'");
                    
                $opning=0;
                $quality=$qty;
                $issue=0;
                $adjust_plus=0;
                $adjust_minus=0;
                $outbranch=0;
                $outqty=0;
                $inbranch=0;
                $inqty=0;
                $total=$qty;
          
                $sel=mysqli_fetch_assoc(mysqli_query($con,"SELECT * FROM `livestockmaster` WHERE itemcode='$itemcode' AND branchid='$branchid' ORDER BY id DESC LIMIT 1"));                 
              
                if($sel){
                    $opning=$sel['opning'];
                    $quality=$sel['quality']+$qty;
                    $issue=$sel['issue'];
                    $adjust_plus=$sel['adjust_plus'];
                    $adjust_minus=$sel['adjust_minus'];
                    $outqty=$sel['outqty'];
                    $inqty=$sel['inqty'];
                    $total=($opning+$quality+$adjust_plus+$inqty)-($issue+$adjust_minus+$outqty);
                 }
                 mysqli_query($con,"INSERT into livestockmaster ( branchid,itemcode,opning,quality,issue,adjust_plus,adjust_minus,outbranch,outqty,inbranch,inqty,total,created_by,created_time) Values ('$branchid','$itemcode','$opning','$quality','$issue','$adjust_plus','$adjust_minus','$outbranch','$outqty','$inbranch','$inqty','$total','$created_by','$CURRENT_MILLIS')");
               
                if($create){
         
              
                    $sumqtyg=mysqli_fetch_assoc(mysqli_query($con,"SELECT SUM(qty) x FROM gateentry WHERE purchaseid='$purchaseid' AND itemcode='$itemcode'"))['x'];
                    $sumqtyp=mysqli_fetch_assoc(mysqli_query($con,"SELECT qty x FROM purchase WHERE purchaseid='$purchaseid' AND itemcode='$itemcode'"))['x'];
                    if($sumqtyg==$sumqtyp){
                            mysqli_query($con,"UPDATE purchase SET quality_by='$created_by', quality_time='$CURRENT_MILLIS' WHERE purchaseid='$purchaseid' AND itemcode='$itemcode'");
                         
                            echo '{"status":"success"}';
                        }else{
                            echo '{"status":"success"}';
                        }
                        
                }else{
                    echo '{"status":"falid"}';
                }
            _close($con);
     }else{
        echo '{"status":"falid"}';
     }
?>