<?php 
 
    $base    = '../../';
    
    $navenq3 = 'background:#1B1464;';
    
    include('header.php');

    if (!in_array('Payment Bill', $arrysession)) {
   
        echo "<script>window.location.href='/process/dashboard.php';</script>";
       
        exit;

    }

    if (session_status() == PHP_SESSION_NONE) { session_start(); }
    
    $sessionby = $_SESSION['employeeid'];
    
    $branchid  = $_SESSION['branchid'];

?>

<style>

    .table-list td,
    .table-list th {
        border: 1px solid #ddd;
        padding: 1px !important;
        font-size: 13px;
    }

    .table-list td {
        padding-top: 2px !important;
        margin: 0;
        text-align: center;
    }

    .table-list tr:nth-child(even) {
        background-color: #f2f2f2;
    }

    .table-list th {
        padding-top: 5px;
        padding-bottom: 5px;
        text-align: center;
        background-color: #16a085;
        color: white;
    }

</style>

<?php  

    $admin_branch = '';

    if($_SESSION['role']=='admin'){

        $branchid_admin = '';
        
        echo '<select onchange="branchadmin(this)" class="form-control input-sm" style="margin:2px;">';
        
            if($_COOKIE['branchid']!=''){

                $branchid_admin = $_COOKIE['branchid'];
                
                $branchname1    = mysqli_fetch_assoc(mysqli_query($con,"SELECT branchname x FROM branchmaster WHERE branchid='$branchid_admin'"))['x'];
            
                echo '<option value="'.$branchid_admin.'">'.$branchname1.'</option>';
            }
            
            echo '<option value="allbranch">All Branches</option>';
      
            $select1 = mysqli_query($con,"SELECT branchid, branchname FROM branchmaster WHERE branchid<>'$branchid_admin'");
      
            while($rows = mysqli_fetch_assoc($select1)){
        
                echo '<option value="'.$rows['branchid'].'">'.$rows['branchname'].'</option>';
      
            }
    
        echo '</select>';

    }

?>

<div class="table-ui container-fluid">
    
    <div class="tr row">
        
        <div class="col-sm-1 th">Order No.</div>
        
        <div class="col-sm-1 th">Bill Details</div>
        
        <div class="col-sm-2 th">Supplier Info</div>
        
        <div class="col-sm-6 th">Order</div>
        
        <div class="col-sm-2 th">Action</div>
    </div>
        
    <?php
          
        if($_SESSION['role']=='admin'){
            
            if($branchid_admin==''){
                
                $result=mysqli_query($con,"SELECT DISTINCTROW purchaseid,podate,poforbranch,billid FROM `purchasebill` WHERE purchasebillno <>'' AND payment_time = 0 AND confirm_time<>0 ORDER BY id DESC");
            
            } else {
                
                $result=mysqli_query($con,"SELECT DISTINCTROW purchaseid,podate,poforbranch,billid FROM `purchasebill` WHERE  poforbranch='$branchid_admin' AND purchasebillno <>'' AND payment_time = 0 AND confirm_time<>0 ORDER BY id DESC");
            
            }
           
        } else {
            
            $result=mysqli_query($con,"SELECT DISTINCTROW purchaseid,podate,poforbranch,billid FROM `purchasebill` WHERE poforbranch='$branchid' AND purchasebillno <>'' AND payment_time = 0  AND confirm_time<>0 ORDER BY id DESC");
          
        }
         
        while($rows=mysqli_fetch_assoc($result)){

            $purchaseid   = $rows['purchaseid'];
                
            $podate       = $rows['podate'];
            
            $poforbranch  = $rows['poforbranch'];
            
            $billid       = $rows['billid'];

            $BillDetails  = mysqli_fetch_assoc(mysqli_query($con,"SELECT branchid,pobillimglink,purchasebillno,purchasedate FROM `purchasebill` WHERE billid='$billid' ORDER BY id DESC LIMIT 1"));
                
            $supplierid   = mysqli_fetch_assoc(mysqli_query($con,"SELECT supplierid x FROM `purchase` WHERE purchaseid='$purchaseid' ORDER BY id DESC LIMIT 1"))['x'];      
            
            $supplierjson = get_object_vars(json_decode(mysqli_fetch_assoc(mysqli_query($con,"SELECT supplierjson x FROM suppliermaster WHERE supplierid='$supplierid'"))['x']));
            
            $branchname   = mysqli_fetch_assoc(mysqli_query($con, "SELECT branchname x FROM `branchmaster` WHERE branchid='$poforbranch'"))['x'];
          
    ?>
        
    <div class="tr row">
        
        <div class="col-sm-1 td" style="word-wrap:break-word;">
            
            <?php echo $purchaseid; ?><br><br><?php echo $branchname; ?>
        
        </div>
        
        <div class="col-sm-1 td" style="word-wrap:break-word;">
            
            <?php echo date("d-m-Y",strtotime($BillDetails['purchasedate'])); ?>
            <br>
            <?php echo "Bill No : ".$BillDetails['purchasebillno']; ?>

            <a class="btn-info btn-sm" href="<?php echo $BillDetails['pobillimglink'];?>" target="_blank">Bill Image</a>


        
        </div>
        
        <div class="col-sm-2 td" style="word-wrap:break-word;">
            
            <?php
                    
                $contactjson = json_decode(mysqli_fetch_assoc(mysqli_query($con,"SELECT contactjson x FROM suppliermaster WHERE supplierid='$supplierid'"))['x']);
                
                $sbranch     = mysqli_fetch_assoc(mysqli_query($con,"SELECT sbranch x FROM `purchase` WHERE purchaseid='$purchaseid' ORDER BY id DESC LIMIT 1"))['x'];      
              
                foreach($contactjson as $i){
                    // if(get_object_vars($i)['sbranch']==$sbranch){
                    echo 'Company : '.$supplierjson['suppliername']; 
                    echo '<br> Branch: '.get_object_vars($i)['sbranch']; 
                    echo '<br> Address : '.get_object_vars($i)['sbranchaddress'];
                    echo '<br> Mobile : '.get_object_vars($i)['mobile']; 
                    echo '<br> email : '.get_object_vars($i)['email']; 
                    // }
                }
                
            ?>

        </div>
        
        <div class="col-sm-6 td" style="word-wrap:break-word;">
            
            <?php 
            
                $purchsedetails = mysqli_fetch_assoc(mysqli_query($con,"SELECT id,close_time  FROM `purchase` WHERE purchaseid='$purchaseid' ORDER BY id DESC"));      

                $result1        = mysqli_query($con,"SELECT * FROM `purchasebill` WHERE purchaseid='$purchaseid' AND billid='$billid' ");
           
                echo '<table class="table table-list form-group">';
           
                    echo '<thead>';
                        
                        echo '<tr>';
                            
                            echo '<th width="300">Item Name</th>';
                            
                            echo '<th>Qty</th>';
                            
                            echo '<th width="80">Unit</th>';
                            
                            echo '<th>Rate</th>';
                            
                            echo '<th>DISC.</th>';
                            
                            echo '<th>Amount</th>';
                            
                            echo '<th>CGST</th>';
                            
                            echo '<th>SGST</th>';
                            
                            echo '<th>IGST</th>';
                            
                            echo '<th>Net Amount</th>';
                            
                            echo '<th >Close</th>';
                        
                        echo '</tr>';
                    
                    echo '</thead>';
                    
                    echo '<tbody>';
                        
                        $total = 0;
                        
                        while($rowss=mysqli_fetch_assoc($result1)){
                            
                            echo '<tr>';
                                
                                echo '<td>'.$rowss['itemname'].'</td>';
                                
                                echo '<td>'.$rowss['qty'].'</td>';
                                
                                echo '<td>'.$rowss['unit'].'</td>';
                                
                                echo '<td>'.$rowss['rate'].'</td>';
                                
                                echo '<td>'.$rowss['descamt'].'</td>';
                                
                                echo '<td>'.$rowss['amount'].'</td>';
                                
                                echo '<td>'.$rowss['cgstamt'].'</td>';
                                
                                echo '<td>'.$rowss['sgstamt'].'</td>';
                                
                                echo '<td>'.$rowss['igstamt'].'</td>';
                                
                                echo '<td>'.$rowss['netamount'].'</td>';
                                
                                echo '<td>';

                                    if($purchsedetails['close_time']!=0){
                                        
                                        echo 'Close';

                                    }

                                echo '</td>';
                                
                                $total+=$rowss['netamount'];
                            
                            echo '</tr>';
                        
                        }
                    echo '</tbody>';
                    
                echo '</table>';
                    
                echo 'Total : '.$total;
   
               
            ?>

        </div>

        <div class="col-sm-2 td" style="word-wrap:break-word;">
            
            <button class="btn btn-info btn-block" data-billid="<?php echo $billid;?>" data-supplierid="<?php echo $supplierid;?>" data-branchid="<?php echo $BillDetails['branchid'];?>" data-purchaseid="<?php echo $purchaseid;?>" data-amount="<?php echo $total;?>" data-toggle="modal" data-target="#myModal" onclick="openModelPayment(this)">Payment</button>
        
        </div>
        
    </div>

    <?php } ?>

</div>


<div id="myModal" class="modal fade" role="dialog">

    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">

            <div class="modal-header">

                <h4 class="modal-title" align="center">Payment</h4>

            </div>

            <div class="modal-body" id="pestBill">

                <div class="col-sm-4" >
                    
                    <div class="form-group">

                        <label for="usr">Payment Date</label>

                        <input class="form-control" type="date" data-role="date" data-name="paymentdate" id="paymentdate" value="<?php echo $today;?>">

                    </div>

                </div>

                <div style="clear:both;"></div>

                <div class="col-sm-4" >

                    <input type="hidden" class="form-control" data-role="text" data-name="billid" id="billid" >
                    
                    <input type="hidden" class="form-control" data-role="text" data-name="supplierid" id="supplierid">
                    
                    <input type="hidden" class="form-control" data-role="text" data-name="branchid" id="branchid">
                    
                    <input type="hidden" class="form-control" data-role="text" data-name="purchaseid" id="purchaseid">

                    <div class="form-group">

                        <label for="usr">Instrument Type</label>

                        <select class="form-control input-md" data-role="select" data-name="instrumentType" id="instrumentType" onchange="">

                            <option value="Select">Select Instrument Type</option>

                            <option value="Cash">Cash</option>

                            <option value="Cheque">Cheque</option>

                            <option value="NEFT">NEFT</option>

                        </select>

                    </div>

                </div>

                <div class="col-sm-4">
                    
                    <div class="form-group">

                        <label for="usr">Instrument No</label>

                        <input type="text" class="form-control"  data-name="instrumentNo" id="instrumentNo">

                    </div>

                </div>


                <div class="col-sm-4" >
                    
                    <div class="form-group">

                        <label for="usr">Instrument Date</label>

                        <input class="form-control" type="date" data-role="date" data-name="instrumentDate" id="instrumentDate" value="<?php echo $today;?>">

                    </div>

                </div>

                <div class="col-sm-4">
                    
                    <div class="form-group">

                        <label for="usr">Amount</label>

                        <input type="number" data-role="text" class="form-control" data-name="amount" data-role="number" id="amount" readonly>

                    </div>

                </div>


                <div class="col-sm-8">
                        
                    <div class="form-group">

                        <label for="usr">Remark</label>

                        <textarea type="text" class="form-control" data-role="text" data-name="remark" id="remark"></textarea>

                    </div>

                </div>
                            
                <div class="clr"></div>


                <div class="form-group">

                    <button class="btn btn-success btn-block btn-sm" id="newBill" onclick="insert()">Add New Bill</button>

                </div>

            </div>

        </div>

    </div>

</div>

<?php 
include('footer.php');
?>
<script>


function openModelPayment(e) {

    $('#billid').val($(e).attr('data-billid'));

    $('#supplierid').val($(e).attr('data-supplierid'));

    $('#branchid').val($(e).attr('data-branchid'));

    $('#purchaseid').val($(e).attr('data-purchaseid'));

    $('#amount').val($(e).attr('data-amount'));

}

function insert() {

    var valid = true;

    var arr = [];

    if (checker('pestBill') != false) {

        valid = valid * true;

    } else {

        valid = valid * false;

    }

    if (valid) {

        var data = checker('pestBill');

        var datastr = JSON.stringify(data);

        $.ajax({

            type: "POST",

            data: {
                data: datastr,
                
                branchid: $('#branchid').val(),
            },

            url: 'api/billpayment.php',

            cache: false,

            success: function (res) {

                if (res.status == 'success') {

                    window.location.reload();

                }

            }

        })

    }

}


$('#myModal').on('hidden.bs.modal', function (e) {

    $(this).find("input,textarea,select").val('').end();

})

</script>