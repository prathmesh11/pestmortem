<?php 
$base='../../';
$navenq1='background:#1B1464;';
include('header.php');

if (!in_array("Purchase Requisition", $arrysession)) {
   
  echo "<script>window.location.href='/process/dashboard.php';</script>";
 
  exit;

}

$enquiryidnew=mysqli_fetch_assoc(mysqli_query($con,"SELECT id x FROM crmmaster ORDER BY id DESC"))['x'];

  $year1=0;
  $year2=0;

if ( date('m') > 3 ) {
    $year1 = date('y');
    $year2 = date('y')+1;
}else{
    $year1 = date('y')-1;
    $year2 = date('y');
}

if (session_status() == PHP_SESSION_NONE) { session_start(); }
$sessionby = $_SESSION['employeeid'];
$branchid = $_SESSION['branchid'];

$branchshortname=mysqli_fetch_assoc(mysqli_query($con,"SELECT branchshortname x FROM branchmaster WHERE branchid='$branchid'"))['x'];

  if($enquiryidnew){
    $enquiryidnew++;
    $enquiryidnew=str_pad($enquiryidnew,6, '0', STR_PAD_LEFT);
    $enquiryidnew='PMI/'.$branchshortname.'/ENQ/'.$enquiryidnew.'/'.$year1.'-'.$year2;
  }else{
    $enquiryidnew='PMI/'.$year1.'-'.$year2.'/B'.$branchid.'/E'.$sessionby.'/1';
    $enquiryidnew='PMI/'.$branchshortname.'/ENQ/000001/'.$year1.'-'.$year2;
  }

?>
<br>
<style>
.input-container {
 
  max-width: 300px;
  background-color: #EDEDED;
  border: 1px solid #DFDFDF;
  border-radius: 5px;
}

input[type='file'] {
  display: none;
}

.file-info {
  font-size: 0.9em;
}

.browse-btn {
  background: #03A595;
  color: #fff;
  min-height: 35px;
  padding: 10px;
  border: none;
  border-top-left-radius: 5px;
  border-bottom-left-radius: 5px;
}

.browse-btn:hover {
  background: #4ec0b4;
}

@media (max-width: 300px) {
  button {
    width: 100%;
    border-top-right-radius: 5px;
    border-bottom-left-radius: 0;
  }
  
  .file-info {
    display: block;
    margin: 10px 5px;
  }
}
</style>
<div class="table-ui container-fluid">
<div class="tr row">
<div class="col-sm-1 th">Requisition_No.</div>    
<div class="col-sm-3 th">Supplier Info</div>
<div class="col-sm-5 th">Order</div>
<div class="col-sm-1 th">Order Date</div>
<div class="col-sm-2 th">Action</div>
</div>

<?php
$result=mysqli_query($con,"SELECT requisitionid,supplierid,itemjson FROM purchasemaster WHERE branchid='$branchid' AND approved_by=0 AND approved_time=0 ORDER BY created_time DESC");
while($rows = mysqli_fetch_assoc($result)){

    $customerid=$rows['customerid'];

    $cifo=mysqli_fetch_assoc(mysqli_query($con,"SELECT * FROM customermaster WHERE customerid='$customerid'"));

?>

<div class="row tr">
<div class="col-sm-1 td" style="word-wrap:break-word;"><?php echo $rows['enquiryid']; ?></div>
<div class="col-sm-3 td">
<?php 

echo 'Company : '.$cifo['customername']; 
echo '<br>Address : '.get_object_vars(json_decode($cifo['customerjson']))['address']; 
echo '<br>Name  : '.get_object_vars(json_decode($cifo['contactjson'])[0])['name'];
echo '<br>Dept. : '.get_object_vars(json_decode($cifo['contactjson'])[0])['department'];
echo '<br>Mobile : <a href="tel:'.get_object_vars(json_decode($cifo['contactjson'])[0])['mobile'].'">'.get_object_vars(json_decode($cifo['contactjson'])[0])['mobile'].'</a>';
echo '<br>Landline'.get_object_vars(json_decode($cifo['contactjson'])[0])['landline'];
echo '<br>Email : <a href="mailto:'.get_object_vars(json_decode($cifo['contactjson'])[0])['email'].'">'.get_object_vars(json_decode($cifo['contactjson'])[0])['email'].'</a>';
?>
</div>
<div class="col-sm-1 td"><?php echo $rows['category']; ?></div>
<div class="col-sm-2 td">
<!-- offer  -->

<div class="input-container">
  <input type="file" class="real-input" name="quatation" accept="application/pdf">
    <button class="browse-btn">
      Browse Files
    </button>
  <span class="file-info">Upload a file</span>
</div>
<button class="btn btn-primary btn-sm btn-block" data-enquiryid="<?php echo $rows['enquiryid']; ?>" onclick="upload(this)">Upload </button>

<?php 
    $enquir=str_replace('/', '-',$rows['enquiryid']);
    $path="api/offer/".$enquir.'.pdf';
  if(file_exists($path)){
    echo '<br><center><a href="'.$path.'" target="_blank">View PDF</a></center>';
  }else{
    echo '<br>Please Upload .pdf File';
  }
?>



<!-- offer  -->
</div>
<div class="col-sm-2 td"><?php echo $rows['followupnote']; ?></div>

<div class="col-sm-1 td"><?php 

$lastfollowup=$rows['followuptime'];
$followUpStatusFlag='';

if($lastfollowup!=0){
  if($START_OF_THE_DAY<$lastfollowup && $END_OF_THE_DAY<=$lastfollowup){
      $followUpStatusFlag='#3F51B5';
  }

  if($START_OF_THE_DAY>$lastfollowup && $END_OF_THE_DAY>$lastfollowup){
      $followUpStatusFlag='#F44336';
  }
  
  if($START_OF_THE_DAY<=$lastfollowup && $END_OF_THE_DAY>$lastfollowup ){
      $followUpStatusFlag='#4CAF50';
  }
}else{
  $followUpStatusFlag='#ff9800';
}
 if($lastfollowup==0){ $lastfollowup='NO Followup'; }else{ $lastfollowup=date("d-m-Y",$lastfollowup/1000); }
echo '<p style="background:'.$followUpStatusFlag.';color:#fff;">'.$lastfollowup.'</p>'; 

?></div>

<div class="col-sm-2 td">
<button class="btn btn-primary btn-sm btn-block" data-toggle="modal" data-target="#myModal2" data-enquiryid="<?php echo $rows['enquiryid']; ?>" onclick="setidtomidal(this)">Followup</button>
<button class="btn btn-success btn-sm btn-block" data-link1="enquiry_time" data-link2="enquiry_by" data-enquiryid="<?php echo $rows['enquiryid']; ?>" onclick="confirms(this)">Order Confirm</button>
<button class="btn btn-sm btn-block btn-danger" data-enquiryid="<?php echo $rows['enquiryid']; ?>" onclick="closes(this)">Order Close</button>

</div>

</div>
<?php
}
?>
</div>
</div>
</div>

<a class="btn" href="purchase-req/index.php" style="font-size:25px;border-radius:50px;width:60px;height:60px;position: fixed;bottom: 40px;right: 40px;background:#eb2f06;color:#fff;" >+</a>

<?php 
include('footer.php');
?>
<script>


</script>