<?php 
    $base='../../';
    $navenq2='background:#1B1464;';
    include('header.php');

    if (!in_array('Gate Entry', $arrysession)) {
   
      echo "<script>window.location.href='/process/dashboard.php';</script>";
     
      exit;

  }

    if (session_status() == PHP_SESSION_NONE) { session_start(); }
    $sessionby = $_SESSION['employeeid'];
    $branchid = $_SESSION['branchid'];

?>
<style>
    .table-list td,
    .table-list th {
        border: 1px solid #ddd;
        padding: 1px !important;
        font-size: 13px;
    }
    .table-list td{
        padding-top: 2px !important;
        margin:0;

    }

    .table-list tr:nth-child(even) {
        background-color: #f2f2f2;
    }

    .table-list th {
        padding-top: 5px;
        padding-bottom: 5px;
        text-align: center;
        background-color: #16a085;
        color: white;
    }
</style>
  <?php   
   $admin_branch='';
  if($_SESSION['role']=='admin'){
      $branchid_admin='';
      echo '<select onchange="branchadmin(this)" class="form-control input-sm" style="margin:2px;">';
      if($_COOKIE['branchid']!=''){
        $branchid_admin=$_COOKIE['branchid'];
        $branchname1=mysqli_fetch_assoc(mysqli_query($con,"SELECT branchname x FROM branchmaster WHERE branchid='$branchid_admin'"))['x'];
        echo '<option value="'.$branchid_admin.'">'.$branchname1.'</option>';
      }
        echo '<option value="allbranch">All Branches</option>';
      $select1=mysqli_query($con,"SELECT branchid, branchname FROM branchmaster WHERE branchid<>'$branchid_admin'");
      while($rows = mysqli_fetch_assoc($select1)){
        echo '<option value="'.$rows['branchid'].'">'.$rows['branchname'].'</option>';
      }
    
  echo '</select>';
    }
?>
<?php
       if($_SESSION['role']=='admin'){
        echo '<input id="ip-branch" class="hidden" value="'.$branchid_admin.'">';
       }else{
        echo '<input id="ip-branch" class="hidden" value="'.$branchid.'">';
       }
    ?>
<img class="hidden" id=imageid src="../../../../img/PestMortemLogo.jpg">
<div class="table-ui container-fluid">
    <div class="tr row">
        <div class="col-sm-1 th">Order No.</div>
        <div class="col-sm-2 th">Supplier Info</div>
        <div class="col-sm-9 th">Order</div>

        </div>
        <?php
         if($_SESSION['role']=='admin'){
        
            if($branchid_admin==''){
                $result=mysqli_query($con,"SELECT DISTINCTROW purchaseid,podate,poforbranch FROM `purchase` WHERE confirm_time<>0 AND gateentry_time=0 AND purchasebill_time=0 AND close_time=0 ORDER BY id DESC");
            }else{
                $result=mysqli_query($con,"SELECT DISTINCTROW purchaseid,podate,poforbranch FROM `purchase` WHERE confirm_time<>0 AND gateentry_time=0 AND purchasebill_time=0 AND close_time=0 AND poforbranch='$branchid_admin' ORDER BY id DESC");
            }
        }else{
            $result=mysqli_query($con,"SELECT DISTINCTROW purchaseid,podate,poforbranch FROM `purchase` WHERE poforbranch='$branchid' AND confirm_time<>0 AND gateentry_time=0 AND purchasebill_time=0 AND close_time=0 ORDER BY id DESC");
        }
           while($rows=mysqli_fetch_assoc($result)){
                $purchaseid=$rows['purchaseid'];
                $podate=$rows['podate'];
                $poforbranch=$rows['poforbranch'];
                $supplierid=mysqli_fetch_assoc(mysqli_query($con,"SELECT supplierid x FROM `purchase` WHERE purchaseid='$purchaseid' ORDER BY id DESC LIMIT 1"))['x'];      
                $supplierjson=get_object_vars(json_decode(mysqli_fetch_assoc(mysqli_query($con,"SELECT supplierjson x FROM suppliermaster WHERE supplierid='$supplierid'"))['x']));
                $branchname=mysqli_fetch_assoc(mysqli_query($con, "SELECT branchname x FROM `branchmaster` WHERE branchid='$poforbranch'"))['x'];
               ?>
        <div class="tr row">
        <div class="col-sm-1 td" style="word-wrap:break-word;"><?php echo $purchaseid; ?><br><br><?php echo $branchname; ?></div>
        <div class="col-sm-2 td" style="word-wrap:break-word;">
        <?php
                   $contactjson=json_decode(mysqli_fetch_assoc(mysqli_query($con,"SELECT contactjson x FROM suppliermaster WHERE supplierid='$supplierid'"))['x']);
                   $sbranch=mysqli_fetch_assoc(mysqli_query($con,"SELECT sbranch x FROM `purchase` WHERE purchaseid='$purchaseid' ORDER BY id DESC LIMIT 1"))['x'];      
             foreach($contactjson as $i){
               //if(get_object_vars($i)['sbranch']==$sbranch){
                echo 'Company : '.$supplierjson['suppliername']; 
                // echo '<br> Branch: '.get_object_vars($i)['sbranch']; 
                // echo '<br> Address : '.get_object_vars($i)['sbranchaddress'];
                echo '<br> Mobile : '.get_object_vars($i)['mobile']; 
                echo '<br> email : '.get_object_vars($i)['email']; 
               //}
               break;
            }
            if($_SESSION['role']=='admin'){
            echo '<button class="btn btn-sm btn-block btn-warning" data-purchaseid='.$purchaseid.' onclick="printer(this)">Print</button>';
            }
      ?>
        </div>
        <div class="col-sm-9 td" style="word-wrap:break-word;">
        <?php 
        if($_SESSION['role']=='admin'){
            $result1=mysqli_query($con,"SELECT * FROM `purchase` WHERE purchaseid='$purchaseid' AND confirm_time<>0 AND gateentry_time=0 AND quality_time=0 AND purchasebill_time=0 AND close_time=0");
     
        }else{
            $result1=mysqli_query($con,"SELECT * FROM `purchase` WHERE purchaseid='$purchaseid' AND poforbranch='$branchid' AND confirm_time<>0 AND gateentry_time=0 AND quality_time=0 AND purchasebill_time=0 AND close_time=0");
     
        }
      
       echo '<table class="table table-list form-group">';
       echo '<thead>';
       echo '<tr>';
       echo '<th>Item Name</th>';
       echo '<th width="100">ORDER_QTY</th>';
       echo '<th width="100">PENDING_QTY</th>';
       echo '<th width="100">GET_QTY</th>';
       echo '<th width="100">Unit</th>';
       echo '<th style="display:none;">Rate</th>';
       echo '<th style="display:none;">DISC.</th>';
       echo '<th style="display:none;">Amount</th>';
       echo '<th style="display:none;">CGST</th>';
       echo '<th style="display:none;">SGST</th>';
       echo '<th style="display:none;">IGST</th>';
       echo '<th style="display:none;">Net Amount</th>';
       echo '<th >Action</th>';
       echo '</tr>';
       echo '</thead>';
       echo '<tbody>';
       while($rowss=mysqli_fetch_assoc($result1)){
                echo '<tr>';
                echo '<td align="center"  class="itemname">'.$rowss['itemname'].'</td>';
                $itemcode=$rowss['itemcode'];
                $sumqty=mysqli_fetch_assoc(mysqli_query($con,"SELECT SUM(qty) x FROM gateentry WHERE purchaseid='$purchaseid' AND itemcode='$itemcode'"))['x'];

                echo '<td align="center"  class="qty">'.($rowss['qty']).'</td>';

                echo '<td align="center"  class="qty">'.($rowss['qty']-$sumqty).'</td>';
                echo '<td align="center" ><input type="text" class="form-control input-sm" onkeyup="gateqty(this)" value="'.($rowss['qty']-$sumqty).'" /></td>';
                echo '<td align="center"  style="display:none;" class="itemcode">'.$rowss['itemcode'].'</td>';
                echo '<td align="center"  class="unit">'.$rowss['unit'].'</td>';
                echo '<td align="center" style="display:none;" class="rate">'.$rowss['rate'].'</td>';
                echo '<td align="center"  style="display:none;" class="descper">'.$rowss['descper'].'</td>';
                echo '<td align="center" style="display:none;" class="descamt" >'.$rowss['descamt'].'</td>';
                echo '<td align="center" style="display:none;" class="amount">'.$rowss['amount'].'</td>';
                echo '<td align="center" style="display:none;" class="cgstamt">'.$rowss['cgstamt'].'</td>';
                echo '<td align="center" style="display:none;" class="sgstamt">'.$rowss['sgstamt'].'</td>';
                echo '<td align="center" style="display:none;" class="igstamt">'.$rowss['igstamt'].'</td>';
                echo '<td align="center" style="display:none;" class="cgstper" style="display:none;">'.$rowss['cgstper'].'</td>';
                echo '<td align="center" style="display:none;" class="sgstper" style="display:none;">'.$rowss['sgstper'].'</td>';
                echo '<td align="center" style="display:none;" class="igstper" style="display:none;">'.$rowss['igstper'].'</td>';
                echo '<td align="center" style="display:none;" class="netamount">'.$rowss['netamount'].'</td>';
                $itemid=$rowss['itemcode'];
                $ct=mysqli_fetch_assoc(mysqli_query($con,"SELECT count(id) x FROM batchnos WHERE itemid='$itemid' AND purchaseid='$purchaseid'"))['x'];
                echo '<td>';
                if($ct!=0){
                    echo '<button class="btn btn-sm btn-block btn-success" data-ids="'.$rowss['id'].'" data-itemcode="'.$rowss['itemcode'].'" onclick="gateentry(this)">Confirm</button>';
                    echo '<button class="btn btn-sm btn-block btn-info batch" title="batch Added" data-ids="'.$rowss['id'].'" data-pono="'.$purchaseid.'" data-branchId="'.$rowss['branchid'].'" onclick="getbatch(this)">Batch</button>';
                }else{
                    echo '<button class="btn btn-sm btn-block btn-success batch" data-ids="'.$rowss['id'].'" data-pono="'.$purchaseid.'" data-branchId="'.$rowss['branchid'].'" onclick="getbatch(this)">Batch</button>';
                    echo '<button class="btn btn-sm btn-block btn-primary" data-purchaseid="'.$purchaseid.'" onclick="editgateentry(this)">Edit</button></td>';
                }
                
                echo '</td>';
                echo '</tr>';
        }
                echo '</tbody>';
                echo '</table>';
       ?>
        </div>
       
        </div>
        <?php
            }
        ?>
        
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <input type="hidden" id="inp-id">
                <input type="hidden" id="inp-branchId">

                <h5 class="modal-title">Batch number for item : <input type="text" id="inp-itemname"  style="border: none" readonly></h5>
                 <p id="inp-qty">Total Quntity : <input type="text" id="inp-qty123"  style="border: none" readonly> </p>
            </div>
                <div class="modal-body">
                <div class="row">
                <input class="hidden" value="" id="purchaseid" >
                <table class="table table-list" id="table">
                    <thead>
                        <tr>
                            <th>Batch No</th>
                            <th>Quantity</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
                </div>
                <div>
                <button type="button" class="btn btn-primary btn-block" onclick="addbatch()">Add Batch</button>
                </div>
            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-primary btn-block btn-success" onclick="savetobatchno()">Submit</button>
            </div>
        </div>
        
        </div>
    </div>
        
</div>


<?php 
include('footer.php');
?>
<script>
function editgateentry(e){
  var editgateentry = $(e).data('purchaseid');
  window.location= '../../../process/purchase/purchase-order/index.php?editgateentry='+editgateentry;
}


var itemid=0;
function batchentry(e){
    var qty1=0;
    $('.clsqty').each(function(){
        var qty100=parseFloat($(this).val());
        if(isNaN(qty100)){
            qty100=0;
            $(e).val(0);
        }
       qty1 = qty1 + qty100;
    })
    var qty=parseFloat($("#inp-qty123").val());
    if(qty1>qty){
        alert('Quntity must be less than item Quntity');
        $(e).val(0);
    }
}

function savetobatchno(){
    var arr=[];
    var itemid=$("#inp-id").val();
    var valid=true;
    var qty1=0;
    var purchaseid=$("#purchaseid").val();
    $('.clsqty').each(function(){
        var qty100=parseFloat($(this).val());
        if(isNaN(qty100)){
            qty100=0;
            $(e).val(0);
        }
       qty1 = qty1 + qty100;
        })

    var qty=parseFloat($("#inp-qty123").val());
        if(qty1<qty){
            alert('Quntity must be less than item Quntity');
            valid=valid*false;
        }else{
            valid=valid*true;
        }

    $("#table tbody tr").each(function(){
        if ($(this).find('td input.clsbatch').val() != '' && $(this).find('td input.clsqty').val() != '0') {
            arr.push({
                purchaseid: $("#purchaseid").val(),
                batchno: $(this).find('td input.clsbatch').val(),
                qty: $(this).find('td input.clsqty').val()
            });
            valid = valid * true;
        } else {
            alert("Something went wrong!")
            valid = valid * false;
        }
    })

 

    if(arr!=''){
        valid=valid*true;
    }else{
        valid=valid*false;
    }
    if(valid){
        $.ajax({
            type: "POST",
            data: 'branchid='+$('#inp-branchId').val()+'&itemid=' + itemid+ '&purchaseid=' + purchaseid+ '&data=' + JSON.stringify(arr),
            url: 'api/batchentry.php',
            cache: false,
            success: function (res) {
                if (res.status == 'success') {
                    $('#myModal').modal('hide');
                    location.reload();
                }
            }
        }) 
    }
    
   
}

function addbatch() {

  $("#table tbody").append('<tr class="batchno"><td align="center"><input type="text"class="clsbatch"></td><td align="center"><input type="text" class="clsqty" value="0" onkeyup="batchentry(this)"></td><td><button onclick="remover(this)" class="btn-danger btn-sm"  align="center" style="padding: 1px 10px">Remove</button></td></tr>')

  $(".btn-remove").on('click', function () {

    $(this).parent().parent().remove();

  })
  
}

function getbatch(e){
    $('#table tbody').empty();
    var qty=$(e).parent().parent().find('input').val();  
    var ids=$(e).parent().parent().find('td.itemcode').text(); 
    var itemname=$(e).parent().parent().find('td.itemname').text(); 
    $("#inp-id").val(ids);
    $("#inp-qty123").val(qty);
    $("#inp-itemname").val(itemname);
    $("#purchaseid").val($(e).data('pono'));
    $("#inp-branchId").val($(e).attr('data-branchid'));

    itemid=ids;
    if(itemid){
    $.ajax({
            type: "POST",
            data: 'itemid=' + itemid+'&purchaseid='+$(e).data('pono')+'&branchid='+$(e).attr('data-branchid'),
            url: 'api/selectbatch.php',
            cache: false,
            success: function (res) {
                if (res.status == 'success') {
                    var json=res.json;
                    console.log(json);
                    json.forEach(myFunction);
                    function myFunction(item, index, arr) {
                        $('#myModal').modal();
                        $("#table tbody").append('<tr class="batchno"><td align="center"><input type="text"class="clsbatch" value='+item.batchno+'></td><td align="center"><input type="number" class="clsqty" value='+item.quntity+' onkeyup="batchentry(this)"></td><td><button onclick="remover(this)" class="btn-danger btn-sm"  align="center" style="padding: 1px 10px">Remove</button></td></tr>'
                            )
                    }
                }
                $('#myModal').modal();  
            }
        }) 
    }
}
function gateqty(e){

    var qty=parseFloat($(e).val());
    var qty1=parseFloat($(e).parent().parent().find('.qty').text());
    if(qty<=qty1){
        var rate=parseFloat($(e).parent().parent().find('.rate').text());
    var descper=parseFloat($(e).parent().parent().find('.descper').text());
    var cgstper=parseFloat($(e).parent().parent().find('.cgstper').text());
    var sgstper=parseFloat($(e).parent().parent().find('.sgstper').text());
    var igstper=parseFloat($(e).parent().parent().find('.igstper').text());

    var amountbefaor=rate*qty;
    var descamt=parseFloat(descper)*amountbefaor/100;
    var amount=amountbefaor-descamt;
    var cgstamt=cgstper*amount/100;
    var sgstamt=sgstper*amount/100;
    var igstamt=igstper*amount/100;
    var netamount=amount+cgstamt+sgstamt+igstamt;

    $(e).parent().parent().find('.descamt').text(descamt.round(2));
    $(e).parent().parent().find('.amount').text(amount.round(2));
    $(e).parent().parent().find('.cgstamt').text(cgstamt.round(2));
    $(e).parent().parent().find('.sgstamt').text(sgstamt.round(2));
    $(e).parent().parent().find('.igstamt').text(igstamt.round(2));
    $(e).parent().parent().find('.netamount').text(netamount.round(2));
    }else{
    alert('Quantity Wrong');
    $(e).val(qty1);
    gateqty(e);
}
}

function gateentry(e) {

  var qty = $(e).parent().parent().find('input').val();

  var ids = $(e).data('ids');

  if (confirm("Are You Sure TO Confirm Gate Entry?")) {

    $.ajax({

      type: "POST",

      data: 'qty=' + qty + '&ids=' + ids,

      url: 'api/gateentry.php',

      cache: false,

      success: function (res) {

        if (res.status == 'success') {

          window.location.reload();

        }

      }

    })

  }

}


function getBase64Image(img) {
  var canvas = document.createElement("canvas");
  canvas.width = img.width;
  canvas.height = img.height;
  var ctx = canvas.getContext("2d");
  ctx.drawImage(img, 0, 0);
  var dataURL = canvas.toDataURL("image/png");
  return dataURL.replace('/^data:image\/(png|jpg);base64,/', "");
}
function withDecimal(s) {
  n=Math.round(s * 100) / 100;
    var nums = n.toString().split('.')
    var whole = convertNumberToWords(nums[0])
    if (nums.length == 2) {
      var res1 = nums[1].slice(0, 1);
      var res2 = nums[1].slice(1, 2);
        var fraction = convertNumberToWords(res1)+''+convertNumberToWords(res2)
        return whole+'point '+fraction;
    } else {
        return whole;
    }
}
function convertNumberToWords(amount) {
    var words = new Array();
    words[0] = '';
    words[1] = 'One';
    words[2] = 'Two';
    words[3] = 'Three';
    words[4] = 'Four';
    words[5] = 'Five';
    words[6] = 'Six';
    words[7] = 'Seven';
    words[8] = 'Eight';
    words[9] = 'Nine';
    words[10] = 'Ten';
    words[11] = 'Eleven';
    words[12] = 'Twelve';
    words[13] = 'Thirteen';
    words[14] = 'Fourteen';
    words[15] = 'Fifteen';
    words[16] = 'Sixteen';
    words[17] = 'Seventeen';
    words[18] = 'Eighteen';
    words[19] = 'Nineteen';
    words[20] = 'Twenty';
    words[30] = 'Thirty';
    words[40] = 'Forty';
    words[50] = 'Fifty';
    words[60] = 'Sixty';
    words[70] = 'Seventy';
    words[80] = 'Eighty';
    words[90] = 'Ninety';
    amount = amount.toString();
    var atemp = amount.split(".");
    var number = atemp[0].split(",").join("");
    var n_length = number.length;
    var words_string = "";
    if (n_length <= 9) {
        var n_array = new Array(0, 0, 0, 0, 0, 0, 0, 0, 0);
        var received_n_array = new Array();
        for (var i = 0; i < n_length; i++) {
            received_n_array[i] = number.substr(i, 1);
        }
        for (var i = 9 - n_length, j = 0; i < 9; i++, j++) {
            n_array[i] = received_n_array[j];
        }
        for (var i = 0, j = 1; i < 9; i++, j++) {
            if (i == 0 || i == 2 || i == 4 || i == 7) {
                if (n_array[i] == 1) {
                    n_array[j] = 10 + parseInt(n_array[j]);
                    n_array[i] = 0;
                }
            }
        }
        value = "";
        for (var i = 0; i < 9; i++) {
            if (i == 0 || i == 2 || i == 4 || i == 7) {
                value = n_array[i] * 10;
            } else {
                value = n_array[i];
            }
            if (value != 0) {
                words_string += words[value] + " ";
            }
            if ((i == 1 && value != 0) || (i == 0 && value != 0 && n_array[i + 1] == 0)) {
                words_string += "Crores ";
            }
            if ((i == 3 && value != 0) || (i == 2 && value != 0 && n_array[i + 1] == 0)) {
                words_string += "Lakhs ";
            }
            if ((i == 5 && value != 0) || (i == 4 && value != 0 && n_array[i + 1] == 0)) {
                words_string += "Thousand ";
            }
            if (i == 6 && value != 0 && (n_array[i + 1] != 0 && n_array[i + 2] != 0)) {
                words_string += "Hundred and ";
            } else if (i == 6 && value != 0) {
                words_string += "Hundred ";
            }
        }
        words_string = words_string.split("  ").join(" ");
    }
    return words_string;
}

var purchaseid=$('#purchaseid').val();
  console.log(purchaseid);
  

function printer(e) {
    var purchaseid = $(e).data('purchaseid');
        $.ajax({
            type: "POST",
            data: 'purchaseid=' + purchaseid,
            url: 'api/purchaseorderbill.php',
            cache: false,
    success: function (res) {
      if (res.status == 'success') {
        var content=[];
        var json=res.json[0];
        var servicejson=json.servicejson;
        var count=0;
        var gstamt=0;
        var totalamt=0;
        var discount=0;
        var netamt=0;
        var border1=  [true,false,false,false];
        var totalamtword=0;
        var base64 = getBase64Image(document.getElementById("imageid"));
  content.push(
    {text:'PURCHASE ORDER\n\n',fontSize: 14,bold:1,alignment: "center"},
	    {
      table:{
      margin: [10, 0],
      widths: [75, 170,'*', '*'],
      body:[
        [
          {
          rowSpan: 2,
          stack: [{image:base64,
                    width: 75,
                    height:50,
                    margin:[0,20]
                    },],
          },
          {
          rowSpan: 2, 
          stack: [{
            text: [
            {text:'Pest Mortem (l) Pvt Ltd\n',fontSize: 10,bold:1,},
            {text:json.branchname+'\n',fontSize: 10},
            {text:json.address+'\n',fontSize: 10},
            {text:'GST No: '+json.gstnumber+' \n',fontSize: 10},
            {text:'State: '+json.statename+' \n',fontSize: 10},
            {text:'Email:'+json.email+' \n',fontSize: 10},
            {text:'Phone No:'+json.phone+'\n',fontSize: 10},
            ]},
            ],
          },
        {
        border:[1,1,0,0],
        text: [
          {text:'Po. No.\n',fontSize: 10},
          {text:json.purchaseid+'\n',fontSize: 10,bold:1},
          ]
        },
        {
        border:[1,1,1,0],
        text: [
            {text:'Dated\n',fontSize: 10},
            {text:json.podate+'\n',fontSize: 10,bold:1},
            ]
        }
    ],
    [
      {}, {},
      {
        border:[0,0,0,0],
        text: [
          // {text:'Supplier\'s Ref./Order No.\n',fontSize: 10},
          // {text:json.gstnumber+'\n',fontSize: 10,bold:1},
          {text:'\nDespatch through\n',fontSize: 10},
          {text:'',fontSize: 10},
          ]
        }, 
        { 
        border:[1,0,1,0],
        text: [
          {text:'Mode/term of Payment\n',fontSize: 10},
          {text:json.creditdays+' Days\n',fontSize: 10,bold:1},
          {text:'\nOther Reference(s)\n',fontSize: 10},
          {text:'',fontSize: 10,bold:1},
          ]
        }, 
      ],
    [
      {
      rowSpan: 2,
      colSpan: 2, 
          stack: [{
            text: [
            {text:'Supplier\'s\n',fontSize: 10},
            {text:json.suppliername+'\n',fontSize: 10,bold:1,},
            {text:json.sbranchaddress+'\n',fontSize: 10},
            {text:'Mumbai \n',fontSize: 10},
            {text:'GSTIN/UIN        : '+json.sbranchgstno+' \n',fontSize: 10},
            {text:'State Name       : '+json.statename+'\n',fontSize: 10}
            ]},
          ],    
        },
        {},
        {   
        rowSpan: 2,
        border:[1,1,1,1],
          text: [
              {text:'\nTerms of Delivery\n',fontSize: 10},
              {text:'\n',fontSize: 10,bold:1},
              ]
        }, 
        {
          rowSpan: 2,
          stack: [{
            text: [
              {text:'\nDestination\n',fontSize: 10,bold:1},
              {text:json.poforbadd+'\n',fontSize: 10},
            ]},
          ], 
          // text: [
          //   {text:'\nDestination\n',fontSize: 10,bold:1},
          //   {text:json.poforbadd+'\n',fontSize: 10},
          //   ]
          }
    ],
      [{}, {},{}, {}],
    ]
    },layout: {
        hLineColor: function (i, node) {
          return (i === 0 || i === node.table.body.length) ? 'gray' : 'gray';
        },
        vLineColor: function (i, node) {
          return (i === 0 || i === node.table.widths.length) ? 'gray' : 'gray';
        },
      }
    },
),

content.push(
  { 
    fontSize: 10,
    unbrackable:1,
    table: { 
        // widths: [10,'*','auto','auto','auto','auto','auto','auto','auto'],
        widths: [10,'*','auto','auto'],

        heights:[20],  
        body: [ 
        [
        {text:'#',color:'white', border: [1,1,1,1],alignment:'center'},
        {text:'Description of Goods',color:'white', border: [1,1,1,1],alignment:'center'},
        // {text:'GST%',color:'white', border: [1,1,1,1],alignment:'center'},
        // {text:'Due-on',color:'white', border: [1,1,1,1],alignment:'center'},
        {text:'Qty',color:'white', border: [1,1,1,1],alignment:'center'},
        {text:'PER',color:'white', border: [1,1,1,1],alignment:'center'},
        // {text:'Rate',color:'white', border: [1,1,1,1],alignment:'center'},
        // {text:'Dis%',color:'white', border: [1,1,1,1],alignment:'center'},
        // {text:'Amount',color:'white', border: [1,1,1,1],alignment:'center'},
        ],
      ] 
    },layout: { 
        fillColor: function (rowIndex) { 
          if(rowIndex===0){
            return ('#1E90FF');
          }
        },
        hLineColor: function (i, node) {
          return (i === 0 || i === node.table.body.length) ? 'gray' : 'gray';
        },
        vLineColor: function (i, node) {
          return (i === 0 || i === node.table.widths.length) ? 'gray' : 'gray';
        },
      } 
  },
),  
 servicejson.forEach(myFunction)
    function myFunction(item, index, arr) {
      //arr[index] = item * 10;
      count++;
      var gstper;
      var rate=item.rate;
      var amt=item.netamount;
      var dis=parseInt(item.descper);
      if(rate==0){
        rate="";
        amt="";
        dis="";
      }else{
        amt='₹'+amt;
        dis=dis+'%';
      }
     
      if(item.igstper==0){
        gstper=parseInt(item.cgstper)+parseInt(item.sgstper);
        gstamt=gstamt+parseFloat(item.cgstamt)+parseFloat(item.sgstamt);
      }else{
        gstper=parseInt(item.igstper);
        gstamt=gstamt+parseFloat(item.igstamt);
      }
      totalamt=totalamt+parseFloat(item.qty)*parseFloat(item.rate);
      discount=discount+parseFloat(item.descamt);
      netamt+=parseFloat(item.netamount);
      // if(item.qty>1){
      //   var unit=item.unit+"'S";
      // }
      content[2].table.heights.push(20)
      content[2].table.body.push(
        [ 
        {text:count, border: [1,1,1,1],alignment:'center'},
        {text:item.itemname, border: [1,1,1,1],alignment:'left'},
        // {text:gstper+'%', border: [1,1,1,1],alignment:'right'},
        // {text:'', border: [1,1,1,1],alignment:'center'},
        {text:item.qty, border: [1,1,1,1],alignment:'center'},
        {text:item.unit, border: [1,1,1,1],alignment:'center'},
        // {text:rate, border: [1,1,1,1],alignment:'center'},
        // {text:dis, border: [1,1,1,1],alignment:'center'},
        // {text:amt, border: [1,1,1,1],alignment:'right'}
        ],
      )
    }
    content[2].table.heights.push(20)
    if(netamt==0){
        netamt="";
        gstamt="";
        discount="";
        totalamt="";
        totalamtword="";
      }else{
        totalamtword=withDecimal(netamt);
        netamt='₹'+netamt;
        gstamt='₹'+gstamt;
        discount='₹'+discount;
        totalamt='₹'+totalamt;
      }
    
      content.push(
          {text:'\n'},
          {
        columns: [
          
          {
            fontSize: 10,
            margin: [326, 0, 0, 15],
            table: {
              widths: [85,85],
              body: [
                
                [

                {colSpan:2,text:'\n\nAuthorised Signatory\n', border: [0, 0, 0, 0],alignment:'center'}
                ]
                
                
              ]
            },
            layout: { 
                  fillColor: function (rowIndex) { 
                    if(rowIndex===3){
                        return ('#1E90FF');
                      }else{
                        if(rowIndex===0 || rowIndex===1 || rowIndex===2){
                          return ('#D3D3D3');
                        }
                      }
                    }
                } 
          }
       ]
    },
      )
    dd = {
        pageSize: 'A4',
        pageOrientation: 'portrait',
        //pageMargins: [40, 30, 30, 35],
        pageMargins: [ 50, 30, 30, 40 ],
        footer: function (currentPage, pageCount) {
          return {
            margin: 10,
            columns: [{
              fontSize: 9,
              text: [{
                  text: '--------------------------------------------------------------------------' +
                    '\n',
                  margin: [0, 20]
                },
                {
                  text: '© Pest Mortem (India) PVT. LTD. | PAGE ' + currentPage.toString() + ' of ' + pageCount,
                }
              ],
              alignment: 'center'
            }]
          };

        },
        content,
        styles: {
          tablfont: {
            fontSize: 9
          },
          tablfont1: {
            fontSize: 9
          }
        }
      }
      var win = window.open('', '_blank');
      pdfMake.createPdf(dd).open({}, win);

    }
      }
    });
  }
</script>