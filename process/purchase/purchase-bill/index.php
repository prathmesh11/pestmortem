<?php 

	$base    = '../../../';

	$navenq4 = 'background:#1B1464;';

	include('../header.php');

	if (!in_array('Purchase Bill', $arrysession)) {
   
        echo "<script>window.location.href='/process/dashboard.php';</script>";
       
        exit;

    }
	
?>

<br>
<style>

	body {
		background: #eee;
	}

	.table-list td,
	.table-list th {
		border: 1px solid #ddd;
		padding: 1px !important;
		font-size: 13px;
	}

	.table-list td {
		padding-top: 2px !important;
		margin: 0;

	}

	.table-list tr:nth-child(even) {
		background-color: #f2f2f2;
	}

	.table-list th {
		padding-top: 5px;
		padding-bottom: 5px;
		text-align: center;
		background-color: #16a085;
		color: white;
	}
	
</style>

<div class="container-fluid">

	<input type="hidden" id="billid" data-name="billid" value='<?php echo $_GET['billid']; ?>' />

	<div class="row" id="div-select"
		style="border-radius:4px;border:1px solid #CCC;background:#FFF;padding-top:10px;-webkit-box-shadow: 2px 2px 3px 3px #ccc; -moz-box-shadow:2px 2px 3px 3px #ccc;box-shadow:2px 2px 3px 3px #ccc;">

		<div class="col-sm-3">

			<div class="col-sm-5">Order for Branch</div>

			<div class="col-sm-7">

				<div class="form-group">

					<select class="form-control input-sm select-js1" data-role="select" data-name="branchid" id="branchid" onchange="pono()" disabled="true">

						<option value="Select">Select</option>

						<?php

							$result1=mysqli_query($con, "SELECT branchname,branchid FROM `branchmaster`");

								while($rows=mysqli_fetch_assoc($result1)) {

									echo '<option value="'.$rows['branchid'].'">'.$rows['branchname'].'</option>';
									
								}		
							
						?>

					</select>

				</div>

			</div>

		</div>

		<div class="col-sm-3">

			<div class="col-sm-4">PO NO.</div>

			<div class="col-sm-8">

				<div class="form-group">

					<select class="form-control input-sm select-js1" data-role="select" data-name="purchaseid" id="purchaseid" onchange="itemDetails()">

						<option value="Select">Select PO. NO.</option>

					</select>

				</div>

			</div>

		</div>

		<div class="col-sm-3">

			<div class="col-sm-4">BILL DATE</div>

			<div class="col-sm-8">

				<div class="form-group">

					<input type="date" id="purchasedate" data-name="purchasedate" value="<?php echo $today;?>"class="form-control input-sm">

				</div>

			</div>

		</div>

		

		<div class="col-sm-2">

			<button class="btn btn-sm btn-success btn-block" id="btn-insert" onclick="insert()">Submit</button>

		</div>


		<div class="col-sm-3">

			<div class="col-sm-5">BILL NO.</div>

			<div class="col-sm-7">

				<div class="form-group">

					<input type="text" id="purchasebillno" data-name="purchasebillno" class="form-control input-sm">

				</div>

			</div>

		</div>

		<div class="col-sm-4">

			<div class="col-sm-3">BILL FILE</div>

			<div class="col-sm-9">

				<div class="form-group">

					<input type="file" class="form-control input-xm file" id="pobillimglink" name="pobillimglink">

				</div>

			</div>

		</div>

		<div class="col-sm-3">

			<a href="" id="showImg" class="btn btn-sm btn-info btn-block" style="display:none;" target="_blank"> Bill Img</a>

		</div>


	</div>

	<br>

	<div class="row"
		style="border-radius:4px;border:1px solid #CCC;background:#FFF;padding-top:10px;-webkit-box-shadow: 2px 2px 3px 3px #ccc; -moz-box-shadow:2px 2px 3px 3px #ccc;box-shadow:2px 2px 3px 3px #ccc;">

		<table id="table-order" class="table table-list form-group">

			<thead>

                <tr>

                    <th rowspan="2" width="300">Item Name</th>

                    <th rowspan="2">Qty</th>

                    <th rowspan="2" width="80">Unit</th>

                    <th rowspan="2" >Rate</th>

                    <th rowspan="2" >DISC.</th>

                    <th rowspan="2" >Amount</th>

                    <th colspan="2" >CGST</th>

                    <th colspan="2" >SGST</th>

                    <th colspan="2" >IGST</th>

					<th rowspan="2" >Net Amount</th>
					
					<th rowspan="2" width="80">Action</th>

                </tr>

                <tr>

                    <th >RATE %</th>

                    <th >AMOUNT</th>

                    <th >RATE %</th>

                    <th >AMOUNT</th>

                    <th >RATE %</th>

                    <th >AMOUNT</th>

                </tr>

            </thead>

			<tbody>

				<tr id="abc">

                    <td>

						<input type="text" class="form-control input-sm" data-name="podate" data-role="text" id="podate" autocomplete='sameer' style="display:none;">

						<input type="text" class="form-control input-sm" data-name="itemcode" data-role="text" id="itemcode" autocomplete='sameer' style="display:none;">

						<input type="text" class="form-control input-sm" data-name="itemname" data-role="text" id="itemname" autocomplete='sameer' readonly>

                    </td>

                    <td>

                        <input type="text" class="form-control input-sm" data-name="qty" data-role="number" id="qty"
                            onkeyup="calc()" autocomplete='sameer' readonly>

                    </td>

                    <td>
                        
                        <input type="text" class="form-control input-sm" data-name="unit" data-role="text" id="unit" readonly>
                    
                    </td>

                    <td>
                        
                        <input type="text" class="form-control input-sm" data-name="rate" data-role="number" onkeyup="calc()" id="rate" >
                    
                    </td>

                    <td>
                        
                        <input type="text" class="form-control input-sm" data-name="descamt" data-role="number"
                            id="descamt" onkeyup="desc2()" autocomplete='sameer' >
                        
                    </td>

                    <td>
                        
                        <input type="text" class="form-control input-sm" data-name="amount" data-role="number" id="amount" readonly > 
                        
                    </td>

                    <td>
                        
                        <input type="text" class="form-control input-sm" data-name="cgstper" data-role="number"
                            id="cgstper" onkeyup="calc()" >
                    
                    </td>

                    <td>
                        
                        <input type="text" class="form-control input-sm" data-name="cgstamt" data-role="number"
                            id="cgstamt" readonly>
                        
                    </td>

                    <td>
                        
                        <input type="text" class="form-control input-sm" data-name="sgstper" data-role="number"
                            id="sgstper" onkeyup="calc()" >
                        
                    </td>


                    <td>
                        
                        <input type="text" class="form-control input-sm" data-name="sgstamt" data-role="number"
                            id="sgstamt" readonly>
                        
                    </td>

                    <td>
                        
                        <input type="text" class="form-control input-sm" data-name="igstper" data-role="number"
                            id="igstper" onkeyup="calc()" >
                        
                    </td>

                    <td>
                        
                        <input type="text" class="form-control input-sm" data-name="igstamt" data-role="number" id="igstamt" readonly>
                        
                    </td>

                    <td>
                        
                        <input type="text" class="form-control input-sm" data-name="netamount" data-role="number"
                            id="netamount" readonly>
                        
                    </td>

                    <td align="center">
                        
                        <button class="btn btn-sm btn-success" id="btn-add" onclick="add(this)" data-edit="">Add</button>
                        
                    </td>

				</tr>
				
			</tbody>

		</table>

	</div>

</div>

<?php 

	include('../footer.php');

?>

<script>


function pono() {

	let branchid = $('#branchid').val();

	if (branchid != 'Select') {

		$.ajax({

			type: "POST",

			data: 'branchid=' + branchid,

			url: 'pono.php',

			cache: false,

			success: function (res) {

				if (res.status == 'success') {

					let json = res.purchasejson;

					let str = '<option value="Select">Select</option>';

					for (let i in json) {

						str+="<option value="+json[i].purchaseid+">"+json[i].purchaseid+"</option>";

					} 

					$('#purchaseid').html(str); 

				}

			}

		});

		
	} else {

		$('#purchaseid').html("<option value='Select'>Select</option>"); 

	}

}

function itemDetails() {

	$('#table-order #xyz').remove();

	let purchaseid = $('#purchaseid').val(); 

	if (purchaseid != "Select") {

		$.ajax({

			type: "POST",

			data: 'purchaseid=' + purchaseid,

			url: 'itemdetails.php',

			cache: false,

			success: function (res) {

				if (res.status == 'success') {

					let arr = res.itemjson;

					let str  = '<tr id="xyz">';

					let couter = 1;

					  for (var i in arr) {

							str += '<td class="srno" style="display:none;">' + couter + '</td>';

							str += '<td class="podate" style="display:none;">' + arr[i].podate + '</td>';

							str += '<td class="itemcode" style="display:none;">' + arr[i].itemcode + '</td>';

							str += '<td class="itemname">' + arr[i].itemname + '</td>';

							str += '<td class="qty">' + arr[i].qty + '</td>';

							str += '<td class="unit">' + arr[i].unit + '</td>';

							str += '<td class="rate"> ' + arr[i].rate + ' </td>';

							str += '<td class="descamt"> ' + arr[i].descamt + '</td>';

							str += '<td class="amount"> ' + arr[i].amount + ' </td>';

							str += '<td class="cgstper"> ' + arr[i].cgstper + ' </td>';

							str += '<td class="cgstamt"> ' + arr[i].cgstamt + ' </td>';

							str += '<td class="sgstper"> ' + arr[i].sgstper + ' </td>';
							
							str += '<td class="sgstamt"> ' + arr[i].sgstamt + ' </td>';

							str += '<td class="igstper"> ' + arr[i].igstper + ' </td>';
							
							str += '<td class="igstamt"> ' + arr[i].igstamt + ' </td>';

							str += '<td class="netamount"> ' + arr[i].netamount + ' </td>';

							str += '<td align="center"><button class="btn btn-sm btn-success" onclick="editor(' + couter + ',this)">E</button></td>';

							str += '</tr>';
						  
						//fun_adder(couter, arr[i].podate, arr[i].itemcode, arr[i].itemname, arr[i].qty, arr[i].unit, arr[i].rate, arr[i].descamt, arr[i].amount,arr[i].cgstper,arr[i].cgstamt,arr[i].sgstper,arr[i].sgstamt,arr[i].igstper,arr[i].igstamt,arr[i].netamount);

						couter++;
					

					}

					$('#table-order > tbody > tr:last').after(str);


				}

			}

		});
		
	} else {

		$('#purchaseid').html("<option value='Select'>Select</option>"); 

	}
	
}




// add function start :
function add(e) {

	var chk       = $(e).attr('data-edit');

	var data      = checker('table-order');

	var podate    = data.podate;

	var itemname  = data.itemname;

	var itemcode  = data.itemcode;

	var qty       = data.qty;

	var unit      = data.unit;

	var rate      = data.rate;

	var descamt   = data.descamt;

	var amount    = data.amount;

	var cgstper   = data.cgstper;

	var cgstamt   = data.cgstamt;

	var sgstper   = data.sgstper;

	var sgstamt   = data.sgstamt;

	var igstper   = data.igstper;

	var igstamt   = data.igstamt;

	var netamount = data.netamount;

	var valid     = true;

	if (checker('table-order') != false) {

		valid = valid * true;

	} else {

		valid = valid * false;

	}

	
	if (valid) {

		var chk = $(e).attr('data-edit');

		console.log(chk);

		if (chk == '') {
			console.log('a');

			var len = $('#table-order .srno').length + 1;

			fun_adder(len,podate, itemcode,itemname,qty, unit, rate,descamt,amount,cgstper,cgstamt,sgstper,sgstamt,igstper,igstamt,netamount);

		} else {
			console.log('b');

			fun_adder_edit(chk,podate, itemcode,itemname,qty, unit, rate,descamt,amount,cgstper,cgstamt,sgstper,sgstamt,igstper,igstamt,netamount);

		}

		modals.clear('table-order');

		$('#btn-add').data('edit', '');

	}

}

// add function end :

// fun_adder function start :
function fun_adder(len,podate,itemcode,itemname,qty, unit, rate,descamt,amount,cgstper,cgstamt,sgstper,sgstamt,igstper,igstamt,netamount) {
		var str = '<tr>';

		str += '<td class="srno" style="display:none;">' + len + '</td>';

		str += '<td class="podate" style="display:none;">' + podate + '</td>';

		str += '<td class="itemcode" style="display:none;">' + itemcode + '</td>';

		str += '<td class="itemname">' + itemname + '</td>';

		str += '<td class="qty">' + qty + '</td>';

		str += '<td class="unit">' + unit + '</td>';

		str += '<td class="rate"> ' + rate + ' </td>';

		str += '<td class="descamt"> ' + descamt + '</td>';

		str += '<td class="amount"> ' + amount + ' </td>';

		str += '<td class="cgstper"> ' + cgstper + ' </td>';

		str += '<td class="cgstamt"> ' + cgstamt + ' </td>';

		str += '<td class="sgstper"> ' + sgstper + ' </td>';
		
		str += '<td class="sgstamt"> ' + sgstamt + ' </td>';

		str += '<td class="igstper"> ' + igstper + ' </td>';
		
		str += '<td class="igstamt"> ' + igstamt + ' </td>';

		str += '<td class="netamount"> ' + netamount + ' </td>';

		str += '<td align="center"><button class="btn btn-sm btn-success" onclick="editor(' + len + ',this)">E</button></td>';

		str += '</tr>';

		$('#table-order > tbody').append(str);
}
// fun_adder function end :

// fun_adder_edit function start :
function fun_adder_edit(len,podate,itemcode,itemname,qty, unit, rate,descamt,amount,cgstper,cgstamt,sgstper,sgstamt,igstper,igstamt,netamount) {

   var str = '<td align="center" class="srno" style="display:none;">' + len + '</td>';

  		str += '<td class="podate" style="display:none;">' + podate + '</td>';
	
  		str += '<td class="itemcode" style="display:none;">' + itemcode + '</td>';

		str += '<td class="itemname">' + itemname + '</td>';

		str += '<td class="qty">' + qty + '</td>';

		str += '<td class="unit">' + unit + '</td>';

		str += '<td class="rate"> ' + rate + ' </td>';

		str += '<td class="descamt"> ' + descamt + '</td>';

		str += '<td class="amount"> ' + amount + ' </td>';

		str += '<td class="cgstper"> ' + cgstper + ' </td>';

		str += '<td class="cgstamt"> ' + cgstamt + ' </td>';

		str += '<td class="sgstper"> ' + sgstper + ' </td>';
		
		str += '<td class="sgstamt"> ' + sgstamt + ' </td>';

		str += '<td class="igstper"> ' + igstper + ' </td>';
		
		str += '<td class="igstamt"> ' + igstamt + ' </td>';

		str += '<td class="netamount"> ' + netamount + ' </td>';

		str += '<td align="center"><button class="btn btn-sm btn-success" onclick="editor(' + len + ',this)">E</button></td>';

		$('#table-order .srno').each(function(){

			var srno = $(this).text().trim();

			if(srno==len){

				$(this).parent().html(str);

			}

		});
	  
}
// fun_adder_edit function end :

//editor function start :
function editor(srno,e){

	$('#podate').val($(e).parent().parent().find('.podate').text().trim());

	$('#itemcode').val($(e).parent().parent().find('.itemcode').text().trim());

	$('#itemname').val($(e).parent().parent().find('.itemname').text().trim());

	$('#qty').val($(e).parent().parent().find('.qty').text().trim());

	$('#unit').val($(e).parent().parent().find('.unit').text().trim());

	$('#rate').val($(e).parent().parent().find('.rate').text().trim());

	$('#descamt').val($(e).parent().parent().find('.descamt').text().trim());

	$('#amount').val($(e).parent().parent().find('.amount').text().trim());

	$('#cgstper').val($(e).parent().parent().find('.cgstper').text().trim());

	$('#cgstamt').val($(e).parent().parent().find('.cgstamt').text().trim());

	$('#sgstper').val($(e).parent().parent().find('.sgstper').text().trim());

	$('#sgstamt').val($(e).parent().parent().find('.sgstamt').text().trim());

	$('#igstper').val($(e).parent().parent().find('.igstper').text().trim());

	$('#igstamt').val($(e).parent().parent().find('.igstamt').text().trim());

	$('#netamount').val($(e).parent().parent().find('.netamount').text().trim());

	$('#btn-add').attr('data-edit',srno);
	
}
//editor function end :


function calc(){

	var rate=parseFloat($('#rate').val());
    if(rate==0){

        var qty=parseFloat($('#qty').val());
        var descamt=parseFloat($('#descamt').val());
        var cgstper=parseFloat($('#cgstper').val());
        var sgstper=parseFloat($('#sgstper').val());
        var igstper=parseFloat($('#igstper').val());

        var amount=rate*qty;
        var afterdiscamt=amount-descamt;
        var cgstamt=cgstper*afterdiscamt/100;
        var sgstamt=sgstper*afterdiscamt/100;
        var igstamt=igstper*afterdiscamt/100;
        var netamt=afterdiscamt+cgstamt+sgstamt+igstamt;

        $('#amount').val(0);
        $('#cgstamt').val(0);
        $('#sgstamt').val(0);
        $('#igstamt').val(0);
        $('#netamount').val(0)
    }else{

    var qty=parseFloat($('#qty').val());
    var descamt=parseFloat($('#descamt').val());
    var cgstper=parseFloat($('#cgstper').val());
    var sgstper=parseFloat($('#sgstper').val());
    var igstper=parseFloat($('#igstper').val());



    var amount=rate*qty;
    var afterdiscamt=amount-descamt;
    var cgstamt=cgstper*afterdiscamt/100;
    var sgstamt=sgstper*afterdiscamt/100;
    var igstamt=igstper*afterdiscamt/100;
    var netamt=afterdiscamt+cgstamt+sgstamt+igstamt;

    $('#amount').val(afterdiscamt.round(2));
    $('#cgstamt').val(cgstamt.round(2));
    $('#sgstamt').val(sgstamt.round(2));
    $('#igstamt').val(igstamt.round(2));
    $('#netamount').val(netamt.round(2));
    }
 }


 function desc2(){
     //var descper=$('#descper').val();
    // var desamt=$('#desamt').val();
    //   var qty=parseFloat($('#qty').val());
	//  var rate=parseFloat($('#rate').val());
	//  var descamt=parseFloat($('#descamt').val());

	//  var amount=(qty*rate)-descamt;
	
    //var amount=parseFloat($('#amount').val());

    //     descper=(parseFloat(desamt)/amount)*100;
    //     $('#descper').val(parseFloat(descper).round(2));
    //$('#amount').val(parseFloat(amount).round(2));

        calc();
}


var json2 = [];
var billid = $('#billid').val();
if (billid != "") {
	edit();
}


function insert() {

	var formData = new FormData();

	var data = modals.tabletdbyclass('table-order');

	data.shift();

	var valid = true;
	

	if (data != '') {

		valid = valid * true;

		$('#table-order').css('border', '2px solid green');

	} else {

		valid = valid * false;

		$('#table-order').css('border', '2px solid red');

	}

	if (valid) {

        var datastr = JSON.stringify(data);
        formData.append('pobillimglink', $('#pobillimglink')[0].files[0]);
        formData.append('data', datastr);
        formData.append('pobranchid',$('#branchid').val());
		formData.append('purchasedate',$('#purchasedate').val());
        formData.append('purchaseid',$('#purchaseid').val());
        formData.append('purchasebillno',$('#purchasebillno').val());



		$.ajax({

			type: "POST",

			data : formData,

			// data: {
			// 	pobranchid: $('#branchid').val(),
			// 	billdate: $('#billdate').val(),
			// 	purchaseid: $('#purchaseid').val(),
			// 	data: JSON.stringify(data)
			// },

			url: 'insert.php',

			cache: false,

            processData: false,

            contentType: false,

			success: function (res) {

				if (res.status == 'success') {


					swal({

						type: 'success',
						title: 'Purchase Bill Created',
						showConfirmButton: false,
						timer: 3000

					});

					setTimeout(function () {

						window.location = "../purchase-bill.php";

					}, 3000);

				}

			}

		});

	}

}



function edit() {

	$('#btn-insert').attr('onclick', 'update()');

	$('#btn-insert').text('Update');

	$.ajax({

		type: "POST",

		data: 'billid=' + $('#billid').val(),

		url: 'select.php',

		cache: false,

		success: function (res) {

			if (res.status == 'success') {

				var json = res.json;
				json2    = json;

				$("#branchid").val(res.branchid).trigger('change');

				$("#purchasedate").val(res.purchasedate);
				$("#purchasebillno").val(res.purchasebillno);

				if (res.pobillimglink != '') {
					$('#showImg').show();
					$('#showImg').attr('href',res.pobillimglink);

				}
				//$("#purchasedate").val(res.billdate);


				setTimeout(function(){ 

					$("#purchaseid").html('<option value="'+res.purchaseid+'">'+res.purchaseid+'</option>');


				}, 500);

				$("#purchaseid").select2({disabled:"readonly"});
				$("#branchid").select2({disabled:"readonly"});


				var arr   = res.json;

				var couter = 1;

				for (var i in arr) {

					fun_adder(couter, arr[i].podate, arr[i].itemcode, arr[i].itemname, arr[i].qty, arr[i].unit, arr[i].rate, arr[i].descamt, arr[i].amount,arr[i].cgstper,arr[i].cgstamt,arr[i].sgstper,arr[i].sgstamt,arr[i].igstper,arr[i].igstamt,arr[i].netamount);

					couter++;

				}

			}

		}

	});

}

function update() {

	var formData = new FormData();

	var data = modals.tabletdbyclass('table-order');

	data.shift();

	var datastr = JSON.stringify(data);
	formData.append('pobillimglink', $('#pobillimglink')[0].files[0]);
	formData.append('data', datastr);
	formData.append('pobranchid',$('#branchid').val());
	formData.append('purchasedate',$('#purchasedate').val());
	formData.append('purchaseid',$('#purchaseid').val());
	formData.append('purchasebillno',$('#purchasebillno').val());
	formData.append('billid',$('#billid').val());


	$.ajax({

		type: "POST",

		// data: {
		// 	pobranchid: $('#branchid').val(),
		// 	billdate: $('#billdate').val(),
		// 	purchaseid: $('#purchaseid').val(),
		// 	billid: $('#billid').val(),
		// 	data: JSON.stringify(data)
		// },

		data : formData,

		url: 'update.php',

		cache: false,

        processData: false,

        contentType: false,

		success: function (res) {

			if (res.status == 'success') {

				swal({

					type: 'success',
					title: 'Purchase Bill Updated',
					showConfirmButton: false,
					timer: 3000

				});

				setTimeout(function () {

					window.location = "../purchase-bill.php";

				}, 3000);

			}
			
		}

	});
	
}

// var branchid = $('#inp-session-branchid').val();
// $('#branchid').val(branchid);

$(document).ready(function () {
	$('.select-js1').select2({
		width: '100%'
	});
	$('.select').attr('style', 'width:100%!important;');

});

var role=$('#accessrole').val();
if(role=='admin'){
	$('#branchid').prop('disabled',false);
}
</script>