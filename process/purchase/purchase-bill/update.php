<?php

    $base='../../../';

    include($base.'_in/connect.php');

    header('content-type: application/json; charset=utf-8');

    header("access-control-allow-origin: *");
    
    if(isset($_POST['pobranchid']) && isset($_POST['purchasedate']) && isset($_POST['purchaseid']) && isset($_POST['purchasebillno']) && isset($_POST['billid']) ) {

        $con=_connect();

        if (session_status()==PHP_SESSION_NONE) { session_start(); }
        
        $created_by = $_SESSION['employeeid'];
     
        $pobranchid = _clean($con,$_POST["pobranchid"]);

        $purchasedate = _clean($con,$_POST["purchasedate"]);

        $purchaseid = _clean($con,$_POST["purchaseid"]);

        $billid     = _clean($con,$_POST["billid"]);

        $purchasebillno  = _clean($con,$_POST["purchasebillno"]);


        if ($_FILES['pobillimglink']['size'] != '') {

            $billids          = 'Bill-'.$billid;

            $info           = pathinfo($_FILES['pobillimglink']['name']);
                    
            $ext            = $info['extension']; 
                    
            $newname        = $billids.".".$ext;

            $target         = '../purchasebill/'.$newname;
                    
            $pobillimglink  = '/process/purchase/purchasebill/'.$newname;
                    
            if(file_exists($target)) {
                            
                unlink($target); //remove the file
                        
            }

            move_uploaded_file($_FILES['pobillimglink']['tmp_name'],$target);
            
        } else {

            $pobillimglink  = mysqli_fetch_assoc(mysqli_query($con,"SELECT pobillimglink x FROM purchasebill WHERE billid = '$billid'"))['x'];

        }
        
        $data       = json_decode($_POST["data"]);

        //print_r($data);

        foreach ($data as $i) {
            
            $podate    = get_object_vars($i)['podate'];

            $itemcode  = get_object_vars($i)['itemcode'];

            $itemname  = get_object_vars($i)['itemname'];

            $qty       = get_object_vars($i)['qty'];

            $unit      = get_object_vars($i)['unit'];

            $rate      = get_object_vars($i)['rate'];

            $descamt   = get_object_vars($i)['descamt'];

            $amount    = get_object_vars($i)['amount'];

            $cgstper   = get_object_vars($i)['cgstper'];

            $cgstamt   = get_object_vars($i)['cgstamt'];

            $sgstper   = get_object_vars($i)['sgstper'];

            $sgstamt   = get_object_vars($i)['sgstamt'];

            $igstper   =  get_object_vars($i)['igstper'];

            $igstper   = get_object_vars($i)['igstper'];

            $netamount = get_object_vars($i)['netamount'];

            $update = mysqli_query($con,"UPDATE  purchasebill SET podate='$podate', itemname='$itemname', itemcode='$itemcode', qty='$qty', unit='$unit', rate='$rate', descper='0', descamt='$descamt',amount='$amount', cgstper='$cgstper', sgstper='$sgstper', igstper='$igstper', cgstamt='$cgstamt', sgstamt='$sgstamt', igstamt='$igstamt', netamount='$netamount', created_by='$created_by', created_time='$CURRENT_MILLIS', billdate='$billdate' , purchasebillno = '$purchasebillno',purchasedate = '$purchasedate',pobillimglink='$pobillimglink' WHERE billid='$billid' AND itemcode = '$itemcode' ");

            $purchasebillid = mysqli_fetch_assoc(mysqli_query($con,"SELECT id x FROM purchasebill WHERE billid = '$billid' AND itemcode='$itemcode' "))['x'];

            mysqli_query($con,"UPDATE purchase SET purchasebillno='$purchasebillno', purchasebillid='$purchasebillid', purchasebill_by='$created_by', purchasebill_time='$CURRENT_MILLIS' WHERE purchaseid='$purchaseid' AND itemcode='$itemcode'");    


        }

        if ($update) {

            //mysqli_query($con, "UPDATE gateentry SET purchasebill_by = '$created_by',purchasebill_time = '$CURRENT_MILLIS' WHERE purchaseid = '$purchaseid' ");
            
            echo '{"status":"success"}';


        } else {

            echo '{"status":"falid"}';

        }

     
            _close($con);
    } else {

        echo '{"status":"falid1"}';

    }
?>