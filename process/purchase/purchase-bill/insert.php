<?php

    $base='../../../';

    include($base.'_in/connect.php');

    header('content-type: application/json; charset=utf-8');

    header("access-control-allow-origin: *");
    
    if(isset($_POST['pobranchid']) && isset($_POST['purchasedate']) && isset($_POST['purchaseid']) && isset($_POST['purchasebillno'])) {

        $con=_connect();

        if (session_status()==PHP_SESSION_NONE) { session_start(); }
        
        $created_by      = $_SESSION['employeeid'];
     
        $pobranchid      = _clean($con,$_POST["pobranchid"]);

        $purchasedate    = _clean($con,$_POST["purchasedate"]);

        $purchaseid      = _clean($con,$_POST["purchaseid"]);

        $purchasebillno  = _clean($con,$_POST["purchasebillno"]);

        $purchaseDetails = mysqli_fetch_assoc(mysqli_query($con, "SELECT supplierid,poforbranch FROM purchase WHERE purchaseid ='$purchaseid'"));

        $supplierid      = $purchaseDetails['supplierid'];

        $poforbranch     = $purchaseDetails['poforbranch'];

        $billid          = mysqli_fetch_assoc(mysqli_query($con,"SELECT count(id) x FROM `purchasebill`"))['x'];
        
        $billid++;
        
        $billid          = 'Bill-'.$billid;

        $info           = pathinfo($_FILES['pobillimglink']['name']);
                
        $ext            = $info['extension']; 
                
        $newname        = $billid.".".$ext;

        $target         = '../purchasebill/'.$newname;
                
        $pobillimglink  = '/process/purchase/purchasebill/'.$newname;
                
        if(file_exists($target)) {
                        
            unlink($target); //remove the file
                    
        }

        move_uploaded_file($_FILES['pobillimglink']['tmp_name'],$target);


        $data            = json_decode($_POST["data"]);

        foreach ($data as $i) {
            
            $podate    = get_object_vars($i)['podate'];

            $itemcode  = get_object_vars($i)['itemcode'];

            $itemname  = get_object_vars($i)['itemname'];

            $qty       = get_object_vars($i)['qty'];

            $unit      = get_object_vars($i)['unit'];

            $rate      = get_object_vars($i)['rate'];

            $descamt   = get_object_vars($i)['descamt'];

            $amount    = get_object_vars($i)['amount'];

            $cgstper   = get_object_vars($i)['cgstper'];

            $cgstamt   = get_object_vars($i)['cgstamt'];

            $sgstper   = get_object_vars($i)['sgstper'];

            $sgstper   = get_object_vars($i)['sgstper'];

            $igstper   =  get_object_vars($i)['igstper'];

            $igstper   = get_object_vars($i)['igstper'];

            $netamount = get_object_vars($i)['netamount'];


            $insert = mysqli_query($con,"INSERT INTO purchasebill (branchid, purchaseid, podate, supplierid, itemname, itemcode, qty, unit, rate, descper, descamt, amount, cgstper, sgstper, igstper, cgstamt, sgstamt, igstamt, netamount, created_by, created_time, poforbranch, billdate,billid,purchasebillno,purchasedate,pobillimglink) VALUES 
            ('$pobranchid' , '$purchaseid','$podate','$supplierid','$itemname','$itemcode','$qty','$unit','$rate','0','$descamt','$amount','$cgstper','$sgstper','$igstper','$cgstamt','$sgstamt','$igstamt','$netamount','$created_by','$CURRENT_MILLIS','$poforbranch','$billdate','$billid','$purchasebillno','$purchasedate','$pobillimglink')");
            
            $purchasebillid = mysqli_fetch_assoc(mysqli_query($con,"SELECT max(id) x FROM purchasebill"))['x'];
                   
            mysqli_query($con,"UPDATE purchase SET purchasebillno='$purchasebillno', purchasebillid='$purchasebillid', purchasebill_by='$created_by', purchasebill_time='$CURRENT_MILLIS' WHERE purchaseid='$purchaseid' AND itemcode='$itemcode'");    


        }

        if ($insert) {

            mysqli_query($con, "UPDATE gateentry SET purchasebill_by = '$created_by',purchasebill_time = '$CURRENT_MILLIS' WHERE purchaseid = '$purchaseid' ");
            
            echo '{"status":"success"}';


        } else {

            echo '{"status":"falid"}';

        }

     
            _close($con);
    } else {

        echo '{"status":"falid1"}';

    }
?>