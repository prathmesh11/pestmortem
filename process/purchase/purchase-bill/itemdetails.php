<?php

    $base='../../../';

    include($base.'_in/connect.php');

    header('content-type: application/json; charset=utf-8');

    header("access-control-allow-origin: *");
    
    if(isset($_POST['purchaseid'])) {

        $con=_connect();
     
        $purchaseid =_clean($con,$_POST["purchaseid"]);

        $results=mysqli_query($con,"SELECT  DISTINCTROW itemcode FROM gateentry WHERE purchaseid='$purchaseid' AND purchasebill_time = 0 ");

        while ($row = mysqli_fetch_assoc($results)) {

            $itemcode     = $row['itemcode'];
            
            $result       =  mysqli_fetch_assoc(mysqli_query($con,"SELECT DISTINCT podate, supplierid, itemname, unit, rate, descper, descamt, amount, cgstper, sgstper, igstper, cgstamt, sgstamt, igstamt, netamount , (SELECT SUM(qty) FROM gateentry WHERE purchaseid='$purchaseid' AND purchasebill_time = 0 AND itemcode='$itemcode') sumqty FROM `gateentry` WHERE purchaseid='$purchaseid' AND purchasebill_time = 0 AND itemcode='$itemcode' "));

            $qty          =  $result ['sumqty'];

            $supplierid   = $result['supplierid'];

            $supplierrate = mysqli_fetch_assoc(mysqli_query($con,"SELECT rate,discount_rate FROM msirate WHERE itemcode = '$itemcode' AND scode = '$supplierid' "));

            $unit2value   = mysqli_fetch_assoc(mysqli_query($con,"SELECT unit2value a FROM stockmaster WHERE stockid = '$itemcode' "))['a'];

            $discount     = ($qty * $unit2value) * $supplierrate['discount_rate'];

            $amount       = ($qty * $supplierrate['rate']) - $discount;

            $json.= ',{"podate":"'.$result['podate'].'","supplierid":"'.$result['supplierid'].'","itemname":"'.$result['itemname'].'","itemcode":"'.$row['itemcode'].'","qty":"'.$qty.'","unit":"'.$result['unit'].'","rate":"'.$supplierrate['rate'].'","descper":"'.$result['descper'].'","descamt":"'.$discount.'","amount":"'.$amount.'","cgstper":"'.$result['cgstper'].'","sgstper":"'.$result['sgstper'].'","igstper":"'.$result['igstper'].'","cgstamt":"'.$result['cgstamt'].'","sgstamt":"'.$result['sgstamt'].'","igstamt":"'.$result['igstamt'].'","netamount":"'.$amount.'"}';
            
        }

        $str  = substr($json,1);

        $str  = '['.$str.']';


        if($results){

            echo '{"status":"success","itemjson":'.$str.' }';
        
        } else {

            echo '{"status":"falid2"}';

        }

        _close($con);

    } else {

        echo '{"status":"falid1"}';

    }
?>