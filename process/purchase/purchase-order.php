<?php 
  $base='../../';
  $navenq1='background:#1B1464;';
  include('header.php');

  if (!in_array('Purchase Order', $arrysession)) {
   
    echo "<script>window.location.href='/process/dashboard.php';</script>";
   
    exit;

}

  if (session_status() == PHP_SESSION_NONE) { session_start(); }
  $sessionby = $_SESSION['employeeid'];
  $branchid = $_SESSION['branchid'];

?>
<style>
  .table-list td,
    .table-list th {
        border: 1px solid #ddd;
        padding: 1px !important;
        font-size: 13px;
    }
    .table-list td{
    padding-top: 2px !important;
      margin:0;

    }

    .table-list tr:nth-child(even) {
        background-color: #f2f2f2;
    }

    .table-list th {
        padding-top: 5px;
        padding-bottom: 5px;
        text-align: center;
        background-color: #16a085;
        color: white;
    }
</style>
  <?php   
   $admin_branch='';
  if($_SESSION['role']=='admin'){
      $branchid_admin='';
      echo '<select onchange="branchadmin(this)" class="form-control input-sm" style="margin:2px;">';
      if($_COOKIE['branchid']!=''){
        $branchid_admin=$_COOKIE['branchid'];
        $branchname1=mysqli_fetch_assoc(mysqli_query($con,"SELECT branchname x FROM branchmaster WHERE branchid='$branchid_admin'"))['x'];
        echo '<option value="'.$branchid_admin.'">'.$branchname1.'</option>';
      }
        echo '<option value="allbranch">All Branches</option>';
      $select1=mysqli_query($con,"SELECT branchid, branchname FROM branchmaster WHERE branchid<>'$branchid_admin'");
      while($rows = mysqli_fetch_assoc($select1)){
        echo '<option value="'.$rows['branchid'].'">'.$rows['branchname'].'</option>';
      }
    
  echo '</select>';
    }
?>
<img class="hidden" id=imageid src="../../../../img/PestMortemLogo.jpg">
<div class="table-ui container-fluid">
    <div class="tr row">
        <div class="col-sm-1 th">Order No.</div>
        <div class="col-sm-1 th">Order Date</div>
        <div class="col-sm-3 th">Supplier Info</div>
        <div class="col-sm-6 th">Order</div>
        <div class="col-sm-1 th">Action</div>
        </div>
        <?php
          
          if($branchid_admin==''){
            $result=mysqli_query($con,"SELECT DISTINCTROW purchaseid,podate,poforbranch FROM `purchase` WHERE  confirm_time=0 AND gateentry_time=0 AND close_time=0 AND purchasebill_time=0 ORDER BY id DESC");
          }else{
            $result=mysqli_query($con,"SELECT DISTINCTROW purchaseid,podate,poforbranch FROM `purchase` WHERE  confirm_time=0 AND gateentry_time=0 AND close_time=0 AND purchasebill_time=0 AND poforbranch='$branchid_admin' ORDER BY id DESC");
          }
          
         //  $result=mysqli_query($con,"SELECT DISTINCTROW purchaseid,podate,poforbranch FROM `purchase` WHERE  confirm_time=0 AND gateentry_time=0 AND close_time=0 AND purchasebill_time=0 ORDER BY id DESC");
          while($rows=mysqli_fetch_assoc($result)){
                $purchaseid=$rows['purchaseid'];
                $podate=$rows['podate'];
                $poforbranch=$rows['poforbranch'];
                $branchname=mysqli_fetch_assoc(mysqli_query($con, "SELECT branchname x FROM `branchmaster` WHERE branchid='$poforbranch'"))['x'];
                // $branchadd=mysqli_fetch_assoc(mysqli_query($con, "SELECT branchjson x FROM `branchmaster` WHERE branchid='$poforbranch'"))['x'];
                // $address=get_object_vars(json_decode($branchadd))['address'];
                // echo $address;
                $supplierid=mysqli_fetch_assoc(mysqli_query($con,"SELECT supplierid x FROM `purchase` WHERE purchaseid='$purchaseid' ORDER BY id DESC LIMIT 1"))['x'];      
                $supplierjson=get_object_vars(json_decode(mysqli_fetch_assoc(mysqli_query($con,"SELECT supplierjson x FROM suppliermaster WHERE supplierid='$supplierid'"))['x']));
               ?>
        <div class="tr row">
        <input type="hidden" id="purchaseid" value='<?php echo $purchaseid; ?>' />
        <input type="hidden" id="poforbranch" value='<?php echo $poforbranch; ?>' />
        <div class="col-sm-1 td" style="word-wrap:break-word;"><?php echo $purchaseid; ?><br><br><?php echo $branchname; ?></div>
        <div class="col-sm-1 td" style="word-wrap:break-word;"><?php echo date("d-m-Y",strtotime($podate)); ?></div>
        <div class="col-sm-3 td" style="word-wrap:break-word;">
        <?php
            $contactjson=json_decode(mysqli_fetch_assoc(mysqli_query($con,"SELECT contactjson x FROM suppliermaster WHERE supplierid='$supplierid'"))['x']);
            $sbranch=mysqli_fetch_assoc(mysqli_query($con,"SELECT sbranch x FROM `purchase` WHERE purchaseid='$purchaseid' ORDER BY id DESC LIMIT 1"))['x'];      
            $user_email='';
            foreach($contactjson as $i){
        //if(get_object_vars($i)['sbranch']==$sbranch){
         echo 'Company : '.$supplierjson['suppliername']; 
         echo '<br> Branch: '.get_object_vars($i)['sbranch']; 
         echo '<br> Address : '.get_object_vars($i)['sbranchaddress'];
         echo '<br> Mobile : '.get_object_vars($i)['mobile']; 
         echo '<br> email : '.get_object_vars($i)['email']; 
         $user_email=get_object_vars($i)['email'];
        break;
        //}
     }  
        ?>
        </div>
        <div class="col-sm-6 td" style="word-wrap:break-word;">
        <?php 
       $result1=mysqli_query($con,"SELECT * FROM `purchase` WHERE purchaseid='$purchaseid'");
       echo '<table class="table table-list form-group">';
       echo '<thead>';
       echo '<tr>';
       echo '<th width="300">Item Name</th>';
       echo '<th>Qty</th>';
       echo '<th width="80">Unit</th>';
       echo '<th style="display:none;">Rate</th>';
       echo '<th style="display:none;">DISC.</th>';
       echo '<th style="display:none;">Amount</th>';
       echo '<th style="display:none;">CGST</th>';
       echo '<th style="display:none;">SGST</th>';
       echo '<th style="display:none;">IGST</th>';
       echo '<th style="display:none;">Net Amount</th>';
       echo '</tr>';
       echo '</thead>';
       echo '<tbody>';
       $total=0;
       while($rowss=mysqli_fetch_assoc($result1)){
                echo '<tr>';
                echo '<td>'.$rowss['itemname'].'</td>';
                echo '<td>'.$rowss['qty'].'</td>';
                echo '<td>'.$rowss['unit'].'</td>';
                echo '<td style="display:none;">'.$rowss['rate'].'</td>';
                echo '<td style="display:none;">'.$rowss['descamt'].'</td>';
                echo '<td style="display:none;">'.$rowss['amount'].'</td>';
                echo '<td style="display:none;">'.$rowss['cgstamt'].'</td>';
                echo '<td style="display:none;">'.$rowss['sgstamt'].'</td>';
                echo '<td style="display:none;">'.$rowss['igstamt'].'</td>';
                echo '<td style="display:none;">'.$rowss['netamount'].'</td>';
                $total+=$rowss['netamount'];
                echo '</tr>';
        }
                echo '</tbody>';
                echo '</table>';
                // echo 'Total : '.$total;
       ?>
        </div>
        <div class="col-sm-1 td" style="word-wrap:break-word;"><button class="btn btn-sm btn-block btn-success" data-link1="confirm_by" data-link2="confirm_time" data-purchaseid='<?php echo $purchaseid; ?>' onclick="confirms(this)">Confirm</button>
        <a class="btn btn-sm btn-block btn-primary"  onclick="this.href='purchase-order/index.php?purchaseid=' + '<?php echo $purchaseid; ?>'; return true">Edit</a>
        <button class="btn btn-sm btn-block btn-warning" data-purchaseid='<?php echo $purchaseid; ?>' onclick="printer(this)">Print</button>
        <a class="btn btn-sm btn-block btn-primary" onclick="TriggerOutlook(this)" data-emailId = "<?php echo $user_email; ?>" data-purchaseid="<?php echo $purchaseid; ?>">Email</a>
  
        </div>
        </div>
                <?php
            }
        ?>
</div>

<a class="btn" href="purchase.php" style="font-size:25px;border-radius:50px;width:60px;height:60px;position: fixed;bottom: 40px;right: 40px;background:#eb2f06;color:#fff;" >+</a>

<?php 
include('footer.php');
?>
<script>

function TriggerOutlook(e) {

  var email = $(e).attr('data-emailId');
  var purchaseid = $(e).attr('data-purchaseid');

  var valid = true;

  var regemail = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;

  if (!regemail.test(email)) {

    valid = valid * false;

    alert('Pls Check Customer Email id');

  } else {

    valid = valid * true;

  }


  //var body = href = "http://pestmorten.local/process/purchase/purchaseemail.php?purchaseid=" + btoa(purchaseid);

  // var body = href = "http://pestmorten.local/process/invoice/email.php?invoice_bill=" + btoa(invoice_bill)+''+branchId;
  var body = href = "http://pestmortem.co.in/process/purchase/purchaseemail.php?purchaseid=" + btoa(purchaseid);

  var subject = "Pest Mortem (India) Pvt. Ltd.";
  var TO = email;

  if (valid) {

    window.location.href = "mailto: " + TO + "?body=Pls Click Link And Download This Purchase Order To Given Below %0D%0A%0D%0A" + body + "&subject=" + subject;

    window.location.reload();

  }

}


function getBase64Image(img) {
  var canvas = document.createElement("canvas");
  canvas.width = img.width;
  canvas.height = img.height;
  var ctx = canvas.getContext("2d");
  ctx.drawImage(img, 0, 0);
  var dataURL = canvas.toDataURL("image/png");
  return dataURL.replace('/^data:image\/(png|jpg);base64,/', "");
}
function withDecimal(s) {
  n=Math.round(s * 100) / 100;
    var nums = n.toString().split('.')
    var whole = convertNumberToWords(nums[0])
    if (nums.length == 2) {
      var res1 = nums[1].slice(0, 1);
      var res2 = nums[1].slice(1, 2);
        var fraction = convertNumberToWords(res1)+''+convertNumberToWords(res2)
        return whole+'point '+fraction;
    } else {
        return whole;
    }
}
function convertNumberToWords(amount) {
    var words = new Array();
    words[0] = '';
    words[1] = 'One';
    words[2] = 'Two';
    words[3] = 'Three';
    words[4] = 'Four';
    words[5] = 'Five';
    words[6] = 'Six';
    words[7] = 'Seven';
    words[8] = 'Eight';
    words[9] = 'Nine';
    words[10] = 'Ten';
    words[11] = 'Eleven';
    words[12] = 'Twelve';
    words[13] = 'Thirteen';
    words[14] = 'Fourteen';
    words[15] = 'Fifteen';
    words[16] = 'Sixteen';
    words[17] = 'Seventeen';
    words[18] = 'Eighteen';
    words[19] = 'Nineteen';
    words[20] = 'Twenty';
    words[30] = 'Thirty';
    words[40] = 'Forty';
    words[50] = 'Fifty';
    words[60] = 'Sixty';
    words[70] = 'Seventy';
    words[80] = 'Eighty';
    words[90] = 'Ninety';
    amount = amount.toString();
    var atemp = amount.split(".");
    var number = atemp[0].split(",").join("");
    var n_length = number.length;
    var words_string = "";
    if (n_length <= 9) {
        var n_array = new Array(0, 0, 0, 0, 0, 0, 0, 0, 0);
        var received_n_array = new Array();
        for (var i = 0; i < n_length; i++) {
            received_n_array[i] = number.substr(i, 1);
        }
        for (var i = 9 - n_length, j = 0; i < 9; i++, j++) {
            n_array[i] = received_n_array[j];
        }
        for (var i = 0, j = 1; i < 9; i++, j++) {
            if (i == 0 || i == 2 || i == 4 || i == 7) {
                if (n_array[i] == 1) {
                    n_array[j] = 10 + parseInt(n_array[j]);
                    n_array[i] = 0;
                }
            }
        }
        value = "";
        for (var i = 0; i < 9; i++) {
            if (i == 0 || i == 2 || i == 4 || i == 7) {
                value = n_array[i] * 10;
            } else {
                value = n_array[i];
            }
            if (value != 0) {
                words_string += words[value] + " ";
            }
            if ((i == 1 && value != 0) || (i == 0 && value != 0 && n_array[i + 1] == 0)) {
                words_string += "Crores ";
            }
            if ((i == 3 && value != 0) || (i == 2 && value != 0 && n_array[i + 1] == 0)) {
                words_string += "Lakhs ";
            }
            if ((i == 5 && value != 0) || (i == 4 && value != 0 && n_array[i + 1] == 0)) {
                words_string += "Thousand ";
            }
            if (i == 6 && value != 0 && (n_array[i + 1] != 0 && n_array[i + 2] != 0)) {
                words_string += "Hundred and ";
            } else if (i == 6 && value != 0) {
                words_string += "Hundred ";
            }
        }
        words_string = words_string.split("  ").join(" ");
    }
    return words_string;
}

var purchaseid=$('#purchaseid').val();
  console.log(purchaseid);
  

function printer(e) {
    var purchaseid = $(e).data('purchaseid');
        $.ajax({
            type: "POST",
            data: 'purchaseid=' + purchaseid,
            url: 'api/purchaseorderbill.php',
            cache: false,
    success: function (res) {
      if (res.status == 'success') {
        var content=[];
        var json=res.json[0];
        var servicejson=json.servicejson;
        var count=0;
        var gstamt=0;
        var totalamt=0;
        var discount=0;
        var netamt=0;
        var border1=  [true,false,false,false];
        var totalamtword=0;
        var base64 = getBase64Image(document.getElementById("imageid"));
  content.push(
    {text:'PURCHASE ORDER\n\n',fontSize: 14,bold:1,alignment: "center"},
	    {
      table:{
      margin: [10, 0],
      widths: [75, 170,'*', '*'],
      body:[
        [
          {
          rowSpan: 2,
          stack: [{image:base64,
                    width: 75,
                    height:50,
                    margin:[0,20]
                    },],
          },
          {
          rowSpan: 2, 
          stack: [{
            text: [
            {text:'Pest Mortem (l) Pvt Ltd\n',fontSize: 10,bold:1,},
            {text:json.branchname+'\n',fontSize: 10},
            {text:json.address+'\n',fontSize: 10},
            {text:'GST No: '+json.gstnumber+' \n',fontSize: 10},
            {text:'State: '+json.statename+' \n',fontSize: 10},
            {text:'Email:'+json.email+' \n',fontSize: 10},
            {text:'Phone No:'+json.phone+'\n',fontSize: 10},
            ]},
            ],
          },
        {
        border:[1,1,0,0],
        text: [
          {text:'Po. No.\n',fontSize: 10},
          {text:json.purchaseid+'\n',fontSize: 10,bold:1},
          ]
        },
        {
        border:[1,1,1,0],
        text: [
            {text:'Dated\n',fontSize: 10},
            {text:json.podate+'\n',fontSize: 10,bold:1},
            ]
        }
    ],
    [
      {}, {},
      {
        border:[0,0,0,0],
        text: [
          // {text:'Supplier\'s Ref./Order No.\n',fontSize: 10},
          // {text:json.gstnumber+'\n',fontSize: 10,bold:1},
          {text:'\nDespatch through\n',fontSize: 10},
          {text:'',fontSize: 10},
          ]
        }, 
        { 
        border:[1,0,1,0],
        text: [
          {text:'Mode/term of Payment\n',fontSize: 10},
          {text:json.creditdays+' Days\n',fontSize: 10,bold:1},
          {text:'\nOther Reference(s)\n',fontSize: 10},
          {text:'',fontSize: 10,bold:1},
          ]
        }, 
      ],
    [
      {
      rowSpan: 2,
      colSpan: 2, 
          stack: [{
            text: [
            {text:'Supplier\'s\n',fontSize: 10},
            {text:json.suppliername+'\n',fontSize: 10,bold:1,},
            {text:json.sbranchaddress+'\n',fontSize: 10},
            {text:'Mumbai \n',fontSize: 10},
            {text:'GSTIN/UIN        : '+json.sbranchgstno+' \n',fontSize: 10},
            {text:'State Name       : '+json.statename+'\n',fontSize: 10}
            ]},
          ],    
        },
        {},
        {   
        rowSpan: 2,
        border:[1,1,1,1],
          text: [
              {text:'\nTerms of Delivery\n',fontSize: 10},
              {text:'\n',fontSize: 10,bold:1},
              ]
        }, 
        {
          rowSpan: 2,
          stack: [{
            text: [
              {text:'\nDestination\n',fontSize: 10,bold:1},
              {text:json.poforbadd+'\n',fontSize: 10},
            ]},
          ], 
          // text: [
          //   {text:'\nDestination\n',fontSize: 10,bold:1},
          //   {text:json.poforbadd+'\n',fontSize: 10},
          //   ]
          }
    ],
      [{}, {},{}, {}],
    ]
    },layout: {
        hLineColor: function (i, node) {
          return (i === 0 || i === node.table.body.length) ? 'gray' : 'gray';
        },
        vLineColor: function (i, node) {
          return (i === 0 || i === node.table.widths.length) ? 'gray' : 'gray';
        },
      }
    },
),

content.push(
  { 
    fontSize: 10,
    unbrackable:1,
    table: { 
        // widths: [10,'*','auto','auto','auto','auto','auto','auto','auto'],
        widths: [10,'*','auto','auto'],

        heights:[20],  
        body: [ 
        [
        {text:'#',color:'white', border: [1,1,1,1],alignment:'center'},
        {text:'Description of Goods',color:'white', border: [1,1,1,1],alignment:'center'},
        // {text:'GST%',color:'white', border: [1,1,1,1],alignment:'center'},
        // {text:'Due-on',color:'white', border: [1,1,1,1],alignment:'center'},
        {text:'Qty',color:'white', border: [1,1,1,1],alignment:'center'},
        {text:'PER',color:'white', border: [1,1,1,1],alignment:'center'},
        // {text:'Rate',color:'white', border: [1,1,1,1],alignment:'center'},
        // {text:'Dis%',color:'white', border: [1,1,1,1],alignment:'center'},
        // {text:'Amount',color:'white', border: [1,1,1,1],alignment:'center'},
        ],
      ] 
    },layout: { 
        fillColor: function (rowIndex) { 
          if(rowIndex===0){
            return ('#1E90FF');
          }
        },
        hLineColor: function (i, node) {
          return (i === 0 || i === node.table.body.length) ? 'gray' : 'gray';
        },
        vLineColor: function (i, node) {
          return (i === 0 || i === node.table.widths.length) ? 'gray' : 'gray';
        },
      } 
  },
),  
 servicejson.forEach(myFunction)
    function myFunction(item, index, arr) {
      //arr[index] = item * 10;
      count++;
      var gstper;
      var rate=item.rate;
      var amt=item.netamount;
      var dis=parseInt(item.descper);
      if(rate==0){
        rate="";
        amt="";
        dis="";
      }else{
        amt='₹'+amt;
        dis=dis+'%';
      }
     
      if(item.igstper==0){
        gstper=parseInt(item.cgstper)+parseInt(item.sgstper);
        gstamt=gstamt+parseFloat(item.cgstamt)+parseFloat(item.sgstamt);
      }else{
        gstper=parseInt(item.igstper);
        gstamt=gstamt+parseFloat(item.igstamt);
      }
      totalamt=totalamt+parseFloat(item.qty)*parseFloat(item.rate);
      discount=discount+parseFloat(item.descamt);
      netamt+=parseFloat(item.netamount);
      // if(item.qty>1){
      //   var unit=item.unit+"'S";
      // }
      content[2].table.heights.push(20)
      content[2].table.body.push(
        [ 
        {text:count, border: [1,1,1,1],alignment:'center'},
        {text:item.itemname, border: [1,1,1,1],alignment:'left'},
        // {text:gstper+'%', border: [1,1,1,1],alignment:'right'},
        // {text:'', border: [1,1,1,1],alignment:'center'},
        {text:item.qty, border: [1,1,1,1],alignment:'center'},
        {text:item.unit, border: [1,1,1,1],alignment:'center'},
        // {text:rate, border: [1,1,1,1],alignment:'center'},
        // {text:dis, border: [1,1,1,1],alignment:'center'},
        // {text:amt, border: [1,1,1,1],alignment:'right'}
        ],
      )
    }
    content[2].table.heights.push(20)
    if(netamt==0){
        netamt="";
        gstamt="";
        discount="";
        totalamt="";
        totalamtword="";
      }else{
        totalamtword=withDecimal(netamt);
        netamt='₹'+netamt;
        gstamt='₹'+gstamt;
        discount='₹'+discount;
        totalamt='₹'+totalamt;
      }
    // content[2].table.body.push(
    //   [ 
    //     {text:'', border: [1,1,1,1],alignment:'center'},
    //     {text:'TOTALS', border: [1,1,1,1],alignment:'center',bold:1},
    //     {text:'', border: [1,1,1,1],alignment:'right'},
    //     {text:'', border: [1,1,1,1],alignment:'center'},
    //     {text:'', border: [1,1,1,1],alignment:'center'},
    //     {text:'', border: [1,1,1,1],alignment:'center'},
    //     {text:'', border: [1,1,1,1],alignment:'center'},
    //     {text:'', border: [1,1,1,1],alignment:'center'},
    //     {text:netamt, border: [1,1,1,1],alignment:'right'}
    //     ]
    //   )
      content.push(
          {text:'\n'},
          {
        columns: [
          //   {
          //     margin: [10, 5, 0, 0],
              
          //       table: {
          //           widths: [220],
                    
          //           body:[
          //               [{text:'Amount Chargeable (in words)\n',fontsize:10,border:[0,0,0,0]}],
          //               [{text:totalamtword+'\n\n',border:[0,0,0,0]}],
          //               [{text:'\n\nCompany\'s PAN\n',fontsize:10,border:[0,0,0,0]}],
          //               [{text:'\n\n',border:[0,0,0,0]}]
          //               ],
                        
          //       },layout: { 
          //                 fillColor: function (rowIndex) { 
          //                   if(rowIndex===1 || rowIndex===3){
          //                     return ('#D3D3D3');
          //                   }
          //                 }
                          
          //         },
          // },
          // {
          //   fontSize: 10,
          //   margin: [65, 0, 0, 15],
          //   table: {
          //     widths: [85,85],
          //     body: [
          //       [
          //       {text:'Sub-total', border: [0, 0, 0, 0]},
          //       {text:totalamt, border: [0, 0, 0, 0],alignment:'right'},
          //       ],
          //       [
          //       {text:'Discount', border: [0, 0, 0, 0]},
          //       {text:discount, border: [0, 0, 0, 0],alignment:'right'},
          //       ],
          //       [
          //       {text:'GST', border: [0, 0, 0, 0]},
          //       {text:gstamt, border: [0, 0, 0, 0],alignment:'right'},
          //       ],
          //       [
          //       {text:'TOTAL',bold:1, border: [0, 0, 0, 0],color:'white'},
          //       {text:netamt, border: [0, 0, 0, 0],color:'white',alignment:'right'},
          //       ],
          //       [
          //       {colSpan:2,text:'\n\nAuthorised Signatory', border: [0, 0, 0, 0],alignment:'center'}
          //       ]
                
                
          //     ]
          //   },
          {
            fontSize: 10,
            margin: [326, 0, 0, 15],
            table: {
              widths: [85,85],
              body: [
                
                [
                {colSpan:2,text:'\n\nAuthorised Signatory\n', border: [0, 0, 0, 0],alignment:'center'}
                ]
                
                
              ]
            },
            layout: { 
                  fillColor: function (rowIndex) { 
                    if(rowIndex===3){
                        return ('#1E90FF');
                      }else{
                        if(rowIndex===0 || rowIndex===1 || rowIndex===2){
                          return ('#D3D3D3');
                        }
                      }
                    }
                } 
          }
       ]
    },
      )
    dd = {
        pageSize: 'A4',
        pageOrientation: 'portrait',
        //pageMargins: [40, 30, 30, 35],
        pageMargins: [ 50, 30, 30, 40 ],
        footer: function (currentPage, pageCount) {
          return {
            margin: 10,
            columns: [{
              fontSize: 9,
              text: [{
                  text: '--------------------------------------------------------------------------' +
                    '\n',
                  margin: [0, 20]
                },
                {
                  text: '© Pest Mortem (India) PVT. LTD. | PAGE ' + currentPage.toString() + ' of ' + pageCount,
                }
              ],
              alignment: 'center'
            }]
          };

        },
        content,
        styles: {
          tablfont: {
            fontSize: 9
          },
          tablfont1: {
            fontSize: 9
          }
        }
      }
      var win = window.open('', '_blank');
      pdfMake.createPdf(dd).open({}, win);

    }
      }
    });
  }


</script>
