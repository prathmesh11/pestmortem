<?php 
 
    $base    = '../../';
    
    $navenq4 = 'background:#1B1464;';
    
    include('header.php');

    if (!in_array('Purchase Bill', $arrysession)) {
   
        echo "<script>window.location.href='/process/dashboard.php';</script>";
       
        exit;

    }

    if (session_status() == PHP_SESSION_NONE) { session_start(); }
    
    $sessionby = $_SESSION['employeeid'];
    
    $branchid  = $_SESSION['branchid'];

?>

<style>

    .table-list td,
    .table-list th {
        border: 1px solid #ddd;
        padding: 1px !important;
        font-size: 13px;
    }

    .table-list td {
        padding-top: 2px !important;
        margin: 0;
        text-align: center;
    }

    .table-list tr:nth-child(even) {
        background-color: #f2f2f2;
    }

    .table-list th {
        padding-top: 5px;
        padding-bottom: 5px;
        text-align: center;
        background-color: #16a085;
        color: white;
    }

</style>

<?php  

    $admin_branch = '';

    if($_SESSION['role']=='admin'){

        $branchid_admin = '';
        
        echo '<select onchange="branchadmin(this)" class="form-control input-sm" style="margin:2px;">';
        
            if($_COOKIE['branchid']!=''){

                $branchid_admin = $_COOKIE['branchid'];
                
                $branchname1    = mysqli_fetch_assoc(mysqli_query($con,"SELECT branchname x FROM branchmaster WHERE branchid='$branchid_admin'"))['x'];
            
                echo '<option value="'.$branchid_admin.'">'.$branchname1.'</option>';
            }
            
            echo '<option value="allbranch">All Branches</option>';
      
            $select1 = mysqli_query($con,"SELECT branchid, branchname FROM branchmaster WHERE branchid<>'$branchid_admin'");
      
            while($rows = mysqli_fetch_assoc($select1)){
        
                echo '<option value="'.$rows['branchid'].'">'.$rows['branchname'].'</option>';
      
            }
    
        echo '</select>';

    }

?>

<div class="table-ui container-fluid">
    
    <div class="tr row">
        
        <div class="col-sm-1 th">Order No.</div>
        
        <div class="col-sm-2 th">Purchase_Detail</div>
        
        <div class="col-sm-2 th">Supplier Info</div>
        
        <div class="col-sm-5 th">Order</div>
        
        <div class="col-sm-2 th">Action</div>
    </div>
        
    <?php
          
        if($_SESSION['role']=='admin'){
            
            if($branchid_admin==''){
                
                $result=mysqli_query($con,"SELECT DISTINCTROW purchaseid,podate,poforbranch,billid FROM `purchasebill` WHERE payment_time=0  AND confirm_time=0 ORDER BY id DESC");
            
            } else {
                
                $result=mysqli_query($con,"SELECT DISTINCTROW purchaseid,podate,poforbranch,billid FROM `purchasebill` WHERE  poforbranch='$branchid_admin' AND payment_time=0 AND confirm_time=0 ORDER BY id DESC");
            
            }
           
        } else {
            
            $result=mysqli_query($con,"SELECT DISTINCTROW purchaseid,podate,poforbranch,billid FROM `purchasebill` WHERE poforbranch='$branchid' AND payment_time=0 AND confirm_time=0 ORDER BY id DESC");
          
        }
         
        while($rows=mysqli_fetch_assoc($result)){

            $purchaseid   = $rows['purchaseid'];
                
            $podate       = $rows['podate'];
            
            $poforbranch  = $rows['poforbranch'];
            
            $billid       = $rows['billid'];
                
            $supplierid   = mysqli_fetch_assoc(mysqli_query($con,"SELECT supplierid x FROM `purchase` WHERE purchaseid='$purchaseid' ORDER BY id DESC LIMIT 1"))['x'];      
            
            $supplierjson = get_object_vars(json_decode(mysqli_fetch_assoc(mysqli_query($con,"SELECT supplierjson x FROM suppliermaster WHERE supplierid='$supplierid'"))['x']));
            
            $branchname   = mysqli_fetch_assoc(mysqli_query($con, "SELECT branchname x FROM `branchmaster` WHERE branchid='$poforbranch'"))['x'];

            $BillDetails  = mysqli_fetch_assoc(mysqli_query($con,"SELECT branchid,pobillimglink,purchasebillno,purchasedate FROM `purchasebill` WHERE billid='$billid' ORDER BY id DESC LIMIT 1"));
          
    ?>
        
    <div class="tr row">
        
        <div class="col-sm-1 td" style="word-wrap:break-word;">
            
            <?php echo $purchaseid; ?><br><br><?php echo $branchname; ?>
        
        </div>
        
        <div class="col-sm-2 td" style="word-wrap:break-word;">
            
            <?php echo "Bill Date : ". date("d-m-Y",strtotime($BillDetails['purchasedate'])); ?>

            <br>

            <?php echo "Bill No : ".$BillDetails['purchasebillno']; ?>
            
            <br>

            <a class="btn-info btn-sm" href="<?php echo $BillDetails['pobillimglink'];?>" target="_blank">Bill Image</a>    

        </div>
        
        <div class="col-sm-2 td" style="word-wrap:break-word;">
            
            <?php
                    
                $contactjson = json_decode(mysqli_fetch_assoc(mysqli_query($con,"SELECT contactjson x FROM suppliermaster WHERE supplierid='$supplierid'"))['x']);
                
                $sbranch     = mysqli_fetch_assoc(mysqli_query($con,"SELECT sbranch x FROM `purchase` WHERE purchaseid='$purchaseid' ORDER BY id DESC LIMIT 1"))['x'];      
              
                foreach($contactjson as $i){
                    // if(get_object_vars($i)['sbranch']==$sbranch){
                    echo 'Company : '.$supplierjson['suppliername']; 
                    echo '<br> Branch: '.get_object_vars($i)['sbranch']; 
                    echo '<br> Address : '.get_object_vars($i)['sbranchaddress'];
                    echo '<br> Mobile : '.get_object_vars($i)['mobile']; 
                    echo '<br> email : '.get_object_vars($i)['email']; 
                    // }
                }
                
            ?>

        </div>
        
        <div class="col-sm-5 td" style="word-wrap:break-word;">
            
            <?php 
            
                $purchsedetails = mysqli_fetch_assoc(mysqli_query($con,"SELECT id,close_time  FROM `purchase` WHERE purchaseid='$purchaseid' ORDER BY id DESC"));      

                $result1        = mysqli_query($con,"SELECT * FROM `purchasebill` WHERE purchaseid='$purchaseid' AND billid='$billid' ");
           
                echo '<table class="table table-list form-group">';
           
                    echo '<thead>';
                        
                        echo '<tr>';
                            
                            echo '<th width="300">Item Name</th>';
                            
                            echo '<th>Qty</th>';
                            
                            echo '<th width="80">Unit</th>';
                            
                            echo '<th>Rate</th>';
                            
                            echo '<th>DISC.</th>';
                            
                            echo '<th>Amount</th>';
                            
                            echo '<th>CGST</th>';
                            
                            echo '<th>SGST</th>';
                            
                            echo '<th>IGST</th>';
                            
                            echo '<th>Net Amount</th>';
                            
                            // echo '<th >Close</th>';
                        
                        echo '</tr>';
                    
                    echo '</thead>';
                    
                    echo '<tbody>';
                        
                        $total = 0;
                        
                        while($rowss=mysqli_fetch_assoc($result1)){

                            $itemcode.=',{"itemcode":"'.$rowss['itemcode'].'"}';
                            
                            echo '<tr>';
                                
                                echo '<td>'.$rowss['itemname'].'</td>';
                                
                                echo '<td>'.$rowss['qty'].'</td>';
                                
                                echo '<td>'.$rowss['unit'].'</td>';
                                
                                echo '<td>'.$rowss['rate'].'</td>';
                                
                                echo '<td>'.$rowss['descamt'].'</td>';
                                
                                echo '<td>'.$rowss['amount'].'</td>';
                                
                                echo '<td>'.$rowss['cgstamt'].'</td>';
                                
                                echo '<td>'.$rowss['sgstamt'].'</td>';
                                
                                echo '<td>'.$rowss['igstamt'].'</td>';
                                
                                echo '<td>'.$rowss['netamount'].'</td>';
                                
                                // echo '<td>';

                                //     if($purchsedetails['close_time']!=0){
                                        
                                //         echo 'Close';

                                //     }

                                // echo '</td>';
                                
                                $total+=$rowss['netamount'];
                            
                            echo '</tr>';
                        
                        }
                    echo '</tbody>';
                    
                echo '</table>';
                    
                echo 'Total : '.$total;

                $json = substr($itemcode,1);

                $json = '['.$json.']';
   
                $count1 = mysqli_fetch_assoc(mysqli_query($con,"SELECT COUNT(id) x FROM `purchase` WHERE purchaseid='$purchaseid'"))['x'];
                
                $count2 = mysqli_fetch_assoc(mysqli_query($con,"SELECT COUNT(id) x FROM `purchase` WHERE purchaseid='$purchaseid' AND close_time<>0"))['x'];
                
            ?>

        </div>

        <div class="col-sm-2 td" style="word-wrap:break-word;">
            
            <?php if($count1==$count2){ ?>
                
                <button class="btn btn-sm btn-block btn-success"  data-purchaseid='<?php echo $purchaseid; ?>' data-billid='<?php echo $billid; ?>' data-itemcode='<?php echo $json;?>' onclick="confirms1(this)">Confirm</button>
                <!-- <button class="btn btn-sm btn-block btn-warning" onclick="">Print</button> -->
                
                <input type="text" class="form-control input-xm text" placeholder="Enter Bill No" />
                
                <input type="date" class="form-control input-xm date" placeholder="Enter Bill date" />
                
                <input type="file" class="form-control input-xm file" placeholder="Bill" />
       
            <?php } else {

                //echo '<button class="btn btn-danger btn-sm btn-block" data-purchaseid="'.$purchaseid.'" onclick="closes(this)">Close</button>';
                echo '<button class="btn btn-sm btn-block btn-success"  data-purchaseid="'.$purchaseid.'" data-billid="'.$billid.'" data-itemcode='. $json.' onclick="confirms1(this)">Confirm</button>';
                echo '<a class="btn btn-info btn-sm btn-block" href="purchase-bill/index.php?billid='.$billid.'" >Edit</a>';

            } ?>
        
        </div>
        
    </div>

    <?php } ?>

</div>

<a class="btn" href="purchase-bill/index.php" style="font-size:25px;border-radius:50px;width:60px;height:60px;position: fixed;bottom: 40px;right: 40px;background:#eb2f06;color:#fff;" >+</a>


<?php 
include('footer.php');
?>
<script>

function closes(e){
    
    if (confirm("Are You Sure TO Confirm Order?")) {

        let purchaseid = $(e).data('purchaseid');

        $.ajax({

            type: "POST",
            
            data: 'purchaseid=' + purchaseid,
            
            url: 'api/close.php',
            
            cache: false,
            
            success: function (res) {
                
                if (res.status == 'success') {
                    
                    window.location.reload();
                
                } else if (res.status == 'falid2'){

                        alert('Check Purchase Order Gate Entry');

                }
            
            }
        
        })
    
    }
}

function confirms1(e){
    

    if (confirm("Are You Sure TO Confirm Order?")) {
        

        $.ajax({
            
            type: "POST",
            
            data: {
			purchaseid: $(e).data('purchaseid'),
			billid: $(e).data('billid'),
			itemcode: $(e).attr('data-itemcode')
		},

            
            url: 'api/purchasebill.php',
            
            cache: false,

            success: function (res) {
            
                if (res.status == 'success') {
            
                    window.location.reload();
            
                }
            
            }
        
        })
    
    }

}
</script>