<?php 
  $base='../../';
  $navenq1='background:#1B1464;';
  include('header.php');

  if (!in_array('Purchase Order', $arrysession)) {
   
    echo "<script>window.location.href='/process/dashboard.php';</script>";
   
    exit;

}

  $purchaseid=mysqli_fetch_assoc(mysqli_query($con,"SELECT count(DISTINCTROW purchaseid) x FROM `purchase`"))['x'];
$purchaseid++;
  if (session_status() == PHP_SESSION_NONE) { session_start(); }
  $sessionby = $_SESSION['employeeid'];
  $branchid = $_SESSION['branchid'];

?>
<style>
  .table-list td,
    .table-list th {
        border: 1px solid #ddd;
        padding: 1px !important;
        font-size: 13px;
    }
    .table-list td{
    padding-top: 2px !important;
      margin:0;

    }

    .table-list tr:nth-child(even) {
        background-color: #f2f2f2;
    }

    .table-list th {
        padding-top: 5px;
        padding-bottom: 5px;
        text-align: center;
        background-color: #16a085;
        color: white;
    }
</style>
<br>
<div class="container-fluid">
<input type="hidden" id="purchaseid" value="<?php echo $purchaseid; ?>">
<input type="hidden" id="supplierjson">
<div class="row" style="border-radius:4px;border:1px solid #CCC;background:#FFF;padding-top:10px;-webkit-box-shadow: 2px 2px 3px 3px #ccc; -moz-box-shadow:2px 2px 3px 3px #ccc;box-shadow:2px 2px 3px 3px #ccc;">
        
        <div class="col-sm-3">
        <div class="col-sm-4">BRANCH</div>
        <div class="col-sm-8">
                    <div class="form-group">
                            <select class="form-control input-sm" data-role="select" id="inp-branchid" data-name="branchid" onchange="getprlist(this)"  >
                                    <option value="Select">Select</option>
                                    <?php
                                    
                                        $result1=mysqli_query($con,"SELECT DISTINCTROW branchid FROM `purchase_req` WHERE approvard_time<>0 AND qty<>podoneqty");
                                        while($rows=mysqli_fetch_assoc($result1)) {
                                            $branchid=$rows['branchid'];
                                            $branchname=mysqli_fetch_assoc(mysqli_query($con, "SELECT branchname x FROM `branchmaster` WHERE branchid='$branchid'"))['x'];
                                            echo '<option value="'.$rows['branchid'].'">'.$branchname.'</option>';
                                        }
                                    ?>
                            </select>
                    </div>
            </div>
        </div>
        
        <div class="col-sm-3">
        <div class="col-sm-6"><button class="btn btn-sm btn-success btn-block" >Submit</button></div>
        <div class="col-sm-6"><button class="btn btn-sm btn-danger btn-block" >Reset</button></div>
    </div>
    <br>
    <table id="table-purchase1" class="table table-list">
                    <thead>
                        <tr>
                            <th rowspan="2">TEMP PO No</th>
                            <th rowspan="2">Supplier</th>
                            <th rowspan="2">Item Details</th>
                            <th rowspan="2" width="80">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
        </table>
        <br>

    <table id="table-purchase" class="table table-list">
                    <thead>
                        <tr>
                            <th rowspan="2">PR No</th>
                            <th rowspan="2">Supplier</th>
                            <th rowspan="2">Item Name</th>
                            <th rowspan="2">DESC</th>
                            <th rowspan="2">Qty</th>
                            <th rowspan="2">Unit</th>
                            <th rowspan="2" width="80">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
        </table>
</div>
<form method="POST" id="formpo" target="_blank" style="display:none;" action="purchase-order/index.php">
    <input type="text" name="branchid" id="branchid1">
    <input type="text" name="pono" id="pono">
    <input type="text" name="supplierid" id="supplierid">
    <input type="text" name="itemjson" id="itemjson">
</form>

<?php 
include('footer.php');
?>
<script>

function getprlist(e) {

    var branchid = $(e).val();

    $.ajax({

        type: "POST",

        data: 'branchid=' + branchid,

        url: 'api/getprlist.php',

        cache: false,

        success: function (res) {

            if (res.status == 'success') {

                var arr  = res.json;

                var arr1 = res.json1;

                var str  = '';

                for (var i in arr) {

                    var supplierJson = arr[i].supplierJson;

                    var str1 = '<select class="form-control input-sm supplierid"><option class="Select">Select</option>';

                    for (var a in supplierJson) {


                        if (arr[i].suppliercode == supplierJson[a].suppliercode) {

                            str1 += '<option value="' + supplierJson[a].suppliercode + '" selected>' + supplierJson[a].suppliername + '</option>';

                        } else {
                            console.log('b');

                            str1 += '<option value="' + supplierJson[a].suppliercode + '">' + supplierJson[a].suppliername + '</option>';

                        }

                    }

                    str1 += '</select>';


                    str += '<tr>';

                    str += '<td class="prno">' + arr[i].prno + '</td>';

                    str += '<td>' + str1 + '</td>';

                    str += '<td class="itemname">' + arr[i].itemname + '</td>';

                    str += '<td class="itemcode" style="display:none;">' + arr[i].itemcode + '</td>';

                    str += '<td class="descip">' + arr[i].descip + '</td>';

                    str += '<td class="qty">' + arr[i].qty + '</td>';

                    str += '<td class="unit">' + arr[i].unit + '</td>';

                    str += '<td align="center"><button class="btn btn-sm btn-primary" onclick="add(this)">add</button></td>';

                    str += '</tr>';

                }

                $('#table-purchase > tbody').html(str);

            }

        }

    });
    
}



function add(e) {
    var prno = $(e).parent().parent().find('.prno').text().trim();
    var supplierid = $(e).parent().parent().find('.supplierid').val();
    var suppliername = $(e).parent().parent().find('.supplierid option:selected').text();
    var itemname = $(e).parent().parent().find('.itemname').text().trim();
    var itemcode = $(e).parent().parent().find('.itemcode').text().trim();
    var descip = $(e).parent().parent().find('.descip').text().trim();
    var qty = $(e).parent().parent().find('.qty').text().trim();
    var unit = $(e).parent().parent().find('.unit').text().trim();
    var pono = 'PMI/SUP-' + supplierid + '/' + supplierid + '/' + getCurrentFinancialYear();
    if (supplierid == 'Select') {
        $(e).parent().parent().find('.supplierid').css('border', '1px solid red');
    } else {
        var len=$('#table-purchase1 .pono').length;
        if(len==0){
            var str = '<tr>';
                str += '<td class="pono">' + pono + '</td>';
                str += '<td class="suppliername">' + suppliername + '</td>';
                str += '<td class="supplierid" style="display:none">' + supplierid + '</td>';
                var str1 = '';
                str1 += '<table class="table table-list innertable">';
                str1 += '<thead>';
                str1 += '<tr>';
                str1 += '<th rowspan="2">Item Name</th>';
                str1 += '<th rowspan="2">DESC</th>';
                str1 += '<th rowspan="2">Qty</th>';
                str1 += '<th rowspan="2">Unit</th>';
                str1 += '<th rowspan="2" width="80">Action</th>';
                str1 += '</tr>';
                str1 += '</thead>';
                str1 += '<tbody>';
                str1 += '<tr>';
                str1 += '<td class="prno" style="display:none;">' + prno + '</td>';
                str1 += '<td class="itemname">' + itemname + '</td>';
                str1 += '<td class="itemcode" style="display:none;">' + itemcode + '</td>';
                str1 += '<td class="descip">' + descip + '</td>';
                str1 += '<td class="qty">' + qty + '</td>';
                str1 += '<td class="unit">' + unit + '</td>';
                str1 += '<td align="center"><button class="btn btn-sm btn-danger" onclick="rem(this)">R</button></td>';
                str1 += '</tr>';
                str1 += '</tbody>';
                str1 += '</table>';
                str += '<td>' + str1 + '</td>';
                str += '<td><button class="btn btn-sm btn-success" onclick="createpo(this)">Create PO</button></td>';
                str += '</tr>';
                $('#table-purchase1 > tbody').append(str);
        }else{
            checkadder(pono,suppliername,supplierid,prno,itemname,itemcode,descip,qty,unit);
        }
        $(e).parent().parent().remove();
    }
    
}

function checkadder(pono,suppliername,supplierid,prno,itemname,itemcode,descip,qty,unit){
    valid=false;
    $('#table-purchase1 > tbody > tr').each(function(){
       var pono1 = $(this).find('.pono').text().trim();
       if(pono1==pono){
        valid=true;
        var str1 = '<tr>';
                str1 += '<td class="prno" style="display:none;">' + prno + '</td>';
                str1 += '<td class="itemname">' + itemname + '</td>';
                str1 += '<td class="itemcode" style="display:none;">' + itemcode + '</td>';
                str1 += '<td class="descip">' + descip + '</td>';
                str1 += '<td class="qty">' + qty + '</td>';
                str1 += '<td class="unit">' + unit + '</td>';
                str1 += '<td align="center"><button class="btn btn-sm btn-danger" onclick="rem(this)">R</button></td>';
                str1 += '</tr>';
                $(this).find('.innertable > tbody').append(str1);
       }
    });
    console.log(valid);
    if(valid==false){
        var str = '<tr>';
        str += '<td class="pono">' + pono + '</td>';
        str += '<td class="suppliername">' + suppliername + '</td>';
        str += '<td class="supplierid" style="display:none">' + supplierid + '</td>';
        var str1 = '';
        str1 += '<table class="table table-list innertable">';
        str1 += '<thead>';
        str1 += '<tr>';
        str1 += '<th rowspan="2">Item Name</th>';
        str1 += '<th rowspan="2">DESC</th>';
        str1 += '<th rowspan="2">Qty</th>';
        str1 += '<th rowspan="2">Unit</th>';
        str1 += '<th rowspan="2" width="80">Action</th>';
        str1 += '</tr>';
        str1 += '</thead>';
        str1 += '<tbody>';
        str1 += '<tr>';
        str1 += '<td class="prno" style="display:none;">' + prno + '</td>';
        str1 += '<td class="itemname">' + itemname + '</td>';
        str1 += '<td class="itemcode" style="display:none;">' + itemcode + '</td>';
        str1 += '<td class="descip">' + descip + '</td>';
        str1 += '<td class="qty">' + qty + '</td>';
        str1 += '<td class="unit">' + unit + '</td>';
        str1 += '<td align="center"><button class="btn btn-sm btn-danger" onclick="rem(this)">R</button></td>';
        str1 += '</tr>';
        str1 += '</tbody>';
        str1 += '</table>';
        str += '<td>' + str1 + '</td>';
        str += '<td><button class="btn btn-sm btn-success" onclick="createpo(this)">Create PO</button></td>';
        str += '</tr>';
        $('#table-purchase1 > tbody').append(str);
    }
    var counter=0;
}
function rem(e){
    var prno = $(e).parent().parent().find('.prno').text().trim();
    var itemname = $(e).parent().parent().find('.itemname').text().trim();
    var itemcode = $(e).parent().parent().find('.itemcode').text().trim();
    var descip = $(e).parent().parent().find('.descip').text().trim();
    var qty = $(e).parent().parent().find('.qty').text().trim();
    var unit = $(e).parent().parent().find('.unit').text().trim();
    var arr1 = JSON.parse($('#supplierjson').val());
    var str1='<select class="form-control input-sm supplierid"><option class="Select">Select</option>';
                for(var i in arr1){
                     str1+='<option value="'+arr1[i].supplierid+'">'+arr1[i].suppliername+'</option>';
                }
                str1+='</select>';
                var str='';
                    str+='<tr>';
                    str+='<td class="prno">'+ prno+'</td>';
                    str+='<td>'+str1+'</td>';
                    str+='<td class="itemname">'+ itemname+'</td>';
                    str+='<td class="itemcode" style="display:none;">'+ itemcode+'</td>';
                    str+='<td class="descip">'+ descip+'</td>';
                    str+='<td class="qty">'+ qty+'</td>';
                    str+='<td class="unit">'+ unit+'</td>';
                    str+='<td align="center"><button class="btn btn-sm btn-primary" onclick="add(this)">add</button></td>';
                    str+='</tr>';
                $('#table-purchase > tbody').append(str);
                var len=$(e).parent().parent().parent().find('tr').length;
                console.log(len);
                if(len==1){
                    $(e).parent().parent().parent().parent().parent().parent().remove();
                }else{
                    $(e).parent().parent().remove();
                }
}


function getCurrentFinancialYear() {
  var fiscalyear = "";
  var today = new Date();
  if ((today.getMonth() + 1) <= 3) {
    fiscalyear = (today.getFullYear() - 1) + "-" + today.getFullYear()
  } else {
    fiscalyear = today.getFullYear() + "-" + (today.getFullYear() + 1)
  }
  return fiscalyear;
}

function createpo(e){
    var pono = $(e).parent().parent().find('.pono').text().trim();
    var supplierid = $(e).parent().parent().find('.supplierid').text().trim();
    var arr=[];
    console.log(supplierid);
    $(e).parent().parent().find('tbody > tr').each(function (){
        console.log($(this).html());
        
    arr.push({
    "prno" : $(this).find('.prno').text().trim(),
    "itemname" : $(this).find('.itemname').text().trim(),
    "itemcode" : $(this).find('.itemcode').text().trim(),
    "descip" : $(this).find('.descip').text().trim(),
    "qty" : $(this).find('.qty').text().trim(),
    "unit" : $(this).find('.unit').text().trim()
    });
    })
    
$('#branchid1').val($('#inp-branchid').val());
$('#pono').val(pono);
$('#supplierid').val(supplierid);
$('#itemjson').val(JSON.stringify(arr));
$('#formpo').submit();
$(e).parent().parent().remove();
}

</script>
