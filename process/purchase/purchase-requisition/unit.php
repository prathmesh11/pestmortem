<?php

   $base='../../../';

   include($base.'_in/connect.php');

   header('content-type: application/json; charset=utf-8');

   header("access-control-allow-origin: *");

   if(isset($_POST['itemcode'])){

      $con      = _connect();

      $itemcode = _clean($con,$_POST["itemcode"]);

      $itemmaster    = mysqli_fetch_assoc(mysqli_query($con, "SELECT unit1,supplier_json FROM stockmaster WHERE stockid='$itemcode'")); 

      if($itemmaster){

         if ($itemmaster['supplier_json'] == '') {
      
            $supplier_json = '[]';
      
         } else {
      
            $supplier_json = $itemmaster['supplier_json'];
      
         }

         echo '{"status":"success","json":"'.$itemmaster['unit1'].'","supplier_json":'.$supplier_json.'}';

      } else {

         echo '{"status":"falid1"}';

      }

      _close($con);

   } else {

      echo '{"status":"falid2"}';
     
   }
?>