<?php

    $base = '../../../';

    include($base.'_in/connect.php');

    header('content-type: application/json; charset=utf-8');

    header("access-control-allow-origin: *");
    
    if(isset($_POST['pobranchid']) &&  isset($_POST['podate']) && isset($_POST['department'])  && isset($_POST['data'])) {

      $con = _connect();

     
      if (session_status()==PHP_SESSION_NONE) { session_start(); }

      $created_by = $_SESSION['employeeid'];

      $role       = mysqli_fetch_assoc(mysqli_query($con,"SELECT role x FROM employeemaster WHERE employeeid='$created_by'"))['x'];

      $branchid   = _clean($con,$_POST["pobranchid"]);

      $podate     = _clean($con,$_POST["podate"]);

      $department = _clean($con,$_POST["department"]);

      $data       = json_decode($_POST['data']);

      $year1      = 0;

      $year2      = 0;

      if ( date('m') > 3 ) {

        $year1 = date('y');

        $year2 = date('y')+1;

      } else {

        $year1 = date('y')-1;

        $year2 = date('y');

      }

      $ids=mysqli_fetch_assoc(mysqli_query($con,"SELECT max(pr_no_count) x FROM purchase_req WHERE branchid='$branchid' ORDER BY id DESC"))['x'];

      if(!$ids){

        $pr_no_count = 1;

      } else {

        $ids++;

        $pr_no_count = $ids;

      }


      $branchshortname = mysqli_fetch_assoc(mysqli_query($con,"SELECT branchshortname x FROM branchmaster WHERE branchid='$branchid'"))['x'];

      $purchaseid      = mysqli_fetch_assoc(mysqli_query($con,"SELECT DISTINCTROW max(pr_no_count) x FROM `purchase_req` WHERE branchid='$branchid'"))['x'];

      $purchaseid++;

      $pr_no = 'PMI/'.$branchshortname.'/'.$purchaseid.'/'.$year1.'-'.$year2;

      foreach($data as $i){

        $itemname     = get_object_vars($i)['itemname'];

        $itemcode     = get_object_vars($i)['itemcode'];

        $itemdes      = get_object_vars($i)['itemdes'];

        $qty          = get_object_vars($i)['qty'];

        $suppliercode = get_object_vars($i)['suppliercode'];

        $suppliername = get_object_vars($i)['suppliername'];

        $unit         = get_object_vars($i)['unit'];

        $remark       = get_object_vars($i)['remark'];

        if ($role=='admin') {

          mysqli_query($con,"INSERT INTO purchase_req (branchid,pr_no_count,pr_no, prdate, department,itemname,itemcode,descip,qty,suppliercode,suppliername,unit,remark, created_by, created_time,approvard_by,approvard_time) VALUES ('$branchid','$pr_no_count','$pr_no','$podate','$department','$itemname','$itemcode','$itemdes','$qty','$suppliercode','$suppliername','$unit','$remark','$created_by','$CURRENT_MILLIS','$created_by','$CURRENT_MILLIS')");

        } else {

          mysqli_query($con,"INSERT INTO purchase_req (branchid,pr_no_count,pr_no, prdate, department,itemname,itemcode,descip,qty,suppliercode,suppliername,unit,remark, created_by, created_time) VALUES ('$branchid','$pr_no_count','$pr_no','$podate','$department','$itemname','$itemcode','$itemdes','$qty','$suppliercode','$suppliername','$unit','$remark','$created_by','$CURRENT_MILLIS')");

        }

      }
        
      echo '{"status":"success"}';

      _close($con);

    } else {

      echo '{"status":"falid"}';

    }
?>