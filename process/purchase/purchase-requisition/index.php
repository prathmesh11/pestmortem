<?php 

	$base    = '../../../';

	$navenq5 = 'background:#1B1464;';

	include('../header.php');

	if (!in_array("Purchase Requisition", $arrysession) ) {
   
        echo "<script>window.location.href='/process/dashboard.php';</script>";
       
        exit;

    }
	
?>

<br>
<style>

	body {
		background: #eee;
	}

	.table-list td,
	.table-list th {
		border: 1px solid #ddd;
		padding: 1px !important;
		font-size: 13px;
	}

	.table-list td {
		padding-top: 2px !important;
		margin: 0;

	}

	.table-list tr:nth-child(even) {
		background-color: #f2f2f2;
	}

	.table-list th {
		padding-top: 5px;
		padding-bottom: 5px;
		text-align: center;
		background-color: #16a085;
		color: white;
	}
	
</style>

<div class="container-fluid">

	<input type="hidden" id="pr_no" data-name="pr_no" value='<?php echo $_GET['pr_no']; ?>' />

	<div class="row" id="div-select"
		style="border-radius:4px;border:1px solid #CCC;background:#FFF;padding-top:10px;-webkit-box-shadow: 2px 2px 3px 3px #ccc; -moz-box-shadow:2px 2px 3px 3px #ccc;box-shadow:2px 2px 3px 3px #ccc;">

		<div class="col-sm-3">

			<div class="col-sm-5">Order for Branch</div>

			<div class="col-sm-7">

				<div class="form-group">

					<select class="form-control input-sm select-js1" data-role="select" data-name="branchid" id="branchid" disabled="true">

						<option value="Select">Select</option>

						<?php

							$result1=mysqli_query($con, "SELECT branchname,branchid FROM `branchmaster`");

								while($rows=mysqli_fetch_assoc($result1)) {

									echo '<option value="'.$rows['branchid'].'">'.$rows['branchname'].'</option>';
									
								}		
							
						?>

					</select>

				</div>

			</div>

		</div>

		<div class="col-sm-3">

			<div class="col-sm-4">DATE</div>

			<div class="col-sm-8">

				<div class="form-group">

					<input type="date" id="podate" value="<?php echo $today;?>"class="form-control input-sm">

				</div>

			</div>

		</div>

		<div class="col-sm-3">

			<div class="col-sm-4">Catagory</div>

			<div class="col-sm-8">

				<div class="form-group">

					<input type="text" data-role="text" data-name="department" id="department" class="form-control input-sm">


					<!-- <select class="form-control input-sm" data-role="select" data-name="department" id="department">

						<option value="Select">Select</option>

						<?php

							$result1=mysqli_query($con, "SELECT groupCode,groupName FROM `additionalmaster` WHERE mainGroupName='DEPARTMENT' AND mainGroupCode=7  ORDER BY groupName ASC");

								while($rows=mysqli_fetch_assoc($result1)) {

									echo '<option value="'.$rows['groupCode'].'">'.$rows['groupName'].'</option>';

								}

						?>

						<option value="other">Other</option>

					</select> -->

				</div>

			</div>

		</div>

		<div class="col-sm-2">

			<button class="btn btn-sm btn-success btn-block" id="btn-insert" onclick="insert()">Submit</button>

		</div>

	</div>

	<br>

	<div class="row"
		style="border-radius:4px;border:1px solid #CCC;background:#FFF;padding-top:10px;-webkit-box-shadow: 2px 2px 3px 3px #ccc; -moz-box-shadow:2px 2px 3px 3px #ccc;box-shadow:2px 2px 3px 3px #ccc;">

		<table id="table-order" class="table table-list form-group">

			<thead>

				<tr>

					<th rowspan="3">SrNo</th>

					<th rowspan="3" width="300">Item Name</th>

					<th rowspan="2">Item Description</th>

					<th rowspan="1">Qty</th>

					<th width="300">Supplier Name</th>

					<th rowspan="2">Unit</th>

					<th rowspan="2">Remarks</th>

					<th rowspan="2">Action</th>

				</tr>

			</thead>

			<tbody>

				<tr>

					<td> </td>

					<td>

						<select class="form-control input-sm select-js1" data-name="itemcode" data-role="select"
							onchange="itemcode(this)" id="itemcode" >

							<option value="Select">Select</option>

							<?php

								if (session_status() == PHP_SESSION_NONE) { session_start(); }

								$branchid = $_SESSION['branchid'];

								$result1=mysqli_query($con, "SELECT stockid,itemname FROM `stockmaster` ORDER BY itemname ASC");

									while($rows=mysqli_fetch_assoc($result1)) {

										echo '<option value="'.$rows['stockid'].'">'.$rows['itemname'].'</option>';
									}

							?>

						</select>

					</td>

					<td>

						<input type="text" class="form-control input-sm" data-name="itemdes" data-role="text" id="itemdes" autocomplete='sameer'>
					
					</td>

					<td>

						<input type="text" class="form-control input-sm" data-name="qty" data-role="number" id="qty">

					</td>

					<td>

						<select class="form-control input-sm select-js1" data-name="suppliername" data-role="select" id="suppliername">

							<option value="Select">Select</option>

						</select>			

					</td>

					<td>

						<input type="text" class="form-control input-sm" data-name="unit" data-role="text" id="unit" readonly>

					</td>

					<td>
						
						<input type="text" class="form-control input-sm" data-name="remark" data-role="text" id="remark" autocomplete='sameer'>

					</td>

					<td align="center">
						
						<button class="btn btn-sm btn-success" id="btn-add" data-edit="" onclick="add(this)">Add</button>

					</td>

				</tr>

			</tbody>

		</table>

	</div>

</div>

<?php 

	include('../footer.php');

?>

<script>

// add function start :
function add(e) {

	var chk          = $(e).data('edit');

	var data         = checker('table-order');

	var itemname     = $('#itemcode').find(":selected").text();

	var itemcode     = data.itemcode;

	var itemdes      = data.itemdes;

	var qty          = data.qty;

	var suppliercode = data.suppliername;

	var suppliername = $('#suppliername').find(":selected").text();

	var unit         = data.unit;

	var remark       = data.remark;

	var valid        = true;

	if (checker('table-order') != false) {

		valid = valid * true;

	} else {

		valid = valid * false;

	}

	$('#table-order .itemcode').each(function () {

		var itemcode1 = $(this).text().trim();

		if (itemcode1 == itemcode && chk == '') {

			valid = valid * false;

			alert('Dublicate Item Name');

		}

	});

	if (valid) {

		var chk = $(e).data('edit');

		if (chk == '') {

			var len = $('#table-order .srno').length + 1;

			fun_adder(len, itemname, itemcode, itemdes, qty, suppliercode, suppliername, unit, remark);

		} else {

			fun_adder_edit(chk, itemname, itemcode, itemdes, qty, suppliercode, suppliername, unit, remark);

		}

		modals.clear('table-order');

		$('#btn-add').data('edit', '');

	}

}

// add function end :

// fun_adder function start :
function fun_adder(len,itemname,itemcode,itemdes,qty, suppliercode, suppliername,unit,remark) {
		var str = '<tr>';
		str += '<td align="center" class="srno">' + len + '</td>';
		str += '<td align="center" class="itemname">' + itemname + '</td>';
		str += '<td align="center" class="itemcode" style="display:none;">' + itemcode + '</td>';
		str += '<td align="center" class="itemdes">' + itemdes + '</td>';
		str += '<td align="center" class="qty">' + qty + '</td>';
		str += '<td align="center" class="suppliercode" style="display:none;">' + suppliercode + '</td>';
		str += '<td align="center" class="suppliername">' + suppliername + '</td>';
		str += '<td align="center" class="unit">' + unit + '</td>';
		str += '<td align="center" class="remark">' + remark + '</td>';
		str += '<td align="center"><button class="btn btn-sm btn-success" onclick="editor(' + len + ',this)">E</button><button class="btn btn-sm btn-danger remover" onclick="remover(this)">R</buuton></td>';
		str += '</tr>';
		$('#table-order > tbody').append(str);
}
// fun_adder function end :

// fun_adder_edit function start :
function fun_adder_edit(len,itemname,itemcode,itemdes,qty,suppliercode, suppliername,unit,remark) {
   var str = '<td align="center" class="srno">' + len + '</td>';
		str += '<td align="center" class="itemname">' + itemname + '</td>';
		str += '<td align="center" class="itemcode" style="display:none;">' + itemcode + '</td>';
		str += '<td align="center" class="itemdes">' + itemdes + '</td>';
		str += '<td align="center" class="qty">' + qty + '</td>';
		str += '<td align="center" class="suppliercode" style="display:none;">' + suppliercode + '</td>';
		str += '<td align="center" class="suppliername">' + suppliername + '</td>';
		str += '<td align="center" class="unit">' + unit + '</td>';
		str += '<td align="center" class="remark">' + remark + '</td>';
		str += '<td align="center"><button class="btn btn-sm btn-success" onclick="editor(' + len + ',this)">E</button><button class="btn btn-sm btn-danger remover" onclick="remover(this)">R</buuton></td>';

			$('#table-order .srno').each(function(){
			var srno=$(this).text().trim();
			if(srno==len){
				$(this).parent().html(str);
			}
		});
	  
}
// fun_adder_edit function end :

//editor function start :
function editor(srno,e){

	var itemcode=$(e).parent().parent().find('.itemcode').text().trim();
	$('#itemcode').val(itemcode);
	$('#itemcode').trigger('change');

	var suppliercode=$(e).parent().parent().find('.suppliercode').text().trim();

	setTimeout(function(){ 

		$('#suppliername').val(suppliercode).trigger('change');;

	}, 500);

	$('#itemdes').val($(e).parent().parent().find('.itemdes').text().trim());
	 $('#qty').val($(e).parent().parent().find('.qty').text().trim());
	 $('#remark').val($(e).parent().parent().find('.remark').text().trim());
	 
	$('#btn-add').data('edit',srno);
	
}
//editor function end :

var json2 = [];
var pr_no = $('#pr_no').val();
if (pr_no != "") {
	edit();
}


function itemcode(e) {

	var itemcode = $(e).val();

	if (itemcode != 'Select') {
		
		$.ajax({

			type: "POST",

			data: 'itemcode=' + itemcode,

			url: 'unit.php',

			cache: false,

			success: function (res) {

				if (res.status == 'success') {

					var json = res.json;

					$('#unit').val(json);

					let supplierJson = res.supplier_json;

					let str = '<option value="Select">Select</option>';

					for (let i in supplierJson) {

						str+="<option value="+supplierJson[i].suppliercode+">"+supplierJson[i].suppliername+"</option>";

					} 

					$('#suppliername').html(str); 

				}

			}

		});
	
	} else {

		$('#suppliername').html("<option value='Select'>Select</option>"); 

	}

}


function insert() {

	var department = $('#department').val();
	var data = modals.tabletdbyclass('table-order');
	data.shift();

	console.log(data);

	var valid = true;
	if (department != '') {
		valid = valid * true;
		$('#department').css('border', '2px solid green');
	} else {
		valid = valid * false;
		$('#department').css('border', '2px solid red');
	}

	if (data != '') {
		valid = valid * true;
		$('#table-order').css('border', '2px solid green');
	} else {
		valid = valid * false;
		$('#table-order').css('border', '2px solid red');
	}

	if (valid) {
		$.ajax({
			type: "POST",
			data: {
				pobranchid: $('#branchid').val(),
				podate: $('#podate').val(),
				department: $('#department').val(),
				data: JSON.stringify(data)
			},
			url: 'insert.php',
			cache: false,
			success: function (res) {
				if (res.status == 'success') {

					swal({
						type: 'success',
						title: 'Purchase Requisition Created',
						showConfirmButton: false,
						timer: 3000
					});
					setTimeout(function () {
						window.location = "../purchase-requisition.php";
					}, 3000);
				}
			}
		});
	}

}



function edit() {

	$('#btn-insert').attr('onclick', 'update()');

	$('#btn-insert').text('Update');

	$.ajax({

		type: "POST",

		data: 'pr_no=' + $('#pr_no').val(),

		url: 'select.php',

		cache: false,

		success: function (res) {

			if (res.status == 'success') {

				var json = res.json;
				json2    = json;

				$("#branchid").val(res.branchid);

				$("#podate").val(res.prdate);

				$("#department").val(res.department).trigger('change');

				var json   = res.json;

				var couter = 1;

				for (var i in json) {

					fun_adder(couter, json[i].itemname, json[i].itemcode, json[i].itemdes, json[i].qty, json[i].suppliercode, json[i].suppliername, json[i].unit, json[i].remark);

					couter++;

				}

			}

		}

	});

}

function update() {

	var data = modals.tabletdbyclass('table-order');

	data.shift();

	$.ajax({

		type: "POST",

		data: {
			pobranchid: $('#branchid').val(),
			podate: $('#podate').val(),
			department: $('#department').val(),
			pr_no: $('#pr_no').val(),
			data: JSON.stringify(data)
		},

		url: 'update.php',

		cache: false,

		success: function (res) {

			if (res.status == 'success') {

				swal({

					type: 'success',
					title: 'Purchase Requisition Updated',
					showConfirmButton: false,
					timer: 3000

				});

				setTimeout(function () {

					window.location = "../purchase-requisition.php";

				}, 3000);

			}
			
		}

	});
	
}

var branchid = $('#inp-session-branchid').val();
$('#branchid').val(branchid);

$(document).ready(function () {
	$('.select-js1').select2({
		width: '100%'
	});
	$('.select').attr('style', 'width:100%!important;');

});

var role=$('#accessrole').val();
if(role=='admin'){
	$('#branchid').prop('disabled',false);
}
</script>