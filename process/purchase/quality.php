<?php 
    $base='../../';
    $navenq3='background:#1B1464;';
    include('header.php');

    if (session_status() == PHP_SESSION_NONE) { session_start(); }
    $sessionby = $_SESSION['employeeid'];
    $branchid = $_SESSION['branchid'];

?>
<style>
    .table-list td,
    .table-list th {
        border: 1px solid #ddd;
        padding: 1px !important;
        font-size: 13px;
    }
   .table-list td{
	padding-top: 2px !important;
    margin:0;

   }

   .table-list tr:nth-child(even) {
       background-color: #f2f2f2;
   }

   .table-list th {
       padding-top: 5px;
       padding-bottom: 5px;
       text-align: center;
       background-color: #16a085;
       color: white;
   }
</style>
<br>
<div class="table-ui container-fluid">
    <div class="tr row">
        <div class="col-sm-1 th">Order No.</div>
        <div class="col-sm-2 th">Supplier Info</div>
        <div class="col-sm-9 th">Order</div>

        </div>
        <?php
           $result=mysqli_query($con,"SELECT DISTINCTROW purchaseid,poforbranch,podate FROM `gateentry` WHERE branchid='$branchid' AND confirm_time=0 ORDER BY id DESC");
           while($rows=mysqli_fetch_assoc($result)){
                $purchaseid=$rows['purchaseid'];
                $podate=$rows['podate'];
                $poforbranch=$rows['poforbranch'];
                $supplierid=mysqli_fetch_assoc(mysqli_query($con,"SELECT supplierid x FROM `purchase` WHERE purchaseid='$purchaseid' ORDER BY id DESC LIMIT 1"))['x'];      
                $supplierjson=get_object_vars(json_decode(mysqli_fetch_assoc(mysqli_query($con,"SELECT supplierjson x FROM suppliermaster WHERE supplierid='$supplierid'"))['x']));
                $branchname=mysqli_fetch_assoc(mysqli_query($con, "SELECT branchname x FROM `branchmaster` WHERE branchid='$poforbranch'"))['x'];
               ?>
        <div class="tr row">
        <div class="col-sm-1 td" style="word-wrap:break-word;"><?php echo $purchaseid; ?><br><br><?php echo $branchname; ?></div>
        <div class="col-sm-2 td" style="word-wrap:break-word;">
        <?php
                    $contactjson=json_decode(mysqli_fetch_assoc(mysqli_query($con,"SELECT contactjson x FROM suppliermaster WHERE supplierid='$supplierid'"))['x']);
                    $sbranch=mysqli_fetch_assoc(mysqli_query($con,"SELECT sbranch x FROM `purchase` WHERE purchaseid='$purchaseid' ORDER BY id DESC LIMIT 1"))['x'];      
              foreach($contactjson as $i){
                if(get_object_vars($i)['sbranch']==$sbranch){
                 echo 'Company : '.$supplierjson['suppliername']; 
                 echo '<br> Branch: '.get_object_vars($i)['sbranch']; 
                 echo '<br> Address : '.get_object_vars($i)['sbranchaddress'];
                 echo '<br> Mobile : '.get_object_vars($i)['mobile']; 
                 echo '<br> email : '.get_object_vars($i)['email']; 
                }
             }
        ?>
        </div>
        <div class="col-sm-9 td" style="word-wrap:break-word;">
        <?php 
       $result1=mysqli_query($con,"SELECT * FROM `gateentry` WHERE purchaseid='$purchaseid' AND confirm_time=0");
       echo '<table class="table table-list form-group">';
       echo '<thead>';
       echo '<tr>';
       echo '<th width="300">Item Name</th>';
       echo '<th width="40">QTY</th>';
       echo '<th width="80">QTY</th>';
       echo '<th width="40">Unit</th>';
       echo '<th>Rate</th>';
       echo '<th>DISC.</th>';
       echo '<th>Amount</th>';
       echo '<th>CGST</th>';
       echo '<th>SGST</th>';
       echo '<th>IGST</th>';
       echo '<th >Net Amount</th>';
       echo '<th >Action</th>';
       echo '</tr>';
       echo '</thead>';
       echo '<tbody>';
       while($rowss=mysqli_fetch_assoc($result1)){
                echo '<tr>';
                echo '<td align="center"  class="itemname">'.$rowss['itemname'].'</td>';
                $gateentryid=$rowss['gateentryid'];
                $qty3=mysqli_fetch_assoc(mysqli_query($con,"SELECT SUM(qty) x FROM quality WHERE gateentryid='$gateentryid'"))['x'];
                echo $rowss['id'];
                echo '<td align="center"  class="qty">'.($rowss['qty']-$qty3).'</td>';
                echo '<td align="center" ><input type="text" class="form-control input-sm" onkeyup="gateqty(this)" value="'.($rowss['qty']-$qty3).'" /></td>';
                echo '<td align="center"  style="display:none;" class="itemcode">'.$rowss['itemcode'].'</td>';
                echo '<td align="center"  class="unit">'.$rowss['unit'].'</td>';
                echo '<td align="center"  class="rate">'.$rowss['rate'].'</td>';
                echo '<td align="center"  style="display:none;" class="descper">'.$rowss['descper'].'</td>';
                echo '<td align="center"  class="descamt" >'.$rowss['descamt'].'</td>';
                echo '<td align="center"  class="amount">'.$rowss['amount'].'</td>';
                echo '<td align="center"  class="cgstamt">'.$rowss['cgstamt'].'</td>';
                echo '<td align="center"  class="sgstamt">'.$rowss['sgstamt'].'</td>';
                echo '<td align="center"  class="igstamt">'.$rowss['igstamt'].'</td>';
                echo '<td align="center"  class="cgstper" style="display:none;">'.$rowss['cgstper'].'</td>';
                echo '<td align="center"  class="sgstper" style="display:none;">'.$rowss['sgstper'].'</td>';
                echo '<td align="center"  class="igstper" style="display:none;">'.$rowss['igstper'].'</td>';
                echo '<td align="center"  class="netamount">'.$rowss['netamount'].'</td>';
                echo '<td><button class="btn btn-sm btn-block btn-success" data-ids="'.$rowss['id'].'" onclick="gateentry(this)">Confirm</button></td>';
                echo '</tr>';
        }
                echo '</tbody>';
                echo '</table>';
       ?>
        </div>
       
        </div>
                <?php
            }
        ?>
</div>


<?php 
include('footer.php');
?>
<script>

function gateqty(e){

var qty=parseFloat($(e).val());
var qty1=parseFloat($(e).parent().parent().find('.qty').text());
if(qty<=qty1){
    var rate=parseFloat($(e).parent().parent().find('.rate').text());
var descper=parseFloat($(e).parent().parent().find('.descper').text());
var cgstper=parseFloat($(e).parent().parent().find('.cgstper').text());
var sgstper=parseFloat($(e).parent().parent().find('.sgstper').text());
var igstper=parseFloat($(e).parent().parent().find('.igstper').text());

var amountbefaor=rate*qty;
var descamt=parseFloat(descper)*amountbefaor/100;
var amount=amountbefaor-descamt;
var cgstamt=cgstper*amount/100;
var sgstamt=sgstper*amount/100;
var igstamt=igstper*amount/100;
var netamount=amount+cgstamt+sgstamt+igstamt;

$(e).parent().parent().find('.descamt').text(descamt.round(2));
$(e).parent().parent().find('.amount').text(amount.round(2));
$(e).parent().parent().find('.cgstamt').text(cgstamt.round(2));
$(e).parent().parent().find('.sgstamt').text(sgstamt.round(2));
$(e).parent().parent().find('.igstamt').text(igstamt.round(2));
$(e).parent().parent().find('.netamount').text(netamount.round(2));
}else{
alert('Quantity Wrong');
$(e).val(qty1);
gateqty(e);
}
}

function gateentry(e){
    var qty=$(e).parent().parent().find('input').val();  
    var ids=$(e).data('ids');
    if (confirm("Are You Sure TO Confirm Gate Entry?")) {
        $.ajax({
            type: "POST",
            data: 'qty=' + qty + '&ids=' + ids,
            url: 'api/quality.php',
            cache: false,
            success: function (res) {
                if (res.status == 'success') {
                    //window.location.reload();
                }
            }
        })
    }
}
</script>