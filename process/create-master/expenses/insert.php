<?php 
    
    $base='../../../';
    
    include($base.'_in/connect.php');

    header('content-type: application/json; charset=utf-8');

    header("access-control-allow-origin: *");

    if(isset($_POST['data'])) {

        if (session_status()==PHP_SESSION_NONE) { session_start(); }
    
        $created_by     = $_SESSION['employeeid'];
    
        $con            = _connect();
    
        $data           = get_object_vars(json_decode($_POST["data"]));

        $branchid       = _clean($con, $data['branchid']);
        
        $expense_id     = _clean($con, $data['expense_id']);
        
        $payment_date   = _clean($con, $data['payment_date']);
        
        $payment_mode   = _clean($con, $data['payment_mode']);
        
        $transaction_no = _clean($con, $data['transaction_no']);
        
        $ac_head_from   = _clean($con, $data['ac_head_from']);
        
        $ac_head_to     = _clean($con, $data['ac_head_to']);
        
        $reference      = _clean($con, $data['reference']);
        
        $note           = _clean($con, $data['note']);
        
        $payment_now    = _clean($con, $data['payment_now']);
 
        $results        = mysqli_fetch_assoc(mysqli_query($con, "SELECT * FROM expense WHERE expense_id='$expense_id'"));
    
        if($results) {

            $new_expense_id = GetExpensesNo($con,$branchId);

            $create = mysqli_query($con, "INSERT INTO expense (branchid, expense_id, payment_date, payment_mode, transaction_no, ac_head_from, ac_head_to, reference, note, payment_now, created_by, created_time) VALUES ('$branchid', '$new_expense_id', '$payment_date', '$payment_mode', '$transaction_no', '$ac_head_from', '$ac_head_to', '$reference', '$note', '$payment_now', '$created_by','$CURRENT_MILLIS')");
    
        } else {
        
            $create = mysqli_query($con, "INSERT INTO expense (branchid, expense_id, payment_date, payment_mode, transaction_no, ac_head_from, ac_head_to, reference, note, payment_now, created_by, created_time) VALUES ('$branchid', '$expense_id', '$payment_date', '$payment_mode', '$transaction_no', '$ac_head_from', '$ac_head_to', '$reference', '$note', '$payment_now', '$created_by','$CURRENT_MILLIS')");
    
        }

        if($create) {
    
            echo '{"status":"success"}';

        } else {
        
            echo '{"status":"falid1"}';
    
        }
    
        _close($con);
    
        echo trim(ob_get_clean());

    } else {
    
        echo '{"status":"falid3"}';

    }

?>