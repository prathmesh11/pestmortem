<?php 
    
    $base='../../../';
    
    include($base.'_in/connect.php');

    header('content-type: application/json; charset=utf-8');

    header("access-control-allow-origin: *");

    if(isset($_POST['data'])) {

        if (session_status()==PHP_SESSION_NONE) { session_start(); }
    
        $created_by     = $_SESSION['employeeid'];
    
        $con            = _connect();
    
        $data           = get_object_vars(json_decode($_POST["data"]));

        $ids            = _clean($con, $data['entryId']);

        $branchid       = _clean($con, $data['branchid']);
        
        $expense_id     = _clean($con, $data['expense_id']);
        
        $payment_date   = _clean($con, $data['payment_date']);
        
        $payment_mode   = _clean($con, $data['payment_mode']);
        
        $transaction_no = _clean($con, $data['transaction_no']);
        
        $ac_head_from   = _clean($con, $data['ac_head_from']);
        
        $ac_head_to     = _clean($con, $data['ac_head_to']);
        
        $reference      = _clean($con, $data['reference']);
        
        $note           = _clean($con, $data['note']);
        
        $payment_now    = _clean($con, $data['payment_now']);

        $Update         = mysqli_query($con, "UPDATE expense SET payment_date = '$payment_date', payment_mode = '$payment_mode', transaction_no = '$transaction_no', ac_head_from = '$ac_head_from', ac_head_to = '$ac_head_to', reference = '$reference', note = '$note', payment_now = '$payment_now', updated_by = '$created_by', updated_time = '$CURRENT_MILLIS' WHERE branchid = '$branchid' AND expense_id = '$expense_id' AND id = '$ids'");
    
        if($Update) {
    
            echo '{"status":"success"}';

        } else {
        
            echo '{"status":"falid1"}';
    
        }
    
        _close($con);
    
    } else {
    
        echo '{"status":"falid3"}';

    }

?>