<?php
    $base='../../../';
    $js='<script src="'.$base.'js/fwork.js"></script>
    <script src="'.$base.'js/alart.js"></script>
    <script src="'.$base.'js/list.min.js"></script>';
    $css='<link rel="stylesheet" href="'.$base.'css/fwork.css">
    <link rel="stylesheet" href="'.$base.'css/alart.css">';
    include($base.'_in/header.php');
    include($base.'_in/connect.php');

    if (!in_array("Country Master", $arrysession)) {
   
        echo "<script>window.location.href='/process/dashboard.php';</script>";
       
        exit;
    
    }

    $con=_connect();
?>
<style>
    th {
        background: #8e44ad;
        color: #fff;
        text-align: center;
        padding-top: 2px;
        padding-bottom: 2px;
        border: 1px solid #fff;
    }
</style>
<div class="container-fluid">
    <div class="content" id="section">
        <h2 align="center" style="margin-top:0px;">Country Master</h2>
        <div style="clear:both;"></div>
        <hr class="style-hr">
        
        <div class="col-sm-12">
                <div class="col-sm-3"></div>
                <div class="col-sm-1"></div>
                <div class="col-sm-2">Country Code</div>
                <div class="col-sm-3 form-group">
                <input type="text" data-role='text' data-name='countrycode'class="form-control input-md">
                </div>
        <div class="col-sm-3"></div>
        </div>
        <div class="col-sm-12">
                <div class="col-sm-3"></div>
                <div class="col-sm-1"></div>
                <div class="col-sm-2">Country Name</div>
                <div class="col-sm-3 form-group">
                    <input type="text" data-role='text' data-name='countryname' class="form-control input-md">
                    <input type="hidden" id="id" data-name="id"/>
                </div>
                <div class="col-sm-3"></div>
        </div>
        <div class="col-sm-12">
            <div class="col-sm-3"></div>
            <div class="col-sm-3">
                <button class="btn btn-success btn-sm btn-block" id="btn-submit" onclick="submit()" style="margin:5px;">Submit</button>
            </div>
            <div class="col-sm-3">
                <button class="btn btn-danger btn-sm btn-block" id="btn-reset"style="margin:5px;">Reset</button>
            </div>
            <br><br><br>
        </div>
        <hr style="margin-top:20px;">
        <div class="col-sm-12">
        <div class="col-sm-2"></div>
        <div class="col-sm-8">
        <table class="table table-center" id="table" style="border: 1px solid #ddd;">
            <thead>
                <tr>
                <th width="20">Sr. No.</th>
                <th width="20">Country Code</th>
                <th width="50">Country Name</th>
                <th width="20">Action</th>
                </tr>
            </thead>
            <tbody>
            <?php
		        $count=0;
		          $result=mysqli_query($con,"SELECT * FROM countrymaster ORDER BY countryname ASC");
		           while($rows = mysqli_fetch_assoc($result)){
		                $count++;
		                echo '<tr>';
		                    echo '<td align="center">'.$count.'</td>';
                            echo '<td>'.$rows['countrycode'].'</td>';
                            echo '<td>'.$rows['countryname'].'</td>';
                            echo '<td align="center"><button data-id="'.$rows['id'].'" onclick="edit(this)" class="btn-primary btn update">Edit</button></td>';
		                echo '<tr>';
		          }
		      ?>
            </tbody>
        </table>
        </div>
        <div class="col-sm-2"></div>
        </div>
    </div>
</div>

<?php
include($base.'_in/footer.php');
?>
<script>
    function submit() {
		var valid = true;
        if (checker('section') != false) {
            valid = valid * true;
        } else {
            valid = valid * false;
        }
        if (valid) {
            var data = checker('section');
            data = JSON.stringify(data);
            $.ajax({
        		type: "POST",
        		data: {
                  data: data
        		},
				url: 'insert.php',
				cache: false,
				success: function (res) {
                  	if(res.status=='success'){
                      window.location.reload();
                  	}
		    	}
			});
        }
    }
    function edit(e){
      $("#btn-submit").attr('onclick','update()');
      $("#btn-submit").text('Update');
      var id=$(e).data('id');
      $.ajax({
			type: "POST",
			data: 'id='+id,
			url: 'select.php',
			cache: false,
			success: function (res) {
          if(res.status=='success'){
            $('#id').val(id);
            modals.putvalue('section',res.json);
          }
	            }
        });
    }
    function update() {
        var valid = true;
        if (checker('section') != false) {
            valid = valid * true;
        } else {
            valid = valid * false;
        }
        if (valid) {
            var data = checker('section');
              data = JSON.stringify(data);
              console.log(data);
              $.ajax({
				type: "POST",
				data: {
          		data: data,
				},
				url: 'update.php',
				cache: false,
				success: function (res) {
	              if(res.status=='success'){
	                  window.location.reload();
	              }
		        }
	        });
        }
    }


</script>
