<?php 
    $base='../../../';

    include($base.'_in/connect.php');

    header('content-type: application/json; charset=utf-8');

    header("access-control-allow-origin: *");

    if(isset($_POST['data'])) {

        if (session_status()==PHP_SESSION_NONE) { session_start(); }
    
        $created_by  = $_SESSION['employeeid'];
    
        $con         = _connect();
    
        $data        = get_object_vars(json_decode($_POST["data"]));

        $ids         = _clean($con, $data['entryId']);
    
        $branchid    = _clean($con, $data['branchid']);
    
        $head_name   = _clean($con, strtoupper($data['head_name']));
    
        $employee_id = _clean($con, $data['employee_id']);
    
        $head_type   = _clean($con, $data['head_type']);

        $results     = mysqli_fetch_assoc(mysqli_query($con, "SELECT head_name x FROM accounthead WHERE head_name='$head_name' AND id<>'$ids'"))['x'];
    
        if( !$results) {
            
            $update      = mysqli_query($con, "UPDATE accounthead SET head_name='$head_name', employee_id='$employee_id',head_type='$head_type', updated_by = '$created_by', updated_time = '$CURRENT_MILLIS' WHERE id='$ids' AND branchid = '$branchid'");
        
            if($update) {
            
                echo '{"status":"success"}';
            
            } else {
              
                echo '{"status":"falid1"}';
            
            }

        } else {
              
            echo '{"status":"falid2"}';
            
        }
          
        _close($con);

    } else {
    
        echo '{"status":"falid3"}';

    }

?>