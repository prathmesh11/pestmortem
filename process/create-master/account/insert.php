<?php 
    
    $base='../../../';
    
    include($base.'_in/connect.php');

    header('content-type: application/json; charset=utf-8');

    header("access-control-allow-origin: *");

    if(isset($_POST['data'])) {

        if (session_status()==PHP_SESSION_NONE) { session_start(); }
    
        $created_by  = $_SESSION['employeeid'];
    
        $con         = _connect();
    
        $data        = get_object_vars(json_decode($_POST["data"]));
    
        $branchid    = _clean($con, $data['branchid']);
    
        $head_name   = _clean($con, strtoupper($data['head_name']));
    
        $employee_id = _clean($con, $data['employee_id']);
    
        $head_type   = _clean($con, $data['head_type']);

        $results     = mysqli_fetch_assoc(mysqli_query($con, "SELECT head_name x FROM accounthead WHERE head_name='$head_name'"))['x'];
    
        if( !$results) {
    
            $create = mysqli_query($con, "INSERT INTO accounthead (branchid, head_name, employee_id, head_type, created_by, created_time) VALUES ('$branchid', '$head_name', '$employee_id', '$head_type', '$created_by','$CURRENT_MILLIS')");
    
            if($create) {
    
                echo '{"status":"success"}';
    
            } else {
            
                echo '{"status":"falid1"}';
        
            }
    
        } else {
        
            echo '{"status":"falid2"}';
    
        }
    
        _close($con);
    
        echo trim(ob_get_clean());

    } else {
    
        echo '{"status":"falid3"}';

    }

?>