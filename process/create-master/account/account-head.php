<?php
    $base='../../../';
    $js='<script src="'.$base.'js/fwork.js"></script>
    <script src="'.$base.'js/alart.js"></script>
    <script src="'.$base.'js/list.min.js"></script>';
    $css='<link rel="stylesheet" href="'.$base.'css/fwork.css">
    <link rel="stylesheet" href="'.$base.'css/grid.min.css">
    <link href="'.$base.'/css/select2.min.css" rel="stylesheet" />

    <link rel="stylesheet" href="'.$base.'css/alart.css">';

    include($base.'_in/header.php');
    include($base.'_in/connect.php');

    if (!in_array("Account Head", $arrysession)) {
   
        echo "<script>window.location.href='/process/dashboard.php';</script>";
       
        exit;
    
    }

    $con=_connect();

    if (session_status()==PHP_SESSION_NONE) { session_start(); }
        
    $branchid = $_SESSION['branchid'];

?>
<style>
    hr.style-hr {
        border: 0;
        height: 1px;
        background-image: linear-gradient(to right, rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.75), rgba(0, 0, 0, 0));
    }

    .table-list td,
    .table-list th {
        border: 1px solid #ddd;
        padding: 1px !important;
        font-size: 13px;
        text-align:center;
    }
   .table-list td{
	padding-top: 10px !important;

   }

   .table-list tr:nth-child(even) {
       background-color: #f2f2f2;
   }

   .table-list th {
       padding-top: 5px;
       padding-bottom: 5px;
       text-align: center;
       background-color: #16a085;
       color: white;
   }

   .sidenav1 {
       height: 100%;
       width: 0;
       position: fixed;
       z-index: 1;
       top: 0;
       left: 0;
       background-color: #111;
       overflow-x: hidden;
       transition: 0.5s;
       padding-top: 60px;
   }

   .sidenav1 a {
       padding: 8px 8px 8px 32px;
       text-decoration: none;
       font-size: 25px;
       color: #818181;
       display: block;
       transition: 0.3s;
   }

   .sidenav1 a:hover {
       color: #f1f1f1;
   }

   .sidenav1 .closebtn1 {
       position: absolute;
     
       right: 25px;
       font-size: 36px;
       margin-left: 50px;
   }

   @media screen and (max-height: 450px) {
       .sidenav1 {
           padding-top: 15px;
       }

       .sidenav1 a {
           font-size: 18px;
       }
   }

   .list {
       padding: 0px;
   }

   .list li span {
       display: inline;
   }

   .list li {
       text-decoration: none;
       display: block;
       padding: 2px;
       background: #FFF;
       border: 1px solid #333;
       color: #000;
   }
   input.largerCheckbox { 
            width: 20px; 
            height: 20px; 
        } 
</style>
<div class="container-fluid">

    <div class="content" id="section">

    <input type="hidden" class="form-control input-sm " data-name="branchid" value="<?php echo $branchid;?>">

    <input type="hidden" class="form-control input-sm " data-name="entryId" id="id">

        <span style="font-size:24px;cursor:pointer" onclick="openNav1()">&#9776; Search</span>

        <div id="mysidenav1" class="sidenav1" style="margin-left:80px;">

            <div id="test-list" style="padding:5px;">

                <a href="javascript:void(0)" class="closebtn1" onclick="closeNav1()">&times;</a>

                <br><br><br>

                <div class="form-group">

                    <input type="text" placeholder="Search" class="form-control input-sm fuzzy-search"
                        style="border-radius:3px;">

                </div>

                <ul class="list">

                    <?php 

                        $result2=mysqli_query($con,"SELECT * FROM accounthead ORDER by id DESC");

                        while($rows = mysqli_fetch_assoc($result2)){

                            echo '<li><span class="head_name">' .$rows['head_name']. '<span><button style="margin-left:2px;" data-id="' .$rows['id']. '" onclick="edit(this)" class="btn btn-primary btn-sm"><span class="fa fa-edit"></span> E</button></i>';

                        }

                    ?>

                </ul>

            </div>

        </div>

        <h2 align="center" style="margin-top:0px;">Account Head</h2>

        <div style="clear:both;"></div>

        <hr class="style-hr">

        <div class="row">
                        
            <div class="col-sm-3"></div>
            
            <div class="col-sm-6">
            
                <div class="col-sm-4">Head Name</div>
            
                <div class="col-sm-8">
            
                    <div class="form-group">
            
                        <input type="text" data-role="text" data-name="head_name" class="form-control input-sm">
            
                    </div>
            
                </div>
            
            </div>

            <div class="col-sm-3"></div>
            
            <div style="clear:both;"></div>
            
            <div class="col-sm-3"></div>
            
            <div class="col-sm-6">
            
                <div class="col-sm-4">Employee Name</div>
            
                <div class="col-sm-8">
            
                    <div class="form-group">
                        
                        <select class="form-control input-sm select-js" data-name="employee_id">
                            
                            <option value="">Select Employee Name</option>

                            <?php
 
                                $result = mysqli_query($con,"SELECT employeeid,username FROM employeemaster WHERE branchid = '$branchid' ORDER BY username ASC");

                                while ($row = mysqli_fetch_assoc($result)) {

                                    echo '<option value="'.$row['employeeid'].'">'.$row['username'].'</option>';
                                }

                            ?>

                        </select>
            
                    </div>
            
        
                </div>
            
            </div>
            
            <div class="col-sm-3"></div>

            <div style="clear:both;"></div>
            
            <div class="col-sm-3"></div>
            
            <div class="col-sm-6">
            
                <div class="col-sm-4">Head Type</div>
            
                <div class="col-sm-8">
            
                    <div class="form-group">
            
                        <select class="form-control input-sm" data-role="select" data-name="head_type">
            
                            <option value="">Select Head Type</option>
            
                            <option value="Income">Income</option>
            
                            <option value="Expenses">Expenses</option>

                            <option value="General">General</option>

                            <option value="Cash Account">Cash Account</option>

                            <option value="Bank Account">Bank Account</option>

                        </select>
            
                    </div>
            
                </div>
            
            </div>

            <div class="col-sm-3"></div>

            <div style="clear:both;"></div>
            
            <hr class="style-hr">
            
            <div class="col-sm-3"></div>
            
            <div class="col-sm-6">
            
                <div class="col-sm-6">
                
                    <button class="btn btn-success btn-sm btn-block" id="btn-submit"
                        style="margin:5px;" onclick="submit()">Submit</button>
                
                </div>
                
                <div class="col-sm-6">
                    
                    <button class="btn btn-danger btn-sm btn-block" id="btn-reset"
                        style="margin:5px;">Reset</button>
                
                </div>
            
            </div>
            
            <div class="col-sm-3"></div>

        </div>

    </div>

</div>


     



<?php
include($base.'_in/footer.php');
?>
<script>

$(document).ready(function() {
    $('.select-js').select2({width: '100%', tags: true});

    $('.select').attr('style','width:100%!important;');
});

function openNav1() {
    document.getElementById("mysidenav1").style.width = "300px";
}

function closeNav1() {
    document.getElementById("mysidenav1").style.width = "0";
}

var monkeyList = new List('test-list', {
    valueNames: ['head_name']
});



//--------------------- Insert Function Start -------------------- //              

function submit() {

    var valid    = true;

    if (checker('section') != false) {

        valid = valid * true;

    } else {

        valid = valid * false;

    }

    if (valid) {

        data        = checker('section');

        var datastr = JSON.stringify(data);

        $.ajax({
            type: "POST",
            data: {
                    data: datastr
                },
            url: 'insert.php',
            cache: false,
            success: function (res) {
                if (res.status == 'success') {

                    swal({
                        type: 'success',
                        title: 'Head Add',
                        showConfirmButton: false,
                        timer: 1000
                    });
                    setTimeout(function () {
                        window.location.reload();
                    }, 1000);


                } else if (res.status == 'falid2') {

                    swal(
                        'Cancelled',
                        'Duplicate Head Name :)',
                        'error'
                    )

                }
            }
        })
    }
}

//--------------------- Insert Function End -------------------- //  


//--------------------- Edit Function Start -------------------- //              

function edit(e) {

    var id = $(e).attr('data-id');

    $.ajax({

        type: "POST",
        
        data: 'id=' + id,
        
        url: 'select.php',
        
        cache: false,
        
        success: function (res) {
        
            if (res.status == 'success') {
        
                modals.putvalue('section', res.json);

                $('#id').val(res.json[0].id);

                closeNav1();

                $('#btn-submit').attr('onclick', 'update()');

                $('#btn-submit').text('Update');

            } else {


            }

        }

    })

}

//--------------------- Edit Function End -------------------- //  

//--------------------- Update Function Start -------------------- //  

function update() {

    var valid = true;

    if (checker('section') != false) {

        valid = valid * true;

    } else {

        valid = valid * false;

    }

    if (valid) {

        data        = checker('section');
        
        var datastr = JSON.stringify(data);

        $.ajax({
            
            type: "POST",
            
            data: {

                data: datastr

            },

            url: 'update.php',
            
            cache: false,
            
            success: function (res) {
            
                if (res.status == 'success') {

                    swal({
                        type: 'success',
                        title: 'Document Update',
                        showConfirmButton: false,
                        timer: 1000
                    });
                    setTimeout(function () {
                        window.location.reload();
                    }, 1000);


                } else if (res.status == 'falid2') {

                    swal(
                        'Cancelled',
                        'Duplicate Head Name :)',
                        'error'
                    )

                }
            }
        })
    }
}

//--------------------- Update Function End -------------------- // 



</script>