<?php 
$base='../../../';
$css = '<link rel="stylesheet" href="'.$base.'css/classic.css">
<link rel="stylesheet" href="'.$base.'css/classic.date.css">
<link rel="stylesheet" href="'.$base.'css/classic.time.css">
<link href="'.$base.'/css/select2.min.css" rel="stylesheet" />
<script src="'.$base.'js/alart.js"></script>
<link rel="stylesheet" href="'.$base.'css/grid.min.css">';

$js = '<script src="'.$base.'js/picker.js"></script>
<script src="'.$base.'js/picker.date.js"></script>
<script src="'.$base.'js/picker.time.js"></script>
<script src="'.$base.'js/pdfmake.min.js"></script>
<script src="'.$base.'js/vfs_fonts.js"></script>
<script src="'.$base.'/js/select2.min.js"></script>
<link rel="stylesheet" href="'.$base.'css/alart.css">
<script src="'.$base.'js/fwork.js"></script>';

include($base.'_in/header.php');
include($base.'_in/connect.php');

if (!in_array("Salary Master", $arrysession)) {
   
    echo "<script>window.location.href='/process/dashboard.php';</script>";
   
    exit;

}

$con=_connect();
if (session_status() == PHP_SESSION_NONE) { session_start(); }
$branchid = $_SESSION['branchid'];
$json1='';
$result=mysqli_query($con,"SELECT * FROM employeemaster ORDER BY username ASC");
while($rows = mysqli_fetch_assoc($result)){
    $phone=get_object_vars(json_decode($rows['employeejson']))['phone'];
    $email=get_object_vars(json_decode($rows['employeejson']))['email'];
   
    $json1.=',{"phone":"'.$phone.'","email":"'.$email.'","branchid":"'.$rows['branchid'].'","employeeid":"'.$rows['employeeid'].'","username":"'.$rows['username'].'"}';
}
$json1=substr($json1,1);
$json1='['.$json1.']';
?>

<style>
 .picker__select--year{
        height:auto;
    }
    .picker__select--month{
        height:auto;
    }

    .table-list td,
   .table-list th {
       border: 1px solid #ddd;
       padding: 2px;
       font-size: 13px;
   }

   .table-list tr:nth-child(even) {
       background-color: #f2f2f2;
   }

   .table-list th {
       padding-top: 12px;
       padding-bottom: 12px;
       text-align: left;
       background-color: #16a085;
       color: white;
   }
</style>

<input type="hidden" id="inp-branch" value='<?php echo $branchid; ?>' />
<input type="hidden" id="item-json1" value='<?php echo str_replace("'"," &#39;",$json1); ?>' />

    <div class="container-fluid" style="background:#eee;">
            <div class="row content">
                <div class="col-sm-2" id="section0">
                    <div class="row form-group" style="border-radius:4px;border:1px solid #CCC;background:#FFF;padding-top:10px;-webkit-box-shadow: 2px 2px 3px 3px #ccc; -moz-box-shadow:2px 2px 3px 3px #ccc;box-shadow:2px 2px 3px 3px #ccc;">
                        <div class="col-sm-12" align="center"><h4>Salary Master</h4></div>
                        <div class="col-sm-12" align="center">
                        <select class="form-control input-sm select-js" data-role="select" data-name="branchid" id="branchid" onchange="empmaster(this)">
                        <option value="Select">Select</option>
                            <?php 
                                    $result1=mysqli_query($con, "SELECT * FROM `branchmaster`");
                                    while($rows=mysqli_fetch_assoc($result1)) {
                                    echo '<option value="'.$rows['branchid'].'">'.$rows['branchname'].'</option>';
                                }
                            ?>
                        </select>
                        </div>
                        <div class="col-sm-12" align="center"><h4>
                        <select class="form-control input-sm select-js" data-role="select" id="employeeid" data-name="employeeid" onchange="empdetails(this)">
                            <option value="Select">Select</option>
                        </select>
                        </div>
                        <div class="col-sm-12 img-class" align="center" ><img id="img" class="img-responsive" style="width:170px;height:170px;border:1px solid #CCC;" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMgAAADICAIAAAAiOjnJAAAACXBIWXMAAAsSAAALEgHS3X78AAAJ00lEQVR42u2dXXOTXBeGcScQSkqTZog1GbWOdvTI//83nPFQR20aQyMpLYHSncCmzwFvO33TJiUpJOzFfR046jiVwMW91v6AvLq9vVUAyBuGUwAgFoBYAGIBALEAxAIQCwCIBSAWgFgAQCxQduo4BQtEURRFkRCCc776XzabTUVRVFVVVRXnbYFXWITmnIdhyDmfzWbPyrQMVVV1XW80Gs1ms9Fo1Go1iFVRscIw9H3f9/0oinL/4YZhmKZpmmZlw6xyYgkhPM9zXbcIn540rN1ut1otiEVZKdd1XddNkmTL/7Wqqt1ut1J6VUUs3/dHo9H2lVrQq9/vG4YBsYgElW3bvu+X5Hg6nY5lWeS7e+Jicc6Hw+F22qns6Lre6/V0XYdYUuJ53vn5+W7L3zIYY8fHx4TdIiuW53mj0ajkB9nv96l29DSXdNJWvfzHORqNNp6ShVg76KuksCrl9PSUpFvUxBJCDIfDcvZVT5IkiW3bQgiIVWps2y7bGDBLxNq2DbHKi+u65ZmvWrcplPTI6YslhHAcR97jH4/HlAoiHbHG47FErdVjoihyXRdile6qeJ5HoJSTCS0iYkldBB+OEMmEFgWxaMQVsdCiIBaNuLoPLRo3ifRipTtCKQ3UaVRD6cUiZlVa2Qks8kAsfCiI9QjOOckVXAKz8AwXANUQYi0SBIFClDAMIRZua4QxIbFkv6eRWCXl+vpaIY3UbiGxSj3mhVg7aLCk2ykKsXDSy3LzQCyIhVpPQizyDZbsoYXEglgQ6w4hhNTb26tw/0gp1mw2U6qBvLtJGe5j3EIQS/r7GImF+xggsegi76wKeiwAse6oyFwDxAKo+/KLVZHFHNlHKkgsALEAxMKQEGKhmQUQC0AsALEAqLxY5B/OgVgQC6AUAogFIBYAEAtALHCPqqoQC0AsiAUgFoBYKA25Xh7GIBbEyh9d1yEWABAL8QyxFmg2mxALYoEXUavVIBZuYjTvEAsfFmJRuo8hFk437p/qidVoNBBXEAu3chXvH4hVXqSesZO1x5J3dRaJhbt5xzePvLOjEotlGAZtsWT/gBALkQyx/r9/pz2bhcTCqS+kwZL9tpFYLNM0qYpF4KPJLRbVSQcCYcxwZ5fukjCGxIJY+FBExaJXDdvtNsTCZch/PEhjtCu9WJ1Oh5JYrVaLSKdI4Ban1GmRCWAKDQqZ0Gq1WmSWEyiIZRgGjb6EUlknMqQiUEEMw6C0gZGIWASKSLfbpTQKoTMJJPWFabVaxNbU6Ygl9bUhFlcKsXc3SHp5Op0Ovb1lpMSScXjIGLMsSyEHtYW2fr8vXcpK/dBEVcRSVVWi2SBd14ktSZEVS1EUy7Jk2fLQ6/UUohAUq1arSVEQO50O4Ue6aW7tNU2z5CvTqqqS7NmJi5VWmTIXxLdv35Ls2emLVeaCaFkW+feaUH61hmmaJRxz7e3t0Ztnr5ZYiqIcHR1pmlae44njuCJvjaP/MqD3798nSVKGI0mSZDweX15eQiwKpN96Xwa3HMeZz+cQiwicc8YY53y3bjmOE4ahoijT6XQ+n0Ms6Ukvp2EYl5eXu3LLcZwgCNLf397eXlxcQCzpub6+vh8k2ra9ZbeSJHloVcpoNIJY0tfBe5M0TWs2m2dnZ1urREmS2La9YJWiKJPJRAgBsSTG87yHf2y3241G4+/fv9PpdAtOL5NYCPHv3z+IJTFXV1cLf2NZVq1Wu7i4sG07juOCgir9+SvK7nA4hFgSx9XjS1uv19PV3zRRLi4u8u26giA4Ozt7NhEnk8nNzQ3hk//q9vaW6mf7+fNnOon1mOl0ej80Y4wZhnF4eFiv11+SUkEQeJ6XPQU/ffr05csXqie/TvWDeZ63zCpFUQ4ODubzue/7904EQaBpmmmauq5nXwVKkiQMw5ubm8cd+rMMBoOTkxOqexxoJpYQ4vfv3yvEuq9HqVuL/QFjmqbt7e0pT329ShzHcRzPZrM4jl84wDw5Ofn8+TMSSxomk8mzVimK0ul05vP5bDZ7nEOcc8550cf558+fDx8+lGqZHM37UsIwdF0304dn7M2bNzt8VX8cxz9+/MCoUI4iuNa8NmPs9evXO9zHfHp6SnJZmppYg8EgSxFcwLKsw8PDXR3zt2/f6E3EkxJrNBpt3Bi12+2jo6OdjNFubm6+f/8Oscpr1cICzroYhtHv99PB4PYP/tevX5huKF1fNR6PX2jVQ6bT6dXV1fbL09evX9+9ewexymLVYDDIfWogSRLXdZ+c5SqCvb29drut63q/36fx4mS5xQrD8OzsrLgtVnEcX11dFadX+lb3g4ODh6tJNNySVSwhxGQyyThf9XK90jWfDcaby3zSdX3FS5darZZ0r82hIJbneefn59vfZzyfz4Mg4Jw/nqx/llqtpmla+gbbLFPtuq73ej15n2uVTCzP8xzHySs5XtKBzefzdHtqulwYx/H9UaUOKYpSr9fr9bqmaemvG/xHlmV1Oh0ZF6rlEEsI4Xme67o7V2r7pOtO+/v7culVarGEEEEQ+L6/tdFZmfVqt9sSva20jGJFURSGIXxa1nvt7++n+8YgVqZwCu/Ywn4VGhnWbDYbjUb6a9kK5S7Fgkw5kn7xvWEYmqapqrpz1bYtFmTaZqTpuq7rejpttmXVtiQW59z3/dQnXPIdplpqWLPZLPqF+MWK5d9RkhcJgYekb2otaCKjELE45+kKLnySomKaptntdvOdyMhZrHQaE82TjLRarRy3OuYmVkkWW8AL06vb7eby4tYcxOKc27aNlKLUe/V6vRdG10vFchxnMpngYhDj5XsrNhdLCDEcDjF9QLgsHh8fb+zWhmKh/MGt/MXinJ+enmIqoSJuffz4cYOZiLUf/xJCwKrqkCTJcDjc4IEltq5Vg8EAVlUKzvl4PC5WLPRV1cTzvHX3xq0h1jafswNlYzwer1UQs4oVRZHjODi/lSWKorUmLLOKNRqN0FpVnLUeZskkFvZRgZTsVSuTWFX4ig6QsYvPGFosx58FEFpriIWeHTzE9/0sw0P2bHeFuAIPSZIky6vI2LMDAZxKsIEVq8SKoggzouBJMZ5dgFkl1uOvzgLgfki3uVg5vtUT0GvhNxSLc462HWxcDRniChQRWmzjrAMVZ/U36bFlQYc6CFbDOV8xU8oQV6CI0HpaLOxlAFm4vr5GYoH8WRFADHEFNiaKomVtFlsr3wDIGENILLAtsfCAF8jOMlvY43+HhyZA/omFOghyCS2GOgiKCK1FsTb4wjSAxHpGLCEEEgusy5NhxBBXoPDEwtQoyKvNQmKBQkILiQUKFiuKIkyNgrz6d7aiTAKQQ2JhogHk2L8jsUAhofU/sTA1CgoRCxMNIN9S+B9fledcwXKJ+QAAAABJRU5ErkJggg==" accept="image/*" /></div>
                        <div class="col-sm-12"><br><input type="file" class="form-control" onchange="getfile(this)" name="photo"></div>
                        <div class="col-sm-12"><br><button style="border-radius:0px;" class="btn btn-primary btn-block" onclick="personalui()">PERSONAL DETAILS</button></div>
                        <div class="col-sm-12"><br><button style="border-radius:0px;" class="btn btn-primary btn-block" onclick="official()">OFFICIAL DETAILS</button></div>
                        <div class="col-sm-12"><br><button style="border-radius:0px;" class="btn btn-primary btn-block" onclick="attachment()">ATTACHED DOCUMENTS</button></div>
                        <div class="col-sm-12"><br><button style="border-radius:0px;" class="btn btn-primary btn-block" data-toggle="modal" data-target="#newEmployeeModal">CREATE NEW EMPLOYEE</button><br><br></div>
                        <div class="col-sm-12"><br><button style="border-radius:0px;" class="btn btn-success btn-block" id="submit" onclick="submit()">SUBMIT</button><br></div>
                    </div>
                </div>
                <div class="col-sm-10" id="section1">
                <div class="col-sm-12" align="center" style="border-radius:4px;border:1px solid #CCC;background:#FFF;padding-top:10px;-webkit-box-shadow: 2px 2px 3px 3px #ccc; -moz-box-shadow:2px 2px 3px 3px #ccc;box-shadow:2px 2px 3px 3px #ccc;"><h3 style="margin:5px;" id="details">Personal Details</h3></div>            
                    <div style="clear:both;"></div>
                    <hr style="margin:6px;">
                    <div class="col-sm-6" style="padding-bottom:10px;border-radius:4px;border:1px solid #CCC;background:#FFF;padding-top:10px;-webkit-box-shadow: 2px 2px 3px 3px #ccc; -moz-box-shadow:2px 2px 3px 3px #ccc;box-shadow:2px 2px 3px 3px #ccc;">
                        <div class="col-sm-4" style="margin-top:4px;">Gender</div><div class="col-sm-8" style="margin-top:4px;">
                        
                        <label for="r1"><input type="radio" class="Radio-Input" id="r1" name="gender" data-role="radio" data-name="gender" value="male" checked /> Male</label>
                        <label for="r2"><input type="radio" class="Radio-Input" id="r2" name="gender" data-role="radio" data-name="gender" value="female" /> Female</label>
                        
                        </div>
                        <div class="col-sm-4" style="margin-top:4px;">DOB</div><div class="col-sm-8" style="margin-top:4px;"><input type="text" class="datepicker form-control input-sm" data-role="text" data-name="dob"/></div>
                        <div class="col-sm-4" style="margin-top:4px;">Contact No</div><div class="col-sm-8" style="margin-top:4px;"><input type="text" id="phoneno"  class="form-control input-sm" data-role="number" data-name="contactno" autocomplete="rutjfkde" autofocus /></div>
                        <div class="col-sm-4" style="margin-top:4px;">Email Id</div><div class="col-sm-8" style="margin-top:4px;"><input type="text"  id="emailno" class="form-control input-sm" data-name="email" autocomplete="rutjfkde" autofocus /></div>
                        <div class="col-sm-4" style="margin-top:4px;">Experience</div><div class="col-sm-8" style="margin-top:4px;"><input type="text"  class="form-control input-sm" data-role="text" data-name="experience"autocomplete="rutjfkde" autofocus /></div>
                        <div class="col-sm-4" style="margin-top:4px;">Qualification</div><div class="col-sm-8" style="margin-top:4px;"><input type="text"  class="form-control input-sm" data-role="text" data-name="qualification" autocomplete="rutjfkde" autofocus /></div>
                        <div class="col-sm-4" style="margin-top:4px;">Address</div><div class="col-sm-8" style="margin-top:4px;"><textarea rows="5" type="text"  id="inp-address" class="form-control input-sm" data-role="text" data-name="address" autocomplete="rutjfkde" autofocus ></textarea></div>
                        <div class="col-sm-4" style="margin-top:4px;">Permenant Address</div><div class="col-sm-8" style="margin-top:4px;"><textarea rows="5"  id="inp-paddress" class="form-control input-sm" data-role="text" data-name="paddress" autocomplete="rutjfkde" autofocus></textarea></div>
                        <div class="col-sm-4" style="margin-top:4px;">Language</div><div class="col-sm-8" style="margin-top:4px;"><input type="text"  class="form-control input-sm" data-role="text" data-name="language"autocomplete="rutjfkde" autofocus /></div>
                        <div class="col-sm-4" style="margin-top:4px;">Blood Group</div><div class="col-sm-8" style="margin-top:4px;"><input type="text"  class="form-control input-sm" data-role="text" data-name="blood_group" autocomplete="rutjfkde" autofocus /></div>

                        <div class="col-sm-4" style="margin-top:4px;">Emergency Name</div><div class="col-sm-8" style="margin-top:4px;"><input type="text"  class="form-control input-sm" data-role="text" data-name="emergency_Name"autocomplete="rutjfkde" autofocus /></div>
                        <div class="col-sm-4" style="margin-top:4px;">Emergency Contact</div><div class="col-sm-8" style="margin-top:4px;"><input type="number"  class="form-control input-sm" data-role="text" data-name="emergency_Contact" autocomplete="rutjfkde" autofocus /></div>
                        <div class="col-sm-4" style="margin-top:4px;">Relation</div><div class="col-sm-8" style="margin-top:4px;"><input type="text"  class="form-control input-sm" data-role="text" data-name="emergency_Relation"autocomplete="rutjfkde" autofocus /></div>

                        <div class="col-sm-4" style="margin-top:4px;">Machine Emp. Id.</div><div class="col-sm-8" style="margin-top:4px;"><input type="text" class="form-control input-sm" data-name="machineid" autocomplete="rutjfkde" autofocus/></div>
                    </div>
                    <div class="col-sm-6" style="padding-bottom:10px;border-radius:4px;margin-left:0px;border:1px solid #CCC;background:#FFF;padding-top:10px;-webkit-box-shadow: 2px 2px 3px 3px #ccc; -moz-box-shadow:2px 2px 3px 3px #ccc;box-shadow:2px 2px 3px 3px #ccc;">
                        <div class="col-sm-4" style="margin-top:4px;">Reference By</div><div class="col-sm-8" style="margin-top:4px;"><input type="text" class="form-control input-sm" value="-" data-role="text" data-name="reference" autocomplete="rutjfkde" autofocus/></div>
                        <div class="col-sm-4" style="margin-top:4px;">Category</div><div class="col-sm-8" style="margin-top:4px;">
                        
                        <select class="form-control input-sm" data-role="select" data-name="category" >
                        <option value="Select">Select</option>
                            <?php 
                                    $result1=mysqli_query($con, "SELECT groupCode,groupName FROM `additionalmaster` WHERE  mainGroupName='CATEGORY' ");
                                    while($rows=mysqli_fetch_assoc($result1)) {
                                    echo '<option value="'.$rows['groupCode'].'">'.$rows['groupName'].'</option>';
                                }
                            ?>
                        </select>
                        
                        </div>
                        <div class="col-sm-4" style="margin-top:4px;">Department</div><div class="col-sm-8" style="margin-top:4px;">                        
                        <select class="form-control input-sm" data-role="select" data-name="department" >
                        <option value="Select">Select</option>
                            <?php
                                    $result1=mysqli_query($con, "SELECT groupCode,groupName FROM `additionalmaster` WHERE  mainGroupName='DEPARTMENT' ");
                                    while($rows=mysqli_fetch_assoc($result1)) {
                                    echo '<option value="'.$rows['groupCode'].'">'.$rows['groupName'].'</option>';
                                }
                            ?>
                        </select>
                        </div>
                        <div class="col-sm-4" style="margin-top:4px;">Designation</div><div class="col-sm-8" style="margin-top:4px;">
                        <select class="form-control input-sm" data-role="select" data-name="designation" >
                        <option value="Select">Select</option>
                            <?php 
                                    $result1=mysqli_query($con, "SELECT groupCode,groupName FROM `additionalmaster` WHERE  mainGroupName='DESIGNATION' ");
                                    while($rows=mysqli_fetch_assoc($result1)) {
                                    echo '<option value="'.$rows['groupCode'].'">'.$rows['groupName'].'</option>';
                                }
                            ?>
                        </select>
                        </div>
                        <div class="col-sm-4" style="margin-top:4px;">Company</div><div class="col-sm-8" style="margin-top:4px;">
                        <select class="form-control input-sm" data-role="select" data-name="company" >
                        <option value="Select">Select</option>
                            <?php 
                                    $result1=mysqli_query($con, "SELECT groupCode,groupName FROM `additionalmaster` WHERE  mainGroupName='COMPANY' ");
                                    while($rows=mysqli_fetch_assoc($result1)) {
                                    echo '<option value="'.$rows['groupCode'].'">'.$rows['groupName'].'</option>';
                                }
                            ?>
                        </select>
                        </div>
                        <div class="col-sm-4" style="margin-top:4px;">PAN</div><div class="col-sm-8" style="margin-top:4px;"><input class="form-control input-sm" data-role="text" data-name="pan" autocomplete="rutjfkde" autofocus/></div>
                        <div class="col-sm-4" style="margin-top:4px;">Aadhar Card</div><div class="col-sm-8" style="margin-top:4px;"><input class="form-control input-sm" data-role="text" data-name="aadharcard" autocomplete="rutjfkde" autofocus/></div>
                     
                        <div class="col-sm-4" style="margin-top:4px;">Driving Licence</div><div class="col-sm-8" style="margin-top:4px;"><input class="form-control input-sm"  data-name="driving" autocomplete="rutjfkde" autofocus/></div>
                        <div class="col-sm-4" style="margin-top:4px;">Passport No</div><div class="col-sm-8" style="margin-top:4px;"><input class="form-control input-sm" data-name="passport" autocomplete="rutjfkde" autofocus/></div>
                     
                        <div class="col-sm-4" style="margin-top:4px;">Salary Per</div><div class="col-sm-8" style="margin-top:4px;">
                        <select class="form-control input-sm" data-role="select" data-name="salaryper" >
                            <option value="Select">Select</option>
                            <option value="MONTHLY">MONTHLY</option>
                            <option value="DAILY">DAILY</option>
                        </select>
                        </div>
                        <div class="col-sm-4" style="margin-top:4px;">Payment</div><div class="col-sm-8" style="margin-top:4px;">
                        <select class="form-control input-sm" data-role="select" data-name="payment" >
                            <option value="Select">Select</option>
                            <option value="CASH">CASH</option>
                            <option value="CHEQUE">CHEQUE</option>
                            <option value="BANK TRASFER">BANK TRASFER</option>
                        </select>
                        </div>
                        <div class="col-sm-4" style="margin-top:4px;">Bank Name</div><div class="col-sm-8" style="margin-top:4px;"><input class="form-control input-sm" type="text" data-name="bankname" autocomplete="rutjfkde" autofocus/></div>
                        <div class="col-sm-4" style="margin-top:4px;">Branch Name</div><div class="col-sm-8" style="margin-top:4px;"><input class="form-control input-sm" type="text" data-name="branchname" autocomplete="rutjfkde" autofocus/></div>
                        <div class="col-sm-4" style="margin-top:4px;">IFSC Code</div><div class="col-sm-8" style="margin-top:4px;"><input class="form-control input-sm" type="text" data-name="ifsccode" autocomplete="rutjfkde" autofocus/></div>
                        <div class="col-sm-4" style="margin-top:4px;">Account Number</div><div class="col-sm-8" style="margin-top:4px;"><input class="form-control input-sm" type="text" data-name="accountnumber" autocomplete="rutjfkde" autofocus/><br><br></div>

                        <div class="col-sm-4" style="margin-top:1px;">Smoking</div><div class="col-sm-3" style="margin-top:1px;">
                        <label ><input type="radio" data-role="radio" class="Radio-Input" name="smoking" data-name="smoking" value="yes"  /> YES</label>
                        <label ><input type="radio" data-role="radio" class="Radio-Input" name="smoking" data-name="smoking"  value="no" checked/> No</label>
                        </div>

                        <div class="col-sm-2" style="margin-top:1px;">Tobacco</div><div class="col-sm-3" style="margin-top:1px;">
                        <label ><input type="radio" data-role="radio" class="Radio-Input" name="tobacco" data-name="tobacco" value="yes"  /> YES</label>
                        <label ><input type="radio" data-role="radio" class="Radio-Input" name="tobacco" data-name="tobacco"  value="no" checked/> No</label>
                        </div>

                        <div class="col-sm-4" style="margin-top:1px;">Liquor</div><div class="col-sm-8" style="margin-top:1px;">
                        <label ><input type="radio" data-role="radio" class="Radio-Input" name="liquor" data-name="liquor" value="yes"  /> YES</label>
                        <label ><input type="radio" data-role="radio" class="Radio-Input" name="liquor" data-name="liquor"  value="no" checked/> No</label>
                        </div>


                        <div class="col-sm-12">
                            <center><button type="button" class="btn btn-primary btn-md" data-toggle="modal" data-target="#myModal">Add Family details</button></center>
                            <br>
                            <div class="table-responsive">
                                <table id="contact-table" class="table table-list">
                                    <thead>
                                        <tr>
                                            <th>SR.</th>
                                            <th>Name</th>
                                            <th>DOB</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </div>
                </div>
           
            <div class="col-sm-10" id="section2" style="display:none;">
            <div class="col-sm-12" align="center" style="border-radius:4px;border:1px solid #CCC;background:#FFF;padding-top:10px;-webkit-box-shadow: 2px 2px 3px 3px #ccc; -moz-box-shadow:2px 2px 3px 3px #ccc;box-shadow:2px 2px 3px 3px #ccc;"><h3 style="margin:5px;" id="details">Official Details</h3></div>            
                    <div style="clear:both;"></div>
                    <hr style="margin:6px;">
                        <div class="col-sm-4" id="section22" style="padding-bottom:10px;border-radius:4px;border:1px solid #CCC;background:#FFF;padding-top:10px;-webkit-box-shadow: 2px 2px 3px 3px #ccc; -moz-box-shadow:2px 2px 3px 3px #ccc;box-shadow:2px 2px 3px 3px #ccc;">
                                <div class="col-sm-6" style="margin-top:4px;">Basic</div><div class="col-sm-6" style="margin-top:4px;">                 <input id="inp-text-1" onkeyup="officialchange()" class="form-control input-sm" type="text" value="0" data-role="number" data-name="basic" autocomplete="rutjfkde" autofocus /></div>
                                <div class="col-sm-6" style="margin-top:4px;">DA</div><div class="col-sm-6" style="margin-top:4px;">                    <input id="inp-text-2" onkeyup="officialchange()" class="form-control input-sm" type="text" value="0" data-role="number" data-name="das" autocomplete="rutjfkde" autofocus /></div>
                                <div class="col-sm-6" style="margin-top:4px;">HRA</div><div class="col-sm-6" style="margin-top:4px;">                   <input id="inp-text-3" onkeyup="officialchange()" class="form-control input-sm" type="text" value="0" data-role="number" data-name="hra" autocomplete="rutjfkde" autofocus /></div>
                                <div class="col-sm-6" style="margin-top:4px;">Travelling Allowance</div><div class="col-sm-6" style="margin-top:4px;">  <input id="inp-text-4" onkeyup="officialchange()" class="form-control input-sm" type="text" value="0" data-role="number" data-name="ta" autocomplete="rutjfkde" autofocus /></div>
                                <div class="col-sm-6" style="margin-top:4px;">Medical</div><div class="col-sm-6" style="margin-top:4px;">               <input id="inp-text-5" onkeyup="officialchange()" class="form-control input-sm" type="text" value="0" data-role="number" data-name="madical" autocomplete="rutjfkde" autofocus /></div>
                                <div class="col-sm-6" style="margin-top:4px;">Conveyance</div><div class="col-sm-6" style="margin-top:4px;">            <input id="inp-text-6" onkeyup="officialchange()" class="form-control input-sm" type="text" value="0" data-role="number" data-name="conveyance" autocomplete="rutjfkde" autofocus /></div>
                                <div class="col-sm-6" style="margin-top:4px;">Education</div><div class="col-sm-6" style="margin-top:4px;">             <input id="inp-text-7" onkeyup="officialchange()" class="form-control input-sm" type="text" value="0" data-role="number" data-name="education" autocomplete="rutjfkde" autofocus /></div>
                                <div class="col-sm-6" style="margin-top:4px;">CCA</div><div class="col-sm-6" style="margin-top:4px;">                   <input id="inp-text-8" onkeyup="officialchange()" class="form-control input-sm" type="text" value="0" data-role="number" data-name="cca" autocomplete="rutjfkde" autofocus /></div>
                                <div class="col-sm-6" style="margin-top:4px;">Total Salary</div><div class="col-sm-6" style="margin-top:4px;">          <input id="inp-text-9" onkeyup="officialchange()" class="form-control input-sm" type="text" value="0" data-role="number" data-name="totalsalary" autocomplete="rutjfkde" autofocus readonly /></div>
                                <div class="col-sm-6" style="margin-top:4px;">Mobile</div><div class="col-sm-6" style="margin-top:4px;">                <input id="inp-text-10" onkeyup="officialchange()" class="form-control input-sm" type="text" value="0" data-role="number" data-name="mobile" autocomplete="rutjfkde" autofocus /></div>
                                <div class="col-sm-6" style="margin-top:4px;">Uniform</div><div class="col-sm-6" style="margin-top:4px;">               <input id="inp-text-11" onkeyup="officialchange()" class="form-control input-sm" type="text" value="0" data-role="number" data-name="uniform" autocomplete="rutjfkde" autofocus /></div>
                                <div class="col-sm-6" style="margin-top:4px;">Internet</div><div class="col-sm-6" style="margin-top:4px;">              <input id="inp-text-12" onkeyup="officialchange()" class="form-control input-sm" type="text" value="0" data-role="number" data-name="internet" autocomplete="rutjfkde" autofocus /></div>
                                <div class="col-sm-6" style="margin-top:4px;">Shoes</div><div class="col-sm-6" style="margin-top:4px;">                 <input id="inp-text-13" onkeyup="officialchange()" class="form-control input-sm" type="text" value="0" data-role="number" data-name="shoes" autocomplete="rutjfkde" autofocus /></div>
                                <div class="col-sm-6" style="margin-top:4px;">Employee ESIC %</div><div class="col-sm-6" style="margin-top:4px;">       <input id="inp-text-14" onkeyup="officialchange()" class="form-control input-sm" type="text" value="0" data-role="number" data-name="employeeesic" autocomplete="rutjfkde" autofocus /></div>
                                <div class="col-sm-6" style="margin-top:4px;">Employer ESIC %</div><div class="col-sm-6" style="margin-top:4px;">       <input id="inp-text-15" onkeyup="officialchange()" class="form-control input-sm" type="text" value="0" data-role="number" data-name="employeresic" autocomplete="rutjfkde" autofocus /></div>
                        </div>

                        <div class="col-sm-8" style="padding:0;">
                        <div class="col-sm-12" style="padding-bottom:10px;border-radius:4px;border:1px solid #CCC;background:#FFF;padding-top:10px;-webkit-box-shadow: 2px 2px 3px 3px #ccc; -moz-box-shadow:2px 2px 3px 3px #ccc;box-shadow:2px 2px 3px 3px #ccc;">
                        <div class="col-sm-4" style="margin-top:4px;">Training Date</div><div class="col-sm-8" style="margin-top:4px;"><input type="text" class="datepicker form-control input-sm" data-name="trainingdate" autocomplete="rutjfkde" autofocus /></div>
                        <div class="col-sm-4" style="margin-top:4px;">Joining Date</div><div class="col-sm-8" style="margin-top:4px;"><input type="text" class="datepicker form-control input-sm" data-name="joiningdate" autocomplete="rutjfkde" autofocus /></div>
                        <div class="col-sm-4" style="margin-top:4px;">Probation Period</div><div class="col-sm-8" style="margin-top:4px;"><input type="text" class="form-control input-sm" data-name="probation" autocomplete="rutjfkde" autofocus /></div>
                        <div class="col-sm-4" style="margin-top:4px;">Permanent Date</div><div class="col-sm-8" style="margin-top:4px;"><input type="text" class="datepicker form-control input-sm" data-name="permanentdate" autocomplete="rutjfkde" autofocus /></div>
                        <div class="col-sm-4" style="margin-top:4px;">Leaving Date</div><div class="col-sm-8" style="margin-top:4px;"><input type="text" class="datepicker form-control input-sm" data-name="leaving" autocomplete="rutjfkde" autofocus /></div>
                        <div class="col-sm-4" style="margin-top:4px;">Leaving Reason</div><div class="col-sm-8" style="margin-top:4px;"><input type="text" class="form-control input-sm" data-name="leavingreason" autocomplete="rutjfkde" autofocus /></div>


                        <div class="col-sm-4" style="margin-top:4px;">Additional Charges</div><div class="col-sm-8" style="margin-top:4px;">
                        <div class="col-sm-3"><input class="form-control input-sm" type="text" data-role="text" placeholder="Desciption" value="-" data-name="charges1" autocomplete="rutjfkde" autofocus /></div>                  
                        <div class="col-sm-3"><input class="form-control input-sm" type="text" value="0" data-role="number" placeholder="amount" data-name="charges1amount" autocomplete="rutjfkde" autofocus /></div>
                        <div class="col-sm-3"><input class="form-control input-sm" type="text" data-role="text" placeholder="Desciption"  value="-"  data-name="charges12" autocomplete="rutjfkde" autofocus /></div>                  
                        <div class="col-sm-3"><input class="form-control input-sm" type="text" value="0" data-role="number" placeholder="amount" data-name="charges1amount" autocomplete="rutjfkde" autofocus /></div>
    
                        </div>
                        <div class="col-sm-4" style="margin-top:4px;">Additional Deduction</div><div class="col-sm-8" style="margin-top:4px;">
                        <div class="col-sm-3"><input class="form-control input-sm" data-role="text" placeholder="Desciption" data-name="deduction1"  value="-" autocomplete="rutjfkde" autofocus /></div>                  
                        <div class="col-sm-3"><input class="form-control input-sm" type="text" value="0" data-role="number" placeholder="amount"     data-name="deduction1amount" autocomplete="rutjfkde" autofocus /></div>
                        <div class="col-sm-3"><input class="form-control input-sm" data-role="text" placeholder="Desciption" data-name="deduction2"  value="-" autocomplete="rutjfkde" autofocus /></div>                  
                        <div class="col-sm-3"><input class="form-control input-sm" type="text" value="0" data-role="number" placeholder="amount"     data-name="deduction2amount" autocomplete="rutjfkde" autofocus /></div>
                        </div>
                        
                        </div>
                        <div class="col-sm-6" style="padding-bottom:10px;border-radius:4px;border:1px solid #CCC;background:#FFF;padding-top:10px;-webkit-box-shadow: 2px 2px 3px 3px #ccc; -moz-box-shadow:2px 2px 3px 3px #ccc;box-shadow:2px 2px 3px 3px #ccc;">
                        <div class="col-sm-6" style="margin-top:4px;">PF. Wages</div><div class="col-sm-6" style="margin-top:4px;">             <input id="inp-text-16" onkeyup="officialchange()" class="form-control input-sm" type="text" value="0" data-role="number" data-name="wages" autocomplete="rutjfkde" autofocus /></div>
                        <div class="col-sm-6" style="margin-top:4px;">Employee PF %</div><div class="col-sm-6" style="margin-top:4px;">         <input id="inp-text-17" onkeyup="officialchange()" class="form-control input-sm" type="text" value="0" data-role="number" data-name="employeepf" autocomplete="rutjfkde" autofocus /></div>
                        <div class="col-sm-6" style="margin-top:4px;">Employer PF %</div><div class="col-sm-6" style="margin-top:4px;">         <input id="inp-text-18" onkeyup="officialchange()" class="form-control input-sm" type="text" value="0" data-role="number" data-name="employerpf" autocomplete="rutjfkde" autofocus /></div>
                        <div class="col-sm-6" style="margin-top:4px;">Bonus</div><div class="col-sm-6" style="margin-top:4px;">                 <input id="inp-text-19" onkeyup="officialchange()" class="form-control input-sm" type="text" value="0" data-role="number" data-name="bonus" autocomplete="rutjfkde" autofocus /></div>
                        <div class="col-sm-6" style="margin-top:4px;">Total CTC (Yearly)</div><div class="col-sm-6" style="margin-top:4px;">    <input id="inp-text-20" onkeyup="officialchange()" class="form-control input-sm" type="text" value="0" data-role="number" data-name="totalctc" autocomplete="rutjfkde" autofocus readonly /></div>
                        <div class="col-sm-6" style="margin-top:4px;">Working Hours</div><div class="col-sm-6" style="margin-top:4px;"><input class="form-control input-sm" type="text" value="9" data-role="number" data-name="workinghours" autocomplete="rutjfkde" autofocus /></div>
                        <div class="col-sm-6" style="margin-top:4px;">Professional Tax</div><div class="col-sm-6" style="margin-top:4px;">
                        <label ><input type="radio" data-role="radio" class="Radio-Input" name="pt" data-name="pt" value="yes"  /> YES</label>
                        <label ><input type="radio" data-role="radio" class="Radio-Input" name="pt" data-name="pt" value="no" checked/> No</label>
                        </div>
                        <div class="col-sm-6" style="margin-top:4px;">Over Time</div><div class="col-sm-6" style="margin-top:4px;">
                        <label ><input type="radio" data-role="radio" class="Radio-Input" name="ot" data-name="ot" value="yes"  />YES</label>
                        <label ><input type="radio" data-role="radio" class="Radio-Input" name="ot" data-name="ot" value="no" checked/> No</label>
                        
                        </div>

                        </div>
                        <div class="col-sm-6" style="padding-bottom:10px;border-radius:4px;border:1px solid #CCC;background:#FFF;padding-top:10px;-webkit-box-shadow: 2px 2px 3px 3px #ccc; -moz-box-shadow:2px 2px 3px 3px #ccc;box-shadow:2px 2px 3px 3px #ccc;">
                        <div class="col-sm-6" style="margin-top:4px;">Sunday Payment</div><div class="col-sm-6" style="margin-top:4px;">
                        <label ><input type="radio" data-role="radio" class="Radio-Input" name="sundaypayment" data-name="sundaypayment" value="yes"  /> YES</label>
                        <label ><input type="radio" data-role="radio" class="Radio-Input" name="sundaypayment" data-name="sundaypayment" value="no" checked /> No</label>
                       
                        </div>
                        <div class="col-sm-6" style="margin-top:4px;">Dress Allowed</div><div class="col-sm-6" style="margin-top:4px;">
                        <label ><input type="radio" data-role="radio" class="Radio-Input" name="da" data-name="da" value="yes"  /> YES</label>
                        <label ><input type="radio" data-role="radio" class="Radio-Input" name="da" data-name="da"  value="no" checked/> No</label>
                        </div>
                        <div class="col-sm-6" style="margin-top:4px;">Shoes Allowed</div><div class="col-sm-6" style="margin-top:4px;">
                        <label ><input type="radio" data-role="radio" class="Radio-Input"  name="sa" data-name="sa" value="yes"  /> YES</label>
                        <label ><input type="radio" data-role="radio" class="Radio-Input" name="sa" data-name="sa"  value="no" checked/> No</label>
                        </div>
                        <div class="col-sm-6" style="margin-top:4px;">Lockers Allowed</div><div class="col-sm-6" style="margin-top:4px;">
                        <label ><input type="radio" data-role="radio" class="Radio-Input" name="la" data-name="la" value="yes"  /> YES</label>
                        <label ><input type="radio" data-role="radio" class="Radio-Input" name="la" data-name="la" value="no" checked/> No</label>
                        </div>
                        <div class="col-sm-6" style="margin-top:4px;">ESIC No.</div><div class="col-sm-6" style="margin-top:4px;"><input type="text" class="form-control input-sm" data-name="esicno" autocomplete="rutjfkde" autofocus /></div>
                        <div class="col-sm-6" style="margin-top:4px;">PF No.</div><div class="col-sm-6" style="margin-top:4px;"><input type="text" class="form-control input-sm" data-name="pfno" autocomplete="rutjfkde" autofocus /></div>
                        <div class="col-sm-6" style="margin-top:4px;">IT Deduction</div><div class="col-sm-6" style="margin-top:4px;"><input type="text" class="form-control input-sm" data-name="itdeduction" autocomplete="rutjfkde" autofocus /></div>
                        <div class="col-sm-6" style="margin-top:4px;">Weekly Off</div><div class="col-sm-6" style="margin-top:4px;">
                        <select class="form-control input-sm" data-name="weeklyoff" >
                            <option value="Select">Select</option>
                            <option value="SUNDAY">SUNDAY</option>
                            <option value="MONDAY">MONDAY</option>
                            <option value="TUESDAY">TUESDAY</option>
                            <option value="WEDNESDAY">WEDNESDAY</option>
                            <option value="THURSDAY">THURSDAY</option>
                            <option value="FRIDAY">FRIDAY</option>
                            <option value="SATURDAY">SATURDAY</option>
                        </select>
                        </div>
                        </div>
                        </div>
                </div>
                <div class="col-sm-10" id="section3" style="display:none;">
                <div class="col-sm-12" align="center" style="border-radius:4px;border:1px solid #CCC;background:#FFF;padding-top:10px;-webkit-box-shadow: 2px 2px 3px 3px #ccc; -moz-box-shadow:2px 2px 3px 3px #ccc;box-shadow:2px 2px 3px 3px #ccc;"><h3 style="margin:5px;" id="details">Attachment Documents</h3></div>            
                    <div style="clear:both;"></div>
                    <hr style="margin:6px;">
           
                        <div class="col-sm-12" style="padding-bottom:10px;border-radius:4px;border:1px solid #CCC;background:#FFF;padding-top:10px;-webkit-box-shadow: 2px 2px 3px 3px #ccc; -moz-box-shadow:2px 2px 3px 3px #ccc;box-shadow:2px 2px 3px 3px #ccc;">
                        <div class="col-sm-2" style="margin-top:4px;">Attachment 1</div><div class="col-sm-3"><input class="form-control" type="text" data-name="file1" id="files1"/> </div><div class="col-sm-3" style="margin-top:4px;"><input type="file" class="form-control input-sm" name="file1" id="file1" autocomplete="rutjfkde" autofocus /></div><div class="col-sm-3" id="filelink1"></div><div style="clear:both;"></div>
                        <div class="col-sm-2" style="margin-top:4px;">Attachment 2</div><div class="col-sm-3"><input class="form-control" type="text" data-name="file2" id="files2"/> </div><div class="col-sm-3" style="margin-top:4px;"><input type="file" class="form-control input-sm" name="file2" id="file2" autocomplete="rutjfkde" autofocus /></div><div class="col-sm-3" id="filelink2"></div><div style="clear:both;"></div>
                        <div class="col-sm-2" style="margin-top:4px;">Attachment 3</div><div class="col-sm-3"><input class="form-control" type="text" data-name="file3" id="files3"/> </div><div class="col-sm-3" style="margin-top:4px;"><input type="file" class="form-control input-sm" name="file3" id="file3" autocomplete="rutjfkde" autofocus /></div><div class="col-sm-3" id="filelink3"></div><div style="clear:both;"></div>
                        <div class="col-sm-2" style="margin-top:4px;">Attachment 4</div><div class="col-sm-3"><input class="form-control" type="text" data-name="file4" id="files4"/> </div><div class="col-sm-3" style="margin-top:4px;"><input type="file" class="form-control input-sm" name="file4" id="file4" autocomplete="rutjfkde" autofocus /></div><div class="col-sm-3" id="filelink4"></div><div style="clear:both;"></div>
                        <div class="col-sm-2" style="margin-top:4px;">Attachment 5</div><div class="col-sm-3"><input class="form-control" type="text" data-name="file5" id="files5"/> </div><div class="col-sm-3" style="margin-top:4px;"><input type="file" class="form-control input-sm" name="file5" id="file5" autocomplete="rutjfkde" autofocus /></div><div class="col-sm-3" id="filelink5"></div><div style="clear:both;"></div>
                        <div class="col-sm-2" style="margin-top:4px;">Attachment 6</div><div class="col-sm-3"><input class="form-control" type="text" data-name="file6" id="files6"/> </div><div class="col-sm-3" style="margin-top:4px;"><input type="file" class="form-control input-sm" name="file6" id="file6" autocomplete="rutjfkde" autofocus /></div><div class="col-sm-3" id="filelink6"></div><div style="clear:both;"></div>
                        <div class="col-sm-2" style="margin-top:4px;">Attachment 7</div><div class="col-sm-3"><input class="form-control" type="text" data-name="file7" id="files7"/> </div><div class="col-sm-3" style="margin-top:4px;"><input type="file" class="form-control input-sm" name="file7" id="file7" autocomplete="rutjfkde" autofocus /></div><div class="col-sm-3" id="filelink7"></div><div style="clear:both;"></div>
                        <div class="col-sm-2" style="margin-top:4px;">Attachment 8</div><div class="col-sm-3"><input class="form-control" type="text" data-name="file8" id="files8"/> </div><div class="col-sm-3" style="margin-top:4px;"><input type="file" class="form-control input-sm" name="file8" id="file8" autocomplete="rutjfkde" autofocus /></div><div class="col-sm-3" id="filelink8"></div><div style="clear:both;"></div>
                        <div class="col-sm-2" style="margin-top:4px;">Attachment 9</div><div class="col-sm-3"><input class="form-control" type="text" data-name="file9" id="files9"/> </div><div class="col-sm-3" style="margin-top:4px;"><input type="file" class="form-control input-sm" name="file9" id="file9" autocomplete="rutjfkde" autofocus /></div><div class="col-sm-3" id="filelink9"></div><div style="clear:both;"></div>
                        <div class="col-sm-2" style="margin-top:4px;">Attachment 10</div><div class="col-sm-3"><input class="form-control" type="text" data-name="file10" id="files10"/> </div><div class="col-sm-3" style="margin-top:4px;"><input type="file" class="form-control input-sm" name="file10" id="file10" autocomplete="rutjfkde" autofocus /></div><div class="col-sm-3" id="filelink10"></div><div style="clear:both;"></div>
                        
                        </div>
                        <div>
            </div>
    </div>
    </div>


        <!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" align="center">Add Family Members Details</h4>
            </div>
            <div class="modal-body" id='modal'>
                <div class="row">
                    <div class="col-sm-4">Person Name</div>
                    <div class="col-sm-8">
                        <div class="form-group"><input type="text" data-role='text' data-name='name' class="form-control input-sm"></div>
                    </div>

                    <div class="col-sm-4">Date Of Birth</div>
                    <div class="col-sm-8">
                        <div class="form-group"><input type="date" data-name='dob' class="form-control input-sm"></div>
                    </div>

                    <div style="clear:both;"></div>
                    <hr>
                    <div class="col-sm-4"></div>
                    <div class="col-sm-4"><button class="btn btn-success btn-sm btn-block" id="btn-contact" style="margin:5px;">Submit</button></div>
                    <div class="col-sm-4"><button class="btn btn-danger btn-sm btn-block" id="btn-clear" style="margin:5px;">Clear</button></div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- new Employee Model -->

<div id="newEmployeeModal" class="modal fade" role="dialog">
    
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
    
            <div class="modal-header">
    
                <button type="button" class="close" data-dismiss="modal">&times;</button>
    
                <h4 class="modal-title" align="center">Add New Emlpoyee</h4>
    
            </div>
    
            <div class="modal-body" id='employeeModal'>
    
                <div class="row">
    
                    <div class="col-sm-4">Person Name</div>
    
                    <div class="col-sm-8">
    
                        <div class="form-group">
                            
                            <select class="form-control input-sm select-js" data-role="select" data-name="branchids" id="branchids">
                                
                                <option value="Select">Select</option>
                                
                                <?php 
                                        
                                    $result1=mysqli_query($con, "SELECT * FROM `branchmaster`");
                                    
                                    while($rows=mysqli_fetch_assoc($result1)) {
                                    
                                        echo '<option value="'.$rows['branchid'].'">'.$rows['branchname'].'</option>';
                                    
                                    }
                                
                                ?>
                                
                            </select>

                        </div>
                   
                    </div>

                    <div class="col-sm-4">Employee Name</div>
                    
                    <div class="col-sm-8">
                        
                        <div class="form-group">
                            
                            <input type="text" data-name='employeeName' data-role="text" class="form-control input-sm">
                        
                        </div>
                    
                    </div>

                    <div class="col-sm-4">Mobile No</div>
                    
                    <div class="col-sm-8">
                        
                        <div class="form-group">
                            
                            <input type="number" data-name='phone' data-role="number" class="form-control input-sm">
                        
                        </div>
                    
                    </div>

                    <div style="clear:both;"></div>
                    <hr>
                    <div class="col-sm-4"></div>
                    <div class="col-sm-4"><button class="btn btn-success btn-sm btn-block" id="btn-addnewEmployee"
                            style="margin:5px;">Submit</button></div>
                    <div class="col-sm-4"><button class="btn btn-danger btn-sm btn-block" id="btn-clear"
                            style="margin:5px;">Clear</button></div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
include($base.'_in/footer.php');
?>
<script>


$(document).ready(function () {

$('.select-js').select2({width: '100%'});

$('.select').attr('style','width:100%!important;');


});

$('#btn-contact').on('click',function(){
    valid=true;
    if(checker('modal') !=false ) {
			valid=valid*true;
		}else{
		    valid=valid*false;
         }
    if(valid){
        data=checker('modal');
        tableui(data.name,data.dob);
        modals.clear('modal');
    }
})


var count=1;
function tableui(name,dob){
var sr=count;
count++;
var str='';
str+='<tr id="tr-'+sr+'">';
str+='<td class="sr">'+sr+'</td>';
str+='<td class="name">'+name+'</td>';
str+='<td class="dob">'+dob+'</td>';
str+='<td><button data-id="tr-'+sr+'" onclick="remover(this)" class="btn btn-danger btn-sm" >R</nutton></td>';
str+='</tr>';

$('#contact-table tbody').append(str);
}


function empmaster(e){    
    var json1=JSON.parse($('#item-json1').val());
    var branchid=$(e).val();
    var str='<option value="Select">Select</option>';
    for(var i in json1){
        if(json1[i].branchid==branchid){
        str+='<option value="'+json1[i].employeeid+'">'+json1[i].username+'</option>';
        }
    }
    $('#employeeid').html(str);
}
$('#branchid').val($('#inp-branch').val()).trigger('change');

function getfile(input){
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#img').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
}

$('.datepicker').pickadate({     
    selectYears: 100,
	selectMonths: true,
	onClose: function () {
		try {
			var dateStamp = this.get('select')['pick'];
			var id = this.get('id');
			$('#' + id).attr('data-stamp', dateStamp);
			$('#' + id).parent().find('.timepicker').click();
		} catch (err) {
			console.log(err);
		}
	}
});

$('#inp-address').on('keyup',function (){
    $('#inp-paddress').val($(this).val());
})

function personalui(){
$('#section1').fadeIn(500);
$('#section2').fadeOut(500);
$('#section3').fadeOut(500);
}

function official(){
    $('#section1').fadeOut(500);
    $('#section2').fadeIn(500);
    $('#section3').fadeOut(500);
}

function attachment(){
    $('#section1').fadeOut(500);
    $('#section2').fadeOut(500);
    $('#section3').fadeIn(500);    
}

function officialchange() {
var inp1= parseFloat($('#inp-text-1').val());
var inp2= parseFloat($('#inp-text-2').val());
var inp3= parseFloat($('#inp-text-3').val());
var inp4= parseFloat($('#inp-text-4').val());
var inp5= parseFloat($('#inp-text-5').val());
var inp6= parseFloat($('#inp-text-6').val());
var inp7= parseFloat($('#inp-text-7').val());
var inp8= parseFloat($('#inp-text-8').val());
var inp9= parseFloat($('#inp-text-9').val());
var inp10=parseFloat($('#inp-text-10').val());
var inp11=parseFloat($('#inp-text-11').val());
var inp12=parseFloat($('#inp-text-12').val());
var inp13=parseFloat($('#inp-text-13').val());
var inp14=parseFloat($('#inp-text-14').val());
var inp15=parseFloat($('#inp-text-15').val());
var inp16=parseFloat($('#inp-text-16').val());
var inp17=parseFloat($('#inp-text-17').val());
var inp18=parseFloat($('#inp-text-18').val());
var inp19=parseFloat($('#inp-text-19').val());
var inp20=parseFloat($('#inp-text-20').val());

var a = (inp1+inp2+inp3+inp4+inp5+inp6+inp7+inp8);
var b = (inp10+inp11+inp12+inp13);
var c = ((a + b)*inp15)/100;
var d= ((inp1+inp6)*inp18)/100;
var ctc = ((a+b+c+d)*12)+inp19;

$('#inp-text-9').val(a.round(2));
$('#inp-text-20').val(ctc.round(2));

}

function empdetails(e){
    modals.clear('section1');
                         modals.clear('section2');
                         modals.clear('section3');
var employeeid=$(e).val();
var json1=JSON.parse($('#item-json1').val());
    for(var i in json1){
        if(json1[i].employeeid==employeeid){
        $('#phoneno').val(json1[i].phone);
        $('#emailno').val(json1[i].email);
        }
    }
    
        $.ajax({
				type: "POST",
				data: 'employeeid='+employeeid,
				url:  'empdetails.php',
				cache: false,
				success: function (res) {
					if (res.status == 'success') {
                        modals.clear('section1');
                         modals.clear('section2');
                         modals.clear('section3');
                         $('#filelink1').html('');
                         $('#filelink2').html('');
                         $('#filelink3').html('');
                         $('#filelink4').html('');
                         $('#filelink5').html('');
                         $('#filelink6').html('');
                         $('#filelink7').html('');
                         $('#filelink8').html('');
                         $('#filelink9').html('');
                         $('#filelink10').html('');
                        var personaljson=res.json[0].personaljson;
                        var officialjson=res.json[0].officialjson;
                        var attachedjson=res.json[0].attachedjson;
                        console.log(attachedjson);
                        for(var i in attachedjson){
                            var file='';
                            var keys='';
                            var url=attachedjson[i].url;
                            var documentid=attachedjson[i].documentid;



                            // if ($('#file1').attr('name')==documentid) {
                            //     

                            // }
                            if (typeof attachedjson[i].documentid !== 'undefined') { file=attachedjson[i].documentname; keys='file'+attachedjson[i].documentid;    $('#filelink'+attachedjson[i].documentid).html('<a href="employeedata/'+url+'" download>Download File'+documentid+'</a> ,   <a target="_blank_" href="employeedata/'+url+'">Open File'+documentid+'</a> , <button class="btn btn-sm btn-danger" data-empid="'+employeeid+'" data-documentid="'+documentid+'" data-url="'+url+'" onclick="deletepic(this)">Delete File'+documentid+'</button>');}
                            // if (typeof attachedjson[i].documentid !== 'undefined') { file=attachedjson[i].documentid; keys='file2';    $('#filelink2').html('<a href="employeedata/'+url+'" download>Download File 2</a> ,   <a target="_blank_" href="employeedata/'+url+'">Open File 2</a> , <button data-empid="'+employeeid+'" data-url="'+url+'" onclick="deletepic(this)">Delete File 2</button>');}
                            // if (typeof attachedjson[i].documentid !== 'undefined') { file=attachedjson[i].documentid; keys='file3';    $('#filelink3').html('<a href="employeedata/'+url+'" download>Download File 3</a> ,   <a target="_blank_" href="employeedata/'+url+'">Open File 3</a> , <button data-empid="'+employeeid+'" data-url="'+url+'" onclick="deletepic(this)">Delete File 3</button>');}
                            // if (typeof attachedjson[i].documentid !== 'undefined') { file=attachedjson[i].documentid; keys='file4';    $('#filelink4').html('<a href="employeedata/'+url+'" download>Download File 4</a> ,   <a target="_blank_" href="employeedata/'+url+'">Open File 4</a> , <button data-empid="'+employeeid+'" data-url="'+url+'" onclick="deletepic(this)">Delete File 4</button>');}
                            // if (typeof attachedjson[i].documentid !== 'undefined') { file=attachedjson[i].documentid; keys='file5';    $('#filelink5').html('<a href="employeedata/'+url+'" download>Download File 5</a> ,   <a target="_blank_" href="employeedata/'+url+'">Open File 5</a> , <button data-empid="'+employeeid+'" data-url="'+url+'" onclick="deletepic(this)">Delete File 5</button>');}
                            // if (typeof attachedjson[i].documentid !== 'undefined') { file=attachedjson[i].documentid; keys='file6';    $('#filelink6').html('<a href="employeedata/'+url+'" download>Download File 6</a> ,   <a target="_blank_" href="employeedata/'+url+'">Open File 6</a> , <button data-empid="'+employeeid+'" data-url="'+url+'" onclick="deletepic(this)">Delete File 6</button>');}
                            // if (typeof attachedjson[i].documentid !== 'undefined') { file=attachedjson[i].documentid; keys='file7';    $('#filelink7').html('<a href="employeedata/'+url+'" download>Download File 7</a> ,   <a target="_blank_" href="employeedata/'+url+'">Open File 7</a> , <button data-empid="'+employeeid+'" data-url="'+url+'" onclick="deletepic(this)">Delete File 7</button>');}
                            // if (typeof attachedjson[i].documentid !== 'undefined') { file=attachedjson[i].documentid; keys='file8';    $('#filelink8').html('<a href="employeedata/'+url+'" download>Download File 8</a> ,   <a target="_blank_" href="employeedata/'+url+'">Open File 8</a> , <button data-empid="'+employeeid+'" data-url="'+url+'" onclick="deletepic(this)">Delete File 8</button>');}
                            // if (typeof attachedjson[i].documentid !== 'undefined') { file=attachedjson[i].documentid; keys='file9';    $('#filelink9').html('<a href="employeedata/'+url+'" download>Download File 9</a> ,   <a target="_blank_" href="employeedata/'+url+'">Open File 9</a> , <button data-empid="'+employeeid+'" data-url="'+url+'" onclick="deletepic(this)">Delete File 9</button>');}
                            // if (typeof attachedjson[i].documentid !== 'undefined') { file=attachedjson[i].documentid; keys='file10'; $('#filelink10').html('<a href="employeedata/'+url+'"download>Download File 10</a> ,  <a target="_blank_" href="employeedata/'+url+'>Open File 10</a> , <button data-empid="'+employeeid+'" data-url="'+url+'" onclick="deletepic(this)">Delete File 10</button>');}
                           
                            var str=JSON.parse('[{"'+keys+'":"'+file+'"}]');
                            console.log(str);

                            modals.putvalue('section3',str);
                        }
                        var photo=res.json[0].photo;
                        modals.putvalue('section1',personaljson);
                        console.log(personaljson);
                        
                        modals.putvalue('section2',officialjson);
                        $('#img').attr('src', photo);
                        $('#submit').text('Update');
                        $('#submit').attr('onclick','update()');

                        var contact=res.json[0].contactjson;
                        console.log(contact);
                        
                            for(var i in contact){
                                tableui(contact[i].name,contact[i].dob);
                            }

                    }else{
                        $('#submit').text('Submit');
                        $('#submit').data('onclick','submit()');

                        $('.datepicker ').val('');
                        $('#contact-table > tbody> tr').remove();
                       

                    }
				}
			});
}



function submit(){
    
    var valid=true;
    if(checker('section0') !=false ) {
		valid=valid*true;
	}else{
		valid=valid*false;
    }

    if(checker('section1') !=false ) {
		valid=valid*true;
	}else{
		valid=valid*false;
    }

    if(checker('section2') !=false ) {
		valid=valid*true;
	}else{
		valid=valid*false;
    }

    if(checker('section3') !=false ) {
		valid=valid*true;
	}else{
		valid=valid*false;
    }
    var data4=$('#img');
    data4=data4[0].currentSrc;

    if(valid){
        var data0=checker('section0');
        var data1=checker('section1');
        var data2=checker('section2');
        var data3=checker('section3');
        
        var datastr0 = JSON.stringify(data0);
        var datastr1 = JSON.stringify(data1);
        var datastr2 = JSON.stringify(data2);
        var datastr3 = JSON.stringify(data3);
        var datastr4 = JSON.stringify(data4);
        var datastr5=JSON.stringify(modals.tabletdbyclass('contact-table'));

        
        var file1=$('#file1')[0].files[0];
        var file2=$('#file2')[0].files[0];
        var file3=$('#file3')[0].files[0];
        var file4=$('#file4')[0].files[0];
        var file5=$('#file5')[0].files[0];
        var file6=$('#file6')[0].files[0];
        var file7=$('#file7')[0].files[0];
        var file8=$('#file8')[0].files[0];
        var file9=$('#file9')[0].files[0];
        var file10=$('#file10')[0].files[0];

        var formData = new FormData();
        formData.append('data0',datastr0);
        formData.append('data1',datastr1);
        formData.append('data2',datastr2);
        formData.append('data3',datastr3);
        formData.append('data4',datastr4);
        formData.append('data5',datastr5);

        formData.append('file1',file1);
        formData.append('file2',file2);
        formData.append('file3',file3);
        formData.append('file4',file4);
        formData.append('file5',file5);
        formData.append('file6',file6);
        formData.append('file7',file7);
        formData.append('file8',file8);
        formData.append('file9',file9);
        formData.append('file10',file10);
        $.ajax({
            type: "POST",
            data: formData,
            url: 'insert.php',
            cache: false,
            processData: false,
            contentType: false,
				success: function (res) {
					if (res.status == 'success') {
						swal({
							type: 'success',
							title: 'Salary Master Created',
							showConfirmButton: false,
							timer: 2000
						});
						setTimeout(function () {
							window.location.reload();
						}, 2000);
                    }
                   
				}
			});
    }
}

function update(){
    var valid=true;
    if(checker('section0') !=false ) {
		valid=valid*true;
	}else{
		valid=valid*false;
    }

    if(checker('section1') !=false ) {
		valid=valid*true;
	}else{
		valid=valid*false;
    }

    if(checker('section2') !=false ) {
		valid=valid*true;
	}else{
		valid=valid*false;
    }

    if(checker('section3') !=false ) {
		valid=valid*true;
	}else{
		valid=valid*false;
    }
    var data4=$('#img');
    data4=data4[0].currentSrc;

    if(modals.tabletdbyclass('contact-table')!=false){
        valid=valid*true;
		}else{
		valid=valid*false;
    }
    if(valid){
        var data0=checker('section0');
        var data1=checker('section1');
        var data2=checker('section2');
        var data3=checker('section3');
        var datastr5=JSON.stringify(modals.tabletdbyclass('contact-table'));

        var datastr0 = JSON.stringify(data0);
        var datastr1 = JSON.stringify(data1);
        var datastr2 = JSON.stringify(data2);
        var datastr3 = JSON.stringify(data3);
        var datastr4 = JSON.stringify(data4);
        
        var file1=$('#file1')[0].files[0];
        var file2=$('#file2')[0].files[0];
        var file3=$('#file3')[0].files[0];
        var file4=$('#file4')[0].files[0];
        var file5=$('#file5')[0].files[0];
        var file6=$('#file6')[0].files[0];
        var file7=$('#file7')[0].files[0];
        var file8=$('#file8')[0].files[0];
        var file9=$('#file9')[0].files[0];
        var file10=$('#file10')[0].files[0];

        var formData = new FormData();
        formData.append('data0',datastr0);
        formData.append('data1',datastr1);
        formData.append('data2',datastr2);
        formData.append('data3',datastr3);
        formData.append('data4',datastr4);

        formData.append('data5',datastr5);

        formData.append('file1',file1);
        formData.append('file2',file2);
        formData.append('file3',file3);
        formData.append('file4',file4);
        formData.append('file5',file5);
        formData.append('file6',file6);
        formData.append('file7',file7);
        formData.append('file8',file8);
        formData.append('file9',file9);
        formData.append('file10',file10);
        $.ajax({
            type: "POST",
            data: formData,
            url: 'update.php',
            cache: false,
            processData: false,
            contentType: false,
				success: function (res) {
					if (res.status == 'success') {
						swal({
							type: 'success',
							title: 'Salary Master updated',
							showConfirmButton: false,
							timer: 2000
						});
						setTimeout(function () {
							window.location.reload();
						}, 2000);
                    }
                   
				}
			});
    }
}


function deletepic(e) {

    let empid = $(e).attr('data-empid');
    let url   = $(e).attr('data-url');
    let documentid = $(e).attr('data-documentid');

    $.ajax({
        type: "POST",
        data: "empid=" + empid + "&url=" + url + "&documentid="+documentid,
        url: 'deletepic.php',
        cache: false,
        success: function (res) {
            if (res.status == 'success') {
                swal({
                    type: 'success',
                    title: 'Photo Deleted',
                    showConfirmButton: false,
                    timer: 2000
                });
                setTimeout(function () {
                    $('#files'+documentid).val('');
                    $('#filelink'+documentid).hide();
                }, 2000);
            }

        }
    });
}

// -----------------------Add New Employee------------------------------//

$('#btn-addnewEmployee').on('click',function(){

    var valid = true;

    if(checker('employeeModal') !=false ) {

		valid = valid*true;

	} else {
		
        valid = valid*false;

    }

    if(valid){

        var data     = checker('employeeModal');

        var datastr1 = JSON.stringify(data);

        $.ajax({

            type: "POST",
            data: {

                data1: datastr1

            },
            url: 'addnewemployee.php',
            cache: false,
            success: function (res) {

                if (res.status == 'success') {

                    swal({
                        type: 'success',
                        title: 'Emplyee Add',
                        showConfirmButton: false,
                        timer: 1000
                    });

                    setTimeout(function () {

                        window.location.reload();

                    }, 1000);

                } else if(res.status == 'falid2'){

                    swal(
                        'Cancelled',
                        'Mobile No Already Exist :)',
                        'error'
                    )

                }

            }

        });

    }

})

$("#newEmployeeModal").on('hidden.bs.modal', function (e) {
  $(this)
    .find("input,textarea,select")
       .val('')
       .end();
})

</script>