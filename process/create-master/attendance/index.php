<?php
$base='../../../';
 
$js=' <script src="'.$base.'js/fwork.js"></script>
<script src="'.$base.'js/alart.js"></script> 
<script src="'.$base.'js/list.min.js"></script>
<script src="'.$base.'js/picker.js"></script>
<script src="'.$base.'js/picker.date.js"></script>
<script src="'.$base.'js/picker.time.js"></script>
<script src="'.$base.'js/pdfmake.min.js"></script>
<script src="'.$base.'js/vfs_fonts.js"></script>
<script src="'.$base.'js/moment.js"></script>
 ';

$css='<link rel="stylesheet" href="'.$base.'css/fwork.css">
<link rel="stylesheet" href="'.$base.'css/alart.css">
<link rel="stylesheet" href="'.$base.'css/grid.min.css">
<link rel="stylesheet" href="'.$base.'css/classic.date.css">
<link rel="stylesheet" href="'.$base.'css/classic.time.css">
<link rel="stylesheet" href="'.$base.'css/classic.css">';
include($base.'_in/header.php');
include($base.'_in/connect.php');
$con=_connect();
$json1='';
$result=mysqli_query($con,"SELECT branchid,employeeid,username FROM employeemaster ORDER BY username ASC");
while($rows = mysqli_fetch_assoc($result)){
    $json1.=',{"branchid":"'.$rows['branchid'].'","employeeid":"'.$rows['employeeid'].'","username":"'.$rows['username'].'"}';
}
$json1=substr($json1,1);
$json1='['.$json1.']';

?>

<style>
 
hr.style-hr {
    border: 0;
    height: 1px;
    background-image: linear-gradient(to right, rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.75), rgba(0, 0, 0, 0));
}

</style>
<style>
hr.style-hr {
    border: 0;
    height: 1px;
    background-image: linear-gradient(to right, rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.75), rgba(0, 0, 0, 0));
}
.table-list td,
   .table-list th {
       border: 1px solid #ddd;
       padding: 1px !important;
       font-size: 13px;
   }
   .table-list td{
	padding-top: 10px !important;

   }

   .table-list tr:nth-child(even) {
       background-color: #f2f2f2;
   }

   .table-list th {
       padding-top: 5px;
       padding-bottom: 5px;
       text-align: center;
       background-color: #16a085;
       color: white;
   }
</style>
<div class="container-fluid" id="section">
    <div class=" content">
        <h2 align="center" style="margin:0;">Daily Attendance</h2>
        <hr class="style-hr">
<form action="index.php" method="POST">
        <div class="col-sm-12">
        <div class="col-sm-3">
              <input type="hidden" id="item-json1" value='<?php echo str_replace("'"," &#39;",$json1); ?>' />
        
                <div class="col-sm-3">Month/Year</div>
                <div class="col-sm-9">
                    <div class="form-group">
                      
                       <div class="col-sm-6">
  
                           <select class="form-control input-sm" data-name="month" id="month" name ="month" >
                            <option value="Select">Select</option>
                           <?php
                                for ($i = 0; $i < 12; $i++) {
                                    $time = strtotime(sprintf('%d months', $i));   
                                    $label = date('F', $time);   
                                    $value = date('n', $time);
                                    if($value == date('m')){
                                        echo "<option value='$value' selected>$label</option>";
                                    }else{
                                        echo "<option value='$value'>$label</option>";    
                                    }
                                    
                                }
                                ?>
                        </select> 
                        </div>
                           <div class="col-sm-6">
                           <select class="form-control input-sm" data-name="year" id="year" name="year" >
                            <option value="Select">Select</option>
                           <?php
                                foreach(range(2000, (int)date("Y")) as $year) {
                                    if($year == date("Y")){
                                        echo "\t<option value='".$year."' selected>".$year."</option>\n\r";
                                    }else{
                                        echo "\t<option value='".$year."'>".$year."</option>\n\r";
                                        }
                                
                            }
                                ?>
                        </select>
                        <br>
                        </div>
                     
                   
                    </div>
                </div>

           
            
        </div>
        
          <div class="col-sm-3">
           
                <div class="col-sm-4">Branch Name </div>
                <div class="col-sm-8">
                    <div class="form-group">
                     
                        <select class="form-control input-sm" data-name="branchid" id="branchid" name="branchid" onchange="empmaster(this);" >
                        <option value="Select">Select</option>
                            <?php 
                                    $result1=mysqli_query($con, "SELECT * FROM `branchmaster`");
                                    while($rows=mysqli_fetch_assoc($result1)) {

                                        echo '<option value="'.$rows['branchid'].'">'.$rows['branchname'].'</option>';    
                                      
                                    
                                }
                            ?>
                        </select>
                    </div>
                
                    </div>
            </div>
           
             
         <div class="col-sm-3" >
              <div class="col-sm-4">Employee Name </div>
                <div class="col-sm-8">
                    <div class="form-group">
                     
                        <select class="form-control input-sm"    data-name="employeeid" id="employeeid" name="employeeid" >
                        <option value="Select">Select</option>
                       
                         
                    </select>
                    </div>
                
                    </div>
              
 
           </div>
           <div class="col-sm-2"><input  type="submit" value ="Add Attendance" class="btn btn-primary btn-sm btn-block" id="btn-add" style="margin:5px;"> </div>
          </div>
         </form> 

            <div style="clear:both;"></div>
<input type="hidden" data-name="hemployeeid" value="<?php echo $_POST['employeeid']; ?>">
<input type="hidden" data-name="hbranchid" value="<?php echo $_POST['branchid']; ?>">
<?php
if(isset($_POST['employeeid'])){
  $employeeid =$_POST['employeeid'];
  $empname=mysqli_fetch_assoc(mysqli_query($con,"SELECT username x FROM employeemaster WHERE employeeid='$employeeid'"))['x'];

  $titlename = $empname." - ".$_POST['month'] ."/".$_POST['year'];
}
  
?>
              <hr class="style-hr">
              <div class="col-sm-12">
              <div class="col-sm-8">

              <h4 align="center"><?php echo $titlename ; ?> </h4>
              <table class="table" >
               <thead><th>Date</th><th>In Time</th><th>Out Time</th><th>Total Hrs</th><th>Working Hrs</th><th>Over Time</th><th>Remarks</th></thead><tbody>
              <?php 
              if(isset($_POST['employeeid'])) {  
                $month = $_POST['month'];
                $year = $_POST['year'];
                
              $start_date = "01-".$month."-".$year;
              $start_time = strtotime($start_date);

              $end_time = strtotime("+1 month", $start_time);

              for($i=$start_time; $i<$end_time; $i+=86400)
              {
                 $attdate = date('d-m-Y', $i);
                  
                 ?>
                          <tr background-color="lightgreen"><td><input type="text" data-name="attdate_<?php echo $attdate; ?>" class = "form-control " id="attdate" data-role="text" value="<?php echo $attdate;?>"></td><td><input type="text" data-name="intime_<?php echo $attdate; ?>" id="intime_<?php echo $attdate; ?>"  class = "form-control timepicker1"></td><td><input type="text" data-name="outtime_<?php echo $attdate; ?>" id="outtime_<?php echo $attdate; ?>"   class = "form-control timepicker2" ></td><td><input type="number" data-name="totalhrs_<?php echo $attdate; ?>"   " id="totalhrs_<?php echo $attdate; ?>" class = "form-control "   ></td><td><input type="number" data-name="workinghrs_<?php echo $attdate; ?>" id="workinghrs_<?php echo $attdate; ?>" value= "9"   class = "form-control"  ></td><td><input type="number"  id="overtime_<?php echo $attdate; ?>" data-name="overtime_<?php echo $attdate; ?>" class = "form-control overtime" ></td><td><input type="text" data-name="remark_<?php echo $attdate; ?>"  class = "form-control" ></td></tr>
                          <?php } } ?>
              </table>
              </div>
              <div class="col-sm-4">
                <div class="col-sm-5">Late Days </div>
                  <div class="col-sm-7">
                    <div class="form-group">
                      <input type="text" data-role="number" data-name="latedays" id="latedays" class="form-control input-sm"  value="0">
                     </div>
                
                  </div>
                  <div class="col-sm-5">Without Pay</div>
                  <div class="col-sm-7">
                    <div class="form-group">
                      <input type="text" data-role="number" data-name="withoutpay" id="withoutpay" class="form-control input-sm"  value="0">
                     </div>
                
                  </div>
                  <div class="col-sm-5">Over Time </div>
                  <div class="col-sm-7">
                    <div class="form-group">
                      <input type="text" data-role="number" data-name="totalovertime" id="totalovertime" class="form-control input-sm"  value="0">
                     </div>
                
                  </div>
                  <div class="col-sm-5">Working Days </div>
                  <div class="col-sm-7">
                    <div class="form-group">
                      <input type="text" data-role="number" data-name="workingdays" id="workingdays" class="form-control input-sm"  value="0">
                     </div>
                
                  </div>
                  <div class="col-sm-5">Holidays Working Hrs </div>
                  <div class="col-sm-7">
                    <div class="form-group">
                      <input type="text" data-role="number" data-name="holidayhrs" id="holidayhrs" class="form-control input-sm" value="0">
                     </div>
                
                  </div>
                  <div class="col-sm-5">Sunday </div>
                  <div class="col-sm-7">
                    <div class="form-group">
                      <input type="text" data-role="number" data-name="sunday" id="sunday" class="form-control input-sm"  value="0">
                     </div>
                
                  </div>
                  <div class="col-sm-5">Paid Holidays </div>
                  <div class="col-sm-7">
                    <div class="form-group">
                      <input type="text" data-role="number" data-name="paidholidays" id="paidholidays" class="form-control input-sm"  value="0">
                     </div>
                
                  </div>
                  <div class="col-sm-5">Late Cutting</div>
                  <div class="col-sm-7">
                    <div class="form-group">
                      <input type="text" data-role="number" data-name="latecutting" id="latecutting" class="form-control input-sm"  value="0">
                     </div>
                
                  </div>
                  <div class="col-sm-5">Total PL </div>
                  <div class="col-sm-7">
                    <div class="form-group">
                      <input type="text" data-role="number" data-name="workingdays" id="workingdays" class="form-control input-sm"  value="0">
                     </div>
                
                  </div>
                  <div class="col-sm-5">Total CL </div>
                  <div class="col-sm-7">
                    <div class="form-group">
                      <input type="text" data-role="number" data-name="workingdays" id="workingdays" class="form-control input-sm"  value="0">
                     </div>
                
                  </div>
                  <div class="col-sm-5">Total SL </div>
                  <div class="col-sm-7">
                    <div class="form-group">
                      <input type="text" data-role="number" data-name="workingdays" id="workingdays" class="form-control input-sm"  value="0">
                     </div>
                
                  </div>
                  <div class="col-sm-5">Working Days </div>
                  <div class="col-sm-7">
                    <div class="form-group">
                      <input type="text" data-role="number" data-name="totalworkingdays" id="totalworkingdays" class="form-control input-sm"  value="0" >
                     </div>
                
                  </div>
                </div>
              </div>
              </div>
              <div class="col-sm-3"></div>
              <div class="col-sm-6">
                  <div class="col-sm-6"><button class="btn btn-success btn-sm btn-block" id="btn-submit"
                          style="margin:5px;">Submit</button></div>
                  <div class="col-sm-6"><button class="btn btn-danger btn-sm btn-block" id="btn-reset"
                          onclick="$('#section').find('select').val('');$('#section').find('input').val('');" style="margin:5px;">Reset</button></div>
              </div>
              <div class="col-sm-3"></div>
              
          </div>

          </div>
         
           
                       
        
</div>

<?php
include($base.'_in/footer.php');
?>

<script>
 
 /*
 $('#btn-add').click(function(){
 var valid=true;
    if(checker('section1') !=false ) {
      valid=valid*true;
    }else{
        valid=valid*false;
         }
    if(valid){
    var str = '<thead><th>Date</th><th>In Time</th><th>Out Time</th><th>Total Hrs</th><th>Working Hrs</th><th>Over Time</th><th>Remarks</th></thead><tbody>';
    var str1 = '';
    $('#allinputs').empty();
    
      month1= $("#month").val();

      year1= $("#year").val();
    var numberOfDays = getDaysInMonth(month1, year1);
    var allDates = '';
      for (var i = 1; i <= numberOfDays; i++) {
        //allDates += i + ' ';
        attdate = i+'/'+month1+'/'+year1; 
        attdatetime = moment(attdate, "DD/MM/YYYY").format('x');
        var myDate = new Date();
        myDate.setFullYear(year1);
        myDate.setMonth(month1);
        myDate.setDate(i);
        
        var employeeid = $("#employeeid").val();
       //console.log(mydate.getDay(0));
       var resdata = $.parseJSON($.ajax({
        type: "POST",
        data: {attdatetime: attdatetime,employeeid:employeeid  },
        url: 'getdates.php',
        cache: false,
        success: function (res) {  },
        dataType: "json", 
        async: false
      }).responseText);
      resdata =JSON.stringify(resdata);
      rdata = JSON.parse(resdata);  
      result = rdata.status;  
     // alert(result);        
        if(result == 'success' || myDate.getDay() == 0){
              str1 += '<tr background-color="lightgreen"><td><input type="text" data-name="attdate" class = "form-control " id="attdate" data-role="text" value="'+attdate+'H" readonly></td><td><input type="text" data-name="intime" id="intime" data-role="text" class = "form-control timepicker1" readonly ></td><td><input type="text" data-name="outtime" id="outtime" data-role="text" class = "form-control timepicker2" readonly ></td><td><input type="number" data-name="totalhrs"  data-role="number" id="totalhrs" class = "form-control "  readonly ></td><td><input type="number" data-name="workinghrs" id="workinghrs" value= "9" data-role="number" class = "form-control"  readonly></td><td><input type="number" data-role="number" id="overtime" data-name="overtime" class = "form-control overtime" readonly></td><td><input type="text" data-name="remark"  class = "form-control" readonly ></td></tr>';

           }else{
            str1 += '<tr background-color="lightgreen"><td><input type="text" data-name="attdate" class = "form-control " id="attdate" data-role="text" value="'+attdate+'"></td><td><input type="text" data-name="intime" id="intime" data-role="text" class = "form-control timepicker1" value="9:30 AM" ></td><td><input type="text" data-name="outtime" id="outtime" data-role="text" class = "form-control timepicker2" value="6:30 PM"></td><td><input type="number" data-name="totalhrs"  data-role="number" id="totalhrs" class = "form-control "   ></td><td><input type="number" data-name="workinghrs" id="workinghrs" value= "9" data-role="number" class = "form-control"  ></td><td><input type="number" data-role="number" id="overtime" data-name="overtime" class = "form-control overtime" ></td><td><input type="text" data-name="remark"  class = "form-control" ></td></tr>';
           }
      }
      str1 +='</tbody> ';
    $('#allinputs').append(str+str1);
    
      }

    
});
 */
function getDaysInMonth(month,year) {
  return new Date(year, month, 0).getDate();
}

$('.timepicker1').pickatime({
    interval: 1, 
     
         onClose: function(){
            try{
                var dateStamp = this.get('select')['pick']*60*1000;
               
                  var id = this.get('id');
                  $('#' + id).attr('data-stamp', dateStamp);
                  $('#' + id).parent().find('.timepicker').click();
            }catch(err){

            }
        }
       
    });
  

 $('.timepicker2').pickatime({
    interval: 1, 
     
         onClose: function(){
            try{
                var dateStamp = this.get('select')['pick']*60*1000;
               
                  var id = this.get('id');
                  $('#' + id).attr('data-stamp', dateStamp);
                  $('#' + id).parent().find('.timepicker').click();
            }catch(err){

            }
        }
       
    }); 

/*
$('.timepicker1').pickatime({
    interval: 1, 
         onClose: function(){
            try{
                var dateStamp = this.get('select')['pick']*60*1000;
               
                  var id = this.get('id');
                  $('#' + id).attr('data-stamp', dateStamp);
                  $('#' + id).parent().find('.timepicker').click();
            }catch(err){

            }
        }
        
    });

 */
 
  $("#totalhrs").focusin(function(){
  var intime1 = $("#intime").val();
  var outtime1 = $("#outtime").val();
  var attdate = $("#attdate").val();
  var date1 = new Date(attdate + " "+intime1);
  var intime = date1.getTime();
  var date2 = new Date(attdate + " "+outtime1);
  var outtime = date2.getTime();
 
  var totalhrs = diff_hours(date1, date2);
  $('#totalhrs').val(totalhrs);
  var workinghrs = $("#workinghrs").val();
  $('#overtime').val(totalhrs-workinghrs);
   
//  alert(totalhrs);

      
});

function diff_hours(dt2, dt1) 
 {

  var diff =(dt2.getTime() - dt1.getTime()) / 1000;
  diff /= (60 * 60);
  return Math.abs(Math.round(diff));
  
 }


function empmaster(e){    
    var json1=JSON.parse($('#item-json1').val());
    var branchid=$(e).val();
    var str='<option value="Select">Select</option>';
    for(var i in json1){
        if(json1[i].branchid==branchid){
        str+='<option value="'+json1[i].employeeid+'">'+json1[i].username+'</option>';
        }
    }
    $('#employeeid').html(str);
}
  
$('#btn-submit').on('click',function(){
    var valid=true;
    if(checker('section') !=false ) {
			valid=valid*true;
		}else{
		    valid=valid*false;
         }
    if(valid){
        var data1=checker('section');
         data1 = JSON.stringify(data1);
        $.ajax({
				type: "POST",
				data: {
                    data: data1      
				},
				url: 'insert.php',
				cache: false,
				success: function (res) {
					 //alert(res.status);
						swal({
							type: 'success',
							title: 'Your Work is Save',
							showConfirmButton: false,
							timer: 2000
						});
						setTimeout(function () {
							window.location.reload();
						}, 2000);
                    
				}
			}); 
    } 
})

function getempid(e){
     
    var empid = $(e).val();
    $('#empcode').val(empid);
         
}


</script>