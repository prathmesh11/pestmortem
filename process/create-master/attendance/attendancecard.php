<?php
$base='../../../';
 
$js=' <script src="'.$base.'js/fwork.js"></script>
<script src="'.$base.'js/alart.js"></script> 
<script src="'.$base.'js/list.min.js"></script>
<script src="'.$base.'js/picker.js"></script>
<script src="'.$base.'js/picker.date.js"></script>
<script src="'.$base.'js/picker.time.js"></script>
<script src="'.$base.'js/pdfmake.min.js"></script>
<script src="'.$base.'js/vfs_fonts.js"></script>

<script src="'.$base.'js/moment.js"></script>
 ';

$css='<link rel="stylesheet" href="'.$base.'css/fwork.css">
<link rel="stylesheet" href="'.$base.'css/alart.css">
<link rel="stylesheet" href="'.$base.'css/grid.min.css">
<link rel="stylesheet" href="'.$base.'css/classic.date.css">
<link rel="stylesheet" href="'.$base.'css/classic.time.css">
<link rel="stylesheet" href="'.$base.'css/classic.css">';
include($base.'_in/header.php');
include($base.'_in/connect.php');
$con=_connect();
 $json1='';
$result=mysqli_query($con,"SELECT * FROM employeemaster ORDER BY username ASC");
while($rows = mysqli_fetch_assoc($result)){
    $json1.=',{"branchid":"'.$rows['branchid'].'","employeeid":"'.$rows['employeeid'].'","username":"'.$rows['username'].'"}';
}
$json1=substr($json1,1);
$json1='['.$json1.']';

?>

<style>

hr.style-hr {
    border: 0;
    height: 1px;
    background-image: linear-gradient(to right, rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.75), rgba(0, 0, 0, 0));
}

</style>
<style>
hr.style-hr {
    border: 0;
    height: 1px;
    background-image: linear-gradient(to right, rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.75), rgba(0, 0, 0, 0));
}
.table-list td,
   .table-list th {
       border: 1px solid #ddd;
       padding: 1px !important;
       font-size: 13px;
   }
   .table-list td{
    padding-top: 10px !important;

   }

   .table-list tr:nth-child(even) {
       background-color: #f2f2f2;
   }

   .table-list th {
       padding-top: 5px;
       padding-bottom: 5px;
       text-align: center;
       background-color: #16a085;
       color: white;
   }
</style>
<div class="container-fluid" id="section">
    <div class=" content">
        <h2 align="center" style="margin:0;">Attendance Card</h2>
        <hr class="style-hr">
          <div class="col-sm-10" id="section1">
           <input type="hidden" id="inp-branch" value='<?php echo $branchid; ?>' />
        <input type="hidden" id="item-json1" value='<?php echo str_replace("'"," &#39;",$json1); ?>' />

            <div class="col-sm-3"></div>
            <div class="col-sm-6">
            <div class="col-sm-4">Branch Name</div>
                <div class="col-sm-8">
                    <div class="form-group">
                    <select class="form-control input-sm" data-role="select" data-name="branchid" id="branchid" onchange="empmaster(this);" >
                        <option value="Select">Select</option>
                            <?php 
                                    $result1=mysqli_query($con, "SELECT * FROM `branchmaster`");
                                    while($rows=mysqli_fetch_assoc($result1)) {
                                    echo '<option value="'.$rows['branchid'].'">'.$rows['branchname'].'</option>';
                                }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="col-sm-4">Employee Name </div>
                <div class="col-sm-8">
                    <div class="form-group">
                        <select class="form-control input-sm" data-role="select" data-name="employeeid" id="employeeid"  >
                        <option value="Select">Select</option>
                         
                    </select>
                    </div>
                </div>

            </div>
           
            <div class="col-sm-3"></div>
        </div>
        <div class="col-sm-10" id="section2">
         <div class="col-sm-3"></div>
            <div class="col-sm-6">
                <div class="col-sm-4">Month / Year</div>
                <div class="col-sm-8">
                    <div class="form-group">
                        <div class="col-sm-6">
  
                           <select class="form-control input-sm" data-name="month" >
                            <option value="Select">Select</option>
                           <?php
                                for ($i = 0; $i < 12; $i++) {
                                    $time = strtotime(sprintf('%d months', $i));   
                                    $label = date('F', $time);   
                                    $value = date('n', $time);
                                    if($value == date('m')){
                                        echo "<option value='$value' selected>$label</option>";
                                    }else{
                                        echo "<option value='$value'>$label</option>";    
                                    }
                                    
                                }
                                ?>
                        </select> 
                        </div>
                           <div class="col-sm-6">
                           <select class="form-control input-sm" data-name="month" >
                            <option value="Select">Select</option>
                           <?php
                                foreach(range(2000, (int)date("Y")) as $year) {
                                    if($year == date("Y")){
                                        echo "\t<option value='".$year."' selected>".$year."</option>\n\r";
                                    }else{
                                        echo "\t<option value='".$year."'>".$year."</option>\n\r";
                                        }
                                
                            }
                                ?>
                        </select>
                        <br>
                        </div>
                     
                    </div>
                </div>

            </div>
           
            <div class="col-sm-3"></div>
        </div>
        
         
          
         
                       
        <div style="clear:both;"></div>
        <hr class="style-hr">
        <div class="col-sm-3"></div>
        <div class="col-sm-6">
             <div id="pdfdiv" class="col-sm-4"><button class="btn btn-primary btn-sm btn-block" id="btn-pdf" style="margin:5px;">Download</button></div> 
            <div class="col-sm-4"><button class="btn btn-danger btn-sm btn-block" id="btn-reset"
                    onclick="$('#section').find('select').val('');$('#section').find('input').val('');" style="margin:5px;">Reset</button></div>
                   
        </div>
        <div class="col-sm-3"></div>
        <table id="attrows" class="table-list table table-bordered">

    <tbody>
      
    </tbody>
  </table>
    </div>
</div>

<?php
include($base.'_in/footer.php');
?>
<style type="text/css">
.picker__select--year{
  height:auto;
}

.picker__select--month{
  height:auto;
}
</style>
<script>

  function empmaster(e){    
    var json1=JSON.parse($('#item-json1').val());
    var branchid=$(e).val();
    var str='<option value="Select">Select</option>';
    for(var i in json1){
        if(json1[i].branchid==branchid){
        str+='<option value="'+json1[i].employeeid+'">'+json1[i].username+'</option>';
        }
    }
    $('#employeeid').html(str);
}
 /*
$('#btn-pdf').on('click',function(){
  var valid=true;
  if(checker('section') !=false ) {
            valid=valid*true;
        }else{
            valid=valid*false;
         }

         var empid= $("#employeeid").val();
       if(valid){
        var data1=checker('section');
         data1 = JSON.stringify(data1);
        var jsondata = $.parseJSON($.ajax({
                type: "POST",
                data: {
                    data1: data1      
                },
                url: 'attendancedetails.php',
                
                dataType: "json", 
                async: false,
                success: function (response) {
                
                }
            }).responseText);

        jsondata =JSON.stringify(jsondata);
        data1 = JSON.parse(jsondata);  
        json1 = data1.json;  

        var fullDate = new Date();
        var twoDigitMonth = ((fullDate.getMonth().length + 1) === 1) ? (fullDate.getMonth() + 1) : '' + (fullDate.getMonth() + 1);
       // var currentDate = fullDate.getDate() + "/" + twoDigitMonth + "/" + fullDate.getFullYear();
        var content=[]; 
        var totalhrs = '9';
        var hrsworked = '9';

         dd = {
                        

                    pageSize: 'A4',
                    pageMargins: [40, 30, 30, 40],
                   
                    content: [
                          '\n',
                            {
                                text: 'Attendance Details',
                                 fontSize: 10,
                                      bold: true,
                                      alignment: 'center',
                            },
 
                              table: {
                                body: [[table(json1,  ['Date', 'In Time','Outtime','Remarks','Over Time'])]]
                              }                                                   
                           ]
                  }
                              
               //pdfMake.createPdf(dd).download("Attendance_"+empid+".pdf");
             pdfMake.createPdf(dd).open();



      }

});
*/
function empmaster(e){    
    var json1=JSON.parse($('#item-json1').val());
    var branchid=$(e).val();
    var str='<option value="Select">Select</option>';
    for(var i in json1){
        if(json1[i].branchid==branchid){
        str+='<option value="'+json1[i].employeeid+'">'+json1[i].username+'</option>';
        }
    }
    $('#employeeid').html(str);
    
       
}
function buildTableBody(data, columns) {
          var body = [];

          body.push(columns);

          data.forEach(function(row) {
              var dataRow = [];

              columns.forEach(function(column) {
                  dataRow.push(row[column].toString());
              })

              body.push(dataRow);
          });

          return body;
      }

      function table(data, columns) {
          return {
              table: {
                  headerRows: 1,
                  body: buildTableBody(data, columns)
              }
          };
      }
$('#btn-submit').on('click',function(){
    var valid=true;
    if(checker('section') !=false ) {
            valid=valid*true;
        }else{
            valid=valid*false;
         }
         var empid= $("#empname").val();
    if(valid){
        var data1=checker('section');
         data1 = JSON.stringify(data1);
        var jsondata = $.parseJSON($.ajax({
                type: "POST",
                data: {
                    data1: data1      
                },
                url: 'attendancedetails.php',
                
                dataType: "json", 
                async: false,
                success: function (response) {
                
                }
            }).responseText);

        jsondata =JSON.stringify(jsondata);
        data1 = JSON.parse(jsondata);  
        json1 = data1.json;   

        // alert(json1);
        $('#attrows').html('<thead><tr><th>SR. No</th> <th>Date</th><th>In Time</th><th>Out Time</th><th>Total Hrs</th><th>Remark</th><th>OT Hours</th><th>Hrs Worked</th></tr> </thead>');
         var trHTML = '';
         $.each(json1, function (i, item) {
                var totalhrs = item.outtime - item.intime;
                var hrsworked = totalhrs + parseInt(item.overtime);
                // var time = moment(item.attendancedate).format("DD-MM-YYYY");
                  var intime = new Date(item.intime).toLocaleTimeString();
                  var outtime = new Date(item.outtime).toLocaleTimeString();
                  var attdate = new Date(item.attendancedate).toDateString();
                  var date = new Date(parseInt(item.attendancedate)* 1000);
                var formattedDate = ('0' + date.getDate()).slice(-2) + '/' + ('0' + (date.getMonth() + 1)).slice(-2) + '/' + date.getFullYear() ;

                        trHTML += '<tr><td align="center">' + (i+1)+ '</td><td align="center">' + formattedDate + '</td><td align="center">' + intime+ '</td><td align="center">' + outtime + '</td><td align="center">' + totalhrs + '</td><td align="center">' + item.remark + '</td><td align="center">' + item.overtime + '</td><td align="center">' + hrsworked + '</td></tr>';
                    });
                    $('#attrows').append(trHTML);
    }
    //alert(trHTML);
})

function getempid(e){
     
    var empid = $(e).val();
    $('#empcode').val(empid);


         
}


</script>