<?php
$base='../../../';
 
$js=' <script src="'.$base.'js/fwork.js"></script>
<script src="'.$base.'js/alart.js"></script> 
<script src="'.$base.'js/list.min.js"></script>
<script src="'.$base.'js/picker.js"></script>
<script src="'.$base.'js/picker.date.js"></script>
<script src="'.$base.'js/picker.time.js"></script>
<script src="'.$base.'js/pdfmake.min.js"></script>
<script src="'.$base.'js/vfs_fonts.js"></script>
 ';

$css='<link rel="stylesheet" href="'.$base.'css/fwork.css">
<link rel="stylesheet" href="'.$base.'css/alart.css">
<link rel="stylesheet" href="'.$base.'css/grid.min.css">
<link rel="stylesheet" href="'.$base.'css/classic.date.css">
<link rel="stylesheet" href="'.$base.'css/classic.time.css">
<link rel="stylesheet" href="'.$base.'css/classic.css">';
include($base.'_in/header.php');
include($base.'_in/connect.php');
$con=_connect();
?>

<style>

hr.style-hr {
    border: 0;
    height: 1px;
    background-image: linear-gradient(to right, rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.75), rgba(0, 0, 0, 0));
}

</style>
<style>
hr.style-hr {
    border: 0;
    height: 1px;
    background-image: linear-gradient(to right, rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.75), rgba(0, 0, 0, 0));
}
.table-list td,
   .table-list th {
       border: 1px solid #ddd;
       padding: 1px !important;
       font-size: 13px;
   }
   .table-list td{
	padding-top: 10px !important;

   }

   .table-list tr:nth-child(even) {
       background-color: #f2f2f2;
   }

   .table-list th {
       padding-top: 5px;
       padding-bottom: 5px;
       text-align: center;
       background-color: #16a085;
       color: white;
   }
</style>
<div class="container-fluid" id="section">
    <div class=" content">
        <h2 align="center" style="margin:0;">Daily Attendance</h2>
        <hr class="style-hr">
        <div class="col-sm-5">
        <div class="col-sm-12">
             
                <div class="col-sm-4">Date</div>
                <div class="col-sm-8">
                    <div class="form-group">
                      
                    <input type="text"  data-name="attendancedate" id="attendancedate" class="form-control sm">
                    <input type="hidden" data-name="attendancedate-stamp" id="attendancedate-stamp">
       
                    </div>
                </div>

           
            
        </div>
        
          <div class="col-sm-12">
           
                <div class="col-sm-4">Employee Name </div>
                <div class="col-sm-8">
                    <div class="form-group">
                     
                        <select class="form-control input-sm"   data-name="empname" id="empname" onchange="getempid(this);">
                        <option value="Select">Select</option>
                       
                         <?php 
                    $result1=mysqli_query($con, "SELECT * FROM `employeemaster`");
                    while($rows=mysqli_fetch_assoc($result1)) {
                        echo '<option value="'.$rows['employeeid'].'">'.$rows['username'].'</option>';
                    }  
                        ?>
                    </select>
                    </div>
                
                    </div>
            </div>
           
             
         <div class="col-sm-12" >
             
                <div class="col-sm-4">Employee Code </div>
                <div class="col-sm-8">
                    <div class="form-group">
                     
                        <div class="form-group"><input type="text"  data-name="empcode" id="empcode" class="form-control input-sm" readonly></div>
               
                    </div>
                </div>
 
           </div>
          </div>
          
        <div class="col-sm-6">
        <div class="col-sm-12"  >

             
                <div class="col-sm-4">In Time</div>

                <div class="col-sm-4">Out Time</div>
 
                <div class="col-sm-4">Over Time</div>
     
                <div class="col-sm-4">
                    <div class="form-group">
                     
                     <input type="text"  data-name="intime" id="intime" class="form-control">
                     <input type="hidden" data-name="intime-stamp" id="intime-stamp">
                     
                    </div>
                </div>
                 <div class="col-sm-4">
                    <div class="form-group">
                     
                     <input type="text"  data-name="outtime" id="outtime" class="form-control ">
                      <input type="hidden"  data-name="outtime-stamp" id="outtime-stamp">
                    
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                     
                     <input type="text" data-name="overtime" id="overtime" class="form-control" placeholder="In Hours">
                      
                    </div>
                </div>
                 <div class="col-sm-12">
                 <div class="col-sm-6">Remark</div>
 
                    <div class="form-group">
                     
                     <input type="text" data-name="remark" id="remark" class="form-control input-sm"> 
                    </div>
                </div>
 
            
        </div>
        
        </div>    
           
                       
        <div style="clear:both;"></div>
        <hr class="style-hr">
        <div class="col-sm-3"></div>
        <div class="col-sm-6">
            <div class="col-sm-6"><button class="btn btn-success btn-sm btn-block" id="btn-submit"
                    style="margin:5px;">Submit</button></div>
            <div class="col-sm-6"><button class="btn btn-danger btn-sm btn-block" id="btn-reset"
                    onclick="$('#section').find('select').val('');$('#section').find('input').val('');" style="margin:5px;">Reset</button></div>
        </div>
        <div class="col-sm-3"></div>
        <table class="table-list table table-bordered">
    <thead>
      <tr>
        <th>SR. No</th>
        <th>Emp Code</th>
        <th>Emp Name</th>
        <th>Attendance Date</th>
        <th>In Time</th>
        <th>Out Time</th>
        <th>Over Time</th>
        
        <th>Remarks</th>
         
      </tr>
    </thead>
    <tbody>
    <?php
                    $count=0;
                    $result4=mysqli_query($con,"SELECT * FROM attendancemaster ORDER BY id ASC" );
                     while($rows3 = mysqli_fetch_assoc($result4)){
                         $count++;

                         $resemp=mysqli_query($con,"SELECT * FROM employeemaster WHERE employeeid = '".$rows3['empcode']."'" );
                         $rowsemp = mysqli_fetch_assoc($resemp);
                         $empname =  $rowsemp['username'];


                      $attdate= date('d/m/Y',$rows3['attendancedate']/1000);
                        $intime = date('H:i a', $rows['intime']);
                        $outtime = date('H:i a', $rows['outtime']);
                        
                     echo '<tr>';
                     echo '<td align="center">'.$count.'</td>';
                     echo '<td align="center">'.$rows3['empcode'].'</td>';
                     echo '<td align="center">'.$empname.'</td>';
                     
                     echo '<td align="center">'.$attdate.'</td>';
                     
                     echo '<td align="center">'.$intime.'</td>';
                     echo '<td align="center">'.$outtime.'</td>';
                     echo '<td align="center">'.$rows3['overtime'].'</td>';
                     echo '<td align="center">'.$rows3['remark'].'</td>';
                     
                     echo '<tr>';
                    }
    ?>
    </tbody>
  </table>
    </div>
</div>

<?php
include($base.'_in/footer.php');
?>
<script>
var attendancedate = $("#attendancedate").pickadate({
         labelMonthNext: 'Go to the next month',
          labelMonthPrev: 'Go to the previous month',
          labelMonthSelect: 'Pick a month from the dropdown',
          labelYearSelect: 'Pick a year from the dropdown',
          selectMonths: true,
          selectYears: 100,
        onClose: function(){
            try{
                var dateStamp = this.get('select')['pick'];
                $("#attendancedate-stamp").val(dateStamp);
            }catch(err){

            }
        }
    });


var intimeval = $('#intime').pickatime({
    interval: 1, 
          onClose: function(){
            try{
                var dateStamp = this.get('select')['pick']*60*1000;
                console.log(dateStamp);
                $("#intime-stamp").val(dateStamp);
            }catch(err){

            }
        }
    });
var outtimeval = $('#outtime').pickatime({
    interval: 1, 
         onClose: function(){
            try{
                var dateStamp = this.get('select')['pick']*60*1000;
                $("#outtime-stamp").val(dateStamp);
            }catch(err){

            }
        }
        
    });

$('#btn-submit').on('click',function(){
    var valid=true;
    if(checker('section') !=false ) {
			valid=valid*true;
		}else{
		    valid=valid*false;
         }
    if(valid){
        var data1=checker('section');
         data1 = JSON.stringify(data1);
        $.ajax({
				type: "POST",
				data: {
                    data: data1      
				},
				url: 'insert.php',
				cache: false,
				success: function (res) {
					 //alert(res.status);
						swal({
							type: 'success',
							title: 'Your Work is Save',
							showConfirmButton: false,
							timer: 2000
						});
						setTimeout(function () {
							window.location.reload();
						}, 2000);
                    
				}
			});
    }else{
        alert("here");
    }
})

function getempid(e){
     
    var empid = $(e).val();
    $('#empcode').val(empid);
         
}


</script>