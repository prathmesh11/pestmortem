<?php
    
    $base = '../../../';

    include($base.'_in/connect.php');

    header('content-type: application/json; charset=utf-8');

    header("access-control-allow-origin: *");

    if(isset($_POST['data'])){

        $con   = _connect();

        $data1 = get_object_vars(json_decode($_POST["data"]));

        if (session_status()==PHP_SESSION_NONE) { session_start(); }

        $created_by     = $_SESSION['employeeid'];

        $branchid       = $data1['branchid'];

        $stockid        = $data1['stockid'];

        $maxlevel       = $data1['maxlevel'];

        $minlevel       = $data1['minlevel'];

        $reorderlevel   = $data1['reorderlevel'];

        $reorderqty     = $data1['reorderqty'];

        $openingbal     = $data1['openingbal'];

        $openingbaldate = $data1['openingbaldate'];

        $opning         = $openingbal;

        $quality        = 0;
        
        $issue          = 0;
        
        $adjust_plus    = 0;
        
        $adjust_minus   = 0;
        
        $outbranch      = 0;
        
        $outqty         = 0;
        
        $inbranch       = 0;
        
        $inqty          = 0;
        
        $total          = $openingbal;

        $sel            = mysqli_fetch_assoc(mysqli_query($con,"SELECT * FROM `livestockmaster` WHERE itemcode='$stockid' AND branchid='$branchid' ORDER BY id DESC LIMIT 1"));

        if($sel){

            $opning       = $openingbal;

            $quality      = $sel['quality'];

            $issue        = $sel['issue'];

            $adjust_plus  = $sel['adjust_plus'];

            $adjust_minus = $sel['adjust_minus'];

            $outbranch    = $sel['outbranch'];

            $outqty       = $sel['outqty'];

            $inbranch     = $sel['inbranch'];

            $inqty        = $sel['inqty'];

            $total        = ($opning+$quality+$adjust_plus+$inqty)-($issue+$adjust_minus+$outqty);

        }

        
        $check = mysqli_fetch_assoc(mysqli_query($con,"SELECT stockid x FROM stockordermaster WHERE stockid='$stockid' And branchid='$branchid'"))['x'];
            

        if($check){

            if ($openingbal == 0) {

                $results = mysqli_query($con,"UPDATE stockordermaster SET maxlevel='$maxlevel',minlevel='$minlevel',reorderlevel='$reorderlevel',reorderqty='$reorderqty',openingbal='$openingbal',openingbaldate='$openingbaldate' WHERE stockid='$stockid' And branchid='$branchid'");

                if($results){

                    echo '{"status":"success"}';

                } else {

                    echo '{"status":"falid1"}';
                }

            } else {

                $results = mysqli_query($con,"UPDATE stockordermaster SET maxlevel='$maxlevel',minlevel='$minlevel',reorderlevel='$reorderlevel',reorderqty='$reorderqty' WHERE stockid='$stockid' And branchid='$branchid'");

                if($results){

                    echo '{"status":"success"}';

                } else {

                    echo '{"status":"falid1"}';

                }

            }

        } else {
                

            mysqli_query($con,"INSERT into livestockmaster ( branchid,itemcode,opning,quality,issue,adjust_plus,adjust_minus,outbranch,outqty,inbranch,inqty,total,created_by,created_time) Values ('$branchid','$stockid','$opning','$quality','$issue','$adjust_plus','$adjust_minus','$outbranch','$outqty','$inbranch','$inqty','$total','$created_by','$CURRENT_MILLIS')");
            
            $results = mysqli_query($con,"INSERT into stockordermaster (branchid,stockid,maxlevel,minlevel,reorderlevel,reorderqty,openingbal,openingbaldate,created_by,created_time) Values ('$branchid','$stockid','$maxlevel','$minlevel','$reorderlevel','$reorderqty','$openingbal','$openingbaldate','$created_by','$CURRENT_MILLIS')");

            if($results){

                mysqli_query($con,"INSERT into batchnos ( branchid,batchno,itemid,quntity,created_by,created_time) Values ('$branchid','Opening Balance Batch','$stockid','$opning','$created_by','$CURRENT_MILLIS')");

                echo '{"status":"success"}';

            } else {

                echo '{"status":"falid2"}';

            }

        }
         
        _close($con);

    } else {

        echo '{"status":"falid3"}';

    }

?>