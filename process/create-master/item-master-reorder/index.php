<?php

    $base = '../../../';

    $js = '<script src="'.$base.'js/fwork.js"></script>
    <link href="'.$base.'/css/select2.min.css" rel="stylesheet" />
    <script src="'.$base.'js/alart.js"></script>';

    $css = '<link rel="stylesheet" href="'.$base.'css/fwork.css">
    <link rel="stylesheet" href="'.$base.'css/alart.css">';

    include($base.'_in/header.php');

    include($base.'_in/connect.php');

    if (!in_array("Item Master ReOrder", $arrysession)) {
   
        echo "<script>window.location.href='/process/dashboard.php';</script>";
       
        exit;
    
    }
 
    $con = _connect();

?>

<style>

hr.style-hr {
    border: 0;
    height: 1px;
    background-image: linear-gradient(to right, rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.75), rgba(0, 0, 0, 0));
}

</style>

<div class="container-fluid" id="section">

    <div class="content">

        <h2 align="center" style="margin:0;">Re-Order Maintainance</h2>

        <hr class="style-hr">

        <div class="" id="section1">

            <div class="col-sm-3"></div>

            <div class="col-sm-6">

                <div class="col-sm-4">Branch</div>

                <div class="col-sm-8">

                    <div class="form-group">

                        <select class="form-control input-sm select-js" data-name="branchid" id="branchid" >

                            <?php 
                            
                                $result1=mysqli_query($con, "SELECT * FROM `branchmaster`");

                                while($rows=mysqli_fetch_assoc($result1)) {
                            
                                    echo '<option value="'.$rows['branchid'].'">'.$rows['branchname'].'</option>';
                                    
                                }
                            ?>

                        </select>

                    </div>

                </div>

                <div class="col-sm-4">Main Group</div>

                <div class="col-sm-8">

                    <div class="form-group">
                        
                        <select class="form-control input-sm select-js" data-role="select" onchange="groupcode(this)" data-name="groupcode1">

                            <option value="Select">Select</option>
                            
                            <?php 
                                    
                                $result1=mysqli_query($con, "SELECT * FROM `mitemgroup` WHERE mainGroupCode='MG' AND groupCode<>'MG'");
                                
                                while($rows=mysqli_fetch_assoc($result1)) {

                                    echo '<option value="'.$rows['groupCode'].'">'.$rows['groupName'].'</option>';

                                }
                            ?>
                        </select>

                    </div>

                </div>

                <div class="col-sm-4">Item Name</div>

                <div class="col-sm-8">

                    <div class="form-group">
                        
                        <select class="form-control input-sm select-js" data-role="select" id="stockid" onchange="itemcode(this)" data-name="stockid">

                            <option value="Select">Select</option>

                        </select>

                    </div>

                </div>

                <div class="col-sm-4">Max Level</div>

                <div class="col-sm-8">

                    <div class="form-group">
                        
                        <input type="text" data-role="text" data-name="maxlevel" class="form-control input-sm">
                        
                    </div>

                </div>

                <div class="col-sm-4">MIN Level</div>

                <div class="col-sm-8">

                    <div class="form-group">
                        
                        <input type="text" data-role="text" data-name="minlevel" class="form-control input-sm">
                    
                    </div>

                </div>

                <div class="col-sm-4">Re-Order Level</div>

                <div class="col-sm-8">

                    <div class="form-group">
                        
                        <input type="text" data-role="text" data-name="reorderlevel" class="form-control input-sm">

                    </div>

                </div>

                <div class="col-sm-4">Re-Order Qty</div>

                <div class="col-sm-8">

                    <div class="form-group">
                        
                        <input type="text" data-role="number" data-name="reorderqty" class="form-control input-sm">
                    
                    </div>

                </div>

                <div class="col-sm-4">Opening Balance</div>

                <div class="col-sm-8">

                    <div class="form-group">
                        
                        <input type="text" data-role="number" data-name="openingbal" class="form-control input-sm">
                    
                    </div>

                </div>

                <div class="col-sm-4">Opening Balance date</div>

                <div class="col-sm-8">

                    <div class="form-group">
                        
                        <input type="date" data-name="openingbaldate" class="form-control input-sm">
                    
                    </div>
                
                </div>

            </div>

            <div class="col-sm-3"></div>

        </div>

        <div style="clear:both;"></div>

        <hr class="style-hr">

        <div class="col-sm-3"></div>

        <div class="col-sm-6">

            <div class="col-sm-6">
                
                <button class="btn btn-success btn-sm btn-block" id="btn-submit" style="margin:5px;">Submit</button>

            </div>

            <div class="col-sm-6">
                
                <button class="btn btn-danger btn-sm btn-block" id="btn-reset" style="margin:5px;">Reset</button>
            
            </div>

        </div>

        <div class="col-sm-3"></div>

    </div>

</div>

<?php

    include($base.'_in/footer.php');

?>

<script>

function groupcode(e){

    var groupcode1 = $(e).val();

    $.ajax({

        type: "POST",

        data:'groupcode1='+groupcode1,

        url: 'itemname.php',

        cache: false,

        success: function (res) {

            if (res.status == 'success') {

                var json = res.json;

                var str = '<option value="Select">Select</option>';

                for(var i in json){

                    str+='<option value="'+json[i].stockid+'">'+json[i].itemname+'</option>';

                }

                $('#stockid').html(str);

            }

        }

    })

}

function itemcode(e) {

    var stockid  = $(e).val();

    var branchid = $('#branchid').val();

    $.ajax({

        type: "POST",

        data: 'stockid=' + stockid + '&branchid=' + branchid,

        url: 'select.php',

        cache: false,

        success: function (res) {

            if (res.status == 'success') {

                modals.putvalue('section', res.json);

            }

        }

    })
    
}

$('#btn-submit').on('click',function(){

    var valid = true;

    if(checker('section') !=false ) {

		valid=valid*true;

	} else {

		valid=valid*false;

    }

    if(valid){

        var data1 = checker('section');

         data1    = JSON.stringify(data1);

        $.ajax({

			type: "POST",

			data: {

                data: data1

			},

			url: 'insert.php',

			cache: false,

			success: function (res) {
					
                if (res.status == 'success') {

                    swal({

                        type: 'success',
                        title: 'Your Work is Save',
                        showConfirmButton: false,
                        timer: 2000

                    });

                    setTimeout(function () {

                        window.location.reload();

                    }, 2000);

                }

			}

		});

    }
    
})

$(document).ready(function() {

    $('.select-js').select2({width: '100%', tags: true});

    $('.select').attr('style','width:100%!important;');
    
});

</script>