<?php
$base='../../../';
$js='<script src="'.$base.'js/fwork.js"></script>
<script src="'.$base.'js/alart.js"></script>
<script src="'.$base.'js/list.min.js"></script>';
$css='<link rel="stylesheet" href="'.$base.'css/fwork.css">
<link rel="stylesheet" href="'.$base.'css/grid.min.css">
<link rel="stylesheet" href="'.$base.'css/alart.css">';

include($base.'_in/header.php');
include($base.'_in/connect.php');

if (!in_array("Branch Master", $arrysession)) {
   
    echo "<script>window.location.href='/process/dashboard.php';</script>";
   
    exit;

}

$con=_connect();

?>
<style>
hr.style-hr {
    border: 0;
    height: 1px;
    background-image: linear-gradient(to right, rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.75), rgba(0, 0, 0, 0));
}

.table-list td,
   .table-list th {
       border: 1px solid #ddd;
       padding: 2px;
       font-size: 13px;
   }

   .table-list tr:nth-child(even) {
       background-color: #f2f2f2;
   }

   .table-list th {
       padding-top: 12px;
       padding-bottom: 12px;
       text-align: left;
       background-color: #16a085;
       color: white;
   }
   .sidenav1 {
       height: 100%;
       width: 0;
       position: fixed;
       z-index: 1;
       top: 0;
       left: 0;
       background-color: #111;
       overflow-x: hidden;
       transition: 0.5s;
       padding-top: 60px;
   }

   .sidenav1 a {
       padding: 8px 8px 8px 32px;
       text-decoration: none;
       font-size: 25px;
       color: #818181;
       display: block;
       transition: 0.3s;
   }

   .sidenav1 a:hover {
       color: #f1f1f1;
   }

   .sidenav1 .closebtn1 {
       position: absolute;
     
       right: 25px;
       font-size: 36px;
       margin-left: 50px;
   }

   @media screen and (max-height: 450px) {
       .sidenav1 {
           padding-top: 15px;
       }

       .sidenav1 a {
           font-size: 18px;
       }
   }

   .list {
       padding: 0px;
   }

   .list li span {
       display: inline;
   }

   .list li {
       text-decoration: none;
       display: block;
       padding: 2px;
       background: #FFF;
       border: 1px solid #333;
       color: #000;
   }
</style>

<div class="container-fluid">
    <div class=" content" id="section">

        <span style="font-size:24px;cursor:pointer" onclick="openNav1()">&#9776; Search</span>
        <div id="mysidenav1" class="sidenav1">

            <div id="test-list" style="padding:5px;">
                <a href="javascript:void(0)" class="closebtn1" onclick="closeNav1()">&times;</a>
                <br><br><br>
                <div class="form-group">
                    <input type="text" placeholder="Search" class="form-control input-sm fuzzy-search"
                        style="border-radius:3px;">
                </div>
                <ul class="list">
                    <?php 
                    $result1=mysqli_query($con, "SELECT * FROM `branchmaster` ORDER BY branchname");
                    while($rows=mysqli_fetch_assoc($result1)) {
                        echo '<li><span class="branchmaster">' .$rows['branchname']. '<span><button style="margin-left:2px;" data-branchid="' .$rows['branchid']. '" onclick="edit(this)" class="btn btn-primary btn-sm"><span class="fa fa-edit"></span> E</button></i>';
                    }
                ?>
                </ul>
            </div>
        </div>

        <h2 align="center" style="margin-top:0px;">Create Branch Master</h2>

        <div style="clear:both;"></div>
        <hr class="style-hr">

        <div class="col-sm-5">
            <center> Branch Details</center>
            <hr class="style-hr">
            <div class="col-sm-5">Branch Name</div>
            <div class="col-sm-7">
                <div class="form-group"><input type="text" data-role='text' data-name='companybranchname'
                        class="form-control input-sm">
                    <input type="text" style="display:none;" id='branchid'>
                </div>
            </div>
            <div class="col-sm-5">Branch Short Name</div>
            <div class="col-sm-7">
                <div class="form-group"><input type="text" data-role='text' data-name='branchshortname'
                        class="form-control input-sm">
                </div>
            </div>
            <div class="col-sm-5">address</div>
            <div class="col-sm-7">
                <div class="form-group"><textarea data-role='textarea' data-name='address'
                        class="form-control input-sm"> </textarea></div>
            </div>
            <div class="col-sm-5">Phone No.</div>
            <div class="col-sm-7">
                <div class="form-group"><input type="text" data-role='text' data-name='phone'
                        class="form-control input-sm"></div>
            </div>
            <div class="col-sm-5">E.mail id</div>
            <div class="col-sm-7">
                <div class="form-group"><input type="text" data-role='text' data-name='email'
                        class="form-control input-sm"></div>
            </div>
            <div class="col-sm-5">Country</div>
            <div class="col-sm-7">
                <div class="form-group">
                    <select class="form-control input-sm" data-role="select" data-name="country">
                        <option value="IN">India</option>
                        <?php 
                    $result1=mysqli_query($con, "SELECT * FROM `countrymaster`");
                    while($rows=mysqli_fetch_assoc($result1)) {
                        echo '<option value="'.$rows['countrycode'].'">'.$rows['countryname'].'</option>';
                    }
                        ?>
                    </select>
                </div>
            </div>
            <div class="col-sm-5">State</div>
            <div class="col-sm-7">
                <div class="form-group">
                    <select class="form-control input-sm" data-role="select" id="state" data-name="state">
                        <option value="Select">Select</option>
                        <?php 
                    $result1=mysqli_query($con, "SELECT * FROM `statemaster`");
                    while($rows=mysqli_fetch_assoc($result1)) {
                        echo '<option value="'.$rows['statecode'].'">'.$rows['statename'].'</option>';
                    }
                        ?>
                        <option value="00">OTHER</option>
                    </select>
                </div>
            </div>
            <div class="col-sm-5">GST/HST/VAT Number</div>
            <div class="col-sm-7">
                <div class="form-group"><input type="text" data-name='gstnumber' class="form-control input-sm"></div>
            </div>
            <div style="clear:both;"></div>
            <div class="col-sm-5">ALP Registration No.</div>
            <div class="col-sm-7">
                <div class="form-group"><input type="text" data-role='text' data-name='alpreg'
                        class="form-control input-sm"></div>
            </div>
            <div class="col-sm-5">MBR Registration No.</div>
            <div class="col-sm-7">
                <div class="form-group"><input type="text" data-role='text' data-name='mbrreg'
                        class="form-control input-sm"></div>
            </div>
            <div class="col-sm-5">AFAS Registration No.</div>
            <div class="col-sm-7">
                <div class="form-group"><input type="text" data-name='afasreg' class="form-control input-sm"></div>
            </div>
            <div class="col-sm-5">AEI Registration No.</div>
            <div class="col-sm-7">
                <div class="form-group"><input type="text" data-name='aeireg' class="form-control input-sm"></div>
            </div>
            <div class="col-sm-5">LUT No.</div>
            <div class="col-sm-7">
                <div class="form-group"><input type="text" data-name='lutno' class="form-control input-sm"></div>
            </div>
        </div>
        <div class="col-sm-7">

            <div class="col-sm-12" style="display:none;">
                <center> Contact Person Details</center>
                <hr class="style-hr">
                <center><button type="button" class="btn btn-primary btn-sm" data-toggle="modal"
                        data-target="#myModal">Add
                        + </button></center>
                <br>
                <div class="table-responsive">
                    <table id="contact-table" class="table table-list">
                        <thead>
                            <tr>
                                <th>SR.</th>
                                <th>Name</th>
                                <th>Department</th>
                                <th>Mobile</th>
                                <th>Landline</th>
                                <th>Email</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="col-sm-12">
                <center>Bank Details</center>
                <hr class="style-hr">
                <div class="col-sm-5">Bank Name</div>
                <div class="col-sm-7">
                    <div class="form-group"><input type="text" data-role='text' data-name='bankname'
                            class="form-control input-sm"></div>
                </div>
                <div class="col-sm-5">Branch Name</div>
                <div class="col-sm-7">
                    <div class="form-group"><input type="text" data-role='text' data-name='bankbranchname'
                            class="form-control input-sm"></div>
                </div>
                <div class="col-sm-5">Branch Address</div>
                <div class="col-sm-7">
                    <div class="form-group"><textarea data-role='textarea' data-name='bankaddress'
                            class="form-control input-sm"> </textarea></div>
                </div>
                <div class="col-sm-5">IFSC Code</div>
                <div class="col-sm-7">
                    <div class="form-group"><input type="text" data-role='text' data-name='ifsccode'
                            class="form-control input-sm"></div>
                </div>
                <div class="col-sm-5">Account Number</div>
                <div class="col-sm-7">
                    <div class="form-group"><input type="text" data-role='text' data-name='accountno'
                            class="form-control input-sm"></div>
                </div>

                <div class="col-sm-5">Upload Signature </div>
                <div class="col-sm-5">
                  <input type="file" id="myFile" name="filename">
                </div>

                <div class="col-sm-2" id="signature" style="display:none;">
                    <a class="btn btn-info btn-sm" target="_blank_" href="" id="show_sign">Show Sign</a>
                </div>

                

            </div>
            <div class="col-sm-12">
                <div style="clear:both;"></div>
                <hr class="style-hr">

                <div class="col-sm-3">
                </div>
                <div class="col-sm-3"><button class="btn btn-success btn-sm btn-block" id="btn-submit"
                        onclick="submit()" style="margin:5px;">Submit</button></div>
                <div class="col-sm-3"><button class="btn btn-danger btn-sm btn-block" id="btn-reset"
                        style="margin:5px;">Reset</button></div>
                <div class="col-sm-3"></div>
            </div>
        </div>


    </div>
</div>
<br>
<br>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" align="center">Add Contact</h4>
            </div>
            <div class="modal-body" id='modal'>
                <div class="row">
                    <div class="col-sm-5">Person Name</div>
                    <div class="col-sm-7">
                        <div class="form-group"><input type="text" data-role='text' data-name='name'
                                class="form-control input-sm"></div>
                    </div>
                    <div class="col-sm-5">Department</div>
                    <div class="col-sm-7">
                        <div class="form-group"><input type="text" data-role='text' data-name='department'
                                class="form-control input-sm"></div>
                    </div>
                    <div class="col-sm-5">Mobile No.</div>
                    <div class="col-sm-7">
                        <div class="form-group"><input type="text" data-name='mobile' class="form-control input-sm">
                        </div>
                    </div>
                    <div class="col-sm-5">Landline No.</div>
                    <div class="col-sm-7">
                        <div class="form-group"><input type="text" data-name='landline' class="form-control input-sm">
                        </div>
                    </div>
                    <div class="col-sm-5">Email</div>
                    <div class="col-sm-7">
                        <div class="form-group"><input type="text" data-role='text' data-name='email'
                                class="form-control input-sm"></div>
                    </div>
                    <div style="clear:both;"></div>
                    <hr>
                    <div class="col-sm-6"><button class="btn btn-success btn-sm btn-block" id="btn-contact"
                            style="margin:5px;">Submit</button></div>
                    <div class="col-sm-6"><button class="btn btn-danger btn-sm btn-block" id="btn-clear"
                            style="margin:5px;">Clear</button></div>



                </div>
            </div>
        </div>
    </div>
</div>

<?php
include($base.'_in/footer.php');
?>
<script>

function openNav1() {
    document.getElementById("mysidenav1").style.width = "300px";
}

function closeNav1() {
    document.getElementById("mysidenav1").style.width = "0";
}
var monkeyList = new List('test-list', {
    valueNames: ['branchmaster']
});


$('#btn-contact').on('click', function () {
    valid = true;
    if (checker('modal') != false) {
        valid = valid * true;
    } else {
        valid = valid * false;
    }
    if (valid) {
        data = checker('modal');
        tableui(data.name, data.department, data.mobile, data.email, data.landline);
        modals.clear('modal');
    }
})



var count = 1;

function tableui(name, department, mobile, email, landline) {
    var sr = count;
    count++;
    var str = '';
    str += '<tr id="tr-' + sr + '">';
    str += '<td class="sr">' + sr + '</td>';
    str += '<td class="name">' + name + '</td>';
    str += '<td class="department">' + department + '</td>';
    str += '<td class="mobile">' + mobile + '</td>';
    str += '<td class="landline">' + landline + '</td>';
    str += '<td class="email">' + email + '</td>';
    str += '<td><button data-id="tr-' + sr + '" onclick="remover(this)" class="btn btn-danger btn-sm" >R</nutton></td>';
    str += '</tr>';

    $('#contact-table tbody').append(str);
}

function remover(e) {
    $(e).parent().parent().fadeOut(1000, function () {
        $(this).remove();
    });
}

function edit(e) {
    $('#btn-submit').attr('onclick', 'update()');
    $('#btn-submit').text('Update');
    $('#contact-table > tbody > tr').remove();
    var branchid = $(e).data('branchid');
    $('#branchid').val(branchid);
    $.ajax({
        type: "POST",
        data: 'branchid=' + branchid,
        url: 'select.php',
        cache: false,
        success: function (res) {
            if (res.status == 'success') {
                console.log(res.branchjson);

                modals.putvalue('section', res.branchjson);
                var contact = res.contactjson;
                for (var i in contact) {
                    tableui(contact[i].name, contact[i].department, contact[i].mobile, contact[i].landline, contact[i].email);
                }

                $('#signature').show();
                $('#show_sign').attr("href", res.signature);

                closeNav1();
            }
        }
    });
}

function update() {
    var valid = true;
    var formData = new FormData();

    if (checker('section') != false) {
        valid = valid * true;
    } else {
        valid = valid * false;
    }

    if (valid) {
        var data = checker('section');
        var datastr = JSON.stringify(data);
        var branchid = $('#branchid').val();
        formData.append('signature', $('#myFile')[0].files[0]);
        formData.append('data', datastr);
        formData.append('data2','[]');
        formData.append('branchid',branchid);

        $.ajax({
            type: "POST",
            data : formData,
            url: 'update.php',
            cache: false,
            processData: false,
            contentType: false,
            success: function (res) {
                if (res.status == 'success') {
                    swal({
                        type: 'success',
                        title: 'Branch Master Updated',
                        showConfirmButton: false,
                        timer: 3000
                    });
                    setTimeout(function () {
                        window.location.reload();
                    }, 3000);
                }
                if (res.status == 'falid2') {
                    swal('Branch Master Not Exist', '', 'error');
                    $('#inp-text-1').val('');
                    $('#inp-phone').css('border-bottom', '1px solid red');
                }
            }
        });
    }
}

$('#btn-reset').on('click', function () {
    window.location.reload();
});

function submit() {
    var valid = true;
    var formData = new FormData();

    if (checker('section') != false) {
        valid = valid * true;
    } else {
        valid = valid * false;
    }


    if (valid) {
        
        var data = checker('section');
        var datastr = JSON.stringify(data);
        formData.append('signature', $('#myFile')[0].files[0]);
        formData.append('data', datastr);
        formData.append('data2','[]');

        $.ajax({
            type: "POST",
            data : formData,
            url: 'insert.php',
            cache: false,
            processData: false,
            contentType: false,
            success: function (res) {
                if (res.status == 'success') {
                    swal({
                        type: 'success',
                        title: 'Branch Master Created',
                        showConfirmButton: false,
                        timer: 3000
                    });
                    setTimeout(function () {
                        window.location.reload();
                    }, 3000);
                }
                if (res.status == 'falid2') {
                    swal('Branch Master already Exist', '', 'error');
                    $('#inp-text-1').val('');
                    $('#inp-phone').css('border-bottom', '1px solid red');
                }
            }
        });
    }
}

</script>