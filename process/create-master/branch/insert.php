<?php
    $base='../../../';
    include($base.'_in/connect.php');
    header('content-type: application/json; charset=utf-8');
    header("access-control-allow-origin: *");
    if(isset($_POST['data']) && isset($_POST['data2'])) {
        $con=_connect();
        if (session_status()==PHP_SESSION_NONE) { session_start(); }
        $created_by=$_SESSION['employeeid'];

        $data1=$_POST["data"];
        $data2=$_POST["data2"];
        $data=get_object_vars(json_decode($_POST["data"]));
        $branchname=strtoupper($data['companybranchname']);
        $branchshortname=strtoupper($data['branchshortname']);
        $branchid=mysqli_fetch_assoc(mysqli_query($con, "SELECT max(id) x FROM branchmaster"))['x'];
        if($branchid) { $branchid++; } else {  $branchid=1; }

        $filename = $branchid.'-'.$CURRENT_MILLIS;


        $storableLocation  = "";

        if ($_FILES['signature']['size'] != '') {

            $fileInfo  = pathinfo($_FILES['signature']['name']);

            $extension = $fileInfo['extension'];

            $fileLocation = 'signature/'.$filename.'.'.$extension;

            $storableLocation = '/process/create-master/branch/signature/'.$filename.'.'.$extension;

            move_uploaded_file($_FILES['signature']['tmp_name'], $fileLocation);

        } 

        $results=mysqli_fetch_assoc(mysqli_query($con, "SELECT branchname x FROM branchmaster WHERE branchname='$branchname' "))['x'];
        if( !$results) {
            $create=mysqli_query($con, "INSERT INTO branchmaster (branchshortname,branchid,branchname,branchjson,contactjson,signature,created_by,created_time) VALUES ('$branchshortname','$branchid','$branchname','$data1','$data2','$storableLocation','$created_by','$CURRENT_MILLIS')");
            if($create) {
                echo '{"status":"success"}';
            }
            else {
                echo '{"status":"falid1"}';
            }
        }
        else {
            echo '{"status":"falid2"}';
        }
        _close($con);
    }
    else {
        echo '{"status":"falid"}';
    }
?>