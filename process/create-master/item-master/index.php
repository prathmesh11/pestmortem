<?php

    $base = '../../../';

    $js='<script src="'.$base.'js/fwork.js"></script>
    <script src="'.$base.'js/alart.js"></script>
    <script src="'.$base.'js/list.min.js"></script>';

    $css='<link rel="stylesheet" href="'.$base.'css/fwork.css">
    <link rel="stylesheet" href="'.$base.'css/grid.min.css">
    <link href="'.$base.'css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="'.$base.'css/alart.css">';

    $title = "Item Master";

    include($base.'_in/header.php');

    include($base.'_in/connect.php');

    if (!in_array("Item Master", $arrysession)) {
   
        echo "<script>window.location.href='/process/dashboard.php';</script>";
       
        exit;
    
    }

    $con=_connect();

?>

<style>

    .panel {
        z-index: 1;
        -webkit-box-shadow: 0px 3px 7px -1px rgba(0, 0, 0, 0.75);
        -moz-box-shadow: 0px 3px 7px -1px rgba(0, 0, 0, 0.75);
        box-shadow: 0px 3px 7px -1px rgba(0, 0, 0, 0.75);
    }

    .list {
        padding: 0px;
    }

    .list li span {
        display: inline;
    }

    .list li {
        text-decoration: none;
        display: block;
        padding: 2px;
        background: #FFF;
        border: 1px solid #333;
        color: #000;
        font-size: 11px;
    }

    .admin {
        display: none;
    }


    .th{
        background:#8e44ad;
        color:#fff;
        text-align:center;
        padding-top:2px;
        padding-bottom:2px;
        border:1px solid #fff;
    }
    .td{
        border:1px solid #ddd;
    }
    .table-ui .btn{
        margin:3px;
    }
    #myModal1 .col-sm-4,.col-sm-8{
      margin-bottom:2px;
    }
    .picker__select--year{
        height:auto;
    }
    .picker__select--month{
        height:auto;
    }
    .table-list td,
   .table-list th {
       border: 1px solid #ddd;
       padding: 1px !important;
       font-size: 13px;
   }
   .table-list td{
	padding-top: 10px !important;
   }

   .table-list tr:nth-child(even) {
       background-color: #f2f2f2;
   }

   .table-list th {
       padding-top: 5px;
       padding-bottom: 5px;
       text-align: center;
       background-color: #16a085;
       color: white;
   }

</style>

    <div class="container-fluid">

        <div class=" content" >

            <h2 align="center">Create Item Master</h2>

            <hr class="style-hr">

            <div class="col-sm-9">

                <div class="col-sm-6" id="section">

                    <div class="col-sm-4">Main Group Name</div>

                    <div class="col-sm-8">

                        <div class="form-group">

                            <select class="form-control input-sm" data-select="2" onchange="maingroupcode(this)" id="inp-text-1"
                                data-role="select" data-name="groupcode1">

                                <option value="Select">Select</option>

                                <?php 

                                    $result1=mysqli_query($con, "SELECT * FROM `mitemgroup` WHERE mainGroupCode='MG' AND groupCode<>'MG'");

                                        while($rows=mysqli_fetch_assoc($result1)) {

                                            echo '<option value="'.$rows['groupCode'].'">'.$rows['groupName'].'</option>';

                                        }

                                ?>

                            </select>

                        </div>

                    </div>

                    <div class="col-sm-4" id="groupcode2"></div>

                    <div class="col-sm-8">

                        <div class="form-group">

                            <select class="form-control input-sm"data-select="3" onchange="maingroupcode(this)" id="inp-text-2"
                                data-name="groupcode2">

                                <option value="Select">Select</option>

                            </select>
                            
                        </div>

                    </div>

                    <div class="col-sm-4" id="groupcode3"></div>

                    <div class="col-sm-8">

                        <div class="form-group">

                            <select class="form-control input-sm"data-select="4" onchange="maingroupcode(this)" id="inp-text-3"
                                data-name="groupcode3">

                                <option value="Select">Select</option>

                            </select>

                        </div>

                    </div>

                    <div class="col-sm-4" id="groupcode4"></div>

                    <div class="col-sm-8">

                        <div class="form-group">

                            <select class="form-control input-sm" data-select="5" onchange="maingroupcode(this)" id="inp-text-4"
                                data-name="groupcode4">

                                <option value="Select">Select</option>

                            </select>

                        </div>

                    </div>

                    <div class="col-sm-4" id="groupcode5"></div>

                    <div class="col-sm-8">

                        <div class="form-group">

                            <select class="form-control input-sm" data-select="6" onchange="maingroupcode(this)" id="inp-text-5"
                                data-name="groupcode5">

                                <option value="Select">Select</option>

                            </select>

                        </div>

                    </div>

                    <div class="col-sm-4" id="groupcode6"></div>

                    <div class="col-sm-8">

                        <div class="form-group">

                            <select class="form-control input-sm"  id="inp-text-6" data-name="groupcode6">

                                <option value="Select">Select</option>

                            </select>

                        </div>

                    </div>

                    <div class="col-sm-4">Item_Short_Name</div>

                    <div class="col-sm-8">

                        <div class="form-group">

                            <input type="text" class="form-control input-sm" data-name="shortname" id="inp-text-7">
                            
                        </div>

                    </div>

                    <div class="col-sm-4">Item_Name</div>

                    <div class="col-sm-8" id="test-list2">

                        <div class="form-group">
                            
                            <input type="text" data-role="text" data-name="itemname" class="form-control input-sm fuzzy-search" id="inp-text-9">

                        </div>

                    </div>

                </div>

                <div class="col-sm-6" id="section">

                    <input type="hidden" id="stockid" class="form-control input-sm">

                    <div class="col-sm-4">Unit</div>

                    <div class="col-sm-4">

                        <div class="form-group">

                            <input type="text" class="form-control input-sm" value="1" data-name="unit1value" >

                        </div>

                    </div>

                    <div class="col-sm-4">

                        <div class="form-group">

                            <select class="form-control input-sm select-js1" data-role="select" data-name="unit1" id="inp-text-11">

                               <option value="Select">Select</option>

                               <?php 

                                    $results=mysqli_query($con,"SELECT unitcode,uniteway FROM munit");

                                        while($rows=mysqli_fetch_assoc($results)) {

                                            echo '<option value="'.$rows["unitcode"].'">'.$rows["uniteway"].'</option>'; 
                                        }

                               ?>
                               
                           </select>

                        </div>

                    </div>

                    <div style="clear:both"></div>

                    <div class="col-sm-4">Unit =</div>

                    <div class="col-sm-4">

                        <div class="form-group">

                            <input type="text" class="form-control input-sm" value="1" data-name="unit2value" >

                        </div>

                    </div>

                    <div class="col-sm-4">

                        <div class="form-group">

                            <select class="form-control input-sm select-js1" data-role="select" data-name="unit2" id="inp-text-12">

                               <option value="Select">Select</option>

                               <?php 

                                    $results=mysqli_query($con,"SELECT unitcode,uniteway FROM munit");

                                    while($rows=mysqli_fetch_assoc($results)) {

                                        echo '<option value="'.$rows["unitcode"].'">'.$rows["uniteway"].'</option>'; 

                                    }
                               ?>

                            </select>

                        </div>

                    </div>

                    <div style="clear:both"></div>

                    <div class="col-sm-4">Stock Ind.</div>

                    <div class="col-sm-8">

                        <div class="form-group">

                            <select class="form-control input-sm" data-role="select" data-name="stockind">

                                <option value="Select">Select</option>

                                <option value="S">Stock</option>

                                <option value="N">Non Stock</option>

                            </select>

                        </div>

                    </div>

                    <div class="col-sm-4">Class</div>

                    <div class="col-sm-8">

                        <div class="form-group">

                            <select class="form-control input-sm" data-role="select" data-name="class">

                                <option value="Select">Select</option>

                                <option value="A">A</option>
                                
                                <option value="B">B</option>

                                <option value="C">C</option>

                            </select>

                        </div>

                    </div>

                    <div class="col-sm-4">HSN Code</div>

                    <div class="col-sm-8">

                        <div class="form-group">

                            <input type="text" data-role="text" data-name="hsncode" class="form-control input-sm">

                        </div>

                    </div>

                    <div class="col-sm-4">GST Rate</div>

                    <div class="col-sm-8">

                        <div class="form-group">

                            <input type="text" data-role="number" data-name="gstrate" class="form-control input-sm">

                        </div>

                    </div>

                </div>

                <div class="col-sm-12" id="suppliertable">

                    <table class="table-list table" id="table-supplier">

                        <thead>

                            <th>#</th>

                            <th>Supplier Name</th>

                            <th>Discount Rate</th>

                            <th>Rate</th>

                            <th>Remark</th>

                            <th>Action</th>

                        </thead>

                        <tbody>

                            <tr>

                                <td></td>

                                <td>

                                    <select class="form-control input-sm select-js1" data-role="select" id="suppliername" data-name="suppliername">

                                        <option value="Select">Select</option>

                                        <?php
                                            
                                            $result5=mysqli_query($con,"SELECT supplierid,suppliername FROM suppliermaster");

                                                while($rows1 = mysqli_fetch_assoc($result5)){  

                                                    echo '<option value="'.$rows1['supplierid'].'">'.$rows1['suppliername'].'</option>';

                                                }
                                        ?>

                                    </select>

                                </td>

                                <td>

                                    <input data-role="number" id="disRate" data-name="disrate" class="form-control input-sm" type="number" />

                                </td>

                                <td>

                                    <input data-role="number" id="rate" data-name="rate" class="form-control input-sm" type="number" />

                                </td>

                                <td>

                                    <input data-role="text" id="remark" data-name="remark" class="form-control input-sm" type="text" />

                                </td>

                                <td>

                                    <button class="btn btn-sm btn-primary" id="btn-add" data-edit="" onclick="btnenq(this)">Add</button>

                                </td>

                            </tr>

                        </tbody>

                    </table>
                </div>

                <div class="col-sm-6">

                    <button class="btn btn-success btn-sm btn-block" id="submit" onclick="submit()" style="margin:5px;">Submit</button>

                </div>

                <div class="col-sm-6">

                    <button class="btn btn-danger btn-sm btn-block" id="reset" style="margin:5px;">Reset</button>

                </div>

               
            </div>

            <div class="col-sm-3 ">

                <div class="panel panel-default">

                    <div class="panel-heading" align="center">Search </div>

                    <div class="panel-body" style="padding:0px;">

                        <div id="test-list" style="padding:5px;">

                            <div class="form-group">

                                <input type="text" placeholder="Search" class="form-control input-sm fuzzy-search" style="border-radius:3px;" id="itemSearch">

                            </div>

                            <ul class="list list1" style="height:400px;overflow: scroll;" id="myList">

                                <?php 
                                    
                                    $result2=mysqli_query($con,"SELECT stockid,itemname FROM stockmaster ORDER by itemname ASC");

                                        while($rows = mysqli_fetch_assoc($result2)){

                                            $stockid  = $rows['stockid'];

                                            $itemname = $rows['itemname'];

                                            echo '<li><span class="itemname">' .$itemname. '</span><button style="margin-left:2px;" data-stockid="' .$stockid. '" onclick="edit(this)" class="btn btn-primary btn-sm"><span class="fa fa-edit"></span> E</button> <button style="margin-left:2px;" data-stockid="' .$stockid. '" onclick="deleteitem(this)" class="btn btn-danger btn-sm"><span class="fa fa-edit"></span> D</button></i>';

                                        }

                                ?>

                            </ul>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>


<?php
include($base.'_in/footer.php');
?>
<script>


function btnenq(e) {

	let chk          = $(e).attr('data-edit');
	let data         = checker('table-supplier');
    let suppliername = $('#suppliername').find(":selected").text();
	let suppliercode = data.suppliername;
    let disrate      = data.disrate;
	let rate         = data.rate;
	let remark       = data.remark;
	let valid        = true;

	if (checker('table-supplier') != false) {
		valid = valid * true;
	} else {
		valid = valid * false;
	}


        $('#table-supplier .suppliercode').each(function () {

            let suppliercode1 = $(this).text().trim();

            if (suppliercode1 == suppliercode && chk == '') {

                valid = valid * false;

                alert('Dublicate Supplier');

            }

	    });
        



	if (valid) {

        let chk = $(e).attr('data-edit');
        
		if (chk == '') {

            let len = $('#table-supplier .srno').length + 1;
            
            fun_adder(len, suppliername,suppliercode,disrate,rate,remark);
            
		} else {

            fun_adder_edit(chk, suppliername,suppliercode,disrate,rate,remark);
            
        }
        
        modals.clear('table-supplier');
        
        $('#btn-add').attr('data-edit', '');
        
	}

}


function fun_adder(len, suppliername,suppliercode,disrate,rate,remark) {

		var str = '<tr id="abc">';
		str += '<td align="center" class="srno">' + len + '</td>';
        str += '<td align="center" class="suppliercode" style="display:none;">' + suppliercode + '</td>';
        str += '<td align="center" class="suppliername">' + suppliername + '</td>';
        str += '<td align="center" class="disrate">' + disrate + '</td>';
		str += '<td align="center" class="rate">' + rate + '</td>';
        str += '<td align="center" class="remark">' + remark + '</td>';
		str += '<td align="center"><button class="btn btn-sm btn-success" onclick="editor(' + len + ',this)">E</button>  <button class="btn btn-sm btn-danger remover" onclick="remover(this)">R</buuton></td>';
		str += '</tr>';
		$('#table-supplier > tbody').append(str);
        

}

function fun_adder_edit(len, suppliername,suppliercode,disrate,rate,remark) {

		var str = '<td align="center" class="srno">' + len + '</td>';
        str += '<td align="center" class="suppliercode" style="display:none;">' + suppliercode + '</td>';
        str += '<td align="center" class="suppliername">' + suppliername + '</td>';
        str += '<td align="center" class="disrate">' + disrate + '</td>';
		str += '<td align="center" class="rate">' + rate + '</td>';
        str += '<td align="center" class="remark">' + remark + '</td>';
		str += '<td align="center"><button class="btn btn-sm btn-success" onclick="editor(' + len + ',this)">E</button>  <button class="btn btn-sm btn-danger remover" onclick="remover(this)">R</buuton></td>';

		$('#table-supplier .srno').each(function(){

            var srno=$(this).text().trim();
            
			if(srno==len){

                $(this).parent().html(str);
                
            }
            
        });
        
      

}

function editor(srno,e){

    var suppliercode=$(e).parent().parent().find('.suppliercode').text().trim();
	$('#suppliername').val(suppliercode).trigger('change');
     $('#disRate').val($(e).parent().parent().find('.disrate').text().trim());
	 $('#rate').val($(e).parent().parent().find('.rate').text().trim());
	 $('#remark').val($(e).parent().parent().find('.remark').text().trim());

	$('#btn-add').attr('data-edit',srno);
}

var monkeyList = new List('test-list', {

valueNames: ['itemname']

});

$(document).ready(function(){
  $("#itemSearch").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#myList li").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});

function maingroupcode(e){
var groupcode=$(e).val();
var select=$(e).data('select');
    $.ajax({
        type: "POST",
        data:'groupcode='+groupcode+'&select='+select,
        url: 'itemgroup.php',
        cache: false,
        success: function (res) {
            if (res.status == 'success') {
                var json=res.json;
                var str='<option value="Select">Select</option>';
                for(var i in json){
                    str+='<option value="'+json[i].groupCode+'">'+json[i].groupName+'</option>'
                }
                $('#inp-text-'+res.select).html(str);
                var no=parseInt(res.select);
                for(var i=6;i>=no;i--){
                    $('#inp-text-'+i).val('Select').trigger('change');
                }
                $('#inp-text-7').val('');
                $('#inp-text-9').val('');
            }
        }
    });
}
$('#inp-text-7').on('keyup',function(){
var itemgroup2=$('#inp-text-2 option:selected').text();
var itemgroup3=$('#inp-text-3 option:selected').text();
var itemgroup4=$('#inp-text-4 option:selected').text();
var itemgroup5=$('#inp-text-5 option:selected').text();
var itemgroup6=$('#inp-text-6 option:selected').text();
console.log(itemgroup2);

if(itemgroup2=='Select'){itemgroup2=''}
if(itemgroup3=='Select'){itemgroup3=''}
if(itemgroup4=='Select'){itemgroup4=''}
if(itemgroup5=='Select'){itemgroup5=''}
if(itemgroup6=='Select'){itemgroup6=''}

var itemcode=itemgroup2+' '+itemgroup3+' '+itemgroup4+' '+itemgroup5+' '+itemgroup6;
itemcode=itemcode.trim();
var shortname=$(this).val();
shortname = shortname.replace('"','');
shortname = shortname.replace(",",'');
shortname = shortname.replace("'",'');
shortname = shortname.trim();
var itemname=itemcode+' '+shortname;
itemname=itemname.trim();
itemname = itemname.replace("  ", " ");
$('#inp-text-9').val(itemname);
})

function submit() {

    var valid = true;

    if (checker('section') != false) {

        valid = valid * true;

    } else {

        valid = valid * false;

    }

    var arr = [];

    $('#table-supplier > tbody > tr').each(function () {

        var suppliercode = $(this).find('.suppliercode').text().trim();
        var suppliername = $(this).find('.suppliername').text().trim();
        var disrate      = $(this).find('.disrate').text().trim();
        var rate         = $(this).find('.rate').text().trim();
        var remark       = $(this).find('.remark').text().trim();

        if (suppliercode != '') {

            arr.push({

                "suppliercode": suppliercode,

                "suppliername": suppliername,

                "disrate": disrate,

                "rate": rate,

                "remark": remark,

            });

        }

    })

    if (arr == '') {

        $('#suppliertable').css('border', '2px solid red');

    } else {

        $('#suppliertable').css('border', '2px solid green');

    }



    if (valid) {
        var data1 = checker('section');
        data1 = JSON.stringify(data1);
        var data2 = JSON.stringify(arr);

        $.ajax({
            type: "POST",
            data: {
                data: data1,
                data1: data2

            },
            url: 'insert.php',
            cache: false,
            success: function (res) {
                if (res.status == 'success') {
                    swal({
                        type: 'success',
                        title: 'Your Work is Save',
                        showConfirmButton: false,
                        timer: 3000
                    });
                    setTimeout(function () {
                        window.location.reload();
                    }, 3000);
                }
            }
        });
    }
}

function edit(e) {
    $('#submit').attr('onclick', 'update()');
    $('#submit').text('Update');
    var stockid = $(e).data('stockid');
    $('#stockid').val(stockid);
    $.ajax({
        type: "POST",
        data: 'stockid=' + stockid,
        url: 'select.php',
        cache: false,
        success: function (res) {
            if (res.status == 'success') {
                modals.putvalue('section', res.json);
                setTimeout(function () {
                    $('#inp-text-2').val(res.json[0].groupcode2).trigger('change');
                }, 400);
                setTimeout(function () {
                    $('#inp-text-3').val(res.json[0].groupcode3).trigger('change');
                }, 800);
                setTimeout(function () {
                    $('#inp-text-4').val(res.json[0].groupcode4).trigger('change');
                }, 1500);
                setTimeout(function () {
                    $('#inp-text-5').val(res.json[0].groupcode5).trigger('change');
                }, 2000);
                setTimeout(function () {
                    $('#inp-text-6').val(res.json[0].groupcode6).trigger('change');
                }, 2500);
                setTimeout(function () {
                    $('#inp-text-7').val(res.json[0].shortname);
                    $('#inp-text-9').val(res.json[0].itemname);
                }, 2700);

                let supplierJson = res.json[0].supplier_json;

                if (Object.keys(supplierJson).length != 0) {
                    
                    $('#table-supplier').find('tbody #abc').remove();

                    let count = 1;

                    for (let i in supplierJson) {

                        fun_adder(count, supplierJson[i].suppliername,supplierJson[i].suppliercode,supplierJson[i].disrate,supplierJson[i].rate,supplierJson[i].remark);

                        count++;

                    }

                } else {

                    $('#table-supplier').find('tbody #abc').remove();

                }

                

            }
        }
    });
}


function deleteitem(e) {

    let stockid = $(e).attr('data-stockid');

    $.ajax({

        type: "POST",
        
        data: 'stockid=' + stockid,
        
        url: 'remove.php',
        
        cache: false,
        
        success: function (res) {
        
            if (res.status == 'success') {

                if (res.json == '[]') {

                    swal({

                        type: 'success',
                        title: 'Item Delecte',
                        showConfirmButton: false,
                        timer: 3000

                    });

                    setTimeout(function () {

                        window.location.reload();

                    }, 3000);

                    
                } else {
                    
                    let abc = JSON.stringify(res.json);
                    alert("This Item Available In This Purchase Order So Can't Remove \n"+abc);

                }

            }
        
        }

    })

}

function update() {
    var valid = true;
    if (checker('section') != false) {
        valid = valid * true;
    } else {
        valid = valid * false;
    }

    var arr = [];

    $('#table-supplier > tbody > tr').each(function () {

        var suppliercode = $(this).find('.suppliercode').text().trim();
        var suppliername = $(this).find('.suppliername').text().trim();
        var disrate      = $(this).find('.disrate').text().trim();
        var rate         = $(this).find('.rate').text().trim();
        var remark       = $(this).find('.remark').text().trim();

        if (suppliercode != '') {

            arr.push({

                "suppliercode": suppliercode,

                "suppliername": suppliername,

                "disrate": disrate,

                "rate": rate,

                "remark": remark,

            });

        }

    })

    if (arr == '') {

        $('#suppliertable').css('border', '2px solid red');
        valid = valid * false;

    } else {

        $('#suppliertable').css('border', '2px solid green');
        valid = valid * true;

    }

    if (valid) {
        var data1 = checker('section');
        data1     = JSON.stringify(data1);
        var data2 = JSON.stringify(arr);
        $.ajax({
            type: "POST",
            data: {
                data: data1,
                stockid: $('#stockid').val(),
                data1: data2

            },
            url: 'update.php',
            cache: false,
            success: function (res) {
                if (res.status == 'success') {
                    swal({
                        type: 'success',
                        title: 'Your Work is Save',
                        showConfirmButton: false,
                        timer: 3000
                    });
                    setTimeout(function () {
                        window.location.reload();
                    }, 3000);
                }
            }
        });
    }
}

$(document).ready(function () {

    $('.select-js1').select2();

});
</script>