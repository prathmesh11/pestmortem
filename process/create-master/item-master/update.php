<?php

    $base = '../../../';

    include($base.'_in/connect.php');

    header('content-type: application/json; charset=utf-8');

    header("access-control-allow-origin: *");

    if(isset($_POST['data']) && isset($_POST['stockid'])){

        $con   = _connect();

        $data  = get_object_vars(json_decode($_POST["data"]));

        if (session_status()==PHP_SESSION_NONE) { session_start(); }

        $created_by = $_SESSION['employeeid'];

        $branchid   = $_SESSION['branchid'];

        $groupcode1 = $data["groupcode1"];

        $groupcode2 = $data["groupcode2"];

        $groupcode3 = $data["groupcode3"];

        $groupcode4 = $data["groupcode4"];

        $groupcode5 = $data["groupcode5"];

        $groupcode6 = $data["groupcode6"];

        $shortname  = $data["shortname"];

        $itemname   = $data["itemname"];

        $stockind   = $data["stockind"];

        $unit1      = $data["unit1"];

        $unit2      = $data["unit2"];

        $unit1value = $data["unit1value"];

        $unit2value = $data["unit2value"];

        $class      = $data["class"];

        $hsncode    = $data["hsncode"];

        $gstrate    = $data["gstrate"];

        if($groupcode1=='Select'){ $groupcode1=''; }

        if($groupcode2=='Select'){ $groupcode2=''; }

        if($groupcode3=='Select'){ $groupcode3=''; }

        if($groupcode4=='Select'){ $groupcode4=''; }

        if($groupcode5=='Select'){ $groupcode5=''; }

        if($groupcode6=='Select'){ $groupcode6=''; }
        
        $stockid = $_POST['stockid'];
        
        $data1   = $_POST['data1'];

                
        $create=mysqli_query($con,"UPDATE stockmaster SET unit1value='$unit1value',unit2value='$unit2value', groupcode1='$groupcode1',groupcode2='$groupcode2',groupcode3='$groupcode3',groupcode4='$groupcode4',groupcode5='$groupcode5',groupcode6='$groupcode6',shortname='$shortname',itemname='$itemname',unit1='$unit1',unit2='$unit2',class='$class',hsncode='$hsncode',gstrate='$gstrate',supplier_json='$data1' WHERE stockid='$stockid'");

        if($create){

            $data1 = json_decode($_POST["data1"]);

            foreach($data1 as $i) { 

                $scode   = get_object_vars($i)['suppliercode'];

                $disrate = get_object_vars($i)['disrate'];

                $rate    = get_object_vars($i)['rate'];

                $remark  = get_object_vars($i)['remark'];

                $chk     = mysqli_fetch_assoc(mysqli_query($con,"SELECT * FROM msirate WHERE scode='$scode' AND itemcode='$stockid' "));

                if($chk){

                    $sirid         = $chk['sirid'];

                    $scode         = $chk['scode'];

                    $rate2         = $chk['rate'];

                    $discount_rate = $chk['discount_rate'];

                    $time          = $chk['timestamp'];

                    if($rate2!=$rate){

                    mysqli_query($con,"INSERT INTO msirate_history (sirid,discount_rate,rate,timestamp,remark) VALUES ('$sirid','$discount_rate','$rate2','$time','$remark')");

                    mysqli_query($con,"UPDATE msirate SET discount_rate='$disrate' ,rate='$rate', timestamp='$CURRENT_MILLIS', created_by='$createdby', approval='0', approval_by='0' WHERE sirid='$sirid' ");

                    }

                } else {

                    mysqli_query($con,"INSERT INTO msirate (scode,itemcode,discount_rate,rate,timestamp,created_by) VALUES ('$scode','$stockid','$disrate','$rate','$CURRENT_MILLIS','$createdby')");

                }

            }

            echo '{"status":"success"}';
            
        } else {

            echo '{"status":"falid1"}';

        }

        _close($con);

    } else {

        echo '{"status":"falid"}';

    }
?>