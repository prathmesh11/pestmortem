<?php

  $base = '../../../';

  include($base.'_in/connect.php');

  header('content-type: application/json; charset=utf-8');

  header("access-control-allow-origin: *");

  if(isset($_POST['stockid'])){

    $con     = _connect();

    $stockid = _clean($con,$_POST['stockid']);

    $stock   = mysqli_fetch_assoc(mysqli_query($con,"SELECT unit1value,unit2value,itemname,groupcode1,groupcode2,groupcode3,groupcode4,groupcode5,groupcode6,shortname,stockind,unit1,unit2,class,hsncode,gstrate,supplier_json FROM stockmaster WHERE stockid='$stockid'"));

    if ($stock['supplier_json'] == '') {
      
      $supplier_json = '[]';

    } else {

      $supplier_json = $stock['supplier_json'];

    }

    $json    = '{"unit1value":"'.$stock['unit1value'].'","unit2value":"'.$stock['unit2value'].'","groupcode1":"'.$stock['groupcode1'].'","groupcode2":"'.$stock['groupcode2'].'","groupcode3":"'.$stock['groupcode3'].'","groupcode4":"'.$stock['groupcode4'].'","groupcode5":"'.$stock['groupcode5'].'","groupcode6":"'.$stock['groupcode6'].'","shortname":"'.$stock['shortname'].'","itemname":"'.$stock['itemname'].'","stockind":"'.$stock['stockind'].'","unit1":"'.$stock['unit1'].'","unit2":"'.$stock['unit2'].'","class":"'.$stock['class'].'","hsncode":"'.$stock['hsncode'].'","gstrate":"'.$stock['gstrate'].'","supplier_json":'.$supplier_json.'}';

    if($stock){

      echo '{"status":"success","json":['.$json.']}';

    } else {

      echo '{"status":"faild"}';

    }

    _close($con);

  }

?>
