<?php

    $base='../../../';

    include($base.'_in/connect.php');

    header('content-type: application/json; charset=utf-8');

    header("access-control-allow-origin: *");
    
    if(isset($_POST['data'])) {

        $con = _connect();

        if (session_status()==PHP_SESSION_NONE) { session_start(); }

        $created_by = $_SESSION['employeeid'];

        $data=get_object_vars(json_decode($_POST["data"]));
  
        $branchid         = $data['branchid'];
        $category         = $data['category'];
        $shortDescription = $data['shortDescription'];
        $description      = $data['description'];
        $id               = $_POST["id"];

        $filename = $CURRENT_MILLIS;

        $storableLocation  = "";

        if ($_FILES['image']['size'] != '') {

            $fileInfo  = pathinfo($_FILES['image']['name']);

            $extension = $fileInfo['extension'];

            $fileLocation = 'uploadimg/'.$filename.'.'.$extension;

            $storableLocation = '/process/create-master/document/uploadimg/'.$filename.'.'.$extension;

        } else {

            $storableLocation  = mysqli_fetch_assoc(mysqli_query($con,"SELECT image_link x FROM document WHERE id='$id'"))['x'];

        }
        
        $update=mysqli_query($con,"UPDATE document SET branchid = '$branchid',category='$category',short_Description='$shortDescription',description='$description',image_link='$storableLocation',edit_by='$created_by',edit_time='$CURRENT_MILLIS' WHERE id='$id'");

        if($update) {

            move_uploaded_file($_FILES['image']['tmp_name'], $fileLocation);


            echo '{"status":"success"}';

        } else {

            echo '{"status":"falid1"}';

        }
        
        _close($con);

    } else {

        echo '{"status":"falid"}';

    }
?>