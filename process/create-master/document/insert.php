<?php

    $base='../../../';

    include($base.'_in/connect.php');

    header('content-type: application/json; charset=utf-8');

    header("access-control-allow-origin: *");
    
    if(isset($_POST['data'])) {

        $con = _connect();

        if (session_status()==PHP_SESSION_NONE) { session_start(); }

        $created_by = $_SESSION['employeeid'];

        $data=get_object_vars(json_decode($_POST["data"]));
  
        $branchid         = $data['branchid'];
        $category         = $data['category'];
        $shortDescription = $data['shortDescription'];
        $description      = $data['description'];

        $filename = $CURRENT_MILLIS;


        $storableLocation  = "";


        if ($_FILES['image']['size'] != 0) {

            $fileInfo  = pathinfo($_FILES['image']['name']);

            $extension = $fileInfo['extension'];

            $fileLocation = 'uploadimg/'.$filename.'.'.$extension;

            $storableLocation = '/process/create-master/document/uploadimg/'.$filename.'.'.$extension;


        }
        
        $create=mysqli_query($con,"INSERT INTO document (branchid,category,short_Description,description,image_link,created_by,created_time) VALUES ('$branchid','$category','$shortDescription','$description','$storableLocation','$created_by','$CURRENT_MILLIS')");
        
        if($create) {

            move_uploaded_file($_FILES['image']['tmp_name'], $fileLocation);


            echo '{"status":"success"}';

        } else {

            echo '{"status":"falid1"}';

        }
        
        _close($con);

    } else {

        echo '{"status":"falid"}';

    }
?>