<?php
    $base='../../../';
    $js='<script src="'.$base.'js/fwork.js"></script>
    <script src="'.$base.'js/alart.js"></script>
    <script src="'.$base.'js/list.min.js"></script>';
    $css='<link rel="stylesheet" href="'.$base.'css/fwork.css">
    <link rel="stylesheet" href="'.$base.'css/grid.min.css">
    <link href="'.$base.'/css/select2.min.css" rel="stylesheet" />

    <link rel="stylesheet" href="'.$base.'css/alart.css">';

    include($base.'_in/header.php');
    include($base.'_in/connect.php');

    if (!in_array("Document", $arrysession)) {
   
        echo "<script>window.location.href='/process/dashboard.php';</script>";
       
        exit;
    
    }

    $con=_connect();

    if (session_status()==PHP_SESSION_NONE) { session_start(); }
        
    $branchid = $_SESSION['branchid'];

?>
<style>
    hr.style-hr {
        border: 0;
        height: 1px;
        background-image: linear-gradient(to right, rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.75), rgba(0, 0, 0, 0));
    }

    .table-list td,
    .table-list th {
        border: 1px solid #ddd;
        padding: 1px !important;
        font-size: 13px;
        text-align:center;
    }
   .table-list td{
	padding-top: 10px !important;

   }

   .table-list tr:nth-child(even) {
       background-color: #f2f2f2;
   }

   .table-list th {
       padding-top: 5px;
       padding-bottom: 5px;
       text-align: center;
       background-color: #16a085;
       color: white;
   }

   .sidenav1 {
       height: 100%;
       width: 0;
       position: fixed;
       z-index: 1;
       top: 0;
       left: 0;
       background-color: #111;
       overflow-x: hidden;
       transition: 0.5s;
       padding-top: 60px;
   }

   .sidenav1 a {
       padding: 8px 8px 8px 32px;
       text-decoration: none;
       font-size: 25px;
       color: #818181;
       display: block;
       transition: 0.3s;
   }

   .sidenav1 a:hover {
       color: #f1f1f1;
   }

   .sidenav1 .closebtn1 {
       position: absolute;
     
       right: 25px;
       font-size: 36px;
       margin-left: 50px;
   }

   @media screen and (max-height: 450px) {
       .sidenav1 {
           padding-top: 15px;
       }

       .sidenav1 a {
           font-size: 18px;
       }
   }

   .list {
       padding: 0px;
   }

   .list li span {
       display: inline;
   }

   .list li {
       text-decoration: none;
       display: block;
       padding: 2px;
       background: #FFF;
       border: 1px solid #333;
       color: #000;
   }
   input.largerCheckbox { 
            width: 20px; 
            height: 20px; 
        } 
</style>
<div class="container-fluid">

    <div class="content" id="section">

    <input type="hidden" class="form-control input-sm " data-name="branchid" value="<?php echo $branchid;?>">

    <input type="hidden" class="form-control input-sm " id="id">


        <span style="font-size:24px;cursor:pointer" onclick="openNav1()">&#9776; Search</span>

        <div id="mysidenav1" class="sidenav1" style="margin-left:80px;">

            <div id="test-list" style="padding:5px;">

                <a href="javascript:void(0)" class="closebtn1" onclick="closeNav1()">&times;</a>

                <br><br><br>

                <div class="form-group">

                    <input type="text" placeholder="Search" class="form-control input-sm fuzzy-search"
                        style="border-radius:3px;">

                </div>

                <ul class="list">

                    <?php 

                        $result2=mysqli_query($con,"SELECT * FROM document ORDER by id DESC");

                        while($rows = mysqli_fetch_assoc($result2)){

                            $id = $rows['id'];

                            $short_Description   = $rows['short_Description'];

                            echo '<li><span class="short_Description">' .$short_Description. '<span><button style="margin-left:2px;" data-id="' .$id. '" onclick="edit(this)" class="btn btn-primary btn-sm"><span class="fa fa-edit"></span> E</button></i>';

                        }

                    ?>

                </ul>

            </div>

        </div>

        <h2 align="center" style="margin-top:0px;">Document Upload</h2>

        <div style="clear:both;"></div>

        <hr class="style-hr">

        <div class="row">

            <div class="col-sm-6">

                <div class="col-sm-3">Category</div>

                <div class="col-sm-7">

                    <div class="form-group">

                        <input type="text" list="category" data-role='text' data-name='category' class="form-control input-sm" />

                        <datalist id="category">
                            <?php

                            $result= mysqli_query($con,"SELECT category  FROM document ORDER by id ASC ");

                            while($rows = mysqli_fetch_assoc($result)){

                                echo '<option value="'.$rows['category'].'"></option>';


                            }
                        
                        ?>
                        </datalist>

                    </div>

                </div>

                <div style="clear:both;"></div>

                <div class="col-sm-3">Short Description</div>

                <div class="col-sm-7">

                    <div class="form-group"><input type="text" data-role='text' data-name='shortDescription'
                        class="form-control input-sm">

                    </div>

                </div>

                <div style="clear:both;"></div>


                <div class="col-sm-3">Description</div>

                <div class="col-sm-7">

                    <div class="form-group">
                        <textarea type="text" data-role='text' data-name='description'
                            class="form-control input-sm"></textarea>

                    </div>

                </div>
                <div style="clear:both;"></div>


                <div class="col-sm-2"></div>

                <div class="col-sm-4">

                    <button class="btn btn-success btn-sm btn-block" id="btn-submit" onclick="submit()"
                        style="margin:5px;">Submit</button>

                </div>

                <div class="col-sm-4">

                    <button class="btn btn-danger btn-sm btn-block" id="btn-reset" style="margin:5px;">Reset</button>

                </div>

                <div style="clear:both;"></div>

            </div>

            <div class="col-sm-6">

                <div class="col-sm-4">Upload Image</div>

                <div class="col-sm-8">

                    <div class="form-group"><input type="file" onchange="getfile(this)" data-name="photo" name="photo"
                            id="photo" class="form-control input-sm">

                    </div>

                </div>

                <div class="col-sm-4">Image</div>

                <div class="col-sm-8 img-class" align="center"><img id="img" class="img-responsive"
                        style="width:250px;height:250px;border:1px solid #CCC;" src="" accept="image/*" />

                    <br>

                </div>


                <div class="col-sm-7"></div>

                <div class="col-sm-3" id="filelink1"></div>

                <div style="clear:both;"></div>


            </div>

        </div>


    </div>

</div>


     



<?php
include($base.'_in/footer.php');
?>
<script>

function openNav1() {
    document.getElementById("mysidenav1").style.width = "300px";
}

function closeNav1() {
    document.getElementById("mysidenav1").style.width = "0";
}

var monkeyList = new List('test-list', {
    valueNames: ['short_Description']
});

function baseimg(img) {

    var newTab = window.open();

    newTab.document.body.innerHTML = '<img src="' + img + '" >';

}



function getfile(input) {

    if (input.files && input.files[0]) {

        var reader = new FileReader();

        reader.onload = function (e) {

            $('#img').attr('src', e.target.result);

        }

        reader.readAsDataURL(input.files[0]);

    }

}


//--------------------- Insert Function Start -------------------- //              

function submit() {

    var formData = new FormData();
    var valid    = true;

    if (checker('section') != false) {

        valid = valid * true;

    } else {

        valid = valid * false;

    }

    if (typeof $('#photo')[0].files[0]  === "undefined") {

        $('#photo').css('border-bottom','1px solid red');

        $('#img').css('border','1px solid red');

        valid = valid * false;

    } else {

        $('#photo').css('border-bottom','1px solid green');

        $('#img').css('border','1px solid green');

        valid = valid * true;

    }

    if (valid) {

        data = checker('section');
        var datastr = JSON.stringify(data);
        formData.append('image', $('#photo')[0].files[0]);
        formData.append('data', datastr);

        $.ajax({
            type: "POST",
            data: formData,
            url: 'insert.php',
            cache: false,
            processData: false,
            contentType: false,
            success: function (res) {
                if (res.status == 'success') {

                    swal({
                        type: 'success',
                        title: 'Document Save',
                        showConfirmButton: false,
                        timer: 1000
                    });
                    setTimeout(function () {
                        window.location.reload();
                    }, 1000);


                } else if (res.status == 'dublicate') {

                    swal(
                        'Cancelled',
                        'Document Not Save :)',
                        'error'
                    )

                }
            }
        })
    }
}

//--------------------- Insert Function End -------------------- //  


//--------------------- Edit Function Start -------------------- //              

function edit(e) {

    var id = $(e).attr('data-id');

    $.ajax({
        type: "POST",
        data: 'id=' + id,
        url: 'select.php',
        cache: false,
        success: function (res) {
            if (res.status == 'success') {
                modals.putvalue('section', res.json);

                $('#id').val(res.json[0].id);

                closeNav1();

                var image_link = res.json[0].image_link;

                $('#img').attr('src', image_link);

                $('#btn-submit').attr('onclick', 'update()');
                $('#btn-submit').text('Update');

                $('#filelink1').html('<a class="btn btn-sm btn-primary" href="' + image_link + '" download>Download File</a>');

            } else {


            }
        }
    })
}

//--------------------- Edit Function End -------------------- //  

//--------------------- Update Function Start -------------------- //  

function update() {

    var formData = new FormData();
    var valid = true;

    if (checker('section') != false) {

        valid = valid * true;

    } else {

        valid = valid * false;

    }

    $('#img').each(function () {

        if (this.src.length > 0) {

            $('#photo').css('border-bottom', '1px solid green');

            $('#img').css('border', '1px solid green');

            valid = valid * true;

        } else {

            $('#photo').css('border-bottom', '1px solid red');

            $('#img').css('border', '1px solid red');

            valid = valid * false;

        }
    });


    if (valid) {

        data = checker('section');
        var datastr = JSON.stringify(data);
        formData.append('image', $('#photo')[0].files[0]);
        formData.append('data', datastr);
        formData.append('id', $('#id').val());


        $.ajax({
            type: "POST",
            data: formData,
            url: 'update.php',
            cache: false,
            processData: false,
            contentType: false,
            success: function (res) {
                if (res.status == 'success') {

                    swal({
                        type: 'success',
                        title: 'Document Update',
                        showConfirmButton: false,
                        timer: 1000
                    });
                    setTimeout(function () {
                        window.location.reload();
                    }, 1000);


                } else if (res.status == 'dublicate') {

                    swal(
                        'Cancelled',
                        'Document Not Update :)',
                        'error'
                    )

                }
            }
        })
    }
}

//--------------------- Update Function End -------------------- // 



</script>