<?php
$base='../../../';
$js='<script src="'.$base.'js/fwork.js"></script>
<script src="'.$base.'js/alart.js"></script>
<script src="'.$base.'js/list.min.js"></script>';
$css='<link rel="stylesheet" href="'.$base.'css/fwork.css">
<link rel="stylesheet" href="'.$base.'css/alart.css">';
include($base.'_in/header.php');
include($base.'_in/connect.php');
$con=_connect();
$json1='';
$json2='';
$json3='';
$json4='';

$result1=mysqli_query($con,"SELECT statecode,statename FROM mstates ORDER by statename ASC");
while($rows1 = mysqli_fetch_assoc($result1)){
 $statecode=$rows1["statecode"];
 $statename=$rows1["statename"];
 $json1.=',{"statecode":"'.$statecode.'","statename":"'.$statename.'"}';   
}


$result2=mysqli_query($con,"SELECT ccode,cname FROM mcustmast ORDER by id ASC");
while($rows2 = mysqli_fetch_assoc($result2)){
 $scode=$rows2["ccode"];
 $sname=$rows2["cname"];
 $json2.=',{"scode":"'.$scode.'","sname":"'.$sname.'"}';   
}

$result3=mysqli_query($con,"SELECT categoryname FROM mcategory ORDER by cid ASC");
while($rows3 = mysqli_fetch_assoc($result3)){
 $categoryname=$rows3["categoryname"];
 $json3.=',{"categoryname":"'.$categoryname.'"}';   
}

$result4=mysqli_query($con,"SELECT DISTINCTROW metal FROM mdensitymast ORDER BY metal ASC");
while($rows4 = mysqli_fetch_assoc($result4)){
 $metal=$rows4["metal"];
 $json4.=',{"metal":"'.$metal.'"}';   
}

$json1=substr($json1,1);
$json1='['.$json1.']';
$json2=substr($json2,1);
$json2='['.$json2.']';
$json3=substr($json3,1);
$json3='['.$json3.']';
$json4=substr($json4,1);
$json4='['.$json4.']';

?>
<style>
hr.style-hr {
    border: 0;
    height: 1px;
    background-image: linear-gradient(to right, rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.75), rgba(0, 0, 0, 0));
}

.table-list td,
   .table-list th {
       border: 1px solid #ddd;
       padding: 2px;
       font-size: 13px;
   }

   .table-list tr:nth-child(even) {
       background-color: #f2f2f2;
   }

   .table-list th {
       padding-top: 12px;
       padding-bottom: 12px;
       text-align: left;
       background-color: #16a085;
       color: white;
   }
   .sidenav1 {
       height: 100%;
       width: 0;
       position: fixed;
       z-index: 1;
       top: 0;
       left: 0;
       background-color: #111;
       overflow-x: hidden;
       transition: 0.5s;
       padding-top: 60px;
   }

   .sidenav1 a {
       padding: 8px 8px 8px 32px;
       text-decoration: none;
       font-size: 25px;
       color: #818181;
       display: block;
       transition: 0.3s;
   }

   .sidenav1 a:hover {
       color: #f1f1f1;
   }

   .sidenav1 .closebtn1 {
       position: absolute;
     
       right: 25px;
       font-size: 36px;
       margin-left: 50px;
   }

   @media screen and (max-height: 450px) {
       .sidenav1 {
           padding-top: 15px;
       }

       .sidenav1 a {
           font-size: 18px;
       }
   }

   .list {
       padding: 0px;
   }

   .list li span {
       display: inline;
   }

   .list li {
       text-decoration: none;
       display: block;
       padding: 2px;
       background: #FFF;
       border: 1px solid #333;
       color: #000;
   }
</style>
<input type="hidden" id="item-json1" value='<?php echo str_replace("'"," &#39;",$json1); ?>'/>
<input type="hidden" id="item-json4" value='<?php echo str_replace("'"," &#39;",$json2); ?>'/>
<input type="hidden" id="item-json5" value='<?php echo str_replace("'"," &#39;",$json3); ?>'/>
<input type="hidden" id="item-json6" value='<?php echo str_replace("'"," &#39;",$json4); ?>'/>
<div class="container-fluid" style="margin-left:5%">
    <div class="row content" id="section">

	<span style="font-size:24px;cursor:pointer" onclick="openNav1()">&#9776; Search</span>
<div id="mysidenav1" class="sidenav1" style="margin-left:80px;">

	<div id="test-list" style="padding:5px;">
		<a href="javascript:void(0)" class="closebtn1" onclick="closeNav1()">&times;</a>
	<br><br><br>
		<div class="form-group">
			<input type="text" placeholder="Search" class="form-control input-sm fuzzy-search" style="border-radius:3px;">
		</div>
		<ul class="list">
		</ul>
	</div>
</div>
<script>
var json4 = JSON.parse($("#item-json4").val());
var selectdb5='';
for (var i in json4) {
    selectdb5 += '<li><span class="scode">' + capitalize_Words(json4[i].sname) + '<span><button style="margin-left:2px;" data-scode="' + json4[i].scode + '" onclick="edit(this)" class="btn btn-primary btn-sm"><span class="fa fa-edit"></span> E</button></i>';
}
$('.list').append(selectdb5);
function openNav1() {
    document.getElementById("mysidenav1").style.width = "350px";
}

function closeNav1() {
    document.getElementById("mysidenav1").style.width = "0";
}
var monkeyList = new List('test-list', {
    valueNames: ['scode']
});
</script>
        <h2 align="center" style="margin-top:0px;">Create Customer Master</h2>

        <div style="clear:both;"></div>
        <hr class="style-hr">
        <input type="hidden" data-name='ccode' class="form-control input-sm">
        <div class="col-sm-4">
            <center> Customer Details</center>
            <hr class="style-hr">
            <div class="col-sm-4">Customer Name</div>
            <div class="col-sm-8">
                <div class="form-group"><input type="text" data-role='text' data-name='cname' class="form-control input-sm"></div>
            </div>
            <div class="col-sm-4">address</div>
            <div class="col-sm-8">
                <div class="form-group"><textarea data-role='textarea' data-name='address' class="form-control input-sm"> </textarea></div>
            </div>
            <div class="col-sm-4">Customer Group</div>
            <div class="col-sm-8">
                <div class="form-group"><input type="text" data-role='number' data-name='pin' class="form-control input-sm"></div>
            </div>
            <div class="col-sm-4">Transportation</div>
            <div class="col-sm-8">
                <div class="form-group"><input type="text" data-role='text' data-name='transportation' class="form-control input-sm"></div>
            </div>
            <div class="col-sm-4">Credit days</div>
            <div class="col-sm-8">
                <div class="form-group"><input type="text" data-role='text' data-name='creditdays' class="form-control input-sm"></div>
            </div>
            <div class="col-sm-4">State</div>
            <div class="col-sm-8">
                <div class="form-group">
                    <select class="form-control input-sm" data-role="select" id="state" data-name="state">
                        <option value="Select">Select</option>
                    </select>
                </div>
            </div>
            <div class="col-sm-4">GST Number</div>
            <div class="col-sm-8">
                <div class="form-group"><input type="text" data-role='text' data-name='gstnumber' class="form-control input-sm"></div>
            </div>
            <div class="col-sm-4">PAN</div>
            <div class="col-sm-8">
                <div class="form-group"><input type="text" data-role='text' data-name='pan' class="form-control input-sm"></div>
            </div>
            <div class="col-sm-4">W.O. Space</div>
            <div class="col-sm-8">
                <div class="form-group"><input type="text" data-role='text' data-name='wospace' class="form-control input-sm"></div>
            </div>
            <div class="col-sm-4">Sale Type</div>
            <div class="col-sm-8">
                <div class="form-group">
                <select class="form-control input-sm" data-role="select" data-name="saletype">
                        <option value="Select">Select</option>
                        <option value="S">SEZ</option>
                        <option value="M">Domestic</option>
                        <option value="A">Export</option>
                    </select>
                </div>
            </div>
            <div class="col-sm-4">Vendor Code</div>
            <div class="col-sm-8">
                <div class="form-group"><input type="text" data-name='vendorcode' class="form-control input-sm"></div>
            </div>
            <div class="col-sm-4">Westage Allowed</div>
            <div class="col-sm-8">
                <div class="form-group"><input type="text" data-name='westageallowed' class="form-control input-sm"></div>
            </div>
            <div class="col-sm-4">Powder Coting Area</div>
            <div class="col-sm-8">
                <div class="form-group"><input type="text" data-name='Powdercotingarea' class="form-control input-sm"></div>
            </div>

        </div>
        <div class="col-sm-8">
        <center>SHEET GROUP RATE</center>
        <div class="table-responsive">
                    <table id="metal-table" class="table table-list">
                    </table>
                </div>
            <div class="col-sm-12">
                <center> Contact Person Details</center>
                <hr class="style-hr">
                <center><button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#myModal">Add
                        + </button></center>
                <br>
                <div class="table-responsive">
                    <table id="contact-table" class="table table-list">
                        <thead>
                            <tr>
                                <th>SR.</th>
                                <th>Name</th>
                                <th>Department</th>
                                <th style="display:none;">Category</th>
                                <th>Mobile</th>
                                <th>Landline</th>
                                <th>Email</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>


            <div style="clear:both;"></div>
            <?php 
            
if(isset($_GET['enquiry'])){
 $enquiryid=mysqli_fetch_assoc(mysqli_query($con,"SELECT enquiryid x FROM enqtable ORDER BY id DESC"))['x'];

 if($enquiryid){
    $enquiryid++;
 }else{

    if ( date('m') > 4 ) {
        $year1 = date('y');
        $year2 = date('y')+1;
    }else{
        $year1 = date('y')-1;
        $year2 = date('y');
    }
    
    $enquiryid='CH/'.$year1.'-'.$year2.'/E1';
 }

?>
            <div class="col-sm-2">Enquiry No.</div>
            <div class="col-sm-4">
                <div class="form-group"><input type="text" value="<?php echo $enquiryid; ?>" data-name='enquiryid' class="form-control input-sm" readonly></div>
            </div>
            <div class="col-sm-2" style="display:none;">Category</div>
            <div class="col-sm-4" style="display:none;">
                <div class="form-group">
                <select  data-name='category' class="form-control input-sm">
                <option value="Product">Product</option>
                </select>
                </div>
            </div>
            <div class="col-sm-2">FollowUp</div>
            <div class="col-sm-4">
                <div class="form-group"><input type="date"  data-name='followupdate' class="form-control input-sm"></div>
            </div>
            <div class="col-sm-2">Note</div>
            <div class="col-sm-4">
                <div class="form-group"><input type="text" data-name='note' class="form-control input-sm"></div>
            </div>


<?php
}
?>
        <div class="col-sm-12">
            <div style="clear:both;"></div>
            <hr class="style-hr">

            <div class="col-sm-3">
            </div>
            <div class="col-sm-3"><button class="btn btn-success btn-sm btn-block"  id="btn-submit" onclick="submit()" style="margin:5px;">Submit</button></div>
            <div class="col-sm-3"><button class="btn btn-danger btn-sm btn-block" id="btn-reset" style="margin:5px;">Reset</button></div>
            <div class="col-sm-3"></div>
        </div>
    </div>
</div>
    <br>
    <br>

    <!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" align="center">Add Contact</h4>
            </div>
            <div class="modal-body" id='modal'>
                <div class="row">
                    <div class="col-sm-4">Person Name</div>
                    <div class="col-sm-8">
                        <div class="form-group"><input type="text" data-role='text' data-name='name' class="form-control input-sm"></div>
                    </div>
                    <div class="col-sm-4">Department</div>
                    <div class="col-sm-8">
                        <div class="form-group"><input type="text" data-role='text' data-name='department' class="form-control input-sm"></div>
                    </div>
                    <div class="col-sm-4" style="display:none;">Category</div>
                    <div class="col-sm-8" style="display:none;">
                        <div class="form-group">
                            <select class="form-control input-sm"  id="category" data-name="category">
                                <option value="Select">Select</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-4">Mobile</div>
                    <div class="col-sm-8">
                        <div class="form-group"><input type="text" data-role='phone' data-name='mobile' class="form-control input-sm"></div>
                    </div>
                    <div class="col-sm-4">Landline No.</div>
                    <div class="col-sm-8">
                        <div class="form-group"><input type="text" data-name='landline' class="form-control input-sm"></div>
                    </div>
                    <div class="col-sm-4">Email</div>
                    <div class="col-sm-8">
                        <div class="form-group"><input type="text" data-role='text' data-name='email' class="form-control input-sm"></div>
                    </div>
                    <div style="clear:both;"></div>
                    <hr>
                    <div class="col-sm-4"></div>
                    <div class="col-sm-4"><button class="btn btn-success btn-sm btn-block" id="btn-contact" style="margin:5px;">Submit</button></div>
                    <div class="col-sm-4"><button class="btn btn-danger btn-sm btn-block" id="btn-clear" style="margin:5px;">Clear</button></div>

                </div>
            </div>
        </div>
    </div>
</div>

<?php
include($base.'_in/footer.php');
?>
<script>
var strdb1='';
var strdb2='';
var strdb3='';
var strdb4='';
var json1 = JSON.parse($("#item-json1").val());
for (var i in json1) {  
        strdb1 += '<option value="' + json1[i].statecode + '">' + capitalize_Words(json1[i].statename) + '</option>';
}

var json5 = JSON.parse($("#item-json5").val());
for (var i in json5) {  
        strdb2 += '<option value="' + json5[i].categoryname + '">' + capitalize_Words(json5[i].categoryname) + '</option>';
}

var json6 = JSON.parse($("#item-json6").val());
 
       for(var i in json6){
                strdb3+='<th align="center">'+json6[i].metal+'</th>';
                strdb4+='<td align="center"><input type="text" data-name="'+json6[i].metal+'" class="form-control input-sm"></td>';
       }

$('#metal-table').append('<thead><tr>'+strdb3+'</tr></thead></tbody><tr>'+strdb4+'</tr></tbody>');

$('#state').append(strdb1);
$('#category').append(strdb2);
$('#btn-contact').on('click',function(){
    valid=true;
    if(checker('modal') !=false ) {
			valid=valid*true;
		}else{
		    valid=valid*false;
         }
    if(valid){
        data=checker('modal');
        tableui(data.name,data.department,data.category,data.mobile,data.email,data.landline);
        modals.clear('modal');
    }
})



var count=1;
function tableui(name,department,category,mobile,email,landline){
var sr=count;
count++;
var str='';
str+='<tr id="tr-'+sr+'">';
str+='<td>'+sr+'</td>';
str+='<td>'+name+'</td>';
str+='<td>'+department+'</td>';
str+='<td style="display:none;">'+category+'</td>';
str+='<td>'+mobile+'</td>';
str+='<td>'+landline+'</td>';
str+='<td>'+email+'</td>';
str+='<td><button data-id="tr-'+sr+'" onclick="remover(this)" class="btn btn-danger btn-sm" >R</nutton></td>';
str+='</tr>';

$('#contact-table tbody').append(str);
}

  function remover(e) {
    	$(e).parent().parent().fadeOut(1000, function () {
    		$(this).remove();
    	});
    }

function edit(e){
$('#btn-submit').attr('onclick','update()');
var scode=$(e).attr('data-scode');
$('#contact-table tbody > tr').remove();
			$.ajax({
				type: "POST",
				data: 'scode='+ scode,
				url: 'select.php',
				cache: false,
				success: function (res) {
					if (res.status == 'success') {
                        modals.putvalue('section',res.json);
                        modals.putvalue('metal-table',res.json[0].sheetrate);
                        var contactperson=res.json[0].contactperson;
                            for(var i in contactperson){
                               var table= contactperson[i].table;
                               tableui(table[0],table[1],table[2],table[3],table[5],table[4]);
                            }
                    }
				}
            });
}

function update(){
    var valid=true;
    if(checker('section')!=false) {
			valid=valid*true;
		}else{
		    valid=valid*false;
         }
    if(modals.gettable1('contact-table')!=false){
        valid=valid*true;
		}else{
		valid=valid*false;
    }

    if(valid){
var statename=$('#state option:selected').stext();
        var data=checker('section');
        var data1=checker('metal-table');
        var datastr = JSON.stringify(data);
        var datastr1 = JSON.stringify(data1);
        var datastr2=JSON.stringify(modals.gettable1('contact-table'));
			$.ajax({
				type: "POST",
				data: {
                    data: datastr,
                    data2:datastr2,
                    data3:datastr1,
                    state:statename
				},
				url: 'update.php',
				cache: false,
				success: function (res) {
					if (res.status == 'success') {
						swal({
							type: 'success',
							title: 'Customer Master Updated',
							showConfirmButton: false,
							timer: 3000
						});
						setTimeout(function () {
							window.location.reload();
						}, 3000);
                    }
                    if(res.status == 'falid2'){
                        swal('Supplier Master Not Exist', '', 'error');
                        $('#inp-text-1').val('');
                        $('#inp-phone').css('border-bottom', '1px solid red');
                    }
				}
			});
    }
}

$('#btn-reset').on('click',function(){
    window.location.reload();
});

function submit(){
    var valid=true;
    if(checker('section')!=false) {
			valid=valid*true;
		}else{
		    valid=valid*false;
         }
    if(modals.gettable1('contact-table')!=false){
        valid=valid*true;
		}else{
		valid=valid*false;
    }

    if(valid){
var statename=$('#state option:selected').stext();
        var data=checker('section');
        var data1=checker('metal-table');
        var datastr = JSON.stringify(data);
        var datastr1 = JSON.stringify(data1);
        var datastr2=JSON.stringify(modals.gettable1('contact-table'));
			$.ajax({
				type: "POST",
				data: {
                    data: datastr,
                    data2:datastr2,
                    data3:datastr1,
                    state:statename
				},
				url: 'insert.php',
				cache: false,
				success: function (res) {
					if (res.status == 'success') {
						swal({
							type: 'success',
							title: 'Customer Master Created',
							showConfirmButton: false,
							timer: 3000
						});
						setTimeout(function () {
							window.location.reload();
						}, 3000);
                    }
                    if(res.status == 'falid2'){
                        swal('Customer Master already Exist', '', 'error');
                        $('#inp-text-1').val('');
                        $('#inp-phone').css('border-bottom', '1px solid red');
                    }
				}
			});
    }
}


</script>