<?php
    
  $base = '../../../';

  include($base.'_in/connect.php');

  header('content-type: application/json; charset=utf-8');

  header("access-control-allow-origin: *");
    
  if(isset($_POST['customerid'])) {

    $con = _connect();
     
    $customerid =_clean($con,$_POST["customerid"]);

    $results = mysqli_fetch_assoc(mysqli_query($con,"SELECT customerjson,contactjson,chajson, addressjson FROM customermaster WHERE customerid='$customerid'"));

    if($results){

      if ($results["chajson"] == '') {

        $chajson = '[]';
          
      } else {

        $chajson = $results["chajson"];

      }

      if ($results["addressjson"] == '') {

        $addressjson = '[]';
          
      } else {

        $addressjson = $results["addressjson"];

      }

      echo '{"status":"success","customerjson":['.$results["customerjson"].'], "contactjson":'.$results["contactjson"].', "chajson":'.$chajson.', "addressjson":'.$addressjson.' }';

    } else {

      echo '{"status":"falid2"}';

    }

    _close($con);

  } else {

    echo '{"status":"falid1"}';

  }
  
?>