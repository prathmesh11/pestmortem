<?php

    $base = '../../../';

    $js   = '<script src="'.$base.'js/fwork.js"></script>
    <script src="'.$base.'js/alart.js"></script>
    <script src="'.$base.'/js/select2.min.js"></script>
    <script src="'.$base.'js/list.min.js"></script>';

    $css  = '<link rel="stylesheet" href="'.$base.'css/fwork.css">
    <link href="'.$base.'/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="'.$base.'css/alart.css">';

    include($base.'_in/header.php');

    include($base.'_in/connect.php');

    if (!in_array("Customer Master", $arrysession)) {
   
        echo "<script>window.location.href='/process/dashboard.php';</script>";
       
        exit;
    
    }

    $con = _connect();

    if(isset($_GET['enquiryid'])) {

        $enquiryid = $_GET['enquiryid']; 

    }


    if (session_status()==PHP_SESSION_NONE) { session_start(); }

    $created_by = $_SESSION['employeeid'];

    $branchid   = $_SESSION['branchid'];

?>

<style>
    hr.style-hr {
        border: 0;
        height: 1px;
        background-image: linear-gradient(to right, rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.75), rgba(0, 0, 0, 0));
    }

    .table-list td,
    .table-list th {
        border: 1px solid #ddd;
        padding: 2px;
        font-size: 13px;
    }

    .table-list tr:nth-child(even) {
        background-color: #f2f2f2;
    }

    .table-list th {
        padding-top: 12px;
        padding-bottom: 12px;
        text-align: left;
        background-color: #16a085;
        color: white;
    }

    .sidenav1 {
        height: 100%;
        width: 0;
        position: fixed;
        z-index: 1;
        top: 0;
        left: 0;
        background-color: #111;
        overflow-x: hidden;
        transition: 0.5s;
        padding-top: 60px;
    }

    .sidenav1 a {
        padding: 8px 8px 8px 32px;
        text-decoration: none;
        font-size: 25px;
        color: #818181;
        display: block;
        transition: 0.3s;
    }

    .sidenav1 a:hover {
        color: #f1f1f1;
    }

    .sidenav1 .closebtn1 {
        position: absolute;

        right: 25px;
        font-size: 36px;
        margin-left: 50px;
    }

    @media screen and (max-height: 450px) {
        .sidenav1 {
            padding-top: 15px;
        }

        .sidenav1 a {
            font-size: 18px;
        }
    }

    .list2 {
        padding: 0px;
    }

    .list2 li span {
        display: inline;
    }

    .list2 li {
        text-decoration: none;
        display: block;
        padding: 2px;
        background: #FFF;
        border: 1px solid #333;
        color: #000;
    }

    .list1 li:hover {
        background: #0984e3;
        color: #fff;
    }
</style>

<input type="hidden" value="<?php echo $enquiryid;?>" id="enquiryid">

<div class="container-fluid">

    <div class="row content" id="section" autocomplete="off">

        <span style="font-size:24px;cursor:pointer" onclick="openNav1()">&#9776; Search</span>

        <div id="mysidenav1" class="sidenav1" style="margin-left:80px;">

            <div id="test-list" style="padding:5px;">

                <a href="javascript:void(0)" class="closebtn1" onclick="closeNav1()">&times;</a>

                <br><br><br>

                <div class="form-group">

                    <input type="text" placeholder="Search" class="form-control input-sm fuzzy-search" style="border-radius:3px;">

                </div>

                <ul class="list list2">

                    <?php 

                        $result1=mysqli_query($con, "SELECT * FROM `customermaster` ORDER BY customername");

                        while($rows=mysqli_fetch_assoc($result1)) {

                            echo '<li><span class="customermaster">' .$rows['customername']. '<span><button style="margin-left:2px;" data-customerid="' .$rows['customerid']. '" onclick="edit(this)" class="btn btn-primary btn-sm"><span class="fa fa-edit"></span> E</button></i>';
                        }

                    ?>

                </ul>

            </div>

        </div>

        <h2 align="center" style="margin-top:0px;">Create Customer Master</h2>

        <div style="clear:both;"></div>

        <hr class="style-hr">

        <input type="hidden" value="<?php echo $created_by;?>" data-name='created_by' id="created_by">

        <input type="hidden" value="<?php echo $branchid;?>" data-name='branchid' id="branchid">

        <input type="hidden" data-name='ccode' class="form-control input-sm">

        <div class="col-sm-4">

            <center> Customer Details</center>

            <hr class="style-hr">

            <div class="col-sm-4">Customer Name</div>

            <div class="col-sm-8">

                <div class="form-group">

                    <input type="text" data-role='text' data-name='customername' class="form-control input-sm" id="customername">

                    <input type="text" id='customerid' style="display:none;">

                </div>

            </div>

            <!-- <div class="col-sm-4">Address</div>

            <div class="col-sm-8">

                <div class="form-group">
                    
                    <textarea data-role='textarea' data-name='address' class="form-control input-sm" id="address"> </textarea>
                    
                </div>

            </div>

            <div class="col-sm-4">City</div>

            <div class="col-sm-8">

                <div class="form-group">
                    
                    <input type="text" data-role='text' data-name='city' class="form-control input-sm" >

                </div>

            </div> -->

            <div class="col-sm-4">Email Id</div>

            <div class="col-sm-8">

                <div class="form-group">
                    
                    <input type="text" data-role='text' data-name='emailId' class="form-control input-sm" id="emailId">

                </div>

            </div>

            <div class="col-sm-4">Cust. Category</div>

            <div class="col-sm-8">

                <div class="form-group">

                    <select class="form-control input-sm" data-role="select" data-name="custCategory" id="custCategory">

                        <option value="Select">Select Customer Category</option>

                        <option value="Individual">Individual</option>

                        <option value="Business">Business</option>

                    </select>

                </div>

            </div>

            <div class="col-sm-4">Customer Group</div>

            <div class="col-sm-8">

                <div class="form-group" id="test-list1">

                    <input type="text" data-role='text' placeholder="Search" class="form-control input-sm fuzzy-search"
                        data-name='customergroupcode' id="customergroupcode" style="border-radius:4px;">

                        <div id="searchbox" style="border:1px solid #74b9ff; height:60px; overflow-y: scroll;display:none;">

                        <ul class="list list1" style="list-style-type: none;padding-left:0px;margin-bottom:0;">

                            <?php 

                                $result1=mysqli_query($con, "SELECT DISTINCT customergroupcode FROM `customermaster`");

                                while($rows=mysqli_fetch_assoc($result1)) {

                                    echo '<li style="padding-left:15px;" onclick="jump(this)"><span class="customergroupcode">' .$rows['customergroupcode']. '<span></i>';

                                }

                            ?>

                        </ul>

                    </div>

                </div>

            </div>
            <!-- <div class="col-sm-4">Transportation</div>
            <div class="col-sm-8">
                <div class="form-group"><input type="text" data-role='text' data-name='transportation' class="form-control input-sm"></div>
            </div> -->
            <div class="col-sm-4">Credit days</div>

            <div class="col-sm-8">

                <div class="form-group">
                    
                    <input type="number" data-role='number' value=30 data-name='creditdays'
                        class="form-control input-sm">
                
                </div>

            </div>

            <div class="col-sm-4">Country</div>

            <div class="col-sm-8">

                <div class="form-group">

                    <select class="form-control input-sm" data-role="select" data-name="country">

                        <option value="IN">India</option>

                        <?php 
                            
                            $result1=mysqli_query($con, "SELECT * FROM `countrymaster` ORDER BY countryname ");
                                
                            while($rows=mysqli_fetch_assoc($result1)) {

                                echo '<option value="'.$rows['countrycode'].'">'.$rows['countryname'].'</option>';
                            }

                        ?>

                    </select>

                </div>

            </div>

            <div class="col-sm-4">State</div>

            <div class="col-sm-8">

                <div class="form-group">
                
                    <select class="form-control input-sm" data-role="select" id="state" data-name="state">

                        <option value="Select">Select</option>

                        <?php 

                            $result1=mysqli_query($con, "SELECT * FROM `statemaster` ORDER BY statename");
                            
                            while($rows=mysqli_fetch_assoc($result1)) {
                                
                                echo '<option value="'.$rows['statecode'].'">'.$rows['statename'].'</option>';
                            }

                        ?>

                    </select>

                </div>

            </div>

            <div class="col-sm-4">GST Number</div>

            <div class="col-sm-8">

                <div class="form-group">
                    
                    <input type="text" data-name='gstnumber' class="form-control input-sm">
                
                </div>

            </div>

            <div class="col-sm-4">GST On Bill </div>

            <div class="col-sm-8">

                <div class="form-group">

                    <select class="form-control input-sm" data-role="select" data-name="gstOnBill">

                        <option value="Select">Select GST Required On Bill</option>

                        <option value="Yes">Yes</option>

                        <option value="No">No</option>

                    </select>

                </div>

            </div>

            <!-- <div class="col-sm-4">Sale Type</div>
            <div class="col-sm-8">
                <div class="form-group">
                <select class="form-control input-sm"  data-name="saletype">
                        <option value="Select">Select</option>
                        <option value="S">SEZ</option>
                        <option value="M">Domestic</option>
                        <option value="A">Export</option>
                    </select>
                </div>
            </div> -->
            <div class="col-sm-4">Vendor Code</div>

            <div class="col-sm-8">

                <div class="form-group">
                    
                    <input type="text" data-name='vendorcode' class="form-control input-sm">
                
                </div>

            </div>

            <div class="col-sm-4">Billing Currency</div>

            <div class="col-sm-8">

                <div class="form-group">

                    <input type="text" data-name='westageallowed' class="form-control input-sm">

                </div>

            </div>

            <div class="col-sm-4">Payment Terms </div>

            <div class="col-sm-8">

                <div class="form-group">

                    <select class="form-control input-sm" data-role="select" data-name="paymentTerms">

                        <option value="Select">Select</option>

                        <option value="Cash">Cash</option>

                        <option value="Bank">Bank</option>

                    </select>

                </div>

            </div>

            <div class="col-sm-4">Opening Balance</div>

            <div class="col-sm-8">

                <div class="form-group">

                    <input type="number" data-name='opening_balance' value="0" class="form-control input-sm">

                </div>

            </div>


            <div class="col-sm-4">Opening_Bal. Date</div>

            <div class="col-sm-8">

                <div class="form-group">

                    <input type="date" data-name='opening_date' value="<?php echo $today;?>" class="form-control input-sm">

                </div>

            </div>

        </div>

        <div class="col-sm-8">

            <div class="col-sm-12">

                <center> Contact Person Details</center>

                <hr class="style-hr">

                <center>
                
                    <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#myModal">Add + </button>

                </center>
                <br>

                <div class="table-responsive">

                    <table id="contact-table" class="table table-list">

                        <thead>

                            <tr>

                                <th>SR.</th>
                                <th>Name</th>
                                <th>Department</th>
                                <th style="display:none;">Category</th>
                                <th>Mobile</th>
                                <th>Landline</th>
                                <th>Email</th>
                                <th>Action</th>

                            </tr>

                        </thead>

                        <tbody>
                            
                        </tbody>

                    </table>

                </div>

            </div>


            <div style="clear:both;"></div>
            <?php 

                if(isset($_GET['enquiry'])){
                $enquiryid=mysqli_fetch_assoc(mysqli_query($con,"SELECT enquiryid x FROM enqtable ORDER BY id DESC"))['x'];

                if($enquiryid){
                    $enquiryid++;
                }else{
                    if ( date('m') > 4 ) {
                        $year1 = date('y');
                        $year2 = date('y')+1;
                    }else{
                        $year1 = date('y')-1;
                        $year2 = date('y');
                }
        
                $enquiryid='CH/'.$year1.'-'.$year2.'/E1';
                }

            ?>
            <div class="col-sm-2">Enquiry No.</div>
            <div class="col-sm-4">
                <div class="form-group"><input type="text" value="<?php echo $enquiryid; ?>" data-name='enquiryid'
                        class="form-control input-sm" readonly></div>
            </div>
            <div class="col-sm-2">Category</div>
            <div class="col-sm-4">
                <div class="form-group">
                    <select data-name='category' class="form-control input-sm">
                        <option value="Fumigation">Fumigation</option>
                        <option value="Pest Control">Pest Control</option>
                    </select>
                </div>
            </div>
            <div class="col-sm-2">FollowUp</div>
            <div class="col-sm-4">
                <div class="form-group"><input type="date" data-name='followupdate' class="form-control input-sm"></div>
            </div>
            <div class="col-sm-2">Note</div>
            <div class="col-sm-4">
                <div class="form-group"><input type="text" data-name='note' class="form-control input-sm"></div>
            </div>


            <?php
                }
            ?>

            <div style="clear:both;"></div>

            <hr class="style-hr">

            <center>
                
                <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#myModalCha">Add Cha + </button>
                
            </center>

            <div style="clear:both;"></div>

            <hr class="style-hr">

            <div class="table-responsive">

                <table id="cha-table" class="table table-list">

                    <thead>

                        <tr>

                            <th width="10%">SR.</th>
                            <th>CHA</th>
                            <th width="10%">Action</th>

                        </tr>

                    </thead>

                    <tbody>

                    </tbody>

                </table>

            </div>

            <div style="clear:both;"></div>

            <hr class="style-hr">

            <center>
                
                <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#myModalAddress">Add Address + </button>
                
            </center>

            <div style="clear:both;"></div>

            <hr class="style-hr">

            <div class="table-responsive">

                <table id="address-table" class="table table-list">

                    <thead>

                        <tr>

                            <th width="10%">SR.</th>
                            <th>Address</th>
                            <th>City</th>
                            <th width="10%">Action</th>

                        </tr>

                    </thead>

                    <tbody>

                    </tbody>

                </table>

            </div>


            <div class="col-sm-12">

                <div style="clear:both;"></div>

                <hr class="style-hr">

                <div class="col-sm-3"></div>

                <div class="col-sm-3">
                    
                    <button class="btn btn-success btn-sm btn-block" id="btn-submit" onclick="submit()" style="margin:5px;">Submit</button>
                
                </div>

                <div class="col-sm-3">
                    
                    <button class="btn btn-danger btn-sm btn-block" id="btn-reset" style="margin:5px;">Reset</button>

                </div>

                <div class="col-sm-3"></div>

            </div>

        </div>

    </div>

    <br><br>

    <!-- Modal Contact Start -->

    <div id="myModal" class="modal fade" role="dialog">

        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" align="center">Add Contact</h4>
                </div>
                <div class="modal-body" id='contactModal'>
                    <div class="row">

                      <div class="col-sm-4 customerClass" style="display:none;">Customer Name</div>

                        <div class="col-sm-8 customerClass" style="display:none; margin-bottom:10px;" >

                            <select class="form-control input-sm select-js1" id="custName" data-role="" data-name="custName">

                                <option value="Select">Select Customername</option>

                                <?php

                                    $account = mysqli_query($con,"SELECT customerid,customername,city FROM customermaster WHERE branchid='$branchid' ORDER BY customername");

                                        while ($row = mysqli_fetch_assoc($account)) {

                                            echo '<option value="'.$row['customerid'].'">'.$row['customername'].'</option>';

                                        }
                                                
                                ?>

                            </select>

                        </div>

                        <div class="col-sm-4">Person Name</div>
                        <div class="col-sm-8">
                            <div class="form-group"><input type="text" data-role='text' id="personName" data-name='name'
                                    class="form-control input-sm"></div>
                        </div>
                        <div class="col-sm-4">Department</div>
                        <div class="col-sm-8">
                            <div class="form-group"><input type="text" id="department" data-name='department'
                                    class="form-control input-sm"></div>
                        </div>
                        <div class="col-sm-4">Category</div>
                        <div class="col-sm-8">
                            <div class="form-group">
                                <select class="form-control input-sm" id="category" data-name="category">
                                    <option value="Select">Select</option>
                                    <option value="fumigation">Fumigation</option>
                                    <option value="pest control">Pest Control</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-4">Mobile</div>
                        <div class="col-sm-8">
                            <div class="form-group"><input type="text" id="phone" data-role='text' data-name='mobile'
                                    class="form-control input-sm"></div>
                        </div>
                        <div class="col-sm-4">Landline No.</div>
                        <div class="col-sm-8">
                            <div class="form-group"><input type="text" id="landline" data-name='landline'
                                    class="form-control input-sm"></div>
                        </div>
                        <div class="col-sm-4">Email</div>
                        <div class="col-sm-8">
                            <div class="form-group"><input type="text" id="email" data-name='email' class="form-control input-sm">
                            </div>
                        </div>
                        <div style="clear:both;"></div>
                        <hr>
                        <div class="col-sm-4"></div>
                        <div class="col-sm-4">
                        <button class="btn btn-success btn-sm btn-block" id="btn-contact"
                        data-edits="" onclick="addContactPerson(this)" style="margin:5px;">Submit</button>
                        <button class="btn btn-success btn-sm btn-block hidden" id="btn-move" data-move="" style="margin:5px;">Move</button>

                        </div>
                        <div class="col-sm-4"><button class="btn btn-danger btn-sm btn-block" id="btn-clear"
                                style="margin:5px;">Clear</button></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal Contact End -->


    <!-- Modal ADD CHA Start -->
    <div id="myModalCha" class="modal fade" role="dialog">

        <div class="modal-dialog">
            <!-- Modal content-->

            <div class="modal-content">

                <div class="modal-header">

                    <button type="button" class="close" data-dismiss="modal">&times;</button>

                    <h4 class="modal-title" align="center">Add Cha</h4>

                </div>

                <div class="modal-body" id='modals'>

                    <div class="row">

                        <div class="col-sm-2">Cha</div>

                        <div class="col-sm-10">

                            <div class="form-group">

                                <input type="text" data-role='text' id="cha" data-name='cha' class="form-control input-sm">

                            </div>

                        </div>

                        <div style="clear:both;"></div>

                        <hr>

                        <div class="col-sm-4"></div>

                        <div class="col-sm-4">

                            <button class="btn btn-success btn-sm btn-block" id="btn-cha" style="margin:5px;" data-edit="" onclick="addcha(this)">Submit</button>

                        </div>

                        <div class="col-sm-4">

                            <button class="btn btn-danger btn-sm btn-block" id="btn-clear" style="margin:5px;">Clear</button>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>
    <!-- Modal Cha End -->


       <!-- Modal ADD Address Start -->
    <div id="myModalAddress" class="modal fade" role="dialog">

        <div class="modal-dialog">
            <!-- Modal content-->

            <div class="modal-content">

                <div class="modal-header">

                    <button type="button" class="close" data-dismiss="modal">&times;</button>

                    <h4 class="modal-title" align="center">Add Address</h4>

                </div>

                <div class="modal-body" id='modalAddress'>

                    <div class="row">

                        <div class="col-sm-2">Address</div>

                        <div class="col-sm-10">

                            <div class="form-group">

                                <textarea type="text" data-role='text' id="multiAddress" data-name='multiAddress' class="form-control input-sm"></textarea>

                            </div>

                        </div>

                        <div class="col-sm-2">City</div>

                        <div class="col-sm-10">

                            <div class="form-group">

                                <input type="text" data-role='text' id="multiCity" data-name='multiCity' class="form-control input-sm">

                            </div>

                        </div>

                        <div style="clear:both;"></div>

                        <hr>

                        <div class="col-sm-4"></div>

                        <div class="col-sm-4">

                            <button class="btn btn-success btn-sm btn-block" id="btn-address" style="margin:5px;" data-editAddress="" onclick="addaddress(this)">Submit</button>

                        </div>

                        <div class="col-sm-4">

                            <button class="btn btn-danger btn-sm btn-block" id="btn-clear" style="margin:5px;">Clear</button>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>
<!-- Modal Cha End -->

<?php

    include($base.'_in/footer.php');

?>
<script>



function openNav1() {
    document.getElementById("mysidenav1").style.width = "300px";
}

function closeNav1() {
    document.getElementById("mysidenav1").style.width = "0";
}
var monkeyList = new List('test-list', {
    valueNames: ['customermaster']
});

// var monkeyList1 = new List('test-list1', {
//     valueNames: ['customergroupcode']
// });


function addContactPerson(e) {

    var chks   = $(e).attr('data-edits');
    var data  = checker('contactModal');
    var personName = data.name;
    var department = data.department;
    var category   = data.category;
    var mobile     = data.mobile;
    var landline   = data.landline;
    var email      = data.email;
    var valid      = true;

    if (checker('contactModal') != false) {

        valid = valid * true;

    } else {

        valid = valid * false;

    }

    $('#contact-table .personName').each(function () {

        var personName1 = $(this).text().trim();

        if (personName1 == personName && chks == '') {

            valid = valid * false;

            alert('Dublicate Person name');
        }

    });



    if (valid) {

        var chks = $(e).attr('data-edits');
        console.log(chks);

        if (chks == '') {

            var len = $('#contact-table .srNo').length + 1;
            
            contact_adder(len, personName,department,category,mobile,landline,email);

        } else {

            contact_adder_edit(chks, personName,department,category,mobile,landline,email);

        }

        modals.clear('contactModal');
        $('#btn-contact').attr('data-edits', '');
        $('#myModal').modal('toggle');
        $('#btn-move').attr('data-move', '');


    }

}


function contact_adder(len, personName,department,category,mobile,landline,email) {

    var str = '<tr>';
    str += '<td align="center" class="srNo">' + len + '</td>';
    str += '<td align="center" class="name">' + personName + '</td>';
    str += '<td align="center" class="department">' + department + '</td>';
    str += '<td align="center" class="category" style="display:none">' + category + '</td>';
    str += '<td align="center" class="mobile">' + mobile + '</td>';
    str += '<td align="center" class="landline">' + landline + '</td>';
    str += '<td align="center" class="email">' + email + '</td>';
    str += '<td align="center"><button class="btn btn-sm btn-success" data-toggle="modal" data-target="#myModal" onclick="contactEditor(' + len + ',this) ">E</button>  <button class="btn btn-sm btn-danger remover" onclick="remover(this)">R</button> <button class="btn btn-sm btn-info hidden moveButton" data-toggle="modal" data-target="#myModal" id="moveButton" onclick="contactMove(' + len + ',this)"> M </button> </td>';
    str += '</tr>';
    $('#contact-table > tbody').append(str);

}

function contact_adder_edit(len, personName,department,category,mobile,landline,email) {

    var str = '<td align="center" class="srNo">' + len + '</td>';
    str += '<td align="center" class="name">' + personName + '</td>';
    str += '<td align="center" class="department">' + department + '</td>';
    str += '<td align="center" class="category" style="display:none">' + category + '</td>';
    str += '<td align="center" class="mobile">' + mobile + '</td>';
    str += '<td align="center" class="landline">' + landline + '</td>';
    str += '<td align="center" class="email">' + email + '</td>';    
    str += '<td align="center"><button class="btn btn-sm btn-success" data-toggle="modal" data-target="#myModal" onclick="contactEditor(' + len + ',this)">E</button>  <button class="btn btn-sm btn-danger remover" onclick="remover(this)">R</button> <button class="btn btn-sm btn-info hidden moveButton" data-toggle="modal" data-target="#myModal" id="moveButton"  onclick="contactMove(' + len + ',this)"> M </button> </td>';

    $('#contact-table .srNo').each(function () {
        var srNo = $(this).text().trim();
        if (srNo == len) {
            $(this).parent().html(str);
        }
    });


}

function contactEditor(srNo, e) {
    $('#personName').val($(e).parent().parent().find('.name').text().trim());
    $('#department').val($(e).parent().parent().find('.department').text().trim());
    $('#category').val($(e).parent().parent().find('.category').text().trim());
    $('#phone').val($(e).parent().parent().find('.mobile').text().trim());
    $('#landline').val($(e).parent().parent().find('.landline').text().trim());
    $('#email').val($(e).parent().parent().find('.email').text().trim());
    $('#btn-contact').attr('data-edits', srNo);

    $('.customerClass').hide();
    
    $('#btn-move').addClass('hidden');
    
    $('#btn-contact').removeClass('hidden');
    
    $('#btn-move').attr('data-move', '');

    $('#custName').attr('data-role','');

}

function contactMove(srNo, e) {
    
    $('#personName').val($(e).parent().parent().find('.name').text().trim());
    
    $('#department').val($(e).parent().parent().find('.department').text().trim());
    
    $('#category').val($(e).parent().parent().find('.category').text().trim());
    
    $('#phone').val($(e).parent().parent().find('.mobile').text().trim());
    
    $('#landline').val($(e).parent().parent().find('.landline').text().trim());
    
    $('#email').val($(e).parent().parent().find('.email').text().trim());
    
    $('#btn-contact').attr('data-edits', srNo);
    
    $('.customerClass').show();
    
    $('#btn-move').removeClass('hidden');
    
    $('#btn-contact').addClass('hidden');
    
    $('#btn-move').attr('data-move', srNo);

    $('#custName').attr('data-role','select');

}

// add function start :
function addcha(e) {

    var chk   = $(e).attr('data-edit');
    var data  = checker('modals');
    var cha   = data.cha;
    var valid = true;

    if (checker('modals') != false) {

        valid = valid * true;

    } else {

        valid = valid * false;

    }

    $('#cha-table .cha').each(function () {

        var cha1 = $(this).text().trim();

        if (cha1 == cha && chk == '') {

            valid = valid * false;

            alert('Dublicate Cha name');
        }

    });



    if (valid) {

        var chk = $(e).attr('data-edit');

        if (chk == '') {

            var len = $('#cha-table .srno').length + 1;
            
            fun_adder(len, cha);

        } else {

            fun_adder_edit(chk, cha);

        }

        modals.clear('modals');
        $('#btn-cha').attr('data-edit', '');
        $('#myModalCha').modal('hide');


    }

}


function fun_adder(len, cha) {

    var str = '<tr>';
    str += '<td align="center" class="srno">' + len + '</td>';
    str += '<td align="center" class="cha">' + cha + '</td>';
    str += '<td align="center"><button class="btn btn-sm btn-success" data-toggle="modal" data-target="#myModalCha" onclick="editor(' + len + ',this) ">E</button>  <button class="btn btn-sm btn-danger remover" onclick="remover(this)">R</buuton></td>';
    str += '</tr>';
    $('#cha-table > tbody').append(str);

}

function fun_adder_edit(len, cha) {

    var str = '<td align="center" class="srno">' + len + '</td>';
    str += '<td align="center" class="cha">' + cha + '</td>';
    str += '<td align="center"><button class="btn btn-sm btn-success" data-toggle="modal" data-target="#myModalCha" onclick="editor(' + len + ',this)">E</button>  <button class="btn btn-sm btn-danger remover" onclick="remover(this)">R</buuton></td>';

    $('#cha-table .srno').each(function () {
        var srno = $(this).text().trim();
        if (srno == len) {
            $(this).parent().html(str);
        }
    });


}

function editor(srno, e) {

    $('#cha').val($(e).parent().parent().find('.cha').text().trim());

    $('#btn-cha').attr('data-edit', srno);
}


// -----------------------Remover Function Start----------------------//

function remover(e) {

    
    $(e).parent().parent().fadeOut(1000, function () {
    
        $(this).remove();
    
    });

}

// -----------------------Remover Function End----------------------//


function jump(e) {
    var customergroupcode = $(e).find('span').text().trim();
    $('#customergroupcode').val(customergroupcode);
    $('#searchbox').hide();
}

$('#customergroupcode').keypress(function (e) {
    if (e.which == 13) {
        $('#searchbox').hide();
    } else {
        $('#searchbox').show();
    }
});



//--------- Add Address function start :----------//

function addaddress(e) {

    var chk          = $(e).attr('data-editAddress');

    var data         = checker('modalAddress');

    var multiAddress = data.multiAddress;

    var multiCity    = data.multiCity;

    var valid = true;

    if (checker('modalAddress') != false) {

        valid = valid * true;

    } else {

        valid = valid * false;

    }

    $('#address-table .multiAddress').each(function () {

        var multiAddress1 = $(this).text().trim();

        if (multiAddress1 == multiAddress && chk == '') {

            valid = valid * false;

            alert('Dublicate Address');
        }

    });



    if (valid) {

        var chk = $(e).attr('data-editAddress');

        if (chk == '') {

            var len = $('#address-table .srno').length + 1;

            fun_adder_address(len, multiAddress,multiCity);

        } else {

            fun_adder_address_edit(chk, multiAddress,multiCity);

        }

        modals.clear('modalAddress');

        $('#btn-address').attr('data-editAddress', '');

        $('#myModalAddress').modal('hide');


    }

}


function fun_adder_address(len, multiAddress,multiCity) {

    var str = '<tr>';
        
        str += '<td align="center" class="srno">' + len + '</td>';
        
        str += '<td align="center" class="multiAddress">' + multiAddress + '</td>';

        str += '<td align="center" class="multiCity">' + multiCity + '</td>';
        
        str += '<td align="center"><button class="btn btn-sm btn-success" data-toggle="modal" data-target="#myModalAddress" onclick="editoraddrss(' + len + ',this) ">E</button>  <button class="btn btn-sm btn-danger remover" onclick="removeraddress(this)">R</buuton></td>';
    
    str += '</tr>';
    
    $('#address-table > tbody').append(str);

}

function fun_adder_address_edit(len, multiAddress,multiCity) {

    var str = '<td align="center" class="srno">' + len + '</td>';
        
        str += '<td align="center" class="multiAddress">' + multiAddress + '</td>';
        
        str += '<td align="center" class="multiCity">' + multiCity + '</td>';

        str += '<td align="center"><button class="btn btn-sm btn-success" data-toggle="modal" data-target="#myModalAddress" onclick="editoraddrss(' + len + ',this) ">E</button>  <button class="btn btn-sm btn-danger remover" onclick="removeraddress(this)">R</buuton></td>';

    $('#address-table .srno').each(function () {
        
        var srno = $(this).text().trim();
        
        if (srno == len) {
        
            $(this).parent().html(str);
        
        }
    
    });

}

// -----------------------Multi Address Function Start----------------------//


function editoraddrss(srno, e) {

    $('#multiAddress').val($(e).parent().parent().find('.multiAddress').text().trim());

    $('#multiCity').val($(e).parent().parent().find('.multiCity').text().trim());

    $('#btn-address').attr('data-editAddress', srno);

}

// -----------------------Multi Address Function End----------------------//

// -----------------------Multi Address Removal Function Start----------------------//

function removeraddress(e) {

    
    $(e).parent().parent().fadeOut(1000, function () {
    
        $(this).remove();
    
    });

}

// -----------------------Multi Address Removal Function End----------------------//



// -----------------------Edit Function Start----------------------//

function edit(e) {
    
    $('#btn-submit').attr('onclick', 'update()');
    
    $('#btn-submit').text('Update');
    
    $('#contact-table > tbody > tr').remove();
    
    $('#cha-table > tbody > tr').remove();

    $('#address-table > tbody > tr').remove();

    var customerid = $(e).data('customerid');
    
    $('#customerid').val(customerid);
    
    $.ajax({
    
        type: "POST",
    
        data: 'customerid=' + customerid,
    
        url: 'select1.php',
    
        cache: false,
    
        success: function (res) {
    
            if (res.status == 'success') {
    
                closeNav1();

                modals.putvalue('section', res.customerjson);
    
                var contact = res.contactjson;

                var count = 1;
    
                for (var i in contact) {

                    contact_adder(count, contact[i].name,contact[i].department,contact[i].category,contact[i].mobile,contact[i].landline,contact[i].email);

                    count++;

                    $('.moveButton').removeClass('hidden');

                }

                var chajson = res.chajson;
                
                if (Object.keys(chajson).length != 0) {
                
                    var couter = 1;
                
                    for (var i in chajson) {

                        fun_adder(couter, chajson[i].cha);

                        couter++;

                    }

                } else {

                    $('#cha-table').find('tbody > tr').remove();

                }


                var addresss = res.addressjson;

                var serialNo = 1;
    
                for (var i in addresss) {

                    fun_adder_address(serialNo, addresss[i].multiAddress, addresss[i].multiCity);

                    serialNo++;

                }


            }

        }

    });

}

// -----------------------Edit Function End----------------------//


// -----------------------Update Function Start----------------------//

function update() {
    
    var valid = true;
    
    if (checker('section') != false) {
    
        valid = valid * true;
    
    } else {
    
        valid = valid * false;
    
    }
    
    if (modals.tabletdbyclass('contact-table') != false) {
    
        valid = valid * true;

        $('#contact-table').css('border','2px solid green');
    
    } else {
    
        valid = valid * false;

        $('#contact-table').css('border','2px solid red');
    
    }

    if (modals.tabletdbyclass('cha-table') != false) {

        valid = valid * true;

        $('#cha-table').css('border','2px solid green');

    } else {

        valid = valid * false;

        $('#cha-table').css('border','2px solid red');

    }

    if (modals.tabletdbyclass('address-table') != false) {

        valid = valid * true;

        $('#address-table').css('border','2px solid green');


    } else {

        $('#address-table').css('border','2px solid red');

        valid = valid * false;

    }

    if (valid) {

        var data     = checker('section');

        var datastr  = JSON.stringify(data);

        var datastr2 = JSON.stringify(modals.tabletdbyclass('contact-table'));

        var datastr3 = JSON.stringify(modals.tabletdbyclass('cha-table'));

        var datastr4 = JSON.stringify(modals.tabletdbyclass('address-table'));

        $.ajax({
            
            type: "POST",
            
            data: {
            
                data: datastr,
            
                data2: datastr2,
            
                data3: datastr3,

                data4: datastr4,
            
                customerid: $('#customerid').val()
            
            },
            
            url: 'update1.php',
            
            cache: false,
            
            success: function (res) {
            
                if (res.status == 'success') {
            
                    swal({
            
                        type: 'success',
            
                        title: 'Customer Master Updated',
            
                        showConfirmButton: false,
            
                        timer: 3000
            
                    });
            
                    setTimeout(function () {
            
                        window.location.reload();
            
                    }, 3000);
            
                }
            
                if (res.status == 'falid11') {
            
                    swal('GST Number Invalid', '', 'error');
            
                }
            
                if (res.status == 'falid1' || res.status == 'falid1') {
            
                    swal('Something Went Wrong', '', 'error');
            
                }
            
            }
        
        });
    
    }

}

// -----------------------Update Function End----------------------//


// -----------------------Reset Function Start----------------------//

$('#btn-reset').on('click', function () {
   
    window.location.reload();

});

// -----------------------Reset Function End----------------------//

// -----------------------Submit Function Start----------------------//

function submit() {

    var valid = true;

    if (checker('section') != false) {

        valid = valid * true;

    } else {

        valid = valid * false;

    }

   if (modals.tabletdbyclass('contact-table') != false) {
    
        valid = valid * true;

        $('#contact-table').css('border','2px solid green');
    
    } else {
    
        valid = valid * false;

        $('#contact-table').css('border','2px solid red');
    
    }

    if (modals.tabletdbyclass('cha-table') != false) {

        valid = valid * true;

        $('#cha-table').css('border','2px solid green');

    } else {

        valid = valid * false;

        $('#cha-table').css('border','2px solid red');

    }

    if (modals.tabletdbyclass('address-table') != false) {

        valid = valid * true;

        $('#address-table').css('border','2px solid green');

    } else {

        $('#address-table').css('border','2px solid red');

        valid = valid * false;

    }

    if (valid) {

        var data     = checker('section');

        var datastr  = JSON.stringify(data);

        var datastr2 = JSON.stringify(modals.tabletdbyclass('contact-table'));

        var datastr3 = JSON.stringify(modals.tabletdbyclass('cha-table'));

        var datastr4 = JSON.stringify(modals.tabletdbyclass('address-table'));

        $.ajax({
           
            type: "POST",
           
            data: {
           
                data: datastr,
           
                data2: datastr2,
           
                data3: datastr3,

                data4: datastr4,
           
                enquiryid: $('#enquiryid').val()
           
            },
           
            url: 'insert1.php',
           
            cache: false,
           
            success: function (res) {
           
                if (res.status == 'success') {

                    if ($('#enquiryid').val() != '') {

                        window.location.href = "/process/enquiry/index.php";

                    } else {

                        swal({

                            type: 'success',

                            title: 'Customer Master Created',

                            showConfirmButton: false,

                            timer: 3000

                        });

                        setTimeout(function () {

                            window.location.reload();

                        }, 3000);

                    }

                }

                if (res.status == 'falid11') {

                    swal('GST Number Invalid', '', 'error');

                }

                if (res.status == 'falid22') {

                    swal('GST Number Already Exist', '', 'error');

                }

                if (res.status == 'falid1' || res.status == 'falid1') {

                    swal('Something Went Wrong', '', 'error');

                }

            }

        });

    }

}

// -----------------------Submit Function End----------------------//


if ("<?php echo isset($_GET['enquiryid']) != '' ?>" &&  "<?php echo isset($_GET['customername'])  != '' ?>" && "<?php echo isset($_GET['customeraddress']) != '' ?>" && "<?php echo isset($_GET['emailid']) != '' ?>" && "<?php echo isset($_GET['state']) != '' ?>" && "<?php echo isset($_GET['category']) != '' ?>" && "<?php echo isset($_GET['contactno']) != '' ?>") {
    
    $('#customername').val("<?php echo $_GET['customername']?>");

    $('#address').val("<?php echo $_GET['customeraddress']?>");

    $('#emailId').val("<?php echo $_GET['emailid']?>");

    $('#custCategory').val("<?php echo $_GET['category']?>");

    $('#state').val("<?php echo $_GET['state']?>").trigger('change');

    $('#phone').val("<?php echo $_GET['contactno']?>");

}

// -----------------------Select2 Function Start----------------------//

$(document).ready(function () {
	
    $('.select-js1').select2({
	
    	width: '100%'
	
    });
	
    $('.select').attr('style', 'width:100%!important;');

});

// -----------------------Select2 Function Start----------------------//

// -----------------------Move Contact Function Start----------------------//

$('#btn-move').on('click', function () {

    let srNo  = $(this).attr('data-move');

    let valid = true;

    if (checker('contactModal') != false) {

        valid = valid * true;

    } else {

        valid = valid * false;

    }

    if (valid) {

        var data     = checker('contactModal');

        var datastr  = JSON.stringify(data);

        $.ajax({
       
            type: "POST",
  
            data: {
  
                srNo: srNo,
  
                data2: datastr,
  
                branchid: $('#branchid').val(),
  
                customerid: $('#customerid').val()
  
            },

            url: 'movecontactperson.php',
       
            cache: false,
       
            success: function (res) {

                if (res.status == 'success') {

                    swal({
       
                        type: 'success',
       
                        title: 'Contact Person Move',
       
                        showConfirmButton: false,
       
                        timer: 3000
       
                    });
       
                    setTimeout(function () {
       
                        window.location.reload();
       
                    }, 3000);

                } else {


                }
           
            }

        });

    }

});

// -----------------------Move Contact Function End----------------------//


</script>