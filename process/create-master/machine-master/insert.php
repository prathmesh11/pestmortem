<?php $base='../../../';
include($base.'_in/connect.php');
header('content-type: application/json; charset=utf-8');
header("access-control-allow-origin: *");
if(isset($_POST['data'])) {
    if (session_status()==PHP_SESSION_NONE) {
        session_start();
    }
    $created_by=$_SESSION['employeeid'];
    $con=_connect();
    $data=get_object_vars(json_decode($_POST["data"]));
    $branchid=_clean($con, $data['branchid']);
    $machinename=_clean($con, strtoupper($data['machinename']));
    $make=_clean($con, $data['make']);
    $asset=_clean($con, $data['asset']);
    $capacity=_clean($con, $data['capacity']);
    $remark=_clean($con, $data['remark']);
    
    $machineid=mysqli_fetch_assoc(mysqli_query($con, "SELECT max(id) x FROM machinemaster"))['x'];
    if($machineid) { $machineid++; } else {  $machineid=1; }   
    $results=mysqli_fetch_assoc(mysqli_query($con, "SELECT machinename x FROM machinemaster WHERE machinename='$machinename'"))['x'];
    if( !$results) {
        $create=mysqli_query($con, "INSERT INTO machinemaster (branchid,machineid,asset,machinename,make,capacity,remark,created_by,created_time) VALUES ('$branchid','$machineid','$asset','$machinename','$make','$capacity','$remark','$created_by','$CURRENT_MILLIS')");
        if($create) {
            echo '{"status":"success"}';
        }
        else {
            echo '{"status":"falid1"}';
        }
    }
    else {
        echo '{"status":"falid2"}';
    }
    _close($con);
    echo trim(ob_get_clean());
}
else {
    echo '{"status":"falid3"}';
}
?>