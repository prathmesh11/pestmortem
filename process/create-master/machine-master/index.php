<?php

    $base = '../../../';
    
    $js   = '<script src="'.$base.'js/fwork.js"></script>
             <script src="'.$base.'js/alart.js"></script>';
    $css  = '<link rel="stylesheet" href="'.$base.'css/fwork.css">
             <link rel="stylesheet" href="'.$base.'css/grid.min.css">
             <link href="'.$base.'/css/select2.min.css" rel="stylesheet" />
             <link rel="stylesheet" href="'.$base.'css/alart.css">';
    
    include($base.'_in/header.php');
    
    include($base.'_in/connect.php');

    if (!in_array("Machine/Assets Master", $arrysession)) {
    
        echo "<script>window.location.href='/process/dashboard.php';</script>";
    
        exit;

    }

    $con = _connect();

?>

<style>

    hr.style-hr {
        border: 0;
        height: 1px;
        background-image: linear-gradient(to right, rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.75), rgba(0, 0, 0, 0));
    }

    .table-list td,
    .table-list th {
        border: 1px solid #ddd;
        padding: 1px !important;
        font-size: 13px;
    }

    .table-list td {
        padding-top: 10px !important;

    }

    .table-list tr:nth-child(even) {
        background-color: #f2f2f2;
    }

    .table-list th {
        padding-top: 5px;
        padding-bottom: 5px;
        text-align: center;
        background-color: #16a085;
        color: white;
    }


    .sidenav1 {
        height: 100%;
        width: 0;
        position: fixed;
        z-index: 1;
        top: 0;
        left: 0;
        background-color: #111;
        overflow-x: hidden;
        transition: 0.5s;
        padding-top: 60px;
    }

    .sidenav1 a {
        padding: 8px 8px 8px 32px;
        text-decoration: none;
        font-size: 25px;
        color: #818181;
        display: block;
        transition: 0.3s;
    }

    .sidenav1 a:hover {
        color: #f1f1f1;
    }

    .sidenav1 .closebtn1 {
        position: absolute;

        right: 25px;
        font-size: 36px;
        margin-left: 50px;
    }

    @media screen and (max-height: 450px) {
        .sidenav1 {
            padding-top: 15px;
        }

        .sidenav1 a {
            font-size: 18px;
        }
    }

    .list2 {
        padding: 0px;
    }

    .list2 li span {
        display: inline;
    }

    .list2 li {
        text-decoration: none;
        display: block;
        padding: 2px;
        background: #FFF;
        border: 1px solid #333;
        color: #000;
    }

    .list1 li:hover {
        background: #0984e3;
        color: #fff;
    }
    
</style>

<div class="container-fluid">
    
    <div class=" content">

    <span style="font-size:24px;cursor:pointer" onclick="openNav1()">&#9776; Search</span>

    <div id="mysidenav1" class="sidenav1" style="margin-left:80px;">

        <div id="test-list" style="padding:5px;">

            <a href="javascript:void(0)" class="closebtn1" onclick="closeNav1()">&times;</a>

            <br><br><br>

            <div class="form-group">

                <input type="text" placeholder="Search" class="form-control input-sm fuzzy-search" style="border-radius:3px;">

            </div>

            <ul class="list list2">

                <?php 

                    $result1=mysqli_query($con, "SELECT * FROM machinemaster ORDER BY branchid ASC");

                    while($rows=mysqli_fetch_assoc($result1)) {

                        echo '<li><span class="machinemaster">' .$rows['machinename']. '<span><button style="margin-left:2px;" data-machineid="' .$rows['machineid']. '" onclick="edit(this)" class="btn btn-primary btn-sm"><span class="fa fa-edit"></span> E</button></i>';
                    
                    }

                ?>

            </ul>

        </div>

    </div>
    
        <h2 align="center">Create Machine/Equipment Master</h2>
    
        <hr class="style-hr">
    
        <div class="col-sm-3"></div>
    
        <div class="col-sm-6" id="section">
            
            <div class="col-sm-4">Branch</div>
            
            <div class="col-sm-8">
                
                <div class="form-group">
                    
                    <select class="form-control input-sm select-js" data-role="select" id="branchid" data-name="branchid">
                        
                        <option value="Select">Select</option>
                        
                        <?php 
                            
                            $result1 = mysqli_query($con, "SELECT * FROM `branchmaster`");
                                
                            while($rows=mysqli_fetch_assoc($result1)) {
                                
                                echo '<option value="'.$rows['branchid'].'">'.$rows['branchname'].'</option>';
                            
                            }
                        
                        ?>
                    
                    </select>
                
                </div>
            
            </div>
            
            <div class="col-sm-4">Asset</div>
            
            <div class="col-sm-8">
                
                <div class="form-group">
                    
                    <select class="form-control input-sm" data-role="select" id="asset" data-name="asset">
                        
                        <option value="Select">Select</option>
                        
                        <option value="Machine">Machine</option>
                        
                        <option value="Equipment">Equipment</option>
                    
                    </select>
                
                </div>
            
            </div>
            
            <div class="col-sm-4">Machine/Asset Name</div>
            
            <div class="col-sm-8">
                
                <div class="form-group">
                    
                    <input type="text" data-role='text' id="machinename" data-name='machinename' class="form-control input-sm">
                    
                    <input type="text" style="display:none;" id="ids" data-name='ids' class="form-control input-sm">
                
                </div>
            
            </div>
            
            <div class="col-sm-4">Make</div>
            
            <div class="col-sm-8">
            
                <div class="form-group">
                    
                    <input type="text" id="make" data-role='text' data-name='make' class="form-control input-sm">
                
                </div>
            
            </div>
            
            <div class="col-sm-4">Capacity</div>
            
            <div class="col-sm-8">
                
                <div class="form-group">
                
                    <input type="text" id="capacity" data-role='text' data-name='capacity' class="form-control input-sm">
                
                </div>
            
            </div>

            <div class="col-sm-4">Select Employee</div>
            
            <div class="col-sm-8">
                
                <div class="form-group">
                    
                    <select class="form-control input-sm select-js" data-role="select" id="employeeAllotment" data-name="employee_allotment">
                        
                        <option value="Select">Select</option>
                        
                        <?php 
                            
                            $result1 = mysqli_query($con,"SELECT employeeid,username FROM employeemaster WHERE branchid='$branchid' AND role = 'employee' OR role = 'None' ");

                            while($rows = mysqli_fetch_assoc($result1)){
                       
                              echo '<option value="'.$rows['employeeid'].'">'.$rows['username'].'</option>';
      
                            }
                        
                        ?>
                    
                    </select>
                
                </div>
            
            </div>

            <div class="col-sm-4">Next Shedule Date</div>
            
            <div class="col-sm-8">
                
                <div class="form-group">
                    
                    <input type="date" data-role='date' id="nextServiceDate" data-name='next_service_date' value="<?php echo $today;?>" class="form-control input-sm">
                
                </div>
            
            </div>

            <div class="col-sm-4">Remark</div>
            
            <div class="col-sm-8">
                
                <div class="form-group">
                    
                    <input type="text" data-role='text' id="remark" data-name='remark' class="form-control input-sm">
                
                </div>
            
            </div>
        
        </div>

        
        <div style="clear:both;"></div>
        
        <hr class="style-hr">
        
        <div class="col-sm-3"></div>
        
        <div class="col-sm-3">
            
            <button class="btn btn-success btn-sm btn-block" id="btn-submit" onclick="insert()" style="margin:5px;">Submit</button>
            
        </div>
        
        <div class="col-sm-3">
            
            <button class="btn btn-danger btn-sm btn-block" id="btn-reset" style="margin:5px;">Reset</button>
        
        </div>
    
    </div>
    
    <div class="col-sm-3"></div>
    
    <div style="clear:both;"></div>
    
    <hr class="style-hr">
    
    <div class="col-sm-1"></div>
    
    <div class="col-sm-11">

        <table class="table-list table table-bordered">
            
            <thead>
                
                <tr>
                
                    <th>SR. No</th>
                
                    <th>Branch Name</th>
                
                    <th>Asset</th>
                
                    <th>Name</th>
                
                    <th>Make</th>
                
                    <th>Capacity</th>
                
                    <th>Remark</th>
                
                    <th>Action</th>
                
                </tr>
            
            </thead>
            
            <tbody>
                
                <?php
                    
                    $count   = 0;
                    
                    $result4 = mysqli_query($con,"SELECT machineid,branchid,machinename,make,capacity,remark,asset FROM machinemaster ORDER BY branchid ASC" );
                     
                    while($rows3 = mysqli_fetch_assoc($result4)){
                         
                        $count++;
                        
                        $branchid   = $rows3['branchid'];

                        $id         = $rows3['machineid'];
                        
                        $branchname = mysqli_fetch_assoc(mysqli_query($con,"SELECT branchname x FROM branchmaster WHERE branchid='$branchid' ORDER BY branchid ASC"))['x'];

                        echo '<tr>';
                            
                            echo '<td align="center">'.$count.'</td>';
                             
                            echo '<td align="center">'.$branchname.'</td>';
                            
                            echo '<td align="center">'.$rows3['asset'].'</td>';
                            
                            echo '<td align="center">'.$rows3['machinename'].'</td>';
                            
                            echo '<td align="center">'.$rows3['make'].'</td>';
                            
                            echo '<td align="center">'.$rows3['capacity'].'</td>';
                            
                            echo '<td align="center">'.$rows3['remark'].'</td>';
                     
                            echo '<td align="center"><button  onclick="removeitem('.$id.')" class="btn btn-danger">R</button> <button data-zero="'.$rows3['machineid'].'" data-one="'.$rows3['machinename'].'"  data-two="'.$rows3['branchid'].'"   data-three="'.$rows3['make'].'"   data-four="'.$rows3['capacity'].'" data-five="'.$rows3['remark'].'"  data-six="'.$rows3['asset'].'" onclick="endit(this)" class="btn btn-primary">E</button></td>';
                        
                        echo '<tr>';
                    }
                
                ?>
            
            </tbody>
        
        </table>
    
    </div>

</div>

<?php
    
    include($base.'_in/footer.php');

?>

<script>

    function openNav1() {
        
        document.getElementById("mysidenav1").style.width = "300px";
    
    }

    function closeNav1() {
    
        document.getElementById("mysidenav1").style.width = "0";
    
    }
    
    var monkeyList = new List('test-list', {
    
        valueNames: ['machinemaster']
    
    });

    $('#btn-reset').on('click', function () {
        
        window.location.reload();
    
    });

    $(document).ready(function () {

        $('.select-js').select2({width: '100%'});

        $('.select').attr('style','width:100%!important;');

    });

    function insert() {
        var valid = true;
        if (checker('section') != false) {
            valid = valid * true;
        } else {
            valid = valid * false;
        }

        if (valid) {
            data = checker('section');

            var datastr = JSON.stringify(data);

            $.ajax({
                type: "POST",
                data: {
                    data: datastr
                },
                url: 'insert.php',
                cache: false,
                success: function (res) {
                    console.log(res);
                    if (res.status == 'success') {
                        swal({
                            type: 'success',
                            title: 'Machine Master Created',
                            showConfirmButton: false,
                            timer: 3000
                        });
                        setTimeout(function () {
                            window.location.reload();
                        }, 3000);
                    }
                    if (res.status == 'falid2') {
                        swal('Machine Master already Exist', '', 'error');
                        $('#inp-phone').val('');
                        $('#inp-phone').css('border-bottom', '1px solid red');
                    }
                }
            });
        }
    }

    function update() {
        var valid = true;
        if (checker('section') != false) {
            valid = valid * true;
        } else {
            valid = valid * false;
        }

        if (valid) {
            data = checker('section');

            var datastr = JSON.stringify(data);

            $.ajax({
                type: "POST",
                data: {
                    data: datastr
                },
                url: 'update.php',
                cache: false,
                success: function (res) {
                    console.log(res);
                    if (res.status == 'success') {
                        swal({
                            type: 'success',
                            title: 'Machine Master updated',
                            showConfirmButton: false,
                            timer: 3000
                        });
                        setTimeout(function () {
                            window.location.reload();
                        }, 3000);
                    }

                }
            });
        }
    }

    function removeitem(e) {

        swal({
            title: 'Are you sure?',
            text: 'You will not be able to recover this Machine!',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'No, keep it'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: "POST",
                    data: 'id=' + e,
                    url: 'remove.php',
                    cache: false,
                    success: function (res) {
                        if (res.status == 'success') {
                            swal(
                                'Deleted!',
                                'Your Machine has been deleted.',
                                'success'
                            )
                            setTimeout(function () {
                                window.location = window.location.href.split("?")[0];
                            }, 3000);
                        }
                    }
                })
            } else if (result.dismiss === swal.DismissReason.cancel) {
                swal(
                    'Cancelled',
                    'Your Machine is safe :)',
                    'error'
                )
            }
        })

    }


    function edit(e) {
    closeNav1();
    $('#btn-submit').attr('onclick', 'update()');
    $('#btn-submit').text('Update');
    $('#contact-table > tbody > tr').remove();
    var customerid = $(e).data('customerid');
    $('#customerid').val(customerid);
    $.ajax({
        type: "POST",
        data: 'customerid=' + customerid,
        url: 'select.php',
        cache: false,
        success: function (res) {
            if (res.status == 'success') {
                modals.putvalue('section', res.customerjson);
                var contact = res.contactjson;

                var count = 1;
                for (var i in contact) {

                    contact_adder(count, contact[i].name,contact[i].department,contact[i].category,contact[i].mobile,contact[i].landline,contact[i].email);

                    count++;

                }

                var chajson = res.chajson;
                if (Object.keys(chajson).length != 0) {
                    var couter = 1;
                    for (var i in chajson) {

                        fun_adder(couter, chajson[i].cha);

                        couter++;

                    }
                } else {
                    console.log('1');

                    $('#cha-table').find('tbody > tr').remove();

                }



            }
        }
    });
}


    function endit(e) {
        $('#ids').val($(e).data('zero'));
        $('#machinename').val($(e).data('one'));
        $('#branchid').val($(e).data('two'));
        $('#make').val($(e).data('three'));
        $('#capacity').val($(e).data('four'));
        $('#remark').val($(e).data('five'));
        $('#asset').val($(e).data('six'));
        $('#btn-submit').attr('onclick', 'update()');
        $('#btn-submit').text('Update');
    }

</script>