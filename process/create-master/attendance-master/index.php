<?php
$base='../../../';
 
$js=' <script src="'.$base.'js/fwork.js"></script>
<script src="'.$base.'js/alart.js"></script>
 <script src="'.$base.'js/pdfmake.min.js"></script>
 <script src="'.$base.'js/vfs_fonts.js"></script>';
$css='<link rel="stylesheet" href="'.$base.'css/fwork.css">
<link rel="stylesheet" href="'.$base.'css/alart.css">
<link rel="stylesheet" href="'.$base.'css/grid.min.css">';


include($base.'_in/header.php');
include($base.'_in/connect.php');

if (!in_array("Monthly Attendance", $arrysession)) {
   
    echo "<script>window.location.href='/process/dashboard.php';</script>";
   
    exit;

}

$con=_connect();

$json1='';
$result=mysqli_query($con,"SELECT * FROM employeemaster ORDER BY username ASC");
while($rows = mysqli_fetch_assoc($result)){
    $empid=$rows['employeeid'];
    $brid=$rows['branchid'];
    $officialjson=get_object_vars(json_decode(mysqli_fetch_assoc(mysqli_query($con,"SELECT officialjson x FROM `salarymaster` WHERE employeeid='$empid' AND branchid='$brid'"))['x']));
    $json1.=',{"branchid":"'.$brid.'","employeeid":"'.$empid.'","username":"'.$rows['username'].'","weeklyoff":"'.$officialjson['weeklyoff'].'","workinghours":"'.$officialjson['workinghours'].'"}';
}
$json1=substr($json1,1);
$json1='['.$json1.']';

$json2='';
$result=mysqli_query($con,"SELECT holidaydate FROM paidholidays");
while($rows = mysqli_fetch_assoc($result)){
    $holidaydate=$rows['holidaydate'];
    $holidaydate=date("Y-m-d",($holidaydate/1000));
    $json2.=',{"holidaydate":"'.$holidaydate.'"}';
}
$json2=substr($json2,1);
$json2='['.$json2.']';

$json3='';
$result=mysqli_query($con,"SELECT fromdate,todate,leavetype,branchid,employeeid FROM leaveallotmentmaster");
while($rows = mysqli_fetch_assoc($result)){
    $json3.=',{"fromdate":"'.date("Y-m-d",($rows['fromdate']/1000)).'","todate":"'.date("Y-m-d",($rows['todate']/1000)).'","leavetype":"'.$rows['leavetype'].'","branchid":"'.$rows['branchid'].'","employeeid":"'.$rows['employeeid'].'"}';
}
$json3=substr($json3,1);
$json3='['.$json3.']';
?>

<style>
hr.style-hr {
    border: 0;
    height: 1px;
    background-image: linear-gradient(to right, rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.75), rgba(0, 0, 0, 0));
}
.table-list td,
   .table-list th {
       border: 1px solid #ddd;
       padding: 1px !important;
       font-size: 13px;
   }
   .table-list td{
	padding-top: 2px !important;
    margin:0;

   }

   .table-list tr:nth-child(even) {
       background-color: #f2f2f2;
   }

   .table-list th {
       padding-top: 5px;
       padding-bottom: 5px;
       text-align: center;
       background-color: #16a085;
       color: white;
   }
   body{
       background:#eee;
   }
   .shadow {
  -webkit-box-shadow: 3px 3px 5px 6px #ccc; -moz-box-shadow: 3px 3px 5px 6px #ccc; box-shadow: 3px 3px 5px 6px #ccc; 
}
</style>
<input type="hidden" id="item-json1" value='<?php echo str_replace("'"," &#39;",$json1); ?>' />
<input type="hidden" id="item-json2" value='<?php echo str_replace("'"," &#39;",$json2); ?>' />
<input type="hidden" id="item-json3" value='<?php echo str_replace("'"," &#39;",$json3); ?>' />
<input type="hidden" id="item-weekoff" />
<div class="container-fluid" id="section">
    <div class=" content">

        <h2 align="center"  class="shadow" style="background:#FFF;padding:10px;">Attendance Master</h2>
        <hr class="style-hr">
        <div class="col-sm-12 shadow" style="background:#FFF;padding:10px;">
            <div class="col-sm-1" align="right">Month</div>
            <div class="col-sm-2"><input type="month" class="form-control input-sm" onchange="ui(this)" id="inp-month" value="<?php echo date("Y-m"); ?>"></div>
            <div class="col-sm-1" align="right">Branch</div>
            <div class="col-sm-2">
            <select class="form-control input-sm" data-name="branchid" id="branchid" name="branchid" onchange="empmaster(this);" >
                        <option value="Select">Select</option>
                            <?php
                                    $result1=mysqli_query($con, "SELECT * FROM `branchmaster`");
                                    while($rows=mysqli_fetch_assoc($result1)) {
                                        echo '<option value="'.$rows['branchid'].'">'.$rows['branchname'].'</option>';    
                                }
                            ?>
                            </select>
            </div>
            <div class="col-sm-1" align="right">Employee</div>
            <div class="col-sm-2">
                <select class="form-control input-sm" data-name="employeeid" id="employeeid" onchange="empdetails(this)" >
                        <option value="Select">Select</option>
                </select>
            </div>
            <div class="col-sm-1"><button class="btn btn-sm btn-success btn-block" onclick="insert()" id="btn-submit">Submit</button></div>
            <div class="col-sm-1"><button class="btn btn-sm btn-primary btn-block" onclick="printreportforsingleemp(this)">Print</button></div>
            <!--<div class="col-sm-1"><button class="btn btn-sm btn-primary btn-block" onclick="printreportforallemp(this)">Print All</button></div> -->
            <div class="col-sm-1"><button class="btn btn-sm btn-danger btn-block">Reset</button></div>
        </div>
        <div style="clear:both;"></div>
        <br>
        <div class="col-sm-6 shadow" style="background:#FFF;padding:10px;">
        <div class="col-sm-2">In Time</div>
        <div class="col-sm-3"><input type="time" id="default-in" class="form-control input-sm"></div>
        <div class="col-sm-2">Out Time</div>
        <div class="col-sm-3"><input type="time" id="default-out" class="form-control input-sm"></div>
        <div class="col-sm-2"><button onclick="defailttimeset()" class="btn btn-block btn-sm btn-primary">Apply To All</button></div>
        </div>
        <div class="col-sm-1">
        </div>
        <div class="col-sm-5 shadow" style="background:#FFF;padding:10px;">
        <button class="btn btn-sm btn-primary"  data-toggle="modal" data-target="#myModal">Holiday List</button>
        </div>
        <div style="clear:both;"></div>
        <br>
        <div class="col-sm-6 shadow" style="background:#FFF;padding:10px;">
        <table class="table table-list" id="table1">
        <thead>
        <th>Days</th>
        <th>WeekDays</th>
        <th>In Time</th>
        <th>Out Time</th>
        <th>Total_Hrs</th>
        <th>Working_Hrs</th>
        <th>Over_Time</th>
        <th>Remark</th>
        </thead>
        <tbody>
        </tbody>
        </table>
        </div>
        <div class="col-sm-1" style="padding:0;margin:0;"></div> 
        <input type="hidden" name="htable2" id="htable2">
        <div class="col-sm-5 shadow" style="background:#FFF;margin:0;padding:10px;" id="table2">
        <div class="col-sm-5">Total Days In Month</div><div class="col-sm-7"><div class="form-group"><input type="text" data-role="number" data-name="daysinmonth" id="daysinmonth" class="form-control input-sm"  value="0" readonly></div></div>
        <div class="col-sm-5" style="background:red;color:#fff;">Total Holidays In Months</div><div class="col-sm-7"><div class="form-group"><input type="text" data-role="number" data-name="holidayinmonth" id="holidayinmonth" class="form-control input-sm"  value="0" readonly></div></div>
        <div class="col-sm-5" style="background:yellow;">Total Weekoffs In Months</div><div class="col-sm-7"><div class="form-group"><input type="text" data-role="number" data-name="weekoffinmonth" id="weekoffinmonth" class="form-control input-sm"  value="0" readonly></div></div>
        <div class="col-sm-5">Total Working Days In Months</div><div class="col-sm-7"><div class="form-group"><input type="text" data-role="number" data-name="workingdayinmonth" id="workingdayinmonth" class="form-control input-sm"  value="0" readonly></div></div>
        <div style="clear:both;"></div>
        <hr class="style-hr">
        <div class="col-sm-5">Total Paid Leaves</div><div class="col-sm-7"><div class="form-group"><input type="text" data-role="number" data-name="paidleave" id="paidleave" class="form-control input-sm"  value="0" readonly></div></div>
        <div class="col-sm-5">Total Casual Leave</div><div class="col-sm-7"><div class="form-group"><input type="text" data-role="number" data-name="casualleave" id="casualleave" class="form-control input-sm"  value="0" readonly></div></div>
        <div class="col-sm-5">Total Seek Leave</div><div class="col-sm-7"><div class="form-group"><input type="text" data-role="number" data-name="seekleave" id="seekleave" class="form-control input-sm"  value="0" readonly></div></div>
        <div class="col-sm-5">Total Comman Off</div><div class="col-sm-7"><div class="form-group"><input type="text" data-role="number" data-name="commanoff" id="commanoff" class="form-control input-sm"  value="0" readonly></div></div>
        <div class="col-sm-5">Total No calls</div><div class="col-sm-7"><div class="form-group"><input type="text" data-role="number" data-name="nocall" id="nocall" class="form-control input-sm"  value="0" readonly></div></div>
        <div style="clear:both;"></div>
        <hr class="style-hr">
        <div class="col-sm-5">Total Overtime Days</div><div class="col-sm-7"><div class="form-group"><input type="text" data-role="number" data-name="overtimeday" id="overtimeday" class="form-control input-sm"  value="0" readonly></div></div>
        <div class="col-sm-5">Total Overtime Hrs</div><div class="col-sm-7"><div class="form-group"><input type="text" data-role="number" data-name="overtimehrs" id="overtimehrs" class="form-control input-sm"  value="0" readonly></div></div>
        <div class="col-sm-5">Total Paid Weekoff Days</div><div class="col-sm-7"><div class="form-group"><input type="text" data-role="number" data-name="paidweekofdays" id="paidweekofdays" class="form-control input-sm"  value="0" readonly></div></div>
        <div class="col-sm-5">Total Paid Weekoff Hrs</div><div class="col-sm-7"><div class="form-group"><input type="text" data-role="number" data-name="paidweekofhrs" id="paidweekofhrs" class="form-control input-sm"  value="0" readonly></div></div>
        <div style="clear:both;"></div>
        <hr class="style-hr">
        <div class="col-sm-5">Total Late Days</div><div class="col-sm-7"><div class="form-group"><input type="text" data-role="number" data-name="lateday" id="lateday" class="form-control input-sm"  value="0" readonly></div></div>
        <div class="col-sm-5">Total Half Days</div><div class="col-sm-7"><div class="form-group"><input type="text" data-role="number" data-name="halfday" id="halfday" class="form-control input-sm"  value="0" readonly></div></div>
        <div class="col-sm-5">Total Late Cutting Days</div><div class="col-sm-7"><div class="form-group"><input type="text" data-role="number" data-name="latecutting" id="latecutting" class="form-control input-sm"  value="0"></div></div>
        <div style="clear:both;"></div>
        <hr class="style-hr">
        <div class="col-sm-5">Total Working Hrs in Months</div><div class="col-sm-7"><div class="form-group"><input type="text" data-role="number" data-name="workinghrsinmonths" id="workinghrsinmonths" class="form-control input-sm"  value="0" readonly></div></div>
         <div class="col-sm-5">Emp Working + overtime Hrs</div><div class="col-sm-7"><div class="form-group"><input type="text" data-role="number" data-name="empworkingovertimehrs" id="empworkingovertimehrs" class="form-control input-sm"  value="0" readonly></div></div>
         <div class="col-sm-5">Emp Working + overtime Days</div><div class="col-sm-7"><div class="form-group"><input type="text" data-role="number" data-name="empworkingovertimeday" id="empworkingovertimeday" class="form-control input-sm"  value="0" readonly></div></div>
      
        </div>
    </div>
     <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
      <div class="modal-dialog">

          <!-- Modal content-->
          <div class="modal-content">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title" align="center">Holiday List</h4>
              </div>
              <div class="modal-body">
                  <table class="table table-list">
                  <thead>
                    <th>SR No.</th>
                    <th>Date</th>
                    <th>Holiday</th>
                    </thead>
                    <tbody>
                    <?php
                    $count=1;
                    $result=mysqli_query($con,"SELECT holidaydate,reference FROM paidholidays ORDER BY holidaydate ASC");
                            while($rows = mysqli_fetch_assoc($result)){
                                $holidaydate=$rows['holidaydate'];
                                $reference=$rows['reference'];
                                $holidaydate=date("D, d-M-Y",($holidaydate/1000));
                                echo '<tr>';
                                echo '<td align="center">'.$count.'</td>';
                                echo '<td align="center">'.$holidaydate.'</td>';
                                echo '<td align="center">'.$reference.'</td>';
                                echo '</tr>';
                                $count++;
                            }
                    ?>
                    </tbody>
                  </table>
              </div>
              <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>
          </div>

      </div>
  </div>
    <!-- Modal -->
</div>
<?php
include($base.'_in/footer.php');
?>

<script>
 function printreport(e){
    var employeeid=$('#employeeid').val();
    var month=$('#inp-month').val();
    var branchid=$('#branchid').val();
    //get branch info

   var empname = $("#employeeid option:selected").text();
var jsondata = $.parseJSON($.ajax({
         type: "POST",
        data: {
          branchid: branchid
        },
        url: '../../crm/api/getbrjson.php',
        cache: false,
        success: function (res) {
          //alert(res.status);
        //jsondata =JSON.stringify(res.json);
         },
        dataType: "json", 
        async: false
    }).responseText);

jsondata =JSON.stringify(jsondata);
data4 = JSON.parse(jsondata); 

branchjson= data4.branchjson;  
branchname = data4.branchname;

   var content=[]; 
   var fullDate = new Date();
   var twoDigitMonth = ((fullDate.getMonth().length + 1) === 1) ? (fullDate.getMonth() + 1) : '' + (fullDate.getMonth() + 1);
   var currentDate = fullDate.getDate() + "/" + twoDigitMonth + "/" + fullDate.getFullYear();
   var table2=0;
    $.ajax({
      type: "POST",
      data: {
                    month:month,
                    branchid:branchid,
                    employeeid:employeeid,
        },
        url: 'select.php',
        cache: false,
        success: function (res) {
                    if(res.status=='success'){
                      table2 = res.table2[0];
                       
                       dd = {
               
                    pageSize: 'A4',
                    pageMargins: [50, 30, 30, 40],
                    footer: function (currentPage, pageCount) {
                        return {
                            margin: 10,
                            columns: [{
                                fontSize: 8,
                                text: [{
                                        text: '--------------------------------------------------------------------------' +
                                            '\n',
                                        margin: [0, 20]
                                    },
                                    {
                                        text: '©Pest Mortem (India) PVT. LTD. || Printed On ' +currentDate ,
                                    }
                                ],
                                alignment: 'center'
                            }]
                        };

                       },

                    content: [
                   
                            
                             {
                                text: 'PEST MORTEM (INDIA) PRIVATE LIMITED',
                                 fontSize: 14,
                                      bold: true,
                                      alignment: 'center',
                            },
                            {
                                    text: '(Estd. 1980)',
                                     fontSize: 10,
                                      bold: true,
                                      alignment: 'center',
                                },
                                {
                                    text: 'Regd. Office : '+branchjson.address+'\nTel : '+branchjson.phone+' Email : '+branchjson.email ,
                                     fontSize: 8,
                                      bold: true,
                                      alignment: 'center',
                                },
                                '\n',
                                {
                                    text: '    Name : '+empname+'  \nMonth : '+month+'  Branch :  '+branchname,
                                     fontSize: 10,
                                      bold: true,
                                      alignment: 'center',
                                },
                                '\n',
                               {
                                    text: ' Working Days : ',
                                     fontSize: 10,
                                      bold: true,
                                      
                                },
                                {  
                                       border : 0,
                                    //   bold: true,
                                       fontSize: 8,
                                       margin: [0, 15, ],
                                        table: {
                                                widths: [200, 200],
                                                height : 400,
                                                 body: [
                                                    ['Total Days In Month : ',table2.daysinmonth],
                                                    ['Total Holidays In Months : ',table2.holidayinmonth], 
                                                    ['Total Weekoffs In Months : ',table2.weekoffinmonth],
                                                    ['Total Working Days  : ',table2.workingdayinmonth],
                                                       ],
                                                     }, 

                                               },
                                       {
                                              text: ' Leaves : ',
                                               fontSize: 10,
                                                bold: true,
                                               
                                          },

                                       {  
                                       border : 0,
                                    //   bold: true,
                                       fontSize: 8,
                                       margin: [0, 15, ],
                                        table: {
                                                widths: [200, 200],
                                                height : 400,
                                                 body: [
                                                    ['Total Paid Leaves  : ',table2.paidleave],     
                                                    ['Total Casual Leaves  : ',table2.casualleave],
                                                    ['Total Seek Leaves  : ',table2.seekleave],
                                                    ['Total Comman Off  : ',table2.commanoff],
                                                    ['Total No Calls  : ',table2.nocall],
                                                  
                                                       ],


                                                                }, 

                                                      },
                                         {
                                            text: ' Over Time : ',
                                             fontSize: 10,
                                              bold: true,
                                              
                                        },

                                      {  
                                       border : 0,
                                    //   bold: true,
                                       fontSize: 8,
                                       margin: [0, 15, ],
                                        table: {
                                                widths: [200, 200],
                                                height : 400,
                                                 body: [
                                                    
                                                    ['Total Over Time Days  : ',table2.overtimeday],
                                                    ['Total Over Time Hrs  : ',table2.overtimehrs],

                                                    ['Total Paid Weekly off Days : ',table2.paidweekofdays],
                                                    ['Total Paid Weekly off Hrs : ',table2.paidweekofhrs],
                                                     ],


                                                                }, 

                                                      },
                                    {
                                        text: ' Late Days : ',
                                         fontSize: 10,
                                          bold: true,
                                          
                                    },
                                   {  
                                       border : 0,
                                    //   bold: true,
                                       fontSize: 8,
                                       margin: [0, 15, ],
                                        table: {
                                                widths: [200, 200],
                                                height : 400,
                                                 body: [
                                                    
                                                      ['Total Late Days  : ',table2.lateday],
                                                    ['Total Half Days : ',table2.halfday],
                                                    ['Total Late Cutting Days : ',table2.latecutting],
                                                     
                                                 
                                                       ],


                                                                }, 

                                                      },
                                     {
                                        text: ' Total Working Hours : ',
                                         fontSize: 10,
                                          bold: true,
                                          
                                    },
                                    {  
                                       border : 0,
                                    //   bold: true,
                                       fontSize: 8,
                                       margin: [0, 15, ],
                                        table: {
                                                widths: [200, 200],
                                                height : 400,
                                                 body: [
                                                    
                                                    ['Total Working Hrs In Month  : ',table2.workinghrsinmonths],
                                                    ['Emp Working + OverTime Hrs : ',table2.empworkingovertimeday],
                                                    ['Emp Working + OverTime Days  : ',table2.empworkingovertimehrs],
                                                    
                                                    
                                                    
                                                    
                                                    
                                                    
                                                       ],


                                                                }, 

                                                      }
                                                                     
                                ]
            }
        pdfMake.createPdf(dd).open();

                    }
                  }
                });    



//jsondata =JSON.stringify(jsondata);
//data4 = JSON.parse(jsondata); 
//tablejson= data4.table2[0];
//table2 =$('#htable2').val(); 
// console.log(table2);
    

 }
 function printreportforsingleemp(e){
    var employeeid=$('#employeeid').val();
    var month=$('#inp-month').val();
    var branchid=$('#branchid').val();
    //get branch info

  var empname = $("#employeeid option:selected").text();
  var jsondata = $.parseJSON($.ajax({
         type: "POST",
        data: {
          branchid: branchid
        },
        url: '../../crm/api/getbrjson.php',
        cache: false,
        success: function (res) {
          //alert(res.status);
        //jsondata =JSON.stringify(res.json);
         },
        dataType: "json", 
        async: false
    }).responseText);

jsondata =JSON.stringify(jsondata);
data4 = JSON.parse(jsondata); 

branchjson= data4.branchjson;  
branchname = data4.branchname;

   var content=[]; 
   var fullDate = new Date();
   var twoDigitMonth = ((fullDate.getMonth().length + 1) === 1) ? (fullDate.getMonth() + 1) : '' + (fullDate.getMonth() + 1);
   var currentDate = fullDate.getDate() + "/" + twoDigitMonth + "/" + fullDate.getFullYear();
   var table2=0;
    $.ajax({
      type: "POST",
      data: {
                    month:month,
                    branchid:branchid,
                    employeeid:employeeid,
        },
        url: 'select.php',
        cache: false,
        success: function (res) {
                    if(res.status=='success'){
                      table2 = res.table2[0];
                      table1 = res.table1[0]; 
        dd = {
               
                    pageSize: 'A4',
                    pageMargins: [50, 30, 30, 40],
                    footer: function (currentPage, pageCount) {
                        return {
                            margin: 10,
                            columns: [{
                                fontSize: 8,
                                text: [{
                                        text: '--------------------------------------------------------------------------' +
                                            '\n',
                                        margin: [0, 20]
                                    },
                                    {
                                        text: '©Pest Mortem (India) PVT. LTD. || Printed On ' +currentDate ,
                                    }
                                ],
                                alignment: 'center'
                            }]
                        };

                       },

                    content: [
                   
                            
                             {
                                text: 'PEST MORTEM (INDIA) PRIVATE LIMITED',
                                 fontSize: 14,
                                      bold: true,
                                      alignment: 'center',
                            },
                            {
                                    text: '(Estd. 1980)',
                                     fontSize: 10,
                                      bold: true,
                                      alignment: 'center',
                                },
                                {
                                    text: 'Regd. Office : '+branchjson.address+'\nTel : '+branchjson.phone+' Email : '+branchjson.email ,
                                     fontSize: 8,
                                      bold: true,
                                      alignment: 'center',
                                },
                                '\n',
                                {
                                    text: '    Name : '+empname+'  \nMonth : '+month+'  Branch :  '+branchname,
                                     fontSize: 10,
                                      bold: true,
                                      alignment: 'center',
                                },
                                '\n',
                               
                                 
                                  table(table1, ['day', 'weekday','intime','outtime','totalhrs','overtime','workinghours']),
                                   '\n',
                                       {
                                              text: ' Summary : ',
                                               fontSize: 10,
                                                bold: true,
                                               
                                          },
                                        {  
                                       border : 0,
                                    //   bold: true,
                                       fontSize: 8,
                                       margin: [0, 15, ],
                                        table: {
                                                widths: [80,10,100,10,100,10,80,10],
                                                height : 400,
                                                 body: [
                                                    ['Total Days In Month : ',table2.daysinmonth,
                                                    'Total Holidays In Months : ',table2.holidayinmonth, 
                                                    'Total Weekoffs In Months : ',table2.weekoffinmonth,
                                                    'Total Working Days  : ',table2.workingdayinmonth],
                                                       ],
                                                     }, 

                                               },
                                       
                                      
                                    
                                                                     
                                ]
            }
        pdfMake.createPdf(dd).open();

                    }
                  }
                });    

 

 }


function buildTableBody(data, columns) {
    var body = [];

    body.push(columns);

    data.forEach(function(row) {
        var dataRow = [];

        columns.forEach(function(column) {
            dataRow.push(row[column].toString());
        })

        body.push(dataRow);
    });

    return body;
}

function table(data, columns) {
    return {
      fontSize: 8,
      alignment : 'center',
      
      style: 'tableExample',
        table: {
            widths: [50,100,50,50,50,50,50],
            headerRows: 1,

            body: buildTableBody(data, columns)
        },
        styles: {
          tableExample: {
            fontSize: 10,
            
            margin: [0, 0, 0, 10]
          },
        }
    };
}

 function empmaster(e){ 
    var json1=JSON.parse($('#item-json1').val());
    var branchid=$(e).val();
    var str='<option value="Select">Select</option>';
    for(var i in json1){
        if(json1[i].branchid==branchid){
        str+='<option value="'+json1[i].employeeid+'">'+json1[i].username+'</option>';
        }
    }
    $('#employeeid').html(str);
}
var getDaysInMonth = function(month,year) {
 return new Date(year, month, 0).getDate();
};
ui($('#inp-month'));

function ui(e){
    var json2=JSON.parse($('#item-json2').val());
    var arr = $(e).val().split('-');
    var days=getDaysInMonth(arr[1], arr[0]);
   
    var htmlstr='';
    var gsDayNames = ['SUNDAY','MONDAY','TUESDAY','WEDNESDAY','THURSDAY','FRIDAY','SATURDAY'];
    for(var i=1;i<=days;i++){
        var strclass='';
        var strstyle='';
        for(var k in json2){
            var d1=json2[k].holidaydate;
            var b='0';
            if(i.toString().length==1){
                b+=i;
            }else{
                b=i;
            }
            var d2=$(e).val()+'-'+b;
            if(d1==d2){
                strstyle='style="background:red;"'; 
                strclass='class="holidaydate"';
            }
        }
        
        htmlstr+='<tr '+strclass+' '+strstyle+'>';
        htmlstr+='<td align="center" class="day">'+i+'</td>';
        var d = new Date($(e).val()+'-'+i);
        var dayName = gsDayNames[d.getDay()];
        htmlstr+='<td align="center" class="weekday">'+dayName+'</td>';
        htmlstr+='<td align="center"><input onkeyup="totalhrscalc(this)" mouseup="totalhrscalc(this)" class="intime form-control input-sm" type="time"/></td>';
        htmlstr+='<td align="center"><input onkeyup="totalhrscalc(this)" mouseup="totalhrscalc(this)" class="outtime form-control input-sm" type="time"/></td>';
        htmlstr+='<td align="center"><input class="totalhrs form-control input-sm" type="text" readonly/></td>';
        htmlstr+='<td align="center" class="workinghours"></td>';
        htmlstr+='<td align="center" class="overtime"></td>';
        htmlstr+='<td align="center"><input  class="remark form-control input-sm" type="text"/></td>';
        htmlstr+='</tr>';
    }
    $('#table1 > tbody').html(htmlstr);
}

$('#branchid').val($('#inp-session-branchid').val()).trigger('change');

function defailttimeset(){
    var intime=$('#default-in').val();
    var outtime=$('#default-out').val();
    var diff =  Math.abs(new Date("1970-1-1 " + outtime) - new Date("1970-1-1 " + intime));
    var seconds = Math.floor(diff/1000); //ignore any left over units smaller than a second
    var minutes = Math.floor(seconds/60); 
    seconds = seconds % 60;
    var hours = Math.floor(minutes/60);
    minutes = minutes % 60;
    diff =  + hours + ":" + minutes;
    var weekoff=$('#item-weekoff').val();
    $('#table1 > tbody > tr').each(function(){
        var weekday=$(this).find('.weekday').text().trim();
        var holidaydate=$(this).hasClass("holidaydate");
        
        if(weekday!=weekoff && holidaydate==false && $(this).hasClass("PL")==false && $(this).hasClass("CL")==false && $(this).hasClass("SL")==false && $(this).hasClass("compoff")==false && $(this).hasClass("nocall")==false){
            $(this).find('.intime').val(intime); 
            $(this).find('.outtime').val(outtime); 
            $(this).find('.totalhrs').val(diff); 
        }else{
            $(this).find('.totalhrs').val(0); 
        }
    });
    overtime();
}
function totalhrscalc(e){
var intime=$(e).parent().parent().find('.intime').val();
var outtime=$(e).parent().parent().find('.outtime').val();
var diff =  Math.abs(new Date("1970-1-1 " + outtime) - new Date("1970-1-1 " + intime));
var seconds = Math.floor(diff/1000); //ignore any left over units smaller than a second
var minutes = Math.floor(seconds/60); 
seconds = seconds % 60;
var hours = Math.floor(minutes/60);
minutes = minutes % 60;
diff =  + hours + ":" + minutes;
$(e).parent().parent().find('.totalhrs').val(diff);
overtime();
}

function empdetails(e){
     
    var json1=JSON.parse($('#item-json1').val());
    var json3=JSON.parse($('#item-json3').val());
    var leaveallot=[]; 
    
    var employeeid=$(e).val();
        for(var i in json3){
            if(json3[i].employeeid==employeeid){
                    var leavetype=json3[i].leavetype;
                    var fromdate=new Date(json3[i].fromdate);
                    var todate=new Date(json3[i].todate);
                    while (fromdate <= todate) {
                        var year = fromdate.getFullYear();
                        var month = (fromdate.getMonth()+1) < 10 ? '0' + (fromdate.getMonth()+1) : (fromdate.getMonth()+1);
                        var day = fromdate.getDate() < 10 ? '0' + fromdate.getDate() : fromdate.getDate();
                        leaveallot.push({'day':year+'-'+month+'-'+day,'leavetype':leavetype});
                        fromdate.setDate(fromdate.getDate() + 1);
                    }
            }
        }
    
    $('#table1 > tbody > tr').each(function(){
        var day=parseInt($(this).find('.day').text().trim());
            var b='0'; if(day.toString().length==1){ b+=day; }else{ b=day; }
            var day2=$('#inp-month').val()+'-'+b;
            for(var i in leaveallot){
                day1=leaveallot[i].day;
                if(day1==day2){
                    $(this).addClass(leaveallot[i].leavetype);
                    $(this).attr('style','display:none!important;');
                    $(this).find('.totalhrs').val(0);
                }
            }
    });

    var weeklyoff='';
    var workinghours='';
    for(var i in json1){
        if(json1[i].employeeid==employeeid){
            weeklyoff=json1[i].weeklyoff;
            workinghours=json1[i].workinghours;
        }
    }
    $('#item-weekoff').val(weeklyoff);
    $('#table1 > tbody > tr').each(function(){
        var weekday=$(this).find('.weekday').text().trim();
        var holidaydate=$(this).hasClass("holidaydate");
        if(weekday==weeklyoff){
            $(this).css('background','#f1c40f'); 
            $(this).addClass('weeklyoff');
            $(this).find('.workinghours').text(0); 
        }else{
            if(holidaydate==true){
                $(this).find('.workinghours').text(0);
            }else{
                $(this).find('.workinghours').text(workinghours); 
            }
        } 
    });

        $.ajax({
			type: "POST",
			data: {
                    month:$('#inp-month').val(),
                    branchid:$('#branchid').val(),
                    employeeid:employeeid,
				},
				url: 'select.php',
				cache: false,
				success: function (res) {
                    if(res.status=='success'){
                        modals.putvalue('table2',res.table2);
                        var table1=res.table1[0];
                        var i=0;
                        $('#table1 > tbody > tr').each(function(){ 
                            $(this).find('.day').text(table1[i].day);
                            $(this).find('.intime').val(table1[i].intime);
                            $(this).find('.outtime').val(table1[i].outtime);
                            $(this).find('.overtime').text(table1[i].overtime);
                            $(this).find('.remark').val(table1[i].remark);
                            $(this).find('.totalhrs').val(table1[i].totalhrs);
                            $(this).find('.weekday').text(table1[i].weekday);
                            $(this).find('.workinghours').text(table1[i].workinghours);
                            i++;
                         });
                         $('#btn-submit').attr('onclick','update()');
                    $('#btn-submit').text('Update');
                    }
				}
        });
            
}

function overtime(){
    $('#table1 > tbody > tr').each(function(){
       var totalhrs=parseFloat($(this).find('.totalhrs').val());
       var workinghours=parseFloat($(this).find('.workinghours').text());
       var diff = totalhrs-workinghours;
       if(diff<0){
        diff=0;
       }
        $(this).find('.overtime').text(diff);
   })
   sidecalc();
}

function sidecalc(){
    var daysinmonth=0;
    var holidayinmonth=0;
    var weekoffinmonth=0;
    var workingdayinmonth=0;
    var paidleave=0;
    var casualleave=0;
    var seekleave=0;
    var commanoff=0;
    var nocall=0;
    var overtimeday=0;
    var overtimehrs=0;
    var paidweekofdays=0;
    var paidweekofhrs=0;
    var lateday=0;
    var halfday=0;
    var latecutting=0;
    var workinghrsinmonths=0;
    var empworkingovertimeday=0;
    var empworkingovertimehrs=0;
    $('#table1 > tbody > tr').each(function(){
        daysinmonth++;
        if($(this).hasClass("holidaydate")){ holidayinmonth++; }
        var overtime=parseInt($(this).find(".overtime").text());
         if($(this).hasClass("weeklyoff")){
            weekoffinmonth++; 
            if(overtime!=0){ paidweekofhrs+=overtime; paidweekofdays++; }
            }else{
            if(overtime!=0){ overtimehrs+=overtime; overtimeday++; }
        }

        if($(this).hasClass("PL")){ paidleave++; }
        if($(this).hasClass("CL")){ casualleave++; }
        if($(this).hasClass("SL")){ seekleave++; }
        if($(this).hasClass("compoff")){ commanoff++; }
        if($(this).hasClass("nocall")){ nocall++; }
        var totalhrs=parseFloat($(this).find(".totalhrs").val().replace(':','.'));
        var workinghrs=parseInt($(this).find(".workinghours").text());
        workinghrsinmonths+=workinghrs;
        if(workinghrs>totalhrs){   
            if($(this).hasClass("PL")==false && $(this).hasClass("CL")==false && $(this).hasClass("SL")==false && $(this).hasClass("compoff")==false && $(this).hasClass("nocall")==false){
                if(((workinghrs/2)>totalhrs)){
                    if (totalhrs!=0) {
                        halfday++;
                    }
                }else{
                    lateday++; 
                }   
            }
        }
        
        if(totalhrs!=0 && totalhrs!=0.0){
            empworkingovertimeday++;
            empworkingovertimehrs+=totalhrs;
        }
    });
    workingdayinmonth=daysinmonth-(holidayinmonth+weekoffinmonth);

    $('#daysinmonth').val(daysinmonth);
   
   $('#holidayinmonth').val(holidayinmonth);
    
    $('#weekoffinmonth').val(weekoffinmonth);
    $('#workingdayinmonth').val(workingdayinmonth);

    $('#paidleave').val(paidleave);
    $('#casualleave').val(casualleave);
    $('#seekleave').val(seekleave);
    $('#commanoff').val(commanoff);
    $('#nocall').val(nocall);

    $('#overtimeday').val(overtimeday);
    $('#overtimehrs').val(overtimehrs);
    $('#paidweekofdays').val(paidweekofdays);
    $('#paidweekofhrs').val(paidweekofhrs);
    
    $('#lateday').val(lateday);
    $('#halfday').val(halfday);
    $('#latecutting').val(latecutting);
    $('#workinghrsinmonths').val(workinghrsinmonths);
    $('#empworkingovertimeday').val(empworkingovertimeday);
    $('#empworkingovertimehrs').val(empworkingovertimehrs);
 }


 function validation(){
var obj={};
    var month=$('#inp-month').val();
    var branchid=$('#branchid').val();
    var employeeid=$('#employeeid').val();
    var table1=[];
    $('#table1 > tbody > tr').each(function(){
        table1.push({
            "day":$(this).find('.day').text().trim(),
            "weekday":$(this).find('.weekday').text().trim(),
            "intime":$(this).find('.intime').val(),
            "outtime":$(this).find('.outtime').val(),
            "totalhrs":$(this).find('.totalhrs').val(),
            "workinghours":$(this).find('.workinghours').text().trim(),
            "overtime":$(this).find('.overtime').text().trim(),
            "remark":$(this).find('.remark').val()
        });
    });  
    table2=checker('table2');
    var table1 = JSON.stringify(table1);
    var table2 = JSON.stringify(table2);
    obj={
                    month:month,
                    branchid:branchid,
                    employeeid:employeeid,
                    data1: table1,
                    data2: table2
    }
  

    if(month!='' && branchid!='Select' && employeeid!='Select'){
        return obj;
    }else{
        return false;
    }
 }

 function insert(){
     if(validation()){
        $.ajax({
				type: "POST",
				data: validation(),
				url: 'insert.php',
				cache: false,
				success: function (res) {
                    swal({
							type: 'success',
							title: 'Attandance Master Created',
							showConfirmButton: false,
							timer: 3000
						});
						setTimeout(function () {
							window.location.reload();
						}, 3000);
				}
			});
     }
 }

 function update(){
    if(validation()){
            $.ajax({
				type: "POST",
				data: validation(),
				url: 'update.php',
				cache: false,
				success: function (res) {
                    swal({
							type: 'success',
							title: 'Attendance Master Updated',
							showConfirmButton: false,
							timer: 3000
						});
						setTimeout(function () {
							window.location.reload();
						}, 3000);
				}
            });
        }
 }

</script>