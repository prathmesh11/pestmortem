<?php
   
   $base = '../../../';
   
    $js = '<script src="'.$base.'js/fwork.js"></script>
          <script src="'.$base.'js/alart.js"></script>
          <script src="'.$base.'js/list.min.js"></script>';

    $css = '<link rel="stylesheet" href="'.$base.'css/fwork.css">
           <link rel="stylesheet" href="'.$base.'css/alart.css">';

    $scode = '';

    if(isset($_GET['scode'])){

        $scode = $_GET['scode'];

    }

    include($base.'_in/header.php');

    include($base.'_in/connect.php');

    if (!in_array("Supplier Master", $arrysession)) {
   
        echo "<script>window.location.href='/process/dashboard.php';</script>";
       
        exit;
    
    }
 

    $con = _connect();

?>

<style>

    hr.style-hr {
        border: 0;
        height: 1px;
        background-image: linear-gradient(to right, rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.75), rgba(0, 0, 0, 0));
    }

    .table-list td,
    .table-list th {
        border: 1px solid #ddd;
        padding: 2px;
        font-size: 13px;
    }

    .table-list tr:nth-child(even) {
        background-color: #f2f2f2;
    }

    .table-list th {
        padding-top: 12px;
        padding-bottom: 12px;
        text-align: left;
        background-color: #16a085;
        color: white;
    }

    .sidenav1 {
        height: 100%;
        width: 0;
        position: fixed;
        z-index: 1;
        top: 0;
        left: 0;
        background-color: #111;
        overflow-x: hidden;
        transition: 0.5s;
        padding-top: 60px;
    }

    .sidenav1 a {
        padding: 8px 8px 8px 32px;
        text-decoration: none;
        font-size: 25px;
        color: #818181;
        display: block;
        transition: 0.3s;
    }

    .sidenav1 a:hover {
        color: #f1f1f1;
    }

    .sidenav1 .closebtn1 {
        position: absolute;

        right: 25px;
        font-size: 36px;
        margin-left: 50px;
    }

    @media screen and (max-height: 450px) {
        .sidenav1 {
            padding-top: 15px;
        }

        .sidenav1 a {
            font-size: 18px;
        }
    }

    .list {
        padding: 0px;
    }

    .list li span {
        display: inline;
    }

    .list li {
        text-decoration: none;
        display: block;
        padding: 2px;
        background: #FFF;
        border: 1px solid #333;
        color: #000;
    }

    .list1 li {
        
        border: none;
        
    }

    .list1 li:hover {
        background: #0984e3;
        color: #fff;
    }



 @import url(https://fonts.googleapis.com/css?family=Merriweather);
 body {
	 background: #f1f1f1;
	 font-family: 'Merriweather', sans-serif;
	 /* padding: 1em; */
}
 h1 {
	 text-align: center;
	 color: #a8a8a8;
	 text-shadow: 1px 1px 0 rgba(255, 255, 255, 1);
}
 #pestMortem {
	 max-width: 600px;
	 text-align: center;
	 margin: 20px auto;
}
 #pestMortem input, #pestMortem textarea, #pestMortem button{
	 border: 0;
	 outline: 0;
	 padding: 1em;
	 border-radius: 8px;
	 display: block;
	 width: 100%;
	 margin-top: 1em;
	 font-family: 'Merriweather', sans-serif;
	 box-shadow: 0 1px 1px rgba(0, 0, 0, 0.1);
	 resize: none;
}
#pestMortem input:focus, #pestMortem textarea:focus {
	 box-shadow: 0 0px 2px rgba(231, 76, 60, 1) !important;
}
#pestMortem #input-submit {
	 color: white;
	 background: #e74c3c;
	 cursor: pointer;
}
#pestMortem #input-submit:hover {
	 box-shadow: 0 1px 1px 1px rgba(170, 170, 170, 0.6);
}
#pestMortem textarea {
	 height: 126px;
}
 .half {
	 float: left;
	 width: 100%;
	 margin-bottom: 1em;
}
 .right {
	 width: 50%;
}
 .left {
	 margin-right: 2%;
}
 @media (max-width: 480px) {
	 .half {
		 width: 100%;
		 float: none;
		 margin-bottom: 0;
	}
}
/* Clearfix */
 .cf:before, .cf:after {
	 content: " ";
	/* 1 */
	 display: table;
	/* 2 */
}
 .cf:after {
	 clear: both;
}
 
    
</style>

<div class="container-fluid">

    <div class="row content" id="section">

        <span style="font-size:24px;cursor:pointer" onclick="openNav1()">&#9776; Search</span>
        
        <div id="mysidenav1" class="sidenav1" style="margin-left:80px;">

            <div id="test-list" style="padding:5px;">
            
                <a href="javascript:void(0)" class="closebtn1" onclick="closeNav1()">&times;</a>
                <br><br><br>

                <div class="form-group">

                    <input type="text" placeholder="Search" class="form-control input-sm fuzzy-search" style="border-radius:3px;">

                </div>
                
                <ul class="list">

                    <?php 

                        $result1 = mysqli_query($con, "SELECT * FROM `groupmaster`");

                        while($rows=mysqli_fetch_assoc($result1)) {

                            echo '<li><span class="groupmaster">' .$rows['groupname']. '<span><button style="margin-left:2px;" data-groupid="' .$rows['id']. '" onclick="edit(this)" class="btn btn-primary btn-sm"><span class="fa fa-edit"></span> E</button></i>';

                        }

                    ?>

                </ul>

            </div>

        </div>

        <h2 align="center" style="margin-top:0px;">Create New Groups</h2>

        <div style="clear:both;"></div>

        <hr class="style-hr">

        <input type="hidden" data-name='groupid' id="groupId" class="form-control input-sm">

        <div class="cf" id="pestMortem">
            
            <div class="half left cf">
            
                <input type="text" data-role='text' data-name='groupname' id="input-name" placeholder="Group Name">
            
            </div>
            <!-- <div class="half right cf">
            <textarea name="message" type="text" id="input-message" placeholder="Message"></textarea>
        </div>   -->
            <button type="submit" onclick="submit()" id="input-submit">Submit</button>
        </div>


    </div>

</div>



<?php

    include($base.'_in/footer.php');

?>

<script>

function jump(e) {

    var groupmastercode = $(e).find('span').text().trim();

    $('#groupmastercode').val(groupmastercode);

    $('#searchbox').hide();

}

$('#groupmastercode').keypress(function (e) {

    if (e.which == 13) {

        $('#searchbox').hide();

    } else {

        $('#searchbox').show();

    }

});


function openNav1() {
    document.getElementById("mysidenav1").style.width = "300px";
}

function closeNav1() {
    document.getElementById("mysidenav1").style.width = "0";
}
var monkeyList = new List('test-list', {
    valueNames: ['groupmaster']
});



//---------------------------------Insert-----------------------------------//

function submit() {
    
    var valid = true;

    if (checker('section') != false) {

        valid = valid * true;

    } else {

        valid = valid * false;

    }

    if (valid) {

        var data     = checker('section');

        var datastr  = JSON.stringify(data);

        $.ajax({

            type: "POST",

            data: {

                data: datastr

            },

            url: 'insert.php',

            cache: false,

            success: function (res) {

                if (res.status == 'success') {

                    swal({

                        type: 'success',

                        title: 'Group Created',

                        showConfirmButton: false,
                        
                        timer: 3000

                    });

                    setTimeout(function () {

                        window.location.reload();

                    }, 3000);

                }

                if (res.status == 'falid1') {

                    swal('Duplicate Group Name', '', 'error');

                }

            }

        });

    }

}


//-----------------------------Edit----------------------------//

function edit(e) {

    var groupid = $(e).attr('data-groupid');

    $.ajax({

        type: "POST",

        data: 'groupid=' + groupid,

        url: 'select.php',

        cache: false,

        success: function (res) {

            if (res.status == 'success') {
                
                closeNav1();

                $('#input-submit').attr('onclick', 'update()');

                $('#input-submit').text('Update');

                modals.putvalue('section', res.json);

            }

        }

    });

}


function update() {

    var valid = true;

    if (checker('section') != false) {

        valid = valid * true;

    } else {

        valid = valid * false;

    }

    if (valid) {

        var data     = checker('section');

        var datastr  = JSON.stringify(data);

        $.ajax({

            type: "POST",

            data: {

                data: datastr

            },

            url: 'update.php',

            cache: false,

            success: function (res) {

                if (res.status == 'success') {

                    swal({

                        type: 'success',
                        title: 'Supplier Master Updated',
                        showConfirmButton: false,
                        timer: 3000

                    });

                    setTimeout(function () {

                        window.location.reload();

                    }, 3000);

                }

                if (res.status == 'falid1') {

                    swal('Duplicate Group Name', '', 'error');

                }

            }
            
        });

    }

}

//-------------------------------Button Reset----------------------------//

$('#btn-reset').on('click', function () {

    window.location.reload();

});


</script>