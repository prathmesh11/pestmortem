<?php

    $base = '../../../';

    include($base.'_in/connect.php');

    header('content-type: application/json; charset=utf-8');

    header("access-control-allow-origin: *");

    if(isset($_POST['data'])) {

        $con = _connect();

        if (session_status()==PHP_SESSION_NONE) { session_start(); }

        $created_by = $_SESSION['employeeid'];

        $branchid   = $_SESSION['branchid'];

        $data       = get_object_vars(json_decode($_POST["data"]));

        $groupname  = strtoupper($data['groupname']);

        $checkGroup = mysqli_fetch_assoc(mysqli_query($con,"SELECT groupname x FROM groupmaster WHERE groupname LIKE '%$groupname%'"))['x'];

        if($checkGroup) {
                
            echo '{"status":"falid1"}';

        } else {
           
            $create = mysqli_query($con, "INSERT INTO groupmaster (branchid,groupname,created_by,created_time) VALUES ('$branchid','$groupname','$created_by','$CURRENT_MILLIS')");

            if($create) {

                echo '{"status":"success"}';

            } else {

                echo '{"status":"falid1"}';

            }
               
        }
        
        _close($con);

    } else {

        echo '{"status":"falid"}';

    }
?>