<?php
//  $base='../../../../../';
  $base='../../../';

//  include($base.'in/connect.php');
  include($base.'_in/connect.php');

    header('content-type: application/json; charset=utf-8');
    header("access-control-allow-origin: *");
    if(isset($_POST['data'])){
      $con=_connect();
      if (session_status() == PHP_SESSION_NONE) { session_start(); }
      $created_by = $_SESSION['id'];
      $json1='';
      $data=get_object_vars(json_decode($_POST["data"]));
      $couter1=1;
      $branchid=_clean($con, $data['branchid']);
      $paymentdate=_clean($con, $data['paymentdate']);
      $salarydate=_clean($con, $data['salarydate']);
      // echo $paymentdate;
      // echo $salarydate;

      
      $result5= mysqli_query($con,"SELECT salarydate FROM `salaryprocess` WHERE salarydate='$salarydate' AND branchid='$branchid' LIMIT 1");
      $rows2=mysqli_fetch_assoc($result5);
      $date2=$rows2['salarydate']; 
      
      if($date2==$salarydate){
        echo '{"status":"AlreadyProcessed"}';
      }else{
         //-------------------- Processubg start--------------------
         // $str="SELECT salarymaster.*, employeemaster.username FROM salarymaster Left Join employeemaster on employeemaster.employeeid=salarymaster.employeeid order by salarymaster.employeeid";
        $result1=mysqli_query($con,"SELECT salarymaster.*, employeemaster.username FROM salarymaster Left Join employeemaster on employeemaster.employeeid=salarymaster.employeeid order by salarymaster.employeeid");
        while($rows1 = mysqli_fetch_assoc($result1)){
          $employeeid=$rows1['employeeid']; 
          $employeename=$rows1['username'];
          //echo "stage 1";
  
          //$basic=get_object_vars(json_decode($rows1['officialjson']))['basic'];
          $basic=get_object_vars(json_decode($rows1['officialjson']))['basic'];
          $da=get_object_vars(json_decode($rows1['officialjson']))['das'];
          $hra=get_object_vars(json_decode($rows1['officialjson']))['hra'];
  
  
          $ta=get_object_vars(json_decode($rows1['officialjson']))['ta'];
          $medical=get_object_vars(json_decode($rows1['officialjson']))['medical'];
          $conveyance=get_object_vars(json_decode($rows1['officialjson']))['conveyance'];
          $education=get_object_vars(json_decode($rows1['officialjson']))['education'];
  
          $cca=get_object_vars(json_decode($rows1['officialjson']))['cca'];
          $mobile=get_object_vars(json_decode($rows1['officialjson']))['mobile'];
          $uniform=get_object_vars(json_decode($rows1['officialjson']))['uniform'];
          $internet=get_object_vars(json_decode($rows1['officialjson']))['internet'];
          $shoes=get_object_vars(json_decode($rows1['officialjson']))['shoes'];
          $employeeesic=get_object_vars(json_decode($rows1['officialjson']))['employeeesic'];
          $leavingdate=get_object_vars(json_decode($rows1['officialjson']))['leaving'];
          $employeepf=get_object_vars(json_decode($rows1['officialjson']))['employeepf'];
          $rateinmaster=get_object_vars(json_decode($rows1['officialjson']))['basic'];
          $workhrs=get_object_vars(json_decode($rows1['officialjson']))['workinghours'];
          $pfperc=get_object_vars(json_decode($rows1['officialjson']))['employeepf'];
          $esicperc=get_object_vars(json_decode($rows1['officialjson']))['employeeesic'];
          $otind=get_object_vars(json_decode($rows1['officialjson']))['ot'];
          $itdeduction=get_object_vars(json_decode($rows1['officialjson']))['itdeduction'];
          $pt=get_object_vars(json_decode($rows1['officialjson']))['pt'];
  
          $category=get_object_vars(json_decode($rows1['personaljson']))['category'];
          $departmentid=get_object_vars(json_decode($rows1['personaljson']))['department'];
          $designationid=get_object_vars(json_decode($rows1['personaljson']))['designation'];
          $companyid=get_object_vars(json_decode($rows1['personaljson']))['company'];
          $gender=get_object_vars(json_decode($rows1['personaljson']))['gender'];
          $paymentindicator=get_object_vars(json_decode($rows1['personaljson']))['payment'];
          $salarydays=0;
          $paidleave=0;
          $casualleave=0;
          $seekleave=0;
          $commanoff=0;
          $holidayinmonth=0;
          $weekoffinmonth=0;
          $overtimehrs=0;
          $daysinmonth=0;
          $result2=mysqli_query($con,"SELECT * FROM attendancemaster where employeeid='$employeeid'");
          while($rows2 = mysqli_fetch_assoc($result2)){
             $presdays=get_object_vars(json_decode($rows2['sidejson']))['empworkingovertimeday'];
             $paidleave=get_object_vars(json_decode($rows2['sidejson']))['paidleave'];
             $casualleave=get_object_vars(json_decode($rows2['sidejson']))['casualleave'];
             $seekleave=get_object_vars(json_decode($rows2['sidejson']))['seekleave'];
             $commanoff=get_object_vars(json_decode($rows2['sidejson']))['commanoff'];
             $holidayinmonth=get_object_vars(json_decode($rows2['sidejson']))['holidayinmonth'];
             $weekoffinmonth=get_object_vars(json_decode($rows2['sidejson']))['weekoffinmonth'];
  
             $salarydays=($presdays+$paidleave+$casualleave+$seekleave+$commanoff+$holidayinmonth+$weekoffinmonth);
  
             $overtimehrs=get_object_vars(json_decode($rows2['sidejson']))['overtimehrs'];
             $daysinmonth=get_object_vars(json_decode($rows2['sidejson']))['daysinmonth'];
  
          }
          $designation=mysqli_fetch_assoc(mysqli_query($con,"SELECT groupname x FROM additionalmaster where groupcode='$designationid'")) ['x'];
          $department=mysqli_fetch_assoc(mysqli_query($con,"SELECT groupname x FROM additionalmaster where groupcode='$departmentid'")) ['x'];
          $companyname=mysqli_fetch_assoc(mysqli_query($con,"SELECT groupname x FROM additionalmaster where groupcode='$companyid'")) ['x'];
   
          //      income
          if ($daysinmonth!=$salarydays) {
          //  echo "total salary days ".$salarydays;
  
            $basicpay=($basic/$daysinmonth)*$salarydays;
            $da=($da/$daysinmonth)*$salarydays;
            $hra=($hra/$daysinmonth)*$salarydays;
            $conveyance=($conveyance/$daysinmonth)*$salarydays;
            $medical=($medical/$daysinmonth)*$salarydays;
            $wash=($wash/$daysinmonth)*$salarydays;
          }else {
            $basicpay=$basic;
            $da=$da;
            $hra=$hra;
            $conveyance=$conveyance;
            $medical =$medical;
            $wash=$wash;
          }
  
          $otamt=0;
          if ($ot=='yes'){
            $otamt=(($basicpay/$daysinmonth)/$workhrs)*$overtimehrs;
          } 
  
  
            //        otheradd
            //        lwf
          $pfamt=0;
          $esicamt=0;
          if (($basicpay+$da+$hra+$conveyance)>8500) {
            $pfamt=round((($basicpay+$da+$hra+$conveyance)/100)*$pfperc,0);
          }
          if (($basicpay+$da+$hra+$conveyance+$medical+$wash)>8500) {
            $esicamt=round((($basicpay+$da+$hra+$conveyance+$medical+$wash)/100)*$esicperc,0);
          }
  
          //        lic
          $ptcal=(($basicpay+$da+$hra+$conveyance+$medical+$wash+$otamt));
          $ptamt=0;
          if ($pt=='yes') {
             $result2=mysqli_query($con,"SELECT * FROM professionaltax order by id");
             while($rows2 = mysqli_fetch_assoc($result2)){
              if (($ptcal>$rows2['fromamt']) && ($ptamt<$rows2['toamt'])) {
                  $ptamt=$rows2['taxamt'];
               }
             }              
          }
  
          $loandeduc=0;
          $actloandeduc=0;
          $loanamt=0;
          $result2=mysqli_query($con,"SELECT amount FROM loanadvancemaster where employeeid='$employeeid' and loantype='Loan' order by id");
          while($rows2 = mysqli_fetch_assoc($result2)){
              $loanamt+=$rows2['amount'];
              $actloandeduc=$rows2['dedamt'];
          }              
          $result2=mysqli_query($con,"SELECT loandeduction FROM salaryprocess where employeeid='$employeeid'");
          while($rows2 = mysqli_fetch_assoc($result2)){
              $loandeduc=$rows2['loandeduction'];
          }              
          if ($loanamt>$loandeduc) {
             if (($loanamt-$loandeduc)<$actloandeduc) {
               $actloandeduc=($loanamt-$loandeduc);
             }
          }
          
          $advancededuction=0;
          $result2=mysqli_query($con,"SELECT amount FROM loanadvancemaster where employeeid='$employeeid' and loantype='Advance' and (month(advancedate)=month('salarydate')  and (year(advancedate)=year('$salarydate')) ");
          while($rows2 = mysqli_fetch_assoc($result2)){
              $advancededuction+=$rows2['amount'];
          }              
  
         
          //        otherdeduction
  
          // salary calculation
          $totalearning=(($basicpay+$da+$hra+$conveyance+$medical+$wash+$otamt));
          $totaldeduction=($ptamt+$pfamt+$esicamt+$loandeduc+$advancededuction);
          $netpayment=$totalearning-$totaldeduction;
  
          $json_dayshrsinfo='{"presentdays":"'.$presdays.'","paidleave":"'.$paidleave.'","casualleave":"'.$casualleave.'"
            ,"seekleave":"'.$seekleave.'","commanoff":"'.$commanoff.'","holidayinmonth":"'.$holidayinmonth.'",
            "weekoffinmonth":"'.$weekoffinmonth.'","salarydays":"'.$salarydays.'","overtimehrs":"'.$overtimehrs.'","daysinmonth":"'.$daysinmonth.'"}';      
      
          $json_income='{"basicpay":"'.$basicpay.'","da":"'.$da.'","hra":"'.$hra.'"
            ,"conveyance":"'.$conveyance.'","medical":"'.$medical.'","wash":"'.$wash.'",
            "otamt":"'.$otamt.'"}';        
          //      income
          
          //      deductions
          $json_deduction='{"ptamt":"'.$ptamt.'","pfamt":"'.$pfamt.'","esicamt":"'.$esicamt.'"
            ,"loandeduc":"'.$loandeduc.'","advancededuction":"'.$advancededuction.'","itdeduction":"'.$itdeduction.'"}'; 
            
  
          $create=mysqli_query($con,"INSERT INTO salaryprocess (branchid,paymentdate,salarydate,employeeid,employeename,designationid,designation,departmentid,
         department,companyid,companyname,gender,rateinmaster,paymentindicator,totalearning,totaldeduction,netpayment,
         income,dayshrsinfo,deductions,loandeduction,created_by,created_time) values 
          ('$branchid','$paymentdate','$salarydate','$employeeid','$employeename','$designationid','$designation','$departmentid','$department','$companyid','$companyname','$gender',
          '$rateinmaster','$paymentindicator','$totalearning','$totaldeduction','$netpayment','$json_income','$json_dayshrsinfo',
          '$json_deduction','$loandeduction','$created_by','$CURRENT_MILLIS')");
          
        }
        if($create){
          echo '{"status":"success"}';
        }else{
          echo '{"status":"falid"}';
        }
      }

     
        
      _close($con);
    }else{
        echo '{"status":"falid"}';
     }
?>

