<?php
    $base='../../../';
    $js=' <script src="'.$base.'js/fwork.js"></script>
    <script src="'.$base.'js/alart.js"></script> 
    <script src="'.$base.'js/list.min.js"></script>
    <script src="'.$base.'js/picker.js"></script>
    <script src="'.$base.'js/picker.date.js"></script>
    <script src="'.$base.'js/picker.time.js"></script>
    <script src="'.$base.'js/pdfmake.min.js"></script>
    <script src="'.$base.'js/vfs_fonts.js"></script>';

    $css='<link rel="stylesheet" href="'.$base.'css/fwork.css">
    <link rel="stylesheet" href="'.$base.'css/alart.css">
    <link rel="stylesheet" href="'.$base.'css/grid.min.css">
    <link rel="stylesheet" href="'.$base.'css/classic.date.css">
    <link rel="stylesheet" href="'.$base.'css/classic.time.css">
    <link rel="stylesheet" href="'.$base.'css/classic.css">';
    include($base.'_in/connect.php');
    include($base.'_in/header.php');

    if (!in_array("Salary Processing", $arrysession)) {
   
        echo "<script>window.location.href='/process/dashboard.php';</script>";
       
        exit;
    
    }

    $con=_connect();
?>
<style>
    .picker__select--year{
            height:auto;
        }
        .picker__select--month{
            height:auto;
        }
    hr.style-hr {
        border: 0;
        height: 1px;
        background-image: linear-gradient(to right, rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.75), rgba(0, 0, 0, 0));
    }
    .content{
        margin:80px 80px;
    }
</style>
<div class="container-fluid" id="section">
<div class="content">
    <h2 align="center" style="margin:0;">Salary Process</h2>
    <hr class="style-hr">
    
    <div class="row">
        <div class="col-sm-3">
            <label for="">Branch Name</label>
            <div class="form-group">
            <select class="form-control input-sm" data-role="select" data-name="branchid" id="branchid">
        <option value="Select">Select</option>
            <?php 
                    $result1=mysqli_query($con, "SELECT * FROM `branchmaster`");
                    while($rows=mysqli_fetch_assoc($result1)) {
                    echo '<option value="'.$rows['branchid'].'">'.$rows['branchname'].'</option>';
                }
            ?>
        </select>
            </div>
        </div>
        <div class="col-sm-3">
            <label for="">Payment Date</label>
            <div class="form-group">
            <input type="text" data-name="paymentdate" data-role="text" id="paymentdate"
                    class="form-control sm datepicker">
            </div>
        </div>
        <div class="col-sm-3">
            <label for="">Salary Month/Year</label>
            <div class="form-group">
            <input type="month" data-name="salarydate" class="form-control input-sm"  id="salarydate" value="<?php echo date("Y-m"); ?>">
            </div>
        </div>
        <div class="col-sm-3" style="margin-top:20px;">
        <button class="btn btn-success btn-block" id="btn-submit" onclick="insert()">Submit</button>
        </div>
    </div>
</div>

</div>



<?php
include($base.'_in/footer.php');
?>
<script>

$('.datepicker').pickadate({     
    selectYears: 100,
	selectMonths: true,
	onClose: function () {
		try {
			var dateStamp = this.get('select')['pick'];
			var id = this.get('id');
			$('#' + id).attr('data-stamp', dateStamp);
			$('#' + id).parent().find('.timepicker').click();
		} catch (err) {
			console.log(err);
		}
	}
});



function insert() {
    var valid = true;
    if (checker('section') != false) {
        valid = valid * true;
    } else {
        valid = valid * false;
    }

    if (valid) {
        data = checker('section');
        var datastr = JSON.stringify(data);

        $.ajax({
            type: "POST",
            data: {
                data: datastr
            },
            url: 'insert1.php',
            cache: false,
            success: function (res) {
                if (res.status == 'success') {
                    // $('.loader').hide();
                    console.log("salary inserted");
                    swal({
                        type: 'success',
                        title: 'Salary Processing Completed Successfully',
                        showConfirmButton: false,
                        timer: 3000
                    });
                    setTimeout(function () {
                        window.location.reload();
                    }, 3000);
                }
                if (res.status == 'AlreadyProcessed') {
                    swal({
                        type: 'AlreadyProcessed',
                        title: 'Salary Alrady Processed for this month',
                        showConfirmButton: false,
                        timer: 3000
                    });
                    setTimeout(function () {
                        window.location.reload();
                    }, 3000);
                }
            }
        });
    }
}




</script>

