<?php

    $base = '../../../'; 
   
    $js   = '<script src="'.$base.'js/fwork.js"></script>
            <script src="'.$base.'js/alart.js"></script> 
            <script src="'.$base.'js/list.min.js"></script>
            <script src="'.$base.'js/picker.js"></script>
            <script src="'.$base.'js/picker.date.js"></script>
            <script src="'.$base.'js/picker.time.js"></script>
            <script src="'.$base.'js/pdfmake.min.js"></script>
            <script src="'.$base.'js/vfs_fonts.js"></script>';

    $css  = '<link rel="stylesheet" href="'.$base.'css/fwork.css">
            <link rel="stylesheet" href="'.$base.'css/alart.css">
            <link rel="stylesheet" href="'.$base.'css/grid.min.css">
            <link rel="stylesheet" href="'.$base.'css/classic.date.css">
            <link rel="stylesheet" href="'.$base.'css/classic.time.css">
            <link rel="stylesheet" href="'.$base.'css/classic.css">';

    include($base.'_in/header.php');
    
    include($base.'_in/connect.php');

    if (!in_array("Leave Allotment", $arrysession)) {
   
        echo "<script>window.location.href='/process/dashboard.php';</script>";
       
        exit;
    
    }

    $con   = _connect();
    
    $json1 ='';
    
    $result = mysqli_query($con,"SELECT * FROM employeemaster ORDER BY username ASC");
     
    while($rows = mysqli_fetch_assoc($result)){

        $salaryper = get_object_vars(json_decode($rows['employeejson']))['salaryper'];
        
        $json1.=',{"branchid":"'.$rows['branchid'].'","employeeid":"'.$rows['employeeid'].'","username":"'.$rows['username'].'","salaryper":"'.$salaryper.'"}';

    }

    $json1  = substr($json1,1);
    
    $json1  = '['.$json1.']';

    $json2  = '';
    
    $result = mysqli_query($con,"SELECT * FROM leaveopeningbalance ORDER BY id ASC");
    
    while($rows = mysqli_fetch_assoc($result)){
        
        $json2.=',{"employeeid":"'.$rows['employeeid'].'","paid_leave":"'.$rows['paid_leave'].'","casual_leave":"'.$rows['casual_leave'].'","standanrd_leave":"'.$rows['standanrd_leave'].'"}';
    
    }
    
    $json2 = substr($json2,1);
    
    $json2 = '['.$json2.']';

    $appno = mysqli_fetch_assoc(mysqli_query($con,"SELECT count(appno) x FROM leaveallotmentmaster "))['x'];

    $appno++;
        
    $appno = 10000+$appno;

?>

<style>
 
    hr.style-hr {
        border: 0;
        height: 1px;
        background-image: linear-gradient(to right, rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.75), rgba(0, 0, 0, 0));
    }

    hr.style-hr {
        border: 0;
        height: 1px;
        background-image: linear-gradient(to right, rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.75), rgba(0, 0, 0, 0));
    }

    .table-list td,
    .table-list th {
        border: 1px solid #ddd;
        padding: 1px !important;
        font-size: 13px;
    }

    .table-list td {
        padding-top: 10px !important;

    }

    .table-list tr:nth-child(even) {
        background-color: #f2f2f2;
    }

    .table-list th {
        padding-top: 5px;
        padding-bottom: 5px;
        text-align: center;
        background-color: #16a085;
        color: white;
    }

</style>

<div class="container-fluid" id="section">

    <div class=" content">
        
        <h2 align="center" style="margin:0;">Leave Allotment</h2>
        
        <hr class="style-hr">
      
        <div class="col-sm-12">
       
            <div class="col-sm-6">
        
                <div class="col-sm-12" id="section1">
        
                    <input type="hidden" id="inp-branch" value='<?php echo $branchid; ?>' />
        
                    <input type="hidden" id="item-json1" value='<?php echo str_replace("'"," &#39;",$json1); ?>' />
        
                    <input type="hidden" id="item-json2" value='<?php echo str_replace("'"," &#39;",$json2); ?>' />
        
                    <div class="col-sm-12">
                        
                        <div class="col-sm-4">Branch Name</div>
                            
                        <div class="col-sm-8">
                        
                            <div class="form-group">
                        
                                <select class="form-control input-sm" data-role="select" data-name="branchid" id="branchid" onchange="empmaster(this);" >
                        
                                    <option value="Select">Select</option>
                        
                                    <?php 
                        
                                        $result1=mysqli_query($con, "SELECT * FROM `branchmaster`");
                                        
                                        while($rows=mysqli_fetch_assoc($result1)) {
                                        
                                            echo '<option value="'.$rows['branchid'].'">'.$rows['branchname'].'</option>';
                                        
                                        }
                                    
                                    ?>
                                
                                </select>
                            
                            </div>
                        
                        </div>

                    </div>

                    <div class="col-sm-12">
                    
                        <div class="col-sm-4">Employee Name </div>
                    
                        <div class="col-sm-8">
                        
                            <div class="form-group">
                        
                                <select class="form-control input-sm" data-role="select" data-name="employeeid" id="employeeid"  onchange="leavemaster(this);"  >
                        
                                    <option value="Select">Select</option>
                        
                                </select>
                        
                            </div>
                        
                        </div>
                    
                    </div>
                
                </div>
                
                <div class="col-sm-12" >
                
                    <div class="col-sm-12">
                
                        <div class="col-sm-4">Salary Type </div>
                
                        <div class="col-sm-8">
                
                            <div class="form-group">
                
                                <div class="form-group">
                
                                    <input type="text" data-role="text" data-name="ltype" id="ltype" class="form-control input-sm" >
                
                                </div>
                
                            </div>
                
                        </div>
                    
                    </div>
                
                </div>
                
                <div class="col-sm-12" id="section2">

                
                    <div class="col-sm-12">
                
                        <div class="col-sm-4">Application No. </div>
                
                        <div class="col-sm-8">
                
                            <div class="form-group">
                
                                <div class="form-group">
                
                                    <input type="text" data-role="number" data-name="appno" id="appno" value="<?php echo $appno;?>" class="form-control input-sm" readonly>
                
                                </div>
                
                            </div>
                
                        </div>
                
                    </div>
                
                </div>
                
                <div class="col-sm-12">
                
                    <div class="col-sm-12">
                
                        <div class="col-sm-4">From Date</div>
                
                        <div class="col-sm-8">
                
                            <div class="form-group">
                
                                <input type="text"  data-name="fromdate" data-role="text" data-role="text" class="form-control sm datepicker">
                
                            </div>
                
                        </div>
                
                    </div>
                
                </div>
                
                <div class="col-sm-12">
                
                    <div class="col-sm-12">
                
                        <div class="col-sm-4">To Date</div>
                
                        <div class="col-sm-8">
                
                            <div class="form-group">
                
                                <input type="text"  data-name="todate" data-role="text" class="form-control sm datepicker">
                
                            </div>
                
                        </div>
                
                    </div>
                
                </div>
            
            </div>
        
            <div class="col-sm-6">
        
                <div class="col-sm-12" id="section2">
        
                    <div class="col-sm-2">Leave_Type</div>
            
                    <div class="col-sm-8"> 
            
                        <table class="table">
            
                            <tr>
                                
                                <td width="100"> <input type="radio" data-name="leavetype" name="leavetype" value="PL"    checked> PL</td>
                                
                                <td width="80"><input type="text" data-name="paid_leave" id="paid_leave" size="5"  value="0" readonly class="form-control"></td>
                                
                                <td></td>
                            
                            </tr>
                        
                            <tr>
                                
                                <td><input type="radio" data-name="leavetype" name="leavetype"  value="CL"> CL</td>
                                
                                <td><input type="text" data-name="casual_leave" size="5" id="casual_leave" readonly value="0" class="form-control"> </td>
                                
                                <td></td>
                            
                            </tr>
                            
                            <tr>
                                
                                <td><input type="radio" data-name="leavetype" name="leavetype" value="SL"> SL</td>
                                
                                <td><input type="text" data-name="standanrd_leave" size="5" id="standanrd_leave" readonly value ="0" class="form-control"> </td>
                                
                                <td></td>
                            
                            </tr>
                        
                            <tr>
                                
                                <td><input type="radio" data-name="leavetype" name="leavetype" value="compoff"> Comp. Off</td>
                                
                                <td> <input type="text" data-name="compoff"  size="5"  value ="0" readonly class="form-control">   </td>
                            
                                <td> <input type="text"  data-name="compdate" id="compdate" class="form-control datepicker" > </td>
                        
                            </tr>
                            
                            <tr>
                                
                                <td><input type="radio" data-name="leavetype" name="leavetype" value="nocall"> No Call</td>
                                
                                <td>  </td>
                                
                                <td>  </td>
                        
                            </tr>
                        
                        </table>
                    
                    </div>
                
                </div>
                
            </div>

        </div>
                  
        
        <div style="clear:both;"></div>
        
        <hr class="style-hr">
        
        <div class="col-sm-3"></div>
        
        <div class="col-sm-6">
            
            <div class="col-sm-6">
            
                <button class="btn btn-success btn-sm btn-block" id="btn-submit" style="margin:5px;">Submit</button>
            
            </div>
            
            <div class="col-sm-6">
                
                <button class="btn btn-danger btn-sm btn-block" id="btn-reset" onclick="$('#section').find('select').val('');$('#section').find('input').val('');" style="margin:5px;">Reset</button>
            
            </div>
        
        </div>
        
        <div class="col-sm-3"></div>
        
        <table class="table-list table table-bordered">
            
            <thead>

                <tr>
                    
                    <th>SR. No</th>
                    
                    <th>Branch Name</th>
                    
                    <th>Emp Name</th>
                    
                    <th>Type</th>
                    
                    <th>From Date</th>
                    
                    <th>To Date</th>
                    
                    <th>Leave Type</th>
                    
                    <th>Action</th> 
                
                </tr>

            </thead>

            <tbody>
                
                <?php
                    
                    $count  = 0;

                    if (session_status()==PHP_SESSION_NONE) { session_start(); }

                    $role       = $_SESSION['role'];

                    $created_by = $_SESSION['employeeid'];


                    if ($role == 'admin' || $role == 'Hr') {

                        $query = "SELECT * FROM leaveallotmentmaster ORDER BY id DESC";
                        
                    } else {

                        $query = "SELECT * FROM leaveallotmentmaster WHERE employeeid = '$created_by' ORDER BY id DESC";
                        
                    }

                    $result4 = mysqli_query($con,$query);
                     
                    while($rows3 = mysqli_fetch_assoc($result4)){
                        
                        $count++;

                        $resemp     = mysqli_query($con,"SELECT * FROM employeemaster WHERE employeeid = '".$rows3['employeeid']."'" );
                        
                        $rowsemp    = mysqli_fetch_assoc($resemp);
                        
                        $empname    =  $rowsemp['username'];

                        $branchid   = $rows3['branchid'];
                        
                        $branchname = mysqli_fetch_assoc(mysqli_query($con,"SELECT branchname x FROM branchmaster WHERE branchid='$branchid'"))['x'];

                      
                        $fromdate   = date('d/m/Y',$rows3['fromdate']/1000);
                      
                        $todate     = date('d/m/Y',$rows3['todate']/1000);
                     
                        echo '<tr>';
                     
                            echo '<td align="center">'.$count.'</td>';
                      
                            echo '<td align="center">'.$branchname.'</td>';
                      
                            echo '<td align="center">'.$empname.'</td>';
                    
                            echo '<td align="center">'.$rows3['ltype'].'</td>';
                    
                            echo '<td align="center">'.$fromdate.'</td>';
                    
                            echo '<td align="center">'.$todate.'</td>';
                    
                            echo '<td align="center">'.$rows3['leavetype'].'</td>';

                            if ($rows3['status'] == '') {

                                if ($role == 'admin' || $role == 'Hr') {

                                    echo '<td align="center"><button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#myModal2" data-ptid="'.$rows3['id'].'" onclick="setidtomidal(this)">E</button> <button class="btn btn-success btn-sm " data-id="'.$rows3['id'].'" data-status = "Approved" onclick="approval(this)">A</button> <button class="btn btn-danger btn-sm " data-id="'.$rows3['id'].'" data-status = "Not Approved" onclick="approval(this)">R</button></td>';
                                    
                                } else {
            
                                    echo '<td align="center"><button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#myModal2" data-ptid="'.$rows3['id'].'" onclick="setidtomidal(this)">E</button></td>';
                                    
                                }


                            } else {
                                
                                echo '<td align="center">'.$rows3['status'].'</td>';
                                
                            }
                    
                    
                        echo '<tr>';

                    }
                ?>
            
            </tbody>
        
        </table>

    </div>

</div>

<div id="myModal2" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" align="center">Edit Leave Alloted </h4>
      </div>
      <div class="modal-body">
          <div class="form-group row">
          <input type="hidden" data-name="ptid" id="ptidset">
 

          <div class="col-sm-4">Application No</div>
       <div class="col-sm-8"><input type="text" data-name="appno" data-role="number" id="appno" class="form-control input-sm"></div>

       <div class="col-sm-4">From Date</div>
       
       <div class="col-sm-8">
       <input type="text" data-name="fromdate"  data-role="text" class="form-control input-sm datepicker">
       </div>
       <div class="col-sm-4">To Date</div>
       
       <div class="col-sm-8">
       <input type="text"  data-name="todate"  data-role="text" class="form-control input-sm datepicker">
       </div>
       <div class="col-sm-4">Type</div>
       <div class="col-sm-8"><input type="text" data-name="ltype" data-role="text" id="ltype" class="form-control input-sm"></div>
       <div class="col-sm-4">leavetype</div>
       <div class="col-sm-8"> 
        <select class="form-control input-sm" data-role="select" data-name="leavetype" id="leavetype">
            <option value="Select">Select</option>
            <option value="PL">PL</option>
            <option value="CL">CL</option>
            <option value="SL">SL</option>
            <option value="Comp.Off">Comp. Off</option>
            <option value="No Call">No Call</option>
                         
                    </select>

       </div>
       
       <div class="col-sm-4"></div>
       <div class="col-sm-8"><button class="btn btn-success btn-block btn-sm" onclick="edit()">Save</button></div>
        </div>
        
      </div>
    </div>
  </div>
</div>

<?php
include($base.'_in/footer.php');
?>
<style type="text/css">
    .picker__select--year{
    height:auto;
    }

    .picker__select--month{
    height:auto;
    }
</style>
<script>
    function setidtomidal(e) {
        var ptid = $(e).data('ptid');
        $('#ptidset').val(ptid);
        $.ajax({
                    type: "POST",
                    data: 'ptid='+ptid,
                    url: 'leavesdetails.php',
                    cache: false,
                    success: function (res) {
                    // alert(res.status);
                        if(res.status=='success'){
                            modals.putvalue('myModal2',res.json);
                        }
                    }
                });
    }
    function edit(e){
        var valid=true;
        if(checker('myModal2') !=false ) {
                valid=valid*true;
            }else{
                valid=valid*false;
            }
        if(valid){
            var data1=checker('myModal2');
            data1 = JSON.stringify(data1);

            $.ajax({
                    type: "POST",
                    data: {
                        data1: data1      
                    },
                    url: 'update.php',
                    cache: false,
                    success: function (res) {
                    //alert(res.status);
                        if(res.status=='success'){
                            swal({
                                type: 'success',
                                title: 'Your Work is Save',
                                showConfirmButton: false,
                                timer: 2000
                            });
                            setTimeout(function () {
                                window.location.reload();
                            }, 2000);
                        }else if(res.status=='falid2'){
                            swal({
                                type: 'error',
                                title: 'Unable to Allot Leaves',
                                showConfirmButton: false,
                                timer: 2000
                            });
                            setTimeout(function () {
                                window.location.reload();
                            }, 2000);
                        
                    }
                    }
                });
        }
    }
    $('.datepicker').pickadate({     
        selectYears: 100,
    selectMonths: true,
    onClose: function () {
        try {
        var dateStamp = this.get('select')['pick'];
        var id = this.get('id');
        $('#' + id).attr('data-stamp', dateStamp);
        $('#' + id).parent().find('.timepicker').click();
        } catch (err) {
        console.log(err);
        }
    }
    });
    

    function empmaster(e){    
        var json1=JSON.parse($('#item-json1').val());
        var branchid=$(e).val();
        var str='<option value="Select">Select</option>';
        for(var i in json1){
            if(json1[i].branchid==branchid){
            str+='<option value="'+json1[i].employeeid+'">'+json1[i].username+'</option>';
            }
        }
        $('#employeeid').html(str);
        
        
    }

    function leavemaster(e){    
        var json2=JSON.parse($('#item-json2').val());
        
        
        var json1=JSON.parse($('#item-json1').val());
        var employeeid=$(e).val();
        // console.log(employeeid);

        var str='<option value="Select">Select</option>';
        for(var i in json2){
            if(json2[i].employeeid==employeeid){
                
            $('#paid_leave').val(json2[i].paid_leave);
            $('#casual_leave').val(json2[i].casual_leave);
            $('#standanrd_leave').val(json2[i].standanrd_leave);
            }
        }
    

        for(var i in json1){
            if(json1[i].employeeid==employeeid){
            $('#ltype').val(json1[i].salaryper);
                
            }
        }
        
    }
    
    $('#btn-submit').on('click',function(){
        var valid=true;
        if(checker('section') !=false ) {
                valid=valid*true;
            }else{
                valid=valid*false;
            }
        if(valid){
            var data1=checker('section');
            data1 = JSON.stringify(data1);
            $.ajax({
                    type: "POST",
                    data: {
                        data: data1      
                    },
                    url: 'insert.php',
                    cache: false,
                    success: function (res) {

                    if(res.status=='success'){
                            swal({
                                type: 'success',
                                title: 'Your Work is Save',
                                showConfirmButton: false,
                                timer: 2000
                            });
                            setTimeout(function () {
                                window.location.reload();
                            }, 2000);
                        
                    }else if(res.status=='falid2'){
                            swal({
                                type: 'error',
                                title: 'Unable to Allot Leaves',
                                showConfirmButton: false,
                                timer: 2000
                            });
                            setTimeout(function () {
                                window.location.reload();
                            }, 2000);
                        
                    }
                        
                    }
                });
        }
    })

    function getempid(e){
        
        var empid = $(e).val();
        $('#empcode').val(empid);
            
    }


    function approval(e) {

        let id     = $(e).attr('data-id');

        let status = $(e).attr('data-status');

        if (id != '' && status != '') {

            $.ajax({

                type: "POST",
                
                data: "id=" + id + " &status= " + status,
            
                url: 'leaveallotorreject.php',
                    
                cache: false,
                
                success: function (res) {

                    if(res.status=='success'){
                        
                        swal({
                            type: 'success',
                            
                            title: 'Your Work is Save',
                            
                            showConfirmButton: false,
                            
                            timer: 2000
                        
                        });

                        setTimeout(function () {
                            
                            window.location.reload();
                        }, 2000);

                    }
                
                }
            
            })
            
        } else {

        }
  
    }

</script>