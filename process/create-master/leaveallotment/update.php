<?php
    
    $base = '../../../';
    
    include($base.'_in/connect.php');
    
    header('content-type: application/json; charset=utf-8');
    
    header("access-control-allow-origin: *");
    
    if(isset($_POST['data1']) ){
      
        $con        = _connect();

        if (session_status()==PHP_SESSION_NONE) {session_start();}

        $created_by = $_SESSION['employeeid'];

        $role       = $_SESSION['role'];

        $data1      = $_POST['data1'];
        
        $ptid       = get_object_vars(json_decode($_POST['data1']))['ptid'];
        
        $fromdate   = get_object_vars(json_decode($_POST['data1']))['fromdate'];
        
        $todate     = get_object_vars(json_decode($_POST['data1']))['todate'];
        
        $appno      = get_object_vars(json_decode($_POST['data1']))['appno'];
        
        $ltype      = get_object_vars(json_decode($_POST['data1']))['ltype'];
        
        $leavetype  = get_object_vars(json_decode($_POST['data1']))['leavetype'];
         
        $newnumDays = (abs($fromdate - $todate)/60/60/24/1000)+1;

        $slelect    = mysqli_fetch_assoc(mysqli_query($con,"SELECT * FROM leaveallotmentmaster WHERE id='$ptid'")); 
         
        $fromdate1  = $slelect['fromdate'];
        
        $todate1    = $slelect['todate'];
        
        $leavetype1 = $slelect['leavetype'];
        
        $employeeid = $slelect['employeeid'];
        
        $oldnumDays = (abs($fromdate1 - $todate1)/60/60/24/1000)+1; 
            
        $lno        = 0;
              
        if($leavetype1 =="PL") {

            $lno = mysqli_fetch_assoc(mysqli_query($con,"SELECT paid_leave x FROM leaveopeningbalance WHERE employeeid='$employeeid'"))['x'];
            
            $qry = "paid_leave = paid_leave";
                
        } else if($leavetype1 =="CL") {
                    
            $lno = mysqli_fetch_assoc(mysqli_query($con,"SELECT casual_leave x FROM leaveopeningbalance WHERE employeeid='$employeeid'"))['x'];
                   
            $qry = "casual_leave = casual_leave ";
        
        } else if($leavetype1 =="SL") { 

            $lno = mysqli_fetch_assoc(mysqli_query($con,"SELECT standanrd_leave x FROM leaveopeningbalance WHERE employeeid='$employeeid'"))['x'];
                    
            $qry = "standanrd_leave = standanrd_leave"; 
                
        }

        if($lno < $newnumDays){
                    
            echo '{"status":"falid2"}';

                
        } else {

            if ($role == 'admin' || $role == 'Hr') {

                $update = mysqli_query($con, "UPDATE leaveopeningbalance SET ". $qry ."+ ".$oldnumDays." WHERE employeeid = '$employeeid'");
                     
                $update = mysqli_query($con, "UPDATE leaveopeningbalance SET ". $qry ."- ".$newnumDays." WHERE employeeid = '$employeeid'");
                        
                $create = mysqli_query($con,"UPDATE leaveallotmentmaster SET fromdate='$fromdate',todate='$todate' ,appno='$appno' ,ltype='$ltype',leavetype='$leavetype', status = 'Approved',approved_by = '$created_by', approved_time='$CURRENT_MILLIS' WHERE id='$ptid'");
                        
                if($create){

                    echo '{"status":"success"}';
                            
                }else{
                                
                    echo '{"status":"falid1"}';
                            
                }

            } else {

                $create = mysqli_query($con,"UPDATE leaveallotmentmaster SET fromdate='$fromdate',todate='$todate' ,appno='$appno' ,ltype='$ltype',leavetype='$leavetype' WHERE id='$ptid'");
                        
                if($create){

                    echo '{"status":"success"}';
                            
                } else {
                                
                    echo '{"status":"falid1"}';
                            
                }

            }
                   
        }
                 
        _close($con);

    } else {
        
        echo '{"status":"falid"}';
     
    }

?>