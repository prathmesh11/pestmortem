<?php 
    
    $base='../../../';
    
    include($base.'_in/connect.php');
    
    header('content-type: application/json; charset=utf-8');
    
    header("access-control-allow-origin: *");
    
    if(isset($_POST['id']) && isset($_POST['status'])) {
    
        if (session_status()==PHP_SESSION_NONE) { session_start(); }

        $created_by = $_SESSION['employeeid'];
        
        $con        = _connect();
        
        $id         = _clean($con, $_POST['id']);
        
        $status     = _clean($con, $_POST['status']);

        if ($status = 'Not Approved') {

            $updateStatus = mysqli_query($con, "UPDATE leaveallotmentmaster SET status = '$status' , rejected_by = '$created_by', rejected_time = '$CURRENT_MILLIS' WHERE id = '$id'");

            if($updateStatus) {

                echo '{"status":"success"}';

            } else {

                echo '{"status":"falid1"}';

            }
            
        } else {

            $leaveDetails = mysqli_fetch_assoc(mysqli_query($con,"SELECT * FROM leaveallotmentmaster WHERE id='$id'"));

            $employeeid   = $leaveDetails['employeeid'];

            $leavetype    = $leaveDetails['leavetype'];

            $fromdate     = $leaveDetails['fromdate'];

            $todate       = $leaveDetails['todate'];

            $numDays      = (abs($fromdate - $todate)/60/60/24/1000)+1;

            $lno          = 0;

            if($leavetype =="PL") {

                $lno = mysqli_fetch_assoc(mysqli_query($con,"SELECT paid_leave x FROM leaveopeningbalance WHERE employeeid='$employeeid'"))['x'];
                
                $qry = "paid_leave = paid_leave";
        
            } else if($leavetype =="CL") {
    
                $lno = mysqli_fetch_assoc(mysqli_query($con,"SELECT casual_leave x FROM leaveopeningbalance WHERE employeeid='$employeeid'"))['x'];
                
                $qry = "casual_leave = casual_leave ";
        
    
            } else if($leavetype =="SL") { 
    
                $lno = mysqli_fetch_assoc(mysqli_query($con,"SELECT standanrd_leave x FROM leaveopeningbalance WHERE employeeid='$employeeid'"))['x'];
                
                $qry = "standanrd_leave = standanrd_leave"; 
        
            }
    
            if($lno < $numDays){
    
                echo '{"status":"falid2"}';
    
            } else {
    
                $updateStatus = mysqli_query($con, "UPDATE leaveallotmentmaster SET status = '$status' , approved_by = '$created_by', approved_time = '$CURRENT_MILLIS' WHERE id = '$id'");
                    
                if($updateStatus) {

                    mysqli_query($con, "UPDATE leaveopeningbalance SET ". $qry ."- ".$numDays." WHERE employeeid = '$employeeid'");
    
                    echo '{"status":"success"}';
    
                } else {
    
                    echo '{"status":"falid1"}';
    
                }
            
            }
            
        }
                
        _close($con);
        
        echo trim(ob_get_clean());
        
    } else {

        echo '{"status":"falid3"}';

    }
?>