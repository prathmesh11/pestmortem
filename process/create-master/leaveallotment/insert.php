<?php 
    
    $base = '../../../';

    include($base.'_in/connect.php');

    header('content-type: application/json; charset=utf-8');

    header("access-control-allow-origin: *");

    if(isset($_POST['data'])) {

        if (session_status()==PHP_SESSION_NONE) {session_start();}

        $created_by = $_SESSION['employeeid'];

        $role       = $_SESSION['role'];

        $con        = _connect();
        
        $data       = get_object_vars(json_decode($_POST["data"]));
        
        $fromdate   = $data['fromdate'];
        
        $todate     = $data['todate']; 
        
        $branchid   = _clean($con, $data['branchid']);
        
        $employeeid = _clean($con, $data['employeeid']);
        
        $appno      = _clean($con, $data['appno']);
        
        $ltype      = _clean($con, $data['ltype']);
        
        $leavetype  = _clean($con, $data['leavetype']);
        
        $numDays    = (abs($fromdate - $todate)/60/60/24/1000)+1;
        
        $lno        = 0;
        
        if($leavetype =="PL") {
            
            $lno = mysqli_fetch_assoc(mysqli_query($con,"SELECT paid_leave x FROM leaveopeningbalance WHERE employeeid='$employeeid'"))['x'];
            
            $qry = "paid_leave = paid_leave";
        
        } else if($leavetype =="CL") {
            
            $lno = mysqli_fetch_assoc(mysqli_query($con,"SELECT casual_leave x FROM leaveopeningbalance WHERE employeeid='$employeeid'"))['x'];
            
            $qry = "casual_leave = casual_leave ";
        
        } else if($leavetype =="SL") { 
            
            $lno = mysqli_fetch_assoc(mysqli_query($con,"SELECT standanrd_leave x FROM leaveopeningbalance WHERE employeeid='$employeeid'"))['x'];
            
            $qry = "standanrd_leave = standanrd_leave"; 
        
        }

        if($lno < $numDays){

            echo '{"status":"falid2"}';

        } else {

            
            if ($role == 'admin' || $role == 'Hr') {

                $create=mysqli_query($con, "INSERT INTO leaveallotmentmaster (branchid,employeeid,appno,fromdate,todate,ltype,leavetype,created_by,created_time,status,approved_by,approved_time) VALUES ('$branchid','$employeeid','$appno','$fromdate','$todate','$ltype','$leavetype','$created_by' ,'$CURRENT_MILLIS','Approved','$created_by' ,'$CURRENT_MILLIS')");

                $update = mysqli_query($con, "UPDATE leaveopeningbalance SET ". $qry ."- ".$numDays." WHERE employeeid = '$employeeid'");
                
                if($update) {

                    echo '{"status":"success"}';

                } else {
    
                    echo '{"status":"falid1"}';
                
                }

                
            } else {

                $create = mysqli_query($con, "INSERT INTO leaveallotmentmaster (branchid,employeeid,appno,fromdate,todate,ltype,leavetype,created_by,created_time) VALUES ('$branchid','$employeeid','$appno','$fromdate','$todate','$ltype','$leavetype','$created_by' ,'$CURRENT_MILLIS')");

                if($create) {

                    echo '{"status":"success"}';

                } else {
    
                    echo '{"status":"falid1"}';
                
                }

                
            }      
        
        }
            
        _close($con);
        
        echo trim(ob_get_clean());

    } else {
    
        echo '{"status":"falid3"}';

    }

?>