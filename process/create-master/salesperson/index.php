<?php

    $base = '../../../';

    $js='<script src="'.$base.'js/fwork.js"></script>
    <script src="'.$base.'js/alart.js"></script>
    <script src="'.$base.'js/list.min.js"></script>';

    $css='<link rel="stylesheet" href="'.$base.'css/fwork.css">
    <link rel="stylesheet" href="'.$base.'css/grid.min.css">
    <link rel="stylesheet" href="'.$base.'css/alart.css">';

    include($base.'_in/header.php');

    include($base.'_in/connect.php');

    if (!in_array("Sales Person Master", $arrysession)) {
   
        echo "<script>window.location.href='/process/dashboard.php';</script>";
       
        exit;
    
    }

    $con = _connect();

?>

<style>

    hr.style-hr {
        border: 0;
        height: 1px;
        background-image: linear-gradient(to right, rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.75), rgba(0, 0, 0, 0));
    }

    th {
        background: #8e44ad;
        color: #fff;
        text-align: center;
        padding-top: 2px;
        padding-bottom: 2px;
        border: 1px solid #fff;
    }

    .sidenav1 {
        height: 100%;
        width: 0;
        position: fixed;
        z-index: 1;
        top: 0;
        left: 0;
        background-color: #111;
        overflow-x: hidden;
        transition: 0.5s;
        padding-top: 60px;
    }

    .sidenav1 a {
        padding: 8px 8px 8px 32px;
        text-decoration: none;
        font-size: 25px;
        color: #818181;
        display: block;
        transition: 0.3s;
    }

    .sidenav1 a:hover {
        color: #f1f1f1;
    }

    .sidenav1 .closebtn1 {
        position: absolute;

        right: 25px;
        font-size: 36px;
        margin-left: 50px;
    }

    @media screen and (max-height: 450px) {
        .sidenav1 {
            padding-top: 15px;
        }

        .sidenav1 a {
            font-size: 18px;
        }
    }

    .list {
        padding: 0px;
    }

    .list li span {
        display: inline;
    }

    .list li {
        text-decoration: none;
        display: block;
        padding: 2px;
        background: #FFF;
        border: 1px solid #333;
        color: #000;
    }

</style>

<div class="container-fluid">

    <div class=" content" id="section">

        <input type="hidden" id="salespersonid" data-name="salespersonid" />

        <span style="font-size:24px;cursor:pointer" onclick="openNav1()">&#9776; Search</span>

        <div id="mysidenav1" class="sidenav1">

            <div id="test-list" style="padding:5px;">

                <a href="javascript:void(0)" class="closebtn1" onclick="closeNav1()">&times;</a>

                <br><br><br>
                
                <div class="form-group">

                    <input type="text" placeholder="Search" class="form-control input-sm fuzzy-search" style="border-radius:3px;">

                </div>

                <ul class="list">

                    <?php 

                        $result1=mysqli_query($con,"SELECT *  FROM salesperson ORDER BY sales_person_name");

                        while($rows=mysqli_fetch_assoc($result1)) {

                            echo '<li><span class="salesperson">' .$rows['sales_person_name']. '<span><button style="margin-left:2px;" data-salespersonid="' .$rows['id']. '" onclick="edit(this)" class="btn btn-primary btn-sm"><span class="fa fa-edit"></span> E</button></i>';

                        }

                    ?>

                </ul>

            </div>

        </div>

        <h2 align="center" style="margin:0;">Sales Person Master</h2>

        <hr class="style-hr">

        <div class="row" id="section1">
         
            <div class="col-sm-3"></div>

            <div class="col-sm-6">

                <div class="col-sm-4">Sales Person Name</div>

                <div class="col-sm-8">

                    <div class="form-group">

                        <input type="text" data-role="text" data-name="sales_person_name" class="form-control input-sm">

                    </div>

                </div>

            </div>

            <div class="col-sm-3"></div>
            
        </div>

        <div style="clear:both;"></div>

        <hr class="style-hr">

        <div class="col-sm-3"></div>

        <div class="col-sm-6">

            <div class="col-sm-6">
                
                <button class="btn btn-success btn-sm btn-block" id="btn-submit" style="margin:5px;" onclick="insert()">Submit</button>
            
            </div>

            <div class="col-sm-6">
                
                <button class="btn btn-danger btn-sm btn-block" id="btn-reset" style="margin:5px;">Reset</button>
            
            </div>
            
        </div>

        <div class="col-sm-3"></div>

    </div>

</div>

<?php

    include($base.'_in/footer.php');

?>
<script>

function openNav1() {

    document.getElementById("mysidenav1").style.width = "300px";

}

function closeNav1() {

    document.getElementById("mysidenav1").style.width = "0";

}

var monkeyList = new List('test-list', {

    valueNames: ['branchmaster']
    
});

$('#btn-reset').on('click', function () {

    window.location.reload();

})


function insert() {

    var valid = true;

    if (checker('section') != false) {

        valid = valid * true;

    } else {

        valid = valid * false;

    }

    if (valid) {

        var data1 = checker('section');

        data1 = JSON.stringify(data1);

        $.ajax({

            type: "POST",

            data: {
                data: data1
            },

            url: 'insert.php',

            cache: false,

            success: function (res) {

                swal({

                    type: 'success',
                    title: 'Sales Person is Save',
                    showConfirmButton: false,
                    timer: 2000

                });

                setTimeout(function () {

                    window.location.reload();

                }, 2000);

            }

        });

    }

}


function edit(e) {

    var salespersonid = $(e).data('salespersonid');

    $.ajax({

        type: "POST",

        data: 'salespersonid=' + salespersonid,

        url: 'select.php',

        cache: false,

        success: function (res) {

            if (res.status == 'success') {

                $("#btn-submit").attr('onclick', 'update()');

                $("#btn-submit").text('Update');

                closeNav1();

                $('#salespersonid').val(salespersonid);

                modals.putvalue('section', res.json);

            }

        }

    });

}

function update() {

    var valid = true;

    if (checker('section') != false) {

        valid = valid * true;

    } else {

        valid = valid * false;

    }

    if (valid) {

        var data = checker('section');

        data = JSON.stringify(data);

        $.ajax({

            type: "POST",

            data: {
                data: data,
            },

            url: 'update.php',

            cache: false,

            success: function (res) {
                if (res.status == 'success') {

                    swal({

                        type: 'success',
                        title: 'Sales Person Name is Update',
                        showConfirmButton: false,
                        timer: 2000

                    });

                    setTimeout(function () {

                        window.location.reload();

                    }, 2000);

                }

            }

        });

    }
    
}

</script>