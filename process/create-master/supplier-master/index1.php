<?php
   
   $base = '../../';
  
   $navenq1 = 'background:#1B1464;';

   include('header.php');

    $scode = '';

    if(isset($_GET['scode'])){

        $scode = $_GET['scode'];

    }

    if (!in_array("Supplier Master", $arrysession)) {
   
        echo "<script>window.location.href='/process/dashboard.php';</script>";
       
        exit;
    
    }
 

    $con = _connect();

?>

<style>

    hr.style-hr {
        border: 0;
        height: 1px;
        background-image: linear-gradient(to right, rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.75), rgba(0, 0, 0, 0));
    }
    
</style>

    <br>

<center><b>PEST MORTEM INDIA VENDOR REGISTRATION FORM</b></center>

<hr class="style-hr">

    <div class="row" id="section">

        <input type="hidden" data-name='scode' class="form-control input-sm">

        <div class="col-sm-6">

            <div class="col-sm-4">Name Of the Entity/Firm</div>

            <div class="col-sm-8">

                <div class="form-group">
                        
                    <input type="text" data-role='text' data-name='suppliername' class="form-control input-sm">

                    <input type="text" id='supplierid' style="display:none;">

                </div>

            </div>

            <div class="col-sm-4">Address</div>

            <div class="col-sm-8">

                <div class="form-group">
                        
                    <textarea data-role='textarea' data-name='address' class="form-control input-sm"> </textarea>

                </div>

            </div>

            <div class="col-sm-4">Legal Status Of Party</div>

            <div class="col-sm-8">

                <div class="form-group">
                    
                    <select class="form-control input-sm" data-role="select" id="legalStatus" data-name="legalStatus">

                        <option value="Select">Select Legal Status Of Party</option>

                        <option value="Public Ltd">Public Ltd</option>
                        
                        <option value="Pvt Ltd">Pvt Ltd</option>
                        
                        <option value="OPC">OPC</option>
                        
                        <option value="LLP">LLP</option>
                        
                        <option value="Partnership">Partnership</option>
                        
                        <option value="Proprietorship">Proprietorship</option>

                        <option value="Other">Other</option>

                    </select>

                </div>

            </div>

            <div class="col-sm-4">Nature Of the Supplier</div>

            <div class="col-sm-8">

                <div class="form-group">

                    <textarea data-role='textarea' data-name='natureofsupplier' class="form-control input-sm"> </textarea>

                </div>

            </div>

        </div>

        <div class="col-sm-6">

            <div class="col-sm-2"> City</div>

            <div class="col-sm-4">

                <div class="form-group">

                    <input type="text" data-role='text' data-name='city' class="form-control input-sm">
                
                </div>

            </div>

            <div class="col-sm-2"> State</div>

            <div class="col-sm-4">
                                
                <div class="form-group">

                    <select class="form-control input-sm select-js" data-role="select" id="state" data-name="state">

                        <option value="Select">Select State</option>

                        <?php 

                            $result1=mysqli_query($con, "SELECT * FROM `statemaster` ORDER BY statename");
                                
                            while($rows=mysqli_fetch_assoc($result1)) {
                                    
                                echo '<option value="'.$rows['statecode'].'">'.$rows['statename'].'</option>';

                            }

                        ?>

                    </select>
                 
                </div> 
                

            </div>

            <div style="clear: both;"></div>

            <div class="col-sm-2">Phone No</div>

            <div class="col-sm-4">

                <div class="form-group">

                    <input type="text" data-role='text' data-name='phone' class="form-control input-sm">

                </div>   

            </div>

            <div class="col-sm-2">Pin Code</div>

            <div class="col-sm-4">

                <div class="form-group">

                    <input type="text" data-role='text' data-name='pincode' class="form-control input-sm">
                
                </div>

            </div>

            <div class="col-sm-2">Email Id</div>

            <div class="col-sm-4">

                <div class="form-group">

                    <input type="text" data-role='text' data-name='emailid' class="form-control input-sm">

                </div>   

            </div>

            <div class="col-sm-2">Fax No</div>

            <div class="col-sm-4">

                <div class="form-group">

                    <input type="text" data-role='text' data-name='faxno' class="form-control input-sm">
                
                </div>

            </div>

            <div class="col-sm-3">Whether the party has relationship with any of the PMI's Personnel. Pls Provide details</div>

            <div class="col-sm-9">

                <div class="form-group">

                    <textarea data-role='textarea' data-name='relationship' class="form-control input-sm"> </textarea>

                </div>

            </div>

        </div>

        <div class= "col-sm-12">

            <div class="col-sm-3">
                                
                <div class="form-group" id="test-list1">

                    <label for=""> Supplier Group</label>

                    <input type="text" data-role='text' placeholder="Search" class="form-control input-sm fuzzy-search"
                        data-name='suppliergroupcode' id="suppliergroupcode" style="border-radius:4px;">

                        <div id="searchbox" style="border:1px solid #74b9ff; height:60px; overflow-y: scroll;display:none;">

                        <ul class="list list1" style="list-style-type: none;padding-left:0px;margin-bottom:0;">

                            <?php 

                                $result1 = mysqli_query($con, "SELECT DISTINCT suppliergroupcode FROM `suppliermaster`");

                                while($rows=mysqli_fetch_assoc($result1)) {

                                    echo '<li style="padding-left:15px;" onclick="jump(this)"><span class="suppliergroupcode">' .$rows['suppliergroupcode']. '<span></i>';

                                }

                            ?>

                        </ul>

                    </div>

                </div>

            </div>

            <div class="col-sm-3">

                <div class="form-group">
                    
                    <label for=""> Credit days</label>

                    <input type="number" data-role='numnber' data-name='creditdays' value=30 class="form-control input-sm">

                </div>

            </div>

            <div class="col-sm-3">

                <div class="form-group">
                    
                    <label for=""> Opening Balance</label>

                    <input type="number" data-name='opening_balance' value="0" class="form-control input-sm">

                </div>

            </div>

            <div class="col-sm-3">

                <div class="form-group">
                    
                    <label for=""> Opening Balance Date</label>

                    <input type="date" data-name='opening_date' value="<?php echo $today;?>" class="form-control input-sm">

                </div>

            </div>
        
        </div>

    </div> 

    <hr class="style-hr">

    <center><b>STATUTORY DETAILS</b></center>

    <hr class="style-hr">
    
    <div class="row" id="section">

        <div class="col-sm-12">

            <div class="col-sm-2"> Pan No</div>

            <div class="col-sm-3">

                <div class="form-group">

                    <input type="text" data-role='text' data-name='panno' class="form-control input-sm">
                
                </div>

            </div>

            <div class="col-sm-7">(Mandatory if TDS is applicable)</div>

            <div style="clear: both;"></div>

            <div class="col-sm-2"> Type of Service/Product</div>

             <div class="col-sm-3">

                <div class="form-group">

                    <input type="text" data-role='text' data-name='service1' class="form-control input-sm">
                
                </div>

            </div>

            <div class="col-sm-3">

                <div class="form-group">

                    <input type="text" data-role='text' data-name='service2' class="form-control input-sm">
                
                </div>

            </div>

            <div class="col-sm-3">

                <div class="form-group">

                    <input type="text" data-role='text' data-name='service3' class="form-control input-sm">
                
                </div>

            </div>

            <div class="col-sm-1"></div>

            <div class="col-sm-2"> With Tax Type</div>

            <div class="col-sm-3">

                <div class="form-group">

                    <input type="text" data-role='text' data-name='taxtype1' class="form-control input-sm">
                
                </div>

            </div>

            <div class="col-sm-3">

                <div class="form-group">

                    <input type="text" data-role='text' data-name='taxtype2' class="form-control input-sm">
                
                </div>

            </div>

            <div class="col-sm-3">

                <div class="form-group">

                    <input type="text" data-role='text' data-name='taxtype3' class="form-control input-sm">
                
                </div>

            </div>

            <div class="col-sm-1"></div>

            <div class="col-sm-2"> With Hold Tax Code</div>

             <div class="col-sm-3">

                <div class="form-group">

                    <input type="text" data-role='text' data-name='taxcode1' class="form-control input-sm">
                
                </div>

            </div>

            <div class="col-sm-3">

                <div class="form-group">

                    <input type="text" data-role='text' data-name='taxcode2' class="form-control input-sm">
                
                </div>

            </div>

            <div class="col-sm-3">

                <div class="form-group">

                    <input type="text" data-role='text' data-name='taxcode3' class="form-control input-sm">
                
                </div>

            </div>

            <div class="col-sm-1"></div>

            <div class="col-sm-2"> Gst No</div>

            <div class="col-sm-3">

                <div class="form-group">

                    <input type="text" data-role='text' data-name='gstnumber' class="form-control input-sm">
                
                </div>

            </div>

            <div class="col-sm-1"> Payment_Term</div>

            <div class="col-sm-2">

                <div class="form-group">

                    <select class="form-control input-sm" data-role="select" data-name="paymentTerms">

                        <option value="Select">Select</option>

                        <option value="Cash">Cash</option>

                        <option value="Bank">Bank</option>

                    </select>

                </div>

            </div>

            <div class="col-sm-1"> Freight</div>
            
            <div class="col-sm-2">

                <div class="form-group">

                    <input type="text" data-role='text' data-name='freight' class="form-control input-sm">
                
                </div>

            </div>

            <div class="col-sm-1"></div>

            <div class="col-sm-2"> MSM Act</div>
            
            <div class="col-sm-3">

                <div class="form-group">

                    <input type="text" data-role='text' data-name='msmact' class="form-control input-sm">
                
                </div>

            </div>

            <div class="col-sm-1"> MSM_Date</div>
            
            <div class="col-sm-2">

                <div class="form-group">

                    <input type="date" data-role='date' data-name='msmdate' class="form-control input-sm">
                
                </div>

            </div>

            <div style="clear: both;"></div>

        </div>

    </div>

    <hr class="style-hr">

    <center><b>BANK DETAILS</b></center>

    <hr class="style-hr">

    <div class="row" id="section">

        <div class="col-sm-12">

            <div class="col-sm-2"> Bank A/c Holder's Name</div>

            <div class="col-sm-4">
                
                <div class="form-group">
                            
                    <input type="text" data-role='text' data-name='acountholdername' class="form-control input-sm">

                </div>

            </div>

            <div style="clear: both;"></div>

            <div class="col-sm-2"> Bank Name</div>

            <div class="col-sm-4">
                
                <div class="form-group">
                            
                    <input type="text" data-role='text' data-name='bankname' class="form-control input-sm">

                </div>

            </div>

            <div class="col-sm-1"> Branch Name</div>

            <div class="col-sm-4">
                
                <div class="form-group">
                            
                    <input type="text" data-role='text' data-name='branchname' class="form-control input-sm">

                </div>

            </div>

            <div style="clear: both;"></div>

            <div class="col-sm-2"> Account No</div>

            <div class="col-sm-4">
                
                <div class="form-group">
                            
                    <input type="text" data-role='text' data-name='accountno' class="form-control input-sm">

                </div>

            </div>

            <div class="col-sm-1"> A/C Type</div>

            <div class="col-sm-4">
                
                <div class="form-group">
                            
                    <input type="text" data-role='text' data-name='actype' class="form-control input-sm">

                </div>

            </div>

            <div style="clear: both;"></div>

            <div class="col-sm-2"> IFSC Code</div>

            <div class="col-sm-4">
                
                <div class="form-group">
                            
                    <input type="text" data-role='text' data-name='ifsccode' class="form-control input-sm">

                </div>

            </div>

            <div class="col-sm-1"> Branch Code</div>

            <div class="col-sm-4">
                
                <div class="form-group">
                            
                    <input type="text" data-role='text' data-name='branchcode' class="form-control input-sm">

                </div>

            </div>

            <div style="clear: both;"></div>

            <div class="col-sm-2"> Address of the Bank</div>

            <div class="col-sm-4">
                
                <div class="form-group">
                            
                    <textarea data-role='textarea' data-name='addressbank' class="form-control input-sm"> </textarea>

                </div>

            </div>

            <div class="col-sm-1"> Branch City</div>

            <div class="col-sm-4">
                
                <div class="form-group">
                            
                    <input type="text" data-role='text' data-name='branchcity' class="form-control input-sm">

                </div>

            </div>

            <div style="clear: both;"></div>

            <div class="col-sm-2"> Brach State</div>

            <div class="col-sm-4">
                
                <div class="form-group">
                            
                    <select class="form-control input-sm select-js" data-role="select" id="branchstate" data-name="branchstate">

                        <option value="Select">Select Brach State</option>

                        <?php 

                            $result1=mysqli_query($con, "SELECT * FROM `statemaster` ORDER BY statename");
                                
                            while($rows=mysqli_fetch_assoc($result1)) {
                                    
                                echo '<option value="'.$rows['statecode'].'">'.$rows['statename'].'</option>';

                            }

                        ?>

                    </select>

                </div>

            </div>

            <div class="col-sm-1"> Payment Method</div>

            <div class="col-sm-4">
                
                <div class="form-group">
                            
                    <input type="text" data-role='text' data-name='paymentmethod' class="form-control input-sm">

                </div>

            </div>

            <div style="clear: both;"></div>

        </div>

    </div>

    <button class="btn btn-success btn-sm"  id="btn-submit" onclick="submit()" style="position: fixed;bottom: 80px;right: 10px;width: 120px;">Submit</button>
    
    <button class="btn btn-danger btn-sm btn-block" id="btn-reset" style="position: fixed;bottom: 40px;right: 10px;width: 120px;">Reset</button>

<?php

    include('footer.php');

?>

<script>

function jump(e) {

    var suppliergroupcode = $(e).find('span').text().trim();

    $('#suppliergroupcode').val(suppliergroupcode);

    $('#searchbox').hide();

}

$('#suppliergroupcode').keypress(function (e) {

    if (e.which == 13) {

        $('#searchbox').hide();

    } else {

        $('#searchbox').show();

    }

});

$(document).ready(function() {

    if ('<?php echo $_GET['edit'] == 'true' || $_GET['view'] == 'true' &&  $_GET['supplierid'] != '' && $_GET['branchid'] != ''?>') {
        
        edit();
    }

});


//---------------------------------Insert-----------------------------------//

function submit() {

    var valid = true;

    if (checker('section') != false) {

        valid = valid * true;

    } else {

        valid = valid * false;

    }

    if (valid) {

        var data     = checker('section');

        var datastr  = JSON.stringify(data);

        $.ajax({

            type: "POST",

            data: {
                data: datastr
            },

            url: 'insert1.php',

            cache: false,

            success: function (res) {

                if (res.status == 'success') {

                    swal({

                        type: 'success',
                        title: 'Supplier Master Created',
                        showConfirmButton: false,
                        timer: 3000

                    });

                    setTimeout(function () {

                        location.href = 'supplier.php';

                    }, 3000);

                }

                if (res.status == 'falid11') {

                    swal('GST Number Invalid', '', 'error');

                }

                if (res.status == 'falid22') {

                    swal('GST Number Already Exist', '', 'error');

                }

                if (res.status == 'falid1' || res.status == 'falid1') {

                    swal('Something Went Wrong', '', 'error');

                }

            }

        });

    }

}


//-----------------------------Edit----------------------------//

function edit() {

    var supplierid = '<?php echo $_GET['supplierid']; ?>';

    var branchid   = '<?php echo $_GET['branchid']; ?>';

    $.ajax({

        type: "POST",

        data: 'supplierid=' + supplierid + '&branchid=' + branchid ,

        url: 'select1.php',

        cache: false,

        success: function (res) {

            if (res.status == 'success') {
                
                $('#btn-submit').attr('onclick', 'update()');

                $('#btn-submit').text('Update');

                 if ('<?php echo $_GET['view']; ?>') {
        
                    $('#btn-submit').hide();
                   
                    $('#btn-reset').hide();

                }
                                
                modals.putvalue('section', res.json);

            }

        }

    });

}


function update() {

    var valid = true;

    if (checker('section') != false) {

        valid = valid * true;

    } else {

        valid = valid * false;

    }


    if (valid) {

        var data     = checker('section');

        var datastr  = JSON.stringify(data);

        $.ajax({

            type: "POST",

            data: {
                data: datastr
            },

            url: 'update1.php',

            cache: false,

            success: function (res) {

                if (res.status == 'success') {

                    swal({

                        type: 'success',
                        title: 'Supplier Master Updated',
                        showConfirmButton: false,
                        timer: 3000

                    });

                    setTimeout(function () {

                        location.href = 'supplier.php';

                    }, 3000);

                }

                if (res.status == 'falid11') {

                    swal('GST Number Invalid', '', 'error');

                }

                if (res.status == 'falid1' || res.status == 'falid1') {

                    swal('Something Went Wrong', '', 'error');
                    
                }

            }
            
        });

    }

}

//-------------------------------Button Reset----------------------------//

// $('#btn-reset').on('click', function () {

//     window.location.reload();

// });

$('#btn-reset').on('click', function () {
        
    $(':input').not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');

    $("#state").select2("val", "Select");

    $("#branchstate").select2("val", "Select");

});


</script>