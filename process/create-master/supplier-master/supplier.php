<?php 
    
    $base    = '../../';
    $navenq1 = 'background:#1B1464;';

    include('header.php');

    if (!in_array("Supplier Master", $arrysession)) {
   
        echo "<script>window.location.href='/process/dashboard.php';</script>";
       
        exit;
    
    }

  if (session_status() == PHP_SESSION_NONE) { session_start(); }

  $sessionby = $_SESSION['employeeid'];
  
  $branchid  = $_SESSION['branchid'];

  $signature = mysqli_fetch_assoc(mysqli_query($con,"SELECT signature x FROM branchmaster WHERE branchid='$branchid'"))['x'];



?>

<style>
.clr {
        clear:both;
    }
</style>

<br>
<img style="display:none;" id=imageid src="<?php echo $base;?>img/PestMortemLogo.jpg">

<input type="hidden" id=branchid value="<?php echo $branchid;?>">

<img style="display:none;" id="signature" src="<?php echo $base.$signature;?>">

<div class="table-ui container-fluid">

    <div class="tr row">

        <div class="col-sm-2 th" style="word-wrap:break-word;">Supplier Name</div>

        <div class="col-sm-2 th">Phone No</div>

        <div class="col-sm-2 th">City</div>

        <div class="col-sm-2 th">State</div>

        <div class="col-sm-2 th">Email Id</div>

        <div class="col-sm-2 th">Action</div>

    </div>

    <div class="row tr">

      <?php

        $result = mysqli_query($con,"SELECT * FROM  suppliermaster WHERE branchid ='$branchid' ORDER BY id DESC");

        while ($row = mysqli_fetch_assoc($result)) {

          $state     = $row['state'];

          $statename  = mysqli_fetch_assoc(mysqli_query($con,"SELECT statename x FROM statemaster WHERE id='$state'"))['x'];


      ?>

        <div class="col-sm-2 td" style="word-wrap:break-word;">

          <?php echo $row['suppliername'];?>

        </div>

        <div class="col-sm-2 td" style="word-wrap:break-word;">

          <?php echo $row['phone'];?>

        </div>

        <div class="col-sm-2 td" style="word-wrap:break-word;">

          <?php echo $row['city'];?>

        </div>

        <div class="col-sm-2 td" style="word-wrap:break-word;">

          <?php echo $statename; ?>

        </div>

        <div class="col-sm-2 td" style="word-wrap:break-word;">

          <?php echo $row['emailid']; ?>

        </div>

        <div class="col-sm-2 td" style="word-wrap:break-word;">

          <?php if ($row['send_to_approved_time'] == 0) { ?>

            <a class="btn btn-sm btn-primary " href = "index1.php?edit=true&supplierid=<?php echo $row['supplierid'];?>&branchid=<?php echo $row['branchid'];?>" >Edit</a>
                    
            <button class="btn btn-sm btn-warning " data-supplierid="<?php echo $row['supplierid'];?>" data-branchid="<?php echo $row['branchid'];?>" onclick="confirms(this)">Send To Approval</button>

          <?php } else {?>


            <a class="btn btn-sm btn-primary " href = "index1.php?view=true&supplierid=<?php echo $row['supplierid'];?>&branchid=<?php echo $row['branchid'];?>" >View</a>
                    
          <?php }?>

        </div>


      <?php    


        }

      ?>

    </div>

</div>

<a class="btn" style="font-size:25px;border-radius:50px;width:60px;height:60px;position: fixed;bottom: 100px;
    right: 10px;background:#eb2f06;color:#fff; padding-top:10px;" href="index1.php">+</a>

<a class="btn" style="font-size:25px;border-radius:50px;width:60px;height:60px;position: fixed;bottom: 30px;
    right: 10px;background:#2980b9;color:#fff; padding-top:10px;" href="javascript:void(0);" onclick="printpdf()"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></a>

<?php 
include('footer.php');
?>

<script>



function confirms(e) {

  if (confirm("Are You Sure To Confirm Order?")) {

    let supplierid = $(e).attr('data-supplierid');

    let branchid   = $(e).attr('data-branchid');

    $.ajax({

      type: "POST",

      data: 'supplierid=' + supplierid + '&branchid=' + branchid,

      url: 'sendtoapproval.php',

      cache: false,

      success: function (res) {

        if (res.status == 'success') {

          window.location.reload();

        }

      }

    })

  }

}




function getBase64Image(img) {
  var canvas = document.createElement("canvas");
  canvas.width = img.width;
  canvas.height = img.height;
  var ctx = canvas.getContext("2d");
  ctx.drawImage(img, 0, 0);
  var dataURL = canvas.toDataURL("image/png");
  return dataURL.replace('/^data:image\/(png|jpg);base64,/', "");
}

function printpdf() {

  let content = [];

  content.push({

    table: {
      
      widths: ['*'],
      
      body: [

        [{text:'PEST MORTEM INDIA VENDOR REGISTRATION FORM \n',bold:1,fontSize: 10,alignment:'center'}],

      ]

    }

  });

  content.push({

    table: {
      
      widths: [150,'*'],
      
      body: [

        [{text:'Name of the Entity/Firm*',fontSize: 10,alignment:'left',border:[true,false,true,false]},{text:'',border:[true,false,true,false]}],
        [{text:'Address*',fontSize: 10,alignment:'left',border:[true,false,true,false]},{text:'',border:[true,true,true,false]}],
        [{text:'',border:[true,false,true,false]},{text:'',margin: [0,0,0,10],border:[true,true,true,false]}],

      ]

    }

  });

  content.push({

    table: {
      
      widths: [150,'*',60,'*'],
      
      body: [

        [{text:'Location /City*',fontSize: 10,alignment:'left',border:[true,false,true,false]},{text:'',border:[true,true,true,false]},{text:'State*',fontSize: 10,alignment:'left',border:[true,true,true,false]},{text:'',border:[true,true,true,false]}],
        [{text:'Telephone/Cell No',fontSize: 10,alignment:'left',border:[true,false,true,false]},{text:'',border:[true,true,true,false]},{text:'Postal Code*',fontSize: 10,alignment:'left',border:[true,true,true,false]},{text:'',border:[true,true,true,false]}],
        [{text:'Email*',fontSize: 10,alignment:'left',border:[true,false,true,false]},{text:'',border:[true,true,true,true]},{text:'Fax No*',fontSize: 10,alignment:'left',border:[true,true,true,true]},{text:'',border:[true,true,true,true]}],
        [{text:'',border:[true,false,false,false]},{text:'',border:[false,false,false,false]},{text:'',border:[false,false,false,false]},{text:'',border:[false,false,true,false]}],

      ]

    }

  });

  content.push({

    table: {
      
      widths: [150,5,40,5,30,5,23,5,22,5,52,5,60],
      
      body: [

        [{text:'Legal Status of Party*',fontSize: 10,alignment:'left',border:[true,false,true,false]},{text:'',border:[true,true,true,false]},{text:'Public Ltd',fontSize: 9,alignment:'left',border:[true,true,true,false]},{text:'',border:[true,true,true,true]},{text:'Pvt Ltd',fontSize: 9,alignment:'left',border:[true,true,true,true]},{text:'',border:[true,true,true,true]},{text:'OPC',fontSize: 9,alignment:'left',border:[true,true,true,true]},{text:'',border:[true,true,true,true]},{text:'LLP',fontSize: 9,alignment:'left',border:[true,true,true,true]},{text:'',border:[true,true,true,true]},{text:'Partnership',fontSize: 9,alignment:'left',border:[true,true,true,true]},{text:'',border:[true,true,true,true]},{text:'Proprietorship',fontSize: 9,border:[true,true,true,true]}],

        [{text:'(Pls tick where applicable)',fontSize: 8,alignment:'left',border:[true,false,true,false]},{text:'',border:[true,true,true,true]},{text:'Others',fontSize: 9,alignment:'left',border:[true,true,true,true]},{text:'',border:[false,false,false,false]},{text:'',border:[false,false,false,false]},{text:'',border:[false,false,false,false]},{text:'',border:[false,false,false,false]},{text:'',border:[false,false,false,false]},{text:'',border:[false,false,false,false]},{text:'',border:[false,false,false,false]},{text:'',border:[false,false,false,false]},{text:'',border:[false,false,false,false]},{text:'',border:[false,false,true,false]}],
        
        [{text:'',border:[true,false,false,false]},{text:'',border:[false,false,false,false]},{text:'',border:[false,false,false,false]},{text:'',border:[false,false,false,false]},{text:'',border:[false,false,false,false]},{text:'',border:[false,false,false,false]},{text:'',border:[false,false,false,false]},{text:'',border:[false,false,false,false]},{text:'',border:[false,false,false,false]},{text:'',border:[false,false,false,false]},{text:'',border:[false,false,false,false]},{text:'',border:[false,false,false,false]},{text:'',border:[false,false,true,false]}],

      ]

    }

  });

  content.push({

    table: {
      
      widths: [150,'*'],
      
      body: [

        [{text:'Nature of the Supplier*',fontSize: 10,alignment:'left',border:[true,false,true,false]},{text:'',border:[true,true,true,false]}],
        [{text:'(Define nature of goods to be supplied)',fontSize: 8,alignment:'left',border:[true,false,true,false]},{text:'',border:[true,true,true,true]}],
        [{text:'',border:[true,false,false,false]},{text:'',border:[false,false,true,false]}],
      ]

    }

  });

  content.push({

    table: {
      
      widths: [250,'*','*'],
      
      body: [

        [{text:'Whether the Party has relationship with any of the PMIs Personnel.',fontSize: 8,alignment:'left',border:[true,false,true,false]},{text:'',border:[true,true,true,false]},{text:'',border:[true,true,true,false]}],

        [{text:'If Yes, Pls provide Details *',fontSize: 8,alignment:'left',border:[true,false,true,false]},{text:'',colSpan:2,border:[true,true,true,true]},{}],
       
        [{text:'',border:[true,false,true,true],colSpan:3},{},{}],

      ]

    }

  });

  content.push({
    
    text: '',
    margin: [0,0,0,2]

  });

  content.push({

    table: {
      
      widths: ['*'],
      
      body: [

        [{text:'STATUTORY DETAILS',bold:1,fontSize: 9,alignment:'left',border:[true,true,true,true],fillColor: '#000000',color: '#FFF'}],

      ]

    }

  });

  content.push({
    
    text: '',

    margin: [2,0,0,2]

  });


  content.push({

    table: {
      
      widths: [150,'*','*'],
      
      body: [

        [{text:'Pan No #',fontSize: 10,alignment:'left',border:[true,true,true,false]},{text:'',border:[true,true,true,false]},{text:'(Mandatory if TDS is applicable)',fontSize: 9,alignment:'left',border:[true,true,true,false]}],

      ]

    }

  });

  content.push({

    table: {
      
      widths: [150,5,'*',5,'*',5,'*'],
      
      body: [

        [{text:'Type of Service/Product',fontSize: 10,alignment:'left',border:[true,false,true,false]},{text:'1',fontSize:9,border:[true,true,true,false]},{text:'',border:[true,true,true,false]},{text:'2',fontSize:9,border:[true,true,true,false]},{text:'',border:[true,true,true,false]},{text:'3',fontSize:9,border:[true,true,true,false]},{text:'',border:[true,true,true,false]}],
        [{text:'With Tax Type',fontSize: 10,bold:1,alignment:'left',border:[true,false,true,false]},{text:'1',fontSize:9,border:[true,true,true,false]},{text:'',border:[true,true,true,false]},{text:'2',fontSize:9,border:[true,true,true,false]},{text:'',border:[true,true,true,false]},{text:'3',fontSize:9,border:[true,true,true,false]},{text:'',border:[true,true,true,false]}],
        [{text:'With Hold Tax Code',fontSize: 10,bold:1,alignment:'left',border:[true,false,true,false]},{text:'1',fontSize:9,border:[true,true,true,true]},{text:'',border:[true,true,true,true]},{text:'2',fontSize:9,border:[true,true,true,true]},{text:'',border:[true,true,true,true]},{text:'3',fontSize:9,border:[true,true,true,true]},{text:'',border:[true,true,true,true]}],
        [{text:'(If exempted from TDS, please provide exemption certificate)',fontSize: 9,alignment:'left',border:[true,false,true,false],colSpan:7},{},{},{},{},{},{}],

      ]

    }

  });

  content.push({

    table: {
      
      widths: [150,50,'*',10,80,'*'],
      
      body: [

        [{text:'Are you registered under #',fontSize: 10,bold:1,alignment:'left',border:[true,false,true,false]},{text:'GST No',fontSize:10,bold:1,border:[true,true,true,true]},{text:'',border:[true,true,true,true]},{text:'',border:[false,false,false,false]},{text:'Payment Term*',fontSize:10,bold:1,border:[true,true,true,true]},{text:'',border:[true,true,true,true]}],
        [{text:'',border:[true,false,true,false],colSpan:6},{},{},{},{},{}],
        [{text:'',border:[true,false,false,false]},{text:'MSM Act',fontSize:10,bold:1,border:[true,true,true,true]},{text:'',border:[true,true,true,true]},{text:'',border:[false,false,false,false]},{text:'Freight : ',fontSize:10,bold:1,border:[true,true,true,true]},{text:'',border:[true,true,true,true]}],
        [{text:'',border:[true,false,false,false]},{text:'Date',fontSize:10,bold:1,border:[true,false,true,true]},{text:'',border:[true,false,true,true]},{text:'',border:[false,false,false,false]},{text:'',border:[false,false,false,false]},{text:'',border:[false,false,true,false]}],

      ]

    }

  });

  content.push({

    table: {
      
      widths: ['*'],
      
      body: [

        [{text:'(In case Party gets registered under MSM Act (Micro, Small and Medium Enterprises Development Act), subsequently, Party has to provide the registration details under that Act. If no details are provided by Party, it shall be assumed that Party is not covered under this Act.)',fontSize: 8,alignment:'left',border:[true,false,true,true]}],
        
      ]

    }

  });

  content.push({
    
    text: '',
    margin: [0,0,0,2]

  });

  content.push({

    table: {
      
      widths: ['*'],
      
      body: [

        [{text:'BANK DETAILS',bold:1,fontSize: 9,alignment:'left',border:[true,true,true,true],fillColor: '#000000',color: '#FFF'}],

      ]

    }

  });

  content.push({
    
    text: '',

    margin: [2,0,0,2]

  });

  content.push({

    table: {
      
      widths: [150,'*',80,'*'],
      
      body: [

        [{text:'Bank A/c Holders Name*',fontSize: 10,alignment:'left',border:[true,true,true,false]},{text:'',border:[true,true,true,false],colSpan:3},{},{}],
       
        [{text:'Bank Name*',fontSize: 10,alignment:'left',border:[true,false,true,false]},{text:'',border:[false,true,true,false]},{text:'Branch Name*',fontSize:10,border:[false,true,true,false]},{text:'',border:[true,true,true,false]}],
       
        [{text:'Account No*',fontSize: 10,alignment:'left',border:[true,false,true,false]},{text:'',border:[false,true,true,false]},{text:'A/c Type*',fontSize:10,border:[false,true,true,false]},{text:'',border:[true,true,true,false]}],
       
        [{text:'IFSC Code*',fontSize: 10,alignment:'left',border:[true,false,true,false]},{text:'',border:[false,true,true,false]},{text:'Branch Code*',fontSize:10,border:[false,true,true,false]},{text:'',border:[true,true,true,false]}],
       
        [{text:'Address of the Bank*',fontSize: 10,alignment:'left',border:[true,false,true,false]},{text:'',border:[true,true,true,false],colSpan:3},{},{}],
       
        [{text:'Branch Location/City*',fontSize: 10,alignment:'left',border:[true,false,true,false]},{text:'',border:[false,true,true,false]},{text:'State*',fontSize:10,border:[false,true,true,false]},{text:'',border:[true,true,true,false]}],

      ]

    }

  });

  content.push({

    table: {
      
      widths: [150,74,46,'*'],
      
      body: [

        [{text:'',border:[true,false,true,false]},{text:'Payment_Method*',fontSize: 9,border:[false,true,true,true]},{text:'',border:[false,true,true,true]},{text:'',border:[false,true,true,false]}],
        
        [{text:'Please enclose a copy of cancelled cheque of the above mentioned Bank account showing Account Number, IFSC Code, Name of the Account Holder. In case cheque is not available, a copy of the Bank Pass Bokk/ Certificate from the Bank confirming above details may be provided.',fontSize: 9,border:[true,false,true,false],colSpan:4},{},{},{}],

        [{text:'I/We hereby declare that the particulars furnished above are correct and complete. The financial information provided above is to be used by PMI for making payment through electronic mode. I request and authorize PMI to effect payment through electronic mode to my/our bank account as per the details mentioned above. If any transaction is delayed or not effected at all for reasons of incompleteness or incorrectness of information provided as above or any error made by the Bank(s), the Company (Pest Mortem India Pvt. Ltd.) shall not be held responsible. I/We hereby undertake to inform the Company immediately of any chnage in my/our bank/branch and account number.  ',fontSize: 9,border:[true,false,true,false],colSpan:4},{},{},{}],

        [{text:'No column to be left Blank. All Documents provided should be legible to ensure correctness of the details specified above.',fontSize: 9,bold:1,border:[true,false,true,false],colSpan:4},{},{},{}],

      ]

    }

  });

  content.push({

    table: {
      
      widths: [80,'*',54,'*',5,30,5,5,5,5,5,5,5,50],
      
      body: [

        [{text:'Signature',fontSize: 10,bold:1,border:[true,false,true,false]},{text:'',border:[false,true,true,true]},{text:'Designation',fontSize: 10,bold:1,border:[false,true,true,true]},{text:'',border:[false,true,true,true]},{text:'',border:[false,false,true,false]},{text:'Date',fillColor: '#95a5a6',color: '#000',fontSize:9,border:[false,true,true,true]},{text:'',border:[false,true,true,false]},{text:'',border:[false,true,true,false]},{text:'',border:[false,true,true,false]},{text:'',border:[false,true,true,false]},{text:'',border:[false,true,true,false]},{text:'',border:[false,true,true,false]},{text:'',border:[false,false,false,false]},{text:'Seal of the Company',fontSize: 8,rowSpan: 2,margin: [2, 5],border:[true,true,true,true]}],
        
        [{text:'Name',fontSize: 10,bold:1,border:[true,false,true,false]},{text:'',border:[false,true,true,true],colSpan:3},{},{},{text:'',border:[false,false,true,false]},{text:'Place',fillColor: '#95a5a6',color: '#000',fontSize:9,border:[false,true,true,true]},{text:'',border:[false,true,true,true],colSpan:6},{},{},{},{},{},{text:'',border:[false,false,false,false]},{text:'',fontSize: 6,border:[true,true,true,false]}],
        
        [{text:'',border:[true,false,false,true],colSpan:13},{},{},{},{},{},{},{},{},{},{},{},{},{text:'',border:[false,false,true,true]}],

      ]

    }

  });

  content.push({
    
    text: '',
    
    margin: [0,0,0,2]

  });

  content.push({

    table: {
      
      widths: ['*'],
      
      body: [

        [{text:'FOR OFFICIAL USE ONLY (to be filled-in by Unit/Business/Dept)',bold:1,fontSize: 9,alignment:'left',border:[true,true,true,true],fillColor: '#000000',color: '#FFF'}],

      ]

    }

  });

  content.push({
    
    text: '',

    margin: [2,0,0,2]

  });

  content.push({

    table: {
      
      widths: [120,100,'*'],
      
      body: [

        [{text:'Supplier Code',fontSize: 9,border:[true,true,true,false]},{text:'',border:[false,true,true,false]},{text:'Supplier Category',fontSize: 9,border:[false,true,true,false]}],
       

      ]

    }

  });

  content.push({

    table: {
      
      widths: [120,'*',120,'*'],
      
      body: [

        [{text:'Approver Name 1st [Branch]',fontSize: 9,border:[true,true,true,false]},{text:'',border:[false,true,true,false]},{text:'Approver Name 2nd [Head office]',fontSize: 9,border:[false,true,true,false]},{text:'',border:[false,true,true,false]}],
        [{text:'Signature & Date',fontSize: 9,border:[true,true,true,false]},{text:'',border:[false,true,true,false]},{text:'Signature & Date',fontSize: 9,border:[false,true,true,false]},{text:'',border:[false,true,true,false]}],
        [{text:'',border:[true,false,false,true]},{text:'',border:[true,false,false,true]},{text:'',border:[true,false,false,true]},{text:'',border:[true,false,true,true]}],

      ]

    }

  });

  content.push({
    
    text: '',
    
    margin: [0,0,0,2]

  });

  content.push({

    table: {
      
      widths: ['*','*'],
      
      body: [

        [{text:'* Mandatory to fill in the details',bold:1,fontSize: 9,alignment:'center',border:[true,true,true,true],fillColor: '#000000',color: '#FFF'},{text:'# Mandatory to submit photocopy of certificate',bold:1,fontSize: 9,alignment:'center',border:[true,true,true,true],fillColor: '#000000',color: '#FFF'}],

      ]

    }

  });
 
  dd = {

    pageSize: 'A4',

    pageOrientation: 'portrait',

    pageMargins: [40, 10, 30, 10],

    // footer: function (currentPage, pageCount) {

    //   return {

    //     margin: 10,

    //     columns: [{

    //       fontSize: 9,

    //       text: [{

    //           text: '--------------------------------------------------------------------------' +
    //             '\n',
    //           margin: [0, 20]

    //         },
    //         {


    //           text: [{text:'© Pest Mortem (India) PVT. LTD. | '},{text: ' REGD OFFICE : ',bold:1},{text: 'G-2, Sunder Tower, T. J. Road, Sewree (West), Mumbai,400015 | PAGE ' + currentPage.toString() + ' of ' + pageCount}],


    //         }

    //       ],

    //       alignment: 'center'

    //     }]


    //   };

    // },

    content,
    styles: {

      tablfont: {

        fontSize: 9,
        fonts: 'TimesNewRoman'
      }

    }

  }

  var win = window.open('', '_blank');
  pdfMake.createPdf(dd).open({}, win);

}


</script>