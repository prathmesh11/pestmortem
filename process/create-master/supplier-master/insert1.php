<?php

    $base = '../../../';

    include($base.'_in/connect.php');

    header('content-type: application/json; charset=utf-8');

    header("access-control-allow-origin: *");

    if(isset($_POST['data'])) {

        $con = _connect();

        if (session_status()==PHP_SESSION_NONE) { session_start(); }

        $created_by       = $_SESSION['employeeid'];

        $branchid         = $_SESSION['branchid'];

        $data             = get_object_vars(json_decode($_POST["data"]));

        $suppliername     = strtoupper($data['suppliername']);
        
        $address          = $data['address'];
        
        $legalStatus      = $data['legalStatus'];
        
        $natureofsupplier = $data['natureofsupplier'];
        
        $city             = $data['city'];
        
        $state            = $data['state'];
        
        $phone            = $data['phone'];
        
        $pincode          = $data['pincode'];
        
        $emailid          = $data['emailid'];
        
        $faxno            = $data['faxno'];
        
        $relationship     = $data['relationship'];
        
        $panno            = strtoupper($data['panno']);
        
        $service1         = $data['service1'];
        
        $service2         = $data['service2'];
        
        $service3         = $data['service3'];
        
        $taxtype1         = $data['taxtype1'];
        
        $taxtype2         = $data['taxtype2'];
        
        $taxtype3         = $data['taxtype3'];
        
        $taxcode1         = $data['taxcode1'];
        
        $taxcode2         = $data['taxcode2'];
        
        $taxcode3         = $data['taxcode3'];
        
        $gstnumber        = strtoupper($data['gstnumber']);
        
        $paymentTerms     = $data['paymentTerms'];
        
        $freight          = $data['freight'];
        
        $msmact           = $data['msmact'];
        
        $msmdate          = $data['msmdate'];
        
        $acountholdername = $data['acountholdername'];
        
        $bankname         = $data['bankname'];
        
        $branchname       = $data['branchname'];
        
        $accountno        = $data['accountno'];
        
        $actype           = $data['actype'];
        
        $ifsccode         = $data['ifsccode'];
        
        $branchcode       = $data['branchcode'];
        
        $addressbank      = $data['addressbank'];
        
        $branchcity       = $data['branchcity'];
        
        $branchstate      = $data['branchstate'];
        
        $paymentmethod    = $data['paymentmethod'];
       
        $creditdays        = $data['creditdays'];

        $opening_balance   = $data['opening_balance'];

        $opening_date      = $data['opening_date'];

        $suppliergroupcode = $data['suppliergroupcode'];

        $supplierid        = mysqli_fetch_assoc(mysqli_query($con, "SELECT max(id) x FROM suppliermaster"))['x'];

        if($supplierid) { $supplierid++; } else {  $supplierid=1; }


        if($gstnumber=='') {

            $create = mysqli_query($con, "INSERT INTO suppliermaster (supplierid,branchid,suppliername,address,legalStatus,natureofsupplier,city,state,phone,pincode,emailid,faxno,relationship,panno,service1,service2,service3,taxtype1,taxtype2,taxtype3,taxcode1,taxcode2,taxcode3,gstnumber,paymentTerms,freight,msmact,msmdate,acountholdername,bankname,branchname,accountno,actype,ifsccode,branchcode,addressbank ,branchcity,branchstate,paymentmethod, creditdays, opening_balance, opening_date, suppliergroupcode, created_by,created_time) VALUES ('$supplierid','$branchid','$suppliername','$address','$legalStatus','$natureofsupplier','$city','$state','$phone','$pincode','$emailid','$faxno','$relationship','$panno','$service1','$service2','$service3','$taxtype1','$taxtype2','$taxtype3','$taxcode1','$taxcode2','$taxcode3','$gstnumber','$paymentTerms','$freight','$msmact','$msmdate','$acountholdername','$bankname','$branchname','$accountno','$actype','$ifsccode','$branchcode','$addressbank ','$branchcity','$branchstate','$paymentmethod','$creditdays', '$opening_balance', '$opening_date', '$suppliergroupcode', '$created_by','$CURRENT_MILLIS')");

            if($create) {

                echo '{"status":"success"}';

            } else {

                echo '{"status":"falid1"}';

            }

        } else {

            if( !preg_match("/^(0[1-9]|[1-2][0-9]|3[0-5])([a-zA-Z]){5}([0-9]){4}([a-zA-Z]){1}([a-zA-Z0-9]){1}([a-zA-Z]){1}([a-zA-Z0-9]){1}?$/", $gstnumber)) {

                echo '{"status":"falid11"}';

            } else {

                $results = mysqli_fetch_assoc(mysqli_query($con, "SELECT gstnumber x FROM suppliermaster WHERE gstnumber='$gstnumber' "))['x'];

                if( !$results) {

                    $create = mysqli_query($con, "INSERT INTO suppliermaster (supplierid,branchid,suppliername,address,legalStatus,natureofsupplier,city,state,phone,pincode,emailid,faxno,relationship,panno,service1,service2,service3,taxtype1,taxtype2,taxtype3,taxcode1,taxcode2,taxcode3,gstnumber,paymentTerms,freight,msmact,msmdate,acountholdername,bankname,branchname,accountno,actype,ifsccode,branchcode,addressbank ,branchcity,branchstate,paymentmethod, creditdays, opening_balance, opening_date, suppliergroupcode, created_by,created_time) VALUES ('$supplierid','$branchid','$suppliername','$address','$legalStatus','$natureofsupplier','$city','$state','$phone','$pincode','$emailid','$faxno','$relationship','$panno','$service1','$service2','$service3','$taxtype1','$taxtype2','$taxtype3','$taxcode1','$taxcode2','$taxcode3','$gstnumber','$paymentTerms','$freight','$msmact','$msmdate','$acountholdername','$bankname','$branchname','$accountno','$actype','$ifsccode','$branchcode','$addressbank ','$branchcity','$branchstate','$paymentmethod', '$creditdays', '$opening_balance', '$opening_date', '$suppliergroupcode', '$created_by','$CURRENT_MILLIS')");

                    if($create) {

                        echo '{"status":"success"}';

                    } else {

                        echo '{"status":"falid1"}';

                    }
                } else {

                    echo '{"status":"falid22"}';

                }


            }

        }
        
        _close($con);

    } else {

        echo '{"status":"falid"}';

    }
?>