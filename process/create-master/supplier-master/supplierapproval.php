<?php 
    
  $base    = '../../';
    
  $navenq2 = 'background:#1B1464;';

  include('header.php');

  if($_SESSION['role']<>'admin'){ 
      
    echo "<script>window.location.href='/process/dashboard.php';</script>";
    
    exit;

  } 

  if (session_status() == PHP_SESSION_NONE) { session_start(); }

  $sessionby = $_SESSION['employeeid'];
  
  $branchid  = $_SESSION['branchid'];

  $signature = mysqli_fetch_assoc(mysqli_query($con,"SELECT signature x FROM branchmaster WHERE branchid='$branchid'"))['x'];



?>

<style>
.clr {
        clear:both;
    }
</style>

<br>
<img style="display:none;" id=imageid src="<?php echo $base;?>img/PestMortemLogo.jpg">

<input type="hidden" id=branchid value="<?php echo $branchid;?>">

<img style="display:none;" id="signature" src="<?php echo $base.$signature;?>">

<div class="table-ui container-fluid">

    <div class="tr row">

        <div class="col-sm-2 th" style="word-wrap:break-word;">Supplier Name</div>

        <div class="col-sm-2 th">Phone No</div>

        <div class="col-sm-2 th">City</div>

        <div class="col-sm-2 th">State</div>

        <div class="col-sm-2 th">Email Id</div>

        <div class="col-sm-2 th">Action</div>

    </div>

    <div class="row tr">

      <?php

        $result = mysqli_query($con,"SELECT * FROM  suppliermaster WHERE send_to_approved_time <> 0 AND branchid ='$branchid' ORDER BY id DESC");

        while ($row = mysqli_fetch_assoc($result)) {

          $state     = $row['state'];

          $statename  = mysqli_fetch_assoc(mysqli_query($con,"SELECT statename x FROM statemaster WHERE id='$state'"))['x'];


      ?>

        <div class="col-sm-2 td" style="word-wrap:break-word;">

          <?php echo $row['suppliername'];?>

        </div>

        <div class="col-sm-2 td" style="word-wrap:break-word;">

          <?php echo $row['phone'];?>

        </div>

        <div class="col-sm-2 td" style="word-wrap:break-word;">

          <?php echo $row['city'];?>

        </div>

        <div class="col-sm-2 td" style="word-wrap:break-word;">

          <?php echo $statename; ?>

        </div>

        <div class="col-sm-2 td" style="word-wrap:break-word;">

          <?php echo $row['emailid']; ?>

        </div>

        <div class="col-sm-2 td" style="word-wrap:break-word;">

          <a class="btn btn-sm btn-primary " href = "index1.php?edit=true&supplierid=<?php echo $row['supplierid'];?>&branchid=<?php echo $row['branchid'];?>" >Edit</a>
                   
          <button class="btn btn-sm btn-warning " data-supplierid="<?php echo $row['supplierid'];?>" data-branchid="<?php echo $row['branchid'];?>" onclick="confirms(this)">Approve</button>
          
          <button class="btn btn-sm btn-danger " data-supplierid="<?php echo $row['supplierid'];?>" data-branchid="<?php echo $row['branchid'];?>" onclick="confirmBack(this)">Back</button>

        </div>


      <?php    


        }

      ?>

    </div>

</div>



<?php 
include('footer.php');
?>

<script>



function confirms(e) {

  if (confirm("Are You Sure To Confirm Order?")) {

    let supplierid = $(e).attr('data-supplierid');

    let branchid   = $(e).attr('data-branchid');

    $.ajax({

      type: "POST",

      data: 'supplierid=' + supplierid + '&branchid=' + branchid,

      url: 'approval.php',

      cache: false,

      success: function (res) {

        if (res.status == 'success') {

          window.location.reload();

        }

      }

    })

  }

}

function confirmBack(e) {

  if (confirm("Are You Sure To Confirm Order?")) {

    let supplierid = $(e).attr('data-supplierid');

    let branchid   = $(e).attr('data-branchid');

    $.ajax({

      type: "POST",

      data: 'supplierid=' + supplierid + '&branchid=' + branchid,

      url: 'back.php',

      cache: false,

      success: function (res) {

        if (res.status == 'success') {

          window.location.reload();

        }

      }

    })

  }

}


</script>