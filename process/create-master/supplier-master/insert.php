<?php

    $base = '../../../';

    include($base.'_in/connect.php');

    header('content-type: application/json; charset=utf-8');

    header("access-control-allow-origin: *");

    if(isset($_POST['data']) && isset($_POST['data2'])) {

        $con = _connect();

        if (session_status()==PHP_SESSION_NONE) { session_start(); }

        $created_by        = $_SESSION['employeeid'];

        $branchid          = $_SESSION['branchid'];

        $data1             = $_POST["data"];

        $data2             = $_POST["data2"];

        $data              = get_object_vars(json_decode($_POST["data"]));

        $suppliername      = strtoupper($data['suppliername']);

        $gstnumber         = strtoupper($data['gstnumber']);

        $city              = $data['city'];

        $opening_balance   = $data['opening_balance'];

        $opening_date      = $data['opening_date'];

        $suppliergroupcode = $data['suppliergroupcode'];

        $supplierid        = mysqli_fetch_assoc(mysqli_query($con, "SELECT max(id) x FROM suppliermaster"))['x'];

        if($supplierid) { $supplierid++; } else {  $supplierid=1; }


        if($gstnumber=='') {

            $create = mysqli_query($con, "INSERT INTO suppliermaster (supplierid,branchid,suppliername,city,gstnumber,suppliergroupcode,supplierjson,contactjson,opening_balance,opening_date,created_by,created_time) VALUES ('$supplierid','$branchid','$suppliername','$city','$gstnumber','$suppliergroupcode','$data1','$data2','$opening_balance','$opening_date','$created_by','$CURRENT_MILLIS')");

            if($create) {

                echo '{"status":"success"}';

            } else {

                echo '{"status":"falid1"}';

            }

        } else {

            if( !preg_match("/^(0[1-9]|[1-2][0-9]|3[0-5])([a-zA-Z]){5}([0-9]){4}([a-zA-Z]){1}([a-zA-Z0-9]){1}([a-zA-Z]){1}([a-zA-Z0-9]){1}?$/", $gstnumber)) {

                echo '{"status":"falid11"}';

            } else {

                $results = mysqli_fetch_assoc(mysqli_query($con, "SELECT gstnumber x FROM suppliermaster WHERE gstnumber='$gstnumber' "))['x'];

                if( !$results) {

                    $create = mysqli_query($con, "INSERT INTO suppliermaster (supplierid,branchid,suppliername,city,gstnumber,suppliergroupcode,supplierjson,contactjson,opening_balance,opening_date,created_by,created_time) VALUES ('$supplierid','$branchid','$suppliername','$city','$gstnumber','$suppliergroupcode','$data1','$data2','$opening_balance','$opening_date','$created_by','$CURRENT_MILLIS')");

                    if($create) {

                        echo '{"status":"success"}';

                    } else {

                        echo '{"status":"falid1"}';

                    }
                } else {

                    echo '{"status":"falid22"}';

                }


            }

        }
        
        _close($con);

    } else {

        echo '{"status":"falid"}';

    }
?>