<?php

  $base = '../../../';

  include($base.'_in/connect.php');

  header('content-type: application/json; charset=utf-8');

  header("access-control-allow-origin: *");
  
  if(isset($_POST['supplierid']) && isset($_POST['branchid'])) {

    $con        = _connect();
    
    $supplierid = _clean($con,$_POST["supplierid"]);

    $branchid   = _clean($con,$_POST["branchid"]);

    $results    = mysqli_fetch_assoc(mysqli_query($con,"SELECT * FROM suppliermaster WHERE supplierid='$supplierid' AND branchid = '$branchid' "));

    if($results) {

      $json = '[{"scode":"'.$results['supplierid'].'","suppliername":"'.$results['suppliername'].'","address":"'.$results["address"].'","legalStatus":"'.$results["legalStatus"].'","natureofsupplier":"'.$results["natureofsupplier"].'","city":"'.$results["city"].'","state":"'.$results["state"].'","phone":"'.$results["phone"].'","pincode":"'.$results["pincode"].'","emailid":"'.$results["emailid"].'","faxno":"'.$results["faxno"].'","relationship":"'.$results["relationship"].'","panno":"'.$results["panno"].'","service1":"'.$results["service1"].'","service2":"'.$results["service2"].'","service3":"'.$results["service3"].'","taxtype1":"'.$results["taxtype1"].'","taxtype2":"'.$results["taxtype2"].'","taxtype3":"'.$results["taxtype3"].'","taxcode1":"'.$results["taxcode1"].'","taxcode2":"'.$results["taxcode2"].'","taxcode3":"'.$results["taxcode3"].'","gstnumber":"'.$results["gstnumber"].'","paymentTerms":"'.$results["paymentTerms"].'","freight":"'.$results["freight"].'","msmact":"'.$results["msmact"].'","msmdate":"'.$results["msmdate"].'","acountholdername":"'.$results["acountholdername"].'","bankname":"'.$results["bankname"].'","branchname":"'.$results["branchname"].'","accountno":"'.$results["accountno"].'","actype":"'.$results["actype"].'","ifsccode":"'.$results["ifsccode"].'","branchcode":"'.$results["branchcode"].'","addressbank":"'.$results["addressbank"].'","branchcity":"'.$results["branchcity"].'","branchstate":"'.$results["branchstate"].'","paymentmethod":"'.$results["paymentmethod"].'","creditdays":"'.$results["creditdays"].'", "opening_balance":"'.$results["opening_balance"].'", "opening_date":"'.$results["opening_date"].'", "suppliergroupcode":"'.$results["suppliergroupcode"].'"}]';

      echo '{"status":"success","json":'.$json.'}';

    } else {

      echo '{"status":"falid2"}';

    }

    _close($con);

  } else {

    echo '{"status":"falid1"}';

  }

?>