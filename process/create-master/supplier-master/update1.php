<?php

   $base = '../../../';

   include($base.'_in/connect.php');

   header('content-type: application/json; charset=utf-8');

   header("access-control-allow-origin: *");

    if(isset($_POST['data'])) {

        $con = _connect();

        if (session_status()==PHP_SESSION_NONE) { session_start(); }

        $created_by        = $_SESSION['employeeid'];

        $branchid          = $_SESSION['branchid'];

        $data              = get_object_vars(json_decode($_POST["data"]));

        $scode             = $data['scode'];

        $suppliername      = strtoupper($data['suppliername']);
        
        $address           = $data['address'];
        
        $legalStatus       = $data['legalStatus'];
        
        $natureofsupplier  = $data['natureofsupplier'];
        
        $city              = $data['city'];
        
        $state             = $data['state'];
        
        $phone             = $data['phone'];
        
        $pincode           = $data['pincode'];
        
        $emailid           = $data['emailid'];
        
        $faxno             = $data['faxno'];
        
        $relationship      = $data['relationship'];
        
        $panno             = strtoupper($data['panno']);
        
        $service1          = $data['service1'];
        
        $service2          = $data['service2'];
        
        $service3          = $data['service3'];
        
        $taxtype1          = $data['taxtype1'];
        
        $taxtype2          = $data['taxtype2'];
        
        $taxtype3          = $data['taxtype3'];
        
        $taxcode1          = $data['taxcode1'];
        
        $taxcode2          = $data['taxcode2'];
        
        $taxcode3          = $data['taxcode3'];
        
        $gstnumber         = strtoupper($data['gstnumber']);
        
        $paymentTerms      = $data['paymentTerms'];
        
        $freight           = $data['freight'];
        
        $msmact            = $data['msmact'];
        
        $msmdate           = $data['msmdate'];
        
        $acountholdername  = $data['acountholdername'];
        
        $bankname          = $data['bankname'];
        
        $branchname        = $data['branchname'];
        
        $accountno         = $data['accountno'];
        
        $actype            = $data['actype'];
        
        $ifsccode          = $data['ifsccode'];
        
        $branchcode        = $data['branchcode'];
        
        $addressbank       = $data['addressbank'];
        
        $branchcity        = $data['branchcity'];
        
        $branchstate       = $data['branchstate'];
        
        $paymentmethod     = $data['paymentmethod'];

        $creditdays        = $data['creditdays'];

        $opening_balance   = $data['opening_balance'];

        $opening_date      = $data['opening_date'];

        $suppliergroupcode = $data['suppliergroupcode'];

        if($gstnumber=='') {  

            $update = mysqli_query($con,"UPDATE suppliermaster SET suppliername = '$suppliername', address = '$address', legalStatus = '$legalStatus', natureofsupplier = '$natureofsupplier', city = '$city', state = '$state', phone = '$phone', pincode = '$pincode', emailid = '$emailid', faxno = '$faxno', relationship = '$relationship', panno = '$panno', service1 = '$service1', service2 = '$service2', service3 = '$service3', taxtype1 = '$taxtype1', taxtype2 = '$taxtype2', taxtype3 = '$taxtype3', taxcode1 = '$taxcode1', taxcode2 = '$taxcode2', taxcode3 = '$taxcode3', gstnumber = '$gstnumber', paymentTerms = '$paymentTerms', freight = '$freight', msmact = '$msmact', msmdate = '$msmdate', acountholdername = '$acountholdername', bankname = '$bankname', branchname = '$branchname', accountno = '$accountno', actype = '$actype', ifsccode = '$ifsccode', branchcode = '$branchcode', addressbank = '$addressbank', branchcity = '$branchcity', branchstate = '$branchstate', paymentmethod = '$paymentmethod', creditdays = '$creditdays', opening_balance = '$opening_balance', opening_date = '$opening_date', suppliergroupcode = '$suppliergroupcode', updated_by = '$created_by', updated_time = '$CURRENT_MILLIS' WHERE supplierid='$scode' AND branchid = '$branchid' ");

            if($update){

                echo '{"status":"success"}';

            } else {

                echo '{"status":"falid2"}';

            }

        } else {

            if( !preg_match("/^(0[1-9]|[1-2][0-9]|3[0-5])([a-zA-Z]){5}([0-9]){4}([a-zA-Z]){1}([a-zA-Z0-9]){1}([a-zA-Z]){1}([a-zA-Z0-9]){1}?$/", $gstnumber)) {

                echo '{"status":"falid11"}';

            } else {

                $update = mysqli_query($con,"UPDATE suppliermaster SET suppliername = '$suppliername', address = '$address', legalStatus = '$legalStatus', natureofsupplier = '$natureofsupplier', city = '$city', state = '$state', phone = '$phone', pincode = '$pincode', emailid = '$emailid', faxno = '$faxno', relationship = '$relationship', panno = '$panno', service1 = '$service1', service2 = '$service2', service3 = '$service3', taxtype1 = '$taxtype1', taxtype2 = '$taxtype2', taxtype3 = '$taxtype3', taxcode1 = '$taxcode1', taxcode2 = '$taxcode2', taxcode3 = '$taxcode3', gstnumber = '$gstnumber', paymentTerms = '$paymentTerms', freight = '$freight', msmact = '$msmact', msmdate = '$msmdate', acountholdername = '$acountholdername', bankname = '$bankname', branchname = '$branchname', accountno = '$accountno', actype = '$actype', ifsccode = '$ifsccode', branchcode = '$branchcode', addressbank = '$addressbank', branchcity = '$branchcity', branchstate = '$branchstate', paymentmethod = '$paymentmethod', creditdays = '$creditdays', opening_balance = '$opening_balance', opening_date = '$opening_date', suppliergroupcode = '$suppliergroupcode', updated_by = '$created_by', updated_time = '$CURRENT_MILLIS' WHERE supplierid='$scode' AND branchid = '$branchid' ");

                if($update) {

                    echo '{"status":"success"}';

                } else {

                    echo '{"status":"falid1"}';

                }

            }

        }
  
        _close($con);

    } else {

      echo '{"status":"falid"}';

    }
    
?>