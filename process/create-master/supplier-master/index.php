<?php
   
   $base = '../../../';
   
    $js = '<script src="'.$base.'js/fwork.js"></script>
          <script src="'.$base.'js/alart.js"></script>
          <script src="'.$base.'js/list.min.js"></script>';

    $css = '<link rel="stylesheet" href="'.$base.'css/fwork.css">
           <link rel="stylesheet" href="'.$base.'css/alart.css">';

    $scode = '';

    if(isset($_GET['scode'])){

        $scode = $_GET['scode'];

    }

    include($base.'_in/header.php');

    include($base.'_in/connect.php');

    if (!in_array("Supplier Master", $arrysession)) {
   
        echo "<script>window.location.href='/process/dashboard.php';</script>";
       
        exit;
    
    }
 

    $con = _connect();

?>

<style>

    hr.style-hr {
        border: 0;
        height: 1px;
        background-image: linear-gradient(to right, rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.75), rgba(0, 0, 0, 0));
    }

    .table-list td,
    .table-list th {
        border: 1px solid #ddd;
        padding: 2px;
        font-size: 13px;
    }

    .table-list tr:nth-child(even) {
        background-color: #f2f2f2;
    }

    .table-list th {
        padding-top: 12px;
        padding-bottom: 12px;
        text-align: left;
        background-color: #16a085;
        color: white;
    }

    .sidenav1 {
        height: 100%;
        width: 0;
        position: fixed;
        z-index: 1;
        top: 0;
        left: 0;
        background-color: #111;
        overflow-x: hidden;
        transition: 0.5s;
        padding-top: 60px;
    }

    .sidenav1 a {
        padding: 8px 8px 8px 32px;
        text-decoration: none;
        font-size: 25px;
        color: #818181;
        display: block;
        transition: 0.3s;
    }

    .sidenav1 a:hover {
        color: #f1f1f1;
    }

    .sidenav1 .closebtn1 {
        position: absolute;

        right: 25px;
        font-size: 36px;
        margin-left: 50px;
    }

    @media screen and (max-height: 450px) {
        .sidenav1 {
            padding-top: 15px;
        }

        .sidenav1 a {
            font-size: 18px;
        }
    }

    .list {
        padding: 0px;
    }

    .list li span {
        display: inline;
    }

    .list li {
        text-decoration: none;
        display: block;
        padding: 2px;
        background: #FFF;
        border: 1px solid #333;
        color: #000;
    }

    .list1 li {
        
        border: none;
        
    }

    .list1 li:hover {
        background: #0984e3;
        color: #fff;
    }
    
</style>

<div class="container-fluid">

    <div class="row content" id="section">

        <span style="font-size:24px;cursor:pointer" onclick="openNav1()">&#9776; Search</span>
        
        <div id="mysidenav1" class="sidenav1" style="margin-left:80px;">

            <div id="test-list" style="padding:5px;">
            
                <a href="javascript:void(0)" class="closebtn1" onclick="closeNav1()">&times;</a>
                <br><br><br>

                <div class="form-group">

                    <input type="text" placeholder="Search" class="form-control input-sm fuzzy-search" style="border-radius:3px;">

                </div>
                
                <ul class="list">

                    <?php 

                        $result1 = mysqli_query($con, "SELECT * FROM `suppliermaster`");

                        while($rows=mysqli_fetch_assoc($result1)) {

                            echo '<li><span class="suppliermaster">' .$rows['suppliername']. '<span><button style="margin-left:2px;" data-supplierid="' .$rows['supplierid']. '" onclick="edit(this)" class="btn btn-primary btn-sm"><span class="fa fa-edit"></span> E</button></i>';

                        }

                    ?>

                </ul>

            </div>

        </div>

        <h2 align="center" style="margin-top:0px;">Create Supplier Master</h2>

        <div style="clear:both;"></div>

        <hr class="style-hr">

        <input type="hidden" data-name='scode' class="form-control input-sm">

        <div class="col-sm-5">

            <center> Supplier Details</center>

            <hr class="style-hr">

            <div class="col-sm-5">Supplier Name</div>

            <div class="col-sm-7">

                <div class="form-group">
                    
                    <input type="text" data-role='text' data-name='suppliername' class="form-control input-sm">

                    <input type="text" id='supplierid' style="display:none;">

                </div>

            </div>

            <div class="col-sm-5">address</div>

            <div class="col-sm-7">

                <div class="form-group">

                    <textarea data-role='textarea' data-name='address' class="form-control input-sm"> </textarea>
                
                </div>

            </div>

            <div class="col-sm-5">City</div>

            <div class="col-sm-7">

                <div class="form-group">
                    
                    <input type="text" data-role='text' data-name='city' class="form-control input-sm">

                </div>

            </div>

            <div class="col-sm-5">Pin Number</div>

            <div class="col-sm-7">

                <div class="form-group">
                    
                    <input type="text" data-role='number' data-name='pin' class="form-control input-sm">
                
                </div>

            </div>

            <div class="col-sm-5">Supplier Group</div>

            <div class="col-sm-7">

                <div class="form-group" id="test-list1">

                    <input type="text" data-role='text' placeholder="Search" class="form-control input-sm fuzzy-search"
                        data-name='suppliergroupcode' id="suppliergroupcode" style="border-radius:4px;">

                        <div id="searchbox" style="border:1px solid #74b9ff; height:60px; overflow-y: scroll;display:none;">

                        <ul class="list list1" style="list-style-type: none;padding-left:0px;margin-bottom:0;">

                            <?php 

                                $result1 = mysqli_query($con, "SELECT DISTINCT suppliergroupcode FROM `suppliermaster`");

                                while($rows=mysqli_fetch_assoc($result1)) {

                                    echo '<li style="padding-left:15px;" onclick="jump(this)"><span class="suppliergroupcode">' .$rows['suppliergroupcode']. '<span></i>';

                                }

                            ?>

                        </ul>

                    </div>

                </div>

            </div>
            <!-- <div class="col-sm-5">Supply Capacity</div>
            <div class="col-sm-7">
                <div class="form-group"><input type="text" data-role='text' data-name='Supplycapacity' class="form-control input-sm"></div>
            </div> -->
            <div class="col-sm-5">Credit days</div>

            <div class="col-sm-7">

                <div class="form-group">

                    <input type="number" data-role='numnber' data-name='creditdays' value=30 class="form-control input-sm">

                </div>

            </div>

            <div class="col-sm-5">Country</div>

            <div class="col-sm-7">

                <div class="form-group">

                    <select class="form-control input-sm" data-role="select" data-name="country">

                        <option value="IN">India</option>

                        <?php 

                            $result1=mysqli_query($con, "SELECT * FROM `countrymaster`");

                            while($rows=mysqli_fetch_assoc($result1)) {

                                echo '<option value="'.$rows['countrycode'].'">'.$rows['countryname'].'</option>';

                            }

                        ?>

                    </select>

                </div>

            </div>  

            <div class="col-sm-5">State</div>

            <div class="col-sm-7">

                <div class="form-group">
                
                    <select class="form-control input-sm" data-role="select" id="state" data-name="state">

                        <option value="27">MAHARASHTRA</option>

                        <?php 

                            $result1=mysqli_query($con, "SELECT * FROM `statemaster` ORDER BY statename");
                            
                            while($rows=mysqli_fetch_assoc($result1)) {
                                
                                echo '<option value="'.$rows['statecode'].'">'.$rows['statename'].'</option>';
                            }

                        ?>

                    </select>

                </div>

            </div>

            <div style="clear:both;"></div>

            <div class="col-sm-5">GST Number</div>

            <div class="col-sm-7">

                <div class="form-group">
                    
                    <input type="text" data-name='gstnumber' class="form-control input-sm">
                
                </div>

            </div>

            <div class="col-sm-5">PAN</div>

            <div class="col-sm-7">

                <div class="form-group">

                    <input type="text" data-role='text' data-name='pan' class="form-control input-sm">

                </div>

            </div>
          
            <div class="col-sm-5">others</div>

            <div class="col-sm-7">

                <div class="form-group">

                    <input type="text" data-name='others' class="form-control input-sm">

                </div>

            </div>

            <div class="col-sm-5">Opening Balance</div>

            <div class="col-sm-7">

                <div class="form-group">

                    <input type="number" data-name='opening_balance' value="0" class="form-control input-sm">

                </div>

            </div>


            <div class="col-sm-5">Opening Balance Date</div>

            <div class="col-sm-7">

                <div class="form-group">

                    <input type="date" data-name='opening_date' value="<?php echo $today;?>" class="form-control input-sm">

                </div>

            </div>

        </div>

        <div class="col-sm-7">

            <div class="col-sm-12">

                <center> Contact Person Details</center>

                <hr class="style-hr">

                <center>

                    <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#myModal">Add+ </button>

                </center>

                <br>

                <div class="table-responsive">

                    <table id="contact-table" class="table table-list">

                        <thead>

                            <tr>

                                <th>SR.</th>
                                <th>Name</th>
                                <th>Department</th>
                                <th>Mobile</th>
                                <th>Email</th>
                                <th>Action</th>

                            </tr>

                        </thead>

                        <tbody>

                        </tbody>

                    </table>

                </div>

            </div>

            <!-- <div class="col-sm-12">
                <center>Bank Details</center>
                <hr class="style-hr">
                <div class="col-sm-5">Bank Name</div>
                <div class="col-sm-7">
                    <div class="form-group"><input type="text" data-role='text' data-name='bankname' class="form-control input-sm"></div>
                </div>
                <div class="col-sm-5">Branch Name</div>
                <div class="col-sm-7">
                    <div class="form-group"><input type="text" data-role='text' data-name='branchname' class="form-control input-sm"></div>
                </div>
                <div class="col-sm-5">Branch Address</div>
                <div class="col-sm-7">
                    <div class="form-group"><textarea data-role='textarea' data-name='bankaddress' class="form-control input-sm"> </textarea></div>
                </div>
                <div class="col-sm-5">IFSC Code</div>
                <div class="col-sm-7">
                    <div class="form-group"><input type="text" data-role='text' data-name='ifsccode' class="form-control input-sm"></div>
                </div>
                <div class="col-sm-5">Account Number</div>
                <div class="col-sm-7">
                    <div class="form-group"><input type="text" data-role='text' data-name='accountno' class="form-control input-sm"></div>
                </div>
            </div> -->
        </div>

        <div class="col-sm-12">

            <div style="clear:both;"></div>

            <hr class="style-hr">

            <div class="col-sm-3"></div>

            <div class="col-sm-3">
                
                <button class="btn btn-success btn-sm btn-block"  id="btn-submit" onclick="submit()" style="margin:5px;">Submit</button>
            
            </div>

            <div class="col-sm-3">
                
                <button class="btn btn-danger btn-sm btn-block" id="btn-reset" style="margin:5px;">Reset</button>

            </div>
            <div class="col-sm-3"></div>

        </div>

    </div>

</div>

    <br><br>

    <!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">

    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">

            <div class="modal-header">

                <button type="button" class="close" data-dismiss="modal">&times;</button>

                <h4 class="modal-title" align="center">Add Contact</h4>

            </div>

            <div class="modal-body" id='contactModal'>

                <div class="row">

                    <div class="col-sm-5">Person Name</div>

                    <div class="col-sm-7">

                        <div class="form-group">
                            
                            <input type="text" data-role='text' id="personName" data-name='name' class="form-control input-sm">
                        
                        </div>

                    </div>

                    <div class="col-sm-5">Department</div>

                    <div class="col-sm-7">

                        <div class="form-group">
                            
                            <input type="text" data-role='text' id="department" data-name='department' class="form-control input-sm">
                        
                        </div>

                    </div>

                    <div class="col-sm-5">Mobile No.</div>

                    <div class="col-sm-7">

                        <div class="form-group">

                            <input type="text" data-role='text' id="mobile" data-name='mobile' class="form-control input-sm">
                        
                        </div>

                    </div>

                    <div class="col-sm-5">Email</div>

                    <div class="col-sm-7">

                        <div class="form-group">
                            
                            <input type="text" data-role='text' id="email" data-name='email' class="form-control input-sm">
                        
                        </div>

                    </div>

                   
                    <div style="clear:both;"></div>

                    <hr>
                      
                    <div class="col-sm-6">
                        
                        <button class="btn btn-success btn-sm btn-block" data-edits="" onclick="addContactPerson(this)" id="btn-contact" style="margin:5px;">Submit</button>
                    
                    </div>

                    <div class="col-sm-6">
                        
                        <button class="btn btn-danger btn-sm btn-block" id="btn-clear" style="margin:5px;">Clear</button>
                    
                    </div>

                    
                </div>

            </div>

        </div>

    </div>

</div>

<?php

    include($base.'_in/footer.php');

?>

<script>

function jump(e) {

    var suppliergroupcode = $(e).find('span').text().trim();

    $('#suppliergroupcode').val(suppliergroupcode);

    $('#searchbox').hide();

}

$('#suppliergroupcode').keypress(function (e) {

    if (e.which == 13) {

        $('#searchbox').hide();

    } else {

        $('#searchbox').show();

    }

});


function openNav1() {
    document.getElementById("mysidenav1").style.width = "300px";
}

function closeNav1() {
    document.getElementById("mysidenav1").style.width = "0";
}
var monkeyList = new List('test-list', {
    valueNames: ['suppliermaster']
});


function addContactPerson(e) {

    var chks       = $(e).attr('data-edits');

    var data       = checker('contactModal');

    var personName = data.name;

    var department = data.department;

    var mobile     = data.mobile;

    var email      = data.email;

    var valid      = true;

    if (checker('contactModal') != false) {

        valid = valid * true;

    } else {

        valid = valid * false;

    }

    $('#contact-table .personName').each(function () {

        var personName1 = $(this).text().trim();

        if (personName1 == personName && chks == '') {

            valid = valid * false;

            alert('Dublicate Person name');
        }

    });



    if (valid) {

        var chks = $(e).attr('data-edits');

        if (chks == '') {

            var len = $('#contact-table .srNo').length + 1;

            contact_adder(len, personName, department, mobile, email);

        } else {

            contact_adder_edit(chks, personName, department, mobile, email);

        }

        modals.clear('contactModal');

        $('#btn-contact').attr('data-edits', '');

        $('#myModal').modal('toggle');

    }

}


function contact_adder(len, personName, department, mobile, email) {

    var str = '<tr>';

    str += '<td align="center" class="srNo">' + len + '</td>';
    str += '<td align="center" class="name">' + personName + '</td>';
    str += '<td align="center" class="department">' + department + '</td>';
    str += '<td align="center" class="mobile">' + mobile + '</td>';
    str += '<td align="center" class="email">' + email + '</td>';
    str += '<td align="center"><button class="btn btn-sm btn-success" data-toggle="modal" data-target="#myModal" onclick="contactEditor(' + len + ',this) ">E</button>  <button class="btn btn-sm btn-danger remover" onclick="remover(this)">R</buuton></td>';
    str += '</tr>';

    $('#contact-table > tbody').append(str);

}



function contact_adder_edit(len, personName, department, mobile, email) {

    var str = '<td align="center" class="srNo">' + len + '</td>';

    str += '<td align="center" class="name">' + personName + '</td>';
    str += '<td align="center" class="department">' + department + '</td>';
    str += '<td align="center" class="mobile">' + mobile + '</td>';
    str += '<td align="center" class="email">' + email + '</td>';
    str += '<td align="center"><button class="btn btn-sm btn-success" data-toggle="modal" data-target="#myModal" onclick="contactEditor(' + len + ',this)">E</button>  <button class="btn btn-sm btn-danger remover" onclick="remover(this)">R</buuton></td>';

    $('#contact-table .srNo').each(function () {

        var srNo = $(this).text().trim();

        if (srNo == len) {

            $(this).parent().html(str);

        }

    });


}

function contactEditor(srNo, e) {

    $('#personName').val($(e).parent().parent().find('.name').text().trim());
    $('#department').val($(e).parent().parent().find('.department').text().trim());
    $('#mobile').val($(e).parent().parent().find('.mobile').text().trim());
    $('#email').val($(e).parent().parent().find('.email').text().trim());
    $('#btn-contact').attr('data-edits', srNo);

}


//---------------------------------Insert-----------------------------------//

function submit() {

    var valid = true;

    if (checker('section') != false) {

        valid = valid * true;

    } else {

        valid = valid * false;

    }

    if (modals.tabletdbyclass('contact-table') != false) {

        valid = valid * true;

    } else {

        valid = valid * false;

    }

    if (valid) {

        var data     = checker('section');

        var datastr  = JSON.stringify(data);

        var datastr2 = JSON.stringify(modals.tabletdbyclass('contact-table'));

        $.ajax({

            type: "POST",

            data: {
                data: datastr,
                data2: datastr2
            },

            url: 'insert.php',

            cache: false,

            success: function (res) {

                if (res.status == 'success') {

                    swal({

                        type: 'success',
                        title: 'Supplier Master Created',
                        showConfirmButton: false,
                        timer: 3000

                    });

                    setTimeout(function () {

                        window.location.reload();

                    }, 3000);

                }

                if (res.status == 'falid11') {

                    swal('GST Number Invalid', '', 'error');

                }

                if (res.status == 'falid22') {

                    swal('GST Number Already Exist', '', 'error');

                }

                if (res.status == 'falid1' || res.status == 'falid1') {

                    swal('Something Went Wrong', '', 'error');

                }

            }

        });

    }

}


//-----------------------------Edit----------------------------//

function edit(e) {

    $('#btn-submit').attr('onclick', 'update()');

    $('#btn-submit').text('Update');

    $('#contact-table > tbody > tr').remove();

    var supplierid = $(e).data('supplierid');

    $('#supplierid').val(supplierid);

    $.ajax({

        type: "POST",

        data: 'supplierid=' + supplierid,

        url: 'select.php',

        cache: false,

        success: function (res) {

            if (res.status == 'success') {
                
                closeNav1();

                modals.putvalue('section', res.supplierjson);

                var contact = res.contactjson;

                var count   = 1;

                for (var i in contact) {

                    contact_adder(count, contact[i].name, contact[i].department, contact[i].mobile, contact[i].email);

                    count++;
                }

            }

        }

    });

}


function update() {

    var valid = true;

    if (checker('section') != false) {

        valid = valid * true;

    } else {

        valid = valid * false;

    }

    if (modals.tabletdbyclass('contact-table') != false) {

        valid = valid * true;

    } else {

        valid = valid * false;

    }

    if (valid) {

        var data     = checker('section');

        var datastr  = JSON.stringify(data);

        var datastr2 = JSON.stringify(modals.tabletdbyclass('contact-table'));

        $.ajax({

            type: "POST",

            data: {
                data: datastr,
                data2: datastr2,
                supplierid: $('#supplierid').val()
            },

            url: 'update.php',

            cache: false,

            success: function (res) {

                if (res.status == 'success') {

                    swal({

                        type: 'success',
                        title: 'Supplier Master Updated',
                        showConfirmButton: false,
                        timer: 3000

                    });

                    setTimeout(function () {

                        window.location.reload();

                    }, 3000);

                }

                if (res.status == 'falid11') {

                    swal('GST Number Invalid', '', 'error');

                }

                if (res.status == 'falid1' || res.status == 'falid1') {

                    swal('Something Went Wrong', '', 'error');
                    
                }

            }
            
        });

    }

}

//-------------------------------Button Reset----------------------------//

$('#btn-reset').on('click', function () {

    window.location.reload();

});


</script>