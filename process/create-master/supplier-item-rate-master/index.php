<?php
$base='../../../';
$js='<script src="'.$base.'js/fwork.js"></script>
<script src="'.$base.'/js/select2.min.js"></script>
<script src="'.$base.'js/alart.js"></script>';
$css='<link rel="stylesheet" href="'.$base.'css/fwork.css">
<link href="'.$base.'/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" href="'.$base.'css/alart.css">';
include($base.'_in/header.php');
include($base.'_in/connect.php');

if (!in_array("Item Rate Master", $arrysession)) {
   
    echo "<script>window.location.href='/process/dashboard.php';</script>";
   
    exit;

}

$con=_connect();
$json1='';
$json2='';
$json3='';
$result1=mysqli_query($con,"SELECT supplierid,suppliername FROM suppliermaster");
while($rows1 = mysqli_fetch_assoc($result1)){
 $suppliername=$rows1["suppliername"];
 $supplierid=$rows1["supplierid"];
 $json1.=',{"sname":"'.$suppliername.'","scode":"'.$supplierid.'"}';   
}

$result2=mysqli_query($con,"SELECT stockid,itemname FROM stockmaster");
while($rows2 = mysqli_fetch_assoc($result2)){
 $icode=$rows2["stockid"];
 $iname=$rows2["itemname"];
 $json2.=',{"iname":"'.$iname.'","icode":"'.$icode.'"}';   
}



$json1=substr($json1,1);
$json1='['.$json1.']';

$json2=substr($json2,1);
$json2='['.$json2.']';


_close($con);

?>

<input type="hidden" id="item-json1" value='<?php echo str_replace("'"," &#39;",$json1); ?>'/>
<input type="hidden" id="item-json2" value='<?php echo str_replace("'"," &#39;",$json2); ?>'/>

<style>
hr.style-hr {
    border: 0;
    height: 1px;
    background-image: linear-gradient(to right, rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.75), rgba(0, 0, 0, 0));
}
.hiddenstyle{
    display:none;
}
.table-list td,
   .table-list th {
       border: 1px solid #ddd;
       padding: 1px !important;
       font-size: 13px;
   }
   .table-list td{
	padding-top: 10px !important;

   }

   .table-list tr:nth-child(even) {
       background-color: #f2f2f2;
   }

   .table-list th {
       padding-top: 5px;
       padding-bottom: 5px;
       text-align: center;
       background-color: #16a085;
       color: white;
   }

</style>
<div class="container-fluid">
    <div class="content">        
    <h2 align="center">Item Rate Master</h2>

        <hr class="style-hr">
        <div class="col-sm-3"></div>
            <div class="col-sm-2">Item Name</div>
            <div class="col-sm-4">
                <div class="form-group">
                    <select class="form-control input-sm select-js1" data-role="select" onchange="itemcode(this)" data-name="itemcode" id="itemcode">
                        <option value="Select">Select</option>
                    </select>
                </div>
            </div>
            <div style="clear:both;"></div>
            <hr class="style-hr">

            <div class="col-sm-12">
<table id="supplierRate" class="table table-list">
    <thead>
        <tr>
            <th width="20">SR.No</th>
            <th>Suppler Name</th>
            <th>Rate</th>
            <th>Remark</th>
        </tr>
    </thead>
    <tbody>
    </tbody>
</table>
            </div>
            


            <div style="clear:both;"></div>
            <hr class="style-hr">
            <div class="col-sm-3"></div>
            <div class="col-sm-3"><button class="btn btn-success btn-sm btn-block" id="btn-submit" onclick='insert()' style="margin:5px;">Submit</button></div>
            <div class="col-sm-3"><button class="btn btn-danger btn-sm btn-block" id="btn-reset" style="margin:5px;">Reset</button></div>
            <div class="col-sm-3"></div>
    </div>
</div>

<?php
include($base.'_in/footer.php');
?>
<script>

var strdb1='';
var strdb2='';
var strdb3='';
var count1=1;
var json1 = JSON.parse($("#item-json1").val());
for (var i in json1) {  
        strdb1 += '<tr>'
        strdb1 += '<td align="center">'+count1+'</td>'
        strdb1 += '<td align="center">'+ json1[i].sname +'</td>';
        strdb1 += '<td align="center" style="padding:0px!important;"><input class="form-control input-sm" type="number"></td>';
        strdb1 += '<td align="center" style="padding:0px!important;"><input class="form-control input-sm" type="text" readonly></td>';
        strdb1 += '</tr>';
        count1++;
}

var json2 = JSON.parse($("#item-json2").val());
for (var i in json2) {  
        strdb2 += '<option value="' + json2[i].icode + '">' + json2[i].iname + '</option>';
}

$('#supplierRate').append(strdb1);


$('#itemcode').append(strdb2);


$('#btn-reset').on('click',function(){
    window.location.reload();
    
});

function itemcode(e){
    var itemcode=$(e).val();

    $('#supplierRate > tbody > tr').each(function(){
            $(this).find('td:eq(2) input').val('');
            $(this).find('td:eq(3) input').prop('readonly',true);
    })

    $('#customerRate > tbody > tr').each(function(){
            $(this).find('td:eq(2) input').val('');
            $(this).find('td:eq(3) input').prop('readonly',true);
    })

   // $(':input').val('');
            $.ajax({
				type: "POST",
				data: 'itemcode='+itemcode,
				url: 'select.php',
				cache: false,
				success: function (res) {
					if (res.status == 'success') {
                        var json=res.json;
                          $('#supplierRate > tbody > tr').each(function(){
                             var sname=$(this).find('td:eq(1)').text().trim();
                             for(var i in json){
                                if(json[i].sname==sname){
                                    $(this).find('td:eq(2) input').val(json[i].rate);
                                    $(this).find('td:eq(3) input').prop('readonly',false);
                                }
                             }
                          })

                          $('#customerRate > tbody > tr').each(function(){
                             var cname=$(this).find('td:eq(1)').text().trim();
                             for(var i in json){
                                if(json[i].cname==cname){
                                    $(this).find('td:eq(2) input').val(json[i].rate);
                                    $(this).find('td:eq(3) input').prop('readonly',false);
                                }
                             }
                          })
                    }     
				}
			});
}

function insert() {
    var valid = true;
    var itemcode = $('#itemcode').val();
    if (itemcode != '') {
        valid = valid * true;
    } else {
        valid = valid * false;
    }
   var arr1=[];
    $('#supplierRate > tbody > tr').each(function () {
         sname=$(this).find('td:eq(1)').text().trim();
         rate=$(this).find('td:eq(2) input').val().trim();
         remark=$(this).find('td:eq(3) input').val().trim();
         if(rate!=''){
            arr1.push({
                "itemcode":itemcode,
                "sname":sname,
                "rate":rate,
                "remark":remark
            })
         }
    })



    var datastr1 = JSON.stringify(arr1);
    if (valid) {
        $.ajax({
            type: "POST",
            data: {
                    data1: datastr1
				},
            url: 'insert.php',
            cache: false,
            success: function (res) {
                if (res.status == 'success') {
                    swal({
							type: 'success',
							title: 'Item Rate Updated',
							showConfirmButton: false,
							timer: 3000
                        });
                        setTimeout(function () {
							window.location.reload();
						}, 3000);
                }
            }
        });
    }
}

$(document).ready(function() {
    $('.select-js1').select2({width: '100%'});
});

</script>