<?php

    $base = '../../../';

    include($base.'_in/connect.php');

    header('content-type: application/json; charset=utf-8');

    header("access-control-allow-origin: *");

    if(isset($_POST['data']) && isset($_POST['itemDetails'])) {

        $con = _connect();

        if (session_status()==PHP_SESSION_NONE) { session_start(); }

        $created_by   = $_SESSION['employeeid'];

        $data         = get_object_vars(json_decode($_POST["data"]));

        $branchid     = $data['branchid'];

        $employeeid   = $data['employeeid'];

        $category     = $data['category'];

        $itemDetails  = json_decode($_POST["itemDetails"]);

        $opning       = 0;
        
        $quality      = 0;
        
        $issue        = 0;
        
        $adjust_plus  = 0;
        
        $adjust_minus = 0;
        
        $outbranch    = 0;
        
        $outqty       = 0;
        
        $inbranch     = 0;
        
        $inqty        = 0;
        
        $total        = 0;
        
        $govqty       = 0;

        $in_qty       = 0;

        $out_qty      = 0;

        $balance_qty  = 0;

        foreach ($itemDetails as $i) {

            $itemid        = get_object_vars($i)['itemid'];

            $itemname      = get_object_vars($i)['itemname'];

            $attotmentqty  = get_object_vars($i)['attotmentqty'];

            $receivedyesno = get_object_vars($i)['receivedyesno'];

            $receivedqty   = get_object_vars($i)['receivedqty'];
            
            $selects       = mysqli_fetch_assoc(mysqli_query($con,"SELECT * FROM `employeestock` WHERE itemcode='$itemid' AND branchid='$branchid' AND employeeid = '$employeeid' ORDER BY id DESC LIMIT 1"));

            if ($selects) {

                $in_qty      = $selects['in_qty'] + $attotmentqty;

                $out_qty     = $selects['out_qty'] + $receivedqty;

                $balance_qty = $in_qty-$out_qty;
                
            } else {

                $in_qty        = $attotmentqty;

                $out_qty       = $receivedqty;

                $balance_qty   = $in_qty - $out_qty;

            }


            $select  = mysqli_fetch_assoc(mysqli_query($con,"SELECT * FROM `livestockmaster` WHERE itemcode='$itemid' AND branchid='$branchid' ORDER BY id DESC LIMIT 1"));

            if($select){

                $opning       = $select['opning'];
               
                $quality      = $select['quality'];
               
                $adjust_plus  = $select['adjust_plus'];
               
                $adjust_minus = $select['adjust_minus'];
               
                $outqty       = $select['outqty'] + $attotmentqty;
               
                $inqty        = $select['inqty']  + $receivedqty;
               
                $issue        = $select['issue'];
               
                $total        = ($opning+$quality+$adjust_plus+$inqty)-($issue+$adjust_minus+$outqty);
             
                $update       = mysqli_query($con,"INSERT into livestockmaster (branchid,itemcode,opning,quality,issue,adjust_plus,adjust_minus,outbranch,outqty,inbranch,inqty,total,created_by,created_time) Values ('$branchid','$itemid','$opning','$quality','$issue','$adjust_plus','$adjust_minus','$outbranch','$outqty','$inbranch','$inqty','$total','$created_by','$CURRENT_MILLIS')");

            }

            $insert = mysqli_query($con,"INSERT into employeestock (branchid, employeeid, catagory, itemcode, in_qty, received_yes_no, out_qty, balance_qty, created_by, created_time) Values ('$branchid', '$employeeid', '$category', '$itemid', '$in_qty', '$receivedyesno', '$out_qty', '$balance_qty', '$created_by', '$CURRENT_MILLIS')");

        }
      

        if ($insert) {

            echo '{"status":"success"}';

        } else {

            echo '{"status":"falid1"}';

        }
        
        _close($con);

    } else {

        echo '{"status":"falid"}';

    }
?>