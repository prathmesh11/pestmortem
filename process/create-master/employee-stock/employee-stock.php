<?php

    $base    = '../../';
    
    $navenq1 = 'background:#1B1464;';

    include('header.php');

    if (!in_array("Employee Stock", $arrysession)) {

        echo "<script>window.location.href='/process/dashboard.php';</script>";
    
        exit;

    }

    if (session_status() == PHP_SESSION_NONE) { session_start(); }
    
    $sessionby = $_SESSION['employeeid'];
    
    $branchid  = $_SESSION['branchid'];
  

?>

<style>

    hr.style-hr {
        border: 0;
        height: 1px;
        background-image: linear-gradient(to right, rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.75), rgba(0, 0, 0, 0));
    }

    .table-list td,
    .table-list th {
        border: 1px solid #ddd;
        padding: 2px;
        font-size: 13px;
    }

    .table-list tr:nth-child(even) {
        background-color: #f2f2f2;
    }

    .table-list th {
        padding-top: 12px;
        padding-bottom: 12px;
        text-align: left;
        background-color: #16a085;
        color: white;
    }


</style>

<div class="container-fluid">

    <br>

    <h2 align="center" style="margin-top:0px;">Employee Stock</h2>

    <div style="clear:both;"></div>
        
    <hr class="style-hr">

    <div class="col-sm-12" id="section">
            
        <input type="hidden" data-role="text" data-name ='branchid' value ="<?php echo $branchid; ?>" id='branchid'>

        <div class="col-sm-5">

            <div class="form-group" id="employeessidds">

                <label for="">Employee Name</label>
                
                <select class="form-control input-sm select-js" data-name="employeeid" id="employeeId" data-role="select" class="form-control input-sm" onchange="empstock()">

                    <option value="Select">Select Employee Name</option>

                    <?php

                        $result1 = mysqli_query($con,"SELECT employeeid,username FROM employeemaster WHERE branchid='$branchid' AND role = 'employee' OR role = 'None' ");

                        while($rows = mysqli_fetch_assoc($result1)){
                    
                            echo '<option value="'.$rows['employeeid'].'">'.$rows['username'].'</option>';

                        }
                                        
                    ?>

                </select>

            </div>

        </div>

        <div class="col-sm-3">

            <div class="form-group">

                <label for="">Category</label>
                
                <select class="form-control input-sm " data-name="category" id="category" data-role="select" class="form-control input-sm" onchange="itemName()">

                    <option value="Select">Select category</option>
                   
                    <option value="PC">Pest Control</option>
                   
                    <option value="FU">Fumigation</option>

                </select>

            </div>

        </div>
    
    </div>

    <div style="clear:both;"></div>
        
    <div class="col-sm-12">

        <div class="table-responsive" id="serviceTable">

            <table class="table table-list form-group" id="table-order">

                <thead>

                    <th width="2%">#</th>

                    <th width="20%">Item Name</th>

                    <th width="5%">Stock Qty</th>

                    <th width="5%">Employee Qty</th>

                    <th width="5%">Allotment Qty</th>

                    <th width="5%">Recevied Yes/No</th>

                    <th width="5%">Recevied Qty</th>
        
                    <th width="5%">Action</th>

                </thead>

                <tbody>

                    <tr>
                            
                        <td></td>

                        <td>

                            <div class="form-group">

                                <select class="select-js form-control input-sm" data-role='select' id="itemId" data-name="itemid" onchange="stockdetails()">

                                    <option value="Select">Select Item Name</option>

                                </select>

                            </div>
                            
                        </td>

                        <td>

                            <div class="form-group">

                                <input type="number" id="stoctQty" data-name="stoctQty" class="form-control input-sm" readonly>

                            </div>
                        
                        </td>

                        <td>

                            <div class="form-group">

                                <input type="number" id="employeeQty" class="form-control input-sm" readonly>

                            </div>
                        
                        </td>

                        <td>

                            <div class="form-group">

                                <input type="number" id="allotQty" data-name="attotment_qty" data-role="" class="form-control input-sm" onkeyup = "checkqty()">

                            </div>

                        </td>

                        <td>

                            <div class="form-group">

                                <select id="yesNo" data-name="received_yes_no" data-role="select" class="form-control input-sm" onchange="yesno()">

                                    <option value="Select">Select</option>

                                    <option value="No">No</option>

                                    <option value="Yes">Yes</option>
                                
                                </select>

                            </div>

                        </td>

                        <td>

                            <div class="form-group">

                                <input type="number" id="receivedQty" data-name="receivedqty" data-role="" class="form-control input-sm" onkeyup = "checkqty()">

                            </div>

                        </td>


                        <td>

                            <button class="btn btn-success btn-sm center-block" id="btn-add" data-edit="" onclick="add(this)">Add</button>

                        </td>

                    </tr>
                            
                </tbody>
                    
            </table>   

        </div>       
                
    </div>
        
    <div class="col-sm-12">
                
        <div style="clear:both;"></div>
            
        <hr class="style-hr">

        <div class="col-sm-3"> </div>
                
        <div class="col-sm-3">
                
            <button class="btn btn-success btn-sm btn-block" id="btn-submit" onclick="submit()" style="margin:5px;">Submit</button>
            
        </div>
            
        <div class="col-sm-3">
            
            <button class="btn btn-danger btn-sm btn-block" id="btn-reset" style="margin:5px;">Reset</button>
        
        </div>
               
        <div class="col-sm-3"></div>
        
    </div>


</div>

<?php
    
    include($base.'_in/footer.php');

?>

<script>


function itemName() {

    let category = $('#category').val();

    let branchid = $('#branchid').val();

    if (category != 'Select') {
        
        $.ajax({

            type: "POST",

            data: "category=" + category + "&branchid=" + branchid,

            url: 'itemname.php',

            cache: false,

            success: function (res) {

                if (res.status == 'success') {

                    let json = res.json;

                    str = '<option value= "Select">Select Item Name</option>';

                    for(let i in json){

                        str+='<option value = '+json[i].stockid+'>'+json[i].itemname+'</option>'

                    }

                    $('#itemId').html(str);

                } else {

                    $('#itemId').html('<option value= "Select">Select Item Name</option>');

                }

                stockdetails();

            }

        })


    } else {

        $('#itemId').html('<option value= "Select">Select Item Name</option>');

    }

    
}


function stockdetails() {

    let itemId = $('#itemId').val();

    let branchid = $('#branchid').val();

    if (itemId != 'Select') {

        $.ajax({

            type: "POST",

            data: "itemId=" + itemId + "&branchid=" + branchid,

            url: 'item-stock-details.php',

            cache: false,

            success: function (res) {

                if (res.status == 'success') {

                    $('#stoctQty').val(res.total);

                } else {

                    $('#stoctQty').val('');

                }

            }

        });      
        
    } else {

        $('#stoctQty').val('');

    }

    empstock();
}


function yesno() {

    let yesNo = $('#yesNo').val();

    if (yesNo == 'No') {

        $('#receivedQty').val('');

        $('#receivedQty').prop('readonly', true);

        $('#allotQty').prop('readonly', false);

        $('#allotQty').attr('data-role','number');

        $('#receivedQty').attr('data-role','');

    } else if(yesNo == 'Yes') {

        $('#receivedQty').prop('readonly', false);

        $('#allotQty').prop('readonly', true);

        $('#allotQty').val('');
        
        $('#allotQty').attr('data-role','');

        $('#receivedQty').attr('data-role','number');
 
    }
  
}

function checkqty() {

    let receivedQty = parseFloat($('#receivedQty').val());

    let allotQty    = parseFloat($('#allotQty').val());

    let stoctQty    = parseFloat($('#stoctQty').val());

    let employeeQty = parseFloat($('#employeeQty').val());

    if (isNaN(stoctQty)) {
        
        stoctQty = 0
    }

    if (isNaN(employeeQty)) {
        
        employeeQty = 0
    }


    if (!isNaN(allotQty)) {

        if (stoctQty < allotQty) {
            
            alert ('Allotment Qty Greater Than Stock Qty');

            $('#allotQty').val('');

        }
        
    } else if(!isNaN(receivedQty)){

        if (employeeQty < receivedQty) {
            
            alert ('Recevied Qty Greater Than Employee Qty');

            $('#receivedQty').val('');

        }

    }

    
}

function empstock() {

    let employeeId = $('#employeeId').val();

    let itemId     = $('#itemId').val();

    let branchid   = $('#branchid').val();

    if (itemId != 'Select' && employeeId != 'Select') {

        $.ajax({

            type: "POST",

            data: "itemId=" + itemId + "&branchid=" + branchid + "&employeeId=" + employeeId,

            url: 'employee-stock-qty.php',

            cache: false,

            success: function (res) {

                if (res.status == 'success') {

                    $('#employeeQty').val(res.total);

                }  else {

                    $('#employeeQty').val('');

                }

            }

        });      
        
    } else {

        $('#employeeQty').val('');

    }
    
}

//--------------------------Add Multiple Services start-------------------------------------//

function add(e) {

    let chk           = $(e).attr('data-edit');

    let data          = checker('table-order');

    let itemid        = data.itemid;

    let itemname      = $('#itemId').find(":selected").text();
    
    let attotmentqty  = data.attotment_qty;
    
    let receivedyesno = data.received_yes_no;

    let receivedqty   = data.receivedqty;

    let valid         = true;

    if (checker('table-order') != false) {

        valid = valid * true;

    } else {

        valid = valid * false;

    }

    $('#table-order .itemname').each(function () {

        let itemname1 = $(this).text().trim();

        if (itemname1 == itemname && chk == '') {

            valid = valid * false;

            alert('Dublicate Item Name');

        }

    });

    if (valid) {

        let chk = $(e).attr('data-edit');

        if (chk == '') {

            let len = $('#table-order .srNo').length + 1;

            fun_adder(len, itemid, itemname, attotmentqty, receivedyesno, receivedqty);

        } else {

            fun_adder_edit(chk, itemid, itemname, attotmentqty, receivedyesno, receivedqty);

        }

        modals.clear('table-order');

        $('#btn-add').attr('data-edit', '');

    }

}


function fun_adder(len, itemid, itemname, attotmentqty, receivedyesno, receivedqty) {

    let str  = '<tr id = '+len+'>';
        
        str += '<td align="center" class="srNo">' + len + '</td>';
        
        str += '<td align="center" class="itemid" style="display:none;">' + itemid + '</td>';
        
        str += '<td align="center" class="itemname" colSpan="3">' + itemname + '</td>';
        
        str += '<td align="center" class="attotmentqty">' + attotmentqty + '</td>';

        str += '<td align="center" class="receivedyesno">' + receivedyesno + '</td>';

        str += '<td align="center" class="receivedqty">' + receivedqty + '</td>';
        
        str += '<td align="center"><button class="btn btn-sm btn-success" onclick="contactEditor(' + len + ',this) ">E</button>  <button class="btn btn-sm btn-danger remover" onclick="removers(' + len + ',this)">R</buuton></td>';
    
    str += '</tr>';
    
    $('#table-order > tbody').append(str);

    removehidden();
}

function fun_adder_edit(len, itemid, itemname, attotmentqty, receivedyesno, receivedqty) {

    let str = '<td align="center" class="srNo">' + len + '</td>';
        
        str += '<td align="center" class="itemid" style="display:none;">' + itemid + '</td>';
        
        str += '<td align="center" class="itemname" colSpan="3">' + itemname + '</td>';
        
        str += '<td align="center" class="attotmentqty">' + attotmentqty + '</td>';

        str += '<td align="center" class="receivedyesno">' + receivedyesno + '</td>';

        str += '<td align="center" class="receivedqty">' + receivedqty + '</td>';
        
        str += '<td align="center"><button class="btn btn-sm btn-success" onclick="contactEditor(' + len + ',this)">E</button>  <button class="btn btn-sm btn-danger remover" onclick="removers(' + len + ',this)">R</buuton></td>';

    $('#table-order .srNo').each(function () {
        
        let srNo = $(this).text().trim();
        
        if (srNo == len) {
        
            $(this).parent().html(str);
       
        }
   
    });

}

function contactEditor(srNo, e) {

    let itemid = $(e).parent().parent().find('.itemid').text().trim();

    $('#itemId').val(itemid).trigger('change');
	
    $('#allotQty').val($(e).parent().parent().find('.attotmentqty').text().trim());

    $('#yesNo').val($(e).parent().parent().find('.receivedyesno').text().trim());

    $('#receivedQty').val($(e).parent().parent().find('.receivedqty').text().trim());

    $('#btn-add').attr('data-edit', srNo);

}

function removers(srNo,e) {

    $(e).parent().parent().remove();

    removehidden();
}

//-------------------------Remove Readonly Start ---------------------------//

function removehidden () {

    let tableOrderLenght = $('#table-order > tbody > tr').length - 1;

    if (tableOrderLenght == 0) {

        $('#category').prop('disabled', false);
        
    } else {

        $('#category').prop('disabled', 'disabled');

    }

}

//-------------------------Remove Readonly End ---------------------------//

//-------------------------Insert Start ---------------------------//

function submit() {

    let valid = true;

    if (checker('section') != false) {

        valid = valid * true;

    } else {

        valid = valid * false;

    }

    if ($('#employeeId').val() == 'Select') {

        $('#employeessidds').css('border-bottom', '2px solid red');

        valid = valid * false;

    } else {

        $('#employeessidds').css('border-bottom', '2px solid green');

        valid = valid * true;

    }

    let arr = [];

    $('#table-order > tbody > tr').each(function () {

        let srNo          = $(this).find('.srNo').text().trim();

        let itemid        = $(this).find('.itemid').text().trim();
        
        let itemname      = $(this).find('.itemname').text().trim();
                
        let attotmentqty  = $(this).find('.attotmentqty').text().trim();

        let receivedyesno = $(this).find('.receivedyesno').text().trim();

        let receivedqty   = $(this).find('.receivedqty').text().trim();
  
        arr.push({

            "srno": srNo,

            "itemid": itemid,

            "itemname": itemname,

            "attotmentqty": attotmentqty,

            "receivedyesno": receivedyesno,

            "receivedqty": receivedqty,

        });

    })

    arr.shift();

    if (arr == '') {

        $('#table-order').css('border', '2px solid red');

        valid = valid * false;

    } else {

        $('#table-order').css('border', '2px solid green');

        valid = valid * true;

    }

    if (valid) {

        let data1          = checker('section');
           
            data1          = JSON.stringify(data1);

            itemDetails = JSON.stringify(arr);

        $.ajax({
            
            type: "POST",
            
            data: {

                data: data1,

                itemDetails: itemDetails,

            },

            url: 'insert.php',
            
            cache: false,
            
            success: function (res) {
            
                if (res.status == 'success') {
            
                    swal({
                        
                        type: 'success',
                       
                        title: 'Commission Add',
                       
                        showConfirmButton: false,
                       
                        timer: 3000
                    
                    });
                   
                    setTimeout(function () {
                        
                        location.href = 'employee-stock.php';
                    
                    }, 3000);
                
                }

                if (res.status == 'falid1') {

                    swal('conatct No Or Email Id Duplicate', '', 'error');
                   
                    $('#inp-phone').css('border-bottom', '1px solid red');

                }

            }
        
        });
    
    }

}


$('#btn-reset').on('click', function () {
    window.location.reload();
});

$(document).ready(function () {

    $('.select-js').select2({

        width: '100%'

    });

    $('.select').attr('style', 'width:100%!important;');

    yesno();

});

</script>