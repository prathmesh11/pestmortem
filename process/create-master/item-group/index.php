<?php
$base='../../../';
$js='<script src="'.$base.'js/fwork.js"></script>
<script src="'.$base.'js/alart.js"></script>
<script src="'.$base.'js/list.min.js"></script>';
$css='<link rel="stylesheet" href="'.$base.'css/fwork.css">
<link rel="stylesheet" href="'.$base.'css/alart.css">';
$title="Item Master";
include($base.'_in/header.php');
include($base.'_in/connect.php');

if (!in_array("Item Group Master", $arrysession)) {
   
    echo "<script>window.location.href='/process/dashboard.php';</script>";
   
    exit;

}

$con=_connect();
$max=mysqli_fetch_assoc(mysqli_query($con,"SELECT max(id) x FROM mitemgroup"))['x'];
$max++;
?>
<style>

hr.style-hr {
    border: 0;
    height: 1px;
    background-image: linear-gradient(to right, rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.75), rgba(0, 0, 0, 0));
}
</style>

<div class="container-fluid" id="section" style="margin-left:5%;">
<h2 align="center" class="content">Item Group Master</h2>
    <hr class="style-hr">

<?php

$result=mysqli_query($con, "SELECT groupName FROM mitemgroup WHERE mainGroupCode='MG' ");
$count=1;
$count2=1001;
$count3=2001;
$count4=3001;
$count5=4001;
$count6=5001;
$count7=6001;
while($rows=mysqli_fetch_assoc($result)) {
 
    $groupName=$rows["groupName"];
    $style='';
    if($groupName=='MAIN GROUP') {
        $style='style="background:#000;float:right;"';
    }
    else {
        $style='style="width:95%;float:right;"';
    }

    echo '<div class="btn btn-sm btn-primary btn-block" data-main="main'.$count.'" onclick="change(this)" '.$style.'><button data-toggle="modal" data-target="#myModal" style="border-radius:50px;float:left;" data-group="'.$groupName.'" onclick="add(this)" class="btn btn-sm btn-success">+</button> MAIN GROUP > '.$groupName.' <button style="border-radius:50px;float:right;" data-group="'.$groupName.'" onclick="removeitem(this)" class="btn btn-sm btn-danger">-</button></div>';
    echo '<div class="main'.$count.'" style="display:none;">';
    $result1=mysqli_query($con, "SELECT groupName FROM mitemgroup WHERE mainGroupName='$groupName' AND mainGroupName<>'MAIN GROUP' ");
    while($rowss=mysqli_fetch_assoc($result1)) {
      
        $groupName1=$rowss["groupName"];
        echo '<div class=" btn btn-sm btn-warning btn-block" data-main="main'.$count2.'" onclick="change(this)" style="width:90%;float:right;background:#8e44ad;"><button data-toggle="modal" data-target="#myModal" style="border-radius:50px;float:left;" data-group="'.$groupName1.'" onclick="add(this)" class="btn btn-sm btn-success">+</button>MAIN GROUP > '.$groupName.' > '.$groupName1.'<button style="border-radius:50px;float:right;" data-group="'.$groupName1.'" onclick="removeitem(this)" class="btn btn-sm btn-danger">-</button> </div>';
        echo '<div class="main'.$count2.'" style="display:none;">';
        $result2=mysqli_query($con, "SELECT groupName FROM mitemgroup WHERE mainGroupName='$groupName1' ");
        while($rowsss=mysqli_fetch_assoc($result2)) {
          
            $groupName11=$rowsss["groupName"];
            echo '<div class="btn btn-sm btn-warning btn-block" data-main="main'.$count3.'" onclick="change(this)" style="width:85%;float:right;background:#b71540;">MAIN GROUP ><button data-toggle="modal" data-target="#myModal" style="border-radius:50px;float:left;" data-group="'.$groupName11.'" onclick="add(this)" class="btn btn-sm btn-success">+</button> '.$groupName.' > '.$groupName1.' > '.$groupName11.'<button style="border-radius:50px;float:right;" data-group="'.$groupName11.'" onclick="removeitem(this)" class="btn btn-sm btn-danger">-</button></div>';
            echo '<div class="main'.$count3.'" style="display:none;">';
            $result3=mysqli_query($con, "SELECT groupName FROM mitemgroup WHERE mainGroupName='$groupName11' ");
            while($rowssss=mysqli_fetch_assoc($result3)) {
             
                $groupName111=$rowssss["groupName"];
                echo '<div class="btn btn-sm btn-warning btn-block" data-main="main'.$count4.'" onclick="change(this)"  style="width:80%;float:right;background:#1B1464;"><button data-toggle="modal" data-target="#myModal" style="border-radius:50px;float:left;" data-group="'.$groupName111.'" onclick="add(this)" class="btn btn-sm btn-success">+</button>MAIN GROUP > '.$groupName.' > '.$groupName1.' > '.$groupName11.' > '.$groupName111.'<button style="border-radius:50px;float:right;" data-group="'.$groupName111.'" onclick="removeitem(this)" class="btn btn-sm btn-danger">-</button></div>';
                echo '<div class="main'.$count4.'" style="display:none;">';
                $result4=mysqli_query($con, "SELECT groupName FROM mitemgroup WHERE mainGroupName='$groupName111' ");
                while($rowsssss=mysqli_fetch_assoc($result4)) {
                   
                    $groupName1111=$rowsssss["groupName"];
                    echo '<div class="btn btn-sm btn-warning btn-block" data-main="main'.$count5.'" onclick="change(this)"  style="width:75%;float:right;background:#ffa801;"><button data-toggle="modal" data-target="#myModal" style="border-radius:50px;float:left;" data-group="'.$groupName1111.'" onclick="add(this)" class="btn btn-sm btn-success">+</button>MAIN GROUP > '.$groupName.' > '.$groupName1.' > '.$groupName11.' > '.$groupName111.' > '.$groupName1111.'<button style="border-radius:50px;float:right;" data-group="'.$groupName1111.'" onclick="removeitem(this)" class="btn btn-sm btn-danger">-</button></div>';
                    echo '<div class="main'.$count5.'" style="display:none;">';
                    $result5=mysqli_query($con, "SELECT groupName FROM mitemgroup WHERE mainGroupName='$groupName1111' ");
                    while($rowssssss=mysqli_fetch_assoc($result5)) {
                        
                        $groupName11111=$rowssssss["groupName"];
                        echo '<div class="btn btn-sm btn-warning btn-block" data-main="main'.$count6.'" onclick="change(this)"  style="width:70%;float:right;background:#27ae60;"><button data-toggle="modal" data-target="#myModal" style="border-radius:50px;float:left;" data-group="'.$groupName11111.'" onclick="add(this)" class="btn btn-sm btn-success">+</button>MAIN GROUP > '.$groupName.' > '.$groupName1.' > '.$groupName11.' > '.$groupName111.' > '.$groupName1111.' > '.$groupName11111.'<button style="border-radius:50px;float:right;" data-group="'.$groupName11111.'" onclick="removeitem(this)" class="btn btn-sm btn-danger">-</button></div>';
                        echo '<div class="main'.$count6.'" style="display:none;">';
                        $result6=mysqli_query($con, "SELECT groupName FROM mitemgroup WHERE mainGroupName='$groupName11111' ");
                        while($rowsssssss=mysqli_fetch_assoc($result6)) {
                          
                            $groupName111111=$rowsssssss["groupName"];
                            echo '<div class="btn btn-sm btn-warning btn-block" style="width:65%;float:right;background:#1e272e;"><button data-toggle="modal" data-target="#myModal" style="border-radius:50px;float:left;" data-group="'.$groupName111111.'" onclick="add(this)" class="btn btn-sm btn-success">+</button>MAIN GROUP > '.$groupName.' > '.$groupName1.' > '.$groupName11.' > '.$groupName111.' > '.$groupName1111.' > '.$groupName11111.' > '.$groupName111111.'<button style="border-radius:50px;float:right;" data-group="'.$groupName111111.'" onclick="removeitem(this)" class="btn btn-sm btn-danger">-</button></div>';
                           
                        }
                        $count6++;
                        echo '</div>';
                    }
                    $count6++;
                    echo '</div>';
                }
                $count4++;
        echo '</div>';
            }
            $count3++;
        echo '</div>';
        }
        $count2++;
        echo '</div>';
    }
    $count++;
echo '</div>';
}


?>
</div>
<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" align="center">Item Group Master</h4>
      </div>
      <div class="modal-body row">
      <div class="col-sm-4">Main Group Name</div>
      <div class="col-sm-8"><div class="form-group"> <input type="text" data-role="text" data-name="maingroupname" id="maingroupname" class="form-control input-sm" readonly></div> </div>
      <div class="col-sm-4 ">Sub Group Name</div>
      <div class="col-sm-8"><div class="form-group"> <input type="text" data-role="text" data-name="groupname"  class="form-control input-sm"></div> </div>
     
        <div class="col-sm-4">Title</div>
        <div class="col-sm-8"><div class="form-group"><input type="text" data-role="text" class="form-control input-sm" data-name="title"></div> </div>
          
        <div class="col-sm-4">Sub Group code</div>
        <div class="col-sm-8"><div class="form-group"><input type="text" placeholder="Sub Item Group" data-role="text" data-name="groupcode" id="codenamer"value="<?php echo $max; ?>" class="form-control input-sm" readonly> </div>
    
    </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success btn-block" onclick="submit()" >Submit</button>
      </div>
    </div>

  </div>
</div>

<?php
include($base.'_in/footer.php');
?>

<script>

function submit(){
    var valid=true;
    if(checker('myModal') !=false ) {
			valid=valid*true;
		}else{
		    valid=valid*false;
         }
         
    if(valid){
        data=checker('myModal');
        var datastr = JSON.stringify(data);
			$.ajax({
				type: "POST",
				data: {
					data: datastr
				},
				url: 'insert.php',
				cache: false,
				success: function (res) {
					if (res.status == 'success') {
						swal({
							type: 'success',
							title: 'New Item Group Created',
							showConfirmButton: false,
							timer: 3000
						});
						setTimeout(function () {
							window.location.reload();
						}, 3000);
                    }
                    if(res.status == 'falid2'){
                        swal('Item Group already Exist', '', 'error');
                    }
				}
			});
        }  
}

function removeitem(e){
   var groupname=$(e).data('group');
        swal({
            title: 'Are you sure? '+groupname,
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, Delete it!',
            cancelButtonText: 'No, keep it'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: "POST",
                    data: "groupname=" +encodeURIComponent(groupname),
                    url: 'remove.php',
                    cache: false,
                    success: function (res) {
                        if (res.status == 'success') {
                            swal(
                                'Delete!',
                                'Your Item Group has been Deleted.',
                                'success'
                            )
                            setTimeout(function () {
                                window.location.reload();
                            }, 3000);
                        }
                    }
                })
            } else if (result.dismiss === swal.DismissReason.cancel) {
                swal(
                    'Cancelled',
                    'Your Item Group is safe :)',
                    'error'
                )
            }
        })

}
function add(e){
$('#maingroupname').val($(e).data('group'));

}

function change(e){
    var main=$(e).data('main');
    $('.'+main).toggle();
}

</script>