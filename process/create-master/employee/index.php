<?php
    $base='../../../';
    $js='<script src="'.$base.'js/fwork.js"></script>
    <script src="'.$base.'js/alart.js"></script>
    <script src="'.$base.'js/list.min.js"></script>';
    $css='<link rel="stylesheet" href="'.$base.'css/fwork.css">
    <link rel="stylesheet" href="'.$base.'css/grid.min.css">
    <link rel="stylesheet" href="'.$base.'css/alart.css">';

    include($base.'_in/header.php');
    include($base.'_in/connect.php');

    if (!in_array("Employee Login Master", $arrysession)) {
   
        echo "<script>window.location.href='/process/dashboard.php';</script>";
       
        exit;
    
    }

    $con=_connect();

?>
<style>
    hr.style-hr {
        border: 0;
        height: 1px;
        background-image: linear-gradient(to right, rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.75), rgba(0, 0, 0, 0));
    }

    .table-list td,
    .table-list th {
        border: 1px solid #ddd;
        padding: 1px !important;
        font-size: 13px;
        text-align:center;
    }
   .table-list td{
	padding-top: 10px !important;

   }

   .table-list tr:nth-child(even) {
       background-color: #f2f2f2;
   }

   .table-list th {
       padding-top: 5px;
       padding-bottom: 5px;
       text-align: center;
       background-color: #16a085;
       color: white;
   }

   .sidenav1 {
       height: 100%;
       width: 0;
       position: fixed;
       z-index: 1;
       top: 0;
       left: 0;
       background-color: #111;
       overflow-x: hidden;
       transition: 0.5s;
       padding-top: 60px;
   }

   .sidenav1 a {
       padding: 8px 8px 8px 32px;
       text-decoration: none;
       font-size: 25px;
       color: #818181;
       display: block;
       transition: 0.3s;
   }

   .sidenav1 a:hover {
       color: #f1f1f1;
   }

   .sidenav1 .closebtn1 {
       position: absolute;
     
       right: 25px;
       font-size: 36px;
       margin-left: 50px;
   }

   @media screen and (max-height: 450px) {
       .sidenav1 {
           padding-top: 15px;
       }

       .sidenav1 a {
           font-size: 18px;
       }
   }

   .list {
       padding: 0px;
   }

   .list li span {
       display: inline;
   }

   .list li {
       text-decoration: none;
       display: block;
       padding: 2px;
       background: #FFF;
       border: 1px solid #333;
       color: #000;
   }
   input.largerCheckbox { 
            width: 20px; 
            height: 20px; 
        } 
</style>
<div class="container-fluid">

    <div class=" content" id="section">
        <span style="font-size:24px;cursor:pointer" onclick="openNav1()">&#9776; Search</span>
        <div id="mysidenav1" class="sidenav1" style="margin-left:80px;">

            <div id="test-list" style="padding:5px;">
                <a href="javascript:void(0)" class="closebtn1" onclick="closeNav1()">&times;</a>
                <br><br><br>
                <div class="form-group">
                    <input type="text" placeholder="Search" class="form-control input-sm fuzzy-search"
                        style="border-radius:3px;">
                </div>
                <ul class="list">
                <?php 
                        $result2=mysqli_query($con,"SELECT employeeid,username FROM employeemaster ORDER by username");
                        while($rows = mysqli_fetch_assoc($result2)){
                            $employeeid=$rows['employeeid'];
                            $username=$rows['username'];
                            echo '<li><span class="employee">' .$username. '<span><button style="margin-left:2px;" data-employeeid="' .$employeeid. '" onclick="edit(this)" class="btn btn-primary btn-sm"><span class="fa fa-edit"></span> E</button></i>';
                        }
                ?>
                </ul>
            </div>
        </div>

        <h2 align="center" style="margin-top:0px;">Employee Login Master</h2>

        <div style="clear:both;"></div>
        <hr class="style-hr">
        
        <div class="col-sm-12">
        <!-- <div class="col-sm-3"></div> -->
        <!-- <div class="col-sm-6"> -->

            
            <div class="col-sm-2">
            <div class="">Branch</div>
                <div class="form-group">
                    <select class="form-control input-sm" data-role="select" data-name="branchid">
                        <option value="Select">Select</option>
                        <?php 
                    $result1=mysqli_query($con, "SELECT * FROM `branchmaster`");
                    while($rows=mysqli_fetch_assoc($result1)) {
                        echo '<option value="'.$rows['branchid'].'">'.$rows['branchname'].'</option>';
                    }
                        ?>
                    </select>
                </div>
            </div>
         
            <div class="col-sm-2">
            <div class="">Employee Name</div>
                <div class="form-group">
                <input type="text" data-role='text' data-name='username'class="form-control input-sm">
                <input type="text" style="display:none;"  id="employeeid">
                        
                        </div>
            </div>
            
            <div class="col-sm-2">
            <div class="">Phone</div>
                <div class="form-group"><input type="text" data-role='text' data-name='phone'
                        class="form-control input-sm"></div>
            </div>
           
            <div class="col-sm-2">
            <div class="col-sm">Password</div>
                <div class="form-group"><input type="text" data-role='text' value="Password@123" data-name='password'
                        class="form-control input-sm"></div>
            </div>
            
            <div class="col-sm-2">
            <div class="col-sm">Role</div>
                <div class="form-group">
                    <select class="form-control input-sm" data-name="role">
                        <option value="None">None</option>
                        <option value="employee">Employee</option>
                        <option value="manager">Manager</option>
                        <option value="admin">Admin</option>
                        <option value="Hr">Hr</option>
                    </select>
                </div>
            </div>
          
            <div class="col-sm-2">
            <div class="col-sm">Email Id.</div>
                <div class="form-group"><input type="text"  data-name='email'
                        class="form-control input-sm"></div>
            </div>
           
            <div style="clear:both;"></div>
            <hr>
            <div class="col-sm-3"></div>
            <div class="col-sm-6">
            <table class="table-list table" id="accesstable">
            <thead>
            <tr>
            <th>Access</th>
            <th>Main Pages</th>
            <th>Action</th>
            </tr>
            </thead>
            <tbody>
            <tr>
            <td width="30px;"> <input type="checkbox" class="largerCheckbox" /></td>
            <td class="mainpage">DASHBOARD</td>
            <td><Button class="btn btn-primary btn-sm btn-block" data-subpage='["Employee Reports","Certificate Reports","Salary Reports"]' onclick="subpage(this)">Sub Pages</button></td>
            <td style="display:none;" class="classarr"></td>
            </tr>

            <tr>
            <td width="30px;"> <input type="checkbox" class="largerCheckbox" /></td>
            <td class="mainpage">LEDGER</td>
            <td><Button class="btn btn-primary btn-sm btn-block" data-subpage='["Customer Ledger" ,"Supplier Ledger"]' onclick="subpage(this)">Sub Pages</button></td>
            <td style="display:none;" class="classarr"></td>
            </tr>


            <tr>
            <td width="30px;"> <input type="checkbox" class="largerCheckbox" /></td>
            <td class="mainpage">OVERDUE</td>
            <td><Button class="btn btn-primary btn-sm btn-block" data-subpage='["Customer Bills" ,"Supplier Bills"]' onclick="subpage(this)">Sub Pages</button></td>
            <td style="display:none;" class="classarr"></td>
            </tr>

            <tr>
            <td width="30px;"> <input type="checkbox" class="largerCheckbox" /></td>
            <td class="mainpage">ENQUIRY</td>
            <td><Button class="btn btn-primary btn-sm btn-block" data-subpage='["Enquiry & FollowUp" ,"Confirm Enquiry" ,"Close Enquiry"]' onclick="subpage(this)">Sub Pages</button></td>
            <td style="display:none;" class="classarr"></td>
            </tr>

            <tr>
            <td width="30px;"> <input type="checkbox" class="largerCheckbox" /></td>
            <td class="mainpage">QUOTATION</td>
            <td><Button class="btn btn-primary btn-sm btn-block" data-subpage='["Quotation"]' onclick="subpage(this)">Sub Pages</button></td>
            <td style="display:none;" class="classarr"></td>
            </tr>

            <tr>
            <td width="30px;"> <input type="checkbox" class="largerCheckbox" /></td>
            <td class="mainpage">AMC</td>
            <td><Button class="btn btn-primary btn-sm btn-block" data-subpage='["Annual Work Contract"]' onclick="subpage(this)">Sub Pages</button></td>
            <td style="display:none;" class="classarr"></td>
            </tr>
            
            <tr>
            <td width="30px;"> <input type="checkbox" class="largerCheckbox" /></td>
            <td class="mainpage">CRM</td>
            <td><Button class="btn btn-primary btn-sm btn-block" data-subpage='["Work Contract","Job Allotment","Job Completed","Reschedule Job"]' onclick="subpage(this)">Sub Pages</button></td>
            <td style="display:none;" class="classarr"></td>
            </tr>
            <tr>

            <tr>
            <td width="30px;"> <input type="checkbox" class="largerCheckbox" /></td>
            <td class="mainpage">INVOICE</td>
            <td><Button class="btn btn-primary btn-sm btn-block" data-subpage='["Fumigation Invoice","Pestcontrol Invoice","Product Sale Bill Invoice","Proforma Bill Invoice"]' onclick="subpage(this)">Sub Pages</button></td>
            <td style="display:none;" class="classarr"></td>
            </tr>

            <tr>
            <td width="30px;"> <input type="checkbox" class="largerCheckbox" /></td>
            <td class="mainpage">INVOICE PAYMENT</td>
            <td><Button class="btn btn-primary btn-sm btn-block" data-subpage='["Fumigation Invoice Payment"]' onclick="subpage(this)">Sub Pages</button></td>
            <td style="display:none;" class="classarr"></td>
            </tr>

            <tr>
            <td width="30px;"> <input type="checkbox" class="largerCheckbox" /></td>
            <td class="mainpage">AMC</td>
            <td><Button class="btn btn-primary btn-sm btn-block" data-subpage='["Work Contract"]' onclick="subpage(this)">Sub Pages</button></td>
            <td style="display:none;" class="classarr"></td>
            </tr>

            <tr>
            <td width="30px;"> <input type="checkbox" class="largerCheckbox" /></td>
            <td class="mainpage">CERTIFICATION</td>
            <td><Button class="btn btn-primary btn-sm btn-block" data-subpage='["ALP","MBR","AFAS","WPM","Calculation"]' onclick="subpage(this)">Sub Pages</button></td>
            <td style="display:none;" class="classarr"></td>
            </tr>

            <tr>
            <td width="30px;"> <input type="checkbox" class="largerCheckbox" /></td>
            <td class="mainpage">CALCULATION</td>
            <td><Button class="btn btn-primary btn-sm btn-block" data-subpage='["Calculation","reports"]' onclick="subpage(this)">Sub Pages</button></td>
            <td style="display:none;" class="classarr"></td>
            </tr>

            <tr>
            <td width="30px;"> <input type="checkbox" class="largerCheckbox" /></td>
            <td class="mainpage">PURCHASE</td>
            <td><Button class="btn btn-primary btn-sm btn-block" data-subpage='["Purchase Requisition","Approval Order","Purchase Order","Gate Entry","Purchase Bill","Payment Bill","Reporting"]' onclick="subpage(this)">Sub Pages</button></td>
            <td style="display:none;" class="classarr"></td>
            </tr>

            <tr>
            <td width="30px;"> <input type="checkbox" class="largerCheckbox" /></td>
            <td class="mainpage">STOCK</td>
            <td><Button class="btn btn-primary btn-sm btn-block" data-subpage='["Material Issue","Material Receive","Branch To Branch Trasfer","Stock","Government Stock"]' onclick="subpage(this)">Sub Pages</button></td>
            <td style="display:none;" class="classarr"></td>
            </tr>
            
            <tr>
            <td width="30px;"> <input type="checkbox" class="largerCheckbox" /></td>
            <td class="mainpage">MASTER</td>
            <td><Button class="btn btn-primary btn-sm btn-block" data-subpage='["Additional Master","Item Group Master","Item Master","Item Master ReOrder","Supplier Master","Item Rate Master","Customer Master","Branch Master","Service Master","Employee Login Master","Machine/Assets Master","Commodity Master","Accreditation Master","Country Master","Temperature Master","Sales Person Master","Document","Employee Stock","Employee Stock Report","Account Head","Expenses"]' onclick="subpage(this)">Sub Pages</button></td>
            <td style="display:none;" class="classarr"></td>
            </tr>
            
            <tr>
            <td width="30px;"> <input type="checkbox" class="largerCheckbox" /></td>
            <td class="mainpage">SALARY_MASTER</td>
            <td><Button class="btn btn-primary btn-sm btn-block" data-subpage='["Salary Master","Leave Opening Balance","Professional Tax Master","Paid Holidays","Monthly Attendance","Leave Allotment","Loan Advance Details","Salary Processing"]' onclick="subpage(this)">Sub Pages</button></td>
            <td style="display:none;" class="classarr"></td>
            </tr>

            <tr>
            <td width="30px;"> <input type="checkbox" class="largerCheckbox" /></td>
            <td class="mainpage">COMMISSION_MASTER</td>
            <td><Button class="btn btn-primary btn-sm btn-block" data-subpage='["Commission Request","Commission Approval","Bill Paid","Commission Master Agent"]' onclick="subpage(this)">Sub Pages</button></td>
            <td style="display:none;" class="classarr"></td>
            </tr>

            <tr>
            <td width="30px;"> <input type="checkbox" class="largerCheckbox" /></td>
            <td class="mainpage">CREDIT AND DEBIT NOTE</td>
            <td><Button class="btn btn-primary btn-sm btn-block" data-subpage='["Credit Note","Debit Note"]' onclick="subpage(this)">Sub Pages</button></td>
            <td style="display:none;" class="classarr"></td>
            </tr>
           
            </tbody>
            </table>
            </div>
            <div style="clear:both;"></div>
            <div class="col-sm-3"></div>
            <div class="col-sm-3">
            <button class="btn btn-success btn-sm btn-block" id="btn-submit" onclick="submit()" style="margin:5px;">Submit</button></div>
            <div class="col-sm-3"><button class="btn btn-danger btn-sm btn-block" id="btn-reset"style="margin:5px;">Reset</button></div>
          

        <div class="col-sm-12">
            <div style="clear:both;"></div>
            <hr class="style-hr">

            <div class="col-sm-3">
            </div>
            
        </div>
    </div>
</div>
<br>
<br>

<!-- Modal -->
<div class="modal fade" id="myModal1" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title" align="center"></h4>
        </div>
        <div class="modal-body">
        <table class="table-list table">
            <thead>
            <tr>
            <th>Access</th>
            <th>Sub Pages</th>
            </tr>
            </thead>
            <tbody>           
            </tbody>
            </table>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary btn-sm btn-block" id="savetemp" data-dismiss="modal">Submit</button>
        </div>
      </div>
      
    </div>
  </div>

<?php
include($base.'_in/footer.php');
?>
<script>
function tempaddress(e){
    $('#peraddress').val($(e).val());
} 

function openNav1() {
    document.getElementById("mysidenav1").style.width = "300px";
}

function closeNav1() {
    document.getElementById("mysidenav1").style.width = "0";
}

var monkeyList = new List('test-list', {
    valueNames: ['employee']
});

function subpage(e){
    $('#myModal1 table > tbody > tr').remove();
    var headername=$(e).parent().parent().find('.mainpage').text().trim();
    $('#myModal1 .modal-title').text(headername+' SUB PAGES ACESSS');
    var arr=$(e).data('subpage');
    var str='';
    for(var i in arr){
        str+='<tr>';
        str+='<td width="30px;"> <input type="checkbox" class="largerCheckbox" /></td>';
        str+='<td class="subpage">'+arr[i]+'</td>';
        str+='</tr>';
    }
    $('#myModal1 table > tbody').append(str); 
    $('#myModal1').modal('show');
    $('#savetemp').attr('onclick','savetemp("'+headername+'")');
    var json=$(e).parent().parent().find('.classarr').text().trim();
    if(json!=''){
        json= JSON.parse(json);  
        for(var i in json){
            $('#myModal1 table > tbody > tr').each(function (){
                var subpage=$(this).find('.subpage').text().trim();
                if(subpage==json[i].subpage && json[i].status==true){
                    $(this).find('input').prop( "checked",true);
                }
            });
        }
    }
}

function savetemp(e){
    var arr=[];
    $('#myModal1 table > tbody > tr').each(function (){
        var status=$(this).find('.largerCheckbox').is(":checked");
        arr.push({
            "subpage":$(this).find('.subpage').text().trim(),
            "status":status
        });
    });
    
    $('#accesstable > tbody > tr').each(function (){
        var mainpage=$(this).find('.mainpage').text().trim();
        if(mainpage==e){
            $(this).find('.classarr').text(JSON.stringify(arr));
        }
    });
}

function collectdata(){
    var arr=[];
   $('#accesstable > tbody > tr').each(function (){
    var classarr= $(this).find('.classarr').text().trim();
   if(classarr==''){
    classarr='[]';
   }
    arr.push({
        "mainpage":$(this).find('.mainpage').text().trim(),
        "status":$(this).find('.largerCheckbox').is(":checked"),
        "subpage": JSON.parse(classarr)
    });
    });
    return JSON.stringify(arr);
}

function submit(){
    var valid=true;
    if(checker('section')!=false) {
			valid=valid*true;
		}else{
		    valid=valid*false;
         }

    if(valid){
        var data=checker('section');
        var datastr = JSON.stringify(data);
			$.ajax({
				type: "POST",
				data: {
                    data: datastr,
                    data1:collectdata()
				},
				url: 'insert.php',
				cache: false,
				success: function (res) {
					if (res.status == 'success') {
						swal({
							type: 'success',
							title: 'Employee Created',
							showConfirmButton: false,
							timer: 3000
						});
						setTimeout(function () {
							window.location.reload();
						}, 3000);
                    }
                    if(res.status == 'falid2'){
                        swal('Employee already Exist', '', 'error');
                        $('#inp-text-1').val('');
                        $('#inp-phone').css('border-bottom', '1px solid red');
                    }
				}
			});
    }
}


function edit(e){
    $('#accesstable > tbody > tr').each(function (){
            $(this).find('input').prop( "checked",false);
    });

$('#btn-submit').attr('onclick','update()');
$('#btn-submit').text('Update');
var employeeid=$(e).data('employeeid');
    $('#employeeid').val(employeeid);
			$.ajax({
				type: "POST",
				data: 'employeeid='+ employeeid,
				url: 'select.php',
				cache: false,
				success: function (res) {
					if (res.status == 'success') {
                        modals.putvalue('section',res.employeejson);
                        var json=res.pagearr;
                        for(var i in json){
                            $('#accesstable > tbody > tr').each(function (){
                               var mainpage= $(this).find('.mainpage').text().trim();
                               if(mainpage==json[i].mainpage && json[i].status==true){
                                    $(this).find('input').prop( "checked",true);
                                    $(this).find('.classarr').text(JSON.stringify(json[i].subpage));
                               }
                            });
                        }

                        closeNav1();
                    }
				}
            });
}


function update(){
    var valid=true;
    if(checker('section')!=false) {
			valid=valid*true;
		}else{
		    valid=valid*false;
         }
    if(valid){
        var data=checker('section');
        var datastr = JSON.stringify(data);
			$.ajax({
				type: "POST",
				data: {
                    data: datastr,
                    data1:collectdata(),
                    employeeid: $('#employeeid').val()
				},
				url: 'update.php',
				cache: false,
				success: function (res) {
					if (res.status == 'success') {
						swal({
							type: 'success',
							title: 'Employee Updated',
							showConfirmButton: false,
							timer: 3000
						});
						setTimeout(function () {
							window.location.reload();
						}, 3000);
                    }
				}
			});
    }
}

</script>