<?php
$base='../../../';
 
$js=' <script src="'.$base.'js/fwork.js"></script>
<script src="'.$base.'js/alart.js"></script> 
<script src="'.$base.'js/list.min.js"></script>
<script src="'.$base.'js/picker.js"></script>
<script src="'.$base.'js/picker.date.js"></script>
<script src="'.$base.'js/picker.time.js"></script>
<script src="'.$base.'js/pdfmake.min.js"></script>
<script src="'.$base.'js/vfs_fonts.js"></script>
 ';

$css='<link rel="stylesheet" href="'.$base.'css/fwork.css">
<link rel="stylesheet" href="'.$base.'css/alart.css">
<link rel="stylesheet" href="'.$base.'css/grid.min.css">
<link rel="stylesheet" href="'.$base.'css/classic.date.css">
<link rel="stylesheet" href="'.$base.'css/classic.time.css">
<link rel="stylesheet" href="'.$base.'css/classic.css">';
include($base.'_in/header.php');
include($base.'_in/connect.php');

if (!in_array("Loan Advance Details", $arrysession)) {
   
    echo "<script>window.location.href='/process/dashboard.php';</script>";
   
    exit;

}

$con=_connect();
$json1='';
$result=mysqli_query($con,"SELECT * FROM employeemaster ORDER BY username ASC");
while($rows = mysqli_fetch_assoc($result)){
    $json1.=',{"branchid":"'.$rows['branchid'].'","employeeid":"'.$rows['employeeid'].'","username":"'.$rows['username'].'"}';
}
$json1=substr($json1,1);
$json1='['.$json1.']';

 $json2='';
$result=mysqli_query($con,"SELECT * FROM leaveopeningbalance ORDER BY id ASC");
while($rows = mysqli_fetch_assoc($result)){
    $json2.=',{"employeeid":"'.$rows['employeeid'].'","paid_leave":"'.$rows['paid_leave'].'","casual_leave":"'.$rows['casual_leave'].'","standanrd_leave":"'.$rows['standanrd_leave'].'"}';
}
$json2=substr($json2,1);
$json2='['.$json2.']';

?>

<style>

hr.style-hr {
    border: 0;
    height: 1px;
    background-image: linear-gradient(to right, rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.75), rgba(0, 0, 0, 0));
}

</style>
<style>
hr.style-hr {
    border: 0;
    height: 1px;
    background-image: linear-gradient(to right, rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.75), rgba(0, 0, 0, 0));
}
.table-list td,
   .table-list th {
       border: 1px solid #ddd;
       padding: 1px !important;
       font-size: 13px;
   }
   .table-list td{
    padding-top: 10px !important;

   }

   .table-list tr:nth-child(even) {
       background-color: #f2f2f2;
   }

   .table-list th {
       padding-top: 5px;
       padding-bottom: 5px;
       text-align: center;
       background-color: #16a085;
       color: white;
   }
</style>
<div class="container-fluid" id="section">
    <div class=" content">
        <h2 align="center" style="margin:0;">Loan Advance Details</h2>
        <hr class="style-hr">
      <div class="col-sm-12">
        <div class="col-sm-6">
      
        <div class="col-sm-12" id="section1">
           <input type="hidden" id="inp-branch" value='<?php echo $branchid; ?>' />
        <input type="hidden" id="item-json1" value='<?php echo str_replace("'"," &#39;",$json1); ?>' />
        <input type="hidden" id="item-json2" value='<?php echo str_replace("'"," &#39;",$json2); ?>' />
 
           <div class="col-sm-12">
            <div class="col-sm-4">Branch Name</div>
                <div class="col-sm-8">
                    <div class="form-group">
                    <select class="form-control input-sm" data-role="select" data-name="branchid" id="branchid" onchange="empmaster(this);" >
                        <option value="Select">Select</option>
                            <?php 
                                    $result1=mysqli_query($con, "SELECT * FROM `branchmaster`");
                                    while($rows=mysqli_fetch_assoc($result1)) {
                                    echo '<option value="'.$rows['branchid'].'">'.$rows['branchname'].'</option>';
                                }
                            ?>
                        </select>
                    </div>
                </div>
                </div>

                </div>
          <div class="col-sm-12" id="section2">
             
            <div class="col-sm-12">
                <div class="col-sm-4">Employee Name</div>
                <div class="col-sm-8">
                    <div class="form-group">
                     
                       <div class="form-group">
                        <select class="form-control input-sm" data-role="select" data-name="employeeid" id="employeeid">
                        <option value="Select">Select</option>
                         
                    </select>
               
                    </div>
                </div>

            </div>
            </div>
        </div>
            
        <div class="col-sm-12" id="section2">
             
            <div class="col-sm-12">
                <div class="col-sm-4">Application No. </div>
                <div class="col-sm-8">
                    <div class="form-group">
                     
                        <div class="form-group"><input type="text" data-role="text" data-name="appno" id="appno" class="form-control input-sm" ></div>
               
                    </div>
                </div>

            </div>
            
        </div>
            
         
         
        <div class="col-sm-12" id="section1">
           
            <div class="col-sm-12">
                <div class="col-sm-4">Date</div>
                <div class="col-sm-8">
                    <div class="form-group">
                     
                    <input type="text"  data-name="advancedate" data-role="text" id="advancedate" class="form-control sm datepicker">
                   
                    </div>
                </div>
                </div>
           
            
        </div>
        </div>
        <div class="col-sm-6">
      
          
          <div class="col-sm-12" >
             <div class="col-sm-12">
                <div class="col-sm-4">Amount</div>
                <div class="col-sm-8">
                    <div class="form-group">
                     
                        <div class="form-group"><input type="text" data-role="number" data-name="amount" id="amount" class="form-control input-sm" placeholder="0.00"></div>
               
                    </div>
                </div>

            </div>
            
        </div>
          <div class="col-sm-12">
            <div class="col-sm-12">
                <div class="col-sm-4">Ded. Amount </div>
                <div class="col-sm-8">
                    <div class="form-group">
                     
                        <div class="form-group"><input type="text" data-role="number" data-name="dedamt" id="dedamt" class="form-control input-sm" placeholder="0.00"></div>
               
                    </div>
                </div>

            </div>
            
        </div>
            
           <div class="col-sm-12" >
             <div class="col-sm-12">
                <div class="col-sm-4">Interest % </div>
                <div class="col-sm-8">
                    <div class="form-group">
                      <input type="text" data-role="number" data-name="interest"  id="interest" class="form-control input-sm" placeholder="0.00"> 
               
                    </div>
                </div>

            </div>
            
        </div>
         <div class="col-sm-12" id="section2">
             <div class="col-sm-12">
                <div class="col-sm-4">Type</div>
                <div class="col-sm-8">
                    <div class="form-group">
                      <input type="radio" data-name="loantype" name="loantype" value="Loan" placeholder="0.00" checked> Loan <BR>
                      <input type="radio" data-name="loantype" name="loantype" value="Other Loan" placeholder="0.00"> Other Loan <BR>
                      <input type="radio"  data-name="loantype" name="loantype" value="Advance" placeholder="0.00"> Advance <BR>
                      <input type="radio"  data-name="loantype" name="loantype" value="Festival Advance" placeholder="0.00"> Festival Advance <BR>

                    </div>
                </div>

            </div>
            
        </div>
          </div>
          </div>           
        <div style="clear:both;"></div>
        <hr class="style-hr">
        <div class="col-sm-3"></div>
        <div class="col-sm-6">
            <div class="col-sm-6"><button class="btn btn-success btn-sm btn-block" id="btn-submit"
                    style="margin:5px;">Submit</button></div>
            <div class="col-sm-6"><button class="btn btn-danger btn-sm btn-block" id="btn-reset"
                    onclick="$('#section').find('select').val('');$('#section').find('input').val('');" style="margin:5px;">Reset</button></div>
        </div>
        <div class="col-sm-3"></div>
        <table class="table-list table table-bordered">
    <thead>
      <tr>
        <th>SR. No</th>
        <th>Branch Name</th>
        <th>Emp Name</th>
        <th>Advance Date</th>
        <th>Amount</th>
        <th>Ded. Amt</th>
        <th>Interest</th>
        <th>Type</th>
         <th>Action</th>
      </tr>
    </thead>
    <tbody>
    <?php
                    $count=0;
                    $result4=mysqli_query($con,"SELECT * FROM loanadvancemaster ORDER BY id ASC" );
                     while($rows3 = mysqli_fetch_assoc($result4)){
                         $count++;

                         $resemp=mysqli_query($con,"SELECT * FROM employeemaster WHERE employeeid = '".$rows3['employeeid']."'" );
                         $rowsemp = mysqli_fetch_assoc($resemp);
                         $empname =  $rowsemp['username'];
                      $branchid=$rows3['branchid'];
                      $branchname=mysqli_fetch_assoc(mysqli_query($con,"SELECT branchname x FROM branchmaster WHERE branchid='$branchid'"))['x'];


                      $advancedate= date('d/m/Y',$rows3['advancedate']/1000);

                     echo '<tr>';
                     echo '<td align="center">'.$count.'</td>';
                     echo '<td align="center">'.$branchname.'</td>';
                     echo '<td align="center">'.$empname.'</td>';
                     
                     echo '<td align="center">'.$advancedate.'</td>';
                     
                     echo '<td align="center">'.$rows3['amount'].'</td>';
                     echo '<td align="center">'.$rows3['dedamt'].'</td>';
                     echo '<td align="center">'.$rows3['interest'].'</td>';
                     echo '<td align="center">'.$rows3['loantype'].'</td>';
                     echo '<td align="center"><button class="btn btn-primary btn-sm " data-toggle="modal" data-target="#myModal2" data-ptid="'.$rows3['id'].'" onclick="setidtomidal(this)">Edit</button></td>';
                     echo '<tr>';
                    }
    ?>
    </tbody>
  </table>
    </div>
</div>
<div id="myModal2" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" align="center">Edit Loan Advance </h4>
      </div>
      <div class="modal-body">
          <div class="form-group row">
       <div class="col-sm-4">Advance Date</div>
       <input type="hidden" data-name="ptid" id="ptidset">
       <div class="col-sm-8">
       <input type="text" data-name="advancedate"  data-role="text" class="form-control input-sm datepicker">
       </div>
       <div class="col-sm-4">Amount</div>
       <div class="col-sm-8"><input type="text" data-name="amount" data-role="number" id="amount" class="form-control input-sm"></div>
       <div class="col-sm-4">Deduction Amount</div>
       <div class="col-sm-8"><input type="text" data-name="dedamt"  data-role="number" id="dedamt" class="form-control input-sm"></div>
       <div class="col-sm-4">Interest %</div>
       <div class="col-sm-8"><input type="text" data-name="interest"  data-role="number" id="interest" class="form-control input-sm"></div>
       <div class="col-sm-4">Type </div>
       <div class="col-sm-8">
 
      <select class="form-control input-sm" data-role="select" data-name="loantype" id="loantype">
            <option value="Select">Select</option>
            <option value="Loan">Loan</option>
            <option value="Other Loan">Other Loan</option>
            <option value="Advance">Advance</option>
            <option value="Festival Advance">Festival Advance</option>
                         
                    </select>
       </div>
       
       <div class="col-sm-4"></div>
       <div class="col-sm-8"><button class="btn btn-success btn-block btn-sm" onclick="edit()">Save</button></div>
        </div>
        
      </div>
    </div>
  </div>
</div>

<?php
include($base.'_in/footer.php');
?>
<style type="text/css">
.picker__select--year{
  height:auto;
}

.picker__select--month{
  height:auto;
}
</style>
<script>
function setidtomidal(e) {
    var ptid = $(e).data('ptid');
    $('#ptidset').val(ptid);
    $.ajax({
                type: "POST",
                data: 'ptid='+ptid,
                url: 'loandetails.php',
                cache: false,
                success: function (res) {
                 // alert(res.status);
                    if(res.status=='success'){
                        modals.putvalue('myModal2',res.json);
                    }
                }
            });
}
function edit(e){
var valid=true;
    if(checker('myModal2') !=false ) {
            valid=valid*true;
        }else{
            valid=valid*false;
         }
    if(valid){
        var data1=checker('myModal2');
         data1 = JSON.stringify(data1);
        $.ajax({
                type: "POST",
                data: {
                    data1: data1      
                },
                url: 'update.php',
                cache: false,
                success: function (res) {
                    if(res.status=='success'){
                        swal({
                            type: 'success',
                            title: 'Your Work is Save',
                            showConfirmButton: false,
                            timer: 2000
                        });
                        setTimeout(function () {
                            window.location.reload();
                        }, 2000);
                    }
                }
            });
    }
}
$('.datepicker').pickadate({     
    selectYears: 100,
  selectMonths: true,
  onClose: function () {
    try {
      var dateStamp = this.get('select')['pick'];
      var id = this.get('id');
      $('#' + id).attr('data-stamp', dateStamp);
      $('#' + id).parent().find('.timepicker').click();
    } catch (err) {
      console.log(err);
    }
  }
});
 


 function empmaster(e){    
    var json1=JSON.parse($('#item-json1').val());
    var branchid=$(e).val();
    var str='<option value="Select">Select</option>';
    for(var i in json1){
        if(json1[i].branchid==branchid){
        str+='<option value="'+json1[i].employeeid+'">'+json1[i].username+'</option>';
        }
    }
    $('#employeeid').html(str);
}

$('#btn-submit').on('click',function(){
    var valid=true;
    if(checker('section') !=false ) {
            valid=valid*true;
        }else{
            valid=valid*false;
         }
    if(valid){
        var data1=checker('section');
         data1 = JSON.stringify(data1);
        $.ajax({
                type: "POST",
                data: {
                    data: data1      
                },
                url: 'insert.php',
                cache: false,
                success: function (res) {
                  console.log(res.status);
                    if(res.status=='success'){
                        swal({
                            type: 'success',
                            title: 'Your Work is Save',
                            showConfirmButton: false,
                            timer: 2000
                        });
                        setTimeout(function () {
                            window.location.reload();
                        }, 2000);
                    }
                      
                    
                }
            });
    }
})

function getempid(e){
     
    var empid = $(e).val();
    $('#empcode').val(empid);
         
}


</script>