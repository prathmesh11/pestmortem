<?php
$base='../../../';
 
$js=' <script src="'.$base.'js/fwork.js"></script>
<script src="'.$base.'js/alart.js"></script> 
<script src="'.$base.'js/list.min.js"></script>
<script src="'.$base.'js/picker.js"></script>
<script src="'.$base.'js/picker.date.js"></script>
<script src="'.$base.'js/picker.time.js"></script>
<script src="'.$base.'js/pdfmake.min.js"></script>
<script src="'.$base.'js/vfs_fonts.js"></script>
 ';

$css='<link rel="stylesheet" href="'.$base.'css/fwork.css">
<link rel="stylesheet" href="'.$base.'css/alart.css">
<link rel="stylesheet" href="'.$base.'css/grid.min.css">
<link rel="stylesheet" href="'.$base.'css/classic.date.css">
<link rel="stylesheet" href="'.$base.'css/classic.time.css">
<link rel="stylesheet" href="'.$base.'css/classic.css">';
include($base.'_in/header.php');
include($base.'_in/connect.php');

if (!in_array("Paid Holidays", $arrysession)) {
   
    echo "<script>window.location.href='/process/dashboard.php';</script>";
   
    exit;

}

$con=_connect();
?>

<style>

hr.style-hr {
    border: 0;
    height: 1px;
    background-image: linear-gradient(to right, rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.75), rgba(0, 0, 0, 0));
}

</style>
<style>
 .picker__select--year{
        height:auto;
    }
    .picker__select--month{
        height:auto;
    }
hr.style-hr {
    border: 0;
    height: 1px;
    background-image: linear-gradient(to right, rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.75), rgba(0, 0, 0, 0));
}
.table-list td,
   .table-list th {
       border: 1px solid #ddd;
       padding: 1px !important;
       font-size: 13px;
   }
   .table-list td{
	padding-top: 10px !important;

   }

   .table-list tr:nth-child(even) {
       background-color: #f2f2f2;
   }

   .table-list th {
       padding-top: 5px;
       padding-bottom: 5px;
       text-align: center;
       background-color: #16a085;
       color: white;
   }
</style>
<div class="container-fluid" id="section">
    <div class=" content">
        <h2 align="center" style="margin:0;">Paid Holidays Master</h2>
        <hr class="style-hr">
        <div class="col-sm-10" id="section1">
            <div class="col-sm-3"></div>
            <div class="col-sm-6">
                <div class="col-sm-4">Date</div>
                <div class="col-sm-8">
                    <div class="form-group">
                     
                    <input type="text"  data-name="holidaydate" data-role="text" id="holidaydate" class="form-control sm datepicker">
                    </div>
                </div>

            </div>
           
            <div class="col-sm-3"></div>
        </div>
         <div class="col-sm-10" id="section2">
            <div class="col-sm-3"></div>
            <div class="col-sm-6">
                <div class="col-sm-4">Reference</div>
                <div class="col-sm-8">
                    <div class="form-group">
                     
                        <div class="form-group"><input type="text" data-role="text" data-name="reference" id="reference" class="form-control input-sm"></div>
               
                    </div>
                </div>

            </div>
           
            <div class="col-sm-3"></div>
        </div>
         
            
        <div style="clear:both;"></div>
        <hr class="style-hr">
        <div class="col-sm-3"></div>
        <div class="col-sm-6">
            <div class="col-sm-6"><button class="btn btn-success btn-sm btn-block" id="btn-submit"
                    style="margin:5px;">Submit</button></div>
            <div class="col-sm-6"><button class="btn btn-danger btn-sm btn-block" id="btn-reset"
                   onclick="$('#section').find('input').val('');" style="margin:5px;">Reset</button></div>
        </div>
        <div class="col-sm-3"></div>
        <table class="table-list table table-bordered">
    <thead>
      <tr>
        <th>SR. No</th>
        <th>Date</th>
        <th>Reference</th>
        <th>Action</th> 
      </tr>
    </thead>
    <tbody>
    <?php
                    $count=0;
                    $result4=mysqli_query($con,"SELECT * FROM paidholidays ORDER BY holidaydate ASC" );
                     while($rows3 = mysqli_fetch_assoc($result4)){
                         $count++;
                      $holidaydate= date('d/m/Y',$rows3['holidaydate']/1000);
                     echo '<tr>';
                     echo '<td align="center">'.$count.'</td>';
                     echo '<td align="center">'.$holidaydate.'</td>';
                     echo '<td align="center">'.$rows3['reference'].'</td>';
                     echo '<td align="center"><a href="javascript:void(0)" onclick="deleteholiday('.$rows3['id'].');">Delete</a></td>';
                     
                     echo '<tr>';
                    }
    ?>
    </tbody>
  </table>
    </div>
</div>

<?php
include($base.'_in/footer.php');
?>
<script>
$('.datepicker').pickadate({     
    selectYears: 100,
	selectMonths: true,
	onClose: function () {
		try {
			var dateStamp = this.get('select')['pick'];
			var id = this.get('id');
			$('#' + id).attr('data-stamp', dateStamp);
			$('#' + id).parent().find('.timepicker').click();
		} catch (err) {
			console.log(err);
		}
	}
});

function deleteholiday(hid){
  $.ajax({
        type: "POST",
        data: {
                hid: hid      
        },
        url: 'delete.php',
        cache: false,
        success: function (res) {
          console.log(res.status);
             if(res.status == "success"){
            swal({
              type: 'success',
              title: 'Record Deleted',
              showConfirmButton: false,
              timer: 2000
            });
            setTimeout(function () {
              window.location.reload();
            }, 2000);
         } else if(res.status=="falid3"){
          swal({
              type: 'error',
              title: 'Error',
              showConfirmButton: false,
              timer: 2000
            });
          setTimeout(function () {
              window.location.reload();
            }, 2000);
         }    
        }
      });

}
$('#btn-submit').on('click',function(){
    var valid=true;
    if(checker('section') !=false ) {
			valid=valid*true;
		}else{
		    valid=valid*false;
         }
    if(valid){
        var data1=checker('section');
         data1 = JSON.stringify(data1);
        $.ajax({
				type: "POST",
				data: {
                    data: data1      
				},
				url: 'insert.php',
				cache: false,
				success: function (res) {
          console.log(res.status);
					 if(res.status == "success"){
						swal({
							type: 'success',
							title: 'Your Work is Save',
							showConfirmButton: false,
							timer: 2000
						});
						setTimeout(function () {
							window.location.reload();
						}, 2000);
         } else if(res.status=="falid2"){
          swal({
              type: 'error',
              title: 'Duplicate Entry',
              showConfirmButton: false,
              timer: 2000
            });
          setTimeout(function () {
              window.location.reload();
            }, 2000);
         }          
				}
			});
    }
})

</script>