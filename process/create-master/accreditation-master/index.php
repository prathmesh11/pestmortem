<?php
    $base='../../../';
    $js='<script src="'.$base.'js/fwork.js"></script>
    <script src="'.$base.'js/alart.js"></script>
    <script src="'.$base.'js/list.min.js"></script>';
    $css='<link rel="stylesheet" href="'.$base.'css/fwork.css">
    <link rel="stylesheet" href="'.$base.'css/alart.css">';
    include($base.'_in/header.php');
    include($base.'_in/connect.php');

    if (!in_array("Accreditation Master", $arrysession)) {
   
        echo "<script>window.location.href='/process/dashboard.php';</script>";
       
        exit;
    
    }

    $con=_connect();
?>
<style>
    th {
        background: #8e44ad;
        color: #fff;
        text-align: center;
        padding-top: 2px;
        padding-bottom: 2px;
        border: 1px solid #fff;
    }
</style>
<div class="container-fluid">
    <div class="content" id="section">
        <h2 align="center" style="margin-top:0px;">Accreditation Master</h2>
        <div style="clear:both;"></div>
        <hr class="style-hr">
        <div class="col-sm-12">
                <div class="col-sm-3"></div>
                <div class="col-sm-1"></div>
                <div class="col-sm-2">Name</div>
                <div class="col-sm-3 form-group">
                    <input type="text" data-role='text' data-name='name'class="form-control input-md">
                    <input type="hidden" id="id" data-name="id"/>
                </div>
                <div class="col-sm-3"></div>
        </div>
        <div class="col-sm-12">
                <div class="col-sm-3"></div>
                <div class="col-sm-1"></div>
                <div class="col-sm-2">Branch</div>
                <div class="col-sm-3 form-group">
                    <select class="form-control input-sm select-js1" data-role="select" data-name="branchid" id="branchid">
                        <option value="Select">Select</option>
                         <?php
                                    $result1=mysqli_query($con, "SELECT branchname,branchid FROM `branchmaster`");
                                    while($rows=mysqli_fetch_assoc($result1)) {
                                    echo '<option value="'.$rows['branchid'].'">'.$rows['branchname'].'</option>';
                                }
                            ?> 
                    </select>
                </div>
                <div class="col-sm-3"></div>
        </div>
        <div class="col-sm-12">
                <div class="col-sm-3"></div>
                <div class="col-sm-1"></div>
                <div class="col-sm-2">Certificate Type</div>
                <div class="col-sm-3 form-group">
                <select type="select"  data-role="select" data-name='certType' id='certType' class="certType form-control input-sm">
                    <option value="Select">Select</option>
                    <option value="ALP">ALP</option>
                    <option value="MBR">MBR</option>
                    <option value="AFAS">AFAS</option>
                </select>
        </div>
        <div class="col-sm-12">
                <div class="col-sm-3"></div>
                <div class="col-sm-1"></div>
                <div class="col-sm-2">Accreditation Number</div>
                <div class="col-sm-3 form-group">
                <input type="text" data-role='text' data-name='accno'class="form-control input-md">
                </div>
        <div class="col-sm-3"></div>
        </div>
        <div class="col-sm-12">
            <div class="col-sm-3"></div>
            <div class="col-sm-3">
                <button class="btn btn-success btn-sm btn-block" id="btn-submit" onclick="submit()" style="margin:5px;">Submit</button>
            </div>
            <div class="col-sm-3">
                <button class="btn btn-danger btn-sm btn-block" id="btn-reset"style="margin:5px;">Reset</button>
            </div>
            <br><br><br>
        </div>
        <hr style="margin-top:20px;">
        
        <table class="table" id="table" style="border: 1px solid #ddd;">
            <thead>
                <tr>
                <th width="20">Sr. No.</th>
                <th width="100">Name</th>
                <th width="100">Branch Name</th>
                <th width="100" class="text-center">Certificate Type</th>
                <th width="100" >Accreditation Number</th>
                <th width="120">Action</th>
                </tr>
            </thead>
            <tbody>
            <?php
		        $count=0;
		          $result=mysqli_query($con,"SELECT id,name,branchid,certType,accno FROM `accreditationmaster` ORDER BY id ASC");
		           while($rows = mysqli_fetch_assoc($result)){
		                $count++;
		                echo '<tr>';
		                echo '<td align="center">'.$count.'</td>';
                        echo '<td>'.$rows['name'].'</td>';
                        $branchid=$rows['branchid'];
                        $branchname=mysqli_fetch_assoc(mysqli_query($con,"SELECT branchname x FROM `branchmaster` WHERE branchid='$branchid'  ORDER BY id ASC"))['x'];
                        echo '<td align="center">'.$branchname.'</td>';
                        echo '<td align="center">'.$rows['certType'].'</td>';
                        echo '<td align="center">'.$rows['accno'].'</td>';
		                echo '<td align="center"><button data-id="'.$rows['id'].'" onclick="edit(this)" class="btn-primary btn update">Edit</button></td>';
		                echo '<tr>';
		          }
		      ?>
            </tbody>
        </table>

    </div>
</div>

<?php
include($base.'_in/footer.php');
?>
<script>
    function submit() {
		var valid = true;
        if (checker('section') != false) {
            valid = valid * true;
        } else {
            valid = valid * false;
        }
        if (valid) {
            var data = checker('section');
            data = JSON.stringify(data);
            $.ajax({
        		type: "POST",
        		data: {
                  data: data
        		},
				url: 'insert.php',
				cache: false,
				success: function (res) {
                  	if(res.status=='success'){
                      window.location.reload();
                  	}
		    	}
			});
        }
    }
    function edit(e){
      $("#btn-submit").attr('onclick','update()');
      $("#btn-submit").text('Update');
      var id=$(e).data('id');
      $.ajax({
			type: "POST",
			data: 'id='+id,
			url: 'select.php',
			cache: false,
			success: function (res) {
          if(res.status=='success'){
            $('#id').val(id);
            modals.putvalue('section',res.json);
          }
	            }
        });
    }
    function update() {
        var valid = true;
        if (checker('section') != false) {
            valid = valid * true;
        } else {
            valid = valid * false;
        }
        if (valid) {
            var data = checker('section');
              data = JSON.stringify(data);
              $.ajax({
				type: "POST",
				data: {
          		data: data,
				},
				url: 'update.php',
				cache: false,
				success: function (res) {
	              if(res.status=='success'){
	                  window.location.reload();
	              }
		        }
	        });
        }
    }


</script>
