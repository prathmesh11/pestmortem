<?php
$base='../../../';
$js='<script src="'.$base.'js/fwork.js"></script>
<script src="'.$base.'js/alart.js"></script>
<script src="'.$base.'js/list.min.js"></script>
<script src="'.$base.'js/picker.js"></script>
<script src="'.$base.'js/picker.date.js"></script>
<script src="'.$base.'js/picker.time.js"></script>
<script src="'.$base.'js/pdfmake.min.js"></script>
<script src="'.$base.'js/vfs_fonts.js"></script>
<script src="'.$base.'js/fwork.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
';

$css='<link rel="stylesheet" href="'.$base.'css/fwork.css">
<link rel="stylesheet" href="'.$base.'css/grid.min.css">
<link rel="stylesheet" href="'.$base.'css/classic.date.css">
<link rel="stylesheet" href="'.$base.'css/classic.time.css">
<link rel="stylesheet" href="'.$base.'css/classic.css">
<link rel="stylesheet" href="'.$base.'css/alart.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js">';


 
    
 
include($base.'_in/header.php');
include($base.'_in/connect.php');
$con=_connect();

if(!isset($_GET['empid'])){
unset($_SESSION['currentempid']);
}else{
  $_SESSION['currentempid'] = $_GET['empid'];
 // echo "<script>getempdetails(".$_GET['empid'].")</script>";
}

?>
<style>
hr.style-hr {
    border: 0;
    height: 1px;
    background-image: linear-gradient(to right, rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.75), rgba(0, 0, 0, 0));
}

   .sidenav1 {
       height: 100%;
       width: 0;
       position: fixed;
       z-index: 1;
       top: 0;
       left: 0;
       background-color: #111;
       overflow-x: hidden;
       transition: 0.5s;
       padding-top: 60px;
   }

   .sidenav1 a {
       padding: 8px 8px 8px 32px;
       text-decoration: none;
       font-size: 25px;
       color: #818181;
       display: block;
       transition: 0.3s;
   }

   .sidenav1 a:hover {
       color: #f1f1f1;
   }

   .sidenav1 .closebtn1 {
       position: absolute;
     
       right: 25px;
       font-size: 36px;
       margin-left: 50px;
   }

   @media screen and (max-height: 450px) {
       .sidenav1 {
           padding-top: 15px;
       }

       .sidenav1 a {
           font-size: 18px;
       }
   }

   .list {
       padding: 0px;
   }

   .list li span {
       display: inline;
   }

   .list li {
       text-decoration: none;
       display: block;
       padding: 2px;
       background: #FFF;
       border: 1px solid #333;
       color: #000;
   }
</style>
<input type="hidden" id="item-json1" value='<?php echo str_replace("'"," &#39;",$json1); ?>' />
<div class="container-fluid">

    <div class=" content" id="section">
        <span style="font-size:24px;cursor:pointer" onclick="openNav1()">&#9776; Search</span>
        <div id="mysidenav1" class="sidenav1" style="margin-left:80px;">

            <div id="test-list" style="padding:5px;">
                <a href="javascript:void(0)" class="closebtn1" onclick="closeNav1()">&times;</a>
                <br><br><br>
                <div class="form-group">
                    <input type="text" placeholder="Search" class="form-control input-sm fuzzy-search"
                        style="border-radius:3px;">
                </div>
                <ul class="list">
                <?php 
                        $result2=mysqli_query($con,"SELECT employeeid,username FROM employeemaster ORDER by employeeid ASC");
                        while($rows = mysqli_fetch_assoc($result2)){
                            $employeeid=$rows['employeeid'];
                            $username=$rows['username'];
                            echo '<li><span class="employee">' .$username. '<span><button style="margin-left:2px;" data-employeeid="' .$employeeid. '" onclick="edit(this)" class="btn btn-primary btn-sm"><span class="fa fa-edit"></span> E</button></i>';
                        }
                ?>
                </ul>
            </div>
        </div>

        <h2 align="center" style="margin-top:0px;">Create Salary Master</h2>
<ul class="nav nav-tabs  nav-justified">
  <li class="active"><a data-toggle="tab" href="#personal"><h4>Personal</h4></a></li>
  <li><a data-toggle="tab" href="#official"><h4>Official</h4></a></li>
 </ul>

<div class="tab-content">
  <div id="personal" class="tab-pane fade in active">
   <div style="clear:both;"></div>
        <hr class="style-hr">
        <div id="personal1">
        <div class="col-sm-8">
        <center> Employee Details</center>
        <hr class="style-hr">
        <div class="col-sm-6">

            <div class="col-sm-5">Branch</div>
            <div class="col-sm-7">
                <div class="form-group">
                    <select class="form-control input-sm" data-role="select" data-name="branchid" onchange="getemp(this);">
                        <option value="Select">Select</option>
                        <?php 
                    $result1=mysqli_query($con, "SELECT * FROM `branchmaster`");
                    while($rows=mysqli_fetch_assoc($result1)) {
                        echo '<option value="'.$rows['branchid'].'">'.$rows['branchname'].'</option>';
                    }
                        ?>
                    </select>
                </div>
            </div>
            <div class="col-sm-5">Employee Name</div>
            <div class="col-sm-7">
                <div class="form-group">
                    <select class="form-control input-sm" data-role="select" data-name="username" id="username" onchange="getempid(this);">
                        <option value="Select">Select</option>
                       
                         <?php /*
                    $result1=mysqli_query($con, "SELECT * FROM `employeemaster`");
                    while($rows=mysqli_fetch_assoc($result1)) {
                        echo '<option value="'.$rows['employeeid'].'">'.$rows['username'].'</option>';
                    } */
                        ?>
                    </select>
                    <input type="hidden" data-name="hemployeeid" id="hemployeeid">
                <input type="text" readonly  id="employeeid" class="form-control input-sm">
                        
                        </div>
            </div>
            <div class="col-sm-5">Contact No.</div>
            <div class="col-sm-7">
                <div class="form-group"><input type="text" data-role='text' data-name='contactno'
                    id="contactno"    class="form-control input-sm"></div>
            </div>
              <div class="col-sm-5">Address</div>
            <div class="col-sm-7">
                <div class="form-group"><textarea data-role='textarea' onkeyup='tempaddress(this)' data-name='tempaddress' id="tempaddress" class="form-control input-sm"> </textarea></div>
            </div> 
            <div class="col-sm-5">Permenant address</div>
            <div class="col-sm-7">
                <div class="form-group"><textarea data-role='textarea' id="peraddress"  data-name='peraddress'
                        class="form-control input-sm"> </textarea></div>
            </div>

            <div class="col-sm-5">DOB </div>
            <div class="col-sm-7">
                
                    <input type="text"  id="dob-date" class="form-control">
                    <input type="hidden" data-name="dob-date-stamp" id="dob-date-stamp">
       
             </div>  
            <div class="col-sm-5">Machine Emp. Id.</div>
            <div class="col-sm-7">
                <div class="form-group"><input type="text" id="machineempid" data-name='machineempid'
                        class="form-control input-sm"></div>
            </div>

            
            <div class="col-sm-5">Email Id.</div>
            <div class="col-sm-7">
                <div class="form-group"><input type="text"  data-name='email'
                      id="email"  class="form-control input-sm"></div>
            </div>

              
              <div class="col-sm-5">Gender</div>
            <div class="col-sm-7">
                 <input type="radio" data-name='gender' name= "gender" value="M" checked>Male
                 <input type="radio" data-name='gender' name= "gender" value="F">Female
                     
            </div>

            <div class="col-sm-5">Qualification</div>
            <div class="col-sm-7">
                <div class="form-group"><input type="text" id="qualification" data-name='qualification'
                        class="form-control input-sm"></div>
            </div>

            <div class="col-sm-5">Experience</div>
            <div class="col-sm-7">
                <div class="form-group"><input type="text" id="experience"  data-name='experience'
                        class="form-control input-sm"></div>
            </div>

           
            

        </div>
        <div class="col-sm-6">
        
        <div class="col-sm-5">Reference By</div>
            <div class="col-sm-7">
                <div class="form-group"><input type="text"  data-name='refby' id="refby"
                        class="form-control input-sm"></div>
            </div>
         <div class="col-sm-5">Category</div>
            <div class="col-sm-7">
                <div class="form-group">
                    <select class="form-control input-sm" data-role="select" id="category" data-name="category">
                        
                        <option value="Select">Select</option>
                           <?php 
                            $result1=mysqli_query($con, "SELECT * FROM `category`");
                            while($rows=mysqli_fetch_assoc($result1)) {
                                echo '<option value="'.$rows['id'].'">'.$rows['categoryname'].'</option>';
                            } 
                        ?>
                    </select>
                </div>
            </div>

            <div class="col-sm-5">Department</div>
            <div class="col-sm-7">
                <div class="form-group">
                    <select class="form-control input-sm" data-role="select" id="department" data-name="department">
                     
                        <option value="Select">Select</option>
                           <?php 
                            $result1=mysqli_query($con, "SELECT * FROM `department`");
                            while($rows=mysqli_fetch_assoc($result1)) {
                                echo '<option value="'.$rows['id'].'">'.$rows['departmentname'].'</option>';
                            } 
                        ?>
                    </select>
                </div>
            </div>
            
            <div class="col-sm-5">Designation</div>
            <div class="col-sm-7">
                <div class="form-group">
                    <select class="form-control input-sm" data-role="select" data-name="designation" id="designation">
                     
                        <option value="Select">Select</option>
                        <?php 
                            $result1=mysqli_query($con, "SELECT * FROM `designation`");
                            while($rows=mysqli_fetch_assoc($result1)) {
                                echo '<option value="'.$rows['id'].'">'.$rows['designationname'].'</option>';
                            } 
                        ?>
                    </select>
                </div>
            </div> 
            <div class="col-sm-5">Company</div>
            <div class="col-sm-7">
                <div class="form-group">
                    <select class="form-control input-sm" data-role="select" data-name="company" id="company">
                     
                        <option value="Select">Select</option>
                       <?php 
                            $result1=mysqli_query($con, "SELECT * FROM `company`");
                            while($rows=mysqli_fetch_assoc($result1)) {
                                echo '<option value="'.$rows['id'].'">'.$rows['companyname'].'</option>';
                            } 
                        ?>
                    </select>
                </div>
            </div> 

        <div class="col-sm-5">PAN</div>
            <div class="col-sm-7">
                <div class="form-group"><input type="text"  data-name='pan' id="pan"
                        class="form-control input-sm"></div>
            </div>

            <div class="col-sm-5">Salary Per</div>
            <div class="col-sm-7">
                <div class="form-group">
                    <select class="form-control input-sm" data-role="select" data-name="salaryper" id="salaryper">
                     
                        <option value="Select">Select</option>
                        <option value="MONTHLY">MONTHLY</option>
                        <option value="WEEKLY">WEEKLY</option>
                    </select>
                </div>
            </div>
            <div class="col-sm-5"> Payment </div>
            <div class="col-sm-7">
                <div class="form-group">
                    <select class="form-control input-sm" data-role="select" id="modeofpayment" data-name="modeofpayment">
                    
                        <option value="Select">Select</option>
                        <option value="BANK">BANK</option>
                        <option value="CASH">CASH</option>
                    </select>
                </div>
            </div>
            <div class="col-sm-5">Bank Name</div>
                <div class="col-sm-7">
                    <div class="form-group"><input type="text"  data-name='bankname' id="bankname"
                            class="form-control input-sm"></div>
                </div>
                <div class="col-sm-5">Branch Name</div>
                <div class="col-sm-7">
                    <div class="form-group"><input type="text"  data-name='branchname' id="branchname"
                            class="form-control input-sm"></div>
                </div>
                 
                <div class="col-sm-5">Account Number</div>
                <div class="col-sm-7">
                    <div class="form-group"><input type="text"  data-name='accountno' id="accountno"
                            class="form-control input-sm"></div>
                </div>
             
           </div>

           </div>
        </div>
 
        <div class="col-sm-4">
         <center> Document Details</center>
                <hr class="style-hr">
        <div class="row">
        <div class="col-sm-6"> 
       
       
      <form class="md-form"  id="imageUploadForm" action="uploadpic.php" method="post" enctype="multipart/form-data">
          <div class="file-field">
           <span>Upload Your Photo :
        </span>
         <input type="hidden" id="picemployeeid" name="picemployeeid" />
       
            <a class="btn-floating purple-gradient mt-0 float-left">
              <i class="fas fa-cloud-upload-alt" aria-hidden="true"></i>
              <input type="file" name="file" id="ImageBrowse">
            </a>

             
            <input type="submit" name="upload" value="Upload" />
          </div>
          
        </form>
        </div>
         <div class="col-sm-6" id="photodiv">
         <?php
         if(isset($_SESSION['currentempid'])){
          if(file_exists("../../../img/icon/profilepic/".$_SESSION['currentempid']."/userpic.jpg")){
              $imgsrc = "../../../img/icon/profilepic/".$_SESSION['currentempid']."/userpic.jpg";

             }else{
            $imgsrc = "../../../img/icon/blank.png";
            }
        }else{
           $imgsrc = "../../../img/icon/blank.png"; 
        }
         ?>
             <img src=<?php echo $imgsrc; ?> alt="Avatar" class="avatar" 
             style="vertical-align: middle;
                      width: 150px;
                      height: 150px;
                      border-radius: 50%;
                    ">
         </div>
       </div>
        <br><br><br>
         <form class="md-form"  id="imageUploadFormDoc" action="uploaddoc.php" method="post" enctype="multipart/form-data">
          <div class="file-field">
           <span>Upload Your Documents :
        </span>
         <input type="hidden" id="picemployeeid" name="picemployeeid" />
       
            <a class="btn-floating purple-gradient mt-0 float-left">
              <i class="fas fa-cloud-upload-alt" aria-hidden="true"></i>
              <!--<input type="file" multiple name="file[]" id="ImageBrowseDoc"> -->
              <input type="file" name="file" id="ImageBrowseDoc" class="ImageBrowseDoc">
             </a>

             
            <input type="submit" name="upload" value="Upload" style="display: none" />
          </div>
           </form>

          <div id="docdiv">
             <?php 
             if(isset($_SESSION['currentempid'])){
              $dir = "../../../img/documents/".$_SESSION['currentempid']."/";

                // Open a directory, and read its contents
                if (is_dir($dir)){
                  if ($dh = opendir($dir)){
                    while (($file = readdir($dh)) !== false){
                        if($file != "." && $file != ".."){
                            echo "<a href='".$dir.$file."' target = '_BLANK'> " . $file . "</a>

                             <a href='' onclick='deletedoc(".$dir.$file.")'
                             title='Delete file'>  <span class='glyphicon glyphicon-remove'></span> </a>
                             <br>";
                        }
                    }
                    closedir($dh);
                  }
                }
              }
    ?>

          </div>
       
     

        
</div>
        <div class="col-sm-12">
            <div style="clear:both;"></div>
            <hr class="style-hr">

            <div class="col-sm-3">
            </div>
            
            <div id="insertdiv" class="col-sm-3"><button class="btn btn-success btn-sm btn-block" id="btn-submit" onclick="submit(1)" style="margin:5px;">Insert</button></div>
             
              <div id="updatediv" class="col-sm-3"><button class="btn btn-success btn-sm btn-block" id="btn-submit" onclick="submit(2)" style="margin:5px;">Update</button></div>
            
             
            <div class="col-sm-3"><button class="btn btn-danger btn-sm btn-block" id="btn-reset"
                    onclick="$('#personal1').find('select').val('');
 getempid(0)" style="margin:5px;">Reset</button></div>
            <div class="col-sm-3"></div>
        </div>
  </div>
  <div id="official" class="tab-pane fade">
       <br>
            <div class="col-sm-4">
             
                <div class="col-sm-5">Basic</div>
                <div class="col-sm-7">
                    <div class="form-group"><input type="text"  data-name='basic' id='basic'
                         placeholder="0.00"   class="form-control input-sm"></div>
                </div>
                <div class="col-sm-5">DA</div>
                <div class="col-sm-7">
                    <div class="form-group"><input type="text"  data-name='da' id='da'
                         placeholder="0.00" id="da"    class="form-control input-sm"></div>
                </div>
                 
                <div class="col-sm-5">HRA</div>
                <div class="col-sm-7">
                    <div class="form-group"><input type="text"  data-name='hra' id='hra'
                          placeholder="0.00" id="hra"  class="form-control input-sm"></div>
                </div>
                <div class="col-sm-5">Travelling Allowance</div>
                <div class="col-sm-7">
                    <div class="form-group"><input type="text"  data-name='ta' id='ta'
                          placeholder="0.00" id="ta"  class="form-control input-sm"></div>
                </div>
                <div class="col-sm-5">Medical</div>
                <div class="col-sm-7">
                    <div class="form-group"><input type="text"  data-name='medical'  id='medical' 
                         placeholder="0.00" id="medical"   class="form-control input-sm"></div>
                </div>
                <div class="col-sm-5">Conveyance</div>
                <div class="col-sm-7">
                    <div class="form-group"><input type="text"  data-name='conveyance' id='conveyance' 
                          placeholder="0.00" id="conveyance"   class="form-control input-sm"></div>
                </div>
                
                <div class="col-sm-5">Education</div>
                <div class="col-sm-7">
                    <div class="form-group"><input type="text"  data-name='education' id='education'
                         placeholder="0.00" id="education"   class="form-control input-sm"></div>
                </div>

                <div class="col-sm-5">CCA</div>
                <div class="col-sm-7">
                    <div class="form-group"><input type="text"  data-name='cca' id='cca'
                         placeholder="0.00" id="cca" onblur="calculatesalary()" class="form-control input-sm"></div>
                </div>

                <div class="col-sm-5"><b>Total Salary</b></div>
                <div class="col-sm-7">
                    <div class="form-group"><input type="text"  data-name='totalsalary' id='totalsalary'
                          placeholder="0.00" id="totalsalary" class="form-control input-sm"></div>
                </div>
                <br>
                <div class="col-sm-5">Mobile</div>
                <div class="col-sm-7">
                    <div class="form-group"><input type="text"  data-name='mobile' id='mobile'
                         placeholder="0.00"   class="form-control input-sm"></div>
                </div>
                <div class="col-sm-5">uniform</div>
                <div class="col-sm-7">
                    <div class="form-group"><input type="text"  data-name='uniform' id='uniform'
                         placeholder="0.00"   class="form-control input-sm"></div>
                </div>
                <div class="col-sm-5">internet</div>
                <div class="col-sm-7">
                    <div class="form-group"><input type="text"  data-name='internet' id='internet'
                          placeholder="0.00"  class="form-control input-sm"></div>
                </div>
                <div class="col-sm-5">shoes</div>
                <div class="col-sm-7">
                    <div class="form-group"><input type="text"  data-name='shoes' id='shoes'
                         placeholder="0.00"   class="form-control input-sm"></div>
                </div>
                

            </div>

            <div class="col-sm-4">
             
                <div class="col-sm-5">Employee ESIC %</div>
                <div class="col-sm-7">
                    <div class="form-group"><input type="text"  data-name='empesic' id='empesic'
                          placeholder="0.00"  class="form-control input-sm"></div>
                </div>
                <div class="col-sm-5">Employer ESIC %</div>
                <div class="col-sm-7">
                    <div class="form-group"><input type="text"  data-name='empyesic' id='empyesic'
                         placeholder="0.00"   class="form-control input-sm"></div>
                </div>
                 
                <div class="col-sm-5">PF. wages</div>
                <div class="col-sm-7">
                    <div class="form-group"><input type="text"  data-name='pfvages' id='pfvages'
                          placeholder="0.00"  class="form-control input-sm"></div>
                </div>
                 <div class="col-sm-5">Employee PF %</div>
                <div class="col-sm-7">
                    <div class="form-group"><input type="text"  data-name='emppf' id='emppf'
                         placeholder="0.00"   class="form-control input-sm"></div>
                </div>
                <div class="col-sm-5">Employer PF %</div>
                <div class="col-sm-7">
                    <div class="form-group"><input type="text"  data-name='empypf' id='empypf'
                         placeholder="0.00"   class="form-control input-sm"></div>
                </div>

                <div class="col-sm-5">Bonus</div>
                <div class="col-sm-3">
                    <div class="form-group"><input type="text"  data-name='bonuspercent' id='bonuspercent'
                        placeholder="%"    class="form-control input-sm"></div>
                </div>
                 <div class="col-sm-4">
                    <div class="form-group"><input type="text"  data-name='bonusamt' id='bonusamt'
                       placeholder="0.00"     class="form-control input-sm"></div>
                </div>
                <div class="col-sm-5"><b>Total CTC (Yearly)</b></div>
                <div class="col-sm-7">
                    <div class="form-group"><input type="text"  data-name='totalctc'
                         placeholder="0.00" id="totalctc"  class="form-control input-sm"></div>
                </div>
               <br>
                <div class="col-sm-5">Training Date</div>
                <div class="col-sm-7">
                    <div class="form-group"><input type="text"  data-name='trainingdate' id='trainingdate'
                            class="form-control input-sm">
                          <input type="hidden" data-name="training-date-stamp" id="training-date-stamp">
       
                         </div>
                </div>   
                <div class="col-sm-5">Joining Date</div>
                <div class="col-sm-7">
                    <div class="form-group"><input type="text"  id='joiningdate'
                            class="form-control input-sm">
                     <input type="hidden" data-name="joining-date-stamp" id="joining-date-stamp">
                </div>

                </div>                 
                <div class="col-sm-5">Probation Period</div>
                <div class="col-sm-7">
                    <div class="form-group"><input type="text"  data-name='probationperiod' id='probationperiod'  placeholder="0"  class="form-control input-sm"></div>
                </div>                 
                <div class="col-sm-5">Permanent Date</div>
                <div class="col-sm-7">
                    <div class="form-group"><input type="text"  id='permanentdate'
                            class="form-control input-sm"> 
                    <input type="hidden" data-name="permanent-date-stamp" id="permanent-date-stamp">
                </div>
                </div>                 
                <div class="col-sm-5">Leaving Date</div>
                <div class="col-sm-7">
                    <div class="form-group"><input type="text"  id='leavingdate'
                            class="form-control input-sm"> 
                             <input type="hidden" data-name="leaving-date-stamp" id="leaving-date-stamp">
                </div>
                </div>                 

            </div>

            <div class="col-sm-4">
             
                <div class="col-sm-5">Additional Charges</div>
                <div class="col-sm-4">
                    <div class="form-group"><input type="text"  data-name='acdescr1' id='acdescr1'
                         placeholder="Description"    class="form-control input-sm"></div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group"><input type="text"  data-name='acamt1' id='acamt1'
                          placeholder="0.00"   class="form-control input-sm"></div>
                </div>
                <div class="col-sm-5"> </div>
                 <div class="col-sm-4">
                    <div class="form-group"><input type="text"  data-name='acdescr2' id='acdescr2'
                           placeholder="Description"  class="form-control input-sm"></div>
                </div>
                 <div class="col-sm-3">
                    <div class="form-group"><input type="text"  data-name='acamt2' id='acamt2'
                          placeholder="0.00"   class="form-control input-sm"></div>
                </div>
                <div class="col-sm-5">Additional Deduction</div>
                <div class="col-sm-4">
                    <div class="form-group"><input type="text"  data-name='acaddescr1' id='acaddescr1'
                          placeholder="Description"   class="form-control input-sm"></div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group"><input type="text"  data-name='adamt1' id='adamt1'
                          placeholder="0.00"   class="form-control input-sm"></div>
                </div>
                <div class="col-sm-5"> </div>
                 <div class="col-sm-4">
                    <div class="form-group"><input type="text"  data-name='acaddescr2' id='acaddescr2'
                         placeholder="Description"    class="form-control input-sm"></div>
                </div>

                <div class="col-sm-3">
                    <div class="form-group"><input type="text"  data-name='adamt2' id='adamt2'
                         placeholder="0.00"    class="form-control input-sm"></div>
                </div>
                
                <div class="col-sm-5">Sunday Payment</div>
                <div class="col-sm-7">
                    <div class="form-group">
                     <input type="radio" data-name='sundayoff' name= "sundayoff" value="Y" checked> Yes
                        <input type="radio" data-name='sundayoff' name= "sundayoff" value="N"> No
                  </div>
                </div>
                 <div class="col-sm-5">Dress Allowed</div>
                <div class="col-sm-7">
                    <div class="form-group">
                     <input type="radio" data-name='dressallowed' name= "dressallowed" value="Y" checked> Yes
                        <input type="radio" data-name='dressallowed' name= "dressallowed" value="N"> No
                  </div>
                </div>

                <div class="col-sm-5">Shoes Allowed</div>
                <div class="col-sm-7">
                    <div class="form-group">
                     <input type="radio" data-name='shoeallowed' name= "shoeallowed" value="Y" checked> Yes
                        <input type="radio" data-name='shoeallowed' name= "shoeallowed" value="N"> No
                  </div>
                </div>
                <div class="col-sm-5">Lockers Allowed</div>
                <div class="col-sm-7">
                    <div class="form-group">
                     <input type="radio" data-name='lockerallowed' name= "lockerallowed" value="Y" checked> Yes
                        <input type="radio" data-name='lockerallowed' name= "lockerallowed" value="N"> No
                  </div>
                </div>
                
                <div class="col-sm-5">ESIC No.</div>
                <div class="col-sm-7">
                    <div class="form-group"><input type="text"  data-name='esicno' id='esicno'
                         placeholder="0.00"   class="form-control input-sm"></div>
                </div>

                <div class="col-sm-5">PF No.</div>
                <div class="col-sm-7">
                    <div class="form-group"><input type="text"  data-name='pfno' id='pfno'
                         placeholder="0.00"   class="form-control input-sm"></div>
                </div>

                <div class="col-sm-5">IT Deduction</div>
                <div class="col-sm-7">
                    <div class="form-group"><input type="text"  data-name='itdeduction' id='itdeduction'
                         placeholder="0.00"   class="form-control input-sm"></div>
                </div>
                 <div class="col-sm-5">Working Hours</div>
                <div class="col-sm-7">
                    <div class="form-group"><input type="text"  data-name='workinghrs' id='workinghrs'
                         placeholder="0.00"   class="form-control input-sm"></div>
                </div>

                 <div class="col-sm-5">Professional Tax</div>
                <div class="col-sm-7">
                    <div class="form-group">
                    
                         <select class="form-control input-sm" data-role="select" data-name="professionaltax" id="professionaltax">
                     
                        <option value="Select" selected>Select</option>
                       <?php 
                            $result1=mysqli_query($con, "SELECT * FROM `professionaltax`");
                            while($rows=mysqli_fetch_assoc($result1)) {
                                echo '<option value="'.$rows['id'].'">'.$rows['fromamt'].'-'.$rows['toamt'].'</option>';
                            } 
                        ?>
                    </select>
                         </div>
                </div>
            </div>

             <div class="col-sm-12">
            <div style="clear:both;"></div>
            <hr class="style-hr">
<BR><BR><BR><BR>
            <div class="col-sm-3">
            </div>
            <!--<div class="col-sm-3">

            <button class="btn btn-success btn-sm btn-block" id="btn-submit" onclick="submit2()"
                    style="margin:5px;">Submit</button></div>
            -->
              
              <div id="updatediv1" class="col-sm-3"><button class="btn btn-success btn-sm btn-block" id="btn-submit" onclick="submit2()" style="margin:5px;">Update</button></div>
                    
            <div class="col-sm-3"><button class="btn btn-danger btn-sm btn-block" id="btn-reset"
                    style="margin:5px;" onclick="$('#official').find('select').val('');
 $('#official').find('input').val('');">Reset</button></div>
            <div class="col-sm-3"></div>
        </div>
  </div>
  
</div>
        
    </div>
</div>
<br>
<br>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" align="center">Add Contact</h4>
            </div>
            <div class="modal-body" id='modal'>
                <div class="row">
                    <div class="col-sm-5">Person Name</div>
                    <div class="col-sm-7">
                        <div class="form-group"><input type="text" data-role='text' data-name='name'
                                class="form-control input-sm"></div>
                    </div>
                    <div class="col-sm-5">Department</div>
                    <div class="col-sm-7">
                        <div class="form-group"><input type="text" data-role='text' data-name='department'
                                class="form-control input-sm"></div>
                    </div>
                    <div class="col-sm-5">Mobile No.</div>
                    <div class="col-sm-7">
                        <div class="form-group"><input type="text" data-name='mobile' class="form-control input-sm">
                        </div>
                    </div>
                    <div class="col-sm-5">Landline No.</div>
                    <div class="col-sm-7">
                        <div class="form-group"><input type="text" data-name='landline' class="form-control input-sm">
                        </div>
                    </div>
                    <div class="col-sm-5">Email</div>
                    <div class="col-sm-7">
                        <div class="form-group"><input type="text" data-role='text' data-name='email'
                                class="form-control input-sm"></div>
                    </div>
                    <div style="clear:both;"></div>
                    <hr>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
include($base.'_in/footer.php');
?>

 
<script>


var dobdateval = $("#dob-date").pickadate({
          // format: 'm/dd/yyyy',
          selectMonths: true,
          selectYears: 100,
           onSet: function(e){
                        if(e.highlight != null){
                            var picker = dobdateval.pickadate('picker');
                            picker.set('select', new Date(Number(e.highlight[0]), Number(e.highlight[1]), Number(e.highlight[2])));
                        }

                    },
        onClose: function(){
            try{
                var dateStamp = this.get('select')['pick'];

                var id= this.get('id');


                      
                $("#dob-date-stamp").val(dateStamp);

            }catch(err){

            }
        }
    });

var joiningdateval =  $("#joiningdate").pickadate({
         labelMonthNext: 'Go to the next month',
          labelMonthPrev: 'Go to the previous month',
          labelMonthSelect: 'Pick a month from the dropdown',
          labelYearSelect: 'Pick a year from the dropdown',
          selectMonths: true,
          selectYears: 100,
        onClose: function(){
            try{
                var dateStamp = this.get('select')['pick'];
                $("#joining-date-stamp").val(dateStamp);
            }catch(err){

            }
        }
    });
var leavingdateval = $("#leavingdate").pickadate({
        labelMonthNext: 'Go to the next month',
          labelMonthPrev: 'Go to the previous month',
          labelMonthSelect: 'Pick a month from the dropdown',
          labelYearSelect: 'Pick a year from the dropdown',
          selectMonths: true,
          selectYears: 100,
        onClose: function(){
            try{
                var dateStamp = this.get('select')['pick'];
                $("#leaving-date-stamp").val(dateStamp);
            }catch(err){

            }
        }
    });
var permanentdateval = $("#permanentdate").pickadate({
         labelMonthNext: 'Go to the next month',
          labelMonthPrev: 'Go to the previous month',
          labelMonthSelect: 'Pick a month from the dropdown',
          labelYearSelect: 'Pick a year from the dropdown',
          selectMonths: true,
          selectYears: 100,
        onClose: function(){
            try{
                var dateStamp = this.get('select')['pick'];
                $("#permanent-date-stamp").val(dateStamp);
            }catch(err){

            }
        }
    });

var trainingdateval = $("#trainingdate").pickadate({
         labelMonthNext: 'Go to the next month',
          labelMonthPrev: 'Go to the previous month',
          labelMonthSelect: 'Pick a month from the dropdown',
          labelYearSelect: 'Pick a year from the dropdown',
          selectMonths: true,
          selectYears: 100,
        onClose: function(){
            try{
                var dateStamp = this.get('select')['pick'];
                $("#training-date-stamp").val(dateStamp);
            }catch(err){

            }
        }
    });


function calculatesalary(){
    var totalsalary=0;
     var basic=parseFloat($('#basic').val());
     var da=parseFloat($('#da').val());
     var hra=parseFloat($('#hra').val());
     var ta=parseFloat($('#ta').val());
     var conveyance=parseFloat($('#conveyance').val());
     var medical=parseFloat($('#medical').val());
     var education=parseFloat($('#education').val());
     var cca=parseFloat($('#cca').val());
     
     //alert(sgst);
     var grandtotal =  basic + da + hra + ta + education + medical + conveyance + cca;
       // alert(igst);
    var grandtotalctc = grandtotal *12;
   $('#totalsalary').val(grandtotal);
   
   $('#totalctc').val(grandtotalctc);
   
   // $('#inword').text(convertNumberToWords(totalamt.toFixed(2) ));
    
}

 $('#imageUploadForm').on('submit',(function(e) {
        e.preventDefault();
        var formData = new FormData(this);

        //formData.append( 'file', $( '#ImageBrowse' )[0].files[0]);
        $.ajax({
            type:'POST',
            url: "uploadpic.php",
            data:formData,
            cache:false,
            contentType: false,
            processData: false,
            success:function(data){
                console.log("success");
                console.log(data);
                 $('#photodiv') .load('get_photo.php?empid=<?php echo $_SESSION['currentempid']; ?>') .fadeIn("slow");
            },
            error: function(data){
                console.log("error");
                console.log(data);
            }
        });
    }));

    $("#ImageBrowse").on("change", function() {
        $("#imageUploadForm").submit();
    });


 $('#imageUploadFormDoc').on('submit',(function(e) {
        e.preventDefault();
        var formData = new FormData(this);
 
        $.ajax({
            type:'POST',
            url: "uploaddoc.php",
            data:formData,
            cache:false,
            contentType: false,
            processData: false,
            success:function(data){
                console.log("success"+data);
                console.log(data);
                 $('#docdiv') .load('get_docs.php') .fadeIn("slow");
            },
            error: function(data){
                console.log("error");
                console.log(data);
            }
        });
    }));

    $("#ImageBrowseDoc").on("change", function() {
        $("#imageUploadFormDoc").submit();
    });


 function deletedoc(file){

  alert(file);
 }



function tempaddress(e){
    $('#peraddress').val($(e).val());
} 

function openNav1() {
    document.getElementById("mysidenav1").style.width = "300px";
}

function closeNav1() {
    document.getElementById("mysidenav1").style.width = "0";
}

 



function getempid(e){
     
    var empid = $(e).val();
    $('#employeeid').val(empid);
    $('#hemployeeid').val(empid);
    $('#picemployeeid').val(empid);
   // alert("in1" );
     $.ajax({
        type: "POST",
         url: "set_sessionempid.php",
         data: { empid: empid },
         cache: false,
         success: function (res) {
                //alert("in"+res.status);
                if(res.status== 0){
                    $('#updatediv') .hide();
                    $('#insertdiv') .show();
                    $('#updatediv1') .hide();
                    $('#insertdiv1') .show();
                    
                }else{
                    $('#insertdiv') .hide();
                    $('#updatediv') .show();
                    $('#insertdiv1') .hide();
                    $('#updatediv1') .show();
                    
                }
                $('#photodiv') .load('get_photo.php?empid=<?php echo $_SESSION['currentempid']; ?>') .fadeIn("slow");
                  $('#docdiv') .load('get_docs.php?empid=<?php echo $_SESSION['currentempid']; ?>') .fadeIn("slow");
                getempdetails(empid);
              //  location.reload();
         }
    }); 
         
}

function getempdetails(empid){

//alert("here");
$('#personal1').find('input:text').val('');
$('#personal1').find('textarea').val('');

$('#employeeid').val(empid);

 var jsondata = $.parseJSON($.ajax({
        type: "POST",
         url: "getempdetails.php",
         data: { empid: empid },
         cache: false,
         success: function (res) {
             
            },
        dataType: "json", 
        async: false
    }).responseText); 

jsondata =JSON.stringify(jsondata);
data1 = JSON.parse(jsondata);  
json1 = data1.empjson;   


$('#contactno').val(json1.contactno);
$('#email').val(json1.email);
$('#machineempid').val(json1.machineempid);
$('#gender').val(json1.gender);
$('#tempaddress').val(json1.tempaddress);
$('#peraddress').val(json1.peraddress);

$('#qualification').val(json1.qualification);
$('#experience').val(json1.experience);

$('#refby').val(json1.refby);
$('#pan').val(json1.pan);
$('#bankname').val(json1.bankname);
$('#branchname').val(json1.branchname);
$('#accountno').val(json1.accountno);


var d  = new Date(json1.dobdate);
 //$("#dob-date").val(datestring);
//dobdateval.set('view', d);

$('#basic').val(json1.basic);
$('#da').val(json1.da);
$('#hra').val(json1.hra);
$('#ta').val(json1.ta);
$('#medical').val(json1.medical);
$('#internet').val(json1.internet);
$('#education').val(json1.education);
$('#totalsalary').val(json1.totalsalary);
$('#conveyance').val(json1.conveyance);
$('#cca').val(json1.cca);
$('#mobile').val(json1.mobile);
$('#uniform').val(json1.uniform);
$('#shoes').val(json1.shoes);

$('#empesic').val(json1.empesic);
$('#empyesic').val(json1.empyesic);
$('#pfvages').val(json1.pfvages);
$('#emppf').val(json1.emppf);
$('#empypf').val(json1.empypf);
$('#bonusamt').val(json1.bonusamt);
$('#bonuspercent').val(json1.bonuspercent);
$('#totalctc').val(json1.totalctc);
$('#trainingdate').val(json1.trainingdate);
$('#joiningdate').val(json1.joiningdate);
$('#probationperiod').val(json1.probationperiod);
$('#permanentdate').val(json1.permanentdate);
$('#leavingdate').val(json1.leavingdate);
 
$('#acdescr1').val(json1.acdescr1);
$('#acamt1').val(json1.acamt1);
$('#acdescr2').val(json1.acdescr2);
$('#acamt2').val(json1.acamt2);
$('#acaddescr1').val(json1.acaddescr1);
$('#adamt1').val(json1.adamt1);
$('#acaddescr2').val(json1.acaddescr2);
$('#adamt2').val(json1.adamt2);
$('#sundayoff').val(json1.sundayoff);
$('#shoeallowed').val(json1.shoeallowed);
$('#lockerallowed').val(json1.lockerallowed);
$('#dressallowed').val(json1.dressallowed);
$('#esicno').val(json1.esicno);
$('#pfno').val(json1.pfno);
$('#itdeduction').val(json1.itdeduction);
$('#workinghrs').val(json1.workinghrs); 

$('#department option[value='+json1.department+']').attr('selected','selected');
$('#designation option[value='+json1.designation+']').attr('selected','selected');
$('#category option[value='+json1.category+']').attr('selected','selected');
$('#company option[value='+json1.company+']').attr('selected','selected');
$('#salaryper option[value='+json1.salaryper+']').attr('selected','selected');
$('#modeofpayment option[value='+json1.modeofpayment+']').attr('selected','selected');
 
$('#professionaltax option[value='+json1.professionaltax+']').attr('selected','selected');
 

 var date = new Date(json1.dobdate);
 //alert(date);
    var picker = $('#dob-date').pickadate('picker');
    picker.set('select', date);
 
}

function getemp(e){
     
    var branchid = $(e).val();
   //alert(branchid);
    var data_String = 'branchid : '+branchid;
   
   var res1;
            $.ajax({
                            type: "POST",
                            data: 'branchid='+ branchid,  
                            url: 'ajax_getemp1.php',
                            cache: false,
                            success: function (res) {
                                $('#username').empty();
                                $('#username').append(res.status);
                            }

                        });
              }

function submit(act){
    var valid=true;
    if(checker('personal')!=false) {
			valid=valid*true;
		}else{
		    valid=valid*false;
         }
    if(valid){
        var data=checker('personal');
        var datastr = JSON.stringify(data);
			$.ajax({
				type: "POST",
				data: {
                    data: datastr,act:act
				},
				url: 'insert.php',
				cache: false,
				success: function (res) {
             //  alert(res.status);
					if (res.status == 'success') {
						swal({
							type: 'success',
							title: 'Employee Record Created',
							showConfirmButton: false,
							timer: 3000
						});
						setTimeout(function () {
							window.location.reload();
						}, 3000);
                    }

                
				}
			});
    }else{
        alert('Invlid Entry');
    }
}

function submit2(){
    //alert("submit2");
    var valid=true;
    if(checker('official')!=false) {
            valid=valid*true;
        }else{
            valid=valid*false;
         }
    if(valid){
        var data=checker('official');
        var datastr = JSON.stringify(data);
            $.ajax({
                type: "POST",
                data: {
                    data: datastr
                },
                url: 'insertofficial.php',
                cache: false,
                success: function (res) {
                   // alert(res.status);
                    if (res.status == 'success') {
                        swal({
                            type: 'success',
                            title: 'Employee Record Added',
                            showConfirmButton: false,
                            timer: 3000
                        });
                        setTimeout(function () {
                            window.location.reload();
                        }, 3000);
                    }
                
                }
            });
    }else{
        alert('error');
    }
}


function edit(e){
$('#btn-submit').attr('onclick','update()');
$('#btn-submit').text('Update');
var employeeid=$(e).data('employeeid');
    $('#employeeid').val(employeeid);
			$.ajax({
				type: "POST",
				data: 'employeeid='+ employeeid,
				url: 'select.php',
				cache: false,
				success: function (res) {
					if (res.status == 'success') {
                        modals.putvalue('section',res.employeejson);
                    }
				}
            });
}


function update(){
    var valid=true;
    if(checker('section')!=false) {
			valid=valid*true;
		}else{
		    valid=valid*false;
         }
    if(valid){
        var data=checker('section');
        var datastr = JSON.stringify(data);
			$.ajax({
				type: "POST",
				data: {
                    data: datastr,
                    employeeid: $('#employeeid').val()
				},
				url: 'update.php',
				cache: false,
				success: function (res) {
					if (res.status == 'success') {
						swal({
							type: 'success',
							title: 'Employee Updated',
							showConfirmButton: false,
							timer: 3000
						});
						setTimeout(function () {
							window.location.reload();
						}, 3000);
                    }
				}
			});
    }
}

</script>