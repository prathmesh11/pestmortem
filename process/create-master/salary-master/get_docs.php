<?php
 
    if (session_status()==PHP_SESSION_NONE) { session_start(); }
  if(isset($_SESSION['currentempid'])) {
     echo "Your Document List : <br>";
           $dir = "../../../img/documents/".$_SESSION['currentempid']."/";

                // Open a directory, and read its contents
                if (is_dir($dir)){
                  if ($dh = opendir($dir)){
                    while (($file = readdir($dh)) !== false){
                        if($file != "." && $file != ".."){
                            echo "<a href='".$dir.$file."' target = '_BLANK'> " . $file . "</a> ";
                           
                            echo "<button onclick=deletedoc('".$dir.$file."') type='button' class='btn btn-circle btn-sm'><span class='glyphicon glyphicon-remove' style='margin: 0 0px;'></span></button>

                             <br>

                             ";
                        }
                    }
                    closedir($dh);
                  }
                }

} 


?>
<script type="text/javascript">
  
  function deletedoc(file){
     $.ajax({
       url:'deletedoc.php',
       data:{ file:file },
       method:'GET',
       success:function(response){
        if (response === 'deleted')
        {
           alert('File Deleted !!');
            $('#docdiv') .load('get_docs.php') .fadeIn("slow");
        }
       }
      });
   
 }

</script>