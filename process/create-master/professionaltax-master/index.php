<?php
$base='../../../';
 
$js=' <script src="'.$base.'js/fwork.js"></script>
<script src="'.$base.'js/alart.js"></script> 
<script src="'.$base.'js/list.min.js"></script>
<script src="'.$base.'js/picker.js"></script>
<script src="'.$base.'js/picker.date.js"></script>
<script src="'.$base.'js/picker.time.js"></script>
<script src="'.$base.'js/pdfmake.min.js"></script>
<script src="'.$base.'js/vfs_fonts.js"></script>
 ';

$css='<link rel="stylesheet" href="'.$base.'css/fwork.css">
<link rel="stylesheet" href="'.$base.'css/alart.css">
<link rel="stylesheet" href="'.$base.'css/grid.min.css">
<link rel="stylesheet" href="'.$base.'css/classic.date.css">
<link rel="stylesheet" href="'.$base.'css/classic.time.css">
<link rel="stylesheet" href="'.$base.'css/classic.css">';
include($base.'_in/header.php');
include($base.'_in/connect.php');

if (!in_array("Professional Tax Master", $arrysession)) {
   
    echo "<script>window.location.href='/process/dashboard.php';</script>";
   
    exit;

}

$con=_connect();
?>

<style>
.picker__select--year{
        height:auto;
    }
    .picker__select--month{
        height:auto;
    }
hr.style-hr {
    border: 0;
    height: 1px;
    background-image: linear-gradient(to right, rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.75), rgba(0, 0, 0, 0));
}

</style>
<style>
hr.style-hr {
    border: 0;
    height: 1px;
    background-image: linear-gradient(to right, rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.75), rgba(0, 0, 0, 0));
}
.table-list td,
   .table-list th {
       border: 1px solid #ddd;
       padding: 1px !important;
       font-size: 13px;
   }
   .table-list td{
	padding-top: 10px !important;

   }

   .table-list tr:nth-child(even) {
       background-color: #f2f2f2;
   }

   .table-list th {
       padding-top: 5px;
       padding-bottom: 5px;
       text-align: center;
       background-color: #16a085;
       color: white;
   }
</style>
<div class="container-fluid" id="section">
    <div class=" content">
        <h2 align="center" style="margin:0;">Professional Tax Master</h2>
        <hr class="style-hr">
        <div class="col-sm-10" id="section1">
            <div class="col-sm-3"></div>
            <div class="col-sm-6">
                <div class="col-sm-4">Tax Date</div>
                <div class="col-sm-8">
                    <div class="form-group">
                    <input type="text"  data-name="taxdate" data-role="text" class="form-control sm datepicker">
                    </div>
                </div>

            </div>
           
            <div class="col-sm-3"></div>
        </div>
         <div class="col-sm-10" id="section2">
            <div class="col-sm-3"></div>
            <div class="col-sm-6">
                <div class="col-sm-4">From Amount</div>
                <div class="col-sm-8">
                    <div class="form-group">
                     
                        <div class="form-group"><input type="text" data-role="number" data-name="fromamt" id="fromamt" class="form-control input-sm"></div>
               
                    </div>
                </div>

            </div>
           
            <div class="col-sm-3"></div>
        </div>
         <div class="col-sm-10" id="section3">
            <div class="col-sm-3"></div>
            <div class="col-sm-6">
                <div class="col-sm-4">To Amount</div>
                <div class="col-sm-8">
                    <div class="form-group">
                     
                        <div class="form-group"><input type="text" data-role="number" data-name="toamt" id="toamt" class="form-control input-sm"></div>
               
                    </div>
                </div>

            </div>
           
            <div class="col-sm-3"></div>
        </div>
            <div class="col-sm-10" id="section3">
            <div class="col-sm-3"></div>
            <div class="col-sm-6">
                <div class="col-sm-4">Tax Amount</div>
                <div class="col-sm-8">
                    <div class="form-group">
                     
                        <div class="form-group"><input type="text" data-role="number" data-name="taxamt" id="taxamt" class="form-control input-sm"></div>
               
                    </div>
                </div>

            </div>
           
            <div class="col-sm-3"></div>
        </div>
        <div style="clear:both;"></div>
        <hr class="style-hr">
        <div class="col-sm-3"></div>
        <div class="col-sm-6">
            <div class="col-sm-6"><button class="btn btn-success btn-sm btn-block" id="btn-submit"
                    style="margin:5px;">Submit</button></div>
            <div class="col-sm-6"><button class="btn btn-danger btn-sm btn-block" id="btn-reset"
                    onclick="$('#section').find('input').val('');" style="margin:5px;">Reset</button></div>
        </div>
        <div class="col-sm-3"></div>
        <table class="table-list table table-bordered">
    <thead>
      <tr>
        <th>SR. No</th>
        <th>Tax Date</th>
        <th>Tax Slab</th>
        <th>Tax Amount</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
    <?php
                    $count=0;
                    $result4=mysqli_query($con,"SELECT * FROM professionaltax ORDER BY id ASC" );
                     while($rows3 = mysqli_fetch_assoc($result4)){
                         $count++;
                      $taxdate= date('d/m/Y',$rows3['taxdate']/1000);
                     echo '<tr>';
                     echo '<td align="center">'.$count.'</td>';
                     echo '<td align="center">'.$taxdate.'</td>';
                     echo '<td align="center">'.$rows3['fromamt'].'-'.$rows3['toamt'].'</td>';
                     echo '<td align="center">'.$rows3['taxamt'].'</td>';
                     echo '<td align="center"><button class="btn btn-primary btn-sm " data-toggle="modal" data-target="#myModal2" data-ptid="'.$rows3['id'].'" onclick="setidtomidal(this)">Edit</button></td>';
                     
                     echo '<tr>';
                    }
    ?>
    </tbody>
  </table>
    </div>
</div>

<div id="myModal2" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" align="center">Edit Professional Tax</h4>
      </div>
      <div class="modal-body">
          <div class="form-group row">
       <div class="col-sm-4">Tax Date</div>
       <input type="hidden" data-name="ptid" id="ptidset">
       <div class="col-sm-8">
       <input type="text" id="taxdate1" data-name="taxdate"  data-role="text" class="form-control input-sm datepicker">
       </div>
       <div class="col-sm-4">From Amount</div>
       <div class="col-sm-8"><input type="text" data-name="fromamt" data-role="number" id="fromamt1" class="form-control input-sm"></div>
       <div class="col-sm-4">To Amount</div>
       <div class="col-sm-8"><input type="text" data-name="toamt"  data-role="number" id="toamt1" class="form-control input-sm"></div>
       <div class="col-sm-4">Tax Amount</div>
       <div class="col-sm-8"><input type="text" data-name="taxamt"  data-role="number" id="taxamt1" class="form-control input-sm"></div>
       
       <div class="col-sm-4"></div>
       <div class="col-sm-8"><button class="btn btn-success btn-block btn-sm" onclick="edittax()">Save</button></div>
        </div>
        
      </div>
    </div>
  </div>
</div>


<?php
include($base.'_in/footer.php');
?>
<script>
function setidtomidal(e) {
    var ptid = $(e).data('ptid');
    $('#ptidset').val(ptid);
    $.ajax({
                type: "POST",
                data: 'ptid='+ptid,
                url: 'taxdetails.php',
                cache: false,
                success: function (res) {
                    if(res.status=='success'){
                        modals.putvalue('myModal2',res.json);
                    }
                }
            });
}

function edittax(e){
var valid=true;
    if(checker('myModal2') !=false ) {
            valid=valid*true;
        }else{
            valid=valid*false;
         }
    if(valid){
        var data1=checker('myModal2');
         data1 = JSON.stringify(data1);
        $.ajax({
                type: "POST",
                data: {
                    data1: data1      
                },
                url: 'update.php',
                cache: false,
                success: function (res) {
                    if(res.status=='success'){
                        swal({
                            type: 'success',
                            title: 'Your Work is Save',
                            showConfirmButton: false,
                            timer: 2000
                        });
                        setTimeout(function () {
                            window.location.reload();
                        }, 2000);
                    }
                }
            });
    }
}

$('.datepicker').pickadate({     
    selectYears: 100,
	selectMonths: true,
	onClose: function () {
		try {
			var dateStamp = this.get('select')['pick'];
			var id = this.get('id');
			$('#' + id).attr('data-stamp', dateStamp);
			$('#' + id).parent().find('.timepicker').click();
		} catch (err) {
			console.log(err);
		}
	}
});


$('#btn-submit').on('click',function(){
    var valid=true;
    if(checker('section') !=false ) {
			valid=valid*true;
		}else{
		    valid=valid*false;
         }
    if(valid){
        var data1=checker('section');
         data1 = JSON.stringify(data1);
        $.ajax({
				type: "POST",
				data: {
                    data: data1      
				},
				url: 'insert.php',
				cache: false,
				success: function (res) {
                    if(res.status=='success'){
						swal({
							type: 'success',
							title: 'Your Work is Save',
							showConfirmButton: false,
							timer: 2000
						});
						setTimeout(function () {
							window.location.reload();
						}, 2000);
                    }
				}
			});
    }
})

</script>