<?php
$base='../../../';
$js='<script src="'.$base.'js/fwork.js"></script>
<script src="'.$base.'js/alart.js"></script>
<script src="'.$base.'js/list.min.js"></script>';
$css='<link rel="stylesheet" href="'.$base.'css/fwork.css">
<link rel="stylesheet" href="'.$base.'css/alart.css">';
$title="Item Master";
include($base.'_in/header.php');
include($base.'_in/connect.php');

?>
<style>
.hider{
    display:none;
}
hr.style-hr {
    border: 0;
    height: 1px;
    background-image: linear-gradient(to right, rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.75), rgba(0, 0, 0, 0));
}
    .panel{
    z-index: 1;
    -webkit-box-shadow: 0px 3px 7px -1px rgba(0,0,0,0.75);
-moz-box-shadow: 0px 3px 7px -1px rgba(0,0,0,0.75);
box-shadow: 0px 3px 7px -1px rgba(0,0,0,0.75);
}
.list1 {
       padding: 0px;
       min-height:0px;
       max-height:400px;
      
   }

   .list1 li span {
    cursor:pointer;
    display:block;
    font-size:14px;
    margin:2px;
   }

   .list1 li {
       text-decoration: none;
       display: block;
       padding: 2px;
       background: #FFF;
       color: #000;
       font-size:11px;
   }

   .list2 {
       padding: 0px;
       min-height:0px;
       max-height:400px;
      
   }

   .list2 li span {
    cursor:pointer;
    display:block;
    font-size:14px;
    margin:2px;
   }

   .list2 li {
       text-decoration: none;
       display: block;
       padding: 2px;
       background: #FFF;
       color: #000;
       font-size:11px;
   }
   .table-list td,
   .table-list th {
       border: 1px solid #ddd;
       padding: 1px !important;
       font-size: 13px;
   }
   .table-list td{
	padding-top: 10px !important;

   }

   .table-list tr:nth-child(even) {
       background-color: #f2f2f2;
   }

   .table-list th {
       padding-top: 5px;
       padding-bottom: 5px;
       text-align: center;
       background-color: #16a085;
       color: white;
   }
    </style>

<input type="hidden" id="item-json1" value='<?php echo str_replace("'"," &#39;",$json1); ?>'/>
<input type="hidden" id="item-json2" value='<?php echo str_replace("'"," &#39;",$json2); ?>'/>
<input type="hidden" id="item-json3" value='<?php echo str_replace("'"," &#39;",$json3); ?>'/>
<div class="container-fluid" id="section" style="margin-left:5%;">
    <h2 align="center" class="content">Supplier Approvals</h2>
    <hr class="style-hr">
        <div class="row" id="section">
            <div class="col-sm-12">
            <table  class="table table-list">
                    <thead>
                        <tr>
                            <th>Sr No</th>
                            <th>Item Name</th>
                            <th>Details</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                    $con=_connect();
                    $count=0;
                        $result4=mysqli_query($con,"SELECT DISTINCTROW itemcode FROM msirate WHERE approval=0 ORDER BY timestamp DESC" );
                         while($rows3 = mysqli_fetch_assoc($result4)){
                            $count++;
                             $itemcode=$rows3["itemcode"];
                             $itemname=mysqli_fetch_assoc(mysqli_query($con,"SELECT itemname x FROM mitemmaster WHERE itemcode='$itemcode'"))['x'];
                             echo '<tr>';
                             echo '<td align="center">'.$count.'</td>';
                             echo '<td align="center">'.$itemname.'</td>';
                             echo '<td><table  class="table table-list">
                             <thead>
                                 <tr>
                                     <th width="500">Supplier Name</th>
                                     <th>New Supplier Rate</th>
                                     <th>Old Supplier Rate</th>
                                     <th>Action</th>
                                 </tr>
                             </thead>
                             <tbody>';
                             $result5=mysqli_query($con,"SELECT sirid,scode,rate FROM msirate WHERE itemcode='$itemcode' AND approval=0 ORDER BY timestamp DESC");
                             while($rows4 = mysqli_fetch_assoc($result5)){
                                 $scode=$rows4['scode'];
                                 $rate=$rows4['rate'];
                                 $sirid=$rows4['sirid'];
                                 ?>
                                
                                <?php
                                 $sname=mysqli_fetch_assoc(mysqli_query($con,"SELECT sname x FROM msuppmast WHERE scode='$scode'"))['x'];

                                 $hrate=mysqli_fetch_assoc(mysqli_query($con,"SELECT rate x FROM msirate_history WHERE sirid='$sirid' Order by sirhid DESC LIMIT 1"))['x'];
                                echo '<tr>';
                                echo '<td align="center">'.$sname.'</td>';
                                echo '<td align="center">'.$rate.'</td>';
                                echo '<td align="center">'.$hrate.'</td>';
                                echo '<td align="center" width="250"><button class="btn btn-sm btn-success" style="width:100px;" data-app="'.$sirid.'" onclick="approval2(this)" > Approve</button><button style="margin-left:10px;width:100px;" class="btn btn-sm btn-danger" data-app="'.$sirid.'" onclick="reject(this)" > Reject</button></td>';
                                echo '<tr>';
                             }
                                ?>
                                </tbody>
                                </table>

                                <?php
                            
                            
                             echo '</td>';
                             echo '</tr>';
                          
                    }
                    ?>
                    </tbody>
                    </table>

        
        </div>
    </div>
</div>
<script>

function reject(e){
    var code = $(e).attr('data-app');
    $.ajax({
        type: "POST",
        data: 'code='+code,
        url: 'reject.php',
        cache: false,
        success: function (res) {
            if (res.status == 'success') {
                swal({
                    type: 'success',
                    title: 'Item Rate Rejected successfully',
                    showConfirmButton: false,
                    timer: 3000
                });
                setTimeout(function () {
                    window.location.reload();
                }, 3000);
            }
        }
    })
}

function approval2(e) {
    var code = $(e).attr('data-app');
    $.ajax({
        type: "POST",
        data: 'code='+code,
        url: '/process/create-master/approval/approved2.php',
        cache: false,
        success: function (res) {
            if (res.status == 'success') {
                swal({
                    type: 'success',
                    title: 'Item Rate approved successfully',
                    showConfirmButton: false,
                    timer: 3000
                });
                setTimeout(function () {
                    window.location.reload();
                }, 3000);
            }
        }
    })
}

</script>
<?php
include($base.'_in/footer.php');
?>
