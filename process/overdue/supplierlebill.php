<?php 

  $base    = '../../';

  $navenq2 = 'background:#1B1464;';

  include('header.php');

  if (!in_array('Supplier Bills', $arrysession) ) {
   
    echo "<script>window.location.href='/process/dashboard.php';</script>";
   
    exit;

}

  if (session_status() == PHP_SESSION_NONE) { session_start(); }

  $sessionby = $_SESSION['employeeid'];

  $branchid  = $_SESSION['branchid'];

?>

<br>

<style>

  .input-container {

    max-width: 300px;
    background-color: #EDEDED;
    border: 1px solid #DFDFDF;
    border-radius: 5px;
  }

 

  @media (max-width: 300px) {
    button {
      width: 100%;
      border-top-right-radius: 5px;
      border-bottom-left-radius: 0;
    }

    
  }

    .tableFixHead {
        overflow-y: auto;
        height: 600px;
    }

    .tableFixHead thead th {
        position: sticky;
        top: 0;
    }

    .tableFixHead table {
        border-collapse: collapse;
        width: 100%;
    }

    /* #components th, td { padding: 8px 16px; } */
    .tableFixHead th {
        background: #2980b9;
        color: #fff;
    }

</style>

<img class="hidden" id=imageid src="../../../../img/PestMortemLogo.jpg">

<!-- <div class="row">

    <div class="col-sm-2"></div>

    <div class="col-sm-2">

        <b>From Date :</b>

        <div class="form-group">

            <input type="date" class="form-control input-md" data-name="fromdate" id="fromdate">
        
        </div>

    </div>

    <div class="col-sm-2">

        <b>To Date :</b>
    
        <div class="form-group">

            <input type="date" class="form-control input-md" data-name="todate" id="todate">
                
        </div>
            
    </div>


    <div class="col-sm-2">

        <div class="form-group">

            <b>Select Customer</b>

            <select class="form-control input-md select-js1"  data-role="select" data-name="customerid" id="customerid">
            
                <option value="Select">Select Customer</option>

                    <?php 
                                
                    // $result2=mysqli_query($con, "SELECT customerid, customername FROM `customermaster` WHERE branchid = '$branchid' ");

                    // while($rows=mysqli_fetch_assoc($result2)) {

                    //     echo '<option value="'.$rows['customerid'].'">'.$rows['customername'].'</option>';

                    // }

                    ?>
                            
            </select>

        </div>

    </div>

    <div class="col-sm-2">

        <br>

        <a class="btn btn-primary btn-sm btn-block" onclick="submit()">Submit</a>

    </div>

</div> -->

<div class="container-fluid tableFixHead table-responsive">

    <table class="table table-bordered " id="table-order">

        <thead>

            <tr>

              <th class="text-center">Invoice Date</th>

              <th class="text-center">Bill No</th>

              <th class="text-center" width=35%>Customer Name</th>

              <th class="text-center">0 > 30</th>

              <th class="text-center">30 > 60 </th>

              <th class="text-center">60 > 90 </th>

              <th class="text-center">90 > 120 </th>

            </tr>

        </thead>

        <tbody>
        
            
        </tbody>

    </table>

</div>



<?php 

  include('footer.php');

?>

<script>

$(document).ready(function () {

  $('.select-js1').select2();

});


// function submit(){

//     let customerid = $('#customerid').val();

//     let fromdate   = $('#fromdate').val();

//     let todate     = $('#todate').val();

    $.ajax({

        type: "POST",

        // data: 'customerid=' + customerid + '&fromdate=' + fromdate + '&todate=' + todate,

        url: 'api/supplierbill.php',

        cache: false,

        success: function (res) {

            if (res.status == 'success') {

              let json = res.billingjson;
        
              let str  = '';

              for (let i in json) {

                let zeroToThrity     = 0;
                
                let thrityToSixty    = 0;
                
                let sixtyToNinty     = 0;
                
                let nintyToOnetwenty = 0;


                if (0 < json[i].noofday && 30 > json[i].noofday ) {

                  zeroToThrity = parseFloat(json[i].billamount).toFixed(2);

                } else if ( 30 < json[i].noofday && 60 > json[i].noofday) {

                  thrityToSixty = parseFloat(json[i].billamount).toFixed(2);

                } else if ( 60 < json[i].noofday && 90 > json[i].noofday) {

                  sixtyToNinty = parseFloat(json[i].billamount).toFixed(2);

                } else if ( 90 < json[i].noofday && 120 > json[i].noofday) {

                  nintyToOnetwenty = parseFloat(json[i].billamount).toFixed(2);

                }

                str += '<tr>';

                  str +='<td>'+json[i].invoiceDate+'</td>';
              
                  str +='<td>'+json[i].billNo+'</td>';
                  
                  str +='<td>'+json[i].customername+'</td>';
                  
                  str +='<td class="text-right">'+zeroToThrity+'</td>';

                  str +='<td class="text-right">'+thrityToSixty+'</td>';

                  str +='<td class="text-right">'+sixtyToNinty+'</td>';

                  str +='<td class="text-right">'+nintyToOnetwenty+'</td>';
                                        
                str += '</tr>';

              }

                $('#table-order > tbody').html(str);

            }

        }

    })
    
// }




</script>