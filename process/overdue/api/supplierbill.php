<?php

    $base = '../../../';

    include($base.'_in/connect.php');

    header('content-type: application/json; charset=utf-8');

    header("access-control-allow-origin: *");

    $con        = _connect();

    if (session_status() == PHP_SESSION_NONE) { session_start(); }

    $branchid  = $_SESSION['branchid'];

    $todaydate = $todate;

    $billing   = mysqli_query($con,"SELECT DISTINCT purchaseid FROM purchasebill WHERE branchid='$branchid' ");

    while ($rows = mysqli_fetch_assoc($billing)) {

        $purchaseid   = $rows['purchaseid'];

        $row          = mysqli_fetch_assoc(mysqli_query($con,"SELECT billid,supplierid,purchasedate FROM purchasebill WHERE branchid='$branchid' AND purchaseid = '$purchaseid' AND payment_time = 0  "));

        $purchasedate = $row['purchasedate'];

        $billid       = $row['billid'];

        $supplierid   = $row['supplierid'];

        $netamount    = mysqli_fetch_assoc(mysqli_query($con,"SELECT sum(netamount) x FROM purchasebill WHERE branchid='$branchid' AND purchaseid = '$purchaseid' AND payment_time = 0 "))['x'];

        $custDetails  = mysqli_fetch_assoc(mysqli_query($con,"SELECT suppliername,supplierjson FROM suppliermaster WHERE supplierid = '$supplierid' "));

        $creditdays   = get_object_vars(json_decode($custDetails['supplierjson']))['creditdays'];

        $addCreditDay = date('Y-m-d', strtotime($purchasedate. ' + '.$creditdays.' days')); 

        $receiveAmt   = mysqli_fetch_assoc(mysqli_query($con,"SELECT sum(Amount) x FROM paymentbill WHERE supplierid = '$supplierid' AND purchaseid = '$purchaseid' "))['x'];

        if ($receiveAmt) {

            $collectAmount = $netamount - $receiveAmt;

        } else {

            $collectAmount = $netamount;

        }

        $now       = time();
        
        $your_date = strtotime($addCreditDay);
        
        $datediff  = $now - $your_date;

        $noofday   = round($datediff / (60 * 60 * 24)-1);
    
        if ($noofday > 0  && $collectAmount != 0) {
            
            $billingjson.=',{"invoiceDate":"'.date("d-m-Y",strtotime($row['purchasedate'])).'","billNo":"'.$rows['purchaseid'].'","customername":"'.$custDetails['suppliername'].'","billamount":"'.$collectAmount.'","noofday":"'.$noofday.'"}';

        }

        
    }

    $billingjson = substr($billingjson,1);

    $billingjson = '['.$billingjson.']';  


    if ($billing) {

        echo '{"status":"success","billingjson":'.$billingjson.'}';   

        
    } else {

        echo '{"status":"falid1"}';

    }


    _close($con);

?>