<?php

    $base = '../../../';

    include($base.'_in/connect.php');

    header('content-type: application/json; charset=utf-8');

    header("access-control-allow-origin: *");

    // if (isset($_POST['customerid'])) {

        $con        = _connect();
        
        // $customerid = $_POST["customerid"];

        // $fromdate   = '';

        // $todate     = '';

        // if(isset($_POST['fromdate'])){ $fromdate=$_POST['fromdate']; }

        // if(isset($_POST['todate'])){ $todate=$_POST['todate']; }

        if (session_status() == PHP_SESSION_NONE) { session_start(); }

        $branchid = $_SESSION['branchid'];

        // if($fromdate=='' && $todate=='' && $customerid!=''){

        //     $query = "SELECT opening_balance,opening_date FROM customermaster WHERE branchid='$branchid' AND customerid = '$customerid' ";

        // }

        // if($fromdate!='' && $todate!='' && $customerid!=''){

        //     $query = "SELECT opening_balance,opening_date FROM customermaster WHERE branchid='$branchid' AND customerid = '$customerid' ";

        // }

        $todaydate = $todate;

        $billing = mysqli_query($con,"SELECT * FROM billing WHERE branchid='$branchid' ORDER BY invoiceDate DESC ");

        while ($row = mysqli_fetch_assoc($billing)) {

            $invoiceDate = $row['invoiceDate'];

            $customerid  = $row['customerid'];

            $invoiceNo   = $row['invoice_bill'];

            $contractamt = $row['contractamt'];

            $custDetails = mysqli_fetch_assoc(mysqli_query($con,"SELECT customername,customerjson FROM customermaster WHERE customerid = '$customerid' "));

            $creditdays  = get_object_vars(json_decode($custDetails['customerjson']))['creditdays'];

            $addCreditDay = date('Y-m-d', strtotime($invoiceDate. ' + '.$creditdays.' days'));

            $receiveAmt   = mysqli_fetch_assoc(mysqli_query($con,"SELECT sum(receiveAmt) x FROM invoicepayment WHERE customerid = '$customerid' AND invoiceNo = '$invoiceNo' "))['x'];

            if ($receiveAmt) {

                $collectAmount = $contractamt - $receiveAmt;

            } else {

                $collectAmount = $contractamt;

            }

            $now       = time();
            
            $your_date = strtotime($addCreditDay);
            
            $datediff  = $now - $your_date;

            $noofday   = round($datediff / (60 * 60 * 24)-1);
      
            if ($addCreditDay > $todaydate  && $collectAmount != 0) {
                
                $billingjson.=',{"invoiceDate":"'.date("d-m-Y",strtotime($row['invoiceDate'])).'","billNo":"'.$row['invoice_bill'].'","customername":"'.$custDetails['customername'].'","billamount":"'.$collectAmount.'","noofday":"'.$noofday.'"}';

            }

            
        }

        $billingjson = substr($billingjson,1);

        $billingjson = '['.$billingjson.']';  


        if ($billing) {

            echo '{"status":"success","billingjson":'.$billingjson.'}';   

            
        } else {

            echo '{"status":"falid1"}';

        }


        _close($con);

    // } else {

    //     echo '{"status":"falid"}';
        
    // }
?>