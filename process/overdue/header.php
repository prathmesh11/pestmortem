<?php

$css = '<link rel="stylesheet" href="'.$base.'css/classic.css">
<link rel="stylesheet" href="'.$base.'css/classic.date.css">
<link rel="stylesheet" href="'.$base.'css/classic.time.css">
<link href="'.$base.'/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" href="'.$base.'css/alart.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="'.$base.'css/grid.min.css">';

$js = '<script src="'.$base.'js/picker.js"></script>
<script src="'.$base.'js/picker.date.js"></script>
<script src="'.$base.'js/picker.time.js"></script>
<script src="'.$base.'js/pdfmake.min.js"></script>
<script src="'.$base.'js/vfs_fonts_times.js"></script>
<script src="'.$base.'js/vfs_fonts.js"></script>
<script src="'.$base.'/js/select2.min.js"></script>
<script src="'.$base.'../js/alart.js"></script>
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
<script src="'.$base.'js/fwork.js"></script>';
// <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.68/pdfmake.min.js"></script>

    include($base.'_in/header.php');
    include($base.'_in/connect.php');

    if (!in_array("Customer Bills", $arrysession) && !in_array('Supplier Bills', $arrysession) ) {
   
        echo "<script>window.location.href='/process/dashboard.php';</script>";
       
        exit;

    }
    
    $con = _connect();

 
    if (session_status() == PHP_SESSION_NONE) { session_start(); }
    $branchid = $_SESSION['branchid'];
    
?>
<style>
    
    #myModal1 .col-sm-4,.col-sm-8{
      margin-bottom:2px;
    }
    .picker__select--year{
        height:auto;
    }
    .picker__select--month{
        height:auto;
    }
    
</style>
<div class="container-fluid">
    <div id="div-content" class="content">
        <table width="100%">
            <tr>
                <td align="center" style="<?php echo $customerbill; ?>"><a
                        href="/process/overdue/customerbill.php"
                        style="border:1px solid blue;border-radius:0px;<?php echo $navenq1;?>"
                        class="btn btn-primary btn-block">Customer Bills</td>

                <td align="center" style="<?php echo $supplierbill; ?>"><a
                        href="/process/overdue/supplierlebill.php"
                        style="border:1px solid blue;border-radius:0px;<?php echo $navenq2;?>"
                        class="btn btn-primary btn-block">Supplier Bills</td>
            </tr>
        </table>
