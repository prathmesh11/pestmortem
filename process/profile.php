<?php

    $base = '../';
    
    $js   = '<script src="'.$base.'js/fwork.js"></script>
            <script src="'.$base.'js/picker.js"></script>
            <script src="'.$base.'js/picker.date.js"></script>
            <script src="'.$base.'js/picker.time.js"></script>
            <script src="'.$base.'js/alart.js"></script>';
    
    $css  = '<link rel="stylesheet" href="'.$base.'css/fwork.css">
            <link rel="stylesheet" href="'.$base.'css/classic.css">
            <link rel="stylesheet" href="'.$base.'css/classic.date.css">
            <link rel="stylesheet" href="'.$base.'css/classic.time.css">
            <link rel="stylesheet" href="'.$base.'css/alart.css">';
    
    include($base.'_in/header.php');
    
    include($base.'_in/connect.php');
    
    $con = _connect();

    if (session_status()==PHP_SESSION_NONE) { session_start(); }

    $created_by     = $_SESSION['employeeid'];

    $branchid       = $_SESSION['branchid'];

    $employeemaster = mysqli_fetch_assoc(mysqli_query($con,"SELECT password,username FROM employeemaster WHERE branchid = '$branchid' AND employeeid = '$created_by' "));

    $branchname     = mysqli_fetch_assoc(mysqli_query($con,"SELECT branchname x FROM branchmaster WHERE branchid = '$branchid' "))['x'];

    $checkLeave     = mysqli_fetch_assoc(mysqli_query($con,"SELECT paid_leave,casual_leave,standanrd_leave FROM leaveopeningbalance WHERE employeeid='$created_by' AND branchid = '$branchid' "));

?>

<style>

    hr.style-hr {
        border: 0;
        height: 1px;
        background-image: linear-gradient(to right, rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.75), rgba(0, 0, 0, 0));
    }

    .tableFixHead {
        overflow-y: auto;
        height: 446px;
    }

    .tableFixHead thead th {
        position: sticky;
        top: 0;
    }

    .tableFixHead table {
        border-collapse: collapse;
        width: 100%;
    }

    /* #components th, td { padding: 8px 16px; } */
    .tableFixHead th {
        background: #2980b9;
        color: #fff;
    }

</style>

<div class="container-fluid" style="background:#eee;">
            
    <div class="row content">

        <input type="hidden" id="empId" value ="<?php echo $created_by;?>" class="form-control input-sm"  data-role="text" data-name="empId" autocomplete="rutjfkde" autofocus/>

        <div class="col-sm-2" id="section" style="border-radius:4px;border:1px solid #CCC;background:#FFF;padding-top:10px;-webkit-box-shadow: 2px 2px 3px 3px #ccc; -moz-box-shadow:2px 2px 3px 3px #ccc;box-shadow:2px 2px 3px 3px #ccc;">

            <div class="col-sm-12" align="center">

                <h4>Personal Info</h4>

            </div>

            <div style="clear:both;"></div>

            <div class="form-group col-sm-12">

                <input type="text" data-name="username" id="username" data-role="text" class="form-control"  autocomplete="rutjfkde" autofocus>

            </div>

            <div class="col-sm-12 img-class" align="center" ><img id="img" class="img-responsive" style="width:170px;height:170px;border:1px solid #CCC;" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMgAAADICAIAAAAiOjnJAAAACXBIWXMAAAsSAAALEgHS3X78AAAJ00lEQVR42u2dXXOTXBeGcScQSkqTZog1GbWOdvTI//83nPFQR20aQyMpLYHSncCmzwFvO33TJiUpJOzFfR046jiVwMW91v6AvLq9vVUAyBuGUwAgFoBYAGIBALEAxAIQCwCIBSAWgFgAQCxQduo4BQtEURRFkRCCc776XzabTUVRVFVVVRXnbYFXWITmnIdhyDmfzWbPyrQMVVV1XW80Gs1ms9Fo1Go1iFVRscIw9H3f9/0oinL/4YZhmKZpmmZlw6xyYgkhPM9zXbcIn540rN1ut1otiEVZKdd1XddNkmTL/7Wqqt1ut1J6VUUs3/dHo9H2lVrQq9/vG4YBsYgElW3bvu+X5Hg6nY5lWeS7e+Jicc6Hw+F22qns6Lre6/V0XYdYUuJ53vn5+W7L3zIYY8fHx4TdIiuW53mj0ajkB9nv96l29DSXdNJWvfzHORqNNp6ShVg76KuksCrl9PSUpFvUxBJCDIfDcvZVT5IkiW3bQgiIVWps2y7bGDBLxNq2DbHKi+u65ZmvWrcplPTI6YslhHAcR97jH4/HlAoiHbHG47FErdVjoihyXRdile6qeJ5HoJSTCS0iYkldBB+OEMmEFgWxaMQVsdCiIBaNuLoPLRo3ifRipTtCKQ3UaVRD6cUiZlVa2Qks8kAsfCiI9QjOOckVXAKz8AwXANUQYi0SBIFClDAMIRZua4QxIbFkv6eRWCXl+vpaIY3UbiGxSj3mhVg7aLCk2ykKsXDSy3LzQCyIhVpPQizyDZbsoYXEglgQ6w4hhNTb26tw/0gp1mw2U6qBvLtJGe5j3EIQS/r7GImF+xggsegi76wKeiwAse6oyFwDxAKo+/KLVZHFHNlHKkgsALEAxMKQEGKhmQUQC0AsALEAqLxY5B/OgVgQC6AUAogFIBYAEAtALHCPqqoQC0AsiAUgFoBYKA25Xh7GIBbEyh9d1yEWABAL8QyxFmg2mxALYoEXUavVIBZuYjTvEAsfFmJRuo8hFk437p/qidVoNBBXEAu3chXvH4hVXqSesZO1x5J3dRaJhbt5xzePvLOjEotlGAZtsWT/gBALkQyx/r9/pz2bhcTCqS+kwZL9tpFYLNM0qYpF4KPJLRbVSQcCYcxwZ5fukjCGxIJY+FBExaJXDdvtNsTCZch/PEhjtCu9WJ1Oh5JYrVaLSKdI4Ban1GmRCWAKDQqZ0Gq1WmSWEyiIZRgGjb6EUlknMqQiUEEMw6C0gZGIWASKSLfbpTQKoTMJJPWFabVaxNbU6Ygl9bUhFlcKsXc3SHp5Op0Ovb1lpMSScXjIGLMsSyEHtYW2fr8vXcpK/dBEVcRSVVWi2SBd14ktSZEVS1EUy7Jk2fLQ6/UUohAUq1arSVEQO50O4Ue6aW7tNU2z5CvTqqqS7NmJi5VWmTIXxLdv35Ls2emLVeaCaFkW+feaUH61hmmaJRxz7e3t0Ztnr5ZYiqIcHR1pmlae44njuCJvjaP/MqD3798nSVKGI0mSZDweX15eQiwKpN96Xwa3HMeZz+cQiwicc8YY53y3bjmOE4ahoijT6XQ+n0Ms6Ukvp2EYl5eXu3LLcZwgCNLf397eXlxcQCzpub6+vh8k2ra9ZbeSJHloVcpoNIJY0tfBe5M0TWs2m2dnZ1urREmS2La9YJWiKJPJRAgBsSTG87yHf2y3241G4+/fv9PpdAtOL5NYCPHv3z+IJTFXV1cLf2NZVq1Wu7i4sG07juOCgir9+SvK7nA4hFgSx9XjS1uv19PV3zRRLi4u8u26giA4Ozt7NhEnk8nNzQ3hk//q9vaW6mf7+fNnOon1mOl0ej80Y4wZhnF4eFiv11+SUkEQeJ6XPQU/ffr05csXqie/TvWDeZ63zCpFUQ4ODubzue/7904EQaBpmmmauq5nXwVKkiQMw5ubm8cd+rMMBoOTkxOqexxoJpYQ4vfv3yvEuq9HqVuL/QFjmqbt7e0pT329ShzHcRzPZrM4jl84wDw5Ofn8+TMSSxomk8mzVimK0ul05vP5bDZ7nEOcc8550cf558+fDx8+lGqZHM37UsIwdF0304dn7M2bNzt8VX8cxz9+/MCoUI4iuNa8NmPs9evXO9zHfHp6SnJZmppYg8EgSxFcwLKsw8PDXR3zt2/f6E3EkxJrNBpt3Bi12+2jo6OdjNFubm6+f/8Oscpr1cICzroYhtHv99PB4PYP/tevX5huKF1fNR6PX2jVQ6bT6dXV1fbL09evX9+9ewexymLVYDDIfWogSRLXdZ+c5SqCvb29drut63q/36fx4mS5xQrD8OzsrLgtVnEcX11dFadX+lb3g4ODh6tJNNySVSwhxGQyyThf9XK90jWfDcaby3zSdX3FS5darZZ0r82hIJbneefn59vfZzyfz4Mg4Jw/nqx/llqtpmla+gbbLFPtuq73ej15n2uVTCzP8xzHySs5XtKBzefzdHtqulwYx/H9UaUOKYpSr9fr9bqmaemvG/xHlmV1Oh0ZF6rlEEsI4Xme67o7V2r7pOtO+/v7culVarGEEEEQ+L6/tdFZmfVqt9sSva20jGJFURSGIXxa1nvt7++n+8YgVqZwCu/Ywn4VGhnWbDYbjUb6a9kK5S7Fgkw5kn7xvWEYmqapqrpz1bYtFmTaZqTpuq7rejpttmXVtiQW59z3/dQnXPIdplpqWLPZLPqF+MWK5d9RkhcJgYekb2otaCKjELE45+kKLnySomKaptntdvOdyMhZrHQaE82TjLRarRy3OuYmVkkWW8AL06vb7eby4tYcxOKc27aNlKLUe/V6vRdG10vFchxnMpngYhDj5XsrNhdLCDEcDjF9QLgsHh8fb+zWhmKh/MGt/MXinJ+enmIqoSJuffz4cYOZiLUf/xJCwKrqkCTJcDjc4IEltq5Vg8EAVlUKzvl4PC5WLPRV1cTzvHX3xq0h1jafswNlYzwer1UQs4oVRZHjODi/lSWKorUmLLOKNRqN0FpVnLUeZskkFvZRgZTsVSuTWFX4ig6QsYvPGFosx58FEFpriIWeHTzE9/0sw0P2bHeFuAIPSZIky6vI2LMDAZxKsIEVq8SKoggzouBJMZ5dgFkl1uOvzgLgfki3uVg5vtUT0GvhNxSLc462HWxcDRniChQRWmzjrAMVZ/U36bFlQYc6CFbDOV8xU8oQV6CI0HpaLOxlAFm4vr5GYoH8WRFADHEFNiaKomVtFlsr3wDIGENILLAtsfCAF8jOMlvY43+HhyZA/omFOghyCS2GOgiKCK1FsTb4wjSAxHpGLCEEEgusy5NhxBBXoPDEwtQoyKvNQmKBQkILiQUKFiuKIkyNgrz6d7aiTAKQQ2JhogHk2L8jsUAhofU/sTA1CgoRCxMNIN9S+B9fledcwXKJ+QAAAABJRU5ErkJggg==" accept="image/*" /></div>
                    
                <div class="col-sm-12"><br><input type="file" class="form-control" onchange="getfile(this)" name="photo"></div>

                <div class="col-sm-12"><br><button style="border-radius:0px;" class="btn btn-primary btn-block" onclick="personalui()">PERSONAL DETAILS</button></div>
                
                <div class="col-sm-12"><br><button style="border-radius:0px;" class="btn btn-primary btn-block" data-toggle="modal" data-target="#resetPassword" data-branchid='<?php echo $branchid;?>' data-userid='<?php echo $created_by;?>' data-password='<?php echo $employeemaster['password'];?>' onclick="resetPassword(this)">Reset Password</button></div>
                
                <div class="col-sm-12"><br><button style="border-radius:0px;" class="btn btn-primary btn-block" data-branchid='<?php echo $branchid;?>' data-userid='<?php echo $created_by;?>' data-username='<?php echo $employeemaster['username'];?>' data-branchName='<?php echo $branchname;?>' onclick="leaveApplication(this)">Leave Application</button></div>
                
                <div class="col-sm-12"><br><button style="border-radius:0px;" class="btn btn-primary btn-block" onclick="attachment()">Loan Application</button><br><br></div>
                
                <div class="col-sm-12"><br><button style="border-radius:0px;" class="btn btn-success btn-block" id="submit" onclick="submit()">SUBMIT</button><br></div>

            </div>

        <div class="col-sm-10" id="section1">

            <div class="col-sm-12" align="center"
                style="border-radius:4px;border:1px solid #CCC;background:#FFF;padding-top:10px;-webkit-box-shadow: 2px 2px 3px 3px #ccc; -moz-box-shadow:2px 2px 3px 3px #ccc;box-shadow:2px 2px 3px 3px #ccc;">

                <h3 style="margin:5px;" id="details">Personal Details</h3>

            </div>

            <div style="clear:both;"></div>

            <hr style="margin:6px;">

            <div class="col-sm-6" style="padding-bottom:10px;border-radius:4px;border:1px solid #CCC;background:#FFF;padding-top:10px;-webkit-box-shadow: 2px 2px 3px 3px #ccc; -moz-box-shadow:2px 2px 3px 3px #ccc;box-shadow:2px 2px 3px 3px #ccc;">
                
                <div class="col-sm-4" style="margin-top:4px;">Gender</div>
                
                <div class="col-sm-8" style="margin-top:4px;">

                    <label for="r1"><input type="radio" class="Radio-Input" id="r1" name="gender" data-role="radio"
                            data-name="gender" value="male" checked /> Male</label>

                    <label for="r2"><input type="radio" class="Radio-Input" id="r2" name="gender" data-role="radio"
                            data-name="gender" value="female" /> Female</label>

                </div>

                <div class="col-sm-4" style="margin-top:4px;">DOB</div>

                <div class="col-sm-8" style="margin-top:4px;">

                    <input type="text" class="datepicker form-control input-sm" data-role="text" data-name="dob" />

                </div>

                <div class="col-sm-4" style="margin-top:4px;">Contact No</div>

                <div class="col-sm-8" style="margin-top:4px;">
                    
                    <input type="text" id="phoneno" class="form-control input-sm" data-role="number" data-name="contactno" autocomplete="rutjfkde"
                        autofocus />

                </div>

                <div class="col-sm-4" style="margin-top:4px;">Email Id</div>

                <div class="col-sm-8" style="margin-top:4px;">
                    
                    <input type="text" id="emailno" class="form-control input-sm" data-name="email" autocomplete="rutjfkde" autofocus />
                    
                </div>

                <div class="col-sm-4" style="margin-top:4px;">Experience</div>

                <div class="col-sm-8" style="margin-top:4px;">
                    
                    <input type="text" class="form-control input-sm" data-role="text" data-name="experience" autocomplete="rutjfkde" autofocus />
                
                </div>

                <div class="col-sm-4" style="margin-top:4px;">Qualification</div>

                <div class="col-sm-8" style="margin-top:4px;">
                    
                    <input type="text" class="form-control input-sm" data-role="text" data-name="qualification" autocomplete="rutjfkde" autofocus />
                
                </div>

                <div class="col-sm-4" style="margin-top:4px;">Address</div>

                <div class="col-sm-8" style="margin-top:4px;">
                    
                    <textarea rows="5" type="text" id="inp-address" class="form-control input-sm" data-role="text" data-name="address" autocomplete="rutjfkde" autofocus></textarea>
                
                </div>

                <div class="col-sm-4" style="margin-top:4px;">Permenant Address</div>

                <div class="col-sm-8" style="margin-top:4px;">
                    
                    <textarea rows="5" id="inp-paddress" class="form-control input-sm" data-role="text" data-name="paddress" autocomplete="rutjfkde" autofocus></textarea>
                    
                </div>
                
                <div class="col-sm-4" style="margin-top:4px;">Language</div>
                <div class="col-sm-8" style="margin-top:4px;"><input type="text" class="form-control input-sm"
                        data-role="text" data-name="language" autocomplete="rutjfkde" autofocus /></div>
                <div class="col-sm-4" style="margin-top:4px;">Blood Group</div>
                <div class="col-sm-8" style="margin-top:4px;"><input type="text" class="form-control input-sm"
                        data-role="text" data-name="blood_group" autocomplete="rutjfkde" autofocus /></div>

                <div class="col-sm-4" style="margin-top:4px;">Emergency Name</div>
                <div class="col-sm-8" style="margin-top:4px;"><input type="text" class="form-control input-sm"
                        data-role="text" data-name="emergency_Name" autocomplete="rutjfkde" autofocus /></div>
                <div class="col-sm-4" style="margin-top:4px;">Emergency Contact</div>
                <div class="col-sm-8" style="margin-top:4px;"><input type="number" class="form-control input-sm"
                        data-role="text" data-name="emergency_Contact" autocomplete="rutjfkde" autofocus /></div>
                <div class="col-sm-4" style="margin-top:4px;">Relation</div>
                <div class="col-sm-8" style="margin-top:4px;"><input type="text" class="form-control input-sm"
                        data-role="text" data-name="emergency_Relation" autocomplete="rutjfkde" autofocus /></div>

                <div class="col-sm-4" style="margin-top:4px;">Machine Emp. Id.</div>
                <div class="col-sm-8" style="margin-top:4px;"><input type="text" class="form-control input-sm"
                        data-name="machineid" autocomplete="rutjfkde" autofocus /></div>
            </div>


        </div>

        <div class="col-sm-10" id="section2">

            <input type="hidden" value ="<?php echo $created_by;?>" data-name="employeeid" autocomplete="rutjfkde" autofocus/>

            <input type="hidden" value ="<?php echo $branchid;?>"   data-name="branchid" autocomplete="rutjfkde" autofocus/>

            <div class="col-sm-12" align="center"
                style="border-radius:4px;border:1px solid #CCC;background:#FFF;padding-top:10px;-webkit-box-shadow: 2px 2px 3px 3px #ccc; -moz-box-shadow:2px 2px 3px 3px #ccc;box-shadow:2px 2px 3px 3px #ccc;">

                <h3 style="margin:5px;" id="details">Leave Application</h3>

            </div>

            <div style="clear:both;"></div>

            <hr style="margin:6px;">

            <div class="col-sm-6" style="padding-bottom:10px;border-radius:4px;border:1px solid #CCC;background:#FFF;padding-top:10px;-webkit-box-shadow: 2px 2px 3px 3px #ccc; -moz-box-shadow:2px 2px 3px 3px #ccc;box-shadow:2px 2px 3px 3px #ccc;">

                <p>Dear Sir/Mamad,</p>

                <p>Subject : Leave Application.</p>

                <p>I am <?php echo $employeemaster['username'];?> From <?php echo $branchname;?> Branch. I want a 
                    
                    <select class="input-sm" data-role="select" data-name="leavetype" id="leavetype" style="margin-bottom:5px;">
                            
                        <option value="Select">Select</option>
                        
                        <option value="PL">Paternity</option>
                        
                        <option value="CL">Casual</option>
                        
                        <option value="SL">Sick</option>
                            
                    </select> 

                    leave on date                             
                        
                    <input type="text" data-role='text' data-name='fromdate' class="form-control input-sm datepicker">
                    
                    To

                    <input type="text" data-role='text' data-name='todate' class="form-control input-sm datepicker">
                    .
                </p>

                <div  class="form-group">

                    <textarea rows="5" type="text"  id="inp-reason" class="form-control input-sm" data-role="text" data-name="reason" autocomplete="rutjfkde" autofocus placeholder="If Any Reason" ></textarea>

                </div>

                <p>Yours Sincerely,</p>

                <p> <?php echo $employeemaster['username'];?> </p>


                <div class='col-sm-4'>

                    <button class="btn btn-success btn-sm btn-block" id="btn-leaveApplication" style="margin:5px;">Submit</button>

                </div>

                <div class='col-sm-4'>

                    <button class="btn btn-danger btn-sm btn-block" id="btn-clear" style="margin:5px;">Clear</button>

                </div>
            
            </div>

            <div class="col-sm-6" style="padding-bottom:10px;border-radius:4px;border:1px solid #CCC;background:#FFF;padding-top:10px;-webkit-box-shadow: 2px 2px 3px 3px #ccc; -moz-box-shadow:2px 2px 3px 3px #ccc;box-shadow:2px 2px 3px 3px #ccc;">

                <h5 align="center">Remaining Leave</h5>

                <div class="col-sm-5" style="margin-top:4px;">Paternity leave</div>

                <div class="col-sm-7" style="margin-top:4px;">

                    <input type="number" class="form-control input-sm"  value="<?php echo $checkLeave ['paid_leave'];?>" readonly/>

                </div>

                <div class="col-sm-5" style="margin-top:4px;">Casual leave</div>

                <div class="col-sm-7" style="margin-top:4px;">

                    <input type="number" class="form-control input-sm" value="<?php echo $checkLeave ['casual_leave'];?>" readonly/>

                </div>

                <div class="col-sm-5" style="margin-top:4px;">Sick leave</div>

                <div class="col-sm-7" style="margin-top:4px; margin-bottom: 10px;">

                    <input type="number" class="form-control input-sm" value="<?php echo $checkLeave ['standanrd_leave'];?>" readonly/>

                </div>


                <div class="container-fluid tableFixHead table-responsive">

                    <table class="table table-bordered " id="table-order">

                        <thead>

                            <tr>

                                <th class="text-center">SR. No</th>

                                <th class="text-center">From Date</th>

                                <th class="text-center">To Date</th>

                                <th class="text-center">Leave Type</th>

                                <th class="text-center">Status</th>

                            </tr>

                        </thead>

                        <tbody>

                            <?php
                            
                                $count = 0;
                                
                                $result4 = mysqli_query($con,"SELECT * FROM leaveallotmentmaster WHERE employeeid='$created_by' AND branchid = '$branchid' ORDER BY id DESC" );

                                while($rows3 = mysqli_fetch_assoc($result4)){
                                
                                    $count++;

                                    $fromdate = date('d/m/Y',$rows3['fromdate']/1000);
                                    
                                    $todate   = date('d/m/Y',$rows3['todate']/1000);

                                    $status   = $rows3['status'];

                                    if ($status == '') {

                                        $check = 'Pending';
                                        
                                    } else {

                                        $check = $status;

                                    }
                                    
                                    echo '<tr>';
                                            
                                        echo '<td align="center">'.$count.'</td>';
                                            
                                        echo '<td align="center">'.$fromdate.'</td>';
                                        
                                        echo '<td align="center">'.$todate.'</td>';
                                        
                                        echo '<td align="center">'.$rows3['leavetype'].'</td>';
                                            
                                        echo '<td align="center">'.$check.'</td>';
                                        
                                    echo '<tr>';
                                            
                                }

                            ?>
                        
                        </tbody>

                    </table>

                </div>

            </div>

        </div>

    </div>

</div>


<!-- Reset Password Model Start -->

<div id="resetPassword" class="modal fade" role="dialog">

    <div class="modal-dialog">

        <div class="modal-content">

            <div class="modal-header">

                <button type="button" class="close" data-dismiss="modal">&times;</button>

                <h4 class="modal-title" align="center">Reset Password</h4>

            </div>

            <div class="modal-body" id='modal'>

                <input type="hidden" data-role='text' data-name='userid' id="userId" class="form-control input-sm">

                <input type="hidden" data-role='text' data-name='branchid' id="branchId" class="form-control input-sm">


                <div class="row">

                    <div class="col-sm-4">Old Password</div>

                    <div class="col-sm-8">

                        <div class="form-group">
                            
                            <input type="password" data-role='text' data-name='oldpassword' id="oldPassword" class="form-control input-sm password" readonly>
                        
                        </div>

                    </div>

                    <div class="col-sm-4">New Password</div>

                    <div class="col-sm-8">

                        <div class="form-group">

                            <input type="password" data-role='text' data-name='newpassword' class="form-control input-sm password">
                        
                        </div>

                    </div>

                    <div class="col-sm-4"></div>

                    <div class="col-sm-8">

                        <input type="checkbox" class="custom-control-input" id="customCheck1" onchange="showPass()">

                        <label class="custom-control-label switch" for="customCheck1" id="lblauto">Show Password</label>

                    </div>

                    <div style="clear:both;"></div>
                    <hr>
                    <div class="col-sm-4"></div>

                    <div class="col-sm-4">
                        
                        <button class="btn btn-success btn-sm btn-block" id="btn-password" style="margin:5px;">Submit</button>
                    
                    </div>

                    <div class="col-sm-4">
                        
                        <button class="btn btn-danger btn-sm btn-block" id="btn-clear"
                            style="margin:5px;">Clear</button>
                    
                    </div>

                </div>

            </div>

        </div>

    </div>

</div>

<!-- Reset Password Model End -->


<?php

    include($base.'_in/footer.php');

?>
<script>



$( document ).ready(function() {

    empdetails();

});

function resetPassword(e) {

    $('#branchId').val($(e).attr('data-branchid'));

    $('#userId').val($(e).attr('data-userid'));

    $('#oldPassword').val($(e).attr('data-password'));
    
}

function leaveApplication(e) {

    $('#section1').fadeOut(500);

    $('#section2').fadeIn(500);

    $('#section3').fadeOut(500);

    $('#submit').hide();

}

function personalui() {

    $('#section1').fadeIn(500);

    $('#section2').fadeOut(500);

    $('#section3').fadeOut(500);

    $('#submit').show();

}

function attachment(){

    $('#section1').fadeOut(500);

    $('#section2').fadeOut(500);

    $('#section3').fadeIn(500);   

}


function showPass() {

    if ($('.password').attr('type') == "password") {

        $('.password').attr('type', 'text');

        $(".switch").text("Hide Password");

    } else {

        $('.password').attr('type', 'password');

        $(".switch").text("Show Password");

    }

}


$('#btn-password').on('click',function(){

    let valid = true;

    if(checker('modal') !=false ) {

        valid=valid*true;

    } else {
        
        valid=valid*false;
    }
     
    if(valid){
        
        data = checker('modal');
    
        var datastr = JSON.stringify(data);

        $.ajax({

            type: "POST",

            data: {
                data: datastr
            },

            url: '/_api/user/resetpassword.php',

            cache: false,

            success: function (res) {

                if (res.status == 'success') {

                    swal({
                        type: 'success',
                        title: 'Password Updated',
                        showConfirmButton: false,
                        timer: 3000
                    });

                    setTimeout(function () {
                        window.location.reload();
                    }, 3000);

                }

                if(res.status == 'falid2'){
                    swal('Phone Number already Exist', '', 'error');
                    $('#inp-phone').val('');
                    $('#inp-phone').css('border-bottom', '1px solid red');
                }

            }

        });

    }

})


$('#btn-leaveApplication').on('click',function(){

    let valid = true;

    if(checker('section2') !=false ) {

        valid=valid*true;

    } else {
        
        valid=valid*false;
    }
     
    if(valid){
        
        data = checker('section2');
    
        var datastr = JSON.stringify(data);

        $.ajax({

            type: "POST",

            data: {
                data: datastr
            },

            url: '/_api/user/leaveapplication.php',

            cache: false,

            success: function (res) {

                if (res.status == 'success') {

                    swal({
                        type: 'success',
                        title: 'Password Updated',
                        showConfirmButton: false,
                        timer: 3000
                    });

                    setTimeout(function () {
                        window.location.reload();
                    }, 3000);

                }

                if(res.status == 'falid2'){
                    swal('Phone Number already Exist', '', 'error');
                    $('#inp-phone').val('');
                    $('#inp-phone').css('border-bottom', '1px solid red');
                }

            }

        });

    }

})

$('.datepicker').pickadate({
    selectYears: 100,
    selectMonths: true,
    onClose: function () {
        try {
            var dateStamp = this.get('select')['pick'];
            var id = this.get('id');
            $('#' + id).attr('data-stamp', dateStamp);
            $('#' + id).parent().find('.timepicker').click();
        } catch (err) {
            console.log(err);
        }
    }
});

function empdetails() {

    let empId = $('#empId').val();

    if (empId != '') {

        $.ajax({
				
            type: "POST",
			
            data: {
				empid: empId
			},
			
            url: '/_api/user/userdetails.php',
			
            cache: false,
			
            success: function (res) {
			
                if (res.status == 'success') {
                
                    modals.putvalue('section', res.json);

                }

                if(res.status == 'falid2'){

                    swal('Phone Number already Exist', '', 'error');
                    $('#inp-phone').val('');
                    $('#inp-phone').css('border-bottom', '1px solid red');

                }
				
            }
			
        });

    }

}



$('#btn-reset').on('click',function(){
    window.location.reload();
});

$('#btn-submit').on('click',function(){

    let valid = true;
    
    if(checker('section') !=false ) {

		valid=valid*true;

	} else {
		   
        valid=valid*false;
    }
         
    if(valid){
        
        data = checker('section');
      
        var datastr = JSON.stringify(data);

			$.ajax({
				type: "POST",
				data: {
					data: datastr
				},
				url: '/_api/user/updatedetails.php',
				cache: false,
				success: function (res) {
					if (res.status == 'success') {
						swal({
							type: 'success',
							title: 'Info Updated',
							showConfirmButton: false,
							timer: 3000
						});
						setTimeout(function () {
							window.location.reload();
						}, 3000);
                    }
                    if(res.status == 'falid2'){
                        swal('Phone Number already Exist', '', 'error');
                        $('#inp-phone').val('');
                        $('#inp-phone').css('border-bottom', '1px solid red');
                    }
				}
			});
    }
})

</script>