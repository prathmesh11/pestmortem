
<?php 

    $base    = '../../';

    $navenq3 = 'background:#1B1464;';

    if (session_status() == PHP_SESSION_NONE) { session_start(); }

    $sessionby   = $_SESSION['employeeid'];

    $branchid    = $_SESSION['branchid'];

    $arrysession = [];

    include($base.'_in/connect.php');

    $con = _connect();

    $_pagearr    = json_decode($_SESSION['pagearr']);

    foreach($_pagearr as $i){
        
        if(get_object_vars($i)['mainpage']=='CRM' && get_object_vars($i)['status']==true){
        
            $subpagearr = get_object_vars($i)['subpage'];
        
            foreach($subpagearr as $ii){
                
                if(get_object_vars($ii)['subpage']=='Work Contract' && get_object_vars($ii)['status']==true){$WorkContract='display:show;';}
                
                if(get_object_vars($ii)['subpage']=='Job Allotment' && get_object_vars($ii)['status']==true){$JobAllotment='display:show;';}
                
                if(get_object_vars($ii)['subpage']=='Job Completed' && get_object_vars($ii)['status']==true){$JobCompleted='display:show;';}
                
                if (get_object_vars($ii)['status']==true) {
                    
                    array_push($arrysession,get_object_vars($ii)['subpage']);
        
                }
        
            }

        }
        
    }

    if (!in_array('Job Allotment', $arrysession)) {
    
      echo "<script>window.location.href='/process/dashboard.php';</script>";
    
      exit;

    }

?>

<!DOCTYPE html>

<html lang="en">
    
  <head>

    <meta charset="utf-8">
    
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <title>Pest Mortem (India) Pvt. Ltd.</title>

    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    
    <!-- Font Awesome -->
    
    <link rel="stylesheet" href="/plugins/fontawesome-free/css/all.min.css">
    
    <!-- fullCalendar -->
    
    <link rel="stylesheet" href="/plugins/fullcalendar/main.css">
    
    <!-- Theme style -->
    
    <link rel="stylesheet" href="/dist/css/adminlte.min.css">

    <link rel="stylesheet" href="/css/classic.css">
    
    <link rel="stylesheet" href="/css/classic.date.css">
    
    <link rel="stylesheet" href="/css/classic.time.css">

    <link href="/css/select2.min.css" rel="stylesheet" />

    <link rel="stylesheet" href="/css/alart.css">


    <style>

      .picker__select--month {

        width: 35%;

        height: 40px;

      }

      .picker__select--year {

        width: 22.5%;

        height: 40px;

      }

      hr.style-hr {

        border: 0;
        
        height: 1px;
        
        background-image: linear-gradient(to right, rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.75), rgba(0, 0, 0, 0));

      }

      .th{
        background:#8e44ad;
        color:#fff;
        text-align:center;
        padding-top:2px;
        padding-bottom:2px;
        border:1px solid #fff;
    }
    .td{
        border:1px solid #ddd;
    }
    .table-ui .btn{
        margin:3px;
    }
    #myModal1 .col-sm-4,.col-sm-8{
      margin-bottom:2px;
    }
    .picker__select--year{
        height:auto;
    }
    .picker__select--month{
        height:auto;
    }
    .table-list td,
   .table-list th {
       border: 1px solid #ddd;
       padding: 1px !important;
       font-size: 13px;
   }
   .table-list td{
	padding-top: 10px !important;
   }

   .table-list tr:nth-child(even) {
       background-color: #f2f2f2;
   }

   .table-list th {
       padding-top: 5px;
       padding-bottom: 5px;
       text-align: center;
       background-color: #16a085;
       color: white;
   }


   .select2-container--default .select2-selection--single .select2-selection__rendered {
    line-height: 28px;
}

.select2-container--default .select2-selection--single .select2-selection__arrow {

    top: 6px;
}

.select2-container .select2-selection--single {

    height: 38px;
    }
    </style>

  </head>

  <body class="hold-transition sidebar-mini">

    <input type="hidden" value="<?php echo $_GET['joballotstatus'];?>" id="jobAllotStatus">

    <div class="wrapper">

      <div class="content-wrapper" style="margin-left: 0px;">
          
        <section class="content-header">
            
          <div class="container-fluid">

            <table width="100%" >
                
              <tr>

                <td align="center" style="width:20%"><a href="/process/crm/workcontract.php"
                        style="border:1px solid blue;border-radius:0px;"
                        class="btn btn-primary btn-block">Work Contract</td>
                <td align="center" style="width:20%"><a href="/process/crm/job-allotment.php"
                        style="border:1px solid blue;border-radius:0px;<?php echo $navenq3;?>" class="btn btn-primary btn-block">Job
                        Allotment</td>
                <td align="center" style="width:20%"><a href="/process/crm/job-complete.php"
                        style="border:1px solid blue;border-radius:0px;" class="btn btn-primary btn-block">Job
                        Completed</td>

                <td align="center" style="width:20%"><a href="/process/crm/reschedule.php"
                        style="border:1px solid blue;border-radius:0px;" class="btn btn-primary btn-block">Reschedule Job</td>

                <td align="center" style="width:20%"><a href="/process/crm/close-enquiry.php"
                        style="border:1px solid blue;border-radius:0px;"
                        class="btn btn-primary btn-block">Reporting</td>

              </tr>

            </table>

          </div>
        
        </section>

        <section class="content">
            
          <div class="container-fluid">
              
            <div class="row">
                
              <div class="col-md-4">
                  
                <div class="sticky-top mb-3">
                    
                  <div class="card">
                      
                    <div class="card-header">
                    
                        <h4 class="card-title">Upcoming And Current Job Shedule</h4>
                    
                    </div>

                    <div class="card-body">

                      <div id="external-events">
                            
                        <?php 
                            
                          $leftBrdColorArray = array("bg-success", "bg-warning", "bg-info", "bg-primary","bg-danger","bg-dark","bg-secondary");
                      
                          $arrsize           = count($leftBrdColorArray);
                          
                          $i                 = 0;
                          
                          $todaymill         = strtotime($today)*1000;

                          $threedaysbefore   = $CURRENT_MILLIS+172800000;

                          $result            = mysqli_query($con,"SELECT * FROM workcontractservice WHERE servicedate<>0 AND (servicedate<'$threedaysbefore' OR servicedate = '$todaymill' ) AND branchid='$branchid' AND shedule_time=0 ORDER BY servicedate ASC");

                          while($rows = mysqli_fetch_assoc($result)){

                            $contractid   = $rows['contractid'];

                            $category     = mysqli_fetch_assoc(mysqli_query($con,"SELECT category x FROM crmmaster WHERE contractid='$contractid'"))['x'];

                            $customername = mysqli_fetch_assoc(mysqli_query($con,"SELECT customername x FROM customermaster INNER JOIN crmmaster ON customermaster.customerid = crmmaster.customerid and crmmaster.contractid = '$contractid'"))['x'];

                            $customerid = mysqli_fetch_assoc(mysqli_query($con,"SELECT customerid x FROM crmmaster WHERE contractid='$contractid'"))['x'];
                            
                            $cifo         = mysqli_fetch_assoc(mysqli_query($con,"SELECT * FROM customermaster WHERE customerid='$customerid'"));

                            if($i > $arrsize - 1) $i = 0;
                            
                            echo '<div class="external-event '.$leftBrdColorArray[$i].' text-center"  data-toggle="popover" data-content="Address : '.get_object_vars(json_decode($cifo['customerjson']))['address'].'"> <a class="btn btn-sm" href="/process/crm/job-allotment1.php?serviceid='.$rows['serviceid'].'&servicedatetimestap='.$rows['servicedate'].'&servicedate='.date("d F, Y",$rows['servicedate']/1000).'&category='.$category.'&contractid='.$rows['contractid'].'&joballotstatus='.$rows['joballotstatus'].'&serviceName='.$rows['servicename'].'">'.$customername.'</a></div>';

                           // echo '<div class="external-event '.$leftBrdColorArray[$i].' text-center"> <a class="btn btn-sm" data-serviceid='.$rows['serviceid'].' data-servicedatetimestap='.$rows['servicedate'].' data-servicedate="'.date('d F, Y',$rows['servicedate']/1000).'" data-category='.$category.' data-contractid='.$rows['contractid'].' data-joballotstatus='.$rows['joballotstatus'].' data-toggle="modal" data-target="#materialIssue" onclick ="openAndFillData(this)">'.$customername.'</a></div>';


                            $i++;

                          }

                        ?>
                          
                        <div class="checkbox" style="display:none;">
                            
                          <label for="drop-remove">
                          
                            <input type="checkbox" id="drop-remove"> remove after drop
                          
                          </label>
                        
                        </div>
                      
                      </div>
                    
                    </div>
                  
                  </div>
        
                  <div class="card" style="display:none;">
                      
                    <div class="card-header">
                        
                      <h3 class="card-title">Job Shedule & Material Issue</h3>
                    
                    </div>

                    <div class="card-body">
                        
                      <div class="btn-group" style="width: 100%; margin-bottom: 10px;">
                      
                        <ul class="fc-color-picker" id="color-chooser">
                            
                          <li><a class="text-primary" href="#"><i class="fas fa-square"></i></a></li>
                          
                          <li><a class="text-warning" href="#"><i class="fas fa-square"></i></a></li>
                          
                          <li><a class="text-success" href="#"><i class="fas fa-square"></i></a></li>
                          
                          <li><a class="text-danger" href="#"><i class="fas fa-square"></i></a></li>
                          
                          <li><a class="text-muted" href="#"><i class="fas fa-square"></i></a></li>
                        
                        </ul>
                      
                      </div>
                        
                      <div class="input-group">
                          
                        <input id="new-event" type="text" class="form-control" placeholder="Event Title">

                        <div class="input-group-append">
                            
                          <button id="add-new-event" type="button" class="btn btn-primary">Add</button>
                        
                        </div>
                      
                      </div>  
                    
                    </div>
                  
                  </div>
                
                </div>
              
              </div>
        
              <div class="col-md-8">
                  
                <div class="card card-primary">
                    
                  <div class="card-body p-0">

                    <div id="calendar"></div>
                  
                  </div>
        
                </div>
          
              </div>
        
            </div>
          
          </div><!-- /.container-fluid -->
            
        </section>

      </div>

      <aside class="control-sidebar control-sidebar-dark">
          <!-- Control sidebar content goes here -->
      </aside>

    </div>

    
    <div class="container">

      <div class="modal fade" id="materialIssue">
        
        <div class="modal-dialog modal-xl">
        
          <div class="modal-content">
          
            <div class="modal-header ">
              
              <h4 class="modal-title">Job Shedule & Material Issue</h4>
              
              <button type="button" class="close" data-dismiss="modal">&times;</button>
            
            </div>
            
            <div class="modal-body">

              <div class="card bg-primary">
                
                <div class="card-body">
                  
                  <h4 class="text-center">Job Description</h4>
                  
                  <hr class="style-hr"> 

                  <div class="row">

                    <div class="col-sm-3">
                          
                      <p class="card-text">Contract No. : <?php echo $_GET['contractid']?></p>

                    </div>

                    <div class="col-sm-3">
                      
                      <p class="card-text">Service Date : <?php echo $_GET['servicedate']?></p>

                    </div>

                    <div class="col-sm-3">
                      
                      <p class="card-text">Servcie Name : <?php echo $_GET['serviceName']?></p>

                    </div>

                    <div class="col-sm-3">
                      
                      <p class="card-text">Category : <?php echo $_GET['category']?></p>

                    </div>
          
                  </div>    

                </div>

              </div>
                
              <div class="row" id="section">

                <input type="hidden" value="<?php echo $branchid;?>" id="branchid" data-name="branchid">

                <input type="hidden" value="<?php echo $_GET['contractid'];?>" id="contractid" data-name="contractid">

                <input type="hidden" value="<?php echo $_GET['serviceid'];?>" id="serviceid" data-name="serviceid">

                <div class = "col-sm-3">

                  <div class="form-group">

                    <label for="">Address</label>

                    <select id="locationAddress" class="form-control input-sm select-js" onchange="locationAddress()">

                      <option value="Select">Select Address</option>

                      <?php 

                        $contractid          = $_GET['contractid'];

                        $workcontractDetails = mysqli_fetch_assoc(mysqli_query($con,"SELECT servicelocation,customerid FROM workcontract WHERE contractid = '$contractid' AND branchid = '$branchid' "));

                        $customerid          = $workcontractDetails['customerid'];

                        $servicelocation     = json_decode($workcontractDetails['servicelocation']);


                        foreach ($servicelocation as $i) {

                          $servicelocations = get_object_vars($i)['servicelocation'];

                          if ($servicelocations == 'Same as Customer Address') {

                            $customeraddress = mysqli_fetch_assoc(mysqli_query($con,"SELECT customeraddress x FROM customermultiaddress WHERE customerid = '$customerid' "))['x'];

                          } else {

                            $customeraddress = $servicelocations;

                          }

                          echo '<option value="'.$customeraddress.'">'.$customeraddress.'</option>';

                          
                        }

                      ?>

                    </select>
                  
                  </div>

                </div>

                <div class = "col-sm-3">

                  <div class="form-group">

                    <label for="">Service Address</label>
                
                    <textarea type="text" data-role="text" class="form-control input-sm" id="serviceAddress" data-name="serviceAddress"></textarea>
                  
                  </div>
                
                </div>

                <div class = "col-sm-3">

                  <div class="form-group">

                    <label for="">Latitude</label>
                
                    <input type="text" data-role="text" class="form-control input-sm" id="latitude" data-name="latitude" />
                  
                  </div> 

                </div>

                <div class = "col-sm-3">
                  
                  <div class="form-group">

                    <label for="">Longitude</label>
                
                    <input type="text" data-role="text" class="form-control input-sm" id="longitude" data-name="longitude" />
                  
                  </div> 

                </div>

                <div class="col-sm-6">

                  <div class="form-group">

                    <label for="">Start Date</label>
                
                    <input type="text" data-role="text" class="datepicker form-control input-sm" id="startdate" data-name="startdate" />
                  
                  </div>    

                </div>

                <div class="col-sm-6" id="dummymodalid">

                  <div class="form-group">

                    <label for="">End Date</label>

                    <input type="text" data-role="text" class="datepicker form-control input-sm" id="enddate" data-name="enddate" />

                  </div>    
                    
                </div>

              </div> 

              <div style="clear:both;"></div>
        
              <hr class="style-hr">
          
              <div class = "row">
          
                <div class="col-sm-12">
        
                  <div class="table-responsive" id="serviceTable">
      
                    <table class="table table-list form-group" id="table-order">
    
                      <thead>
  
                        <th width="2%">#</th>

                        <th width="10%">Employee Name</th>

                        <th width="10%">Item Name</th>
                        
                        <th width="5%">Item Qty</th>

                        <th width="5%">Allotment Qty</th>
                                                
                        <th width="5%">Action</th>
  
                      </thead>
  
                      <tbody>
  
                        <tr>
                            
                          <td></td>

                          <td>

                            <div class="form-group">

                              <select data-role="select" data-name='employeeid' id="employeeId" class="form-control input-sm select-js" onchange="itemname()">
            
                                <option value="Select">Select</option>
                                
                                <?php 
                                
                                  $result1 = mysqli_query($con,"SELECT employeeid,username FROM employeemaster WHERE branchid='$branchid' AND role = 'employee' OR role = 'None' ");

                                  while($rows = mysqli_fetch_assoc($result1)){
                          
                                    echo '<option value="'.$rows['employeeid'].'">'.$rows['username'].'</option>';

                                  }

                                ?>

                              </select>

                            </div>
                          
                          </td>

                          <td>

                            <div class="form-group">

                              <select data-role="select" data-name='itemid' id="itemId" class="form-control input-sm select-js" onchange="empstock()">
            
                                <option value="Select">Select Item Name</option>
                                  
                              </select>

                            </div>
                          
                          </td>

                          <td>

                            <div class="form-group">

                              <input type="number" id="itemQty" data-name="itemqty" data-role="number" class="form-control input-sm" readonly>

                            </div>
                          
                          </td>

                          <td>

                            <div class="form-group">

                              <input type="number" id="outQty" data-name="outqty" data-role="number" class="form-control input-sm" onkeyup="checkqty()">

                            </div>
                          
                          </td>

                          <td class="text-center">

                            <button class="btn btn-success btn-sm " id="btn-add" data-edit="" onclick="add(this)">Add</button>

                          </td>

                        </tr>
                              
                      </tbody>
                        
                    </table>   
      
                  </div>    
                
                </div>
          
              </div>

              <div style="clear:both;"></div>
                      
              <hr class="style-hr">

              <div class="row">
              
                <div class="col-sm-4"> </div>
                        
                <div class="col-sm-4">
                        
                  <button class="btn btn-success btn-sm btn-block" id="btn-submit" onclick="submit()">Submit</button>
                    
                </div>
                        
                <div class="col-sm-4"></div>
                  
              </div>

            </div>           
            
          </div>

        </div>

      </div>
      
    </div>


    <div class="container">

      <div class="modal fade" id="materialIssue1">
        
        <div class="modal-dialog modal-xl">
        
          <div class="modal-content">
          
            <div class="modal-header ">
              
              <h4 class="modal-title">Job Shedule & Material Issue</h4>
              
              <button type="button" class="close" data-dismiss="modal">&times;</button>
            
            </div>
            
            <div class="modal-body">

              <div class="card bg-primary">
                
                <div class="card-body">
                  
                  <h4 class="text-center">Job Description</h4>
                  
                  <hr class="style-hr"> 

                  <div class="row">

                    <div class="col-sm-3">
                          
                      <p class="card-text">Contract No. : <?php echo $_GET['contractid']?></p>

                    </div>

                    <div class="col-sm-3">
                      
                      <p class="card-text">Service Date : <?php echo $_GET['servicedate']?></p>

                    </div>

                    <div class="col-sm-3">
                      
                      <p class="card-text">Servcie Name : <?php echo $_GET['serviceName']?></p>

                    </div>

                    <div class="col-sm-3">
                      
                      <p class="card-text">Category : <?php echo $_GET['category']?></p>

                    </div>
          
                  </div>    

                </div>

              </div>
                
              <div class="row" id="section1">

                <input type="hidden" value="<?php echo $branchid;?>" id="branchid" data-name="branchid">

                <input type="hidden" value="<?php echo $_GET['contractid'];?>" id="contractid" data-name="contractid">

                <input type="hidden" value="<?php echo $_GET['serviceid'];?>" id="serviceid" data-name="serviceid">

                <div class = "col-sm-3">

                  <div class="form-group">

                    <label for="">Address</label>

                    <select id="locationAddresss" class="form-control input-sm select-js" onchange="locationAddresss()">

                      <option value="Select">Select Address</option>

                      <?php 

                        $contractid          = $_GET['contractid'];

                        $workcontractDetails = mysqli_fetch_assoc(mysqli_query($con,"SELECT servicelocation,customerid FROM workcontract WHERE contractid = '$contractid' AND branchid = '$branchid' "));

                        $customerid          = $workcontractDetails['customerid'];

                        $servicelocation     = json_decode($workcontractDetails['servicelocation']);

                        foreach ($servicelocation as $i) {

                          $servicelocations = get_object_vars($i)['servicelocation'];

                          if ($servicelocations == 'Same as Customer Address') {

                            $customeraddress = mysqli_fetch_assoc(mysqli_query($con,"SELECT customeraddress x FROM customermultiaddress WHERE customerid = '$customerid' "))['x'];

                          } else {

                            $customeraddress = $servicelocations;

                          }

                          echo '<option value="'.$customeraddress.'">'.$customeraddress.'</option>';

                          
                        }

                      ?>

                    </select>
                  
                  </div>

                </div>

                <div class = "col-sm-3">

                  <div class="form-group">

                    <label for="">Service Address</label>
                
                    <textarea type="text" data-role="text" class="form-control input-sm" id="serviceAddress1" data-name="serviceAddress"></textarea>
                  
                  </div>
                
                </div>

                <div class = "col-sm-3">

                  <div class="form-group">

                    <label for="">Latitude</label>
                
                    <input type="text" data-role="text" class="form-control input-sm" id="latitude" data-name="latitude" />
                  
                  </div> 

                </div>

                <div class = "col-sm-3">
                  
                  <div class="form-group">

                    <label for="">Longitude</label>
                
                    <input type="text" data-role="text" class="form-control input-sm" id="longitude" data-name="longitude" />
                  
                  </div> 

                </div>

                <div class="col-sm-6">

                  <div class="form-group">

                    <label for="">Start Date</label>
                
                    <input type="text" data-role="text" class="datepicker form-control input-sm" id="startdate1" data-name="startdate" />
                  
                  </div>    

                </div>

                <div class="col-sm-6" id="dummymodalid">

                  <div class="form-group">

                    <label for="">End Date</label>

                    <input type="text" data-role="text" class="datepicker form-control input-sm" id="enddate1" data-name="enddate" />

                  </div>    
                    
                </div>

              </div> 

              <div style="clear:both;"></div>
        
              <hr class="style-hr">
          
              <div class = "row">
          
                <div class="col-sm-12">
        
                  <div class="table-responsive" id="serviceTable">
      
                    <table class="table table-list form-group" id="table-orderEdit">
    
                      <thead>
  
                        <th width="2%">#</th>

                        <th width="10%">Employee Name</th>

                        <th width="10%">Item Name</th>
                        
                        <th width="5%">Item Qty</th>

                        <th width="5%">Allotment Qty</th>
                                                
                        <th width="5%">Action</th>
  
                      </thead>
  
                      <tbody>
  
                        <tr>
                            
                          <td></td>

                          <td>

                            <div class="form-group">

                              <select data-role="select" data-name='employeeid' id="employeeIdEdit" class="form-control input-sm select-js" onchange="itemname()">
            
                                <option value="Select">Select</option>
                                
                                <?php 
                                
                                  $result1 = mysqli_query($con,"SELECT employeeid,username FROM employeemaster WHERE branchid='$branchid' AND role = 'employee' OR role = 'None' ");

                                  while($rows = mysqli_fetch_assoc($result1)){
                          
                                    echo '<option value="'.$rows['employeeid'].'">'.$rows['username'].'</option>';

                                  }

                                ?>

                              </select>

                            </div>
                          
                          </td>

                          <td>

                            <div class="form-group">

                              <select data-role="select" data-name='itemid' id="itemIdEdit" class="form-control input-sm select-js" onchange="empstock()">
            
                                <option value="Select">Select Item Name</option>
                                  
                              </select>

                            </div>
                          
                          </td>

                          <td>

                            <div class="form-group">

                              <input type="number" id="itemQtyEdit" data-name="itemqty" data-role="number" class="form-control input-sm" readonly>

                            </div>
                          
                          </td>

                          <td>

                            <div class="form-group">

                              <input type="number" id="outQtyEdit" data-name="outqty" data-role="number" class="form-control input-sm" onkeyup="checkqty()">

                            </div>
                          
                          </td>

                          <td class="text-center">

                            <button class="btn btn-success btn-sm " id="btn-addEdit" data-addEdit="" onclick="addEdit(this)">Add</button>

                          </td>

                        </tr>
                              
                      </tbody>
                        
                    </table>   
      
                  </div>    
                
                </div>
          
              </div>

              <div style="clear:both;"></div>
                      
              <hr class="style-hr">

              <div class="row">
              
                <div class="col-sm-3"> </div>

                <?php if ($_GET['allotTime'] == 0) { ?>
                        
                  <div class="col-sm-3">
                          
                    <button class="btn btn-success btn-sm btn-block" id="btn-update" onclick="update()">Update</button>
                      
                  </div>

                  <div class="col-sm-3">
                          
                    <button class="btn btn-sm btn-block btn-success" onclick="joballowted()">Confirm</button>
                      
                  </div>

                <?php } ?>
                        
                <div class="col-sm-3"></div>
                  
              </div>

            </div>           
            
          </div>

        </div>

      </div>
      
    </div>
    <!-- ./wrapper -->

    <!-- jQuery -->
    <script src="/plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- jQuery UI -->
    <script src="/plugins/jquery-ui/jquery-ui.min.js"></script>
    <!-- AdminLTE App -->
    <script src="/dist/js/adminlte.min.js"></script>
    <!-- fullCalendar 2.2.5 -->
    <script src="/plugins/moment/moment.min.js"></script>
    <script src="/plugins/fullcalendar/main.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="/dist/js/demo.js"></script>
    <!-- Page specific script -->

    <script src="/js/picker.js"></script>

    <script src="/js/picker.date.js"></script>

    <script src="/js/picker.time.js"></script>

    <script src="/js/fwork.js"></script>
    
    <script src="/js/select2.min.js"></script>

    <script src="/js/alart.js"></script>';

    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script> -->

    <script>

      $(function () {

        /* initialize the external events
        -----------------------------------------------------------------*/
        function ini_events(ele) {
          
          ele.each(function () {

            // create an Event Object (https://fullcalendar.io/docs/event-object)
            // it doesn't need to have a start or end
            var eventObject = {
          
              title: $.trim($(this).text()) // use the element's text as the event title
          
            }

            // store the Event Object in the DOM element so we can get to it later
            $(this).data('eventObject', eventObject)

            // make the event draggable using jQuery UI
            $(this).draggable({
            
              zIndex        : 1070,
            
              revert        : true, // will cause the event to go back to its
            
              revertDuration: 0  //  original position after the drag
            
            })

          })

        }

        ini_events($('#external-events div.external-event'))

        /* initialize the calendar
        -----------------------------------------------------------------*/
        //Date for the calendar events (dummy data)
        var date = new Date()
        
        var d    = date.getDate(),
        
            m    = date.getMonth(),
        
            y    = date.getFullYear()

        var Calendar    = FullCalendar.Calendar;
        
        var Draggable   = FullCalendar.Draggable;

        var containerEl = document.getElementById('external-events');
        
        var checkbox    = document.getElementById('drop-remove');
        
        var calendarEl  = document.getElementById('calendar');

        // initialize the external events
        // -----------------------------------------------------------------

        new Draggable(containerEl, {
          
          itemSelector: '.external-event',
          
          eventData: function(eventEl) {
          
            return {
          
              title           : eventEl.innerText,
          
              backgroundColor : window.getComputedStyle( eventEl ,null).getPropertyValue('background-color'),
          
              borderColor     : window.getComputedStyle( eventEl ,null).getPropertyValue('background-color'),
          
              textColor       : window.getComputedStyle( eventEl ,null).getPropertyValue('color'),
          
            };
          
          }
        
        });

        $.ajax({

          type: "POST",
          
          data: 'branchid='+ <?php echo $branchid;?>,
          
          url: 'api/joballowtedCalender.php',
          
          cache: false,
          
          success: function (res) {

            var json = res.json;

            var data5 = [];

            for (let i in json) {
                      
              data5.push({
                      
                title           : json[i].title,
                
                start           : new Date(json[i].startdate),
                
                end             : new Date(json[i].enddate),
                
                url             : json[i].url,
                
                backgroundColor : json[i].backgroundColor,
                
                borderColor     : json[i].borderColor,
                
                allDay          : json[i].allDay,
                  
              });
      
            }

            var calendar = new Calendar(calendarEl, {
            
              headerToolbar: {
              
                left  : 'prev,next today',
              
                center: 'title',
              
                right : 'dayGridMonth,timeGridWeek,timeGridDay'
              
              },
              
              themeSystem : 'bootstrap',
              //Random default events
              events      : data5,
        
              editable    : true,
              
              droppable   : true, // this allows things to be dropped onto the calendar !!!
              
              drop        : function(info) {
                // is the "remove after drop" checkbox checked?
                if (checkbox.checked) {
                  // if so, remove the element from the "Draggable Events" list
                  info.draggedEl.parentNode.removeChild(info.draggedEl);
                
                }
            
              },
            
            });

            calendar.render();
            // $('#calendar').fullCalendar()
          }

        });

        /* ADDING EVENTS */
        var currColor = '#3c8dbc' //Red by default
        // Color chooser button
        
        $('#color-chooser > li > a').click(function (e) {
        
          e.preventDefault()
          // Save color
          currColor = $(this).css('color')
          // Add color effect to button
        
          $('#add-new-event').css({
        
            'background-color': currColor,
        
            'border-color'    : currColor
        
          })
        
        })
        
        $('#add-new-event').click(function (e) {
        
          e.preventDefault()
          
          var val = $('#new-event').val()
          
          if (val.length == 0) {
          
            return
          
          }

          // Create events
          var event = $('<div />')
          
          event.css({
          
            'background-color': currColor,
          
            'border-color'    : currColor,
          
            'color'           : '#fff'
          
          }).addClass('external-event')
          
          event.text(val)
          
          $('#external-events').prepend(event)

          // Add draggable funtionality
          ini_events(event)

          // Remove event from text input
          $('#new-event').val('')

        })
        
      })


      //--------------------:) Function Open And Fill Data Start :) -------------------//

      // function openAndFillData(e) {

      //   process/crm/material-issue.php?serviceid=<?php echo $serviceid; ?>&servicedatetimestap=<?php echo $servicedate; ?>&servicedate=<?php echo date("d F, Y",$servicedate/1000);?>&category=<?php echo $category;?>&contractid=<?php echo $contractid;?>&joballotstatus=<?php echo $joballotstatus;?>"

      //   $("#table-order tbody tr.dynamic-row").remove(); 

      //   if ($(e).attr('data-joballotstatus') == 0) {

      //     $('#startdate').val($(e).attr('data-servicedate'));

      //     $('#startdate').attr('data-stamp',$(e).attr('data-servicedatetimestap'));

      //     $('#enddate').val($(e).attr('data-servicedate'));

      //     $('#enddate').attr('data-stamp',$(e).attr('data-servicedatetimestap'));

          
      //     $('#startdate').val('<?php echo $_GET['servicedate'];?>');

      //     $('#startdate').attr('data-stamp','<?php echo $_GET['servicedatetimestap'];?>');

      //     $('#enddate').val('<?php echo $_GET['servicedate'];?>');

      //     $('#enddate').attr('data-stamp','<?php echo $_GET['servicedatetimestap'];?>');


      //   }

      // } 

      $(document).ready(function() {

        if ($('#jobAllotStatus').val() == '0') {

          $('#startdate').val('<?php echo $_GET['servicedate'];?>');

          $('#startdate').attr('data-stamp','<?php echo $_GET['servicedatetimestap'];?>');

          $('#enddate').val('<?php echo $_GET['servicedate'];?>');

          $('#enddate').attr('data-stamp','<?php echo $_GET['servicedatetimestap'];?>');

          $('#materialIssue').modal();


        } else if ($('#jobAllotStatus').val() == 1){

          edit();

          $('#materialIssue1').modal();

        }

      });

      //--------------------------:) Data Picker Start :)----------------------//

      $('.datepicker').pickadate({

        selectYears: true,

        selectMonths: true,

        onClose: function () {

          try {

            var dateStamp = this.get('select')['pick'];
            
            var id        = this.get('id');
            
            $('#' + id).attr('data-stamp', dateStamp);
            
            $('#' + id).parent().find('.timepicker').click();
            
          } catch (err) {

            console.log(err);

          }

        }

      });

      //--------------------------:( Data Picker end :(----------------------//

      //--------------------------:) Function Itemname Start :)----------------------//

      function itemname() {

        let branchid   = $('#branchid').val();

        let employeeId = '';

        if ($('#jobAllotStatus').val() == 1) {

          employeeId = $('#employeeIdEdit').val();
          
        } else {

          employeeId = $('#employeeId').val();

        }

        if (employeeId != 'Select') {
            
          $.ajax({

            type: "POST",

            data: "category=" + '<?php echo $_GET['category']?>' + "&branchid=" + branchid + "&employeeid=" + employeeId,

            url: 'api/itemname.php',

            cache: false,

            success: function (res) {

              if (res.status == 'success') {

                let json = res.json;

                str = '<option value= "Select">Select Item Name</option>';

                for(let i in json){

                  str+='<option value = '+json[i].itemcode+'>'+json[i].itemname+'</option>'

                }


                if ($('#jobAllotStatus').val() == 1) {

                  $('#itemIdEdit').html(str);

                } else {

                  $('#itemId').html(str);

                }

              } else {

                if ($('#jobAllotStatus').val() == 1) {

                  $('#itemIdEdit').html('<option value= "Select">Select Item Name</option>');

                } else {

                  $('#itemId').html('<option value= "Select">Select Item Name</option>');

                }

              }

            }

          })

        } else {

          if ($('#jobAllotStatus').val() == 1) {

            $('#itemIdEdit').html('<option value= "Select">Select Item Name</option>');

          } else {

            $('#itemId').html('<option value= "Select">Select Item Name</option>');

          }

        }

        empstock();

      }

      //--------------------------:( Function Itemname End :(----------------------//


      //-------------------------:) Function empstock Start :)----------------------//

      function empstock() {

          let employeeId = '';

          let itemId     = '';

         if ($('#jobAllotStatus').val() == 1) {

          employeeId = $('#employeeIdEdit').val();

          itemId     = $('#itemIdEdit').val();
          
        } else {

          employeeId = $('#employeeId').val();

          itemId     = $('#itemId').val();

        }

        let branchid   = $('#branchid').val();

        if (itemId != 'Select' && employeeId != 'Select') {

          $.ajax({

            type: "POST",

            data: "itemId=" + itemId + "&branchid=" + branchid + "&employeeId=" + employeeId,

            url: 'api/employee-stock-qty.php',

            cache: false,

            success: function (res) {

              if (res.status == 'success') {

                if ($('#jobAllotStatus').val() == 1) {

                  $('#itemQtyEdit').val(res.total);

                } else {

                  $('#itemQty').val(res.total);

                }

              }  else {

                if ($('#jobAllotStatus').val() == 1) {

                  $('#itemQtyEdit').val('');

                } else {

                  $('#itemQty').val('');

                }

              }

            }

          });      
            
        } else {

          if ($('#jobAllotStatus').val() == 1) {

            $('#itemQtyEdit').val('');

          } else {

            $('#itemQty').val('');

          }

        }
        
      }

      //-------------------------:( Function empstock End :(----------------------//

      //-------------------------:) Function checkqty Start :)----------------------//

      function checkqty() {

        let itemQty = 0;

        let outQty  = 0;

        if ($('#jobAllotStatus').val() == 1) {

          itemQty = parseFloat($('#itemQtyEdit').val());

          outQty  = parseFloat($('#outQtyEdit').val());

        } else {

          itemQty = parseFloat($('#itemQty').val());

          outQty  = parseFloat($('#outQty').val());

        }
  

        if (isNaN(itemQty)) {
            
          itemQty = 0

        }

        if (itemQty < outQty) {
            
          alert ('Allotment Qty Greater Than Item Qty');


          if ($('#jobAllotStatus').val() == 1) {

            $('#outQtyEdit').val('');

          } else {

            $('#outQty').val('');

          }

        }
        
      }

      //-------------------------:( Function checkqty End :(----------------------//

      //-------------------------- :) Add Multiple Services start :)-------------------------------------//

      function add(e) {

        let chk          = $(e).attr('data-edit');

        let data         = checker('table-order');

        let employeeid   = data.employeeid;

        let employeename = $('#employeeId').find(":selected").text();

        let itemid       = data.itemid;

        let itemname     = $('#itemId').find(":selected").text();

        let outqty       = data.outqty;
            
        let valid        = true;

        if (checker('table-order') != false) {

          valid = valid * true;

        } else {

          valid = valid * false;

        }

        $('#table-order > tbody > tr').each(function () {

          let employeeid1 = $(this).find('.employeeid').text().trim();

          let itemid1     = $(this).find('.itemid').text().trim();

          if (employeeid1 == employeeid && chk == '' && itemid == itemid1) {

            valid = valid * false;

            alert('Dublicate Item Name For Same Employee');

          }

        });

        if (valid) {

          let chk = $(e).attr('data-edit');

          if (chk == '') {

            let len = $('#table-order .srNo').length + 1;

            fun_adder(len, employeeid, employeename, itemid, itemname, outqty);

          }

          modals.clear('table-order');

          $('#btn-add').attr('data-edit', '');

        }

      }

      function fun_adder(len, employeeid, employeename, itemid, itemname, outqty) {

        let str  = '<tr id = '+len+' class="dynamic-row">';
            
          str += '<td align="center" class="srNo">' + len + '</td>';
          
          str += '<td align="center" class="employeeid" style="display:none;">' + employeeid + '</td>';
          
          str += '<td align="center" class="employeename">' + employeename + '</td>';

          str += '<td align="center" class="itemid" style="display:none;">' + itemid + '</td>';
          
          str += '<td align="center" class="itemname" colSpan="2">' + itemname + '</td>';
          
          str += '<td align="center" class="outqty">' + outqty + '</td>';
                  
          str += '<td align="center"><button class="btn btn-sm btn-danger remover" data-itemid = '+itemid+' data-employeeid = '+employeeid+' data-outqty = '+outqty+' onclick="removers(this)">R</buuton></td>';
        
        str += '</tr>';
        
        $('#table-order > tbody').append(str);

      }

      //-----------------------:( Add Multiple Services End :(-------------------------//



      //-------------------------- :) Add Multiple Services Edit start :)-------------------------------------//

      function addEdit(e) {

        let chk          = $(e).attr('data-addEdit');

        let data         = checker('table-orderEdit');

        let employeeid   = data.employeeid;

        let employeename = $('#employeeIdEdit').find(":selected").text();

        let itemid       = data.itemid;

        let itemname     = $('#itemIdEdit').find(":selected").text();

        let outqty       = data.outqty;
            
        let valid        = true;

        if (checker('table-orderEdit') != false) {

          valid = valid * true;

        } else {

          valid = valid * false;

        }

        $('#table-orderEdit > tbody > tr').each(function () {

          let employeeid1 = $(this).find('.employeeid').text().trim();

          let itemid1     = $(this).find('.itemid').text().trim();

          if (employeeid1 == employeeid && chk == '' && itemid == itemid1) {

            valid = valid * false;

            alert('Dublicate Item Name For Same Employee');

          }

        });

        if (valid) {

          let chk = $(e).attr('data-addEdit');

          if (chk == '') {

            let len = $('#table-orderEdit .srNo').length + 1;

            fun_adder_edit(len, employeeid, employeename, itemid, itemname, outqty);

          }

          modals.clear('table-orderEdit');

          $('#btn-addEdit').attr('data-addEdit', '');

        }

      }

      function fun_adder_edit(len, employeeid, employeename, itemid, itemname, outqty) {

        let str  = '<tr id = '+len+' class="dynamic-row">';
            
          str += '<td align="center" class="srNo">' + len + '</td>';
          
          str += '<td align="center" class="employeeid" style="display:none;">' + employeeid + '</td>';
          
          str += '<td align="center" class="employeename">' + employeename + '</td>';

          str += '<td align="center" class="itemid" style="display:none;">' + itemid + '</td>';
          
          str += '<td align="center" class="itemname" colSpan="2">' + itemname + '</td>';
          
          str += '<td align="center" class="outqty">' + outqty + '</td>';

          <?php if ($_GET['allotTime'] == 0) { ?>
      
            str += '<td align="center"><button class="btn btn-sm btn-danger remover" data-itemid = '+itemid+' data-employeeid = '+employeeid+' data-outqty = '+outqty+' onclick="removers(this)">R</buuton></td>';

          <?php } ?>

        
        str += '</tr>';
        
        $('#table-orderEdit > tbody').append(str);

      }

      //-----------------------:( Add Multiple Services Edit End :(-------------------------//

      //----------------------:(Remove Function Start:(-------------------------------//

      function removers(e) {

        let itemid     = $(e).attr('data-itemid');

        let employeeid = $(e).attr('data-employeeid');

        let outqty     = $(e).attr('data-outqty');

        let branchid   = $('#branchid').val();
        
        let contractid = $('#contractid').val();

        let serviceid  = $('#serviceid').val();

        $.ajax({
                
          type: "POST",
          
          data: "itemid=" + itemid + "&employeeid=" +employeeid + "&outqty=" + outqty + "&branchid=" + branchid + "&contractid=" + contractid + "&serviceid=" + serviceid,

          url: 'api/job-allotment-remove.php',
              
          cache: false,
              
          success: function (res) {
          
            if (res.status == 'success') {
        
              $(e).parent().parent().remove();
            
            } else {

              $(e).parent().parent().remove();

            }

          }
        
        });
          
      }

      //--------------------:(Remove Function End:(-----------------------//

      //-------------------------:)Insert Function Start :)---------------------------//

      function submit() {

        let valid = true;

        if (checker('section') != false) {

          valid = valid * true;

        } else {

          valid = valid * false;

        }

        let arr = [];

        $('#table-order > tbody > tr').each(function () {

          let srNo          = $(this).find('.srNo').text().trim();

          let employeeid    = $(this).find('.employeeid').text().trim();

          let employeename  = $(this).find('.employeename').text().trim();

          let itemid        = $(this).find('.itemid').text().trim();
          
          let itemname      = $(this).find('.itemname').text().trim();
                  
          let outqty        = $(this).find('.outqty').text().trim();
      
          arr.push({

            "srno": srNo,

            "employeeid": employeeid,

            "employeename": employeename,

            "itemid": itemid,

            "itemname": itemname,

            "outqty": outqty,

          });

        })

        arr.shift();

        if (arr == '') {

          $('#table-order').css('border', '2px solid red');

          valid = valid * false;

        } else {

          $('#table-order').css('border', '2px solid green');

          valid = valid * true;

        }

        if (valid) {

          let data1          = checker('section');
            
              data1          = JSON.stringify(data1);

              itemDetails = JSON.stringify(arr);

          $.ajax({
              
            type: "POST",
            
            data: {

              data: data1,

              itemDetails: itemDetails,

            },

            url: 'api/job-allotment.php',
            
            cache: false,
              
            success: function (res) {
            
              if (res.status == 'success') {
          
                swal({
                    
                  type: 'success',
                
                  title: 'Job Shedule & Material Issue Add',
                
                  showConfirmButton: false,
                
                  timer: 3000
                
                });
              
                setTimeout(function () {
                    
                  location.href = 'job-allotment1.php';
                
                }, 3000);
              
              }

            }
          
          });
        
        }

      }

      //--------------------:(Insert Function End:(------------------------//

      //--------------------:)Edit Function Start:)------------------------//

      function edit() {

        $.ajax({

          type: "POST",

          url: 'api/job-allotment-select.php',

          data: "branchid=" + $('#branchid').val() + "&contractid=" + $('#contractid').val() + '&serviceid='+$('#serviceid').val(),

          cache: false,

          success: function (res) {

            if (res.status == 'success') {

              modals.putvalue('section1', res.json);

              let itemJson = res.jsons;

              let count    = 1;

              for (let i in itemJson) {
                  
                fun_adder_edit(count, itemJson[i].employeeid, itemJson[i].employeename, itemJson[i].itemid, itemJson[i].itemname, itemJson[i].allot_qty);

                count++;

              }

            }

          }

        });
          
      }

      //--------------------:(Edit Function End:(------------------------//


      //-------------------------:)Update Function Start :)---------------------------//

      function update() {

        let valid = true;

        if (checker('section1') != false) {

          valid = valid * true;

        } else {

          valid = valid * false;

        }

        let arr = [];

        $('#table-orderEdit > tbody > tr').each(function () {

          let srNo          = $(this).find('.srNo').text().trim();

          let employeeid    = $(this).find('.employeeid').text().trim();

          let employeename  = $(this).find('.employeename').text().trim();

          let itemid        = $(this).find('.itemid').text().trim();
          
          let itemname      = $(this).find('.itemname').text().trim();
                  
          let outqty        = $(this).find('.outqty').text().trim();
    
          arr.push({

            "srno": srNo,

            "employeeid": employeeid,

            "employeename": employeename,

            "itemid": itemid,

            "itemname": itemname,

            "outqty": outqty,

          });

        })

        arr.shift();

        if (arr == '') {

          $('#table-orderEdit').css('border', '2px solid red');

          valid = valid * false;

        } else {

          $('#table-orderEdit').css('border', '2px solid green');

          valid = valid * true;

        }

        if (valid) {

          let data1          = checker('section1');
            
              data1          = JSON.stringify(data1);

              itemDetails = JSON.stringify(arr);

          $.ajax({
              
            type: "POST",
            
            data: {

              data: data1,

              itemDetails: itemDetails,

            },

            url: 'api/job-allotment-update.php',
            
            cache: false,
              
            success: function (res) {
            
              if (res.status == 'success') {
          
                swal({
                    
                  type: 'success',
                
                  title: 'Job Shedule & Material Issue Update',
                
                  showConfirmButton: false,
                
                  timer: 3000
                
                });
              
                setTimeout(function () {
                    
                  location.href = 'job-allotment1.php';
                
                }, 3000);
              
              }

            }
          
          });
        
        }

      }

      //--------------------:(Update Function End:(------------------------//

      $('#btn-reset').on('click', function () {

        window.location.reload();

      });

      $(document).ready(function() {

        $('.select-js').select2({width: '100%', tags: true});

        $('.select').attr('style','width:100%!important;');

        $('[data-toggle="popover"]').popover({
              trigger: 'hover',
                  'placement': 'top'
          });
                  
      });


      //--------------------:)Modal Close Function Start:)------------------------//

      $('#materialIssue').on('hidden.bs.modal', function (e) {
        
        location.href = 'job-allotment1.php';

      })

      $('#materialIssue1').on('hidden.bs.modal', function (e) {
        
        location.href = 'job-allotment1.php';

      })

      //--------------------:(Modal Close Function End:(------------------------//


      //--------------------:)Job Confirm Function Start:)------------------------//

      function joballowted() {

        var serviceid = $('#serviceid').val();

        var branchid  = $('#branchid').val();

        $.ajax({

          type: "POST",

          data: 'serviceid=' + serviceid + '&branchid=' + branchid,

          url: 'api/joballowted.php',

          cache: false,

          success: function (res) {

            if (res.status == 'success') {

              location.href = 'job-allotment1.php';

            }

          }
          
        })

      }

      //--------------------:(Job Confirm Function End:(------------------------//


    //--------------------:)Location Address Function Start:)------------------------//

    function locationAddress() {

      let locationAddress = $('#locationAddress').val();

      if (locationAddress != 'Select') {

        console.log(locationAddress);

        setTimeout(function () {


          $('#serviceAddress').val(locationAddress);

        }, 500);

      } else {

        $('#serviceAddress').val('');

      }
      
    }

  function locationAddresss() {

      let locationAddress = $('#locationAddresss').val();

      console.log(locationAddress);

      if (locationAddress != 'Select') {


        $('#serviceAddress1').val(locationAddress);

      } else {

        $('#serviceAddress1').val('');

      }
      
    }
    //--------------------:(Location Address Function End:(------------------------//

    </script>

  </body>

</html>
