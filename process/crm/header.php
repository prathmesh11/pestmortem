<?php

$css = '<link rel="stylesheet" href="'.$base.'css/classic.css">
<link rel="stylesheet" href="'.$base.'css/classic.date.css">
<link rel="stylesheet" href="'.$base.'css/classic.time.css">
<link rel="stylesheet" href="'.$base.'css/fancy.css">
<link href="'.$base.'/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" href="'.$base.'css/grid.min.css">
<link rel="stylesheet" href="'.$base.'css/alart.css">';


$js = '<script src="'.$base.'js/picker.js"></script>
<script src="'.$base.'js/picker.date.js"></script>
<script src="'.$base.'js/picker.time.js"></script>
<script src="'.$base.'js/pdfmake.min.js"></script>
<script src="'.$base.'js/vfs_fonts.js"></script>
<script src="'.$base.'/js/select2.min.js"></script>
<script src="'.$base.'js/fwork.js"></script>
<script src="'.$base.'js/fancy.js"></script>
<script src="'.$base.'js/list.min.js"></script>
<script src="'.$base.'js/alart.js"></script>';

    include($base.'_in/header.php');
    include($base.'_in/connect.php');
    
    if (!in_array("Work Contract", $arrysession) && !in_array('Job Allotment', $arrysession) && !in_array('Job Completed', $arrysession) ) {
   
        echo "<script>window.location.href='/process/dashboard.php';</script>";
       
        exit;

    }

    $con = _connect();
?>
<style>
    .th{
        background:#8e44ad;
        color:#fff;
        text-align:center;
        padding-top:2px;
        padding-bottom:2px;
        border:1px solid #fff;
    }
    .td{
        border:1px solid #ddd;
    }
    .table-ui .btn{
        margin:3px;
    }
    #myModal1 .col-sm-4,.col-sm-8{
      margin-bottom:2px;
    }
    .picker__select--year{
        height:auto;
    }
    .picker__select--month{
        height:auto;
    }
    .table-list td,
   .table-list th {
       border: 1px solid #ddd;
       padding: 1px !important;
       font-size: 13px;
   }
   .table-list td{
	padding-top: 10px !important;
   }

   .table-list tr:nth-child(even) {
       background-color: #f2f2f2;
   }

   .table-list th {
       padding-top: 5px;
       padding-bottom: 5px;
       text-align: center;
       background-color: #16a085;
       color: white;
   }
    </style>
<div class="container-fluid">
    <div id="div-content" class="content">
<table width="100%" >
<tr>
<!-- <td align="center" style="<?php echo $EnquiryFollowUp; ?>width:15%"><a href="/process/crm/enquiry.php" style="border:1px solid blue;border-radius:0px;<?php echo $navenq1;?>" class="btn btn-primary btn-block">Enquiry & FollowUp</td> -->
<!-- <td align="center" style="<?php echo $Quotation; ?>width:15%"><a href="/process/crm/quotation.php" style="border:1px solid blue;border-radius:0px;<?php echo $navenq8;?>" class="btn btn-primary btn-block">Quotation</td> -->
<td align="center" style="<?php echo $WorkContract; ?>width:20%"><a href="/process/crm/workcontract.php" style="border:1px solid blue;border-radius:0px;<?php echo $navenq2;?>" class="btn btn-primary btn-block">Work Contract</td>
<td align="center" style="<?php echo $JobAllotment; ?>width:20%"><a href="/process/crm/job-allotment1.php" style="border:1px solid blue;border-radius:0px;<?php echo $navenq3;?>" class="btn btn-primary btn-block">Job Allotment</td>
<td align="center" style="<?php echo $JobCompleted; ?>width:20%"><a href="/process/crm/job-complete.php" style="border:1px solid blue;border-radius:0px;<?php echo $navenq4;?>" class="btn btn-primary btn-block">Job Completed</td>
<td align="center" style="<?php echo $RescheduleJob; ?>width:20%"><a href="/process/crm/reschedule.php" style="border:1px solid blue;border-radius:0px;<?php echo $navenq5;?>" class="btn btn-primary btn-block">Reschedule Job</td>
<!-- <td align="center" style="<?php echo $ManualBiling; ?>width:10%"><a href="/process/crm/Manual-biling.php" style="border:1px solid blue;border-radius:0px;<?php echo $navenq6;?>" class="btn btn-primary btn-block">Manual Biling</td> -->

<!-- <td align="center" style="<?php echo $Payment; ?>width:15%"><a href="/process/crm/payment.php" style="border:1px solid blue;border-radius:0px;<?php echo $navenq6;?>" class="btn btn-primary btn-block">Payment</td> -->
<!-- <td align="center" style="<?php echo $CloseEnquiry; ?>width:10%"><a href="/process/crm/close-enquiry.php" style="border:1px solid blue;border-radius:0px;<?php echo $navenq7;?>" class="btn btn-primary btn-block">Close Enquiry</td> -->
<td align="center" style="<?php echo $CloseEnquiry; ?>width:20%"><a href="/process/crm/close-enquiry.php" style="border:1px solid blue;border-radius:0px;<?php echo $navenq7;?>" class="btn btn-primary btn-block">Reporting</td>

</tr>
</table>















