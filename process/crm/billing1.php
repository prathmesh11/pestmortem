<?php 

$base='../../';
$navenq5='background:#1B1464;';
include('header.php');

if (session_status() == PHP_SESSION_NONE) { session_start(); }
$sessionby = $_SESSION['employeeid'];
$branchid = $_SESSION['branchid'];
?>

<br>
<img class="hidden" id=imageid src="../../../../img/PestMortemLogo.jpg">

<div class="table-ui container-fluid">
<div class="tr row">
<div class="col-sm-1 th" style="word-wrap:break-word;">Enquiry No.</div>  
<div class="col-sm-3 th">Customer Info</div> 
<div class="col-sm-3 th">Bill Description</div>
<div class="col-sm-4 th">Payment info</div>
<div class="col-sm-1 th">Action</div>
</div>

<?php
$threedaysbefore=$CURRENT_MILLIS+172800000;
$result=mysqli_query($con,"SELECT * FROM workcontractbill WHERE paid_time=0 AND bill_time = 0 AND billdate<'$threedaysbefore' AND branchid='$branchid' ORDER BY billdate DESC");
while($rows = mysqli_fetch_assoc($result)){
        $enquiryid=$rows['enquiryid'];
        $customerid=$rows['customerid'];

        //$customerid=mysqli_fetch_assoc(mysqli_query($con,"SELECT customerid x FROM crmmaster WHERE wo_time<>0 AND wo_by<>0 AND enquiryid='$enquiryid'"))['x'];
        if($customerid){
        $cifo=mysqli_fetch_assoc(mysqli_query($con,"SELECT * FROM customermaster WHERE customerid='$customerid'"));
        $category=mysqli_fetch_assoc(mysqli_query($con,"SELECT category x FROM crmmaster WHERE wo_time<>0 AND wo_by<>0 AND enquiryid='$enquiryid'"))['x'];

        $contractid=$rows['contractid'];
       
        $query="SELECT SUM(totalpaidamt) x FROM `workcontractbill` WHERE contractid='$contractid'";
        $totalpaiamt=mysqli_fetch_assoc(mysqli_query($con,$query))['x'];

        $totalamt=mysqli_fetch_assoc(mysqli_query($con,"SELECT amount x FROM workcontract WHERE contractid='$contractid'"))['x'];
        $other=mysqli_fetch_assoc(mysqli_query($con,"SELECT othercharges x FROM workcontract WHERE contractid='$contractid'"))['x'];
        $billqty=mysqli_fetch_assoc(mysqli_query($con,"SELECT billqty x FROM workcontract WHERE contractid='$contractid'"))['x'];

        

        $billid=$rows['billid'];
        $servicedate=$rows['billdate'];
        $servicename=$rows['servicename'];
        $shedulestart=$rows['shedulestart'];
        $sheduleend=$rows['sheduleend'];
        $empjson=$rows['empjson'];
        $contractamt=$rows['contractamt'];
        $followupdate=$rows['followupdate'];
       
        // if ($totalpaiamt==0) {
        //   $totalpaiamt=$contractamt/$billqty;       
        // }else{
        //   $totalpaiamt;
        // }

        ?>
<div class="row tr">
<div class="col-sm-1 td" style="word-wrap:break-word;"><?php echo $enquiryid; ?></div>   
<div class="col-sm-3 td">
<?php


echo 'Company : '.$cifo['customername']; 
echo '<br>Address : '.get_object_vars(json_decode($cifo['customerjson']))['address']; 
echo '<br>Name  : '.get_object_vars(json_decode($cifo['contactjson'])[0])['name'];
echo '<br>Dept. : '.get_object_vars(json_decode($cifo['contactjson'])[0])['department'];
echo '<br>Mobile : <a href="tel:'.get_object_vars(json_decode($cifo['contactjson'])[0])['mobile'].'">'.get_object_vars(json_decode($cifo['contactjson'])[0])['mobile'].'</a>';
echo '<br>Landline'.get_object_vars(json_decode($cifo['contactjson'])[0])['landline'];
echo '<br>Email : <a href="mailto:'.get_object_vars(json_decode($cifo['contactjson'])[0])['email'].'">'.get_object_vars(json_decode($cifo['contactjson'])[0])['email'].'</a>';
?>
</div>   
<div class="col-sm-3 td" style="word-wrap:break-word;"><?php
echo 'Servcie Name : '.$category.'<br>';

$lastfollowup=$servicedate;
$followUpStatusFlag='';

if($lastfollowup!=0){
  if($START_OF_THE_DAY<$lastfollowup && $END_OF_THE_DAY<=$lastfollowup){
      $followUpStatusFlag='#3F51B5';
  }

  if($START_OF_THE_DAY>$lastfollowup && $END_OF_THE_DAY>$lastfollowup){
      $followUpStatusFlag='#F44336';
  }
  
  if($START_OF_THE_DAY<=$lastfollowup && $END_OF_THE_DAY>$lastfollowup ){
      $followUpStatusFlag='#4CAF50';
  }
}else{
  $followUpStatusFlag='#ff9800';
}

echo 'Bill Date : <p style="background:'.$followUpStatusFlag.';color:#fff;">'.date("d-m-Y",$servicedate/1000).'</p>'; 
echo 'Contract No. : '.$contractid.'<br>';
echo 'Bill No. : '.$billid; 

?></div>

<div class="col-sm-4 td">
<?php 
echo 'Contract Amount : '.$contractamt.'<br>';
echo 'Total Paid Amount :'.$totalpaiamt.'<br>';
echo 'Follow Up : ';
if($followupdate!="0"){
echo date("d-m-Y",$followupdate/1000);
}
?>
<table class="table table-list">
<thead>
<tr>
<th>BILL AMT.</th>
<th>CGST</th>
<th>SGST</th>
<th>IGST</th>
<th>OTHER</th>
</tr>
</thead>
<tbody>
<?php
                $bal=0;$billamt=0;$created=""; $para=0;$billamt=0;
                    $result4=mysqli_query($con,"SELECT * FROM workcontractbill WHERE billid ='$billid' ");
                     while($rows3 = mysqli_fetch_assoc($result4)){
                         $count++;
                      $balance = ($rows3['contractamt']-$rows3['billamt']);
                      $para = "'".$rows['billid']."','".$rows3['enquiryid']."','1'";
                      $para1 = "'".$rows['billid']."','".$rows3['enquiryid']."','2'";
                      
                      $created = $rows3['created'];
                      $enquiryid1 = $rows['enquiryid'];
                      $totalpaiamt=mysqli_fetch_assoc(mysqli_query($con,"SELECT SUM(totalpaidamt) x FROM `workcontractbill` WHERE contractid='$contractid' billid ='$billid' "))['x'];
                      if ($totalpaiamt==0) {
                       $billamt =  ($rows3['contractamt']/$billqty)-$rows3['discount_amt'];
                      }else{
                       $billamt =  mysqli_fetch_assoc(mysqli_query($con,"SELECT totalpaidamt x FROM `workcontractbill` WHERE contractid='$contractid' AND billid ='$billid' "))['x'];
                      }
                     echo '<tr>'; 
                     echo '<td align="center">'.$billamt.'</td>';
                     //echo '<td align="center"><a href="javascript:void(0);" onclick="printer('.$para.')" >'.$rows3['billamt'].'</a></td>';
                     echo '<td align="center">'.number_format($rows3['cgstper']/$billqty,2).'</td>';
                     echo '<td align="center">'.number_format($rows3['sgstper']/$billqty,2).'</td>';
                     echo '<td align="center">'.number_format($rows3['igstper']/$billqty,2).'</td>';
                     echo '<td align="center">'.number_format($other/$billqty,2).'</td>';
                     echo '<tr>';
                    }
?>

</tbody>
</table>
</div>   

<div class="col-sm-1 td">


<?php 

if ($category=="sales") {
echo '<button class="btn btn-sm btn-primary" data-enquiryid="'.$rows['enquiryid'].'" data-contractid="'.$rows['contractid'].'" onclick="salespdf(this)">Sales Bill</button>';
  
  }else {?>
    <a class="btn btn-sm btn-block btn-success" href="javascript:void(0);" onclick="printer(<?php echo $para; ?>);" >Print  Invoice</a>

<?php  
  }

  ?>
<button class="btn btn-sm btn-block btn-success" data-billid="<?php echo $billid; ?>" data-billamt="<?php echo $billamt; ?>" data-toggle="modal" data-target="#myModal" onclick="setidtomidal(this)">Confirm</button>
<button class="btn btn-sm btn-block btn-warning" data-billid="<?php echo $billid; ?>"  data-toggle="modal" data-target="#myModal2"  onclick="setidtomidal2(this)">Follow Up</button>
<button class="btn btn-sm btn-block btn-primary" data-billid="<?php echo $billid; ?>" data-billamt="<?php echo $billamt; ?>" data-toggle="modal" data-target="#myModal3"  onclick="discount(this)">Discount</button>


</div>   
</div>


<?php
        }
}
?>


<div id="myModal3" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" align="center">Discount On Bill</h4>
      </div>
      <div class="modal-body">
          <div class="form-group row">
 
       <input type="hidden" data-name="hbillid" id="billidset">
        <div class="col-sm-4" >Bill Id</div>
        <div class="col-sm-6"><input type="text" data-name="billid" id="dis_billid" class="form-control input-sm" readonly></div>
        </div>
         <div class="form-group row">
 
        <div class="col-sm-4" >Total Amount Paid</div>
        <div class="col-sm-6"><input type="text" data-role= "text" data-name="distotalpaidamt" id="dis_totalpaidamt"  class="form-control input-sm" readonly></div>
        </div>
         <div class="form-group row">
 
        <div class="col-sm-4" >Discount Amount</div>
        <div class="col-sm-6"><input type="number" data-role= "number" data-name="discountAmt" onkeyup="discamt()" id="discountAmt" class="form-control input-sm" ></div>
        </div>
        <div class="form-group row">
        <div class="col-sm-4" >After Discount Amount</div>
        <div class="col-sm-6"><input type="number" data-role= "number" data-name="afterdiscountAmt" id="afterdiscountAmt" class="form-control input-sm" readonly></div>
        </div>
     <div class="form-group row">
      <div class="col-sm-4" ></div>
       <div class="col-sm-6"><button class="btn btn-success btn-block btn-sm" id="confirm" onclick="confirmdiscount();">Confirm Discount</button></div>
        </div>
      </div>
    </div>
  </div>
</div>


<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" align="center">Confirm Bill</h4>
      </div>
      <div class="modal-body">
          <div class="form-group row">
 
       <input type="hidden" data-name="hbillid" id="billidset">
        <div class="col-sm-4" >Bill Id</div>
        <div class="col-sm-6"><input type="text" data-name="billid" id="billid" class="form-control input-sm" readonly></div>
        </div>
         <div class="form-group row">
 
        <div class="col-sm-4" >Total Amount Paid</div>
        <div class="col-sm-6"><input type="text" data-role= "text" data-name="totalpaidamt" id="totalpaidamt"  class="form-control input-sm" readonly></div>
        </div>
         <div class="form-group row">
 
        <div class="col-sm-4" >Note</div>
        <div class="col-sm-6"><input type="text" data-role= "text" data-name="note" class="form-control input-sm" ></div>
        </div>
     <div class="form-group row">
      <div class="col-sm-4" ></div>
       <div class="col-sm-6"><button class="btn btn-success btn-block btn-sm" id="confirm" onclick="confirmbill();">Confirm</button></div>
        </div>
      </div>
    </div>
  </div>
</div>


<div id="myModal2" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" align="center">New FollowUp</h4>
      </div>
      <div class="modal-body">
          <div class="form-group row">
       <div class="col-sm-4">Set New Followup</div>
       <input type="hidden" data-name="billid" id="billid2">
       <div class="col-sm-8"><input type="text" data-role ="text" data-name ="newfollowupdate" id="newfollowupdate" class="form-control input-sm datepicker">
            <input type="hidden" id="inp-enq-olddate-stamp" data-name="followuptime">
       </div>
       <div class="col-sm-4">Note</div>
       <div class="col-sm-8"><input type="text" data-role="text" data-name="note" class="form-control input-sm"></div>
       <div class="col-sm-4"></div>
       <div class="col-sm-8"><button class="btn btn-success btn-block btn-sm" onclick="followup()">Add New FollowUp</button></div>
        </div>
        
      </div>
    </div>
  </div>
</div>


</div>
</div>

<?php 
include('footer.php');
?>
<style type="text/css">
  .picker__select--year{
        height:auto;
    }
    .picker__select--month{
        height:auto;
    }

</style>
<script>
$('.datepicker').pickadate({     
    selectYears: 100,
  selectMonths: true,
  onClose: function () {
    try {
      var dateStamp = this.get('select')['pick'];
      var id = this.get('id');
      $('#' + id).attr('data-stamp', dateStamp);
      $('#' + id).parent().find('.timepicker').click();
    } catch (err) {
      console.log(err);
    }
  }
});


function discount(e){
var billid=$(e).data('billid');
var billamt=$(e).data('billamt');

$('#billidset').val(billid);
$('#dis_billid').val(billid);
$('#dis_totalpaidamt').val(billamt);

}

function discamt(){
  var discountAmt  = $('#discountAmt').val();
  var totalpaidamt = $('#dis_totalpaidamt').val();
  var afterdiscountAmt = 0;

  if (parseFloat(discountAmt) > parseFloat(totalpaidamt)) {
    alert('discount amount not grater than total paid amount');
    $('#discountAmt').val(0);
    discamt();
  }else{
    afterdiscountAmt = parseFloat(totalpaidamt) -  parseFloat(discountAmt);
    $('#afterdiscountAmt').val(afterdiscountAmt);
  }
}


function confirmdiscount(billid) {
  var valid = true;

if (checker('myModal3') != false) {
  valid = valid * true;
} else {
  valid = valid * false;
}
if (valid) {
  var data = checker('myModal3');
  var datastr = JSON.stringify(data);

  $.ajax({
    type: "POST",
    data: {
      data: datastr
    },
    url: 'api/confirmbillafterdiscount.php',
    cache: false,
    success: function (res) {
      // alert(res.status);
      if (res.status == 'success') {

        // window.location = "payment.php";
        window.location.reload();
      }
    }
  })
}
}

function confirmbill(billid) {
  var valid = true;

  if (checker('myModal') != false) {
    valid = valid * true;
  } else {
    valid = valid * false;
  }
  if (valid) {
    var data = checker('myModal');
    var datastr = JSON.stringify(data);

    $.ajax({
      type: "POST",
      data: {
        data: datastr
      },
      url: 'api/confirmbill.php',
      cache: false,
      success: function (res) {
        // alert(res.status);
        if (res.status == 'success') {

          // window.location = "payment.php";
          window.location.reload();
        }
      }
    })
  }

}

function getBase64Image(img) {
  var canvas = document.createElement("canvas");
  canvas.width = img.width;
  canvas.height = img.height;
  var ctx = canvas.getContext("2d");
  ctx.drawImage(img, 0, 0);
  var dataURL = canvas.toDataURL("image/png");
  return dataURL.replace('/^data:image\/(png|jpg);base64,/', "");
}

function setidtomidal(e){
var billid=$(e).data('billid');
var billamt=$(e).data('billamt');

$('#billidset').val(billid);
$('#billid').val(billid);
$('#totalpaidamt').val(billamt);

}


function setidtomidal2(e){
var billid=$(e).data('billid');
$('#billid2').val(billid);

}
function confirmbill(billid) {
      var valid = true;
    
  if (checker('myModal') != false) {
    valid = valid * true;
  } else {
    valid = valid * false;
  }
  if (valid) {
    var data=checker('myModal');
    var datastr = JSON.stringify(data);

         $.ajax({
            type: "POST",
            data :{ data: datastr    },
             url: 'api/confirmbill.php',
            cache: false,
            success: function (res) {
              // alert(res.status);
               if (res.status == 'success') {
                  
                    // window.location = "payment.php";
                    window.location.reload();    
                }
            }
        })
       }
    
}

 function followup(){
    var valid = true;
  if (checker('myModal2') != false) {
    valid = valid * true;
  } else {
    valid = valid * false;
  }
  if (valid) {
    var data=checker('myModal2');
    var datastr = JSON.stringify(data);
    $.ajax({
        type: "POST",
        data: {
          data: datastr
        },
        url: 'api/setfollowup.php',
        cache: false,
        success: function (res) {
          if (res.status == 'success') {
             window.location.reload();
          }
        }
      })
    }
  }


function printer(id,enqno,cnt) {
  var billtype="";
  if(cnt == 1){
    billtype = "INVOICE BILL";
  }else if(cnt == 2){
    billtype = "BILL INVOICE";
  }
   var jsondata = $.parseJSON($.ajax({
         type: "POST",
        data: {
          id: id
        },
        url: 'api/getbilljson.php',
        cache: false,
        success: function (res) {
        // alert(res.status);
         },
        dataType: "json", 
        async: false
    }).responseText);
   

jsondata =JSON.stringify(jsondata);
data1 = JSON.parse(jsondata);  
json1 = data1.json;     
 
 //get customer info

var jsondata = $.parseJSON($.ajax({
         type: "POST",
        data: {
          enqno: enqno
        },
        url: 'api/getcustjson.php',
        cache: false,
        success: function (res) {
        //jsondata =JSON.stringify(res.json);
         },
        dataType: "json", 
        async: false
    }).responseText);
   jsondata =JSON.stringify(jsondata);
data2 = JSON.parse(jsondata);  
json2= data2.json; 
customerno = data2.custno;
customername = data2.customername;
state = data2.state;
gstno = data2.gstno;
 
 //get contract info

var jsondata = $.parseJSON($.ajax({
         type: "POST",
        data: {
          enqno: enqno,
          id: id
        },
        url: 'api/getwcjson.php',
        cache: false,
        success: function (res) {
          //alert(res.status);
        //jsondata =JSON.stringify(res.json);
         },
        dataType: "json", 
        async: false
    }).responseText);
itemjson = jsondata["itemjson"];
servicejson = jsondata["servicejson"];
contractdate = jsondata["contractdate"];
contractstart = jsondata["contractstart"];
contractend = jsondata["contractend"];
totalcontractamt = jsondata["contractamt"];
sac = jsondata["sac"];




// if (next_billdate == '01-01-1970' ) {
// next_billdate='';
// next_contract_amt='';
// }
// paid_amt=jsondata['paid_amt'];
// due_payment=jsondata['due_payment'];

// billingpartyaddress=jsondata['billingpartyaddress'];
// if(billingpartyaddress==''){
  address = data2.address;
// }else{
//   address = jsondata['billingpartyaddress'];
// }

itemjson =JSON.stringify(itemjson);
itemjson = JSON.parse(itemjson);  


servicejson =JSON.stringify(servicejson);
servicejson = JSON.parse(servicejson);  
 
billamt = json1.qty * json1.rate;
 
balamt = totalcontractamt - billamt;

totalamtinwords = convertNumberToWords(json1.grand_total)+ " Only";

category = jsondata["category"];

 //get branch info

var jsondata = $.parseJSON($.ajax({
         type: "POST",
        data: {
          enqno: enqno
        },
        url: 'api/getbrjson.php',
        cache: false,
        success: function (res) {
          //alert(res.status);
        //jsondata =JSON.stringify(res.json);
         },
        dataType: "json", 
        async: false
    }).responseText);

jsondata =JSON.stringify(jsondata);
data4 = JSON.parse(jsondata); 

branchjson= data4.branchjson; 
branchshortname= data4.branchshortname; 
branchname = data4.branchname;
console.log(json1.paid_date);

if (json1.paid_date=='01-01-1970') {
   json1.paid_date='';
}else{
  json1.paid_date;
}

// serviceinfo = "\nSERVICE : "+servicejson[0].servicename+"\nCONTRACT PERIOD : "+contractstart+" TO "+contractend+"\nP.O. No. : "+json1.pono+" DTD : "+contractdate+"\n\n";
if (category=='Pest Control') {
  if(json1.next_billdate=='01-01-1970'){
    serviceinfo = "\nCONTRACT PERIOD : "+contractstart+" TO "+contractend+"\n\n Paid Payment : " +json1.paid_amt  +  "  Payment date : " +json1.paid_date;
  }else{
    serviceinfo = "\nCONTRACT PERIOD : "+contractstart+" TO "+contractend+"\n\n Paid Payment : " +json1.paid_amt + " Payment date : " +json1.paid_date + "\nNext Payment Date : " + json1.next_billdate  + '    Next Contract Amount : '+json1.next_totalcontractamt + "\n Pending Payment : " +json1.due_payment;
  }
  
}else{
  serviceinfo = "\nCONTRACT PERIOD : "+contractstart+"\n\n";
}

//balanceinfo ="\nRs. "+json1.totalcontractamt+": TOTAL CONTRACT AMOUNT\nRs."+billamt.toFixed(2)+": 1ST QUARTER BILL\n ----------------\nRs."+balamt.toFixed(2)+" : BALANCE";

paymentinfo = "Rupees : "+totalamtinwords+"\n\nPayment should be made within 30 days from the date of invoice.\n Interest @18% will be charged on overdue Invoices.\n GSTIN: "+gstno+"\n Category of Service : Cleaning Service\n\n E.P.F. A/c No. : MH/40228\n E.S.I.C. Code No. : 31-480-101\n PAN NO. : AACCP25 \n ";
paymentinfo2 = "\n\n Bank Name : "+branchjson.bankname+" \nBranch Name :"+branchjson.bankbranchname+"\n Account holder's Name : M/s. Pest Mortem (India) Pvt. Ltd.\n AC Number : C.A.No. "+branchjson.accountno+"\n RTGS/NEFT IFSC CODE : "+branchjson.ifsccode;



var fullDate = new Date();
var twoDigitMonth = ((fullDate.getMonth().length + 1) === 1) ? (fullDate.getMonth() + 1) : '' + (fullDate.getMonth() + 1);
var currentDate = fullDate.getDate() + "/" + twoDigitMonth + "/" + fullDate.getFullYear();


var content=[]; 
       


                  dd = {
               
                    pageSize: 'A4',
                    pageMargins: [50, 30, 30, 40],
                    footer: function (currentPage, pageCount) {
                        return {
                            margin: 10,
                            columns: [{
                                fontSize: 8,
                                text: [{
                                        text: '--------------------------------------------------------------------------' +
                                            '\n',
                                        margin: [0, 20]
                                    },
                                    {
                                        text: '©Pest Mortem (India) PVT. LTD. || Printed On ' +currentDate ,
                                    }
                                ],
                                alignment: 'center'
                            }]
                        };

                       },

                    content: [
                           {
                                text: billtype,
                                 fontSize: 10,
                                      bold: true,
                                      alignment: 'center',
                            },
                            {
                                text: 'SUBJECT TO MUMBAI JURIDICTION',
                                 fontSize: 8,
                                      bold: true,
                                      alignment: 'center',
                            },
                             {
                                text: 'PEST MORTEM (INDIA) PRIVATE LIMITED',
                                 fontSize: 15,
                                 bold: true,
                                alignment: 'center',
                            },
                            {
                                    text: '(Estd. 1980)\n',
                                     fontSize: 10,
                                      bold: true,
                                      alignment: 'center',
                                },

                                {
                                    text: '\nRegd. Office : '+branchjson.address+'\nTel : '+branchjson.phone+' Email : '+branchjson.email +'\n\n',
                                    fontSize: 8,
                                    bold: true,
                                    alignment: 'center',
                                },
                                {
                                     
                                    bold: true,
                                     fontSize: 8,
                                    margin: [0, 0, 0, 0],
                                    table: {
                                         widths: ['*'],
                                        height:400,
                                       
                                        body: [
                                        [
                                            {
                                             border : [true, true, true, false], 
                                             text: [{text:'Regd. Office : ',bold:true},' G -2, Sunder Tower, T.J. Road, Sewree(west),Mumbai - 400015\n Phone : 24147425/24127935/24111976/24149566/65531746/47 . Fax : 91-22-24150261 . \n E-Mail : pestmort@bom3.vsnl.net.in . pestmortem@pestmortem@pestmortem.com. Website : www.pestmortem.com'] ,
                                              fontSize: 9,
                                               alignment: 'center',
                                              bold:true
                                            },
                                               
                                              ],
                                            ],

                                    },
 
                                },

                                {
                                     
                                    bold: true,
                                     fontSize: 8,
                                    margin: [0, 0, 0, 0],
                                    table: {
                                         widths: [50, '*', 70, '*'],
                                        height:400,
                                       
                                        body: [
                                            [
                                            { border : [true, true, false, false], 
                                            text: 'M/S           : ', fontSize: 8,bold:true
                                            },
                                             {
                                               border : [false, true, false, false], 
                                              text: customername,  fontSize: 8,bold:true
                                            },
                                            { border : [true, true, false, false], 
                                            text: 'CUSTOMER NO.    : ', fontSize: 8,bold:true
                                            },
                                             {
                                               border : [false, true, true, false], 
                                              text: customerno,  fontSize: 8,bold:true
                                            }
                                           
                                              ],
                                              [
                                            { border : [true, false, false, false], 
                                            text: 'ADDRESS : ', fontSize: 8,bold:true
                                            },
                                             {
                                               border : [false, false, false, false], 
                                              text:  address,  fontSize: 8,bold:true
                                            },
                                            { border : [true, false, false, false], 
                                            text: 'INVOICE NO.          : ', fontSize: 8,bold:true
                                            },
                                             {
                                               border : [false, false, true, false], 
                                              text: json1.billid,  fontSize: 8,bold:true
                                            }
                                              ],
                                            [
                                            { border : [true, false, false, false], 
                                            text: 'GST NO.    :', fontSize: 8,bold:true
                                            },
                                             {
                                               border : [false, false, true, false], 
                                              text: gstno,  fontSize: 8,bold:true
                                            },
                                            { border : [true, false, false, false], 
                                            text: 'INVOICE DATE       : ', fontSize: 8,bold:true
                                            },
                                             {
                                               border : [false, false, true, false], 
                                              text: json1.billdate,  fontSize: 8,bold:true
                                            }
                                              ],
                                               [
                                            { border : [true, false, false, false], 
                                            text: 'STATE       :', fontSize: 8,bold:true
                                            },
                                             {
                                               border : [false, false, true, false], 
                                              text: state,  fontSize: 8,bold:true
                                            },
                                            { border : [true, false, false, false], 
                                            text: 'CONTRACT NO.     : ', fontSize: 8,bold:true
                                            },
                                             {
                                               border : [false, false, true, false], 
                                              text: json1.contractid,  fontSize: 8,bold:true
                                            }
                                              ],
                                               [
                                            { border : [true, false, false, false], 
                                            text: 'A/C.           :', fontSize: 8,bold:true
                                            },
                                             {
                                               border : [false, false, true, false], 
                                              text: json1.ac,  fontSize: 8,bold:true
                                            },
                                            { border : [true, false, false, false], 
                                            text: 'CONTRACT DATE  : ', fontSize: 8,bold:true
                                            },
                                             {
                                               border : [false, false, true, false], 
                                              text: contractdate,  fontSize: 8,bold:true
                                            }
                                              ],
                                              /*
                                            ['M/S:', customername+'\n'+address, 'INVOICE No. :',json1.billid],
                                            ['GST no.:', gstno, 'INVOICE Date. :',json1.billdate],
                                            ['State:', state, 'CONTRACT NO. :',json1.contractid],
                                            ['A/C.:', json1.ac, 'CONTRACT DATE :',contractdate]
                                          */
                                        ],

                                    },
 
                                },
                                                                     
                                {
                                     border : 0,
                                    bold: true,
                                     fontSize: 8,
                                    margin: [0, 0, 0, 0, 0],
                                    table: {
                                         widths: [300, 40,20, 50, '*'],
                                   
                                        body: [
                                            [{text:'P A R T I C U L A R S',colSpan:3 }, {},{},'SAC', 'TOTAL'],
                                            [{text:serviceinfo,colSpan:3 }, {},{},'SAC', {text:json1.total}],

                                          //   [itemjson[0].itemname + '\n'+ serviceinfo +'\n'+balanceinfo, sac, json1.qty,json1.rate,billamt.toFixed(2)],
                                          //  [serviceinfo +'\n'+balanceinfo, '', '','',''],

                                        ],

                                    },

                                         
                                },
                                
                                {
                                    border : 0,
                                    bold: true,
                                     fontSize: 8,
                                    margin: [0, 15],
                                    table: {
                                         widths: [350,  '*'],
                                   
                                        body: [
                                            // [paymentinfo,  {text: totalinfo,alignment: 'right'}],
                                          [paymentinfo,  {
                                                               border : 0,
                                                              bold: true,
                                                               fontSize: 8,
                                                              margin: [0, 15 ],
                                                              table: {
                                                                   widths: [80, '*'],
                                                             
                                                                  body: [
                                                                      ['TOTAL  ', ': '+json1.total],
                                                                      ['CGST '+json1.cgstper+'%', ': '+json1.cgstamt ],
                                                                      ['SGST '+json1.sgstper+' %', ': '+json1.sgstamt ],
                                                                      ['IGST '+json1.igstper+' %', ': '+json1.igstamt ],
                                                                      ['Other Charges', ': '+json1.other1], 
                                                                      ['Discount Amount', ': '+json1.discount_amt],       
                                                                      ['GRAND TOTAL ', ': '+json1.grand_total],
                                                                      ['Due Payment ', ': '+json1.due_payment],        
                                                           

                                                                  ],

                                                                }, layout: 'noBorders'

                                                      }],
                                                     ], 
                                    },  
                                     

                                         
                                },
                                {
                                    border : 0,
                                    bold: true,
                                     fontSize: 8,
                                    margin: [0, 15],
                                    table: {
                                         widths: [350,  '*'],
                                   
                                        body: [
                                            // [paymentinfo,  {text: totalinfo,alignment: 'right'}],
                                          [paymentinfo2,  {
                                                               border : 0,
                                                              bold: true,
                                                               fontSize: 8,
                                                              margin: [0 ,0],
                                                              alignment: 'center',
                                                              table: {
                                                                   widths: [150],
                                                             
                                                                  body: [
                                                                      
                                                                      [{text:'For Pest Mortem(India) Pvt. Ltd.',border:[true,true,true,false]}],
                                                                      [{text:'',border:[true,false,true,false,]}],
                                                                      [{text:'',border:[true,false,true,false,]}],
                                                                      [{text:'',border:[true,false,true,false,]}],
                                                                      [{text:'',border:[true,false,true,false,]}],
                                                                      [{text:'',border:[true,false,true,false,]}],
                                                                      [{text:'Authorised Signatory',border:[true,false,true,true,]}],


                                                                      
                                                                                                                                     

                                                                  ],

                                                                }, 

                                                      }],
                                                     ], 
                                    },  
                                     layout: 'noBorders'

                                         
                                },
                                

                    ]
               }
                         // totalinfo = "TOTAL     : "+billamt+"\n()FREIGHT     : "+freight+"\nCGST@ 0.00      : "+cgstamt+"\nSGST@ 0.00      : "+sgstamt+"\nIGST@ 18.00      : "+igstamt+"\nGRAND TOTAL      : "+grandtotal+"\n\n\n\n\n For Pest Mortem(India) Pvt. Ltd.\n\n\n\nAuthorised Signatory";
    
              // pdfMake.createPdf(dd).download("billno_"+id+".pdf");
             pdfMake.createPdf(dd).open();

}


function salespdf(e) {
  var contractid = $(e).data('contractid');
  var enquiryid = $(e).data('enquiryid');

  $.ajax({
    type: "POST",
    data: "contractid=" + contractid + "&enquiryid=" + enquiryid,
    url: 'api/salespdf.php',
    cache: false,
    success: function (res) {
      if (res.status == 'success') {
        var content = [];
        json = res.json[0];

        var branchjson=res.branchjson;


             
  content.push({
    layout: 'noBorders',
    table: {
      widths: ['*'],
      body: [
       [ {
              text: 'Sales Bill',
              fontSize: 10,
              bold: true,
              alignment: 'center',
            }],
            [{
              text: 'SUBJECT TO MUMBAI JURIDICTION',
              fontSize: 8,
              bold: true,
              alignment: 'center',
            }],
            [{
              text: 'PEST MORTEM (INDIA) PRIVATE LIMITED',
              fontSize: 15,
              bold: true,
              alignment: 'center',
            }],
            [{
              text: '(Estd. 1980)\n',
              fontSize: 10,
              bold: true,
              alignment: 'center',
            }],
            [{
              text:  'Regd. Office : ' + branchjson.address + '\nTel : ' + branchjson.phone + ' Email : ' + branchjson.email + '\n\n',
              fontSize: 8,
              bold: true,
              alignment: 'center',
            }],
      ]
    }
  });
  content.push({
    table: {
      widths: ['*'],
      body: [
       [ {
              text: 'Regd. Office : G -2, Sunder Tower, T.J. Road, Sewree(west),Mumbai - 400015\n Phone : 24147425/24127935/24111976/24149566/65531746/47 . Fax : 91-22-24150261 . \n E-Mail : pestmort@bom3.vsnl.net.in . pestmortem@pestmortem@pestmortem.com. Website : www.pestmortem.com' ,
              fontSize: 9,
              bold: true,
              alignment: 'center',
              border: [true, true, true, true],
            }],
            
      ]
    }
  });

  content.push({
    table: {
      widths: ['*','*'],
      body: [
       [{
        text: 'M/S            : ' + json.companyname.toUpperCase(),
              fontSize: 9,
              bold: true,
              alignment: 'left',
              border: [true, false, true, false],
       }, {
              text: 'CUSTOMER NO     : ' + json.enquiryid,
              fontSize: 9,
              bold: true,
              alignment: 'left',
              border: [true, false, true, false],
            }],
            [{
        text: 'ADDRESS  :  ' + json.caddress.toUpperCase(),
              fontSize: 9,
              bold: true,
              alignment: 'left',
              border: [true, false, true, false],
       }, {
              text: 'INVOICE NO.          : ' + res.billjson['billid'],
              fontSize: 9,
              bold: true,
              alignment: 'left',
              border: [true, false, true, false],
            }],
            [{
        text: 'GST NO.     : ' + json.gstnumber,
              fontSize: 9,
              bold: true,
              alignment: 'left',
              border: [true, false, true, false],
       }, {
              text: 'INVOICE DATE       : ' + res.billjson['billdate'],
              fontSize: 9,
              bold: true,
              alignment: 'left',
              border: [true, false, true, false],
            }],
            [{
        text: 'STATE        : ' + json.statename,
              fontSize: 9,
              bold: true,
              alignment: 'left',
              border: [true, false, true, false],
       }, {
              text: 'CONTRACT NO.     : ' + json.contractid,
              fontSize: 9,
              bold: true,
              alignment: 'left',
              border: [true, false, true, false],
            }],
            [{
        text: 'A/C.            : ' ,
              fontSize: 9,
              bold: true,
              alignment: 'left',
              border: [true, false, true, true],
       }, {
              text: 'CONTRACT DATE  : ' + json.date,
              fontSize: 9,
              bold: true,
              alignment: 'left',
              border: [true, false, true, true],
            }],
            
      ]
    }
  });



 


  var table1 = {
				widths: [15,95,40,40,40,40,40,40,40,45],
		                dontBreakRows: true,
                    fontSize: 20,
                    

				body: []
            };
          var particular=  [{text: 'SR NO',},{text: 'ITEM',alignment: 'center',},{text: 'Quantity',alignment: 'center',},{text: 'Rate',alignment: 'center',},{text: 'Amount',alignment: 'center',},{text: 'GST',alignment: 'center',},{text: 'CGST AMT',alignment: 'center',},{text: 'SGST AMT',alignment: 'center',},{text: 'IGST AMT',alignment: 'center',},{text: 'NET AMT',alignment: 'center',}];
           table1['body'].push(particular);     

           content.push({
        table: {
            headerRows: 1,
            // widths: ['*'],
            widths: [15,95,40,40,40,40,40,40,40,45],
            alignment: 'center',

            body: [
              [],
               
            ]
        },
    });
    var cr=1;
    var tablejson=json.tablejson;
      for(var i in tablejson){
        var tb=[];
        tb.push(cr,{text:tablejson[i].itemname,alignment: 'center'},{text:tablejson[i].qty,alignment: 'center'},{text:tablejson[i].rate,alignment: 'center'},{text:tablejson[i].amount,alignment: 'center'},{text:tablejson[i].gst,alignment: 'center'},{text:tablejson[i].cgstamt,alignment: 'center'},{text:tablejson[i].sgstamt,alignment: 'center'},{text:tablejson[i].igstamt,alignment: 'center'},{text:tablejson[i].total,alignment: 'center'});
        table1['body'].push(tb);
        cr++;
      }


content.push({
                        text: "",
                        fontSize: 20,
                        alignment: 'center'
                    }, {
                        style:'tablfont1',
                        table: table1
                    });

                    
content.push({
                        text: "\n",
                    });

                   var contractamt= res.billjson['contractamt'];
                    content.push({
                      table: {
				widths: [350, '*'],

				body: [
					[{rowSpan: 7, text: 'Rupees : ' + convertNumberToWords(contractamt)+'Rupes Only',bold: 'true',fontSize: '10'}, {text:'TOTAL                      : ' + json.amount,bold: 'true',fontSize: '8'}],
					['', {text:'CGST ' + res.billjson['cgstper'] + '%            : '   + res.billjson['cgstamt'],bold: 'true',fontSize: '8'}],
					['', {text:'SGST ' + res.billjson['sgstper'] + '%            : ' +res.billjson['sgstamt'],bold: 'true',fontSize: '8'} ],
					['',{text: 'IGST ' + res.billjson['igstper'] + '%             : ' + res.billjson['igstamt'],bold: 'true',fontSize: '8'}],
					['',{text:'Other Charges        : ' + res.billjson['other1'],bold: 'true',fontSize: '8'}],
          ['',{text:'Discount Amount   : ' + res.billjson['discount_amt'],bold: 'true',fontSize: '8'}],
          ['',{text:'GRAND TOTAL       :  ' +res.billjson['contractamt'],bold: 'true',fontSize: '8'}],
				]
			}
  });




  content.push({
    text: '\n'
  });
  content.push({
    text: '\n'
  });
 
                    content.push({
                      table: {
				widths: [350, '*'],

				body: [
					[{text: 'Bank Name : ' + res.branchjson['bankname'],bold: 'true',fontSize: '10',border: [false, false, false, false]}, {text:'For Pest Mortem(India) Pvt. Ltd.  ',bold: 'true',alignment: 'center',fontSize: '8',border: [true, true, true, false]}],
					[{text: 'Branch Name : ' + res.branchjson['bankbranchname'],bold: 'true',fontSize: '10',border: [false, false, false, false]}, {text:'',border: [true, false, true, false]}],
					[{text: 'Account holders Name :  M/s. Pest Mortem (India) Pvt. Ltd.' ,bold: 'true',fontSize: '10',border: [false, false, false, false]}, {text:'',border: [true, false, true, false]}],
					[{text: 'AC Number : C.A.No. ' + res.branchjson['accountno'] ,bold: 'true',fontSize: '10',border: [false, false, false, false]}, {text:'Authorised Signatory',bold: 'true',alignment: 'center',fontSize: '8',border: [true, false, true, true]}],
					[{text:'RTGS/NEFT IFSC CODE :    : ' + res.branchjson['ifsccode'],bold: 'true',fontSize: '10',border: [false, false, false, false]},{text:'',border: [false, false, false, false]}],
				]
			}
                    });



 
  dd = {
    pageSize: 'A4',
    pageOrientation: 'portrait',
    pageMargins: [40, 30, 30, 35],
    footer: function (currentPage, pageCount) {
      return {
        margin: 10,
        columns: [{
          fontSize: 8,
          text: [{
              text: '--------------------------------------------------------------------------' +
                '\n',
              margin: [0, 20]
            },
            {
              text: '© Pest Mortem (India) PVT. LTD | PAGE ' + currentPage.toString() + ' of ' + pageCount,
            }
          ],
          alignment: 'center'
        }]
      };

    },
    content,
    styles: {
      tablfont: {
        fontSize: 7
      },
      tablfont1: {
        fontSize: 9
      }
    }
  }
  var win = window.open('', '_blank');
  pdfMake.createPdf(dd).open({}, win);

        }
      }
  });  
}


function convertNumberToWords(amount) {
    var words = new Array();
    words[0] = '';
    words[1] = 'One';
    words[2] = 'Two';
    words[3] = 'Three';
    words[4] = 'Four';
    words[5] = 'Five';
    words[6] = 'Six';
    words[7] = 'Seven';
    words[8] = 'Eight';
    words[9] = 'Nine';
    words[10] = 'Ten';
    words[11] = 'Eleven';
    words[12] = 'Twelve';
    words[13] = 'Thirteen';
    words[14] = 'Fourteen';
    words[15] = 'Fifteen';
    words[16] = 'Sixteen';
    words[17] = 'Seventeen';
    words[18] = 'Eighteen';
    words[19] = 'Nineteen';
    words[20] = 'Twenty';
    words[30] = 'Thirty';
    words[40] = 'Forty';
    words[50] = 'Fifty';
    words[60] = 'Sixty';
    words[70] = 'Seventy';
    words[80] = 'Eighty';
    words[90] = 'Ninety';
    amount = amount.toString();
    var atemp = amount.split(".");
    var number = atemp[0].split(",").join("");
    var n_length = number.length;
    var words_string = "";
    if (n_length <= 9) {
        var n_array = new Array(0, 0, 0, 0, 0, 0, 0, 0, 0);
        var received_n_array = new Array();
        for (var i = 0; i < n_length; i++) {
            received_n_array[i] = number.substr(i, 1);
        }
        for (var i = 9 - n_length, j = 0; i < 9; i++, j++) {
            n_array[i] = received_n_array[j];
        }
        for (var i = 0, j = 1; i < 9; i++, j++) {
            if (i == 0 || i == 2 || i == 4 || i == 7) {
                if (n_array[i] == 1) {
                    n_array[j] = 10 + parseInt(n_array[j]);
                    n_array[i] = 0;
                }
            }
        }
        value = "";
        for (var i = 0; i < 9; i++) {
            if (i == 0 || i == 2 || i == 4 || i == 7) {
                value = n_array[i] * 10;
            } else {
                value = n_array[i];
            }
            if (value != 0) {
                words_string += words[value] + " ";
            }
            if ((i == 1 && value != 0) || (i == 0 && value != 0 && n_array[i + 1] == 0)) {
                words_string += "Crores ";
            }
            if ((i == 3 && value != 0) || (i == 2 && value != 0 && n_array[i + 1] == 0)) {
                words_string += "Lakhs ";
            }
            if ((i == 5 && value != 0) || (i == 4 && value != 0 && n_array[i + 1] == 0)) {
                words_string += "Thousand ";
            }
            if (i == 6 && value != 0 && (n_array[i + 1] != 0 && n_array[i + 2] != 0)) {
                words_string += "Hundred and ";
            } else if (i == 6 && value != 0) {
                words_string += "Hundred ";
            }
        }
        words_string = words_string.split("  ").join(" ");
    }
    return words_string;
}
</script>