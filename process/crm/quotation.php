<?php 
$base='../../';
$navenq8='background:#1B1464;';
include('header.php');
if (session_status() == PHP_SESSION_NONE) { session_start(); }
$sessionby = $_SESSION['employeeid'];
$branchid = $_SESSION['branchid'];

?>
<br>
<style>
.input-container {

    max-width: 300px;
    background-color: #EDEDED;
    border: 1px solid #DFDFDF;
    border-radius: 5px;
}

input[type='file'] {
    display: none;
}

.file-info {
    font-size: 0.9em;
}

.browse-btn {
    background: #03A595;
    color: #fff;
    min-height: 35px;
    padding: 10px;
    border: none;
    border-top-left-radius: 5px;
    border-bottom-left-radius: 5px;
}

.browse-btn:hover {
    background: #4ec0b4;
}

@media (max-width: 300px) {
    button {
        width: 100%;
        border-top-right-radius: 5px;
        border-bottom-left-radius: 0;
    }

    .file-info {
        display: block;
        margin: 10px 5px;
    }
}
</style>
<img class="hidden" id=imageid src="../../../../img/PestMortemLogo.jpg">
<div class="table-ui container-fluid">
    <div class="tr row">
        <div class="col-sm-2 th">Quotation No.</div>
        <div class="col-sm-1 th">Quotation Date</div>
        <div class="col-sm-4 th">Customer Info</div>
        <div class="col-sm-1 th">Enquiry For</div>
        <div class="col-sm-2 th">Subject</div>
        <div class="col-sm-2 th">Action</div>
    </div>
<?php 
$result=mysqli_query($con,"SELECT DISTINCTROW quatation_id,quatation_date,customerid,catagory,subject FROM quatation WHERE branchid='$branchid' AND created_time<>0  ORDER BY created_time DESC");
while($rows = mysqli_fetch_assoc($result)){
    $quatation_id=$rows['quatation_id'];
    $quatation_date=date('d-m-Y',$rows['quatation_date']/1000);
    $customerid=$rows['customerid'];
    $cifo=mysqli_fetch_assoc(mysqli_query($con,"SELECT * FROM customermaster WHERE customerid='$customerid'"));
    $catagory=$rows['catagory'];
    $subject=$rows['subject'];
?>
    <div class="tr row">
        <div class="col-sm-2 td"><?php echo $quatation_id;?></div>
        <div class="col-sm-1 td"><?php echo $quatation_date;?></div>
        <div class="col-sm-4 td"><?php 

echo 'Company : '.$cifo['customername']; 
echo '<br>Address : '.get_object_vars(json_decode($cifo['customerjson']))['address']; 
echo '<br>Name  : '.get_object_vars(json_decode($cifo['contactjson'])[0])['name'];
echo '<br>Dept. : '.get_object_vars(json_decode($cifo['contactjson'])[0])['department'];
echo '<br>Mobile : <a href="tel:'.get_object_vars(json_decode($cifo['contactjson'])[0])['mobile'].'">'.get_object_vars(json_decode($cifo['contactjson'])[0])['mobile'].'</a>';
echo '<br>Landline'.get_object_vars(json_decode($cifo['contactjson'])[0])['landline'];
echo '<br>Email : <a href="mailto:'.get_object_vars(json_decode($cifo['contactjson'])[0])['email'].'">'.get_object_vars(json_decode($cifo['contactjson'])[0])['email'].'</a>';
?></div>
        <div class="col-sm-1 td"><?php echo $catagory;?></div>
        <div class="col-sm-2 td"><?php echo $subject;?></div>
        <div class="col-sm-2 td">
        <button class="btn btn-sm btn-block btn-success" data-quatationid="<?php echo $rows['quatation_id']; ?>" onclick="pdf(this)">Quatation pdf</button>
        <a class="btn btn-sm btn-block btn-info" href="/process/crm/quatation/index.php?quatation_id=<?php echo $rows['quatation_id']; ?>">Edit</a>

        </div>
    </div>
    <?php
}
?>
</div>

    <a class="btn" style="font-size:25px;border-radius:50px;width:60px;height:60px;position: fixed;
    bottom: 40px;
    right: 40px;background:#eb2f06;color:#fff;" href="quatation/index.php">+</a>



<?php 
include('footer.php');
?>
<script>


function getBase64FromImageUrl(url) {
        var img = new Image();

        img.setAttribute('crossOrigin', 'anonymous');

        img.onload = function () {
            var canvas = document.createElement("canvas");
            canvas.width = this.width;
            canvas.height = this.height;
            var ctx = canvas.getContext("2d");
            ctx.drawImage(this, 0, 0);
            var dataURL = canvas.toDataURL("image/png");
            var base64 = dataURL.replace(/^data:image\/(png|jpg);base64,/, "");
            $('#imageid').val(base64);
        };
        img.src = url;
    }
getBase64FromImageUrl('/img/PestMortemLogo.jpg');


function pdf(e) {
  var quatationid = $(e).attr('data-quatationid');
  $.ajax({
    type: "POST",
    data: 'quatationid=' + quatationid,
    url: 'quatation/quatationpdf.php',
    cache: false,
    success: function (res) {
      if (res.status == 'success') {
        var content = [];
        var base64 = 'data:image/jpg;base64,' + $('#imageid').val();
        var regex = /<br\s*[\/]?>/gi;
        var conditionandscope = res.conditionandscope.replace(regex, "\n");
        var payment_terms = res.payment_terms.replace(regex, "\n");
        var branchjson = res.branchjson;
         
        var table1 = {
    widths: ['auto', '*', '*', '*'],
    dontBreakRows: true,
    body: []
  };

  var base64 = 'data:image/jpg;base64,' + $('#imageid').val();



  content.push({
    layout: 'noBorders',
    table: {
      widths: ['*', '*'],
      body: [
        ['', {
          text: "PEST MORTEM (INDIA) PRIVATE LIMITED\n",
          color: '#FF0000',
          fontSize: 13,
          bold: true
        }],
        [{
          image: base64,
          width: 100,
          height: 60
          
        }, {
          text: 'Address : '+branchjson.address+ '\nTel : '+branchjson.phone+' \nEmail : ' +branchjson.email+', \nWebsite : www.pestmortem.com, \nPAN NO :BOWPK3099A  \nGST NO :'+branchjson.gstnumber,
          alignment: 'justify',
          fontSize: 9
        }],
      ]
    }
  });

  content.push({
    layout: 'noBorders',
    table: {
      widths: ['*', '*'],
      body: [
        [{
          text: "REF : " + (res.quatation_id),  
          fontSize: 12
        }, {
          text: "Date : " + res.quatation_date,
          fontSize: 11,
          alignment: 'right',
          background: '#ffff00'
        }]
      ]
    }
  });


  content.push({
    layout: 'noBorders',
    table: {
      widths: ['*'],
      body: [
        [{
          text: "TO,",
          fontSize: 11
        }],
        [{
          
          text: res.customername,
          fontSize: 11,
          background: '#ffff00'
        }],
        [{
          
          text: res.customeraddress,
          fontSize: 11
        }],
        [{
          text: "Kind Attn : " + res.kind_attend,
          fontSize: 11
        }],
        [{
          text: "Sub           : " + res.subject,
           fontSize: 11
        }]
        

        
      ]
    }
  });

  content.push({
    text: 'Ref              : As per enquiry\n\n',
    fontSize: 10,
    alignment: 'justify'
  });

  content.push({
    text: '\n'
  });


  content.push({
    text: 'Conditions & Scope:',
    bold: true,
    fontSize: 11,
    background: '#ffff00'
  });

  content.push({
    text: conditionandscope,
    bold: true,
    fontSize: 11,
  });

  content.push({
    text: '\n'
  });

  content.push({
    text: 'Commercial:',
    bold: true,
    fontSize: 11,
    background: '#ffff00'
  });
 

  var table1 = {
        widths: [15,'*',30,50,50,30,'*'],
                    dontBreakRows: true,
                    fontSize: 20,
                    

        body: []
            };
          var particular=  [{text: 'SR NO',},{text: 'SERVICES NAME',alignment: 'center'},{text: 'RATE',alignment: 'center',},{text: 'UNIT',alignment: 'center',},{text: 'MIN. VALUE',alignment: 'center',},{text: 'GST%',alignment: 'center',},{text: 'DESCRIPTION',alignment: 'center',}];
           table1['body'].push(particular);     

           content.push({
        table: {
            headerRows: 1,
            // widths: ['*'],
            widths: [15,'*',30,50,50,30,'*'],
            alignment: 'center',

            body: [
              [],
               
            ]
        },
    });
    var cr=1;
    var commercial=res.json;
for(var i in commercial){
   var tb=[];
   tb.push(cr,{text:commercial[i].servicename,alignment: 'left'},{text:commercial[i].rate,alignment: 'center'},{text:commercial[i].unit,alignment: 'center'},{text:commercial[i].minval,alignment: 'center'},{text:commercial[i].gst,alignment: 'center'},{text:commercial[i].description,alignment: 'left'});
   table1['body'].push(tb);
   cr++;
}


content.push({
                        text: "",
                        fontSize: 20,
                        alignment: 'center'
                    }, {
                        style:'tablfont1',
                        table: table1
                    });

                    
content.push({
                        text: "\n",
                    });


 

content.push({
    text: 'Payment Terms & Condition:-:-',
    bold: true,
    fontSize: 11,
    background: '#ff0000'
  });

  content.push({
    text: payment_terms,
    bold: true,
    fontSize: 11,
  });


  content.push({
    text: '\nAuthorized By  \n\n'+res.autorised_by,
    bold: true,
    fontSize: 11,
  });

        dd = {
          pageSize: 'A4',
          pageOrientation: 'portrait',
          pageMargins: [40, 30, 30, 35],
          footer: function (currentPage, pageCount) {
            return {
              margin: 10,
              columns: [{
                fontSize: 8,
                text: [{
                    text: '--------------------------------------------------------------------------' +
                      '\n',
                    margin: [0, 20]
                  },
                  {
                    text: '© Pest Mortem (India) PVT. LTD | PAGE ' + currentPage.toString() + ' of ' + pageCount,
                  }
                ],
                alignment: 'center'
              }]
            };

          },
          content,
          styles: {
            tablfont: {
              fontSize: 7
            },
            tablfont1: {
              fontSize: 9
            }
          }
        }
        var win = window.open('', '_blank');
        pdfMake.createPdf(dd).open({}, win);
      }
    }
  })
}





</script>