<?php 

  $base    = '../../';

  $navenq3 = 'background:#1B1464;';

  include('header.php');

  if (!in_array('Job Allotment', $arrysession)) {
   
    echo "<script>window.location.href='/process/dashboard.php';</script>";
   
    exit;

}

  if (session_status() == PHP_SESSION_NONE) { session_start(); }

  $sessionby = $_SESSION['employeeid'];

  $branchid  = $_SESSION['branchid'];

?>

<br>

<img style="display:none;" id="imageid" src="<?php echo $base;?>img/PestMortemLogo.jpg">

<div class="table-ui container-fluid">

  <div class="tr row">
    
    <div class="col-sm-1 th" style="word-wrap:break-word;">Enquiry No.</div>  
    
    <div class="col-sm-2 th">Customer Info</div> 
    
    <div class="col-sm-2 th">Job Description</div>
    
    <div class="col-sm-1 th">Employee</div>
    
    <div class="col-sm-2 th">Received Batch details</div>
    
    <div class="col-sm-2 th">Schedule Date</div>
    
    <div class="col-sm-2 th">Action</div>
  
  </div>

  <?php

    $todaymill = strtotime($today)*1000;

    $threedaysbefore = $CURRENT_MILLIS+172800000;

    $result = mysqli_query($con,"SELECT * FROM workcontractservice WHERE servicedate<>0 AND (servicedate<'$threedaysbefore' OR servicedate = '$todaymill' ) AND branchid='$branchid' AND allot_by=0 AND allot_time=0 ORDER BY servicedate ASC");

    while($rows = mysqli_fetch_assoc($result)){

        $contractid     = $rows['contractid'];

        $serviceid      = $rows['serviceid'];

        $joballotstatus = $rows['joballotstatus'];

        $customerid = mysqli_fetch_assoc(mysqli_query($con,"SELECT customerid x FROM crmmaster WHERE wo_time<>0 AND wo_by<>0 AND contractid='$contractid'"))['x'];

        if($customerid){

          $cifo         = mysqli_fetch_assoc(mysqli_query($con,"SELECT * FROM customermaster WHERE customerid='$customerid'"));
          
          $category     = mysqli_fetch_assoc(mysqli_query($con,"SELECT category x FROM crmmaster WHERE wo_time<>0 AND wo_by<>0 AND contractid='$contractid'"))['x'];
      
          $contractid   = $rows['contractid'];

          $servicedate  = $rows['servicedate'];

          $servicename  = $rows['servicename'];

          $shedulestart = $rows['shedulestart'];

          $sheduleend   = $rows['sheduleend'];

          $empjson      = $rows['empjson'];

  ?>

  <div class="row tr">

    <div class="col-sm-1 td" style="word-wrap:break-word;">
    
      <?php echo $contractid; ?>
    
    </div> 

    <div class="col-sm-2 td">

      <?php
        
        echo 'Company : '.$cifo['customername']; 

        echo '<br>Address : '.get_object_vars(json_decode($cifo['customerjson']))['address']; 

        echo '<br>Name  : '.get_object_vars(json_decode($cifo['contactjson'])[0])['name'];

        echo '<br>Dept. : '.get_object_vars(json_decode($cifo['contactjson'])[0])['department'];

        echo '<br>Mobile : <a href="tel:'.get_object_vars(json_decode($cifo['contactjson'])[0])['mobile'].'">'.get_object_vars(json_decode($cifo['contactjson'])[0])['mobile'].'</a>';

        echo '<br>Landline'.get_object_vars(json_decode($cifo['contactjson'])[0])['landline'];

        echo '<br>Email : <a href="mailto:'.get_object_vars(json_decode($cifo['contactjson'])[0])['email'].'">'.get_object_vars(json_decode($cifo['contactjson'])[0])['email'].'</a>';

      ?>

    </div>   

    <div class="col-sm-2 td" style="word-wrap:break-word;">
      
      <?php

        echo 'Servcie Name : '.$category.'<br>';
        
        echo 'Servcie Name : '.$servicename.'<br>';

        $lastfollowup       = $servicedate;
        $followUpStatusFlag = '';

        if($lastfollowup!=0){

          if($START_OF_THE_DAY<$lastfollowup && $END_OF_THE_DAY<=$lastfollowup){

            $followUpStatusFlag = '#3F51B5';

          }

          if($START_OF_THE_DAY>$lastfollowup && $END_OF_THE_DAY>$lastfollowup){

            $followUpStatusFlag = '#F44336';

          }
          
          if($START_OF_THE_DAY<=$lastfollowup && $END_OF_THE_DAY>$lastfollowup ){

            $followUpStatusFlag = '#4CAF50';

          }

        } else {

          $followUpStatusFlag = '#ff9800';

        }

        echo 'Servcie Date : <p style="background:'.$followUpStatusFlag.';color:#fff;">'.date("d-m-Y",$servicedate/1000).'</p>'; 

        echo 'Contract No. : '.$contractid.'<br>';

        echo 'Servcie No. : '.$serviceid; 

      ?>

    </div>

    <div class="col-sm-1 td">

      <?php 
      
        $results  = mysqli_query($con,"SELECT DISTINCT employeename FROM job_allotment WHERE contractid = '$contractid' AND branchid='$branchid'");

        $rowcount = mysqli_num_rows($results);

        while($rowss = mysqli_fetch_assoc($results)){

          if ($rowcount >= 1 ) {

            echo $rowss['employeename'];


          } else if ($rowcount < 1) {
            
            echo $rowss['employeename'].',';

          }
        
        }
       
      ?>

    </div>  

    <div class="col-sm-2 td">

      <br>

      <table class="table-list table" id="table-customer">

        <thead>

          <tr>

                <!-- <th rowspan="2">SR NO</th> -->
            <th rowspan="2">Item Name</th>

            <th rowspan="2">Qty</th>

          </tr>

        </thead> 

        <?php

      
          $results  = mysqli_query($con,"SELECT itemname,allot_qty FROM job_allotment WHERE contractid = '$contractid' AND branchid='$branchid'");

          while($rowsss = mysqli_fetch_assoc($results)){

            echo '<tr>';
                
              echo '<td>'.$rowsss['itemname'].'</td>';
              
              echo   '<td>'.$rowsss['allot_qty'].'</td>';
              
            echo '</tr>';

          }


          // $result1 = mysqli_query($con,"SELECT * FROM batchnos WHERE  branchid='$branchid' AND serviceid LIKE '%$serviceid%' ORDER BY id DESC");

          // while($rowss = mysqli_fetch_assoc($result1)){

          //   $itemid      = $rowss['itemid'];

          //   $servicejson = json_decode('['.$rowss['serviceid'].']');

          //   $itemname    = mysqli_fetch_assoc(mysqli_query($con,"SELECT itemname x FROM stockmaster WHERE stockid='$itemid' "))['x'];
            
          //   echo '<tbody>';  

          //   foreach($servicejson as $i){

          //     $serviceid1 = get_object_vars($i)['serviceid'];
              
          //     if($serviceid==$serviceid1){

          //       echo '<tr>';
                
          //         echo '<td>'.$itemname.'</td>';
                  
          //         echo   '<td>'.$rowss['batchno'].'</td>';
                  
          //         echo   '<td>'.get_object_vars($i)['issue'].'</td>';

          //       echo '</tr>';

          //     }

          //   }
              

          //   echo'</tbody>'; 

          // }

        ?>

      </table>

    </div> 

    
    <div class="col-sm-2 td">
    
      <?php

        if($shedulestart!=0){

          echo date("d-m-Y",$shedulestart/1000).' To '.date("d-m-Y",$sheduleend/1000);

        }

      ?>

    </div>

    <div class="col-sm-2 td">

      <?php if ($joballotstatus == 1) { ?>
          
        <button class="btn btn-sm btn-block btn-success" data-serviceid="<?php echo $serviceid; ?>" onclick="joballowted(this)">Confirm</button>
      
      <?php } ?>

      <a class="btn btn-sm btn-block btn-primary"  href="/process/crm/material-issue.php?serviceid=<?php echo $serviceid; ?>&servicedatetimestap=<?php echo $servicedate; ?>&servicedate=<?php echo date("d F, Y",$servicedate/1000);?>&category=<?php echo $category;?>&contractid=<?php echo $contractid;?>&joballotstatus=<?php echo $joballotstatus;?>">Job Shedule & Material Issue</a>
      
      <!-- <a class="btn btn-sm btn-block btn-primary" href="/process/stock/material-issue.php?serviceid=<?php echo $serviceid; ?>">Material Issue</a> -->

      <?php

        if ($category == 'Pest Control') {

          $servicelocation = mysqli_fetch_assoc(mysqli_query($con,"SELECT servicelocation x FROM workcontract WHERE contractid='$contractid'"))['x'];

          $servicelocation = json_decode($servicelocation);

          foreach ($servicelocation as $i){

            $location = get_object_vars($i)['servicelocation'];  
            
            echo '<button class="btn btn-sm btn-primary" data-contractid="'.$contractid.'" data-location="'.$location.'" data-serviceid="'.$serviceid.'" onclick="servicepdf(this)"> Service Pdf</button>';

          }


        }

      ?>
    
    </div> 

  </div>

  <?php } } ?>

</div>

<div id="myModal1" class="modal fade" role="dialog">

  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">

      <div class="modal-header">

        <h4 class="modal-title" align="center">Job Allotment</h4>

      </div>

      <div class="modal-body">

        <div id="dummymodalid">
        
          <input type="hidden" data-name="serviceid" id="inp-serviceid">

        </div>

        <div class="row form-group">

        <div class="col-sm-6" id="dummymodalid">
          
          Start Date :<input type="text" data-role="text" class="datepicker form-control input-sm" id="startdate" data-name="startdate" />

        </div>

        <div class="col-sm-6" id="dummymodalid">
          
          End Date :<input type="text" data-role="text" class="datepicker form-control input-sm" id="enddate" data-name="enddate" />
        
        </div>

        <div class="col-sm-12">

          <br>
          
          <table class="table-list" id="jobalotment-table" style="width:100%!important;">

            <thead>
            
              <tr>

                <th>Sr. No.</th>

                <th>Employee Name</th>

                <th>Action</th>

              </tr>

            </thead>

            <tbody>

              <tr>
                
                <td></td>

                <td>
                    
                  <select data-role="select" data-name='employeeid' onchange="empchange(this)" class="form-control input-sm select-js">
                  
                    <option value="Select">Select</option>
                    
                    <?php 
                    
                      $result1 = mysqli_query($con,"SELECT employeeid,username FROM employeemaster WHERE branchid='$branchid' AND role = 'employee' OR role = 'None' ");

                      while($rows = mysqli_fetch_assoc($result1)){
                 
                        echo '<option value="'.$rows['employeeid'].'">'.$rows['username'].'</option>';

                      }

                    ?>

                  </select>

                </td>

                <td style="display:none;">
                  
                  <input type="text" data-role="text" class="form-control input-sm" id='enpname' data-name="employeename" readonly />
                
                </td>

                <td align="center">
                  
                  <button class="btn btn-primary btn-sm" onclick="modaldata()">Add+</button>
                
                </td>
              
              </tr>

            </tbody>

          </table>

          <br>

          <div class="col-sm-4"></div>

          <div class="col-sm-4">
            
            <button class="btn btn-sm btn-success btn-block" onclick="submitdata()">Submit</button>
          
          </div>

        </div>

      </div>

    </div>

  </div>

</div>

<?php 

  include('footer.php');

?>
<script>

function jobshedule(e) {

  let serviceid = $(e).attr('data-serviceid');

  $('#inp-serviceid').val(serviceid);

  $('table#jobalotment-table tr#abc').remove();

  if (serviceid != '') {

    $.ajax({

      type: "POST",
      
      data: 'serviceid='+serviceid,
      
      url: 'api/joballowtedselect.php',
      
      cache: false,
      
      success: function (res) {

        $('#myModal1').modal('show');
      
        if (res.status == 'success') {

          $('#startdate').val(res.shedulestartdate);

          $('#startdate').attr('data-stamp',res.shedulestart);

          $('#enddate').val(res.sheduleenddate);

          $('#enddate').attr('data-stamp',res.sheduleend);

          let json = res.empjson;

          let str = '';

          let count = 1;

          for (let i in json) {

            str +='<tr id="abc">';

              str+='<td>'+count+'</td>';

              str+='<td class="employeeid" style="display:none;">'+json[i].employeeid+'</td>';

              str+='<td class="employeename">'+json[i].employeename+'</td>';

              str+='<td align="center"><button class="btn btn-sm btn-danger" onclick="remover(this)">R</button></td>';

            str +='</tr>'; 

            count++;
            
          }

         $('#jobalotment-table tr:last ').after(str);

        } else {

          let servicedatetimestap = $(e).attr('data-servicedatetimestap');

          let servicedate         = $(e).attr('data-servicedate');

          $('#startdate').val(servicedate);

          $('#startdate').attr('data-stamp',servicedatetimestap);
          
          $('#enddate').val(servicedate);

          $('#enddate').attr('data-stamp',servicedatetimestap);

        }
    
      }
    
    })
    
  } else {

    alert('Service Id Missing');

  }

}

function modaldata() {

  var valid = true;

  if (checker('jobalotment-table') != false) {

    valid = valid * true;

  } else {

    valid = valid * false;

  }

  if (valid) {

    var data = checker('jobalotment-table');

    modals.clear('jobalotment-table');

    var len = $('#jobalotment-table > tbody > tr').length;

    var str = '<tr>';

      str += '<td>' + len + '</td>';

      str += '<td class="employeeid" style="display:none;">' + data.employeeid + '</td>';

      str += '<td class="employeename">' + data.employeename + '</td>';

      str += '<td align="center"><button class="btn btn-sm btn-danger" onclick="remover(this)">R</button></td>';

    str += '</tr>';

    $('#jobalotment-table > tbody').append(str);

  }

}

function joballowted(e) {

  var serviceid = $(e).data('serviceid');

  $.ajax({

    type: "POST",

    data: 'serviceid=' + serviceid,

    url: 'api/joballowted.php',

    cache: false,

    success: function (res) {

      if (res.status == 'success') {

        window.location.reload();

      }

    }
    
  })

}


function submitdata() {

  var valid = true;

  if (checker('dummymodalid') != false) {

    valid = valid * true;

  } else {

    valid = valid * false;

  }

  if (modals.tabletdbyclass('jobalotment-table') != false) {

    valid = valid * true;

  } else {

    valid = valid * false;

  }

  if (valid) {

    var data1     = checker('dummymodalid');

    var startdate = data1.startdate;
    
    var enddate   = data1.enddate;
    
    var serviceid = data1.serviceid;
    
    var data      = modals.tabletdbyclass('jobalotment-table');

    data.shift();

    data = JSON.stringify(data);

    $.ajax({

      type: "POST",

      data: {
        data: data,
        startdate: startdate,
        serviceid: serviceid,
        enddate: enddate
      },

      url: 'api/joballotment.php',

      cache: false,

      success: function (res) {

        if (res.status == 'success') {

          window.location.reload();

        }

      }

    })

  }

}

function empchange(e) {

  $('#enpname').val($(e).find('option:selected').text().trim());

}

$('.datepicker').pickadate({

  selectYears: true,

  selectMonths: true,

  onClose: function () {

    try {

      var dateStamp = this.get('select')['pick'];
      
      var id        = this.get('id');
      
      $('#' + id).attr('data-stamp', dateStamp);
      
      $('#' + id).parent().find('.timepicker').click();
      
    } catch (err) {

      console.log(err);

    }

  }

});


function getBase64Image(img) {

  var canvas    = document.createElement("canvas");
  
  canvas.width  = img.width;
  
  canvas.height = img.height;
  
  var ctx       = canvas.getContext("2d");
    
  ctx.drawImage(img, 0, 0);
  
  var dataURL = canvas.toDataURL("image/png");
    
  return dataURL.replace('/^data:image\/(png|jpg);base64,/', "");

}


function servicepdf(e) {

  var contractid = $(e).data('contractid');

  var location   = $(e).attr('data-location');

  var serviceid   = $(e).attr('data-serviceid');

  $.ajax({

    type: "POST",

    data: "contractid=" + contractid + "&serviceid=" + serviceid,

    url: 'api/servicepdf1.php',

    cache: false,

    success: function (res) {

      if (res.status == 'success') {

        var content = [];
        
        var json    = res.json[0];
        
        var base64 = getBase64Image(document.getElementById("imageid"));
         
        content.push( {table:	{widths: ['auto','*'],	body: [
  
          [
            {rowSpan: 3,  
            stack: [
              {image:base64,
                      
                width: 75,
                
                height:40,
                
                margin:[0,10],
                
              },]}, 
            {text: 'PEST MORTEM (INDIA) PRIVATE LIMITED',style: 'header'}],
            
            ['', {text:'(Approved by National Plant Protection Organization, Goverment of India)',style: 'subheader'}],
            
            ['', {text:'AN ISO 9001 : 2008 CERTIFIED COMPANY',style: 'subheader'}],
        ]
    
      }, layout: 'noBorders',  });

      content.push( {table:	{widths: [70,'auto','*'],

        body: [

          [{text:''  ,style: 'subheader1',bold:true,border:[false,false,false,false]},{text:'REGD. OFF    : '  ,style: 'subheader1',bold:true,border:[false,false,false,false]},{text:json.address.toUpperCase(),style: 'subheader1',bold:true,border:[false,false,false,false]}],

          [{text:''  ,style: 'subheader1',bold:true,border:[false,false,false,false]},{text:'Phone            : '  ,style: 'subheader1',bold:true,border:[false,false,false,false]},{text:'+91-22-2412 7935 / 2411  1976/2414 7425',style: 'subheader1',bold:true,border:[false,false,false,false]}],
          
          [{text:''  ,style: 'subheader1',bold:true,border:[false,false,false,false]},{text:'FAX                : '  ,style: 'subheader1',bold:true,border:[false,false,false,false]},{text:'+91-22-2415 0261',style: 'subheader1',bold:true,border:[false,false,false,false]}],
          
          [{text:''  ,style: 'subheader1',bold:true,border:[false,false,false,false]},{text:'E-MAIL          : '  ,style: 'subheader1',bold:true,border:[false,false,false,false]},{text:'pestmortem@pestmortem.com',style: 'subheader1',bold:true,border:[false,false,false,false]}],
          
          [{text:''  ,style: 'subheader1',bold:true,border:[false,false,false,false]},{text:'WEBSITE       : '  ,style: 'subheader1',bold:true,border:[false,false,false,false]},{text:'http://www.pestmortem.com',style: 'subheader1',bold:true,border:[false,false,false,false]}],        
          
        ]}  
        
      }); 

      content.push({text:'\n'});
      
      content.push( {table:	{widths: ['*'],body: [[{text:' PEST MANAGEMENT SERVICE RECORD',alignment:'center',fontSize: 10,bold:true}],]}  }); 
      
      content.push({text:'\n'});
      
      content.push( {table:	{widths: [100,'*'],
        
        body: [
        
          [{text:'Name of Client        : '  ,fontSize: 9,bold:true,border:[false,false,false,false]},{text:json.companyname.toUpperCase(),fontSize: 9,bold:true,border:[false,false,false,false]}],
        
          [{text:'Address of Client    : '  ,fontSize: 9,bold:true,border:[false,false,false,false]},{text:json.companyaddress.toUpperCase(),fontSize: 9,bold:true,border:[false,false,false,false]}],
        
          [{text:'Contract No             : '  ,fontSize: 9,bold:true,border:[false,false,false,false]},{text:json.contractid,fontSize: 9,bold:true,border:[false,false,false,false]}],
        
          [{text:'Contract Date          : '  ,fontSize: 9,bold:true,border:[false,false,false,false]},{text:json.contractdate,fontSize: 9,bold:true,border:[false,false,false,false]}],         
        
        ]}  
      
      }); 
        
      
      if(json.ftype==''){
  
        var servicelocation = json.servicelocation;
          
        if (servicelocation=='Same as Purchaser Address') {

           
          var servicelocation1 = json.companyaddress;
          
        } else {

          
          var servicelocation1 = json.servicelocation;
           
        }
          content.push( {table:	{widths: [100,'*'],body: [
            [{text:'Service Location     : '  ,fontSize: 9,bold:true,border:[false,false,false,false]},{text:servicelocation1.toUpperCase(),fontSize: 9,bold:true,border:[false,false,false,false]}],
          ]}  });  
        } else{

          content.push( {table:	{widths: [100,'*'],body: [
            [{text:'Service Location     : '  ,fontSize: 9,bold:true,border:[false,false,false,false]},{text:location.toUpperCase(),fontSize: 9,bold:true,border:[false,false,false,false]}],
          ]}  }); 
        }
       
        var table1 = {
          widths: ['auto','*','auto','auto','auto'],
          dontBreakRows: true,
          body: []
        };
        var header=['Date','Service Performed','Infestation Status*','Name of Operator','Customer Signature'];
        table1['body'].push(header);
        var services=json.servicejson;

        for(var i in services){
          var arr=[];
          arr.push(services[i].servicedate,services[i].servicename,'','','');
            table1['body'].push(arr);
     
        }
        

        content.push({ columns: [
          {
            style: 'tablfont',
            table: table1,
            fontSize: 8,
            bold:true
            
            // layout: 'noBorders'
          }
        ]})

        

  
        
        dd = {
          pageSize: 'A5',
          pageOrientation: 'portrait',
          // pageOrientation: 'landscape',
          pageMargins: [40, 30, 30, 35],
          footer: function (currentPage, pageCount) {
            return {
              margin: 10,
              columns: [{
                fontSize: 9,
                text: [{
                    text: '--------------------------------------------------------------------------' +
                      '\n',
                    margin: [0, 20]
                  },
                  {
                    text: '© Pest Mortem (India) PVT. LTD. | PAGE ' + currentPage.toString() + ' of ' + pageCount,
                  }
                ],
                alignment: 'center'
              }]
            };

          },
          content,
          styles: {
            tablfont: {
              fontSize: 8
            },
            tablfont1: {
              fontSize: 8
            },
            header: {
                            fontSize: 14,
                            bold: true,
                            alignment: 'center',
                          },
                            subheader: {
                            fontSize: 8,
                            bold: true,
                            alignment: 'center',
                          }, 
                          subheader1: {
                            fontSize: 8,
                            bold: true,
                            alignment: 'left',
                          },
                           subheader2: {
                            fontSize: 10,
                            bold: true,
                            
                          },
          }
        }
        var win = window.open('', '_blank');
        pdfMake.createPdf(dd).open({}, win);

      }
    }
  })
}

</script>