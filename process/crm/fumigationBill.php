<?php

    $base    = '../../';

    $navenq6 = 'background:#1B1464;';

    include('header.php');

    if (session_status() == PHP_SESSION_NONE) { session_start(); }
    $sessionby = $_SESSION['employeeid'];
    $branchid  = $_SESSION['branchid'];

    $branchjson=json_decode(mysqli_fetch_assoc(mysqli_query($con,"SELECT branchjson x FROM branchmaster WHERE branchid='$branchid'"))['x']);

    if($_GET['invoice_bill']){

        $invoice_bill = $_GET['invoice_bill'];

    }

    if($_GET['customerid']){

        $customerid = $_GET['customerid'];

    }

    if($_GET['edit']){

        $edit = $_GET['edit'];

    }
                    
    $cifo  = mysqli_fetch_assoc(mysqli_query($con,"SELECT * FROM customermaster WHERE customerid='$customerid'"));

    if ($cifo == '') {

        $customer = $customerid;
        
    } else {

        $customer    = $cifo['customername'];
        $customeradd = get_object_vars(json_decode($cifo['customerjson']))['address'];
        $gst         = get_object_vars(json_decode($cifo['customerjson']))['gstnumber'];
        $email       = get_object_vars(json_decode($cifo['customerjson']))['emailId'];
        $mobile      = get_object_vars(json_decode($cifo['contactjson'])[0])['mobile'];


    } 
    
   
?>

    <style>
    
    .table-bordered>tbody>tr>td, .table-bordered>tbody>tr>th, .table-bordered>tfoot>tr>td, .table-bordered>tfoot>tr>th, .table-bordered>thead>tr>td, .table-bordered>thead>tr>th {
        border: 2px solid #000000;
    }
    </style>

    <br>

    <input type="hidden" value="<?php echo $branchid;?>" id="branchId">

    <input type="hidden" value="<?php echo $invoice_bill;?>" id="invoice_bill">

    <input type="hidden" value="<?php echo $edit;?>" id="edit">



    <div class="col-sm-2"></div>

    <div class="col-sm-8" id="pestBill" style="background:#ddd;">

        <div class="row">

            <div class="col-sm-12" align="center">
                
                <h3><b>PEST MORTEM (INDIA) PRIVATE LIMITED</b></h3>

                <h6><b>(Approved by National Plant Protection Organization, Government of India)</b></h6>

                <h6><b>AN ISO 9001 : 2008 CERTIFIED COMPANY</b></h6>

                <h5><b><?php echo get_object_vars($branchjson)['address']; ?></b></h5>

                <h5><b><?php echo 'GST NO : ' .get_object_vars($branchjson)['gstnumber']; ?></b></h5>

                <h4><b>EXPERT IN</b></h4>

                <h6><b>PEST CONTROL SERVICES AND FUMIGATION OF EXPORT CARGO, SHIPS, CONTAINERS</b></h6>


                <hr style="border:1px solid #000;">


            </div>

            <div class="col-md-5">

                <div class="form-group" style="padding-top:6px;">

                    <label class="control-label" for="email"><?php echo 'Bill No : ' .$invoice_bill;?></label>

                </div>
   
            </div>

            <div class="col-md-7">

                <div class="pull-right form-horizontal">

                    <div class="form-group">

                        <label class="control-label col-sm-5" for="email"><b>Date of issue : </b></label>
                        
                        <div class="col-sm-7">
                            
                            <input type="date"  data-role="date" data-name="issuedate" class="form-control" value="<?php echo $issuedate; ?>">
                        
                        </div>
                    
                    </div>

                </div>
                
            </div>

            <table class="table table-bordered" style="border: 2px solid #000;">

                <tr>

                    <th style="text-align: center;font-weight:800;">
                        Regd. Offce : G -2, Sunder Tower, T.J. Road, Sewree(west),Mumbai - 400015
                        <br>Phone : 24147425/24127935/24111976/24149566/65531746/47 . Fax : 91-22-24150261 .
                        <br>E-Mail : pestmort@bom3.vsnl.net.in . pestmortem@pestmortem@pestmortem.com. Website : www.pestmortem.com
                    </th>

                </tr>

            </table>

            <div class="col-sm-6">

                <div class="table-responsive"> 

                    <table class="table table-bordered" style="border: 2px solid #000;">

                        <thead>

                            <tr>

                                <th style="text-align: center;font-weight:800;">Purchaser</th>

                            </tr>

                        </thead>

                    </table>

                </div>

                <div class="form-group form-horizontal">

                    <label class="control-label col-sm-3" for="name"><b>Name : </b></label>

                    <div class="col-sm-9">
                    
                        <input type="text" data-name="custName" data-role="text" class="form-control" value="<?php echo $customer; ?>">

                    </div>

                </div>

                <div class="form-group form-horizontal">

                    <label class="control-label col-sm-3" for="name"><b>Address :</b></label>

                    <div class="col-sm-9">

                        <textarea type="text" data-name="custAddress" data-role="text" class="form-control"><?php echo $customeradd; ?></textarea>

                    </div>

                </div>


                <div class="form-group form-horizontal">

                    <label class="control-label col-sm-3" for="name"><b>GST : </b></label>

                    <div class="col-sm-9">

                        <input type="text" data-name="custGst" data-role="text" class="form-control" value="<?php echo $gst; ?>">

                    </div>

                </div>

                <div class="form-group form-horizontal">

                    <label class="control-label col-sm-3" for="name"><b>Email : </b></label>

                    <div class="col-sm-9">

                        <input type="text" data-name="custEmail" data-role="text" class="form-control" value="<?php echo $email; ?>">

                    </div>

                </div>

                <div class="form-group form-horizontal">

                    <label class="control-label col-sm-3" for="name"><b>Phone : </b></label>

                    <div class="col-sm-9">

                        <input type="text" data-name="custmobile" data-role="text" class="form-control" value="<?php echo $mobile; ?>">

                    </div>

                </div>

            
            </div>

            <!-- <div class="col-sm-1"></div> -->

            <div class="col-sm-6">

                <div class="table-responsive"> 

                    <table class="table table-bordered" style="border: 2px solid #000;">

                        <thead>

                            <tr>

                                <th style="text-align: center;font-weight:800;">Premises</th>

                            </tr>

                        </thead>

                    </table>

                </div>

                <div class="form-group form-horizontal">

                    <label class="control-label col-sm-3" for="name"><b>Name : </b></label>

                    <div class="col-sm-9">
                    
                        <input type="text" data-name="premisesName" data-role="text" class="form-control" value="<?php echo $customer; ?>">

                    </div>

                </div>

                <div class="form-group form-horizontal">

                    <label class="control-label col-sm-3" for="name"><b>Address :</b></label>

                    <div class="col-sm-9">

                        <textarea type="text" data-name="premisesAddress" data-role="text" class="form-control">Same as Purchaser Address </textarea>

                    </div>

                </div>


                <div class="form-group form-horizontal">

                    <label class="control-label col-sm-3" for="name"><b>GST : </b></label>

                    <div class="col-sm-9">

                        <input type="text" data-name="premisesGst" data-role="text" class="form-control">

                    </div>

                </div>

                <div class="form-group form-horizontal">

                    <label class="control-label col-sm-3" for="name"><b>Email : </b></label>

                    <div class="col-sm-9">

                        <input type="text" data-name="premisesEmail" data-role="text" class="form-control">

                    </div>

                </div>

                <div class="form-group form-horizontal">

                    <label class="control-label col-sm-3" for="name"><b>Phone : </b></label>

                    <div class="col-sm-9">

                        <input type="text" data-name="premisesmobile" data-role="text" class="form-control">

                    </div>

                </div>

            
            </div>

            <hr style="border:1px solid #000; width:95%;">
            
            <div class="col-sm-12" style="margin-top: 10px;">

                <table class="table table-bordered" style="border: 2px solid #000;">

                    <thead>

                        <tr>

                            <th style="text-align: center;font-weight:800;">TYPE OF SERVICES</th>

                        </tr>

                    </thead>

                </table> 

                <table class="table-list table" id="section1">

                    <thead>

                        <th>Service Name</th>

                        <th>SAC</th>

                        <th>AMOUNT</th>

                        <th>Action</th>

                    </thead>

                    <tbody>
                        <tr>

                            <td>

                                <select class="form-control input-sm"  id="servicename" data-name="servicename">

                                    <option value="Select">Select</option>

                                    <?php

                                        $result5=mysqli_query($con,"SELECT serviceid,servicename FROM servicemaster WHERE category='Fumigation' ORDER BY id ASC");

                                            while($rows1 = mysqli_fetch_assoc($result5)){  

                                                echo '<option value="'.$rows1['serviceid'].'">'.strtoupper($rows1['servicename']).'</option>';

                                            }
                                    ?>

                                </select>

                            </td>

                            <td>

                                <input data-name="sac" class="form-control input-sm" type="number"/>

                            </td>

                            <td>

                                <input data-name="amounts" class="form-control input-sm" type="number"/>

                            </td>

                            <td>

                                <button class="btn btn-sm btn-primary" onclick="btnenq(this)">Add</button>

                            </td>

                        </tr>

                    </tbody>

                </table>

            </div>

            <div class="col-sm-12">

                <table class="table table-bordered" style="border: 2px solid #000;">

                    <tr>

                        <th style="text-align: center;font-weight:800;" colspan="9">PAYMENT DETAILS </th>

                    </tr>

                    <tr>
                        <td>

                            <div class="form-group form-horizontal">

                                <label class="control-label col-sm-4" for="name"><b>Mode of payment :</b></label>

                                <div class="col-sm-8">

                                    <select class="form-control input-sm" data-role="select" data-name="modOfPayment">

                                        <option value="Select">Select</option>

                                        <option value="Cash">Cash</option>

                                        <option value="Cheque">Cheque</option>

                                    </select>  

                                </div>

                            </div>
                                
                        </td>

                            <td colspan="2" style="text-align:center;"><b> SERVICE CHARGES </b></td>

                    </tr>

                    <tr>
                        <td rowspan="5">

                            <div class="form-group form-horizontal">

                                <label class="control-label col-sm-4" for="name"><b>Additional Declaration (if any)</b></label>

                                <div class="col-sm-8">

                                    <textarea type="text" data-role="text" data-name="addDeclaration" cols="30" rows="5"></textarea>

                                </div>

                            </div>
                                
                        </td>

                            <td>

                                <b>Amount</b>

                            </td>

                            <td>

                                <input type="number" data-role="number" data-name="amount" class="form-control input-sm" id="amount" onkeyup="netAmounts();" readonly/>
                                
                            </td>

                    </tr>

                    <tr>

                        <td>

                            <div class="form-group form-horizontal">

                                <label class="control-label col-sm-4" for="name"><b>CGST %</b></label>

                                <div class="col-sm-8">

                                    <input type="number" data-role="number" class="form-control input-sm" data-name="cgstPer" id="cgstPer" onkeyup="netAmounts();">

                                </div>

                            </div>

                        </td>

                        <td>

                            <input type="number" data-role="number" data-name="cgstAmt" class="form-control input-sm" id="cgstAmt" onkeyup="netAmounts();" readonly/>
                            
                        </td>

                    </tr>

                    
                    <tr>

                        <td>

                            <div class="form-group form-horizontal">

                                <label class="control-label col-sm-4" for="name"><b>SGST %</b></label>

                                <div class="col-sm-8">

                                    <input type="number" data-role="number" class="form-control input-sm" data-name="sgstPer" id="sgstPer" onkeyup="netAmounts();">

                                </div>

                            </div>

                            </td>

                            <td>

                            <input type="number" data-role="number" data-name="sgstAmt" class="form-control input-sm" id="sgstAmt" onkeyup="netAmounts();" readonly/>

                        </td>

                    </tr>

                    <tr>

                        <td>

                            <div class="form-group form-horizontal">

                                <label class="control-label col-sm-4" for="name"><b>IGST %</b></label>

                                <div class="col-sm-8">

                                    <input type="number" data-role="number" class="form-control input-sm" data-name="igstPer" id="igstPer" onkeyup="netAmounts();">

                                </div>

                            </div>

                            </td>

                            <td>

                            <input type="number" data-role="number" data-name="igstAmt" class="form-control input-sm" id="igstAmt" onkeyup="netAmounts();" readonly/>

                        </td>

                    </tr>

                    
                    <tr>

                        <td>

                            <b>Net Amount </b>

                        </td>

                        <td>

                            <input type="number" data-role="number" data-name="netAmount" id="netAmount" class="form-control input-sm" readonly/>
                            
                        </td>

                    </tr>

                </table> 
            
            </div>

            <hr style="border:1px solid #000; width:98%;">

            <table class="table table-bordered" style="border: 2px solid #000;">

                <tr>

                    <th style="text-align: center;font-weight:800;" colspan="9">BANK DETAILS </th>

                </tr>

            </table>

            <div class="col-sm-6"> 

                <div class="form-group form-horizontal">

                    <label class="control-label col-sm-4" for="name"><b>Bank Name : </b></label>

                    <div class="col-sm-8">

                        <input type="text" data-role="text" data-name="bankName" class="form-control input-sm" value="<?php echo get_object_vars($branchjson)['bankname']; ?>"/>

                    </div>

                </div>

                <div class="form-group form-horizontal">

                    <label class="control-label col-sm-4" for="name"><b>Branch Name : </b></label>

                    <div class="col-sm-8">

                        <input type="text" data-role="text" data-name="branchName" class="form-control input-sm" value="<?php echo get_object_vars($branchjson)['bankbranchname']; ?>"/>

                    </div>

                </div>
            
            </div>

            <div class="col-sm-6"> 

                <div class="form-group form-horizontal">

                    <label class="control-label col-sm-4" for="name"><b>IFSC Code  : </b></label>

                    <div class="col-sm-8">

                        <input type="text" data-role="text" data-name="ifscCode" class="form-control input-sm" value="<?php echo get_object_vars($branchjson)['ifsccode']; ?>"/>

                    </div>

                </div>

                <div class="form-group form-horizontal">

                    <label class="control-label col-sm-4" for="name"><b>Account No : </b></label>

                    <div class="col-sm-8">

                        <input type="text" data-role="text" data-name="accountNo" class="form-control input-sm" value="<?php echo get_object_vars($branchjson)['accountno']; ?>"/>

                    </div>

                </div>
            
            </div>

        </div>

    </div>

    <button class="btn btn-lg btn-primary" onclick="saveBill();" id="savecertificate" style="border-radius:0px;position:fixed;bottom:10px;right:10px;">Save Fumigation Bill</button>


<?php 
include('footer.php');
?>

<script>

function netAmounts() {

    var amount  = parseFloat($('#amount').val());
    var cgstPer = parseFloat($('#cgstPer').val());
    var sgstPer = parseFloat($('#sgstPer').val());
    var igstPer = parseFloat($('#igstPer').val());
    var cgstAmt = parseFloat($('#cgstAmt').val());
    var sgstAmt = parseFloat($('#sgstAmt').val());
    var igstAmt = parseFloat($('#igstAmt').val());

    var cgstAmount = (amount * cgstPer) / 100;
    var sgstAmount = (amount * sgstPer) / 100;
    var igstAmount = (amount * igstPer) / 100;


    if (isNaN(cgstAmount)) {

        cgstAmount = 0;

    }
    if (isNaN(sgstAmount)) {

        sgstAmount = 0;

    }
    if (isNaN(igstAmount)) {

        igstAmount = 0;

    }

    var netAmount = amount + cgstAmount + sgstAmount + igstAmount;
    
    $('#cgstAmt').val(cgstAmount);
    $('#sgstAmt').val(sgstAmount);
    $('#igstAmt').val(igstAmount);
    $('#netAmount').val(netAmount)



}

function btnenq() {

    var valid = true;

    if (checker('section1') != false) {

        valid = valid * true;

    } else {

        valid = valid * false;

    }

    if (valid) {

        var itemname = $('#servicename').find(":selected").text();
        var sum = 0;
        var data = checker('section1');
        var str = '<tr>';
        str += '<td class="serviceid" style="margin:0;padding:0;display:none;" >' + data.servicename + '</td>';
        str += '<td class="servicename" >' + itemname + '</td>';
        str += '<td style="margin:0;padding:0;" class="sac">' + data.sac + '</td>';
        str += '<td style="margin:0;padding:0;" class="amount">' + data.amounts + '</td>';
        str += '<td><button class="btn btn-sm btn-danger" data-serviceAmount=' + data.amounts + ' onclick="remover(this)">R</button></td>';
        str += '</tr>';

        $('#section1 > tbody').append(str);

        modals.clear('section1');

        $('#section1 .amount').each(function () {

            sum += parseFloat($(this).text().trim());

        });

        $('#amount').val(sum);

    }
    netAmounts();

}

function remover(e) {

    $(e).parent().parent().remove();

    var serviceAmount = parseFloat($(e).attr('data-serviceAmount'));

    var amountss = parseFloat($('#amount').val());

    var sum = amountss - serviceAmount;

    $('#amount').val(sum);

    netAmounts();
}

function saveBill() {

    var arr = [];


    $('#section1 > tbody > tr').each(function () {

        var serviceid = $(this).find('.serviceid').text().trim();
        var servicename = $(this).find('.servicename').text().trim();
        var sac = $(this).find('.sac').text().trim();
        var amount = $(this).find('.amount').text().trim();

        if (servicename != '') {

            arr.push({

                "serviceid": serviceid,

                "servicename": servicename,

                "sac": sac,

                "amount": amount

            });

        }

    })

    //    arr1.shift();

    var valid = true;

    if (checker('pestBill') != false) {

        valid = valid * true;

    } else {

        valid = valid * false;

    }

    if (valid) {

        var data = checker('pestBill');
        var datastr = JSON.stringify(data);
        var serviceDetails = JSON.stringify(arr);

        $.ajax({

            type: "POST",

            data: {

                data: datastr,
                serviceDetails: serviceDetails,
                branchId: $('#branchId').val(),
                invoice_bill: $('#invoice_bill').val(),

            },

            url: 'api/fumigationSaveBill.php',

            cache: false,

            success: function (res) {

                if (res.status == 'success') {

                    swal({

                        type: 'success',
                        title: 'Fumigation Bill Create',
                        showConfirmButton: false,
                        timer: 1000

                    });

                    setTimeout(function () {

                        location.href = '/process/crm/Manual-biling.php';

                    }, 1000);

                }

            }

        })

    }

}



if ($('#branchId').val() != '' && $('#invoice_bill').val() != '' && $('#edit').val() == 'true') {

    $.ajax({

        type: "POST",

        data: "branchId=" + $('#branchId').val() + "&invoice_bill=" + $('#invoice_bill').val(),

        url: 'api/fumigationBillSelect.php',

        cache: false,

        success: function (res) {

            if (res.status == 'success') {

                modals.putvalue('pestBill', res.json);

                $('#savecertificate').attr('onclick', 'saveBill()');

                $('#savecertificate').text('Update Pest Control Bill');

                var enquipmentjson = res.json[0].service_json;

                for (var i = 0; i < enquipmentjson.length; i++) {

                    var str = '<tr>';

                    str += '<td class="serviceid" style="margin:0;padding:0;display:none;" >' + enquipmentjson[i].serviceid + '</td>';

                    str += '<td class="servicename" >' + enquipmentjson[i].servicename + '</td>';

                    str += '<td style="margin:0;padding:0;" class="sac">' + enquipmentjson[i].sac + '</td>';

                    str += '<td style="margin:0;padding:0;" class="amount">' + enquipmentjson[i].amount + '</td>';

                    str += '<td><button class="btn btn-sm btn-danger" data-serviceAmount=' + enquipmentjson[i].amount + ' onclick="remover(this)">R</button></td>';

                    str += '</tr>';

                    $('#section1 > tbody').append(str);
                }
                netAmounts();
            }

        }

    });

}

</script>