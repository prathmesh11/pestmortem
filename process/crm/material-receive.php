<?php 

  $base    = '../../';

  $navenq4 = 'background:#1B1464;';

  include('header.php');

  if (!in_array('Job Allotment', $arrysession)) {
   
    echo "<script>window.location.href='/process/dashboard.php';</script>";
   
    exit;

}

  if (session_status() == PHP_SESSION_NONE) { session_start(); }

  $sessionby = $_SESSION['employeeid'];

  $branchid  = $_SESSION['branchid'];

?>

<br>

<style>

    hr.style-hr {

        border: 0;
        height: 1px;
        background-image: linear-gradient(to right, rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.75), rgba(0, 0, 0, 0));

    }

</style>

<h2 align="center" style="margin-top:0px;">Material Receive</h2>

<div style="clear:both;"></div>
    
<hr class="style-hr">

<div class="container-fluid">

    <div class="row" id="section">

        <input type="hidden" value="<?php echo $branchid;?>" id="branchid" data-name="branchid">

        <input type="hidden" value="<?php echo $_GET['contractid'];?>" id="contractid" data-name="contractid">

        <input type="hidden" value="<?php echo $_GET['serviceid'];?>" id="serviceid" data-name="serviceid">

        <input type="hidden" value="<?php echo $_GET['jobcompletedstatus'];?>" id="jobcompletedstatus">

    </div>

    <div class = "row">

        <div class="col-sm-12">

            <div class="table-responsive" id="serviceTable">

                <table class="table table-list form-group" id="table-order">

                    <thead>

                        <th width="2%">#</th>

                        <th width="10%">Employee Name</th>

                        <th width="10%">Item Name</th>

                        <th width="5%">Branch Stock</th>

                        <th width="5%">Employee Stock</th>
                        
                        <th width="5%">Allotment Qty</th>

                        <th width="5%">Receive Qty</th>

                        <th width="5%">Send To</th>
                                                
                        <th width="5%">Action</th>

                    </thead>

                    <tbody>

                        <tr>
                            
                            <td></td>

                            <td>

                                <div class="form-group">

                                    <select data-role="select" data-name='employeeid' id="employeeId" class="form-control input-sm select-js" onchange="itemname()">
                  
                                        <option value="Select">Select</option>
                                        
                                        <?php 
                                            
                                            $serviceid  = $_GET['serviceid'];

                                            $contractid = $_GET['contractid'];

                                            $result1    = mysqli_query($con,"SELECT DISTINCT employeeid,employeename FROM job_allotment WHERE branchid='$branchid' AND service_id = '$serviceid' AND contractid = '$contractid' ");

                                            while($rows = mysqli_fetch_assoc($result1)){
                                    
                                                echo '<option value="'.$rows['employeeid'].'">'.$rows['employeename'].'</option>';

                                            }

                                        ?>

                                    </select>

                                </div>
                            
                            </td>

                             <td>

                                <div class="form-group">

                                    <select data-role="select" data-name='itemid' id="itemId" class="form-control input-sm select-js" onchange="empstock()">
                  
                                        <option value="Select">Select Item Name</option>
                                         
                                    </select>

                                </div>
                            
                            </td>


                            <td>

                                <div class="form-group">

                                    <input type="number" id="branchQty" class="form-control input-sm" readonly>

                                </div>

                            </td>

                            <td>

                                <div class="form-group">

                                    <input type="number" id="employeeQty" class="form-control input-sm" readonly>

                                </div>

                            </td>

                            <td>

                                <div class="form-group">

                                    <input type="number" id="itemQty" data-name="itemqty" data-role="number" class="form-control input-sm" readonly>

                                </div>
                            
                            </td>

                            <td>

                                <div class="form-group">

                                    <input type="number" id="outQty" data-name="outqty" data-role="number" class="form-control input-sm" onkeyup="checkqty()">

                                </div>
                            
                            </td>

                            <td>

                                 <div class="form-group">

                                    <select data-role="select" data-name='sendto' id="sendTo" class="form-control input-sm">
                  
                                        <option value="Select">Select</option>

                                        <option value="Branch">Branch</option>

                                        <option value="Employee">Employee</option>
                                         
                                    </select>

                                </div>
                            
                            </td>


                            <td>

                                <button class="btn btn-success btn-sm center-block" id="btn-add" data-edit="" onclick="add(this)">Add</button>

                            </td>

                        </tr>
                            
                    </tbody>
                    
                </table>   

            </div>    
        
        </div>

    </div>

    <div class="col-sm-12">
                
        <div style="clear:both;"></div>
            
        <hr class="style-hr">

        <div class="col-sm-3"> </div>
                
        <div class="col-sm-3">
                
            <button class="btn btn-success btn-sm btn-block" id="btn-submit" onclick="submit()" style="margin:5px;">Submit</button>
            
        </div>
            
        <div class="col-sm-3">
            
            <button class="btn btn-danger btn-sm btn-block" id="btn-reset" style="margin:5px;">Reset</button>
        
        </div>
               
        <div class="col-sm-3"></div>
        
    </div>

</div>

<?php 

  include('footer.php');

?>
<script>


$(document).ready(function() {

    if ($('#jobcompletedstatus').val() == 1){

        edit();

    }

});

function itemname() {

    let branchid   = $('#branchid').val();

    let employeeId = $('#employeeId').val();

    let contractid = $('#contractid').val();

    let serviceid  = $('#serviceid').val();

    if (employeeId != 'Select' && contractid != '' && serviceid != '') {
        
        $.ajax({

            type: "POST",

            data: "contractid=" + contractid + "&branchid=" + branchid + "&employeeid=" + employeeId + "&serviceid=" + serviceid,

            url: 'api/itemname-joballotment.php',

            cache: false,

            success: function (res) {

                if (res.status == 'success') {

                    let json = res.json;

                    str = '<option value= "Select">Select Item Name</option>';

                    for(let i in json){

                        str+='<option value = '+json[i].itemcode+'>'+json[i].itemname+'</option>'

                    }

                    $('#itemId').html(str);

                } else {

                    $('#itemId').html('<option value= "Select">Select Item Name</option>');

                }


            }

        })


    } else {

        $('#itemId').html('<option value= "Select">Select Item Name</option>');

    }

    empstock();

}

function empstock() {

    let employeeId = $('#employeeId').val();

    let itemId     = $('#itemId').val();

    let branchid   = $('#branchid').val();

    let contractid = $('#contractid').val();

    let serviceid  = $('#serviceid').val();
 
    if (itemId != 'Select' && employeeId != 'Select' && contractid != '' && serviceid != '') {

        $.ajax({

            type: "POST",

            data: "itemId=" + itemId + "&branchid=" + branchid + "&employeeId=" + employeeId + "&contractid=" + contractid + "&serviceid=" + serviceid,

            url: 'api/job-allot-stock-qty.php',

            cache: false,

            success: function (res) {

                if (res.status == 'success') {

                    $('#itemQty').val(res.total);

                    $('#branchQty').val(res.stockQty);

                    $('#employeeQty').val(res.employeestock);

                }  else {

                    $('#itemQty').val('');

                    $('#branchQty').val('');

                    $('#employeeQty').val('');

                }

            }

        });      
        
    } else {

        $('#itemQty').val('');

        $('#branchQty').val('');

        $('#employeeQty').val('');

    }
    
}


function checkqty() {

    let itemQty = parseFloat($('#itemQty').val());

    let outQty  = parseFloat($('#outQty').val());

    if (isNaN(itemQty)) {
        
        itemQty = 0
    }

    if (itemQty < outQty) {
        
        alert ('Allotment Qty Greater Than Item Qty');

        $('#outQty').val('');

    }
    
}

//--------------------------Add Multiple Services start-------------------------------------//

function add(e) {

    let chk         = $(e).attr('data-edit');

    let data        = checker('table-order');

    let employeeid   = data.employeeid;

    let employeename = $('#employeeId').find(":selected").text();

    let itemid       = data.itemid;

    let itemname     = $('#itemId').find(":selected").text();

    let outqty       = data.outqty;

    let sendto       = data.sendto;
        
    let valid        = true;

    if (checker('table-order') != false) {

        valid = valid * true;

    } else {

        valid = valid * false;

    }

    $('#table-order > tbody > tr').each(function () {

        let employeeid1 = $(this).find('.employeeid').text().trim();

        let itemid1     = $(this).find('.itemid').text().trim();

        if (employeeid1 == employeeid && chk == '' && itemid == itemid1) {

            valid = valid * false;

            alert('Dublicate Item Name For Same Employee');

        }

    });

    if (valid) {

        let chk = $(e).attr('data-edit');

        if (chk == '') {

            let len = $('#table-order .srNo').length + 1;

            fun_adder(len, employeeid, employeename, itemid, itemname, outqty, sendto);

        }

        modals.clear('table-order');

        $('#btn-add').attr('data-edit', '');

    }

}


function fun_adder(len, employeeid, employeename, itemid, itemname, outqty, sendto) {

    let str  = '<tr id = '+len+'>';
        
        str += '<td align="center" class="srNo">' + len + '</td>';
        
        str += '<td align="center" class="employeeid" style="display:none;">' + employeeid + '</td>';
        
        str += '<td align="center" class="employeename">' + employeename + '</td>';

        str += '<td align="center" class="itemid" style="display:none;">' + itemid + '</td>';
        
        str += '<td align="center" class="itemname" colSpan="4">' + itemname + '</td>';
        
        str += '<td align="center" class="outqty">' + outqty + '</td>';

        str += '<td align="center" class="sendto">' + sendto + '</td>';
                
        str += '<td align="center"><button class="btn btn-sm btn-danger remover" data-itemid = '+itemid+' data-employeeid = '+employeeid+' data-outqty = '+outqty+' data-sendto = '+sendto+' onclick="removers(this)">R</buuton></td>';
    
    str += '</tr>';
    
    $('#table-order > tbody').append(str);

}



//-----------------------Add Multiple Services End-------------------------//

//--------------------Remove Function Start----------------------//

function removers(e) {

    let itemid             = $(e).attr('data-itemid');

    let employeeid         = $(e).attr('data-employeeid');

    let outqty             = $(e).attr('data-outqty');

    let sendto             = $(e).attr('data-sendto');

    let branchid           = $('#branchid').val();
    
    let contractid         = $('#contractid').val();

    let serviceid          = $('#serviceid').val();

    $.ajax({
            
        type: "POST",
        
        data: "itemid=" + itemid + "&employeeid=" +employeeid + "&outqty=" + outqty + "&branchid=" + branchid + "&contractid=" + contractid + "&serviceid=" + serviceid + "&sendto=" + sendto,

        url: 'api/job-complete-remove.php',
            
        cache: false,
            
            success: function (res) {
            
                if (res.status == 'success') {
            
                    $(e).parent().parent().remove();
                
                } else {

                    $(e).parent().parent().remove();

                }

            }
        
        });


    
}

//--------------------Remove Function End-----------------------//


//-------------------------Insert Start ---------------------------//

function submit() {

    let valid = true;

    if (checker('section') != false) {

        valid = valid * true;

    } else {

        valid = valid * false;

    }

    let arr = [];

    $('#table-order > tbody > tr').each(function () {

        let srNo          = $(this).find('.srNo').text().trim();

        let employeeid    = $(this).find('.employeeid').text().trim();

        let employeename  = $(this).find('.employeename').text().trim();

        let itemid        = $(this).find('.itemid').text().trim();
        
        let itemname      = $(this).find('.itemname').text().trim();
                
        let outqty        = $(this).find('.outqty').text().trim();

        let sendto        = $(this).find('.sendto').text().trim();
  
        arr.push({

            "srno": srNo,

            "employeeid": employeeid,

            "employeename": employeename,

            "itemid": itemid,

            "itemname": itemname,

            "outqty": outqty,

            "sendto": sendto,

        });

    })

    arr.shift();

    if (arr == '') {

        $('#table-order').css('border', '2px solid red');

        valid = valid * false;

    } else {

        $('#table-order').css('border', '2px solid green');

        valid = valid * true;

    }

    if (valid) {

        let data1       = checker('section');
           
            data1       = JSON.stringify(data1);

            itemDetails = JSON.stringify(arr);

        $.ajax({
            
            type: "POST",
            
            data: {

                data: data1,

                itemDetails: itemDetails,

            },

            url: 'api/job-complete-insert.php',
            
            cache: false,
            
            success: function (res) {
            
                if (res.status == 'success') {
            
                    swal({
                        
                        type: 'success',
                       
                        title: 'Job Complete',
                       
                        showConfirmButton: false,
                       
                        timer: 3000
                    
                    });
                   
                    setTimeout(function () {
                        
                        location.href = 'job-complete.php';
                    
                    }, 3000);
                
                }

            }
        
        });
    
    }

}

//--------------------Insert Function End------------------------//


//--------------------Edit Function Start------------------------//

function edit() {

    $.ajax({

        type: "POST",

        url: 'api/job-complete-select.php',

        data: "branchid=" + $('#branchid').val() + "&contractid=" + $('#contractid').val() + "&serviceid=" + $('#serviceid').val(),

        cache: false,

        success: function (res) {

            if (res.status == 'success') {

                modals.putvalue('section', res.json);

                $('#btn-submit').attr('onclick', 'update()');

                $('#btn-submit').text('Update');

                let itemJson = res.jsons;

                let count    = 1;

                for (let i in itemJson) {
                    
                    fun_adder(count, itemJson[i].employeeid, itemJson[i].employeename, itemJson[i].itemid, itemJson[i].itemname, itemJson[i].received_qty, itemJson[i].send_to);

                    count++;
                }

            }

        }

    });
    
}

//--------------------Edit Function End------------------------//


//-------------------------Update Start ---------------------------//

function update() {

    let valid = true;

    if (checker('section') != false) {

        valid = valid * true;

    } else {

        valid = valid * false;

    }

    let arr = [];

    $('#table-order > tbody > tr').each(function () {

        let srNo          = $(this).find('.srNo').text().trim();

        let employeeid    = $(this).find('.employeeid').text().trim();

        let employeename  = $(this).find('.employeename').text().trim();

        let itemid        = $(this).find('.itemid').text().trim();
        
        let itemname      = $(this).find('.itemname').text().trim();
                
        let outqty        = $(this).find('.outqty').text().trim();

        let sendto        = $(this).find('.sendto').text().trim();
  
        arr.push({

            "srno": srNo,

            "employeeid": employeeid,

            "employeename": employeename,

            "itemid": itemid,

            "itemname": itemname,

            "outqty": outqty,

            "sendto": sendto,

        });

    })

    arr.shift();

    if (arr == '') {

        $('#table-order').css('border', '2px solid red');

        valid = valid * false;

    } else {

        $('#table-order').css('border', '2px solid green');

        valid = valid * true;

    }

    if (valid) {

        let data1          = checker('section');
           
            data1          = JSON.stringify(data1);

            itemDetails = JSON.stringify(arr);

        $.ajax({
            
            type: "POST",
            
            data: {

                data: data1,

                itemDetails: itemDetails,

            },

            url: 'api/job-complete-update.php',
            
            cache: false,
            
            success: function (res) {
            
                if (res.status == 'success') {
            
                    swal({
                        
                        type: 'success',
                       
                        title: 'Job Complete Update',
                       
                        showConfirmButton: false,
                       
                        timer: 3000
                    
                    });
                   
                    setTimeout(function () {
                        
                        location.href = 'job-complete.php';
                    
                    }, 3000);
                
                }

            }
        
        });
    
    }

}

//--------------------Update Function End------------------------//



$('#btn-reset').on('click', function () {

    window.location.reload();

});




</script>