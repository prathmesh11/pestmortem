
<?php
$base='../../';
$navenq6='background:#1B1464;';
include('header.php');

$enqno='0';

if($_GET['enq']){
    $enqno=$_GET['enq'];
}

$billno='0';
if($_GET['billid']){
    $billno=$_GET['billid'];
}

if (session_status() == PHP_SESSION_NONE) { session_start(); }
$sessionby = $_SESSION['employeeid'];
$branchid = $_SESSION['branchid'];

$branchjson=json_decode(mysqli_fetch_assoc(mysqli_query($con,"SELECT branchjson x FROM branchmaster WHERE branchid='$branchid'"))['x']);

$customerid=mysqli_fetch_assoc(mysqli_query($con,"SELECT customerid x FROM crmmaster WHERE enquiryid='$enqno' ORDER BY created_time DESC"))['x'];
$contactjson=json_decode(mysqli_fetch_assoc(mysqli_query($con,"SELECT contactjson x FROM customermaster WHERE customerid='$customerid'"))['x'])[0];

$mobilecontact=get_object_vars($contactjson)['mobile'];
$landlinecontact=get_object_vars($contactjson)['landline'];
$emailcontact=get_object_vars($contactjson)['email'];

//$custjson=json_decode(mysqli_fetch_assoc(mysqli_query($con,"SELECT customerjson x FROM customermaster WHERE customerid='$customerid'"))['x'])[0];
$cifo=mysqli_fetch_assoc(mysqli_query($con,"SELECT * FROM customermaster WHERE customerid='$customerid'"));
$namecontact=get_object_vars(json_decode($cifo['customerjson']))['customername'];

$addresscontact=get_object_vars(json_decode($cifo['customerjson']))['address'];
$state=get_object_vars(json_decode($cifo['customerjson']))['state'];
$state=mysqli_fetch_assoc(mysqli_query($con,"SELECT statename x FROM statemaster where id = '$state'"))['x'];

$gstno = $cifo['gstnumber'];

$wcifo=mysqli_fetch_assoc(mysqli_query($con,"SELECT * FROM workcontract WHERE enquiryid='$enqno'"));

//$servicename=get_object_vars(json_decode($wcifo['servicejson']))['servicename'];
$servicejson = json_decode($wcifo['servicejson']);
$servicename = $servicejson[0]->servicename;

//$itemname=get_object_vars(json_decode($wcifo['itemjson']))['itemname'];
$itemjson = json_decode($wcifo['itemjson']);
$itemname = $itemjson[0]->itemname;
$itemcode = $itemjson[0]->itemcode;

 
$rate=get_object_vars(json_decode($wcifo['itemjson']))['rate'];

 $SAC=mysqli_fetch_assoc(mysqli_query($con,"SELECT hsncode x FROM stockmaster WHERE id='$itemcode'"))['x'];

$qty = 1;
$total = $qty * $rate;
$contractdate = date("d-m-Y",$wcifo['contractdate']/1000);
$contractstart = date("d-m-Y",$wcifo['contractstart']/1000);
$contractend = date("d-m-Y",$wcifo['contractend']/1000);
$contractid = $wcifo['contractid'];
         
$contractamt = $wcifo['amount'];

$wcbifo=mysqli_fetch_assoc(mysqli_query($con,"SELECT * FROM workcontractbill WHERE billid='$billno'"));
//$billid = $wcbifo['id']+1;
$billid = $wcbifo['id'];
$ac = $wcbifo['ac'];
$pono = $wcbifo['pono'];

$qty = $wcbifo['qty'];
$rate = $wcbifo['rate'];
$totalamt = $wcbifo['amount'];
$igstper = $wcbifo['igstper'];
$cgstper = $wcbifo['cgstper'];
$sgstper = $wcbifo['sgstper'];
$igstamt = $wcbifo['igstamt'];
$cgstamt = $wcbifo['cgstamt'];
$sgstamt = $wcbifo['sgstamt'];

$query="SELECT (SUM(totalpaidamt)-(SUM(cgstamt)+SUM(sgstamt)+SUM(igstamt))) x FROM `workcontractbill` WHERE contractid='$contractid'";
$totalpaiamt=mysqli_fetch_assoc(mysqli_query($con,$query))['x'];

?>
<style>
.table-list td,
   .table-list th {
       border: 1px solid #ddd;
       padding: 1px !important;
       font-size: 13px;
   }
   .table-list td{
  padding-top: 10px !important;
   }

   .table-list tr:nth-child(even) {
       background-color: #f2f2f2;
   }

   .table-list th {
       padding-top: 5px;
       padding-bottom: 5px;
       text-align: center;
       background-color: #16a085;
       color: white;
   }
</style>
<br>
<div class="col-sm-2"></div>
<div class="col-sm-8" style="background:#fff;">
<div class="row">
<div class="col-sm-12" align="center">
<h3><b>PEST MORTEM (INDIA) PRIVATE LIMITED</b></h3>
<h4><b>(Estd. 1980)</b></h4>
<h5><?php echo "Regd. Office. :".get_object_vars($branchjson)['address']; ?></h5>
<h5><?php echo "Tel. :".get_object_vars($branchjson)['phone'] ."  Email :".get_object_vars($branchjson)['email'] ; ?></h5>
   
<hr style="border:1px solid #000;">
</div>
<?php 
$WOC=str_replace('ENQ','WOC',$enqno);
?> 
<div id="section">
<input type="hidden" data-name="billid" id="billid" value="<?php echo $billno; ?>">
<div class="col-sm-12">
 <table width="800">
 <tr>
      <td width="100">
      <b>  M/S.: <br><br><br></b>
    </td>
    <td width="300"><?php echo $namecontact."<BR>".$addresscontact; ?></td>
     <td>
       <b>INVOICE NO. : <br><br><br></b>
    </td>
    <td><?php echo $billno;?><br><br><br></td>
    </tr>
  <tr>
      <td width="100">
      <b>  GSTIN :  </b>
    </td>
    <td width="300"><?php echo $gstno; ?></td>
     <td>
       <b>INVOICE DATE:  </b>
    </td>
    <td><?php echo date("d-m-Y");?></td>
    </tr> 
    <tr>
      <td>
       <b> A/C :  </b>
    </td>
    <td>  <input type="text" data-name="ac" id="ac" class="form-control" value="<?php echo $ac;?>"></td>
    <td>
       <b> CONTRACT NO. :  </b>
    </td>
    <td> <?php echo $contractid;  ?></td>


    </tr>
    <tr>
      <td>
       <b> STATE: </b>
    </td>
    <td>  <?php echo $state; ?></td>
     <td>
        <b> CONTRACT DATE : </b>
    </td>
    <td> <?php echo $contractdate; ?></td>


    </tr>
 </table>

 </div>

<input type="hidden" data-name="henqno" id="henqno" value="<?php  echo $enqno; ?>">
<div class="col-sm-12">
  <table border = "1" width="800" height="400">
  <td width="300" align= "center"><b> P A R T I C U L A R S  </b></td>
  <td width="50"  align= "center"> <b>SAC</b></td>
  <td width="50"  align= "center"> <b>QTY</b></td>
  <td width="100" align= "center"> <b>RATE</b></td>
  <td width="100" align= "center"> <b>TOTAL</b></td>
  <tr>
  <td ><?php echo ($itemname);?>
<BR><BR>
     SERVICE : <?php echo $servicename; ?> <br>
  CONTRACT PERIOD : <?php echo $contractstart; ?> TO <?php echo $contractend; ?><BR>
  PO NO. : <input type="text" size = '2' data-name="pono" id="pono" value="<?php echo $pono;?>"> DATED : <?php echo $contractdate; ?> <BR>
 
<BR><BR>
 <input type="text" size = '8' data-name="contractamt" id="contractamt" value="<?php echo number_format($contractamt,2); ?>"   readonly > : TOTAL CONTRACT AMOUNT<br>
 <input type="hidden" id="totalpaiamt" data-name="totalpaiamt" value="<?php echo $totalpaiamt; ?>">
  <input type="hidden" id="hcontractamt" data-name="hcontractamt" value="<?php echo $contractamt; ?>">
  <input type="text" size = '8' data-name="quarterly" id="quarterly" readonly> : 1ST QUARTER BILL  <br>
  <input type="hidden" size = '8' data-name="hquarterly" id="hquarterly"  >
  <hr>
  <input type="text"  size = '8' data-name="balance" id="balance" value="<?php echo number_format(($contractamt - $totalpaiamt),2);?>" readonly> : BALANCE AMOUNT <br>

         
   </td>


  <td align= "center"><?php echo $SAC; ?><br><br><br><br><br><br><br><br><br><br><br><br><br><br></td>
  <td align= "center"><input type="text" size = '2' data-name="qty" id="qty" value="<?php echo $qty;?>" onkeyup="calc();"><br><br><br><br><br><br><br><br><br><br><br><br><br><br></td>
  <td align= "center"><input type="text" size = '5' data-name="rate" id="rate" value="<?php echo $rate;?>" onkeyup="calc();" value="<?php echo ($rate); ?>"><br><br><br><br><br><br><br><br><br><br><br><br><br><br></td>

  <td align= "center"><input type="text" size = '5' data-name="total" id="total" value="<?php echo number_format($total, 2); ?>" readonly ><br><br><br><br><br><br><br><br><br><br><br><br><br><br></td>
  
  </tr>
   
  </table>

</div>
 <div class="col-sm-12">
   <table>
   <td width="500">
   <br>
   <b>Rupees :<span id="inword"></span> Only</b><br>
   <b>Payment should be made within 30 days from the date of invoice.<br>
   Interest @18% will be charged on all overdue invoices.<br>
   GSTIN : <?php echo $gstno; ?><br>
   Category of service : Cleaning services<br>
   </b><br>
   E.P.F. A/c No. MH/40228<br> 
   E.S.I.C. Code No. : 31-48076-101<br> 
   PAN No. AACCP 2255R<br> 
   Fumigation - Accreditation No.<br><br>
   <b>
   Bank Name : <?php echo get_object_vars($branchjson)['bankname']; ?> <br>
   Branch Name : <?php echo get_object_vars($branchjson)['bankbranchname']; ?><br>
   Account Holder's Name : M/s. Pest Mortem(India) Pvt. Ltd.<br>
   AC Number : C.A. No.  <?php echo get_object_vars($branchjson)['accountno']; ?><br>
   RTGS/ NEFT IFSC CODE :  <?php echo get_object_vars($branchjson)['ifsccode']; ?><br>
   </b>

   </td>
   <td width="300">
   <b>
   <?php
    $gstamt=($total)*(18)/100;
        ?>
   <table> 
    <tr><td width="200"> Total  </td> <td width="50"><input type="text" id="totalamt1" size= "12" value="" readonly></td></tr>
   <tr><td>()FREIGHT</td><td><input type="text" id="freight" value="0" size= "12" readonly></td></tr>
   
   <tr><td>CGST@ <input type="text"  size="1" id="cgst"  data-name="cgst" value="<?php echo $cgstper; ?>" onkeyup="calc();">%</td><td><input type="text" id="cgstamt" value="<?php echo $cgstamt; ?>" size= "12" readonly></td></tr>
   
   <tr><td>SGST@ <input type="text"  size="1" id="sgst" data-name="sgst"  value="<?php echo $sgstper; ?>" onkeyup="calc();">%</td> <td><input type="text" id="sgstamt" value="<?php echo $sgstamt; ?>" size= "12" readonly></td></tr>

   
  <tr><td>IGST@  &nbsp;<input type="text" size="1" id="igst" data-name="igst" value="<?php echo $igstper; ?>" onkeyup="calc();">%</td> <td><input type="text" id="igstamt" value="<?php echo $igstamt; ?>" size= "12" readonly></td></tr>
   

   <tr><td>GRAND TOTAL </td><td><input type="text" id="grandtotal" size= "12" value="0.00" readonly>
   <input type="hidden" id="billamt" data-name="billamt" ></td></tr>
    </table>
 
   <br>
   For Pest Mortem(India) Pvt. Ltd.
   <BR><BR><BR><BR>
   Authorised Signatory
   </td>
   </table>

 </div> 
<button class="btn btn-lg btn-primary" onclick="savebill(this)" style="border-radius:0px;position:fixed;bottom:10px;right:10px;">Save Bill</button>
</div>
<?php
include('footer.php');
?>
<script>
$('body').css('background','#eee');
 $(document).ready(function() {
  calc();
 });

 function calc(){
    var totalamt=0;
     var rate=$('#rate').val();
 
    var qty=$('#qty').val();
    if(rate!='' && qty!=''){
        totalamt = rate * qty;

    $('#total').val(totalamt.toFixed(2) );
    $('#quarterly').val(totalamt.toFixed(2) );
    $('#totalamt1').val(totalamt.toFixed(2) );
    $('#hquarterly').val(totalamt);
    
    calc1();
      }
}
function calc1(){
    var totalamt=0;
     var netamt=$('#hcontractamt').val();
     var igst=$('#igst').val();
     var cgst=$('#cgst').val();
     var sgst=$('#sgst').val();
     var freight=$('#freight').val();
    var totalpaiamt=$('#totalpaiamt').val();
     //alert(sgst);
     var grandtotal = 0; 
    // alert(quarterly);
   var billamt = parseInt($('#hquarterly').val());
    if(netamt!='' && billamt!=''){
        totalamt = netamt - billamt;
        cgst = (billamt*cgst)/100;
        sgst = (billamt*sgst)/100;
        igst = (billamt*igst)/100;
        grandtotal = billamt + igst + cgst + sgst;
        balance = (netamt - totalpaiamt) - billamt;
       // alert(igst);
    $('#balance').val(balance.toFixed(2) );
  // $('#totalamt1').val(billamt.toFixed(2));
   $('#cgstamt').val(cgst.toFixed(2));
   $('#sgstamt').val(sgst.toFixed(2));
   $('#igstamt').val(igst.toFixed(2));
   
   $('#grandtotal').val(grandtotal.toFixed(2));
   $('#billamt').val(grandtotal.toFixed(2));
   
    $('#inword').text(convertNumberToWords(grandtotal.toFixed(2) ));
    }
}
 
function savebill(e) {
   var valid=true;
    if(checker('section') !=false ) {
            valid=valid*true;
        }else{
            valid=valid*false;
         }
    if(valid){

        var data1=checker('section');
         data1 = JSON.stringify(data1);
         $.ajax({
            type: "POST",
            data :{ data1: data1    },
             url: 'savebill1.php',
            cache: false,
            success: function (res) {
               if (res.status == 'success') {
                    window.location = "billing.php";
                }
            }
        })
      } else{
        console.log("section Invalid");
      }
    
}
function convertNumberToWords(amount) {
    var words = new Array();
    words[0] = '';
    words[1] = 'One';
    words[2] = 'Two';
    words[3] = 'Three';
    words[4] = 'Four';
    words[5] = 'Five';
    words[6] = 'Six';
    words[7] = 'Seven';
    words[8] = 'Eight';
    words[9] = 'Nine';
    words[10] = 'Ten';
    words[11] = 'Eleven';
    words[12] = 'Twelve';
    words[13] = 'Thirteen';
    words[14] = 'Fourteen';
    words[15] = 'Fifteen';
    words[16] = 'Sixteen';
    words[17] = 'Seventeen';
    words[18] = 'Eighteen';
    words[19] = 'Nineteen';
    words[20] = 'Twenty';
    words[30] = 'Thirty';
    words[40] = 'Forty';
    words[50] = 'Fifty';
    words[60] = 'Sixty';
    words[70] = 'Seventy';
    words[80] = 'Eighty';
    words[90] = 'Ninety';
    amount = amount.toString();
    var atemp = amount.split(".");
    var number = atemp[0].split(",").join("");
    var n_length = number.length;
    var words_string = "";
    if (n_length <= 9) {
        var n_array = new Array(0, 0, 0, 0, 0, 0, 0, 0, 0);
        var received_n_array = new Array();
        for (var i = 0; i < n_length; i++) {
            received_n_array[i] = number.substr(i, 1);
        }
        for (var i = 9 - n_length, j = 0; i < 9; i++, j++) {
            n_array[i] = received_n_array[j];
        }
        for (var i = 0, j = 1; i < 9; i++, j++) {
            if (i == 0 || i == 2 || i == 4 || i == 7) {
                if (n_array[i] == 1) {
                    n_array[j] = 10 + parseInt(n_array[j]);
                    n_array[i] = 0;
                }
            }
        }
        value = "";
        for (var i = 0; i < 9; i++) {
            if (i == 0 || i == 2 || i == 4 || i == 7) {
                value = n_array[i] * 10;
            } else {
                value = n_array[i];
            }
            if (value != 0) {
                words_string += words[value] + " ";
            }
            if ((i == 1 && value != 0) || (i == 0 && value != 0 && n_array[i + 1] == 0)) {
                words_string += "Crores ";
            }
            if ((i == 3 && value != 0) || (i == 2 && value != 0 && n_array[i + 1] == 0)) {
                words_string += "Lakhs ";
            }
            if ((i == 5 && value != 0) || (i == 4 && value != 0 && n_array[i + 1] == 0)) {
                words_string += "Thousand ";
            }
            if (i == 6 && value != 0 && (n_array[i + 1] != 0 && n_array[i + 2] != 0)) {
                words_string += "Hundred and ";
            } else if (i == 6 && value != 0) {
                words_string += "Hundred ";
            }
        }
        words_string = words_string.split("  ").join(" ");
    }
    return words_string;
}
</script>