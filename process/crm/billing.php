<?php 
$base='../../';
$navenq5='background:#1B1464;';
include('header.php');

if (session_status() == PHP_SESSION_NONE) { session_start(); }
$sessionby = $_SESSION['employeeid'];
$branchid = $_SESSION['branchid'];


$fromdate='';
$customerid='';


if(isset($_GET['fromdate'])){ $fromdate=$_GET['fromdate']; }
if(isset($_GET['customerid'])){ $customerid=$_GET['customerid']; }
$query='';

if($customerid=='Select'){ $customerid=''; }



if($customerid!=''  && $fromdate=='' ){
  $query="SELECT * FROM workcontractbill WHERE paid_time=0 AND bill_time = 0 AND customerid='$customerid' AND branchid='$branchid' ORDER BY billdate DESC ";
}


// if($customerid==''  && $fromdate!='' && $todate!=''){
//   $select=mysqli_query($con,"SELECT DATE_FORMAT(podate,'%d-%m-%Y') as podate, (SELECT branchname FROM branchmaster WHERE branchmaster.branchid=purchase.poforbranch) as branchname ,purchaseid,itemname,qty as orderqty, DATE_FORMAT(FROM_UNIXTIME(gateentry_time/1000),'%d-%m-%Y') AS gateentry_time ,(SELECT qty FROM `gateentry` WHERE gateentry.itemcode=purchase.itemcode AND gateentry.purchaseid=purchase.purchaseid) AS receivedqty,unit, (SELECT suppliername FROM suppliermaster WHERE suppliermaster.supplierid=purchase.supplierid) AS suppliername FROM purchase WHERE (podate BETWEEN '$fromdate' AND '$todate')");
// }
// if($customerid=='' && $fromdate=='' && $todate==''){
//   $result=mysqli_query($con,"SELECT * FROM workcontractbill WHERE paid_time=0 AND bill_time = 0 AND customerid='$customerid' ORDER BY billdate DESC ");
// }

// if($customerid!=''  && $fromdate!='' && $todate!=''){
//   $select=mysqli_query($con,"SELECT DATE_FORMAT(podate,'%d-%m-%Y') as podate, (SELECT branchname FROM branchmaster WHERE branchmaster.branchid=purchase.poforbranch) as branchname ,purchaseid,itemname,qty as orderqty, DATE_FORMAT(FROM_UNIXTIME(gateentry_time/1000),'%d-%m-%Y') AS gateentry_time ,(SELECT qty FROM `gateentry` WHERE gateentry.itemcode=purchase.itemcode AND gateentry.purchaseid=purchase.purchaseid) AS receivedqty,unit, (SELECT suppliername FROM suppliermaster WHERE suppliermaster.supplierid=purchase.supplierid) AS suppliername FROM purchase WHERE poforbranch='$branchid' and supplierid='$supplierid' and itemcode='$stockid' AND (podate BETWEEN '$fromdate' AND '$todate')");
// }

?>
<style>
.table-list td,
.table-list th {
  border: 1px solid #ddd;
  padding: 1px !important;
  font-size: 13px;
}

.table-list td {
  padding-top: 2px !important;
  margin: 0;

}

.table-list tr:nth-child(even) {
  background-color: #f2f2f2;
}

.table-list th {
  padding-top: 5px;
  padding-bottom: 5px;
  text-align: center;
  background-color: #16a085;
  color: white;
}
</style>
<br>
<div class="row">

  <div class="col-sm-2">
    Select Customer :
    <select class="form-control input-sm" id="customerid">
      <option value="Select">Select Customer</option>
      <?php
       if (session_status() == PHP_SESSION_NONE) { session_start(); }
       $branchid = $_SESSION['branchid'];
       $select1=mysqli_query($con,"SELECT customerid,customername FROM `customermaster` WHERE branchid='$branchid' ");
          while($rows = mysqli_fetch_assoc($select1)){
            echo '<option value="'.$rows['customerid'].'">'.$rows['customername'].'</option>';
          }
      ?>
    </select>
  </div>
  <div class="col-sm-2">
    Bill date:
    <input type="date" class="form-control input-sm" id="fromdate" />
  </div>

  <div class="col-sm-2">
    <br>
    <a class="btn btn-warning btn-md btn-block" href=""
      onclick="this.href='billing.php?fromdate=' + document.getElementById('fromdate').value +  '&customerid=' + document.getElementById('customerid').value; return true">Search</a>
  </div>

  <div class="col-sm-2">
    <br>

    <button class="btn btn-sm btn-block btn-success" data-billid="<?php echo $billid; ?>"
      data-billamt="<?php echo $billamt; ?>" data-toggle="modal" data-target="#myModal"
      onclick="setidtomidal(this)">Confirm</button>
  </div>
  <div class="col-sm-2">
    <br>

    <button class="btn btn-sm btn-block btn-primary" data-billid="<?php echo $billid; ?>"
      data-billamt="<?php echo $billamt; ?>" data-toggle="modal" data-target="#myModal3"
      onclick="discount(this)">Discount</button>
  </div>

  <div class="col-sm-2">
    <br>

    <button class="btn btn-sm btn-block btn-info" onclick="invoice()">Print Invoice</button>
  </div>

</div>
<br>

<div class="table-ui container-fluid">
  <div class="tr row">
    <div class="col-sm-1 th" style="word-wrap:break-word;">Enquiry No.</div>
    <div class="col-sm-3 th">Customer Info</div>
    <div class="col-sm-3 th">Bill Description</div>
    <div class="col-sm-4 th">Payment info</div>
    <div class="col-sm-1 th">Action</div>
  </div>
            <?php
            $result = mysqli_query($con,$query);
            while($rows = mysqli_fetch_assoc($result)){
                  $enquiryid    = $rows['enquiryid'];
                  $customerid   = $rows['customerid'];
                  $contractid   = $rows['contractid'];
                  $billid       = $rows['billid'];
                  $servicedate  = $rows['billdate'];
                  $servicename  = $rows['servicename'];
                  $shedulestart = $rows['shedulestart'];
                  $sheduleend   = $rows['sheduleend'];
                  $empjson      = $rows['empjson'];
                  $contractamt  = $rows['contractamt'];
                  $followupdate = $rows['followupdate'];

                  if($customerid){
                  $cifo        = mysqli_fetch_assoc(mysqli_query($con,"SELECT * FROM customermaster WHERE customerid='$customerid'"));
                  $category    = mysqli_fetch_assoc(mysqli_query($con,"SELECT category x FROM crmmaster WHERE wo_time<>0 AND wo_by<>0 AND enquiryid='$enquiryid'"))['x'];
                  $query       = "SELECT SUM(totalpaidamt) x FROM `workcontractbill` WHERE contractid='$contractid'";
                  $totalpaiamt = mysqli_fetch_assoc(mysqli_query($con,$query))['x'];
                  $totalamt    = mysqli_fetch_assoc(mysqli_query($con,"SELECT amount x FROM workcontract WHERE contractid='$contractid'"))['x'];
                  $other       = mysqli_fetch_assoc(mysqli_query($con,"SELECT othercharges x FROM workcontract WHERE contractid='$contractid'"))['x'];
                  $billqty     = mysqli_fetch_assoc(mysqli_query($con,"SELECT billqty x FROM workcontract WHERE contractid='$contractid'"))['x'];

            ?>
            <div class="row tr abc">
              <div class="col-sm-1 td" style="word-wrap:break-word;">
                <?php echo $enquiryid; ?>
              </div>
              <div class="col-sm-3 td">
                <?php
                echo 'Company : '.$cifo['customername']; 
                echo '<br>Address : '.get_object_vars(json_decode($cifo['customerjson']))['address']; 
                echo '<br>Name  : '.get_object_vars(json_decode($cifo['contactjson'])[0])['name'];
                echo '<br>Dept. : '.get_object_vars(json_decode($cifo['contactjson'])[0])['department'];
                echo '<br>Mobile : <a href="tel:'.get_object_vars(json_decode($cifo['contactjson'])[0])['mobile'].'">'.get_object_vars(json_decode($cifo['contactjson'])[0])['mobile'].'</a>';
                echo '<br>Landline'.get_object_vars(json_decode($cifo['contactjson'])[0])['landline'];
                echo '<br>Email : <a href="mailto:'.get_object_vars(json_decode($cifo['contactjson'])[0])['email'].'">'.get_object_vars(json_decode($cifo['contactjson'])[0])['email'].'</a>';
                ?>
              </div>

              <div class="col-sm-3 td" style="word-wrap:break-word;">
                <?php
                echo 'Servcie Name : '.$category.'<br>';
                $lastfollowup       = $servicedate;
                $followUpStatusFlag = '';

                if($lastfollowup!=0){

                  if($START_OF_THE_DAY<$lastfollowup && $END_OF_THE_DAY<=$lastfollowup){
                      $followUpStatusFlag = '#3F51B5';
                  }

                  if($START_OF_THE_DAY>$lastfollowup && $END_OF_THE_DAY>$lastfollowup){
                      $followUpStatusFlag = '#F44336';
                  }

                  if($START_OF_THE_DAY<=$lastfollowup && $END_OF_THE_DAY>$lastfollowup ){
                      $followUpStatusFlag = '#4CAF50';
                  }
                } else{
                  $followUpStatusFlag = '#ff9800';
                }

                echo 'Bill Date : <p style="background:'.$followUpStatusFlag.';color:#fff;">'.date("d-m-Y",$servicedate/1000).'</p>'; 
                echo 'Contract No. : '.$contractid.'<br>';
                echo 'Bill No. : '.$billid; 
               ?>
             </div>

             <div class="col-sm-4 td">
                <?php 
                  echo 'Contract Amount : '.$contractamt.'<br>';
                  echo 'Total Paid Amount :'.$totalpaiamt.'<br>';
                  echo 'Follow Up : ';
                  if($followupdate!="0"){
                  echo date("d-m-Y",$followupdate/1000);
                  }
                ?>
                <table class="table table-list">
                  <thead>
                    <tr>
                      <th>BILL AMT.</th>
                      <th>CGST</th>
                      <th>SGST</th>
                      <th>IGST</th>
                      <th>OTHER</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                            $bal=0;$billamt=0;$created=""; $para=0;$billamt=0;
                                $result4=mysqli_query($con,"SELECT * FROM workcontractbill WHERE billid ='$billid' ");
                                  while($rows3 = mysqli_fetch_assoc($result4)){
                                      $count++;
                                  $balance = ($rows3['contractamt']-$rows3['billamt']);
                                  $para = "'".$rows['billid']."','".$rows3['enquiryid']."','1'";
                                  $para1 = "'".$rows['billid']."','".$rows3['enquiryid']."','2'";
                                  
                                  $created = $rows3['created'];
                                  $enquiryid1 = $rows['enquiryid'];
                                  $totalpaiamt=mysqli_fetch_assoc(mysqli_query($con,"SELECT SUM(totalpaidamt) x FROM `workcontractbill` WHERE contractid='$contractid' billid ='$billid' "))['x'];
                                  if ($totalpaiamt==0) {
                                    $billamt =  ($rows3['contractamt']/$billqty)-$rows3['discount_amt'];
                                  }else{
                                    $billamt =  mysqli_fetch_assoc(mysqli_query($con,"SELECT totalpaidamt x FROM `workcontractbill` WHERE contractid='$contractid' AND billid ='$billid' "))['x'];
                                  }
                                  echo '<tr>'; 
                                  echo '<td align="center">'.$billamt.'</td>';
                                  //echo '<td align="center"><a href="javascript:void(0);" onclick="printer('.$para.')" >'.$rows3['billamt'].'</a></td>';
                                  echo '<td align="center">'.number_format($rows3['cgstper']/$billqty,2).'</td>';
                                  echo '<td align="center">'.number_format($rows3['sgstper']/$billqty,2).'</td>';
                                  echo '<td align="center">'.number_format($rows3['igstper']/$billqty,2).'</td>';
                                  echo '<td align="center">'.number_format($other/$billqty,2).'</td>';
                                  echo '<tr>';
                                }
                    ?>

                  </tbody>
                </table>
             </div>

             <div class="col-sm-1 td">
               <button class="btn btn-sm btn-block btn-warning" data-billid="<?php echo $billid; ?>" data-toggle="modal"
                data-target="#myModal2" onclick="setidtomidal2(this)">Follow Up</button>
                <label><input type="checkbox" class="option_billid" value="<?php echo $billid; ?>">Option 2</label>
             </div>
           </div>

            <?php
             }
            }
            ?>
</div>
<?php 
include('footer.php');
?>
<script>
function invoice() {

  var data = [];

  $('.table-ui > .abc ').each(function () {
    data.push({
      // "option_billid": $(this).find('.option_billid').val()
      "option_billid": $(this).find('.option_billid:checked').val()
    })
  })

  var datastr5 = JSON.stringify(data);

  $.ajax({
    type: "POST",
    data: {
      data1: datastr5
    },
     url: 'api/billing.php',
    cache: false,
    success: function (res) {
      if (res.status == 'success') {
           var json = res.json;
           var content = [];
           var branchjson=json.branchjson;
           
         
           content.push({
    layout: 'noBorders',
    table: {
      widths: ['*'],
      body: [
       [ {
              text: 'INVOICE BILL',
              fontSize: 10,
              bold: true,
              alignment: 'center',
            }],
            [{
              text: 'SUBJECT TO MUMBAI JURIDICTION',
              fontSize: 8,
              bold: true,
              alignment: 'center',
            }],
            [{
              text: 'PEST MORTEM (INDIA) PRIVATE LIMITED',
              fontSize: 15,
              bold: true,
              alignment: 'center',
            }],
            [{
              text: '(Estd. 1980)\n',
              fontSize: 10,
              bold: true,
              alignment: 'center',
            }],
            [{
              text:  'Regd. Office : ' + branchjson.address + '\nTel : ' + branchjson.phone + ' Email : ' + branchjson.email + '\n\n',
              fontSize: 8,
              bold: true,
              alignment: 'center',
            }],
      ]
    }
  });
  content.push({
    table: {
      widths: ['*'],
      body: [
       [ {
              text: 'Regd. Office : G -2, Sunder Tower, T.J. Road, Sewree(west),Mumbai - 400015\n Phone : 24147425/24127935/24111976/24149566/65531746/47 . Fax : 91-22-24150261 . \n E-Mail : pestmort@bom3.vsnl.net.in . pestmortem@pestmortem@pestmortem.com. Website : www.pestmortem.com' ,
              fontSize: 9,
              bold: true,
              alignment: 'center',
              border: [true, true, true, true],
            }],
            
      ]
    }
  });

  content.push({
    table: {
      widths: ['*','*'],
      body: [
       [{
        text: 'M/S            : ' + json.customername.toUpperCase(),
              fontSize: 9,
              bold: true,
              alignment: 'left',
              border: [true, false, true, false],
       }, {
              text: 'INVOICE NO.          : ' + json.invoice_bill,
              fontSize: 9,
              bold: true,
              alignment: 'left',
              border: [true, false, true, false],
            }],
            [{
        text: 'ADDRESS  :  ' + json.address.toUpperCase(),
              fontSize: 9,
              bold: true,
              alignment: 'left',
              border: [true, false, true, false],
       }, {
              text: 'INVOICE NO.          : ',
              fontSize: 9,
              bold: true,
              alignment: 'left',
              border: [true, false, true, false],
            }],
            [{
        text: 'GST NO.     : ' + json.gstno,
              fontSize: 9,
              bold: true,
              alignment: 'left',
              border: [true, false, true, false],
       }, {
              text: 'INVOICE DATE       : ' ,
              fontSize: 9,
              bold: true,
              alignment: 'left',
              border: [true, false, true, false],
            }],
            [{
        text: 'STATE        : ' + json.state,
              fontSize: 9,
              bold: true,
              alignment: 'left',
              border: [true, false, true, false],
       }, {
              text: 'CONTRACT NO.     : ',
              fontSize: 9,
              bold: true,
              alignment: 'left',
              border: [true, false, true, false],
            }],
            [{
        text: 'A/C.            : ' ,
              fontSize: 9,
              bold: true,
              alignment: 'left',
              border: [true, false, true, true],
       }, {
              text: 'CONTRACT DATE  : ',
              fontSize: 9,
              bold: true,
              alignment: 'left',
              border: [true, false, true, true],
            }],
            
      ]
    }
  });



                    
content.push({
                        text: "\n",
                    });

                   var contractamt= json.contractamt;
                    content.push({
                      table: {
				widths: [350, '*'],

				body: [
					[{rowSpan: 7, text: 'Rupees : ' + convertNumberToWords(contractamt)+'Rupes Only \n\nPayment should be made within 30 days from the date of invoice.\n Interest @18% will be charged on overdue Invoices.\n GSTIN: ' + json.gstno + ' \n Category of Service : Cleaning Service\n\n E.P.F. A/c No. : MH/40228\n E.S.I.C. Code No. : 31-480-101\n PAN NO. : AACCP25 \n ',bold: 'true',fontSize: '9'}, {text:'TOTAL                      : ' + json.amount,bold: 'true',fontSize: '9'}],
					['', {text:'CGST ' + json.cgstper + '%            : '   + json.cgstamt ,bold: 'true',fontSize: '9'}],
					['', {text:'SGST ' + json.sgstper  + '%            : ' +json.sgstamt ,bold: 'true',fontSize: '9'} ],
					['',{text: 'IGST ' + json.igstper  + '%             : ' + json.igstamt ,bold: 'true',fontSize: '9'}],
					['',{text:'Other Charges        : '  ,bold: 'true',fontSize: '9'}],
          ['',{text:'Discount Amount   : ' + json.discount_amt ,bold: 'true',fontSize: '9'}],
          ['',{text:'GRAND TOTAL       :  ' +json.contractamt ,bold: 'true',fontSize: '9'}],
				]
			}
  });




  // content.push({
  //   text: '\n'
  // });
  // content.push({
  //   text: '\n'
  // });
 
  //                   content.push({
  //                     table: {
	// 			widths: [350, '*'],

	// 			body: [
	// 				[{text: 'Bank Name : ' + branchjson.bankname,bold: 'true',fontSize: '10',border: [false, false, false, false]}, {text:'For Pest Mortem(India) Pvt. Ltd.  ',bold: 'true',alignment: 'center',fontSize: '8',border: [true, true, true, false]}],
	// 				[{text: 'Branch Name : ' + branchjson.bankbranchname,bold: 'true',fontSize: '10',border: [false, false, false, false]}, {text:'',border: [true, false, true, false]}],
	// 				[{text: 'Account holders Name :  M/s. Pest Mortem (India) Pvt. Ltd.' ,bold: 'true',fontSize: '10',border: [false, false, false, false]}, {text:'',border: [true, false, true, false]}],
	// 				[{text: 'AC Number : C.A.No. ' + branchjson.accountno ,bold: 'true',fontSize: '10',border: [false, false, false, false]}, {text:'Authorised Signatory',bold: 'true',alignment: 'center',fontSize: '8',border: [true, false, true, true]}],
	// 				[{text:'RTGS/NEFT IFSC CODE :    : ' + branchjson.ifsccode,bold: 'true',fontSize: '10',border: [false, false, false, false]},{text:'',border: [false, false, false, false]}],
	// 			]
	// 		}
  //                   });



 
  dd = {
    pageSize: 'A4',
    pageOrientation: 'portrait',
    pageMargins: [40, 30, 30, 35],
    footer: function (currentPage, pageCount) {
      return {
        margin: 10,
        columns: [{
          fontSize: 8,
          text: [{
              text: '--------------------------------------------------------------------------' +
                '\n',
              margin: [0, 20]
            },
            {
              text: '© Pest Mortem (India) PVT. LTD | PAGE ' + currentPage.toString() + ' of ' + pageCount,
            }
          ],
          alignment: 'center'
        }]
      };

    },
    content,
    styles: {
      tablfont: {
        fontSize: 7
      },
      tablfont1: {
        fontSize: 9
      }
    }
  }
  var win = window.open('', '_blank');
  pdfMake.createPdf(dd).open({}, win);

        }
      }
  });  
}


function convertNumberToWords(amount) {
    var words = new Array();
    words[0] = '';
    words[1] = 'One';
    words[2] = 'Two';
    words[3] = 'Three';
    words[4] = 'Four';
    words[5] = 'Five';
    words[6] = 'Six';
    words[7] = 'Seven';
    words[8] = 'Eight';
    words[9] = 'Nine';
    words[10] = 'Ten';
    words[11] = 'Eleven';
    words[12] = 'Twelve';
    words[13] = 'Thirteen';
    words[14] = 'Fourteen';
    words[15] = 'Fifteen';
    words[16] = 'Sixteen';
    words[17] = 'Seventeen';
    words[18] = 'Eighteen';
    words[19] = 'Nineteen';
    words[20] = 'Twenty';
    words[30] = 'Thirty';
    words[40] = 'Forty';
    words[50] = 'Fifty';
    words[60] = 'Sixty';
    words[70] = 'Seventy';
    words[80] = 'Eighty';
    words[90] = 'Ninety';
    amount = amount.toString();
    var atemp = amount.split(".");
    var number = atemp[0].split(",").join("");
    var n_length = number.length;
    var words_string = "";
    if (n_length <= 9) {
        var n_array = new Array(0, 0, 0, 0, 0, 0, 0, 0, 0);
        var received_n_array = new Array();
        for (var i = 0; i < n_length; i++) {
            received_n_array[i] = number.substr(i, 1);
        }
        for (var i = 9 - n_length, j = 0; i < 9; i++, j++) {
            n_array[i] = received_n_array[j];
        }
        for (var i = 0, j = 1; i < 9; i++, j++) {
            if (i == 0 || i == 2 || i == 4 || i == 7) {
                if (n_array[i] == 1) {
                    n_array[j] = 10 + parseInt(n_array[j]);
                    n_array[i] = 0;
                }
            }
        }
        value = "";
        for (var i = 0; i < 9; i++) {
            if (i == 0 || i == 2 || i == 4 || i == 7) {
                value = n_array[i] * 10;
            } else {
                value = n_array[i];
            }
            if (value != 0) {
                words_string += words[value] + " ";
            }
            if ((i == 1 && value != 0) || (i == 0 && value != 0 && n_array[i + 1] == 0)) {
                words_string += "Crores ";
            }
            if ((i == 3 && value != 0) || (i == 2 && value != 0 && n_array[i + 1] == 0)) {
                words_string += "Lakhs ";
            }
            if ((i == 5 && value != 0) || (i == 4 && value != 0 && n_array[i + 1] == 0)) {
                words_string += "Thousand ";
            }
            if (i == 6 && value != 0 && (n_array[i + 1] != 0 && n_array[i + 2] != 0)) {
                words_string += "Hundred and ";
            } else if (i == 6 && value != 0) {
                words_string += "Hundred ";
            }
        }
        words_string = words_string.split("  ").join(" ");
    }
    return words_string;
}

</script>