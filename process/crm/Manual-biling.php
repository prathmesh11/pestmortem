<?php 
    
        //crm->header.php,pestControlBill.php,pestControlemail.php,fumigationBill.php,fumigationemail.php

        //crm->api->deleteBill.php,fumigationBillSelect.php,fumigationSaveBill.php,manualBillSelect.php,pestControlSaveBill.php,setBill.php
        
        //database->billing

    $base    = '../../';
    $navenq6 = 'background:#1B1464;';

    include('header.php');

    if (session_status() == PHP_SESSION_NONE) { session_start(); }
    $sessionby = $_SESSION['employeeid'];
    $branchid  = $_SESSION['branchid'];

    $branchshortname = mysqli_fetch_assoc(mysqli_query($con,"SELECT branchshortname x FROM branchmaster WHERE branchid='$branchid'"))['x'];

?>

<br>
<div class="table-ui container-fluid">

    <div class="tr row">

        <div class="col-sm-1 th" style="word-wrap:break-word;">Bill No.</div>

        <div class="col-sm-3 th">Customer Info</div>

        <div class="col-sm-3 th">Bill Description</div>

        <div class="col-sm-3 th">Payment info</div>

        <div class="col-sm-2 th">Action</div>

    </div>


    <div class="row tr">

        <?php

            $result = mysqli_query($con,"SELECT * FROM billing WHERE branchid='$branchid' AND deleted_time=0 ");

            while ($row = mysqli_fetch_assoc($result)) {

                $customerid = $row['customerid'];

                $custEmail = get_object_vars(json_decode($row['customer_json'])[0])['custEmail'];


                $billDate = strtotime(get_object_vars(json_decode($row['customer_json'])[0])['issuedate'])*1000;

                $lastfollowup=$billDate;

                $followUpStatusFlag = '';

                if($lastfollowup!=0){

                  if($START_OF_THE_DAY<$lastfollowup && $END_OF_THE_DAY<=$lastfollowup){

                    $followUpStatusFlag='#3F51B5';

                  }

                  if($START_OF_THE_DAY>$lastfollowup && $END_OF_THE_DAY>$lastfollowup){

                    $followUpStatusFlag='#F44336';

                  }
                  
                  if($START_OF_THE_DAY<=$lastfollowup && $END_OF_THE_DAY>$lastfollowup ){

                    $followUpStatusFlag='#4CAF50';

                  }

                } else {

                  $followUpStatusFlag='#ff9800';

                }
                    
                $cifo      = mysqli_fetch_assoc(mysqli_query($con,"SELECT * FROM customermaster WHERE customerid='$customerid'"));

        ?>

                <div class="col-sm-1 td" style="word-wrap:break-word;">

                  <?php echo $row['invoice_bill']; ?>

                </div>

                <div class="col-sm-3 td">

                    <?php 

                        if ($cifo == '') {

                        echo 'Company : '.$customerid;
                        
                        } else {

                        echo 'Company : '.$cifo['customername']; 
                        echo '<br>Address : '.get_object_vars(json_decode($cifo['customerjson']))['address']; 
                        echo '<br>Name  : '.get_object_vars(json_decode($cifo['contactjson'])[0])['name'];
                        echo '<br>Dept. : '.get_object_vars(json_decode($cifo['contactjson'])[0])['department'];
                        echo '<br>Mobile : <a href="tel:'.get_object_vars(json_decode($cifo['contactjson'])[0])['mobile'].'">'.get_object_vars(json_decode($cifo['contactjson'])[0])['mobile'].'</a>';
                        echo '<br>Landline : '.get_object_vars(json_decode($cifo['contactjson'])[0])['landline'];
                        echo '<br>Email : <a href="mailto:'.get_object_vars(json_decode($cifo['customerjson']))['emailId'].'">'.get_object_vars(json_decode($cifo['customerjson']))['emailId'].'</a>';

                        }

                    ?>

                </div>

                <div class="col-sm-3 td" style="word-wrap:break-word;">

                    <?php
                        
                        if ($row['created_time']<>0) {
                            
                            echo '<table class="table table-list">';

                              echo '<thead>';

                                echo '<tr>';

                                  echo '<th colspan="3">Service Details</th>';

                                echo '</tr>';

                                if ($row['category']=='Pest Control') {
                                
                                  echo '<tr>';

                                    echo '<th>Service Name</th>';

                                    echo '<th>yesno</th>';

                                    echo '<th>frq</th>';

                                  echo '</tr>';

                                echo '</thead>';

                                echo '<tbody>';

                                  $service =  json_decode($row['service_json']);

                                    foreach ($service as $i) {

                                      echo '<tr>';

                                        $frq   = get_object_vars($i)['frq'];
            
                                        if ($frq<>'') {

                                          echo '<td align="center">'.get_object_vars($i)['serv'].'</td>';

                                          echo '<td align="center">'.get_object_vars($i)['yesno'].'</td>';

                                          echo '<td align="center">'.get_object_vars($i)['frq'].'</td>';

                                        }

                                      echo '</tr>';

                                    }  

                                echo '</tbody>';
                                
                              } else {
                                
                                echo '<tr>';

                                  echo '<th>Service Name</th>';

                                  echo '<th>Sac</th>';

                                  echo '<th>Amount</th>';

                                echo '</tr>';

                              echo '</thead>';

                              echo '<tbody>';

                                $service =  json_decode($row['service_json']);

                                  foreach ($service as $i) {

                                    echo '<tr>';

                                      echo '<td align="center">'.get_object_vars($i)['servicename'].'</td>';

                                      echo '<td align="center">'.get_object_vars($i)['sac'].'</td>';

                                      echo '<td align="center">'.get_object_vars($i)['amount'].'</td>';


                                    echo '</tr>';

                                  }  

                              echo '</tbody>';


                              }

                            echo '</table>';


                            echo 'Bill Date : <p style="background:'.$followUpStatusFlag.';color:#fff;">'.date("d-m-Y",$billDate/1000).'</p>'; 

                          
                        }

                    ?>

                </div>

                <div class="col-sm-3 td" style="word-wrap:break-word;">

                  <?php
                  
                    if ($row['created_time']<>0) {

                      echo 'Contract Amount : ' .$row['contractamt'];


                      echo '<table class="table table-list">';

                          echo '<thead>';

                            echo '<tr>';

                              echo '<th>BILL AMT</th>';

                              echo '<th>CGST%</th>';

                              echo '<th>CGST Amt.</th>';

                              echo '<th>SGST%</th>';

                              echo '<th>SGST Amt.</th>';

                              echo '<th>IGST%</th>';

                              echo '<th>IGST Amt.</th>';

                            echo '</tr>';

                          echo '</thead>';

                          echo '<tbody>';

                            echo '<tr>';

                              echo '<td align="center">'.$row['total_amount'].'</td>';

                              echo '<td align="center">'.$row['cgstper'].'</td>';

                              echo '<td align="center">'.$row['cgstamt'].'</td>';

                              echo '<td align="center">'.$row['sgstper'].'</td>';

                              echo '<td align="center">'.$row['sgstamt'].'</td>';

                              echo '<td align="center">'.$row['igstper'].'</td>';

                              echo '<td align="center">'.$row['igstamt'].'</td>';
                              
                             echo '</tr>';

                          echo '</tbody>';

                      echo '</table>';


                    }  


                  ?>

                </div>

                <div class="col-sm-2 td" >

                    <?php

                        if ($row['category']=='Fumigation') {

                          if ($row['created_time']==0) {

                    ?>

                            <a href="fumigationBill.php?invoice_bill=<?php echo $row['invoice_bill'];?>&customerid=<?php echo $customerid;?>" class="btn btn-sm btn-primary btn-block">Fumigation Bill</a>


                    <?php
                          } else {
                    ?>

                            <a href="fumigationBill.php?invoice_bill=<?php echo $row['invoice_bill'];?>&customerid=<?php echo $customerid;?>&edit=true" class="btn btn-sm btn-primary btn-block">Edit</a>

                            <button data-invoice_bill="<?php echo $row['invoice_bill'];?>" data-branchid="<?php echo $row['branchid'];?>" class="btn btn-sm btn-info btn-block" onclick="printfumPdf(this);">Print Bill</button>

                            <button data-invoice_bill="<?php echo $row['invoice_bill'];?>" data-category="<?php echo $row['category'];?>" data-branchid="<?php echo $row['branchid'];?>" data-emailId="<?php echo $custEmail;?>" class="btn btn-sm btn-warning btn-block" onclick="TriggerOutlook(this)">Mail Pdf</a>

                    <?php
                          }

                        } else {

                          if ($row['created_time']==0) {
                    
                    ?>
                        <a href="pestControlBill.php?invoice_bill=<?php echo $row['invoice_bill'];?>&customerid=<?php echo $customerid;?>" class="btn btn-sm btn-primary btn-block">Pest Control Bill</a>

                    <?php
                          } else {

                    ?>
                        <a href="pestControlBill.php?invoice_bill=<?php echo $row['invoice_bill'];?>&customerid=<?php echo $customerid;?>&edit=true" class="btn btn-sm btn-primary btn-block">Edit</a>

                        <button data-invoice_bill="<?php echo $row['invoice_bill'];?>" data-branchid="<?php echo $row['branchid'];?>" class="btn btn-sm btn-info btn-block" onclick="printPdf(this);">Print Bill</button>

                        <button data-invoice_bill="<?php echo $row['invoice_bill'];?>" data-category="<?php echo $row['category'];?>" data-branchid="<?php echo $row['branchid'];?>" data-emailId="<?php echo $custEmail;?>" class="btn btn-sm btn-warning btn-block" onclick="TriggerOutlook(this)">Mail Pdf</a>

                    <?php        
                            
                          }

                        } 
                    
                    ?>

                        <button class="btn btn-sm btn-danger btn-block" data-BillNo="<?php echo $row['invoice_bill'];?>" data-branchid="<?php echo $row['branchid'];?>" onclick="deletebill(this)">Delete Bill</button>


                </div>

        <?php        
                
            }
        
        ?>

    </div>

</div>

<button class="btn" style="font-size:25px;border-radius:50px;width:60px;height:60px;position: fixed;bottom: 40px;
    right: 40px;background:#eb2f06;color:#fff;" data-toggle="modal" data-target="#myModal">+</button>


<div id="myModal" class="modal fade" role="dialog">

    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">

            <div class="modal-header">

                <h4 class="modal-title" align="center">New Bill</h4>

            </div>

            <div class="modal-body">

                <div class="form-horizontal">

                    <div class="form-group">

                        <label class="control-label col-sm-4" for="email">Select Customer :</label>

                        <div class="col-sm-8">

                            <input type="text" list="customerid"  data-name='customerid' data-role="text" class="form-control input-sm" />
            
                            <datalist id="customerid" >

                                <?php

                                    $result= mysqli_query($con,"SELECT customerid,customername  FROM customermaster ORDER by customerid ASC ");

                                    while($rows = mysqli_fetch_assoc($result)){

                                        echo '<option value="'.$rows['customerid'].' '.$rows['customername'].'"></option>';


                                    }
                                
                                ?>

                            </datalist>

                        </div>
                    </div>


                    <div class="form-group">

                        <label class="control-label col-sm-4" for="email">Select Category </label>

                        <div class="col-sm-8">

                            <select data-role="select" data-name='category' class="form-control input-sm">

                                <option value="Fumigation">Fumigation</option>

                                <option value="Pest Control">Pest Control</option>

                             </select>   
                        </div>

                    </div>


                    <div class="form-group">

                        <div class="col-sm-offset-2 col-sm-10">

                            <button class="btn btn-success btn-block btn-sm" id="newqneuiry" onclick="newBill()">Add
                                New Bill</button>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

</div>

<?php 
include('footer.php');
?>

<script>

function newBill() {

  var valid = true;

  if (checker('myModal') != false) {

    valid = valid * true;

  } else {

    alert('Please Select Customer');

    valid = valid * false;

  }

  if (valid) {

    var data = checker('myModal');

    var datastr = JSON.stringify(data);

    $.ajax({

      type: "POST",

      data: {

        data: datastr

      },

      url: 'api/setBill.php',

      cache: false,

      success: function (res) {

        if (res.status == 'success') {

          window.location.reload();

        }

      }

    })

  }

}


function deletebill(e) {

  var BillNo = $(e).attr('data-BillNo');

  var branchid = $(e).attr('data-branchid');


  $.ajax({

    type: "POST",

    data: 'invoice_bill=' + BillNo + '&branchid=' + branchid,

    url: 'api/deleteBill.php',

    cache: false,

    success: function (res) {

      if (res.status == 'success') {

        window.location.reload();

      }

    }

  })

}



function printPdf(e) {

  var invoice_bill = $(e).attr('data-invoice_bill');

  var branchId = $(e).attr('data-branchid');

  $.ajax({

    type: "POST",

    data: "branchId=" + branchId + "&invoice_bill=" + invoice_bill,

    url: 'api/manualBillSelect.php',

    cache: false,

    success: function (res) {

      if (res.status == 'success') {

        var content = [];

        var json = res.json[0];


        content.push({
          text: 'PEST MORTEM (INDIA) PRIVATE LIMITED',
          style: 'header'
        });
        content.push({
          text: '(Approved by National Plant Protection Organization, Goverment of India)',
          style: 'subheader'
        });
        content.push({
          text: 'AN ISO 9001 : 2008 CERTIFIED COMPANY',
          style: 'subheader1'
        });
        content.push({
          text: json.address + '\n GST NO : ' + json.branchgstnumber,
          alignment: 'center',
          fontSize: 11
        });
        content.push({
          text: 'EXPERT IN',
          alignment: 'center',
          fontSize: 11,
          bold: true
        });
        content.push({
          text: 'PEST CONTROL SERVICES AND FUMIGATION OF EXPORT CARGO, SHIPS, CONTAINERS',
          style: 'subheader'
        });

        content.push({
          text: '\n'
        });
        content.push({
          columns: [{
            text: 'Bill No. : ' + invoice_bill,
            fontSize: 9,
            bold: true
          }, {
            text: 'Bill Date : ' + convertDate(json.issuedate),
            alignment: 'right',
            fontSize: 9,
            bold: true
          }]
        });
        content.push({
          table: {
            widths: ['*'],
            body: [
              [{
                text: 'Residential Pest Control Service Agreement',
                alignment: 'center',
                fontSize: 10,
                bold: true
              }],
            ]
          }
        });

        content.push({
          table: {
            widths: ['*', 10, '*'],
            body: [
              [{
                text: 'Purchaser',
                alignment: 'center',
                fontSize: 10,
                bold: true,
                border: [false, false, false, true]
              }, {
                text: '',
                border: [false, false, false, false]
              }, {
                text: 'Premises',
                alignment: 'center',
                fontSize: 10,
                bold: true,
                border: [false, false, false, true]
              }],
              [{
                text: 'Name     : ' + json.custName,
                fontSize: 10,
                border: [false, false, false, true]
              }, {
                text: '',
                border: [false, false, false, false]
              }, {
                text: 'Name     : ' + json.premisesName,
                fontSize: 10,
                border: [false, false, false, true]
              }],
              [{
                text: 'Address : ' + json.custAddress,
                fontSize: 10,
                border: [false, false, false, true]
              }, {
                text: '',
                border: [false, false, false, false]
              }, {
                text: 'Address : ' + json.premisesAddress,
                fontSize: 10,
                border: [false, false, false, true]
              }],
              [{
                text: 'GST No  : ' + json.custGst,
                fontSize: 10,
                border: [false, false, false, true]
              }, {
                text: '',
                border: [false, false, false, false]
              }, {
                text: 'GST No  : ' + json.premisesGst,
                fontSize: 10,
                border: [false, false, false, true]
              }],
              [{
                text: 'Email      : ' + json.custEmail,
                fontSize: 10,
                border: [false, false, false, true]
              }, {
                text: '',
                border: [false, false, false, false]
              }, {
                text: 'Email      : ' + json.premisesEmail,
                fontSize: 10,
                border: [false, false, false, true]
              }],
              [{
                text: 'Phone     : ' + json.custmobile,
                fontSize: 10,
                border: [false, false, false, true]
              }, {
                text: '',
                border: [false, false, false, false]
              }, {
                text: 'Phone     : ' + json.premisesmobile,
                fontSize: 10,
                border: [false, false, false, true]
              }],
            ]
          }
        });


        content.push({
          text: '\nThis agreement is for an initial period of (' + convertDate(json.todate) + ' TO ' + convertDate(json.fromdate) + ') from the date of the frist service and, unless canceled by the purchaser, will automatically continue on a monthly basis until canceled by either party upon thirty days notice.\n\n',
          fontSize: 9,
          bold: true
        });
        content.push({
          table: {
            widths: ['*'],
            body: [
              [{
                text: 'BASIC COVERAGE - TREATMENT OR INSPECTION FOR ',
                alignment: 'center',
                fontSize: 9,
                bold: true
              }],
            ]
          }
        });

        var table1 = {

          widths: ['*', 'auto', 'auto'],
          dontBreakRows: true,
          body: []

        };

        var table2 = {

          widths: ['*', 'auto', 'auto'],
          dontBreakRows: true,
          body: []

        };

        var header = ['Type of Services', 'Yes/No', 'Quantity'];
        var header1 = ['Type of Services', 'Yes/No', 'Quantity'];

        table1['body'].push(header);
        table2['body'].push(header1);

        var services = json.service_json;
        var serviceslen = parseInt(services.length / 2);
        var counter = 0;
        for (var i in services) {
          var arr = [];
          if (services[i].frq == '') {

            var frq = 'Nill';

          } else {
            var frq = services[i].frq;
          }
          arr.push(services[i].serv, services[i].yesno, frq);
          if (serviceslen <= counter) {
            table2['body'].push(arr);
          } else {
            table1['body'].push(arr);
          }
          counter++;
        }
        content.push({
          columns: [{
            style: 'tablfont',
            table: table1,
            layout: 'noBorders'
          }, {
            width: 10,
            text: ''
          }, {
            style: 'tablfont',
            table: table2,
            layout: 'noBorders'
          }]
        })
        content.push({
          text: '\n'
        });

        content.push({
          table: {
            widths: ['*'],
            body: [
              [{
                text: 'INSTALLATION OF EQUIPMENT',
                alignment: 'center',
                fontSize: 9,
                bold: true
              }],
            ]
          }
        });

        var table3 = {

          widths: ['*', '*', '*'],
          dontBreakRows: true,
          body: []

        };


        var header2 = ['Type of Equipments', 'Returnable', 'Quantity'];

        table3['body'].push(header2);

        var enquipmentjson = json.machine_json;
        for (var i in enquipmentjson) {
          var arr = [];
          arr.push(enquipmentjson[i].machinename, enquipmentjson[i].returnable, enquipmentjson[i].machinefrq);
          table3['body'].push(arr);
        }

        content.push({
          columns: [{
            style: 'tablfont',
            width: 500,
            table: table3,
            layout: 'noBorders'
          }]
        })
        content.push({
          text: '\n'
        });

        content.push({
          table: {
            widths: ['*', 100, 100],
            body: [
              [{
                text: 'PAYMENT DETAILS',
                colSpan: 3,
                alignment: 'center',
                fontSize: 9,
                bold: true
              }, {}, {}],
              [{
                rowSpan: 5,
                text: 'Mode of payment : ' + json.modOfPayment + '\nAdditional Declaration (if any)\n' + json.addDeclaration,
                fontSize: 9,
                bold: true
              }, {
                text: 'SERVICE CHARGES',
                colSpan: 2,
                border: [true, true, true, true],
                alignment: 'center',
                fontSize: 9,
                bold: true
              }, {}],
              ['', {
                text: 'Amount',
                fontSize: 9,
                bold: true
              }, {
                text: json.amount,
                fontSize: 9,
                bold: true,
                alignment: 'right'
              }],
              ['', {
                text: 'CGST ' + json.cgstPer + ' % ',
                fontSize: 9,
                bold: true
              }, {
                text:  json.cgstAmt,
                fontSize: 9,
                bold: true,
                alignment: 'right'
              }],
              ['', {
                text: 'SGST ' + json.sgstPer + ' % ',
                fontSize: 9,
                bold: true
              }, {
                text: json.sgstAmt,
                fontSize: 9,
                bold: true,
                alignment: 'right'
              }],
              ['', {
                text: 'IGST ' + json.igstPer + ' % ',
                fontSize: 9,
                bold: true,
              }, {
                text: json.igstAmt,
                fontSize: 9,
                bold: true,
                alignment: 'right'
              }],
              ['', {
                text: 'Net Amount',
                fontSize: 9,
                bold: true
              }, {
                text: json.netAmount,
                fontSize: 9,
                bold: true,
                alignment: 'right'

              }],
              [{
                text: convertNumberToWords(json.netAmount) + 'Only',
                colSpan: 3,
                fontSize: 9,
                bold: true,
                alignment: 'center'
              }, {}, {}],



            ]
          }
        });


        content.push({
          table: {
            headerRows: 1,
            widths: ['*'],
            body: [
              [''],
              ['']
            ]
          },
          layout: 'headerLineOnly'
        }); //LINE CODE

        content.push({
          text: 'Bank Name : ' + json.bankName + ',Branch Name : ' + json.branchName + ',IFSC Code : ' + json.ifscCode + ',Account No : ' + json.accountNo,
          fontSize: 9,
          bold: true
        });
        content.push({
          columns: [{
            text: '\n\nSignature\n\n\nFOR OWNER \n PEST MORTEM (INDIA) PVT.LTD',
            fontSize: 9,
            bold: true
          }, {
            text: '\n\nSignature\n\n\nFOR PURCHASER  \n ' + json.custName,
            alignment: 'right',
            fontSize: 9,
            bold: true
          }]
        })

        dd = {

          pageSize: 'A4',

          pageOrientation: 'portrait',

          pageMargins: [40, 30, 30, 35],

          footer: function (currentPage, pageCount) {

            return {

              margin: 10,

              columns: [{

                fontSize: 9,

                text: [{

                    text: '--------------------------------------------------------------------------' +
                      '\n',
                    margin: [0, 20]

                  },
                  {

                    text: '© Pest Mortem (India) PVT. LTD. | PAGE ' + currentPage.toString() + ' of ' + pageCount,

                  }

                ],

                alignment: 'center'

              }]


            };

          },

          content,
          styles: {

            tablfont: {

              fontSize: 9

            },

            tablfont1: {

              fontSize: 9

            },

            header: {

              fontSize: 16,
              bold: true,
              alignment: 'center',

            },

            subheader: {

              fontSize: 8,
              bold: true,
              alignment: 'center',

            },
            subheader1: {

              fontSize: 8,
              bold: true,
              alignment: 'center',

            },

            subheader2: {

              fontSize: 10,
              bold: true,

            },

          }

        }

        var win = window.open('', '_blank');

        pdfMake.createPdf(dd).open({}, win);


      }

    }

  })

}



function printfumPdf(e) {

  var invoice_bill = $(e).attr('data-invoice_bill');

  var branchId = $(e).attr('data-branchid');

  $.ajax({

    type: "POST",

    data: "branchId=" + branchId + "&invoice_bill=" + invoice_bill,

    url: 'api/fumigationBillSelect.php',

    cache: false,

    success: function (res) {

      if (res.status == 'success') {

        var content = [];

        var json = res.json[0];


        content.push({
          text: 'PEST MORTEM (INDIA) PRIVATE LIMITED',
          style: 'header'
        });
        content.push({
          text: '(Approved by National Plant Protection Organization, Goverment of India)',
          style: 'subheader'
        });
        content.push({
          text: 'AN ISO 9001 : 2008 CERTIFIED COMPANY',
          style: 'subheader1'
        });
        content.push({
          text: json.address + '\n GST NO : ' + json.branchgstnumber,
          alignment: 'center',
          fontSize: 11
        });
        content.push({
          text: 'EXPERT IN',
          alignment: 'center',
          fontSize: 11,
          bold: true
        });
        content.push({
          text: 'PEST CONTROL SERVICES AND FUMIGATION OF EXPORT CARGO, SHIPS, CONTAINERS',
          style: 'subheader'
        });

        content.push({
          text: '\n'
        });
        content.push({
          columns: [{
            text: 'Bill No. : ' + invoice_bill,
            fontSize: 9,
            bold: true
          }, {
            text: 'Bill Date : ' + convertDate(json.issuedate),
            alignment: 'right',
            fontSize: 9,
            bold: true
          }]
        });
        content.push({
          table: {
            widths: ['*'],
            body: [
              [{
                text: 'Regd. Offce : G -2, Sunder Tower, T.J. Road, Sewree(west),Mumbai - 400015 \n Phone : 24147425/24127935/24111976/24149566/65531746/47 . Fax : 91-22-24150261. \n E-Mail : pestmort@bom3.vsnl.net.in . pestmortem@pestmortem@pestmortem.com. \n Website : www.pestmortem.com',
                alignment: 'center',
                fontSize: 10,
                bold: true
              }],
            ]
          }
        });

        content.push({
          table: {
            widths: ['*', 10, '*'],
            body: [
              [{
                text: 'Purchaser',
                alignment: 'center',
                fontSize: 10,
                bold: true,
                border: [false, false, false, true]
              }, {
                text: '',
                border: [false, false, false, false]
              }, {
                text: 'Premises',
                alignment: 'center',
                fontSize: 10,
                bold: true,
                border: [false, false, false, true]
              }],
              [{
                text: 'Name     : ' + json.custName,
                fontSize: 10,
                border: [false, false, false, true]
              }, {
                text: '',
                border: [false, false, false, false]
              }, {
                text: 'Name     : ' + json.premisesName,
                fontSize: 10,
                border: [false, false, false, true]
              }],
              [{
                text: 'Address : ' + json.custAddress,
                fontSize: 10,
                border: [false, false, false, true]
              }, {
                text: '',
                border: [false, false, false, false]
              }, {
                text: 'Address : ' + json.premisesAddress,
                fontSize: 10,
                border: [false, false, false, true]
              }],
              [{
                text: 'GST No  : ' + json.custGst,
                fontSize: 10,
                border: [false, false, false, true]
              }, {
                text: '',
                border: [false, false, false, false]
              }, {
                text: 'GST No  : ' + json.premisesGst,
                fontSize: 10,
                border: [false, false, false, true]
              }],
              [{
                text: 'Email      : ' + json.custEmail,
                fontSize: 10,
                border: [false, false, false, true]
              }, {
                text: '',
                border: [false, false, false, false]
              }, {
                text: 'Email      : ' + json.premisesEmail,
                fontSize: 10,
                border: [false, false, false, true]
              }],
              [{
                text: 'Phone     : ' + json.custmobile,
                fontSize: 10,
                border: [false, false, false, true]
              }, {
                text: '',
                border: [false, false, false, false]
              }, {
                text: 'Phone     : ' + json.premisesmobile,
                fontSize: 10,
                border: [false, false, false, true]
              }],
            ]
          }
        });

        content.push({
          text: '\n'
        });
  
        content.push({
          table: {
            widths: ['*'],
            body: [
              [{
                text: 'TYPE OF SERVICES ',
                alignment: 'center',
                fontSize: 9,
                bold: true,
                border: [true, true, true, false]
              }],
            ]
          }
        });


        var table3 = {

          widths: ['*', 80, 80],
          dontBreakRows: true,
          body: []

        };


        var header2 = ['Service Name', 'Sac', 'Amount'];

        table3['body'].push(header2);

        var enquipmentjson = json.service_json;
        for (var i in enquipmentjson) {
          var arr = [];
          arr.push(enquipmentjson[i].servicename, enquipmentjson[i].sac, enquipmentjson[i].amount);
          table3['body'].push(arr);
        }

        content.push({
          columns: [{
            style: 'tablfont',
            table: table3,
          }]
        })
        content.push({
          text: '\n'
        });

        content.push({
          table: {
            widths: ['*', 100, 100],
            body: [
              [{
                text: 'PAYMENT DETAILS',
                colSpan: 3,
                alignment: 'center',
                fontSize: 9,
                bold: true
              }, {}, {}],
              [{
                rowSpan: 6,
                text: 'Mode of payment : ' + json.modOfPayment + '\nAdditional Declaration (if any)\n' + json.addDeclaration,
                fontSize: 9,
                bold: true
              }, {
                text: 'SERVICE CHARGES',
                colSpan: 2,
                border: [true, true, true, true],
                alignment: 'center',
                fontSize: 9,
                bold: true
              }, {}],
              ['', {
                text: 'Amount',
                fontSize: 9,
                bold: true
              }, {
                text: json.amount,
                fontSize: 9,
                bold: true,
                alignment: 'right'
              }],
              ['', {
                text: 'CGST ' + json.cgstPer + ' % ',
                fontSize: 9,
                bold: true
              }, {
                text:  json.cgstAmt,
                fontSize: 9,
                bold: true,
                alignment: 'right'
              }],
              ['', {
                text: 'SGST ' + json.sgstPer + ' % ',
                fontSize: 9,
                bold: true
              }, {
                text: json.sgstAmt,
                fontSize: 9,
                bold: true,
                alignment: 'right'
              }],
              ['', {
                text: 'IGST ' + json.igstPer + ' % ',
                fontSize: 9,
                bold: true,
              }, {
                text: json.igstAmt,
                fontSize: 9,
                bold: true,
                alignment: 'right',
              }],
              ['', {
                text: 'Net Amount',
                fontSize: 9,
                bold: true
              }, {
                text: json.netAmount,
                fontSize: 9,
                bold: true,
                alignment: 'right'
              }],
              [{
                text: convertNumberToWords(json.netAmount) + 'Only',
                colSpan: 3,
                fontSize: 9,
                bold: true,
                alignment: 'center'
              }, {}, {}],



            ]
          }
        });


        content.push({
          table: {
            headerRows: 1,
            widths: ['*'],
            body: [
              [''],
              ['']
            ]
          },
          layout: 'headerLineOnly'
        }); //LINE CODE

        content.push({
          text: 'Bank Name : ' + json.bankName + ',Branch Name : ' + json.branchName + ',IFSC Code : ' + json.ifscCode + ',Account No : ' + json.accountNo,
          fontSize: 9,
          bold: true
        });
        content.push({
          columns: [{
            text: '\n\nSignature\n\n\nFOR OWNER \n PEST MORTEM (INDIA) PVT.LTD',
            fontSize: 9,
            bold: true
          }, {
            text: '\n\nSignature\n\n\nFOR PURCHASER  \n ' + json.custName,
            alignment: 'right',
            fontSize: 9,
            bold: true
          }]
        })

        dd = {

          pageSize: 'A4',

          pageOrientation: 'portrait',

          pageMargins: [40, 30, 30, 35],

          footer: function (currentPage, pageCount) {

            return {

              margin: 10,

              columns: [{

                fontSize: 9,

                text: [{

                    text: '--------------------------------------------------------------------------' +
                      '\n',
                    margin: [0, 20]

                  },
                  {

                    text: '© Pest Mortem (India) PVT. LTD. | PAGE ' + currentPage.toString() + ' of ' + pageCount,

                  }

                ],

                alignment: 'center'

              }]


            };

          },

          content,
          styles: {

            tablfont: {

              fontSize: 9

            },

            tablfont1: {

              fontSize: 9

            },

            header: {

              fontSize: 16,
              bold: true,
              alignment: 'center',

            },

            subheader: {

              fontSize: 8,
              bold: true,
              alignment: 'center',

            },
            subheader1: {

              fontSize: 8,
              bold: true,
              alignment: 'center',

            },

            subheader2: {

              fontSize: 10,
              bold: true,

            },

          }

        }

        var win = window.open('', '_blank');

        pdfMake.createPdf(dd).open({}, win);


      }

    }

  })

}


function convertDate(inputFormat) {

  var d = new Date(inputFormat);

  var n = d.getMonth() + 1;

  if (n < 10) {

    var month = '0' + n;

  } else {

    var month = n;

  }

  var year = d.getFullYear();

  var da = d.getDate();

  if (da < 10) {

    var date = '0' + da;

  } else {

    var date = da;

  }

  return date + '-' + month + '-' + year;

}



function TriggerOutlook(e) {

  var email        = $(e).attr('data-emailId');
  var invoice_bill = $(e).attr('data-invoice_bill');
  var branchId     = $(e).attr('data-branchid');
  var category     = $(e).attr('data-category');


  var valid = true;

  var regemail = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;

  if (!regemail.test(email)) {

    valid = valid * false;

    alert('Pls Check Customer Email id');

  } else {

    valid = valid * true;

  }

  if (category == 'Pest Control') {

    //var body = href = "http://pestmorten.local/process/crm/pestControlemail.php?invoice_bill=" + invoice_bill; localhost
    var body = href = "http://pestmortem.co.in/process/crm/pestControlemail.php?invoice_bill=" + invoice_bill;
  } else {

   // var body = href = "http://pestmorten.local/process/crm/fumigationemail.php?invoice_bill=" + invoice_bill;
    var body = href = "http://pestmortem.co.in/process/crm/fumigationemail.php?invoice_bill=" + invoice_bill;


  }
  // var body = href = "http://gymerp.epizy.com/admin/purchaseandsale/email.php?pono=" + pono;

  var subject = "Pest Mortem (India) Pvt. Ltd.";
  var TO      = email;

  if (valid) {

    window.location.href = "mailto: " + TO + "?body=Pls Click Link And Download This Purchase Order To Given Below %0D%0A%0D%0A" + body + "&subject=" + subject;

    window.location.reload();

  }
  
}


// function setidtomidal(e) {
//   var billid = $(e).data('billid');
//   var billamt = $(e).data('billamt');

//   $('#billidset').val(billid);
//   $('#billid').val(billid);
//   $('#totalpaidamt').val(billamt);


// }

// function setidtomidal2(e) {
//   var billid = $(e).data('billid');
//   $('#billid2').val(billid);

// }

// function confirmbill(billid) {
//   var valid = true;

//   if (checker('myModal') != false) {
//     valid = valid * true;
//   } else {
//     valid = valid * false;
//   }
//   if (valid) {
//     var data = checker('myModal');
//     var datastr = JSON.stringify(data);

//     $.ajax({
//       type: "POST",
//       data: {
//         data: datastr
//       },
//       url: 'api/confirmbill.php',
//       cache: false,
//       success: function (res) {
//         // alert(res.status);
//         if (res.status == 'success') {

//           window.location = "payment.php";
//         }
//       }
//     })
//   }

// }

// function followup() {
//   var valid = true;
//   if (checker('myModal2') != false) {
//     valid = valid * true;
//   } else {
//     valid = valid * false;
//   }
//   if (valid) {
//     var data = checker('myModal2');
//     var datastr = JSON.stringify(data);
//     $.ajax({
//       type: "POST",
//       data: {
//         data: datastr
//       },
//       url: 'api/setfollowup.php',
//       cache: false,
//       success: function (res) {
//         if (res.status == 'success') {
//           window.location.reload();
//         }
//       }
//     })
//   }
// }



function convertNumberToWords(amount) {
  var words = new Array();
  words[0] = '';
  words[1] = 'One';
  words[2] = 'Two';
  words[3] = 'Three';
  words[4] = 'Four';
  words[5] = 'Five';
  words[6] = 'Six';
  words[7] = 'Seven';
  words[8] = 'Eight';
  words[9] = 'Nine';
  words[10] = 'Ten';
  words[11] = 'Eleven';
  words[12] = 'Twelve';
  words[13] = 'Thirteen';
  words[14] = 'Fourteen';
  words[15] = 'Fifteen';
  words[16] = 'Sixteen';
  words[17] = 'Seventeen';
  words[18] = 'Eighteen';
  words[19] = 'Nineteen';
  words[20] = 'Twenty';
  words[30] = 'Thirty';
  words[40] = 'Forty';
  words[50] = 'Fifty';
  words[60] = 'Sixty';
  words[70] = 'Seventy';
  words[80] = 'Eighty';
  words[90] = 'Ninety';
  amount = amount.toString();
  var atemp = amount.split(".");
  var number = atemp[0].split(",").join("");
  var n_length = number.length;
  var words_string = "";
  if (n_length <= 9) {
    var n_array = new Array(0, 0, 0, 0, 0, 0, 0, 0, 0);
    var received_n_array = new Array();
    for (var i = 0; i < n_length; i++) {
      received_n_array[i] = number.substr(i, 1);
    }
    for (var i = 9 - n_length, j = 0; i < 9; i++, j++) {
      n_array[i] = received_n_array[j];
    }
    for (var i = 0, j = 1; i < 9; i++, j++) {
      if (i == 0 || i == 2 || i == 4 || i == 7) {
        if (n_array[i] == 1) {
          n_array[j] = 10 + parseInt(n_array[j]);
          n_array[i] = 0;
        }
      }
    }
    value = "";
    for (var i = 0; i < 9; i++) {
      if (i == 0 || i == 2 || i == 4 || i == 7) {
        value = n_array[i] * 10;
      } else {
        value = n_array[i];
      }
      if (value != 0) {
        words_string += words[value] + " ";
      }
      if ((i == 1 && value != 0) || (i == 0 && value != 0 && n_array[i + 1] == 0)) {
        words_string += "Crores ";
      }
      if ((i == 3 && value != 0) || (i == 2 && value != 0 && n_array[i + 1] == 0)) {
        words_string += "Lakhs ";
      }
      if ((i == 5 && value != 0) || (i == 4 && value != 0 && n_array[i + 1] == 0)) {
        words_string += "Thousand ";
      }
      if (i == 6 && value != 0 && (n_array[i + 1] != 0 && n_array[i + 2] != 0)) {
        words_string += "Hundred and ";
      } else if (i == 6 && value != 0) {
        words_string += "Hundred ";
      }
    }
    words_string = words_string.split("  ").join(" ");
  }
  return words_string;
}
</script>