<!DOCTYPE html>
<html lang="en">
<head>
  <title>Pest Mortem (India) Pvt. Ltd.</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="/js/pdfmake.min.js"></script>
<script src="/js/vfs_fonts.js"></script>
</head>
<body>
<div class="container-fluid">
<center>
<h1>Thank You For Download..!!</h1>
<h3>(Pest Mortem (India) Pvt. Ltd.)</h3>
</center>

<script>

function withDecimal(s) {
  n = Math.round(s * 100) / 100;
  var nums = n.toString().split('.')
  var whole = convertNumberToWords(nums[0])
  if (nums.length == 2) {
    var res1 = nums[1].slice(0, 1);
    var res2 = nums[1].slice(1, 2);
    var fraction = convertNumberToWords(res1) + '' + convertNumberToWords(res2)
    return whole + 'point ' + fraction;
  } else {
    return whole;
  }
}


function printer() {


  var invoice_bill = location.search.split('invoice_bill=')[1];

  $.ajax({
    type: "POST",
    data: "invoice_bill=" + invoice_bill,

    url: 'api/manualBillSelect.php',

    cache: false,
    success: function (res) {
      if (res.status == 'success') {


        var content = [];

        var json = res.json[0];


        content.push({
          text: 'PEST MORTEM (INDIA) PRIVATE LIMITED',
          style: 'header'
        });
        content.push({
          text: '(Approved by National Plant Protection Organization, Goverment of India)',
          style: 'subheader'
        });
        content.push({
          text: 'AN ISO 9001 : 2008 CERTIFIED COMPANY',
          style: 'subheader1'
        });
        content.push({
          text: json.address + '\n GST NO : ' + json.branchgstnumber,
          alignment: 'center',
          fontSize: 11
        });
        content.push({
          text: 'EXPERT IN',
          alignment: 'center',
          fontSize: 11,
          bold: true
        });
        content.push({
          text: 'PEST CONTROL SERVICES AND FUMIGATION OF EXPORT CARGO, SHIPS, CONTAINERS',
          style: 'subheader'
        });

        content.push({
          text: '\n'
        });
        content.push({
          columns: [{
            text: 'Bill No. : ' + invoice_bill,
            fontSize: 9,
            bold: true
          }, {
            text: 'Bill Date : ' + convertDate(json.issuedate),
            alignment: 'right',
            fontSize: 9,
            bold: true
          }]
        });
        content.push({
          table: {
            widths: ['*'],
            body: [
              [{
                text: 'Residential Pest Control Service Agreement',
                alignment: 'center',
                fontSize: 10,
                bold: true
              }],
            ]
          }
        });

        content.push({
          table: {
            widths: ['*', 10, '*'],
            body: [
              [{
                text: 'Purchaser',
                alignment: 'center',
                fontSize: 10,
                bold: true,
                border: [false, false, false, true]
              }, {
                text: '',
                border: [false, false, false, false]
              }, {
                text: 'Premises',
                alignment: 'center',
                fontSize: 10,
                bold: true,
                border: [false, false, false, true]
              }],
              [{
                text: 'Name     : ' + json.custName,
                fontSize: 10,
                border: [false, false, false, true]
              }, {
                text: '',
                border: [false, false, false, false]
              }, {
                text: 'Name     : ' + json.premisesName,
                fontSize: 10,
                border: [false, false, false, true]
              }],
              [{
                text: 'Address : ' + json.custAddress,
                fontSize: 10,
                border: [false, false, false, true]
              }, {
                text: '',
                border: [false, false, false, false]
              }, {
                text: 'Address : ' + json.premisesAddress,
                fontSize: 10,
                border: [false, false, false, true]
              }],
              [{
                text: 'GST No  : ' + json.custGst,
                fontSize: 10,
                border: [false, false, false, true]
              }, {
                text: '',
                border: [false, false, false, false]
              }, {
                text: 'GST No  : ' + json.premisesGst,
                fontSize: 10,
                border: [false, false, false, true]
              }],
              [{
                text: 'Email      : ' + json.custEmail,
                fontSize: 10,
                border: [false, false, false, true]
              }, {
                text: '',
                border: [false, false, false, false]
              }, {
                text: 'Email      : ' + json.premisesEmail,
                fontSize: 10,
                border: [false, false, false, true]
              }],
              [{
                text: 'Phone     : ' + json.custmobile,
                fontSize: 10,
                border: [false, false, false, true]
              }, {
                text: '',
                border: [false, false, false, false]
              }, {
                text: 'Phone     : ' + json.premisesmobile,
                fontSize: 10,
                border: [false, false, false, true]
              }],
            ]
          }
        });


        content.push({
          text: '\nThis agreement is for an initial period of (' + convertDate(json.todate) + ' TO ' + convertDate(json.fromdate) + ') from the date of the frist service and, unless canceled by the purchaser, will automatically continue on a monthly basis until canceled by either party upon thirty days notice.\n\n',
          fontSize: 9,
          bold: true
        });
        content.push({
          table: {
            widths: ['*'],
            body: [
              [{
                text: 'BASIC COVERAGE - TREATMENT OR INSPECTION FOR ',
                alignment: 'center',
                fontSize: 9,
                bold: true
              }],
            ]
          }
        });

        var table1 = {

          widths: ['*', 'auto', 'auto'],
          dontBreakRows: true,
          body: []

        };

        var table2 = {

          widths: ['*', 'auto', 'auto'],
          dontBreakRows: true,
          body: []

        };

        var header = ['Type of Services', 'Yes/No', 'Quantity'];
        var header1 = ['Type of Services', 'Yes/No', 'Quantity'];

        table1['body'].push(header);
        table2['body'].push(header1);

        var services = json.service_json;
        var serviceslen = parseInt(services.length / 2);
        var counter = 0;
        for (var i in services) {
          var arr = [];
          if (services[i].frq == '') {

            var frq = 'Nill';

          } else {
            var frq = services[i].frq;
          }
          arr.push(services[i].serv, services[i].yesno, frq);
          if (serviceslen <= counter) {
            table2['body'].push(arr);
          } else {
            table1['body'].push(arr);
          }
          counter++;
        }
        content.push({
          columns: [{
            style: 'tablfont',
            table: table1,
            layout: 'noBorders'
          }, {
            width: 10,
            text: ''
          }, {
            style: 'tablfont',
            table: table2,
            layout: 'noBorders'
          }]
        })
        content.push({
          text: '\n'
        });

        content.push({
          table: {
            widths: ['*'],
            body: [
              [{
                text: 'INSTALLATION OF EQUIPMENT',
                alignment: 'center',
                fontSize: 9,
                bold: true
              }],
            ]
          }
        });

        var table3 = {

          widths: ['*', '*', '*'],
          dontBreakRows: true,
          body: []

        };


        var header2 = ['Type of Equipments', 'Returnable', 'Quantity'];

        table3['body'].push(header2);

        var enquipmentjson = json.machine_json;
        for (var i in enquipmentjson) {
          var arr = [];
          arr.push(enquipmentjson[i].machinename, enquipmentjson[i].returnable, enquipmentjson[i].machinefrq);
          table3['body'].push(arr);
        }

        content.push({
          columns: [{
            style: 'tablfont',
            width: 500,
            table: table3,
            layout: 'noBorders'
          }]
        })
        content.push({
          text: '\n'
        });

        content.push({
          table: {
            widths: ['*', 100, 100],
            body: [
              [{
                text: 'PAYMENT DETAILS',
                colSpan: 3,
                alignment: 'center',
                fontSize: 9,
                bold: true
              }, {}, {}],
              [{
                rowSpan: 6,
                text: 'Mode of payment : ' + json.modOfPayment + '\nAdditional Declaration (if any)\n' + json.addDeclaration,
                fontSize: 9,
                bold: true
              }, {
                text: 'SERVICE CHARGES',
                colSpan: 2,
                border: [true, true, true, true],
                alignment: 'center',
                fontSize: 9,
                bold: true
              }, {}],
              ['', {
                text: 'Amount',
                fontSize: 9,
                bold: true
              }, {
                text: json.amount,
                fontSize: 9,
                bold: true,
                alignment: 'right'
              }],
              ['', {
                text: 'CGST ' + json.cgstPer + ' % ',
                fontSize: 9,
                bold: true
              }, {
                text:  json.cgstAmt,
                fontSize: 9,
                bold: true,
                alignment: 'right'
              }],
              ['', {
                text: 'SGST ' + json.sgstPer + ' % ',
                fontSize: 9,
                bold: true
              }, {
                text: json.sgstAmt,
                fontSize: 9,
                bold: true,
                alignment: 'right'
              }],
              ['', {
                text: 'IGST ' + json.igstPer + ' % ',
                fontSize: 9,
                bold: true,
              }, {
                text: json.igstAmt,
                fontSize: 9,
                bold: true,
                alignment: 'right'
              }],
              ['', {
                text: 'Net Amount',
                fontSize: 9,
                bold: true
              }, {
                text: json.netAmount,
                fontSize: 9,
                bold: true,
                alignment: 'right'

              }],
              [{
                text: convertNumberToWords(json.netAmount) + 'Only',
                colSpan: 3,
                fontSize: 9,
                bold: true,
                alignment: 'center'
              }, {}, {}],



            ]
          }
        });


        content.push({
          table: {
            headerRows: 1,
            widths: ['*'],
            body: [
              [''],
              ['']
            ]
          },
          layout: 'headerLineOnly'
        }); //LINE CODE

        content.push({
          text: 'Bank Name : ' + json.bankName + ',Branch Name : ' + json.branchName + ',IFSC Code : ' + json.ifscCode + ',Account No : ' + json.accountNo,
          fontSize: 9,
          bold: true
        });
        content.push({
          columns: [{
            text: '\n\nSignature\n\n\nFOR OWNER \n PEST MORTEM (INDIA) PVT.LTD',
            fontSize: 9,
            bold: true
          }, {
            text: '\n\nSignature\n\n\nFOR PURCHASER  \n ' + json.custName,
            alignment: 'right',
            fontSize: 9,
            bold: true
          }]
        })

        dd = {

          pageSize: 'A4',

          pageOrientation: 'portrait',

          pageMargins: [40, 30, 30, 35],

          footer: function (currentPage, pageCount) {

            return {

              margin: 10,

              columns: [{

                fontSize: 9,

                text: [{

                    text: '--------------------------------------------------------------------------' +
                      '\n',
                    margin: [0, 20]

                  },
                  {

                    text: '© Pest Mortem (India) PVT. LTD. | PAGE ' + currentPage.toString() + ' of ' + pageCount,

                  }

                ],

                alignment: 'center'

              }]


            };

          },

          content,
          styles: {

            tablfont: {

              fontSize: 9

            },

            tablfont1: {

              fontSize: 9

            },

            header: {

              fontSize: 16,
              bold: true,
              alignment: 'center',

            },

            subheader: {

              fontSize: 8,
              bold: true,
              alignment: 'center',

            },
            subheader1: {

              fontSize: 8,
              bold: true,
              alignment: 'center',

            },

            subheader2: {

              fontSize: 10,
              bold: true,

            },

          }

        }

        // var win = window.open('', '_blank');

        // pdfMake.createPdf(dd).open({}, win);
        pdfMake.createPdf(dd).download(invoice_bill + 'pdf');


      }

    }

  });

}

function convertNumberToWords(amount) {
  var words = new Array();
  words[0] = '';
  words[1] = 'One';
  words[2] = 'Two';
  words[3] = 'Three';
  words[4] = 'Four';
  words[5] = 'Five';
  words[6] = 'Six';
  words[7] = 'Seven';
  words[8] = 'Eight';
  words[9] = 'Nine';
  words[10] = 'Ten';
  words[11] = 'Eleven';
  words[12] = 'Twelve';
  words[13] = 'Thirteen';
  words[14] = 'Fourteen';
  words[15] = 'Fifteen';
  words[16] = 'Sixteen';
  words[17] = 'Seventeen';
  words[18] = 'Eighteen';
  words[19] = 'Nineteen';
  words[20] = 'Twenty';
  words[30] = 'Thirty';
  words[40] = 'Forty';
  words[50] = 'Fifty';
  words[60] = 'Sixty';
  words[70] = 'Seventy';
  words[80] = 'Eighty';
  words[90] = 'Ninety';
  amount = amount.toString();
  var atemp = amount.split(".");
  var number = atemp[0].split(",").join("");
  var n_length = number.length;
  var words_string = "";
  if (n_length <= 9) {
    var n_array = new Array(0, 0, 0, 0, 0, 0, 0, 0, 0);
    var received_n_array = new Array();
    for (var i = 0; i < n_length; i++) {
      received_n_array[i] = number.substr(i, 1);
    }
    for (var i = 9 - n_length, j = 0; i < 9; i++, j++) {
      n_array[i] = received_n_array[j];
    }
    for (var i = 0, j = 1; i < 9; i++, j++) {
      if (i == 0 || i == 2 || i == 4 || i == 7) {
        if (n_array[i] == 1) {
          n_array[j] = 10 + parseInt(n_array[j]);
          n_array[i] = 0;
        }
      }
    }
    value = "";
    for (var i = 0; i < 9; i++) {
      if (i == 0 || i == 2 || i == 4 || i == 7) {
        value = n_array[i] * 10;
      } else {
        value = n_array[i];
      }
      if (value != 0) {
        words_string += words[value] + " ";
      }
      if ((i == 1 && value != 0) || (i == 0 && value != 0 && n_array[i + 1] == 0)) {
        words_string += "Crores ";
      }
      if ((i == 3 && value != 0) || (i == 2 && value != 0 && n_array[i + 1] == 0)) {
        words_string += "Lakhs ";
      }
      if ((i == 5 && value != 0) || (i == 4 && value != 0 && n_array[i + 1] == 0)) {
        words_string += "Thousand ";
      }
      if (i == 6 && value != 0 && (n_array[i + 1] != 0 && n_array[i + 2] != 0)) {
        words_string += "Hundred and ";
      } else if (i == 6 && value != 0) {
        words_string += "Hundred ";
      }
    }
    words_string = words_string.split("  ").join(" ");
  }
  return words_string;
}

function convertDate(inputFormat) {

  var d = new Date(inputFormat);

  var n = d.getMonth() + 1;

  if (n < 10) {

    var month = '0' + n;

  } else {

    var month = n;

  }

  var year = d.getFullYear();

  var da = d.getDate();

  if (da < 10) {

    var date = '0' + da;

  } else {

    var date = da;

  }

  return date + '-' + month + '-' + year;

}
printer();
</script>
</div>
</body>
</html>
