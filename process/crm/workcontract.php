<?php 

    $base    = '../../';

    $navenq2 = 'background:#1B1464;';
    
    include('header.php');

    if (!in_array("Work Contract", $arrysession)) {
    
        echo "<script>window.location.href='/process/dashboard.php';</script>";
    
        exit;

    }

    if (session_status() == PHP_SESSION_NONE) { session_start(); }
    
    $sessionby = $_SESSION['employeeid'];
    
    $branchid  = $_SESSION['branchid'];

    $signature = mysqli_fetch_assoc(mysqli_query($con,"SELECT signature x FROM branchmaster WHERE branchid='$branchid'"))['x'];

?>

<br>

<style>

    .input-container {

        max-width: 300px;
        background-color: #EDEDED;
        border: 1px solid #DFDFDF;
        border-radius: 5px;
    }

    input[type='file'] {
        display: none;
    }

    .file-info {
        font-size: 0.9em;
    }

    .browse-btn {
        background: #03A595;
        color: #fff;
        min-height: 35px;
        padding: 10px;
        border: none;
        border-top-left-radius: 5px;
        border-bottom-left-radius: 5px;
    }

    .browse-btn:hover {
        background: #4ec0b4;
    }

    @media (max-width: 300px) {
        button {
            width: 100%;
            border-top-right-radius: 5px;
            border-bottom-left-radius: 0;
        }

        .file-info {
            display: block;
            margin: 10px 5px;
        }
    }

</style>

<img style="display:none;" id="imageid" src="<?php echo $base;?>img/PestMortemLogo.jpg">

<img style="display:none;" id="signature" src="<?php echo $base.$signature;?>">

<div class="table-ui container-fluid">

  <div class="tr row">

    <div class="col-sm-1 th">Contract No.</div>
    <div class="col-sm-3 th">Customer Info</div>
    <div class="col-sm-1 th">Enquery For</div>
    <div class="col-sm-2 th">Offer</div>
    <div class="col-sm-2 th">Note</div>
    <div class="col-sm-1 th">Work_Contract</div>
    <div class="col-sm-2 th">Action</div>

  </div>

  <?php

    $result=mysqli_query($con,"SELECT * FROM crmmaster WHERE created_time<>0 AND enquiry_time<>0 AND wo_time=0 AND wo_by=0 AND close_enquiry_time=0 AND close_enquiry_by=0 AND branchid='$branchid' ORDER BY created_time DESC");

        while($rows = mysqli_fetch_assoc($result)){

            $enquiryid  = $rows['enquiryid'];

            $customerid = $rows['customerid'];

            $contractid = $rows['contractid'];


            $cifo       = mysqli_fetch_assoc(mysqli_query($con,"SELECT * FROM customermaster WHERE customerid='$customerid'"));
              
  ?>

  <div class="row tr">

    <div class="col-sm-1 td" style="word-wrap:break-word;">

        <?php echo $rows['contractid']; ?>

    </div>

    <div class="col-sm-3 td">

        <?php

            if ($customerid) {
            
                echo 'Company : '.$cifo['customername']; 
                
                echo '<br>Address : '.get_object_vars(json_decode($cifo['customerjson']))['address']; 
                
                echo '<br>Name  : '.get_object_vars(json_decode($cifo['contactjson'])[0])['name'];
                
                echo '<br>Dept. : '.get_object_vars(json_decode($cifo['contactjson'])[0])['department'];
                
                echo '<br>Mobile : <a href="tel:'.get_object_vars(json_decode($cifo['contactjson'])[0])['mobile'].'">'.get_object_vars(json_decode($cifo['contactjson'])[0])['mobile'].'</a>';
                
                echo '<br>Landline'.get_object_vars(json_decode($cifo['contactjson'])[0])['landline'];
                
                echo '<br>Email : <a href="mailto:'.get_object_vars(json_decode($cifo['contactjson'])[0])['email'].'">'.get_object_vars(json_decode($cifo['contactjson'])[0])['email'].'</a>';

            }

        ?>

    </div>

    <div class="col-sm-1 td">

        <?php echo $rows['category']; ?>

    </div>

    <div class="col-sm-2 td">
      
      <!-- offer  -->
      <div class="input-container">

        <input type="file" class="real-input" name="quatation" accept="application/pdf">

        <button class="browse-btn">
          Browse Files
        </button>

        <span class="file-info">Upload a file</span>

      </div>

      <button class="btn btn-primary btn-sm btn-block" data-enquiryid="<?php echo $rows['enquiryid']; ?>"
        onclick="upload(this)">Upload </button>


      <?php 

          $enquir=str_replace('/', '-',$rows['enquiryid']);

          $path="api/offer/".$enquir.'.pdf';

          if(file_exists($path)){

            echo '<br><center><a href="'.$path.'" target="_blank">View PDF</a></center>';

          } else {

            echo '<br>Please Upload .pdf File';

          }

      ?>
      <!-- offer  -->
    </div>

    <div class="col-sm-2 td" style="word-wrap:break-word;">

        <?php echo $rows['enquirynote']; ?>

    </div>


    <div class="col-sm-1 td" style="word-wrap:break-word;">
      <?php 

        if($rows['checkcontract'] == 1){

          if ($rows['category']=="Pest Control") {

            echo '<button class="btn btn-sm btn-primary" data-contractid="'.$contractid.'" onclick="servicepdf(this)">Service Pdf</button>';
            echo '<button class="btn btn-sm btn-primary" data-contractid="'.$contractid.'" onclick="contractpdf(this)">Download</button>';
          
          } else if ($rows['category']=="sales") {

            echo '<button class="btn btn-sm btn-primary" data-enquiryid="'.$rows['enquiryid'].'" data-contractid="'.$rows['contractid'].'" onclick="salespdf(this)">Download</button>';

          }  else {

            echo '<button class="btn btn-sm btn-primary" data-contractid="'.$contractid.'" onclick="printer('.$para.')">Fumigation Bill</button>';  
            echo '<button class="btn btn-sm btn-primary" data-contractid="'.$contractid.'" onclick="contractpdf(this)">Download</button>';

          }

        }

      ?>
    </div>

    <div class="col-sm-2 td">

      <?php if($rows['checkcontract'] == 1){ ?>

        <button class="btn btn-success btn-sm btn-block" data-link1="wo_time" data-link2="wo_by"
          data-contractid="<?php echo $contractid; ?>" onclick="confirms(this)">Order Confirm</button>

        <?php 

          if($rows['category']=='Fumigation'){ ?>

            <a class="btn btn-sm btn-block btn-warning"
              href="workcontract/fumigation1.php?branchid=<?php echo $rows['branchid'];?>&contractid=<?php echo $rows['contractid']; ?>&edit=true">Edit</a>

          <?php }else if ($rows['category']=='sales') {

            echo "<a class='btn btn-default btn-sm  btn-block' href=/process/crm/workcontract/sales.php?enquiryid=".$rows['enquiryid']."&contractid=".$rows['contractid'].">Edit</a>";

          } else{ ?>

            <a class="btn btn-sm btn-block btn-warning"
              href="workcontract/pest-control1.php?branchid=<?php echo $rows['branchid'];?>&contractid=<?php echo $rows['contractid']; ?>&edit=true">Edit</a>
              
          <?php } ?>

      <?php } ?>

      <button class="btn btn-sm btn-block btn-danger" data-contractid="<?php echo $contractid; ?>"
        onclick="closes(this)">Order Close</button>

      <?php 

        if($rows['checkcontract'] == 0){

          if($rows['category']=='Fumigation'){ ?>

            <a class="btn btn-sm btn-block btn-primary" href="workcontract/fumigation1.php?contractid=<?php echo $contractid; ?>&branchid=<?php echo $rows['branchid'];?>">Fumigation Contract</a>

          <?php } else if ($rows['category']=='sales') {

            echo '<a class="btn btn-warning btn-sm  btn-block" href="workcontract/sales.php?enq='.$rows['enquiryid'].'" >Sales</a>';

          } else{ ?>

            <a class="btn btn-sm btn-block btn-primary"
              href="workcontract/pest-control1.php?contractid=<?php echo $contractid; ?>&branchid=<?php echo $rows['branchid'];?>">Pest
                  Control Contract</a>

          <?php }

        } 

      ?>

    </div>

  </div>

  <?php

    }

  ?>

</div>

<button class="btn" style="font-size:25px;border-radius:50px;width:60px;height:60px;position: fixed;
    bottom: 40px;
    right: 40px;background:#eb2f06;color:#fff;" data-toggle="modal" data-target="#myModal">+</button>


<div id="myModal" class="modal fade" role="dialog">

    <div class="modal-dialog">

        <div class="modal-content">

            <div class="modal-header">

                <h4 class="modal-title" align="center">Create Job Work</h4>

            </div>

            <div class="modal-body">

                <div class="form-group row">

                    <div class="col-sm-4">Select Category</div>

                    <div class="col-sm-8">

                        <select data-role="select" data-name='category' class="form-control input-sm">

                            <option value="Fumigation">Fumigation</option>
                            
                            <option value="Pest Control">Pest Control</option>
                            
                            <!-- <option value="sales">sales</option> -->

                        </select>

                    </div>

                    <div class="col-sm-4">Note</div>

                    <div class="col-sm-8">
                        
                        <input type="text" data-name="enquirynote" class="form-control input-sm">
                    
                    </div>

                    <div class="col-sm-4"></div>

                    <div class="col-sm-8">
                        
                        <button class="btn btn-success btn-block btn-sm" id="newqneuiry" onclick="newqneuiry()">Add New Enquery</button>
                        
                    </div>

                </div>

            </div>

        </div>

    </div>

</div>

<?php 
    
    include('footer.php');

?>

<script>

function newqneuiry() {

    var valid = true;

    if (checker('myModal') != false) {

        valid = valid * true;

    } else {

        valid = valid * false;

        alert('Please Catogary Customer');

    }

    if (valid) {
    
        $('#newqneuiry').hide();
    
        var data    = checker('myModal');
        
        var datastr = JSON.stringify(data);
        
        $.ajax({
        
            type: "POST",
        
            data: {
        
            data: datastr
        
            },
        
            url: 'api/newqneuiry1.php',
        
            cache: false,
        
            success: function (res) {
        
            if (res.status == 'success') {
            
                window.location.reload();
            
            } else {

                $('#newqneuiry').show();
            }

            }

        })

    }

}

    function getBase64Image(img) {
        var canvas = document.createElement("canvas");
        canvas.width = img.width;
        canvas.height = img.height;
        var ctx = canvas.getContext("2d");
        ctx.drawImage(img, 0, 0);
        var dataURL = canvas.toDataURL("image/png");
        return dataURL.replace('/^data:image\/(png|jpg);base64,/', "");
    }

function contractpdf(e) {
  
  var contractid = $(e).data('contractid');

  $.ajax({

    type: "POST",

    data: "contractid=" + contractid,

    url: 'api/contractpdf.php',

    cache: false,

    success: function (res) {

      if (res.status == 'success') {

        var content = [];

        var json = res.json[0];

        let base64 = getBase64Image(document.getElementById("imageid"));

        let signature = getBase64Image(document.getElementById("signature"));

        content.push({
          text: 'PEST MORTEM (INDIA) PRIVATE LIMITED',style: 'header'
        });

        content.push({
          text: '(Approved by National Plant Protection Organization, Goverment of India)',style: 'subheader'
        });

        content.push({
          text: 'AN ISO 9001 : 2008 CERTIFIED COMPANY',style: 'subheader1'
        });

        content.push({
          text: json.address + '\n GST No : ' + json.branchgstnumber,alignment: 'center',fontSize: 11
        });

        content.push({
          text: 'EXPERT IN',alignment: 'center',fontSize: 11,bold: true
        });

        content.push({
          text: 'PEST CONTROL SERVICES AND FUMIGATION OF EXPORT CARGO, SHIPS, CONTAINERS',style: 'subheader'
        });

        if (json.category == 'Pest Control') {

          content.push({
            text:"\n"
          });

          content.push({
            columns: [{ text: 'Contract No. : ' + json.contractid, fontSize: 9, bold: true
              }, {
              text: 'Dated : ' + json.contractdate,alignment: 'right',fontSize: 9, bold: true}]
          });

          content.push({
            table: {
              widths: ['*'],
              body: [
                [{text: 'Residential Pest Control Service Agreement',alignment: 'center',fontSize: 10,bold: true}],
              ]
            }
          });

          content.push({
            table: {
              widths: ['*', 10, '*'],
              body: [
                [{text: 'Purchaser',alignment: 'center',fontSize: 10,bold: true,border: [false, false, false, true]},{text: '',border: [false, false, false, false]}, {text: 'Premises',alignment: 'center',fontSize: 10,bold: true,border: [false, false, false, true]}],
                [{text: 'Name     : ' + json.companyname,fontSize: 10,border: [false, false, false, true]}, {text: '',border: [false, false, false, false]}, {text: 'Name     : ' + json.companyname,fontSize: 10,border: [false, false, false, true]}],
                [{text: 'Address : ' + json.companyaddress,fontSize: 10,border: [false, false, false, true]}, {text: '',border: [false, false, false, false]},{text: 'Address : ' + json.servicelocation,fontSize: 10,border: [false, false, false, true]}],
                [{text: 'GST No  : ' + json.custgstnumber,fontSize: 10,border: [false, false, false, true]},{text: '',border: [false, false, false, false]},{text: 'GST No  : ' + json.custgstnumber,fontSize: 10,border: [false, false, false, true]}],
                [{text: 'Email      : ' + json.email,fontSize: 10,border: [false, false, false, true]},{text: '',border: [false, false, false, false]}, {text: 'Email      : ' + json.email,fontSize: 10,border: [false, false, false, true]}],
                [{text: 'Phone     : ' + json.mobile,fontSize: 10,border: [false, false, false, true]},{text: '',border: [false, false, false, false]},{text: 'Phone     : ' + json.mobile,fontSize: 10,border: [false, false, false, true]}],
              ]
            }
          });

          content.push({

            text: '\nThis agreement is for an initial period of (' + json.contractstart + ' TO ' + json.contractend + ') from the date of the frist service and, unless canceled by the purchaser, will automatically continue on a monthly basis until canceled by either party upon thirty days notice.\n\n',fontSize: 9,bold: true

          });

          content.push({
            table: {
              widths: ['*'],
              body: [
                [{text: 'BASIC COVERAGE - TREATMENT OR INSPECTION FOR ',alignment: 'center',fontSize: 9,bold: true}],
              ]
            }
          });

          var table1 = {
            widths: ['auto','*', 50],
            dontBreakRows: true,
            body: []
          };
          
          var table2 = {
            widths: ['auto','*', 50],
            dontBreakRows: true,
            body: []
          };
          
          var services = json.servicejson;
          var serviceslen = parseInt(services.length / 2);

          var counter = 0;

          if (services.length < 5) {

            var count = 1;
            table1['body'].push([{text:'Sr No.',bold:1}, {text:'Type of Services',bold:1,alignment:'left'},{text:'Frequency',bold:1,alignment:'center'}])

            for (var i in services) {

              var arr = [];
              arr.push({text:count,alignment:'center'},services[i].servicename, {text:services[i].frequency,alignment:'center'});
              table1['body'].push(arr);

              count++;

            }

            content.push({

              columns: [{
                style: 'tablfont',
                table: table1,
                layout: 'noBorders',
                margin:[120,0]
              }]

            })
            
          } else {

            table1['body'].push([{text:'Sr No.',bold:1}, {text:'Type of Services',bold:1,alignment:'left'},{text:'Frequency',bold:1,alignment:'center'}])
            table2['body'].push([{text:'Sr No.',bold:1}, {text:'Type of Services',bold:1,alignment:'left'},{text:'Frequency',bold:1,alignment:'center'}])

            var count = 1;

            for (var i in services) {

              var arr = [];

              arr.push({text:count,alignment:'center'},services[i].servicename, {text:services[i].frequency,alignment:'center'});

              if (serviceslen <= counter) {

                table2['body'].push(arr);

              } else {

                table1['body'].push(arr);

              }

              count++;

              counter++;

            }

            content.push({

              columns: [{
                style: 'tablfont',
                table: table1,
                layout: 'noBorders'
              }, {
                width: 10,
                text: ''
              }, {
                style: 'tablfont',
                table: table2,
                layout: 'noBorders'
              }]

            })

          }

          var table3 = {
            widths: ['auto','*', 50, 50],
            dontBreakRows: true,
            body: []
          };
          
          if (json.enquipmentjson.length > 0) {

            content.push({
              table: {
                widths: ['*'],
                body: [
                  [{text: 'INSTALLATION OF EQUIPMENT',alignment: 'center',fontSize: 9,bold: true}],
                ]
              }
            });


            table3['body'].push([{text:'Sr No.',bold:1}, {text:'Type of Equipments',bold:1,alignment:'left'},{text:'Returnable',bold:1,alignment:'center'},{text:'Quantity',bold:1,alignment:'center'}])

            var enquipmentjson = json.enquipmentjson;
            var count = 1;
            for (var i in enquipmentjson) {

              var arr = [];

              arr.push({text:count,alignment:'center'},enquipmentjson[i].machinename, {text:enquipmentjson[i].returnable,alignment:'center'},{text:enquipmentjson[i].qty,alignment:'center'});

              table3['body'].push(arr);
              count++;

            }

            content.push({
              columns: [{
                style: 'tablfont',
                table: table3,
                layout: 'noBorders',
                margin:[120,0]
              }]
            })

          }


          content.push({
            table: {
              widths: ['*', 100, 100],
              body: [
                [{text: 'PAYMENT DETAILS',colSpan: 3,alignment: 'center',fontSize: 9,bold: true}, {}, {}],
                [{text: 'Mode of payment : ' + json.paymentby + '\nAdditional Declaration (if any)\n' + json.additinal,rowSpan: 6,fontSize: 9,bold: true}, {text: 'Contract Value',colSpan: 2,border: [true, true, true, true],alignment: 'center',fontSize: 9,bold: true}, {}],
                ['', {text: 'Service Charges',fontSize: 9,bold: true}, {text: json.amount,fontSize: 9,bold: true}],
                ['', {text: 'GST %',fontSize: 9,bold: true}, {text: (parseFloat(json.cgstper) + parseFloat(json.sgstper) + parseFloat(json.igstper)) + ' % ',fontSize: 9,bold: true}],
                ['', {text: 'GST Amount',fontSize: 9,bold: true}, {text: (parseFloat(json.cgstamt) + parseFloat(json.sgstamt) + parseFloat(json.igstamt)) + '.00',fontSize: 9,bold: true}],
                // ['', {text:'Other Charges',fontSize: 9,bold:true}, {text:  json.othercharges  ,fontSize: 9,bold:true}],
                ['', {text: 'Total Annual Amount',fontSize: 9,bold: true}, {text: json.totalcontractamt,fontSize: 9,bold: true}],
                ['', {text: 'No Of Bill',fontSize: 9,bold: true}, {text: json.billqty,fontSize: 9,bold: true}],
                [{text: convertNumberToWords(json.totalcontractamt)+'Only',colSpan: 3,fontSize: 9,bold: true,alignment: 'center'}, {}, {}],
              ]
            }
          });

          content.push({
            table: {
              headerRows: 1,
              widths: ['*'],
              body: [
                [''],
                ['']
              ]
            },
            layout: 'headerLineOnly'
          }); //LINE CODE

          content.push({
            table: {
                  widths: [300],
                  body:[
                    
                    [{text:'Company Name : PEST MORTEM (INDIA) Pvt Ltd  ',border: [true, false, true, false],alignment:'left',fontSize: 9,fonts: 'TimesNewRoman'}],
                    [{text:'Bank Name     : '+ json.bankname  + ' , ' + json.bankbranchname ,border: [true, false, true, false],alignment:'left',fontSize: 9,fonts: 'TimesNewRoman'}],
                    [{text:'Account No     : '+ json.accountno,border: [true, false, true, false],alignment:'left',fontSize: 9,fonts: 'TimesNewRoman'}],
                    [{text:'RTGS/NEFT/IFSC Code : '+ json.ifsccode,border: [true, false, true, false],alignment:'left',fontSize: 9,fonts: 'TimesNewRoman'}],
                  ]
                },layout: 'noBorders',

          });

          content.push({
            text:'\n',
            layout: 'noBorders',

          });

          content.push({
            table: {
              widths: ['*','*'],
              body:[
                
                [{text: 'For Pest Mortem(India) Pvt. Ltd. ',alignment:'left',fontSize: 10,},{text: json.companyname,alignment:'right',fontSize: 10}],
                [{image: signature,width: 100,height: 50,margin:[0,0,0,0],colSpan:2},{}],
                [{text: 'Authorised Signatory ',alignment:'left',fontSize: 10},{text: 'Authorised Signatory ',alignment:'right',fontSize: 10}],
              ]
            },layout: 'noBorders',

          });

        }

        // content.push( {table:	{widths: ['auto','*'],	body: [

        //     [{rowSpan: 6,  stack: [{image:base64,
        //               width: 75,
        //               height:40,
        //               margin:[0,20],
        //               },]}, {text: 'PEST MORTEM (INDIA) PRIVATE LIMITED',style: 'header'}],
        //     ['', {text:'(Approved by National Plant Protection Organization, Goverment of India)',style: 'subheader'}],
        //     ['', {text:'AN ISO 9001 : 2008 CERTIFIED COMPANY',style: 'subheader1'}],
        //     ['', {text:json.address,alignment:'center',fontSize: 11}],
        //     ['', {text:'EXPERT IN',alignment:'center',fontSize: 11,bold:true}],
        //     ['', {text:'PEST CONTROL SERVICES AND FUMIGATION OF EXPORT CARGO, SHIPS, CONTAINERS',style: 'subheader'}],

        //     ]
        //     }, layout: 'noBorders',  });

        if (json.ftype == '') {
          


          //   content.push({table:{headerRows: 1,widths: ['*'],body: [[''],['']]}, layout: 'headerLineOnly' }); //LINE CODE
          // content.push({ columns: [{text:'Contract No. : '+json.contractid,fontSize: 9,bold:true},{text:'Dated : '+json.contractdate,alignment:'right',fontSize:9,bold:true} ]});
          // content.push({ text:'Period : '+json.contractstart+' TO '+ json.contractend,fontSize: 9,bold:true});
          // content.push({table:{headerRows: 1,widths: ['*'],body: [[''],['']]}, layout: 'headerLineOnly' }); //LINE CODE
          // content.push({ text:'In Consultation and as agreed upon a sum of Rs. '+json.amount+' + GST @ '+( parseFloat(json.cgstper)+parseFloat(json.sgstper)+parseFloat(json.igstper) ) +' % Rs. '+( parseFloat(json.cgstamt)+parseFloat(json.sgstamt)+parseFloat(json.igstamt))+' + Other Charges '+json.othercharges+' = '+json.totalcontractamt +' ( '+json.inword+' )',fontSize: 9});
          // content.push({ text:'to be paid by ' +json.companyname+' (herein after called the owners)',fontSize: 9});
          // content.push({ text:json.companyaddress,fontSize: 9});
          // content.push({ text:'TO M/S PEST MORTEM (INDIA) PVT. LTD. '+json.branchname + ' (herein after called the contractor).',fontSize: 9});
          

        } else {
          content.push({
            text: 'WORK CONTRACT',
            alignment: 'center',
            fontSize: 11,
            bold: true
          });
          content.push({
            table: {
              headerRows: 1,
              widths: ['*'],
              body: [
                [''],
                ['']
              ]
            },
            layout: 'headerLineOnly'
          }); //LINE CODE
          content.push({
            columns: [{
              text: 'Contract No. : ' + json.contractid,
              fontSize: 9,
              bold: true
            }, {
              text: 'Dated : ' + json.contractdate,
              alignment: 'right',
              fontSize: 9,
              bold: true
            }]
          });
          content.push({
            text: 'Period : ' + json.contractstart + ' TO ' + json.contractend,
            fontSize: 9,
            bold: true
          });
          content.push({
            table: {
              headerRows: 1,
              widths: ['*'],
              body: [
                [''],
                ['']
              ]
            },
            layout: 'headerLineOnly'
          }); //LINE CODE
          content.push({
            text: 'In Consultation and as agreed upon a sum of Rs. ' + json.amount + ' + GST @ ' + (parseFloat(json.cgstper) + parseFloat(json.sgstper) + parseFloat(json.igstper)) + ' % Rs. ' + (parseFloat(json.cgstamt) + parseFloat(json.sgstamt) + parseFloat(json.igstamt)) + ' + Other Charges ' + json.othercharges + ' = ' + json.totalcontractamt + ' ( ' + json.inword + ' )',
            fontSize: 9
          });
          content.push({
            text: 'to be paid by ' + json.companyname + ' (herein after called the owners)',
            fontSize: 9
          });
          content.push({
            text: json.companyaddress,
            fontSize: 9
          });
          content.push({
            text: 'TO M/S PEST MORTEM (INDIA) PVT. LTD. ' + json.branchname + ' (herein after called the contractor).',
            fontSize: 9
          });
          content.push({
            text: '\nThe contractor shall carry out the following Fumigation services in the premises of owners viz.',
            fontSize: 9,
            bold: true
          });
          content.push({
            table: {
              headerRows: 1,
              widths: ['*'],
              body: [
                [''],
                ['']
              ]
            },
            layout: 'headerLineOnly'
          }); //LINE CODE
        }


        // var table1 = {
        //   widths: ['*', 'auto', 'auto'],
        //   dontBreakRows: true,
        //   body: []
        // };
        // var table2 = {
        //   widths: ['*', 'auto', 'auto'],
        //   dontBreakRows: true,
        //   body: []
        // };
        // var table3 = {
        //   widths: ['*', 'auto', 'auto'],
        //   dontBreakRows: true,
        //   body: []
        // };

        // if (json.ftype == '') {
        //   var header = ['Type of Services', 'Yes/No', 'Quantity'];
        //   var header1 = ['Type of Services', 'Yes/No', 'Quantity'];
        //   var header2 = ['Type of Equipments', 'Returnable', 'Quantity'];
        //   table1['body'].push(header);
        //   table2['body'].push(header1);
        //   table3['body'].push(header2);
        //   var services = json.servicejson;
        //   var serviceslen = parseInt(services.length / 2);
        //   console.log(serviceslen);

        //   var counter = 0;
        //   for (var i in services) {
        //     var arr = [];
        //     arr.push(services[i].servicename, services[i].yesno, services[i].frequency);
        //     if (serviceslen <= counter) {
        //       table2['body'].push(arr);
        //     } else {
        //       table1['body'].push(arr);
        //     }
        //     counter++;
        //   }
        //   content.push({
        //     columns: [{
        //       style: 'tablfont',
        //       table: table1,
        //       layout: 'noBorders'
        //     }, {
        //       width: 10,
        //       text: ''
        //     }, {
        //       style: 'tablfont',
        //       table: table2,
        //       layout: 'noBorders'
        //     }]
        //   })

        //   content.push({
        //     table: {
        //       widths: ['*'],
        //       body: [
        //         [{
        //           text: 'INSTALLATION OF EQUIPMENT',
        //           alignment: 'center',
        //           fontSize: 9,
        //           bold: true
        //         }],
        //       ]
        //     }
        //   });



        //   var enquipmentjson = json.enquipmentjson;
        //   for (var i in enquipmentjson) {
        //     var arr = [];
        //     arr.push(enquipmentjson[i].machinename, enquipmentjson[i].returnable, enquipmentjson[i].frq);
        //     table3['body'].push(arr);
        //   }
        //   // content.push({table:{headerRows: 1,widths: ['*'],body: [[''],['']]}, layout: 'headerLineOnly' }); //LINE CODE

        //   content.push({
        //     columns: [{
        //       style: 'tablfont',
        //       width: 300,
        //       table: table3,
        //       layout: 'noBorders'
        //     }, {
        //       width: 10,
        //       text: ''
        //     }, {
        //       text: ''
        //     }]
        //   })

        //   // content.push( {table:	{widths: ['*'],body: [[{text:'METHOD OF PAYMENT',alignment:'center',fontSize: 9,bold:true}],]}  });
        //   //content.push({text:'Mode of payment : '+json.paymentby,fontSize: 9});

        // } else {
        //   content.push({
        //     text: 'Fumigation Type  : ' + json.ftype + '\nFumigation Type Details : ' + json.ftype2,
        //     fontSize: 9,
        //     bold: true
        //   });
        //   content.push({
        //     text: 'Name Of Fumigant : ' + json.fname,
        //     fontSize: 9,
        //     bold: true
        //   });
        //   content.push({
        //     table: {
        //       headerRows: 1,
        //       widths: ['*'],
        //       body: [
        //         [''],
        //         ['']
        //       ]
        //     },
        //     layout: 'headerLineOnly'
        //   }); //LINE CODE
        // }


        if (json.ftype == '') {
          //   content.push({text:'Service Location',fontSize: 9,bold:true});
          // content.push({text:json.servicelocation,fontSize: 9});
          // content.push({
          //   table: {
          //     widths: ['*', 100, 100],
          //     body: [
          //       [{
          //         text: 'PAYMENT DETAILS',
          //         colSpan: 3,
          //         alignment: 'center',
          //         fontSize: 9,
          //         bold: true
          //       }, {}, {}],
          //       [{
          //         rowSpan: 6,
          //         text: 'Mode of payment : ' + json.paymentby + '\nAdditional Declaration (if any)\n' + json.additinal,
          //         fontSize: 9,
          //         bold: true
          //       }, {
          //         text: 'SERVICE CHARGES',
          //         colSpan: 2,
          //         border: [true, true, true, true],
          //         alignment: 'center',
          //         fontSize: 9,
          //         bold: true
          //       }, {}],
          //       ['', {
          //         text: 'Amount',
          //         fontSize: 9,
          //         bold: true
          //       }, {
          //         text: json.amount,
          //         fontSize: 9,
          //         bold: true
          //       }],
          //       ['', {
          //         text: 'GST %',
          //         fontSize: 9,
          //         bold: true
          //       }, {
          //         text: (parseFloat(json.cgstper) + parseFloat(json.sgstper) + parseFloat(json.igstper)) + ' % ',
          //         fontSize: 9,
          //         bold: true
          //       }],
          //       ['', {
          //         text: 'GST Amount',
          //         fontSize: 9,
          //         bold: true
          //       }, {
          //         text: (parseFloat(json.cgstamt) + parseFloat(json.sgstamt) + parseFloat(json.igstamt)) + '.00',
          //         fontSize: 9,
          //         bold: true
          //       }],
          //       // ['', {text:'Other Charges',fontSize: 9,bold:true}, {text:  json.othercharges  ,fontSize: 9,bold:true}],
          //       ['', {
          //         text: 'Net Amount',
          //         fontSize: 9,
          //         bold: true
          //       }, {
          //         text: json.totalcontractamt,
          //         fontSize: 9,
          //         bold: true
          //       }],
          //       [{
          //         text: json.inword,
          //         colSpan: 3,
          //         fontSize: 9,
          //         bold: true,
          //         alignment: 'center'
          //       }, {}, {}],



          //     ]
          //   }
          // });

          // content.push({
          //   table: {
          //     headerRows: 1,
          //     widths: ['*'],
          //     body: [
          //       [''],
          //       ['']
          //     ]
          //   },
          //   layout: 'headerLineOnly'
          // }); //LINE CODE
          // content.push({text:'Additional Declaration (if any)',fontSize: 9,bold:true});
          // content.push({text:json.additinal,fontSize: 9});
        } else {
          content.push({
            text: 'Name Of Exporter : ' + json.nameofexporter + ' \nAddress Of Exporter : ' + json.addressofexporter,
            fontSize: 9,
            bold: true
          });
          content.push({
            text: json.servicelocation,
            fontSize: 9
          });
          content.push({
            table: {
              headerRows: 1,
              widths: ['*'],
              body: [
                [''],
                ['']
              ]
            },
            layout: 'headerLineOnly'
          }); //LINE CODE
          content.push({
            text: 'Additional Declaration (if any)',
            fontSize: 9,
            bold: true
          });
          content.push({
            text: json.additinal,
            fontSize: 9
          });
          // content.push({
          //   table: {
          //     headerRows: 1,
          //     widths: ['*'],
          //     body: [
          //       [''],
          //       ['']
          //     ]
          //   },
          //   layout: 'headerLineOnly'
          // }); //LINE CODE

        }



        // if (json.ftype != '') {
        //   content.push({
        //     text: 'Payment,',
        //     fontSize: 9,
        //     bold: true
        //   });
        //   content.push({
        //     text: 'Term of payment : ' + json.termofpayment,
        //     fontSize: 9
        //   });
        //   content.push({
        //     text: 'No refund will be considered under any circumstances.',
        //     fontSize: 9
        //   });
        //   content.push({
        //     text: 'No liability attaches to or assumed by the company, its Director or its representatives in respect of this contact.',
        //     fontSize: 9
        //   });
        //   content.push({
        //     text: 'Please see the Terms and conditions overleaf.',
        //     fontSize: 9,
        //     bold: true
        //   });
        //   content.push({
        //     text: 'Please Sign and return copy of the contract as token of acceptance.\n\n\n\n',
        //     fontSize: 9
        //   });
        //   content.push({
        //     columns: [{
        //       text: 'FOR PEST MORTEM (INDIA) PVT.LTD',
        //       fontSize: 9,
        //       bold: true
        //     }, {
        //       text: 'FOR OWNER  \nName : ' + json.companyname + '\nMob No. ' + json.mobile + ' \nTel No.' + json.landline,
        //       alignment: 'right',
        //       fontSize: 9,
        //       bold: true
        //     }]
        //   })
        // } else {
        //   content.push({
        //     text: 'Bank Name : ' + json.bankname + ',Branch Name : ' + json.bankbranchname + ',IFSC Code : ' + json.ifsccode + ',Account No : ' + json.accountno,
        //     fontSize: 9,
        //     bold: true
        //   });
        //   content.push({
        //     columns: [{
        //       text: '\n\nSignature\n\n\nFOR OWNER \n PEST MORTEM (INDIA) PVT.LTD',
        //       fontSize: 9,
        //       bold: true
        //     }, {
        //       text: '\n\nSignature\n\n\nFOR PURCHASER  \n ' + json.companyname,
        //       alignment: 'right',
        //       fontSize: 9,
        //       bold: true
        //     }]
        //   })

        // }

        dd = {
          pageSize: 'A4',
          pageOrientation: 'portrait',
          pageMargins: [40, 30, 30, 35],
          footer: function (currentPage, pageCount) {
            return {
              margin: 10,
              columns: [{
                fontSize: 9,
                text: [{
                    text: '--------------------------------------------------------------------------' +
                      '\n',
                    margin: [0, 20]
                  },
                  {
                    text: [{text:'© Pest Mortem (India) PVT. LTD. | '},{text: ' REGD OFFICE : ',bold:1},{text: 'G-2, Sunder Tower, T. J. Road, Sewree (West), Mumbai,400015 | PAGE ' + currentPage.toString() + ' of ' + pageCount}],
                  }
                ],
                alignment: 'center'
              }]
            };

          },
          content,
          styles: {
            tablfont: {
              fontSize: 9
            },
            tablfont1: {
              fontSize: 9
            },
            header: {
              fontSize: 16,
              bold: true,
              alignment: 'center',
            },
            subheader: {
              fontSize: 8,
              bold: true,
              alignment: 'center',
            },
            subheader1: {
              fontSize: 8,
              bold: true,
              alignment: 'center',
            },
            subheader2: {
              fontSize: 10,
              bold: true,

            },
          }
        }
        var win = window.open('', '_blank');
        pdfMake.createPdf(dd).open({}, win);

      }
    }
  })
}




function servicepdf(e) {
  var contractid = $(e).data('contractid');
  $.ajax({
    type: "POST",
    data: "contractid=" + contractid,
    url: 'api/servicepdf.php',
    cache: false,
    success: function (res) {
      if (res.status == 'success') {
        var content=[];
        var json=res.json[0];
        var base64 = getBase64Image(document.getElementById("imageid"));


         content.push( {table:	{widths: ['auto','*'],	body: [

            [{rowSpan: 3,  stack: [{image:base64,
                      width: 75,
                      height:40,
                      margin:[0,10],
                      },]}, {text: 'PEST MORTEM (INDIA) PRIVATE LIMITED',style: 'header'}],
            ['', {text:'(Approved by National Plant Protection Organization, Goverment of India)',style: 'subheader'}],
            ['', {text:'AN ISO 9001 : 2008 CERTIFIED COMPANY',style: 'subheader'}],
            ]
            }, layout: 'noBorders',  });
 
          content.push( {table:	{widths: [70,'auto','*'],body: [
            [{text:''  ,style: 'subheader1',bold:true,border:[false,false,false,false]},{text:'REGD. OFF    : '  ,style: 'subheader1',bold:true,border:[false,false,false,false]},{text:json.address.toUpperCase(),style: 'subheader1',bold:true,border:[false,false,false,false]}],
            [{text:''  ,style: 'subheader1',bold:true,border:[false,false,false,false]},{text:'Phone            : '  ,style: 'subheader1',bold:true,border:[false,false,false,false]},{text:'+91-22-2412 7935 / 2411  1976/2414 7425',style: 'subheader1',bold:true,border:[false,false,false,false]}],
            [{text:''  ,style: 'subheader1',bold:true,border:[false,false,false,false]},{text:'FAX                : '  ,style: 'subheader1',bold:true,border:[false,false,false,false]},{text:'+91-22-2415 0261',style: 'subheader1',bold:true,border:[false,false,false,false]}],
            [{text:''  ,style: 'subheader1',bold:true,border:[false,false,false,false]},{text:'E-MAIL          : '  ,style: 'subheader1',bold:true,border:[false,false,false,false]},{text:'pestmortem@pestmortem.com',style: 'subheader1',bold:true,border:[false,false,false,false]}],
            [{text:''  ,style: 'subheader1',bold:true,border:[false,false,false,false]},{text:'WEBSITE       : '  ,style: 'subheader1',bold:true,border:[false,false,false,false]},{text:'http://www.pestmortem.com',style: 'subheader1',bold:true,border:[false,false,false,false]}],        
          ]}  }); 

        content.push({text:'\n'});
          content.push( {table:	{widths: ['*'],body: [[{text:' PEST MANAGEMENT SERVICE RECORD',alignment:'center',fontSize: 10,bold:true}],]}  }); 
          content.push({text:'\n'});
          content.push( {table:	{widths: [100,'*'],body: [
            [{text:'Name of Client        : '  ,fontSize: 9,bold:true,border:[false,false,false,false]},{text:json.companyname.toUpperCase(),fontSize: 9,bold:true,border:[false,false,false,false]}],
            [{text:'Address of Client    : '  ,fontSize: 9,bold:true,border:[false,false,false,false]},{text:json.companyaddress.toUpperCase(),fontSize: 9,bold:true,border:[false,false,false,false]}],
            [{text:'Contract No             : '  ,fontSize: 9,bold:true,border:[false,false,false,false]},{text:json.contractid,fontSize: 9,bold:true,border:[false,false,false,false]}],
            [{text:'Contract Date          : '  ,fontSize: 9,bold:true,border:[false,false,false,false]},{text:json.contractdate,fontSize: 9,bold:true,border:[false,false,false,false]}],         
          ]}  }); 
        
        if(json.ftype==''){
          var servicelocation=json.servicelocation;
          
          if (servicelocation=='Same as Purchaser Address') {
           var servicelocation1=json.companyaddress;
          }else{
           var servicelocation1=json.servicelocation;
           
          }
          content.push( {table:	{widths: [100,'*'],body: [
            [{text:'Service Location     : '  ,fontSize: 9,bold:true,border:[false,false,false,false]},{text:servicelocation1.toUpperCase(),fontSize: 9,bold:true,border:[false,false,false,false]}],
          ]}  });  
        } else{
          content.push( {table:	{widths: [100,'*'],body: [
            [{text:'Service Location     : '  ,fontSize: 9,bold:true,border:[false,false,false,false]},{text:json.companyaddress.toUpperCase(),fontSize: 9,bold:true,border:[false,false,false,false]}],
          ]}  }); 
        }
       
        var table1 = {
          widths: ['auto','*','auto','auto','auto'],
          dontBreakRows: true,
          body: []
        };
        var header=['Date','Service Performed','Infestation Status*','Name of Operator','Customer Signature'];
        table1['body'].push(header);
        var services=json.servicejson;

        for(var i in services){
          var arr=[];
          arr.push(services[i].servicedate,services[i].servicename,'','','');
            table1['body'].push(arr);
     
        }
        

        content.push({ columns: [
          {
            style: 'tablfont',
            table: table1,
            fontSize: 8,
            bold:true
            
            // layout: 'noBorders'
          }
        ]})

        

  
        
        dd = {
          pageSize: 'A5',
          pageOrientation: 'portrait',
          // pageOrientation: 'landscape',
          pageMargins: [40, 30, 30, 35],
          footer: function (currentPage, pageCount) {
            return {
              margin: 10,
              columns: [{
                fontSize: 9,
                text: [{
                    text: '--------------------------------------------------------------------------' +
                      '\n',
                    margin: [0, 20]
                  },
                  {
                    text: '© Pest Mortem (India) PVT. LTD. | PAGE ' + currentPage.toString() + ' of ' + pageCount,
                  }
                ],
                alignment: 'center'
              }]
            };

          },
          content,
          styles: {
            tablfont: {
              fontSize: 8
            },
            tablfont1: {
              fontSize: 8
            },
            header: {
                            fontSize: 14,
                            bold: true,
                            alignment: 'center',
                          },
                            subheader: {
                            fontSize: 8,
                            bold: true,
                            alignment: 'center',
                          }, 
                          subheader1: {
                            fontSize: 8,
                            bold: true,
                            alignment: 'left',
                          },
                           subheader2: {
                            fontSize: 10,
                            bold: true,
                            
                          },
          }
        }
        var win = window.open('', '_blank');
        pdfMake.createPdf(dd).open({}, win);

      }
    }
  })
}

$('.browse-btn').on('click',function(){
  $(this).parent().find('input').click();
});

$('.real-input').on('change',function(){
  $(this).parent().find('span').text($(this).val().replace(/C:\\fakepath\\/i, ''));
});


  
function workcontract(){

}

function setidtomidal(e){
var enquiryid=$(e).data('enquiryid');
$('#inp-enquiryidset').val(enquiryid);

}



function upload(e){
var file=$(e).parent().parent().find('input')[0].files[0];
var enquiryid=$(e).data('enquiryid');
var formData = new FormData();

formData.append('offer',file);
formData.append('enquiryid',enquiryid);
$.ajax({
        type: "POST",
        data: formData,
        url: 'api/offer.php',
        cache: false,
        processData: false,
        contentType: false,
        success: function (res) {
             window.location.reload();
        }
    })
}









function printer(id,enqno,cnt) {
  var billtype="";
  if(cnt == 1){
    billtype = "INVOICE BILL";
  }else if(cnt == 2){
    billtype = "BILL INVOICE";
  }
   var jsondata = $.parseJSON($.ajax({
         type: "POST",
        data: {
          id: id
        },
        url: 'api/getbilljson.php',
        cache: false,
        success: function (res) {
        // alert(res.status);
         },
        dataType: "json", 
        async: false
    }).responseText);
   

jsondata =JSON.stringify(jsondata);
data1 = JSON.parse(jsondata);  
json1 = data1.json;     
 
 //get customer info

var jsondata = $.parseJSON($.ajax({
         type: "POST",
        data: {
          enqno: enqno
        },
        url: 'api/getcustjson.php',
        cache: false,
        success: function (res) {
        //jsondata =JSON.stringify(res.json);
         },
        dataType: "json", 
        async: false
    }).responseText);
   jsondata =JSON.stringify(jsondata);
data2 = JSON.parse(jsondata);  
json2= data2.json; 
customerno = data2.custno;
customername = data2.customername;
state = data2.state;
gstno = data2.gstno;
 
 //get contract info

var jsondata = $.parseJSON($.ajax({
         type: "POST",
        data: {
          enqno: enqno,
          id: id
        },
        url: 'api/getwcjson.php',
        cache: false,
        success: function (res) {
          //alert(res.status);
        //jsondata =JSON.stringify(res.json);
         },
        dataType: "json", 
        async: false
    }).responseText);
itemjson = jsondata["itemjson"];
servicejson = jsondata["servicejson"];
contractdate = jsondata["contractdate"];
contractstart = jsondata["contractstart"];
contractend = jsondata["contractend"];
totalcontractamt = jsondata["contractamt"];
sac = jsondata["sac"];




  address = data2.address;


itemjson =JSON.stringify(itemjson);
itemjson = JSON.parse(itemjson);  


servicejson =JSON.stringify(servicejson);
servicejson = JSON.parse(servicejson);  
 
billamt = json1.qty * json1.rate;
 
balamt = totalcontractamt - billamt;

totalamtinwords = convertNumberToWords(json1.grand_total)+ " Only";

category = jsondata["category"];

 //get branch info

var jsondata = $.parseJSON($.ajax({
         type: "POST",
        data: {
          enqno: enqno
        },
        url: 'api/getbrjson.php',
        cache: false,
        success: function (res) {
          //alert(res.status);
        //jsondata =JSON.stringify(res.json);
         },
        dataType: "json", 
        async: false
    }).responseText);

jsondata =JSON.stringify(jsondata);
data4 = JSON.parse(jsondata); 

branchjson= data4.branchjson; 
branchshortname= data4.branchshortname; 
branchname = data4.branchname;
console.log(json1.paid_date);



  serviceinfo = "\nCONTRACT PERIOD : "+contractstart+"\n\n";


//balanceinfo ="\nRs. "+json1.totalcontractamt+": TOTAL CONTRACT AMOUNT\nRs."+billamt.toFixed(2)+": 1ST QUARTER BILL\n ----------------\nRs."+balamt.toFixed(2)+" : BALANCE";

paymentinfo = "Amount : "+totalamtinwords+"\n\nPayment should be made within 30 days from the date of invoice.\n Interest @18% will be charged on overdue Invoices.\n GSTIN: "+gstno+"\n Category of Service : Cleaning Service\n\n E.P.F. A/c No. : MH/40228\n E.S.I.C. Code No. : 31-480-101\n PAN NO. : AACCP25 \n ";
paymentinfo2 = "\n\n Bank Name : "+branchjson.bankname+" \nBranch Name :"+branchjson.bankbranchname+"\n Account holder's Name : M/s. Pest Mortem (India) Pvt. Ltd.\n AC Number : C.A.No. "+branchjson.accountno+"\n RTGS/NEFT IFSC CODE : "+branchjson.ifsccode;



var fullDate = new Date();
var twoDigitMonth = ((fullDate.getMonth().length + 1) === 1) ? (fullDate.getMonth() + 1) : '' + (fullDate.getMonth() + 1);
var currentDate = fullDate.getDate() + "/" + twoDigitMonth + "/" + fullDate.getFullYear();


var content=[]; 
       


                  dd = {
               
                    pageSize: 'A4',
                    pageMargins: [50, 30, 30, 40],
                    footer: function (currentPage, pageCount) {
                        return {
                            margin: 10,
                            columns: [{
                                fontSize: 8,
                                text: [{
                                        text: '--------------------------------------------------------------------------' +
                                            '\n',
                                        margin: [0, 20]
                                    },
                                    {
                                        text: '©Pest Mortem (India) PVT. LTD. || Printed On ' +currentDate ,
                                    }
                                ],
                                alignment: 'center'
                            }]
                        };

                       },

                    content: [
                           {
                                text: billtype,
                                 fontSize: 10,
                                      bold: true,
                                      alignment: 'center',
                            },
                            {
                                text: 'SUBJECT TO MUMBAI JURIDICTION',
                                 fontSize: 8,
                                      bold: true,
                                      alignment: 'center',
                            },
                             {
                                text: 'PEST MORTEM (INDIA) PRIVATE LIMITED',
                                 fontSize: 15,
                                 bold: true,
                                alignment: 'center',
                            },
                            {
                                    text: '(Estd. 1980)\n',
                                     fontSize: 10,
                                      bold: true,
                                      alignment: 'center',
                                },

                                {
                                    text: '\nRegd. Office : '+branchjson.address+'\nTel : '+branchjson.phone+' Email : '+branchjson.email +'\n\n',
                                    fontSize: 8,
                                    bold: true,
                                    alignment: 'center',
                                },
                                {
                                     
                                    bold: true,
                                     fontSize: 8,
                                    margin: [0, 0, 0, 0],
                                    table: {
                                         widths: ['*'],
                                        height:400,
                                       
                                        body: [
                                        [
                                            {
                                             border : [true, true, true, false], 
                                             text: [{text:'Regd. Office : ',bold:true},' G -2, Sunder Tower, T.J. Road, Sewree(west),Mumbai - 400015\n Phone : 24147425/24127935/24111976/24149566/65531746/47 . Fax : 91-22-24150261 . \n E-Mail : pestmort@bom3.vsnl.net.in . pestmortem@pestmortem@pestmortem.com. Website : www.pestmortem.com'] ,
                                              fontSize: 9,
                                               alignment: 'center',
                                              bold:true
                                            },
                                               
                                              ],
                                            ],

                                    },
 
                                },

                                {
                                     
                                    bold: true,
                                     fontSize: 8,
                                    margin: [0, 0, 0, 0],
                                    table: {
                                         widths: [50, '*', 70, '*'],
                                        height:400,
                                       
                                        body: [
                                            [
                                            { border : [true, true, false, false], 
                                            text: 'M/S           : ', fontSize: 8,bold:true
                                            },
                                             {
                                               border : [false, true, false, false], 
                                              text: customername,  fontSize: 8,bold:true
                                            },
                                            { border : [true, true, false, false], 
                                            text: 'CUSTOMER NO.    : ', fontSize: 8,bold:true
                                            },
                                             {
                                               border : [false, true, true, false], 
                                              text: customerno,  fontSize: 8,bold:true
                                            }
                                           
                                              ],
                                              [
                                            { border : [true, false, false, false], 
                                            text: 'ADDRESS : ', fontSize: 8,bold:true
                                            },
                                             {
                                               border : [false, false, false, false], 
                                              text:  address,  fontSize: 8,bold:true
                                            },
                                            { border : [true, false, false, false], 
                                            text: 'INVOICE NO.          : ', fontSize: 8,bold:true
                                            },
                                             {
                                               border : [false, false, true, false], 
                                              text: json1.billid,  fontSize: 8,bold:true
                                            }
                                              ],
                                            [
                                            { border : [true, false, false, false], 
                                            text: 'GST NO.    :', fontSize: 8,bold:true
                                            },
                                             {
                                               border : [false, false, true, false], 
                                              text: gstno,  fontSize: 8,bold:true
                                            },
                                            { border : [true, false, false, false], 
                                            text: 'INVOICE DATE       : ', fontSize: 8,bold:true
                                            },
                                             {
                                               border : [false, false, true, false], 
                                              text: json1.billdate,  fontSize: 8,bold:true
                                            }
                                              ],
                                               [
                                            { border : [true, false, false, false], 
                                            text: 'STATE       :', fontSize: 8,bold:true
                                            },
                                             {
                                               border : [false, false, true, false], 
                                              text: state,  fontSize: 8,bold:true
                                            },
                                            { border : [true, false, false, false], 
                                            text: 'CONTRACT NO.     : ', fontSize: 8,bold:true
                                            },
                                             {
                                               border : [false, false, true, false], 
                                              text: json1.contractid,  fontSize: 8,bold:true
                                            }
                                              ],
                                               [
                                            { border : [true, false, false, false], 
                                            text: 'A/C.           :', fontSize: 8,bold:true
                                            },
                                             {
                                               border : [false, false, true, false], 
                                              text: json1.ac,  fontSize: 8,bold:true
                                            },
                                            { border : [true, false, false, false], 
                                            text: 'CONTRACT DATE  : ', fontSize: 8,bold:true
                                            },
                                             {
                                               border : [false, false, true, false], 
                                              text: contractdate,  fontSize: 8,bold:true
                                            }
                                              ],
                                              /*
                                            ['M/S:', customername+'\n'+address, 'INVOICE No. :',json1.billid],
                                            ['GST no.:', gstno, 'INVOICE Date. :',json1.billdate],
                                            ['State:', state, 'CONTRACT NO. :',json1.contractid],
                                            ['A/C.:', json1.ac, 'CONTRACT DATE :',contractdate]
                                          */
                                        ],

                                    },
 
                                },
                                                                     
                                {
                                     border : 0,
                                    bold: true,
                                     fontSize: 8,
                                    margin: [0, 0, 0, 0, 0],
                                    table: {
                                         widths: [300, 40,20, 50, '*'],
                                   
                                        body: [
                                            [{text:'P A R T I C U L A R S',colSpan:3 }, {},{},'SAC', 'TOTAL'],
                                            [{text:serviceinfo,colSpan:3 }, {},{},'SAC', {text:json1.total}],

                                          //   [itemjson[0].itemname + '\n'+ serviceinfo +'\n'+balanceinfo, sac, json1.qty,json1.rate,billamt.toFixed(2)],
                                          //  [serviceinfo +'\n'+balanceinfo, '', '','',''],

                                        ],

                                    },

                                         
                                },
                                
                                {
                                    border : 0,
                                    bold: true,
                                     fontSize: 8,
                                    margin: [0, 15],
                                    table: {
                                         widths: [350,  '*'],
                                   
                                        body: [
                                            // [paymentinfo,  {text: totalinfo,alignment: 'right'}],
                                          [paymentinfo,  {
                                                               border : 0,
                                                              bold: true,
                                                               fontSize: 8,
                                                              margin: [0, 15 ],
                                                              table: {
                                                                   widths: [80, '*'],
                                                             
                                                                  body: [
                                                                      ['TOTAL  ', ': '+json1.total],
                                                                      ['CGST '+json1.cgstper+'%', ': '+json1.cgstamt ],
                                                                      ['SGST '+json1.sgstper+' %', ': '+json1.sgstamt ],
                                                                      ['IGST '+json1.igstper+' %', ': '+json1.igstamt ],
                                                                      // ['Other Charges', ': '+json1.other1],       
                                                                      ['GRAND TOTAL ', ': '+json1.grand_total],       
                                                           

                                                                  ],

                                                                }, layout: 'noBorders'

                                                      }],
                                                     ], 
                                    },  
                                     

                                         
                                },
                                {
                                    border : 0,
                                    bold: true,
                                     fontSize: 8,
                                    margin: [0, 15],
                                    table: {
                                         widths: [350,  '*'],
                                   
                                        body: [
                                            // [paymentinfo,  {text: totalinfo,alignment: 'right'}],
                                          [paymentinfo2,  {
                                                               border : 0,
                                                              bold: true,
                                                               fontSize: 8,
                                                              margin: [0 ,0],
                                                              alignment: 'center',
                                                              table: {
                                                                   widths: [150],
                                                             
                                                                  body: [
                                                                      
                                                                      [{text:'For Pest Mortem(India) Pvt. Ltd.',border:[true,true,true,false]}],
                                                                      [{text:'',border:[true,false,true,false,]}],
                                                                      [{text:'',border:[true,false,true,false,]}],
                                                                      [{text:'',border:[true,false,true,false,]}],
                                                                      [{text:'',border:[true,false,true,false,]}],
                                                                      [{text:'',border:[true,false,true,false,]}],
                                                                      [{text:'Authorised Signatory',border:[true,false,true,true,]}],


                                                                      
                                                                                                                                     

                                                                  ],

                                                                }, 

                                                      }],
                                                     ], 
                                    },  
                                     layout: 'noBorders'

                                         
                                },
                                

                    ]
               }
                         // totalinfo = "TOTAL     : "+billamt+"\n()FREIGHT     : "+freight+"\nCGST@ 0.00      : "+cgstamt+"\nSGST@ 0.00      : "+sgstamt+"\nIGST@ 18.00      : "+igstamt+"\nGRAND TOTAL      : "+grandtotal+"\n\n\n\n\n For Pest Mortem(India) Pvt. Ltd.\n\n\n\nAuthorised Signatory";
    
              // pdfMake.createPdf(dd).download("billno_"+id+".pdf");
             pdfMake.createPdf(dd).open();

}




function getBase64Image(img) {
  var canvas = document.createElement("canvas");
  canvas.width = img.width;
  canvas.height = img.height;
  var ctx = canvas.getContext("2d");
  ctx.drawImage(img, 0, 0);
  var dataURL = canvas.toDataURL("image/png");
  return dataURL.replace('/^data:image\/(png|jpg);base64,/', "");
}



function salespdf(e) {
  var contractid = $(e).data('contractid');
  var enquiryid = $(e).data('enquiryid');

  $.ajax({
    type: "POST",
    data: "contractid=" + contractid + "&enquiryid=" + enquiryid,
    url: 'api/salespdf.php',
    cache: false,
    success: function (res) {
      if (res.status == 'success') {
        var content = [];
        json = res.json[0];

        var branchjson=res.branchjson;


             
  content.push({
    layout: 'noBorders',
    table: {
      widths: ['*'],
      body: [
       [ {
              text: 'Sales Bill',
              fontSize: 10,
              bold: true,
              alignment: 'center',
            }],
            [{
              text: 'SUBJECT TO MUMBAI JURIDICTION',
              fontSize: 8,
              bold: true,
              alignment: 'center',
            }],
            [{
              text: 'PEST MORTEM (INDIA) PRIVATE LIMITED',
              fontSize: 15,
              bold: true,
              alignment: 'center',
            }],
            [{
              text: '(Estd. 1980)\n',
              fontSize: 10,
              bold: true,
              alignment: 'center',
            }],
            [{
              text:  'Regd. Office : ' + branchjson.address + '\nTel : ' + branchjson.phone + ' Email : ' + branchjson.email + '\n\n',
              fontSize: 8,
              bold: true,
              alignment: 'center',
            }],
      ]
    }
  });
  content.push({
    table: {
      widths: ['*'],
      body: [
       [ {
              text: 'Regd. Office : G -2, Sunder Tower, T.J. Road, Sewree(west),Mumbai - 400015\n Phone : 24147425/24127935/24111976/24149566/65531746/47 . Fax : 91-22-24150261 . \n E-Mail : pestmort@bom3.vsnl.net.in . pestmortem@pestmortem@pestmortem.com. Website : www.pestmortem.com' ,
              fontSize: 9,
              bold: true,
              alignment: 'center',
              border: [true, true, true, true],
            }],
            
      ]
    }
  });

  content.push({
    table: {
      widths: ['*','*'],
      body: [
       [{
        text: 'M/S            : ' + json.companyname.toUpperCase(),
              fontSize: 9,
              bold: true,
              alignment: 'left',
              border: [true, false, true, false],
       }, {
              text: 'CUSTOMER NO     : ' + json.enquiryid,
              fontSize: 9,
              bold: true,
              alignment: 'left',
              border: [true, false, true, false],
            }],
            [{
        text: 'ADDRESS  :  ' + json.caddress.toUpperCase(),
              fontSize: 9,
              bold: true,
              alignment: 'left',
              border: [true, false, true, false],
       }, {
              text: 'INVOICE NO.          : ' + res.billjson['billid'],
              fontSize: 9,
              bold: true,
              alignment: 'left',
              border: [true, false, true, false],
            }],
            [{
        text: 'GST NO.     : ' + json.gstnumber,
              fontSize: 9,
              bold: true,
              alignment: 'left',
              border: [true, false, true, false],
       }, {
              text: 'INVOICE DATE       : ' + res.billjson['billdate'],
              fontSize: 9,
              bold: true,
              alignment: 'left',
              border: [true, false, true, false],
            }],
            [{
        text: 'STATE        : ' + json.statename,
              fontSize: 9,
              bold: true,
              alignment: 'left',
              border: [true, false, true, false],
       }, {
              text: 'CONTRACT NO.     : ' + json.contractid,
              fontSize: 9,
              bold: true,
              alignment: 'left',
              border: [true, false, true, false],
            }],
            [{
        text: 'A/C.            : ' ,
              fontSize: 9,
              bold: true,
              alignment: 'left',
              border: [true, false, true, true],
       }, {
              text: 'CONTRACT DATE  : ' + json.date,
              fontSize: 9,
              bold: true,
              alignment: 'left',
              border: [true, false, true, true],
            }],
            
      ]
    }
  });



 


  var table1 = {
				widths: [15,95,40,40,40,40,40,40,40,45],
		                dontBreakRows: true,
                    fontSize: 20,
                    

				body: []
            };
          var particular=  [{text: 'SR NO',},{text: 'ITEM',alignment: 'center',},{text: 'Quantity',alignment: 'center',},{text: 'Rate',alignment: 'center',},{text: 'Amount',alignment: 'center',},{text: 'GST',alignment: 'center',},{text: 'CGST AMT',alignment: 'center',},{text: 'SGST AMT',alignment: 'center',},{text: 'IGST AMT',alignment: 'center',},{text: 'NET AMT',alignment: 'center',}];
           table1['body'].push(particular);     

           content.push({
        table: {
            headerRows: 1,
            // widths: ['*'],
            widths: [15,95,40,40,40,40,40,40,40,45],
            alignment: 'center',

            body: [
              [],
               
            ]
        },
    });
    var cr=1;
    var tablejson=json.tablejson;
      for(var i in tablejson){
        var tb=[];
        tb.push(cr,{text:tablejson[i].itemname,alignment: 'center'},{text:tablejson[i].qty,alignment: 'center'},{text:tablejson[i].rate,alignment: 'center'},{text:tablejson[i].amount,alignment: 'center'},{text:tablejson[i].gst,alignment: 'center'},{text:tablejson[i].cgstamt,alignment: 'center'},{text:tablejson[i].sgstamt,alignment: 'center'},{text:tablejson[i].igstamt,alignment: 'center'},{text:tablejson[i].total,alignment: 'center'});
        table1['body'].push(tb);
        cr++;
      }


content.push({
                        text: "",
                        fontSize: 20,
                        alignment: 'center'
                    }, {
                        style:'tablfont1',
                        table: table1
                    });

                    
content.push({
                        text: "\n",
                    });


                    content.push({
                      table: {
				widths: [350, '*'],

				body: [
					[{rowSpan: 5, text: 'Amount : ' + convertNumberToWords(json.total)+' Only',bold: 'true',fontSize: '10'}, {text:'TOTAL                 : ' + json.amount,bold: 'true',fontSize: '8'}],
					['', {text:'CGST ' + res.billjson['cgstper'] + '%       : '   + json.cgstamt,bold: 'true',fontSize: '8'}],
					['', {text:'SGST ' + res.billjson['sgstper'] + '%       : ' +json.sgstamt,bold: 'true',fontSize: '8'} ],
					['',{text: 'IGST ' + res.billjson['igstper'] + '%        : ' + json.igstamt,bold: 'true',fontSize: '8'}],
					// ['',{text:'Other Charges   : ' + json.othercharges,bold: 'true',fontSize: '8'}],
          ['',{text:'GRAND TOTAL  :  ' +json.total,bold: 'true',fontSize: '8'}],
				]
			}
  });




  content.push({
    text: '\n'
  });
  content.push({
    text: '\n'
  });
 
                    content.push({
                      table: {
				widths: [350, '*'],

				body: [
					[{text: 'Bank Name : ' + res.branchjson['bankname'],bold: 'true',fontSize: '10',border: [false, false, false, false]}, {text:'For Pest Mortem(India) Pvt. Ltd.  ',bold: 'true',alignment: 'center',fontSize: '8',border: [true, true, true, false]}],
					[{text: 'Branch Name : ' + res.branchjson['bankbranchname'],bold: 'true',fontSize: '10',border: [false, false, false, false]}, {text:'',border: [true, false, true, false]}],
					[{text: 'Account holders Name :  M/s. Pest Mortem (India) Pvt. Ltd.' ,bold: 'true',fontSize: '10',border: [false, false, false, false]}, {text:'',border: [true, false, true, false]}],
					[{text: 'AC Number : C.A.No. ' + res.branchjson['accountno'] ,bold: 'true',fontSize: '10',border: [false, false, false, false]}, {text:'Authorised Signatory',bold: 'true',alignment: 'center',fontSize: '8',border: [true, false, true, true]}],
					[{text:'RTGS/NEFT IFSC CODE :    : ' + res.branchjson['ifsccode'],bold: 'true',fontSize: '10',border: [false, false, false, false]},{text:'',border: [false, false, false, false]}],
				]
			}
                    });



 
  dd = {
    pageSize: 'A4',
    pageOrientation: 'portrait',
    pageMargins: [40, 30, 30, 35],
    footer: function (currentPage, pageCount) {
      return {
        margin: 10,
        columns: [{
          fontSize: 8,
          text: [{
              text: '--------------------------------------------------------------------------' +
                '\n',
              margin: [0, 20]
            },
            {
              text: '© Pest Mortem (India) PVT. LTD | PAGE ' + currentPage.toString() + ' of ' + pageCount,
            }
          ],
          alignment: 'center'
        }]
      };

    },
    content,
    styles: {
      tablfont: {
        fontSize: 7
      },
      tablfont1: {
        fontSize: 9
      }
    }
  }
  var win = window.open('', '_blank');
  pdfMake.createPdf(dd).open({}, win);

        }
      }
  });  
}




function convertNumberToWords(amount) {
    var words = new Array();
    words[0] = '';
    words[1] = 'One';
    words[2] = 'Two';
    words[3] = 'Three';
    words[4] = 'Four';
    words[5] = 'Five';
    words[6] = 'Six';
    words[7] = 'Seven';
    words[8] = 'Eight';
    words[9] = 'Nine';
    words[10] = 'Ten';
    words[11] = 'Eleven';
    words[12] = 'Twelve';
    words[13] = 'Thirteen';
    words[14] = 'Fourteen';
    words[15] = 'Fifteen';
    words[16] = 'Sixteen';
    words[17] = 'Seventeen';
    words[18] = 'Eighteen';
    words[19] = 'Nineteen';
    words[20] = 'Twenty';
    words[30] = 'Thirty';
    words[40] = 'Forty';
    words[50] = 'Fifty';
    words[60] = 'Sixty';
    words[70] = 'Seventy';
    words[80] = 'Eighty';
    words[90] = 'Ninety';
    amount = amount.toString();
    var atemp = amount.split(".");
    var number = atemp[0].split(",").join("");
    var n_length = number.length;
    var words_string = "";
    if (n_length <= 9) {
        var n_array = new Array(0, 0, 0, 0, 0, 0, 0, 0, 0);
        var received_n_array = new Array();
        for (var i = 0; i < n_length; i++) {
            received_n_array[i] = number.substr(i, 1);
        }
        for (var i = 9 - n_length, j = 0; i < 9; i++, j++) {
            n_array[i] = received_n_array[j];
        }
        for (var i = 0, j = 1; i < 9; i++, j++) {
            if (i == 0 || i == 2 || i == 4 || i == 7) {
                if (n_array[i] == 1) {
                    n_array[j] = 10 + parseInt(n_array[j]);
                    n_array[i] = 0;
                }
            }
        }
        value = "";
        for (var i = 0; i < 9; i++) {
            if (i == 0 || i == 2 || i == 4 || i == 7) {
                value = n_array[i] * 10;
            } else {
                value = n_array[i];
            }
            if (value != 0) {
                words_string += words[value] + " ";
            }
            if ((i == 1 && value != 0) || (i == 0 && value != 0 && n_array[i + 1] == 0)) {
                words_string += "Crores ";
            }
            if ((i == 3 && value != 0) || (i == 2 && value != 0 && n_array[i + 1] == 0)) {
                words_string += "Lakhs ";
            }
            if ((i == 5 && value != 0) || (i == 4 && value != 0 && n_array[i + 1] == 0)) {
                words_string += "Thousand ";
            }
            if (i == 6 && value != 0 && (n_array[i + 1] != 0 && n_array[i + 2] != 0)) {
                words_string += "Hundred and ";
            } else if (i == 6 && value != 0) {
                words_string += "Hundred ";
            }
        }
        words_string = words_string.split("  ").join(" ");
    }
    return words_string;
}


$(document).ready(function () {
    $('.select-js').select2({
        width: '100%',
        tags: true
    });
    $('.select').attr('style', 'width:100%!important;');
});
</script>