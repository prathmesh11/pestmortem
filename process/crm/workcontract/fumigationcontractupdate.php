<?php
    
    $base = '../../../';
 
    include($base.'_in/connect.php');
 
    header('content-type: application/json; charset=utf-8');
 
    header("access-control-allow-origin: *");
 
    if(isset($_POST['data1']) && isset($_POST['data2']) && isset($_POST['data4'])){

        $con = _connect();
        
        if (session_status()==PHP_SESSION_NONE) { session_start(); }
      
        $created_by        = $_SESSION['employeeid'];
        
        $branchid          = $_SESSION['branchid'];

        $contractid        = _clean($con,get_object_vars(json_decode($_POST['data1']))['contractid']);
        
        $customerid        = _clean($con,get_object_vars(json_decode($_POST['data1']))['customerid']);
        
        $contractdate      = _clean($con,get_object_vars(json_decode($_POST['data1']))['contractdate']);
        
        $ftype             = _clean($con,get_object_vars(json_decode($_POST['data1']))['ftype']);
        
        $ftype2            = _clean($con,get_object_vars(json_decode($_POST['data1']))['ftype2']);
        
        $quantity          = _clean($con,get_object_vars(json_decode($_POST['data1']))['quantity']);
        
        $volume            = _clean($con,get_object_vars(json_decode($_POST['data1']))['volume']);
        
        $fumigationplaces  =  _clean($con,get_object_vars(json_decode($_POST['data1']))['fumigationplace']);

        $fumigationplace   = '[{"servicelocation":"'.$fumigationplaces.'"}]';
        
        $fname             = _clean($con,get_object_vars(json_decode($_POST['data1']))['fname']);
        
        $termofpayment     = _clean($con,get_object_vars(json_decode($_POST['data1']))['termofpayment']);
        
        $billdate          = _clean($con,get_object_vars(json_decode($_POST['data1']))['billremiderdate']);
        
        $nameofexporter    = _clean($con,get_object_vars(json_decode($_POST['data1']))['nameofexporter']);
        
        $addressofexporter = _clean($con,get_object_vars(json_decode($_POST['data1']))['addressofexporter']);
        
        $billingparty      = _clean($con,get_object_vars(json_decode($_POST['data1']))['billingparty']);
        
        $cert1             = _clean($con,get_object_vars(json_decode($_POST['data1']))['cert1']);
        
        $cert2             = _clean($con,get_object_vars(json_decode($_POST['data1']))['cert2']);
        
        $cert3             = _clean($con,get_object_vars(json_decode($_POST['data1']))['cert3']);
        
        $cert4             = _clean($con,get_object_vars(json_decode($_POST['data1']))['cert4']);
        
        $cert5             = _clean($con,get_object_vars(json_decode($_POST['data1']))['cert5']);
        
        $servicedate       = _clean($con,get_object_vars(json_decode($_POST['data1']))['servicedate']);
        
        $servicetime       = _clean($con,get_object_vars(json_decode($_POST['data1']))['servicetime']);
        
        $amount            = _clean($con,get_object_vars(json_decode($_POST['data1']))['amount']);
        
        $cgstper           = _clean($con,get_object_vars(json_decode($_POST['data1']))['cgstper']);
        
        $sgstper           = _clean($con,get_object_vars(json_decode($_POST['data1']))['sgstper']);
        
        $igstper           = _clean($con,get_object_vars(json_decode($_POST['data1']))['igstper']);
        
        $cgstamt           = _clean($con,get_object_vars(json_decode($_POST['data1']))['cgstamt']);
        
        $sgstamt           = _clean($con,get_object_vars(json_decode($_POST['data1']))['sgstamt']);
        
        $igstamt           = _clean($con,get_object_vars(json_decode($_POST['data1']))['igstamt']);
        
        $othercharges      = _clean($con,get_object_vars(json_decode($_POST['data1']))['othercharges']);
        
        $totalcontractamt  = _clean($con,get_object_vars(json_decode($_POST['data1']))['total']);
    
        $json1             = '[{"cert":"'.$cert1.'","cert":"'.$cert2.'","cert":"'.$cert3.'","cert":"'.$cert4.'","cert":"'.$cert5.'"}]';

        if (file_exists($_FILES['data3']['tmp_name']) || is_uploaded_file($_FILES['data3']['tmp_name'])){

            $contractidt = str_replace('/', '-',$contractid);

            $path        = getcwd()."/photo/".$contractidt.'.jpg';

            if(file_exists('photo/'.$contractidt.'.jpg')){

                unlink('photo/'.$contractidt.'.jpg');

            }

            @move_uploaded_file($_FILES['data3']['tmp_name'], $path);

        }

        $billid   = $rand = substr(md5(microtime()),rand(0,26),5);

        $billid   = $serviceid=$contractid.'/'.$billid;

        $billjson = '[{"billid":"'.$billid.'","billdate":"'.$billdate.'"}]';
        
        
        mysqli_query($con,"UPDATE  workcontractbill SET billid='$billid',billdate='$billdate',cgstper='$cgstper',sgstper='$sgstper',igstper='$igstper',cgstamt='$cgstamt',sgstamt='$sgstamt',igstamt='$igstamt',other1='$othercharges',contractamt='$totalcontractamt',created_by='$created_by',created_time='$CURRENT_MILLIS' WHERE branchid='$branchid' AND enquiryid='$enquiryid' AND contractid='$contractid'"); 

        $serviceid = $rand = substr(md5(microtime()),rand(0,26),5);
        
        $serviceid = $contractid.'/'.$serviceid;

        $servicejson = '[{"serviceid":"'.$serviceid.'","servicename":"'.$ftype.'","servicedate":"'.$servicedate.'","servicetime":"'.$servicetime.'"}]';
        
        $data2 = $_POST['data2'];

        $serviceIdJson = json_decode($data2);

        mysqli_query($con,"DELETE FROM wrokcontractfumigationandpestmortemservice WHERE  branchid = '$branchid' AND contractid = '$contractid'");

        mysqli_query($con,"UPDATE workcontractservice SET serviceid='$serviceid',servicedate='$servicedate',servicetime='$servicetime',servicename='$ftype',created_by='$created_by',created_time='$CURRENT_MILLIS' WHERE branchid='$branchid' AND enquiryid='$enquiryid' AND contractid='$contractid'");

        foreach ($serviceIdJson as $i) {

            $service     = get_object_vars($i)['service'];

            $servicename = get_object_vars($i)['servicename'];
            
            mysqli_query($con,"INSERT INTO wrokcontractfumigationandpestmortemservice (branchid ,customerid, contractid , serviceid , servicename) VALUES ('$branchid', '$customerid', '$contractid' ,'$service', '$servicename')");
            

        }

        $data4      = $_POST['data4'];

        $billIdjson = json_decode($data4);

        mysqli_query($con,"UPDATE billing SET workcontractstatus = '' WHERE branchid='$branchid' AND contractid = '$contractid'");

        mysqli_query($con,"DELETE FROM workcontract_billno WHERE  branchid = '$branchid' AND contractid = '$contractid'");

        foreach ($billIdjson as $i) {

            $billNo = get_object_vars($i)['billNo'];
            
            mysqli_query($con,"INSERT INTO workcontract_billno (branchid ,customerid, contractid , billno) VALUES ('$branchid', '$customerid', '$contractid' ,'$billNo')");
            
            mysqli_query($con,"UPDATE billing SET workcontractstatus = '$contractid' WHERE branchid='$branchid' AND invoice_bill = '$billNo' AND customerid = '$customerid'");

        }
            
        $update = mysqli_query($con,"UPDATE workcontract SET customerid = '$customerid', volume='$volume',quantity='$quantity',contractdate='$contractdate',contractstart='$servicedate',contractend='$servicedate',amount='$amount',cgstper='$cgstper',sgstper='$sgstper',igstper='$igstper',cgstamt='$cgstamt',sgstamt='$sgstamt',igstamt='$igstamt',othercharges='$othercharges',totalcontractamt='$totalcontractamt',billqty='1',servicejson='$data2',billjson='$billjson',billlinkjson = '$data4',itemjson='[]',ftype='$ftype',ftype2='$ftype2',fname='$fname',termofpayment='$termofpayment',nameofexporter='$nameofexporter',addressofexporter='$addressofexporter',billingparty='$billingparty',created_by='$created_by',created_time='$CURRENT_MILLIS',servicetime='$servicetime',servicelocation='$fumigationplace' WHERE branchid='$branchid' AND branchid='$branchid' AND contractid='$contractid'");

        if($update){

            mysqli_query($con,"UPDATE crmmaster SET checkcontract = 1 ,customerid = '$customerid'  WHERE contractid = '$contractid' AND branchid = '$branchid'");

        
            echo '{"status":"success"}';
        
        } else {
        
            echo '{"status":"falid1"}';
        
        }
                
        _close($con);

    } else {

        echo '{"status":"falid2"}';
    
    }

?>