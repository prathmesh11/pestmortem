<?php
    
  $base='../../../';

  include($base.'_in/connect.php');

  header('content-type: application/json; charset=utf-8');

  header("access-control-allow-origin: *");

  if(isset($_POST['data1']) && isset($_POST['data2']) && isset($_POST['data3']) && isset($_POST['data4']) && isset($_POST['data5']) && isset($_POST['data6']) && isset($_POST['data7'])){
    
    $con        = _connect();

    if (session_status()==PHP_SESSION_NONE) { session_start(); }

    $created_by      = $_SESSION['employeeid'];
    
    $branchid        = $_SESSION['branchid'];
    
    $contractid      = _clean($con,get_object_vars(json_decode($_POST['data1']))['contractid']);
    
    $enquiryid       = _clean($con,get_object_vars(json_decode($_POST['data1']))['enquiryid']);
    
    $customerid      = _clean($con,get_object_vars(json_decode($_POST['data1']))['customerid']);
    
    $contractdate    = _clean($con,get_object_vars(json_decode($_POST['data1']))['contractdate']);
    
    $contractstart   = _clean($con,get_object_vars(json_decode($_POST['data1']))['contractstart']);
    
    $contractend     = _clean($con,get_object_vars(json_decode($_POST['data1']))['contractend']);
    
    $amount          = _clean($con,get_object_vars(json_decode($_POST['data1']))['amount']);
    
    $cgstper         = _clean($con,get_object_vars(json_decode($_POST['data1']))['cgstper']);
    
    $sgstper         = _clean($con,get_object_vars(json_decode($_POST['data1']))['sgstper']);
    
    $igstper         = _clean($con,get_object_vars(json_decode($_POST['data1']))['igstper']);
    
    $cgstamt         = _clean($con,get_object_vars(json_decode($_POST['data1']))['cgstamt']);
    
    $sgstamt         = _clean($con,get_object_vars(json_decode($_POST['data1']))['sgstamt']);
    
    $igstamt         = _clean($con,get_object_vars(json_decode($_POST['data1']))['igstamt']);
    
    $othercharges    = _clean($con,get_object_vars(json_decode($_POST['data1']))['othercharges']);
    
    $contractamt     = _clean($con,get_object_vars(json_decode($_POST['data1']))['total']);
    // $servicelocation = _clean($con,get_object_vars(json_decode($_POST['data1']))['servicelocation']);
    $additinal       = _clean($con,get_object_vars(json_decode($_POST['data1']))['additinalcharges']);
    
    $paymentby       = _clean($con,get_object_vars(json_decode($_POST['data1']))['paymentaccptable']);
    
    $billqty         = _clean($con,get_object_vars(json_decode($_POST['data1']))['billqty']);
    
    $trendanalysis   = _clean($con,get_object_vars(json_decode($_POST['data1']))['trendanalysis']);

    $data2           = json_decode($_POST['data2']);
    
    $jsondata2       = '';

    mysqli_query($con,"DELETE FROM workcontractservice WHERE branchid='$branchid' AND enquiryid='$enquiryid' AND contractid='$contractid'");

    mysqli_query($con,"DELETE FROM wrokcontractfumigationandpestmortemservice WHERE  branchid = '$branchid' AND contractid = '$contractid'");

    foreach($data2 as $i){

      $serviceid = get_object_vars($i)['serviceids'];  

      $servicename = get_object_vars($i)['servicename'];  
      
      $arr         = get_object_vars($i)['arr'];  

      mysqli_query($con,"INSERT INTO wrokcontractfumigationandpestmortemservice (branchid ,customerid, contractid , serviceid , servicename) VALUES ('$branchid', '$customerid', '$contractid' ,'$serviceid', '$servicename')");
      
      foreach($arr as $k){

        $servicedate = (strtotime($k)*1000);
        
        $jsondata2.=',{"serviceid":"'.$serviceid.'","servicename":"'.$servicename.'","servicedate":"'.$servicedate.'"}';

        mysqli_query($con,"INSERT INTO workcontractservice (branchid,enquiryid,contractid,serviceid,servicedate,servicename,created_by,created_time) VALUES ('$branchid','$enquiryid','$contractid','$serviceid','$servicedate','$servicename','$created_by','$CURRENT_MILLIS')");
      
      }

    }

    $data3     = $_POST['data3'];
    
    $jsondata4 = '';
    
    $data4     = json_decode($_POST['data4']);

    mysqli_query($con,"DELETE FROM workcontractbill WHERE branchid='$branchid' AND enquiryid='$enquiryid' AND contractid='$contractid'");

    foreach($data4 as $i){

      $billdate = get_object_vars($i)['bill-1'];
    
      $billid   = $rand = substr(md5(microtime()),rand(0,26),5);
    
      $billid   = $serviceid=$contractid.'/'.$billid;
    
      $billdate = (strtotime($billdate)*1000);
    
      $jsondata4.=',{"billid":"'.$billid.'","billdate":"'.$billdate.'"}';
      
      mysqli_query($con,"INSERT INTO workcontractbill (branchid ,customerid,enquiryid ,contractid ,billid ,billdate ,cgstper ,sgstper ,igstper,cgstamt ,sgstamt ,igstamt ,other1, contractamt, created_by, created_time) VALUES ('$branchid' ,'$customerid','$enquiryid' ,'$contractid' ,'$billid' ,'$billdate' ,'$cgstper' ,'$sgstper' ,'$igstper' ,'$cgstamt', '$sgstamt', '$igstamt', '$othercharges','$contractamt' ,'$created_by' ,'$CURRENT_MILLIS')");

    }

    $jsondata2       = substr($jsondata2,1);
    
    $jsondata2       = '['.$jsondata2.']';
    
    $jsondata4       = substr($jsondata4,1);
    
    $jsondata4       = '['.$jsondata4.']';
    
    $data5           = $_POST['data5'];
    
    $servicelocation = $_POST['data6'];

    $data7           = $_POST['data7'];

    $serviceIdJson = json_decode($data7);

    mysqli_query($con,"UPDATE billing SET workcontractstatus = '' WHERE branchid='$branchid' AND contractid = '$contractid'");
    
    mysqli_query($con,"DELETE FROM workcontract_billno WHERE  branchid = '$branchid' AND contractid = '$contractid'");

    foreach ($serviceIdJson as $i) {

      $billNo = get_object_vars($i)['billNo'];
        
      mysqli_query($con,"UPDATE billing SET workcontractstatus = '$contractid' WHERE branchid='$branchid' AND invoice_bill = '$billNo' AND customerid = '$customerid'");
      
      mysqli_query($con,"INSERT INTO workcontract_billno (branchid ,customerid, contractid , billno) VALUES ('$branchid', '$customerid', '$contractid' ,'$billNo')");

    }

    // $billIdjson = json_decode($data4);


    // foreach ($billIdjson as $i) {

    //   $billNo = get_object_vars($i)['billNo'];
        
    //   mysqli_query($con,"INSERT INTO workcontract_billno (branchid ,customerid, contractid , billno) VALUES ('$branchid', '$customerid', '$contractid' ,'$billNo')");
        
    // }


    $update = mysqli_query($con,"UPDATE workcontract SET customerid='$customerid',contractdate ='$contractdate',contractstart ='$contractstart',contractend ='$contractend',amount ='$amount',cgstper ='$cgstper',sgstper ='$sgstper',igstper ='$igstper',cgstamt ='$cgstamt',sgstamt ='$sgstamt',igstamt ='$igstamt',othercharges ='$othercharges',totalcontractamt ='$contractamt',servicelocation ='$servicelocation',additinal ='$additinal',paymentby ='$paymentby',billqty ='$billqty',servicejson ='$jsondata2',billjson ='$jsondata4', billlinkjson = '$data7',enquipmentjson ='$data5',itemjson ='$data3',created_by ='$created_by',created_time='$created_time' WHERE branchid='$branchid' AND enquiryid='$enquiryid' AND contractid='$contractid'");

    if($update){

      mysqli_query($con,"UPDATE crmmaster SET customerid='$customerid',trendanalysis='$trendanalysis' WHERE enquiryid = '$enquiryid' AND contractid = '$contractid' AND branchid='$branchid' ");

      echo '{"status":"success"}';

    } else {

      echo '{"status":"falid"}';

    }
              
    _close($con);

  } else {

    echo '{"status":"falid"}';

  }



?>