<?php 
    
    $base    = '../../../';
    
    $navenq2 = 'background:#1B1464;';

    include('../header.php');

    if (!in_array("Work Contract", $arrysession)) {
   
        echo "<script>window.location.href='/process/dashboard.php';</script>";
       
        exit;

    }

    if (session_status() == PHP_SESSION_NONE) { session_start(); }

    $sessionby = $_SESSION['employeeid'];

    $branchid  = $_SESSION['branchid'];


    $year1 = 0;
    $year2 = 0;

    if ( date('m') > 3 ) {

        $year1 = date('y');

        $year2 = date('y')+1;

    } else {

        $year1 = date('y')-1;

        $year2 = date('y');
    }

    if ( date('m') > 6 ) { //financial year

    $year111 = date('Y');

    $year11 = date('Y') + 1;

    } else {
    $year11 = date('Y') - 1;

    $year111 = date('Y');
    }

    $first = $year111.'-04-01';

    $last  = $year11.'-03-31';

    if (isset($_GET['edit']) == 'true') {

        if(isset($_GET['contractid'])) {

            $no = $_GET['contractid']; 
        }
    

    } else { 
        
        $branchshortname = mysqli_fetch_assoc(mysqli_query($con,"SELECT branchshortname x FROM branchmaster WHERE branchid='$branchid'"))['x'];

        $contractid      = mysqli_fetch_assoc(mysqli_query($con,"SELECT count(contractid) x FROM  crmmaster WHERE category='Pest Control' AND contractid<>'' AND '$today' BETWEEN '$first' AND '$last' AND branchid = '$branchid'  Order By id desc "))['x'];

        if(!$contractid){

            $no = 'PMI/'.$branchshortname.'/ENQ/1/'.$year1.'-'.$year2;

        } else {

            $contractid++;

            $no ='PMI/'.$branchshortname.'/ENQ/'.$contractid.'/'.$year1.'-'.$year2;
        }
    }    

    if (isset($_GET['edit']) == 'true') {

        if(isset($_GET['enquiryid'])) {

            $enqno = $_GET['enquiryid']; 
        }
    

    } else {
        

        $enquiryidnew=mysqli_fetch_assoc(mysqli_query($con,"SELECT count(id) x FROM crmmaster WHERE  '$today' BETWEEN '$first' AND '$last' AND branchid = '$branchid'  ORDER BY id DESC"))['x'];

        if($enquiryidnew){

            $enquiryidnew++;

            $enquiryidnew = str_pad($enquiryidnew,6, '0', STR_PAD_LEFT);

            $enqno        = 'PMI/'.$branchshortname.'/ENQ/'.$enquiryidnew.'/'.$year1.'-'.$year2;

        }else{

            $enqno        = 'PMI/'.$branchshortname.'/ENQ/000001/'.$year1.'-'.$year2;

        }

    }

?>

<style>
.clr {
        clear:both;
    }
</style>

<br>

<input type="hidden" id="enquiryid" value="<?php echo $_GET['enquiryid']; ?>" data-role="text"
    class="form-control input-sm" />

<input type="hidden" id="contractid" value="<?php echo $_GET['contractid']; ?>" data-role="text"
    class="form-control input-sm" />

<input type="hidden" id="getEdit" value="<?php echo $_GET['edit']; ?>" data-role="text"
    class="form-control input-sm" />

<div class="container-fluid">

    <div class="row" id="section">

        <div class="col-sm-1">Contract No.</div>

        <div class="col-sm-3">

            <div class="form-group"><input type="text" value="<?php echo str_replace('ENQ', 'PC',$no); ?>"
                    data-role="text" class="form-control input-sm" data-name="contractid" readonly /></div>

        </div>

        <div class="col-sm-1">Customer</div>

        <div class="col-sm-3">

            <div class="form-group">

                <select class="form-control input-sm select-js" data-role="select" id="customerid" data-name="customerid">

                    <option value="Select">Select Customer</option>

                    <?php 
                          $account = mysqli_query($con,"SELECT customerid,customername FROM customermaster WHERE branchid='$branchid' ORDER BY customername ");

                            while ($row = mysqli_fetch_assoc($account)) {

                               echo '<option value="'.$row['customerid'].'">'.$row['customername'].'</option>';

                            }
                        ?>
                </select>

            </div>

        </div>

        <div class="col-sm-1">Enquiry No.</div>

        <div class="col-sm-3">

            <div class="form-group">

                <input type="text" value="<?php echo $enqno; ?>" data-role="text" class="form-control input-sm"
                    data-name="enquiryid" readonly />

            </div>

        </div>

    </div>

    <hr style="margin:0;border:1px solid #000;">

    <br>

    <div class="row">

        <div class="col-sm-6" id="section">

            <div class="col-sm-4">Contract Date</div>

            <div class="col-sm-8">

                <div class="form-group">

                    <input type="text" data-role="text" id="contractdate" class="datepicker form-control input-sm" data-name="contractdate" />

                </div>

            </div>

            <div class="col-sm-1">Period</div>

            <div class="col-sm-5">

                <div class="form-group">

                    <input type="text" data-role="text" class="datepicker form-control input-sm" data-name="contractstart" id="contractstart" />

                </div>

            </div>

            <div class="col-sm-1">TO</div>

            <div class="col-sm-5">

                <div class="form-group">

                    <input type="text" data-role="text" class="datepicker form-control input-sm contractend" data-name="contractend" id="contractend" />

                </div>

            </div>

            <div class="col-sm-2">Amount</div>

            <div class="col-sm-4">

                <div class="form-group">

                    <input type="text" value="0" data-role="number" class="form-control input-sm" id="amount" onkeyup="calc()" data-name="amount" />

                </div>

            </div>

            <div style="clear:both"></div>
            
            <div class="col-sm-2">CGST%</div>

            <div class="col-sm-2">

                <div class="form-group">

                    <input type="text" data-role="number" value="0" class="form-control input-sm" id="cgstper" onkeyup="calc()" data-name="cgstper" />

                </div>

            </div>

            <div class="col-sm-2">SGST%</div>

            <div class="col-sm-2">

                <div class="form-group">

                    <input type="text" data-role="text" value="0" class="form-control input-sm" id="sgstper" onkeyup="calc()" data-name="sgstper" />

                </div>

            </div>

            <div class="col-sm-2">IGST%</div>

            <div class="col-sm-2">

                <div class="form-group">

                    <input type="text" data-role="number" value="0" class="form-control input-sm" id="igstper" onkeyup="calc()" data-name="igstper" />

                </div>

            </div>

            <div class="col-sm-2">CGST AMT</div>

            <div class="col-sm-2">

                <div class="form-group">

                    <input type="text" data-role="number" value="0" class="form-control input-sm" id="cgstamt" onkeyup="calc()" data-name="cgstamt" readonly />

                </div>

            </div>

            <div class="col-sm-2">SGST AMT</div>

            <div class="col-sm-2">

                <div class="form-group">

                    <input type="text" data-role="number" value="0" class="form-control input-sm" id="sgstamt" onkeyup="calc()" data-name="sgstamt" readonly />
                    
                </div>

            </div>

            <div class="col-sm-2">IGST AMT</div>

            <div class="col-sm-2">

                <div class="form-group">
                    
                    <input type="text" data-role="number" value="0" class="form-control input-sm" id="igstamt" onkeyup="calc()" data-name="igstamt" readonly />

                </div>

            </div>

            <div class="col-sm-3" style="display:none;">Other charges</div>

            <div class="col-sm-4" style="display:none;">

                <div class="form-group"><input type="text" data-role="number" value="0" class="form-control input-sm"
                        id="othercharges" onkeyup="calc()" data-name="othercharges" /></div>

            </div>

            <div class="col-sm-2">Total</div>
            
            <div class="col-sm-4">

                <div class="form-group">

                    <input type="text" data-role="number" value="0" class="form-control input-sm" id="total" onkeyup="calc()" data-name="total" readonly />

                </div>

            </div>

        </div>

        <div class="col-sm-6" >
        
            <!-- <div class="col-sm-3">Service Locations</div>

            <div class="col-sm-9">

                <div class="form-group">

                    <input type="text" data-role="text" class="form-control input-sm" value="Same as Purchaser Address" data-name="servicelocation" />

                </div>

            </div> -->

            <div id="section">

                <div class="col-sm-3">Additional Declaration</div>

                <div class="col-sm-9">

                    <div class="form-group">
                    
                        <input type="text" value="-" data-role="text" class="form-control input-sm" data-name="additinalcharges" />

                    </div>

                </div>

                <div class="col-sm-3">Payment acceptable by</div>

                <div class="col-sm-9">

                    <div class="form-group">

                        <input type="text" data-role="text" class="form-control input-sm" value="Cheque" data-name="paymentaccptable" />

                    </div>

                </div>

                <div class="col-sm-3">Bill Quantity</div>

                <div class="col-sm-9">

                    <div class="form-group">

                        <input type="text" onkeyup="billqty(this)" id="billqty" data-role="number" class="form-control input-sm" data-name="billqty" />

                    </div>

                </div>

                <div class="col-sm-3">Trend Analysis required?</div>

                <div class="col-sm-9">

                    <div class="form-group">

                        <select class="form-control input-sm" data-role="select" data-name="trendanalysis">

                            <option value="No">No</option>

                            <option value="Yes">Yes</option>
                            
                        </select>

                    </div>

                </div>
                
            </div>

            <div class="col-sm-12" id="serviceLocationtableui">

                <hr style="border:1px solid #000;">

                <b>Add Service Location</b><br>

                <table class="table-list table form-group" id="table-serviceLocation" >

                    <thead>

                        <th>#</th>

                        <th>Service Location</th>

                        <th>Action</th>

                    </thead>

                    <tbody>

                        <td></td>

                        <td>
                            
                            <input type="text" data-role="text" class="form-control input-sm" value="Same as Purchaser Address" id="serLocation" data-name="servicelocation">
                            
                        </td>

                      
                        <td>
                            
                            <button class="btn btn-sm btn-primary center-block" id="btn-location" data-location="" onclick="addServiceLoaction(this)">Add</button>

                        </td>

                    </tbody>

                </table>

            </div>

        </div>
        
        <div class="col-sm-6" id="servicetableui">

            <hr style="border:1px solid #000;">

            <table class="table table-list form-group" id="table-order">

                <thead>

                    <th>#</th>

                    <th>Type of Services</th>

                    <th style="width:15%">Yes/No</th>

                    <th>Qty</th>

                    <th>Action</th>

                </thead>

                <tbody>

                    <tr>

                        <td></td>

                        <td>
                            <select class="form-control input-sm select-js" id="servicename" data-name="servicename">

                                <option value="Select">Select Service</option>

                                <?php

                                    $result4=mysqli_query($con,"SELECT servicename FROM servicemaster WHERE category='Pest Control' ORDER BY id ASC" );

                                        while($row = mysqli_fetch_assoc($result4)){  

                                            echo '<option value="'.$row['servicename'].'">'.$row['servicename'].'</option>';

                                        }   

                                
                                    ?>

                            </select>

                        </td>


                        <td>

                            <select class="yesno form-control input-sm" id="yesno" data-name="yesno">

                                <option value="NO">NO</option>

                                <option value="YES">YES</option>

                            </select>

                        </td>

                        <td>

                            <input class="frq form-control input-sm" id="frq" data-name="frq" />

                        </td>

                        <td>

                            <button class="btn btn-sm btn-primary" id="btn-add" data-edit="" onclick="add(this)">Add</button>

                        </td>

                    </tr>

                </tbody>

            </table>

            <div class="col-sm-12">

                <hr>

            </div>

            <div id="table-ui"></div>

        </div>

        <div class="col-sm-6">

            <div>

                <hr style="border:1px solid #000;">

                <b>Installation of Equipment</b><br>

                <table class="table-list table" id="table-enq">
                    <thead>

                        <th>#</th>
                        <th>Equipment</th>
                        <th>Qty</th>
                        <th>Returnable</th>
                        <th>Action</th>

                    </thead>

                    <tbody>

                        <tr>

                            <td></td>

                            <td>

                                <select class="form-control input-sm" data-role="select" id="machinename" data-name="machinename">

                                    <option value="Select">Select</option>

                                    <?php
                                        $result5=mysqli_query($con,"SELECT machinename,machineid FROM machinemaster WHERE asset='Equipment' ORDER BY id ASC");

                                            while($rows1 = mysqli_fetch_assoc($result5)){  

                                                echo '<option value="'.$rows1['machinename'].'">'.$rows1['machinename'].'</option>';

                                            }
                                    ?>

                                </select>

                            </td>

                            <td>

                                <input data-role="number" id="qty" data-name="qty" class="form-control input-sm" type="number" />

                            </td>

                            <td>

                                <select class="form-control input-sm" id="returnable" data-role="select" data-name="returnable">

                                    <option value="Select">Select</option>
                                    <option value="Yes">Yes</option>
                                    <option value="No">No</option>

                                </select>

                            </td>

                            <td>

                                <button class="btn btn-sm btn-primary" id="btn-equadd" data-editenq="" onclick="btnenq(this)">Add</button>

                            </td>

                        </tr>

                    </tbody>

                </table>

            </div>

            <div style="clear:both;"></div>

            <hr style="border:1px solid #000;">

            <table class="table-list table" id="tableadditem" style="display:none;">

                <thead>

                    <th width="500">Item Name</th>
                    <th> Qty</th>
                    <th style="display:none;">Gov. Qty</th>
                    <th>Action</th>

                </thead>

                <tbody>

                    <td width="500">

                        <select class="form-control input-sm" data-role="select" onchange="itemc(this)" data-name="itemcode">

                            <option value="Select">Select</option>

                            <?php

                                $result5=mysqli_query($con,"SELECT itemname,stockid FROM stockmaster");
                                while($rows1 = mysqli_fetch_assoc($result5)){  
                                    echo '<option value="'.$rows1['stockid'].'">'.$rows1['itemname'].'</option>';
                                }

                            ?>

                        </select>
                        
                    </td>

                    <td style="display:none;"><input type="text" class="form-control input-sm" id="itemname"
                            data-name="itemname"></td>

                    <td>
                        
                        <input type="text" data-role="number" class="form-control input-sm" data-name="actualqty">
                        
                    </td>

                    <td style="display:none;"><input type="text" value="" class="form-control input-sm"
                            data-name="govqty"></td>

                    <td>
                        
                        <button class="btn btn-sm btn-primary" onclick="additemadd()">Add</button>
                        
                    </td>

                </tbody>

            </table>

            <table class="table-list table" id="tablebillqty">
            
                <thead>
                            
                    <tr>
                        
                        <th width="60">SR No.</th>
                        <th width="300">Bill Date</th>

                    </tr>

                    <tbody>
                    </tbody>
                    
            </table>

        </div>

        <div class="col-sm-12">

            <hr>

        </div>

        <div class="col-sm-3"></div>

        <div class="col-sm-3">

            <button class="btn btn-sm btn-block btn-success" id="btn-submit" onclick="submit()">Submit</button>

        </div>

        <div class="col-sm-3">

            <button class="btn btn-sm btn-block btn-danger">Reset</button>

        </div>

        <div class="col-sm-12">

            <hr>

        </div>

    </div>

</div>


<?php 
include('../footer.php');
?>

<script>

//--------------------------Add Multiple Location start-------------------------------------//

function addServiceLoaction(e) {

    let chk = $(e).attr('data-location');

    let data = checker('table-serviceLocation');

    let servicelocation = data.servicelocation;

    let valid       = true;


    if (checker('table-serviceLocation') != false) {

        valid = valid * true;

    } else {

        valid = valid * false;

    }

     if (valid) {

        let chk = $(e).attr('data-location');

        if (chk == '') {

            let len = $('#table-serviceLocation .srno').length + 1;

            fun_addservicelocation(len, servicelocation);


        } else {

            fun_adder_servicelocation(chk, servicelocation);

        }

        modals.clear('table-serviceLocation');

        $('#btn-location').attr('data-location', '');

    }
    
}

function fun_addservicelocation(len, servicelocation) {

    var str = '<tr>';
    str += '<td align="center" class="srno">' + len + '</td>';
    str += '<td align="center" class="servicelocation">' + servicelocation + '</td>';
    str += '<td align="center"><button class="btn btn-sm btn-success" onclick="editServiceLocation(' + len + ',this)">E</button>  <button class="btn btn-sm btn-danger remover" onclick="removers(this)">R</buuton></td>';
    str += '</tr>';
    $('#table-serviceLocation > tbody').append(str);

}

function fun_adder_servicelocation(len, servicelocation) {

    var str = '<td align="center" class="srno">' + len + '</td>';
    str += '<td align="center" class="servicelocation">' + servicelocation + '</td>';
    str += '<td align="center"><button class="btn btn-sm btn-success" onclick="editServiceLocation(' + len + ',this)">E</button>  <button class="btn btn-sm btn-danger remover" onclick="removers(this)">R</buuton></td>';

    $('#table-serviceLocation .srno').each(function () {

        var srno = $(this).text().trim();

        if (srno == len) {

            $(this).parent().html(str);

        }

    });


}

function editServiceLocation(srno, e) {

    $('#serLocation').val($(e).parent().parent().find('.servicelocation').text().trim());

    $('#btn-location').attr('data-location', srno);

}


function removers(e) {

    $(e).parent().parent().remove();

}


//--------------------------Add Multiple Location End---------------------------------------//


//--------------------------Installation of Equipment add button start----------------------//

function btnenq(e) {

    var chk         = $(e).data('editenq');
    var data        = checker('table-enq');
    var machinename = data.machinename;
    var qty         = data.qty;
    var returnable  = data.returnable;
    var valid       = true;

    if (checker('table-enq') != false) {

        valid = valid * true;

    } else {

        valid = valid * false;

    }

    $('#table-enq .machinename').each(function () {

        var machinename1 = $(this).text().trim();

        if (machinename1 == machinename && chk == '') {

            valid = valid * false;
            alert('Dublicate Machinename Name');

        }

    });


    if (valid) {

        var chk = $(e).data('editenq');

        if (chk == '') {

            var len = $('#table-enq .srno').length + 1;
            fun_adderenq(len, machinename, qty, returnable);

        } else {

            fun_adder_editenq(chk, machinename, qty, returnable);

        }

        modals.clear('table-enq');

        $('#btn-equadd').data('editenq', '');

    }

}

// add function end :

// fun_adder function start :
function fun_adderenq(len, machinename, qty, returnable) {

    var str = '<tr>';
    str += '<td align="center" class="srno">' + len + '</td>';
    str += '<td align="center" class="machinename">' + machinename + '</td>';
    str += '<td align="center" class="qty">' + qty + '</td>';
    str += '<td align="center" class="returnable" >' + returnable + '</td>';
    str += '<td align="center"><button class="btn btn-sm btn-success" onclick="edit(' + len + ',this)">E</button>  <button class="btn btn-sm btn-danger remover" onclick="remove(this)">R</buuton></td>';
    str += '</tr>';
    $('#table-enq > tbody').append(str);

}
// fun_adder function end :

// fun_adder_edit function start :
function fun_adder_editenq(len, machinename, qty, returnable) {

    var str = '<td align="center" class="srno">' + len + '</td>';
    str += '<td align="center" class="machinename">' + machinename + '</td>';
    str += '<td align="center" class="qty">' + qty + '</td>';
    str += '<td align="center" class="returnable" >' + returnable + '</td>';
    str += '<td align="center"><button class="btn btn-sm btn-success" onclick="edit(' + len + ',this)">E</button>  <button class="btn btn-sm btn-danger remover" onclick="remove(this)">R</buuton></td>';

    $('#table-enq .srno').each(function () {

        var srno = $(this).text().trim();

        if (srno == len) {
            $(this).parent().html(str);
        }

    });


}

function edit(srno, e) {

    var machinename = $(e).parent().parent().find('.machinename').text().trim();
    $('#machinename').val(machinename);
    $('#qty').val($(e).parent().parent().find('.qty').text().trim());
    $('#returnable').val($(e).parent().parent().find('.returnable').text().trim());
    $('#btn-equadd').data('editenq', srno);

}


function remove(e) {

    $(e).parent().parent().remove();

}
//--------------------------Installation of Equipment add button End----------------------//

//--------------------------Services Add button start----------------------//

function add(e) {

    var chk         = $(e).data('edit');
    var data        = checker('table-order');
    var servicename = data.servicename;
    var yesno       = data.yesno;
    var frq         = data.frq;
    var valid       = true;

    if (checker('table-order') != false) {

        valid = valid * true;

    } else {

        valid = valid * false;

    }



    $('#table-order .serv').each(function () {

        var servicename1 = $(this).text().trim();

        if (servicename1 == servicename && chk == '') {

            valid = valid * false;
            alert('Dublicate Service Name');

        }

    });


    if (valid) {

        var chk = $(e).data('edit');

        if (chk == '') {

            var len = $('#table-order .srno').length + 1;
            fun_adder(len, servicename, yesno, frq);

        } else {

            fun_adder_edit(chk, servicename, yesno, frq);

        }

        modals.clear('table-order');
        $('#btn-add').data('edit', '');

    }

}

// add function end :

// fun_adder function start :
function fun_adder(len, servicename, yesno, frq) {

    var str = '<tr class="tds">';
    str += '<td align="center" class="srno">' + len + '</td>';
    str += '<td align="center" class="serv">' + servicename + '</td>';
    str += '<td align="center" class="yesno" data-service-name=' + servicename + ' onchange="funtb(this)">' + yesno + '</td>';
    str += '<td align="center" class="frq" data-service-name=' + servicename + ' onchange="funtb(this)">' + frq + '</td>';
    str += '<td align="center"><button class="btn btn-sm btn-success" onclick="editor(' + len + ',this)">E</button>  <button class="btn btn-sm btn-danger remover" onclick="remover(this)">R</buuton></td>';
    str += '</tr>';
    $('#table-order > tbody').append(str);
    funtb(this);

}

// fun_adder function end :

// fun_adder_edit function start :
function fun_adder_edit(len, servicename, yesno, frq) {

    var str = '<td align="center" class="srno">' + len + '</td>';
    str += '<td align="center" class="serv">' + servicename + '</td>';
    str += '<td align="center" class="yesno" data-service-name=' + servicename + ' onchange="funtb(this)">' + yesno + '</td>';
    str += '<td align="center" class="frq" data-service-name=' + servicename + ' onchange="funtb(this)">' + frq + '</td>';
    str += '<td align="center"><button class="btn btn-sm btn-success" onclick="editor(' + len + ',this)">E</button>  <button class="btn btn-sm btn-danger remover" onclick="remover(this)">R</buuton></td>';

    $('#table-order .srno').each(function () {

        var srno = $(this).text().trim();

        if (srno == len) {

            $(this).parent().html(str);
            
        }

    });

    funtb(this);

}
// fun_adder_edit function end :

//editor function start :
function editor(srno, e) {

    var servicecode = $(e).parent().parent().find('.serv').text().trim();
    $('#servicename').val(servicecode);
    $('#servicename').trigger('change');
    $('#yesno').val($(e).parent().parent().find('.yesno').text().trim());
    $('#frq').val($(e).parent().parent().find('.frq').text().trim());
    $('#btn-add').data('edit', srno);

}

function remover(e) {

    $(e).parent().parent().remove();

    funtb(e);

}
//--------------------------Services add button start----------------------//

//--------------------------bill Qty Function Start----------------------//

function billqty() {

    var billno = parseFloat($('#billqty').val());
    var str    = '';

    for (var i = 1; i <= billno; i++) {

        str += '<tr>';
        str += '<td>' + i + '</td>';
        str += '<td><input type="date" class="form-control input-sm billdata"></td>';
        str += '</tr>';

    }

    $('#tablebillqty > tbody').html(str);

}

//--------------------------bill Qty Function End----------------------//


function itemc(e) {

    var itemname = $(e).find('option:selected').text();
    $('#itemname').val(itemname);

}

function additemadd() {
    var valid = true;
    if (checker('tableadditem') != false) {
        valid = valid * true;
    } else {
        valid = valid * false;
    }
    if (valid) {
        var data = checker('tableadditem');
        var str = '<tr>';
        str += '<td class="itemname">' + data.itemname + '</td>';
        str += '<td class="itemcode" style="display:none;">' + data.itemcode + '</td>';
        str += '<td class="actualqty">' + data.actualqty + '</td>';
        str += '<td class="govqty">' + data.govqty + '</td>';
        str += '<td></td>';
        str += '</tr>';

        $('#tableadditem > tbody').append(str);
        modals.clear('tableadditem');
    }
}


//--------------------------Service Frq Function Start----------------------//

function funtb(e) {

    var arr = [];

    $('.tds').each(function () {

        var serv  = $(this).find('.serv').text().trim();
        var yesno = $(this).find('.yesno').text().trim();
        var frq   = $(this).find('.frq').text().trim();

        if (yesno != 'NO' && frq != 'Nil') {

            arr.push({
                "serv": serv,
                "yesno": yesno,
                "frq": frq
            });

            var str = '<table class="table-list table" id="service-table">';
            str += '<thead>';
            str += '<tr>';
            str += '<th>Service</th>';
            str += '<th>Freq</th>';
            str += '<th>Start Date</th>';
            str += '</tr>';
            str += '</thead>';
            str += '<tbody>';

            for (var i in arr) {

                str += '<tr>';
                str += '<td class="servicename">';
                str += arr[i].serv;
                str += '</td>';
                var frv = arr[i].frq;

                str += '<td class="frequency">' + frv + '</td>';
                str += '<td>';
                str += '<table class="table-list table">';
                str += '<thead>';
                str += '<tr>';
                str += '<th>Date</th>';
                str += '</tr>';
                str += '</thead>';
                str += '<tbody>';
                var counter1 = 0;
                for (var k = 1; k <= frv; k++) {
                    str += '<tr style="margin:0px;padding:0px;">';
                    if (counter1 == 0) {
                        str += '<td style="margin:0px;padding:0px;"><div class="form-group" style="margin:0px;padding:0px;"><input type="date" data-frv="' + frv + '" class="form-control input-sm datelist" onchange="gatedate(this)" /></div></td>';
                        counter1++;
                    } else {
                        str += '<td style="margin:0px;padding:0px;"><div class="form-group" style="margin:0px;padding:0px;"><input type="date" class="form-control input-sm datelist"  /></div></td>';
                        counter1++;
                    }
                    str += '</tr>';
                }
                str += '</tbody>';
                str += '</table>';

                str += '</td>';
                str += '</tr>';
            }
            str += '</tbody>';
            str += '</table>';
            $('#table-ui').html(str);
        } else {
            $('#table-ui').html('');
        }
    })
}
//--------------------------Service Frq Function End----------------------//

//--------------------------Datepicker Function Start----------------------//

$('.datepicker').pickadate({
    selectYears: true,
    selectMonths: true,
    onClose: function () {
        try {
            var dateStamp = this.get('select')['pick'];
            var id = this.get('id');
            $('#' + id).attr('data-stamp', dateStamp);
            $('#' + id).parent().find('.timepicker').click();
        } catch (err) {
            console.log(err);
        }
    }
});

//--------------------------Datepicker Function Start----------------------//

//--------------------------Getedate Function Start----------------------//

function gatedate(e) {
    var dateval = $(e).val();
    var dates1 = dateval.split("-");
    var newDate = dates1[1] + "/" + dates1[2] + "/" + dates1[0];
    var timstamp = new Date(newDate).getTime();
    // var timstamp=timstamp * 1000;

    var contractend = $('.contractend').data('stamp');

    var diff1 = contractend - timstamp;
    var frv = $(e).data('frv');
    var middiff = parseInt(diff1 / frv);
    var totalmiddiff = timstamp;
    // console.log(contractend, timstamp, diff1, frv, middiff);
    counter1 = 0;
    $(e).parent().parent().parent().parent().find('.datelist').each(function () {
        if (counter1 == 0) {
            counter1++;
        } else {
            totalmiddiff += middiff;
        }
        var d = new Date(totalmiddiff);
        var dd = d.getDate();
        var mm = d.getMonth() + 1;
        var yy = d.getFullYear();
        var len = mm.toString().length;
        var len2 = dd.toString().length;
        console.log(len);
        if (len2 != 2) {
            dd = '0' + dd;
        }
        if (len != 2) {
            mm = '0' + mm;
        }
        var newdate = yy + "-" + mm + "-" + dd;
        // console.log(newdate);
        $(this).val(newdate);
    });
}


//--------------------------Getedate Function End----------------------//



//--------------------------Insert Function End----------------------//

function submit() {

    var valid = true;

    if (checker('section') != false) {

        valid = valid * true;

    } else {

        valid = valid * false;

    }

    var data2 = [];

    $('#service-table > tbody > tr').each(function () {

        var servicename = $(this).find('.servicename').text();
        var frequency   = $(this).find('.frequency').text();
        var arr         = [];

        $(this).find('.datelist').each(function () {

            arr.push($(this).val());

        });

        data2.push({
            "servicename": servicename,
            "frequency": frequency,
            "arr": arr
        });

    })

    var data3 = [];

    $('#tableadditem > tbody > tr').each(function () {

        data3.push({
            "itemname": $(this).find('.itemname').text().trim(),
            "itemcode": $(this).find('.itemcode').text().trim(),
            "actualqty": $(this).find('.actualqty').text().trim(),
            "govqty": $(this).find('.govqty').text().trim()
        });

    });

    data3.shift();

    var data4 = [];

    $('#tablebillqty > tbody > tr').each(function () {

        var counter = 1;
        var str;
        if ($(this).find('.billdata').val() != '') {

            str = '{"bill-' + counter + '":"' + $(this).find('.billdata').val() + '"}';

        }

        str = JSON.parse(str);
        data4.push(str);
        counter++;

    });

    var data5 = [];

    $('#table-enq > tbody > tr').each(function () {

        data5.push({
            "machinename": $(this).find('.machinename').text().trim(),
            "qty": $(this).find('.qty').text(),
            "returnable": $(this).find('.returnable').text(),
        });

    });

    data5.shift();

    var data6 = [];

    $('#table-serviceLocation > tbody > tr').each(function () {

        var servicelocation = $(this).find('.servicelocation').text();

        if (servicelocation != '') {

            data6.push({
                "servicelocation": servicelocation,
            });

        }

    })

    if (data2 == '') {

        $('#servicetableui').css('border', '2px solid red');

        valid = valid * false;


    } else {

        valid = valid * true;

        $('#servicetableui').css('border', '2px solid green');

    }

    var billqty = parseInt($('#billqty').val());

    if (data4 == '' && billqty != data4.length) {

        valid = valid * false;

        $('#tablebillqty').css('border', '2px solid red');

    } else {

        valid = valid * true;

        $('#tablebillqty').css('border', '2px solid green');

    }

    if (data6 == '') {

        valid = valid * false;

        $('#serviceLocationtableui').css('border', '2px solid red');

    } else {

        valid = valid * true;

        $('#serviceLocationtableui').css('border', '2px solid green');

    }


    if (valid) {

        var data1    = checker('section');
        var datastr1 = JSON.stringify(data1);
        var datastr2 = JSON.stringify(data2);
        var datastr3 = JSON.stringify(data3);
        var datastr4 = JSON.stringify(data4);
        var datastr5 = JSON.stringify(data5);
        var datastr6 = JSON.stringify(data6);


        $.ajax({
            type: "POST",
            data: {
                data1: datastr1,
                data2: datastr2,
                data3: datastr3,
                data4: datastr4,
                data5: datastr5,
                data6: datastr6

            },
            url: 'pestcontrolcontract.php',
            cache: false,
            success: function (res) {
                if (res.status == 'success') {

                    swal({
                        type: 'success',
                        title: 'Pest Control Work Contract Create',
                        showConfirmButton: false,
                        timer: 1000
                    });

                    setTimeout(function () {
                        location.href = '/process/crm/workcontract.php';
                    }, 1000);

                }

            }

        });

    }

}

//--------------------------Insert Function End----------------------//

//--------------------------Edit Function Start----------------------//

if ($('#enquiryid').val() != '' && $('#contractid').val() != '' && $('#getEdit').val() == 'true') {

    $.ajax({
        type: "POST",
        url: 'pestcontrolselect.php',
        data: "enquiryid=" + $('#enquiryid').val() + "&contractid=" + $('#contractid').val(),
        cache: false,
        success: function (res) {

            if (res.status == 'success') {
                modals.putvalue('section', res.json);
                $('#btn-submit').attr('onclick', 'update()');
                $('#btn-submit').text('Update');
                $('#contractdate').val(res.json[0].contractdate);
                $('#contractdate').attr('data-stamp', res.json[0].contractdate1);
                $('#contractstart').val(res.json[0].contractstart1);
                $('#contractstart').attr('data-stamp', res.json[0].contractstart);
                $('#contractend').val(res.json[0].contractend1);
                $('#contractend').attr('data-stamp', res.json[0].contractend);
                $('#servicedate').val(res.json[0].servicedate);
                $('#servicedate').attr('data-stamp', res.json[0].servicedate1);
                calc();


                var enquipmentjson = res.json[0].enquipmentjson;
                var couter         = 1;

                for (var i in enquipmentjson) {

                    fun_adderenq(couter, enquipmentjson[i].machinename, enquipmentjson[i].qty, enquipmentjson[i].returnable);

                    couter++;

                }

                var servicejson  = res.json[0].servicejson;
                var servicejson1 = res.json[0].servicejson1;
                var couter       = 1;

                for (var i in servicejson) {

                    fun_adder(couter, servicejson[i].servicename, servicejson[i].yesno, servicejson[i].frequency);

                    couter++;

                }

                for (var i in servicejson1) {

                    $('.datelist:eq(' + i + ')').val(format(new Date(Number(servicejson1[i].servicedate)), 'yyyy-MM-dd'));

                }

                var billjson = res.json[0].billjson;

                billqty($('#billqty'));

                for (var i in billjson) {

                    $('.billdata:eq(' + i + ')').val(format(new Date(Number(billjson[i].billdate)), 'yyyy-MM-dd'));

                }

                var servicelocation = res.json[0].servicelocation;
                var couter         = 1;

                for (var i in servicelocation) {

                    fun_addservicelocation(couter, servicelocation[i].servicelocation);

                    couter++;

                }

            }

        }

    });

}

//--------------------------Edit Function Start----------------------//


//--------------------------Date Format Function Start----------------------//

format = function date2str(x, y) {
    var z = {
        M: x.getMonth() + 1,
        d: x.getDate(),
        h: x.getHours(),
        m: x.getMinutes(),
        s: x.getSeconds()
    };
    y = y.replace(/(M+|d+|h+|m+|s+)/g, function (v) {
        return ((v.length > 1 ? "0" : "") + eval('z.' + v.slice(-1))).slice(-2)
    });

    return y.replace(/(y+)/g, function (v) {
        return x.getFullYear().toString().slice(-v.length)
    });
}

//--------------------------Date Format Function End----------------------//


//--------------------------Update Function Start----------------------//

function update() {

    var valid = true;

    if (checker('section') != false) {

        valid = valid * true;

    } else {

        valid = valid * false;

    }

    var data2 = [];

    $('#service-table > tbody > tr').each(function () {

        var servicename = $(this).find('.servicename').text();
        var frequency   = $(this).find('.frequency').text();
        var arr         = [];

        $(this).find('.datelist').each(function () {

            arr.push($(this).val());

        });

        data2.push({
            "servicename": servicename,
            "frequency": frequency,
            "arr": arr
        });

    });

    var data3 = [];

    $('#tableadditem > tbody > tr').each(function () {

        data3.push({
            "itemname": $(this).find('.itemname').text().trim(),
            "itemcode": $(this).find('.itemcode').text().trim(),
            "actualqty": $(this).find('.actualqty').text().trim(),
            "govqty": $(this).find('.govqty').text().trim()
        });

    });

    data3.shift();

    var data4 = [];

    $('#tablebillqty > tbody > tr').each(function () {

        var counter = 1;
        var str;

        if ($(this).find('.billdata').val() != '') {

            str = '{"bill-' + counter + '":"' + $(this).find('.billdata').val() + '"}';

        }

        str = JSON.parse(str);
        data4.push(str);
        counter++;

    });

    var data5 = [];

    $('#table-enq > tbody > tr').each(function () {

        data5.push({
            "machinename": $(this).find('.machinename').text().trim(),
            "qty": $(this).find('.qty').text(),
            "returnable": $(this).find('.returnable').text(),
        });

    });

    data5.shift();

    var data6 = [];

    $('#table-serviceLocation > tbody > tr').each(function () {

        var servicelocation = $(this).find('.servicelocation').text();

        if (servicelocation != '') {

            data6.push({

                "servicelocation": servicelocation,

            });
            
        }
        
    })

    if (data2 == '') {

        $('#servicetableui').css('border', '2px solid red');

        valid = valid * false;


    } else {

        valid = valid * true;

        $('#servicetableui').css('border', '2px solid green');

    }

    var billqty = parseInt($('#billqty').val());

    if (data4 == '' && billqty != data4.length) {

        valid = valid * false;

        $('#tablebillqty').css('border', '2px solid red');

    } else {

        valid = valid * true;

        $('#tablebillqty').css('border', '2px solid green');

    }

    if (data6 == '') {

        valid = valid * false;

        $('#serviceLocationtableui').css('border', '2px solid red');

    } else {

        valid = valid * true;

        $('#serviceLocationtableui').css('border', '2px solid green');

    }


    if (valid) {

        var data1    = checker('section');
        var datastr1 = JSON.stringify(data1);
        var datastr2 = JSON.stringify(data2);
        var datastr3 = JSON.stringify(data3);
        var datastr4 = JSON.stringify(data4);
        var datastr5 = JSON.stringify(data5);
        var datastr6 = JSON.stringify(data6);


        $.ajax({
            type: "POST",
            data: {
                data1: datastr1,
                data2: datastr2,
                data3: datastr3,
                data4: datastr4,
                data5: datastr5,
                data6: datastr6

            },
            url: 'pestcontrolupdate.php',
            cache: false,
            success: function (res) {

                if (res.status == 'success') {

                    swal({
                        type: 'success',
                        title: 'Pest Control Work Contract Updated',
                        showConfirmButton: false,
                        timer: 1000
                    });

                    setTimeout(function () {
                        location.href = '/process/crm/workcontract.php';
                    }, 1000);

                }

            }

        });

    }

}

//--------------------------Update Function End----------------------//

//--------------------------Cal Function Start----------------------//

function calc() {
    var amount       = parseFloat($('#amount').val());
    var cgstper      = parseFloat($('#cgstper').val());
    var sgstper      = parseFloat($('#sgstper').val());
    var igstper      = parseFloat($('#igstper').val());
    var othercharges = parseFloat($('#othercharges').val());
    var cgstamt      = (amount * cgstper / 100).round(2);
    var sgstamt      = (amount * sgstper / 100).round(2);
    var igstamt      = (amount * igstper / 100).round(2);

    $('#cgstamt').val(cgstamt);
    $('#sgstamt').val(sgstamt);
    $('#igstamt').val(igstamt);

    var total = amount + cgstamt + sgstamt + igstamt + othercharges;

    $('#total').val(total.round(2));

}

//--------------------------Cal Function End----------------------//
$("#customerid").select2({disabled:"readonly"});

</script>