<?php

    $base = '../../../';

    include($base.'_in/connect.php');

    header('content-type: application/json; charset=utf-8');

    header("access-control-allow-origin: *");

    if(isset($_POST['data1']) && isset($_POST['data2'])){

        $con = _connect();
        
        if (session_status()==PHP_SESSION_NONE) { session_start(); }
      
        $created_by        = $_SESSION['employeeid'];
        
        $branchid          = $_SESSION['branchid'];

        $contractid        = _clean($con,get_object_vars(json_decode($_POST['data1']))['contractid']);

        $enquiryid         = _clean($con,get_object_vars(json_decode($_POST['data1']))['enquiryid']);
      
        $customerid        = mysqli_fetch_assoc(mysqli_query($con,"SELECT customerid x FROM `crmmaster` WHERE  enquiryid='$enquiryid' "))['x'];

        $contractdate      = _clean($con,get_object_vars(json_decode($_POST['data1']))['contractdate']);
      
        $ftype             = _clean($con,get_object_vars(json_decode($_POST['data1']))['ftype']);
      
        $ftype2            = _clean($con,get_object_vars(json_decode($_POST['data1']))['ftype2']);
      
        $quantity          = _clean($con,get_object_vars(json_decode($_POST['data1']))['quantity']);
      
        $volume            = _clean($con,get_object_vars(json_decode($_POST['data1']))['volume']);
      
        $fumigationplace   =  _clean($con,get_object_vars(json_decode($_POST['data1']))['fumigationplace']);

        $fname             = _clean($con,get_object_vars(json_decode($_POST['data1']))['fname']);
      
        $termofpayment     = _clean($con,get_object_vars(json_decode($_POST['data1']))['termofpayment']);
      
        $billdate          = _clean($con,get_object_vars(json_decode($_POST['data1']))['billremiderdate']);
      
        $nameofexporter    = _clean($con,get_object_vars(json_decode($_POST['data1']))['nameofexporter']);
      
        $addressofexporter = _clean($con,get_object_vars(json_decode($_POST['data1']))['addressofexporter']);
      
        $billingparty      = _clean($con,get_object_vars(json_decode($_POST['data1']))['billingparty']);
      
        $cert1             = _clean($con,get_object_vars(json_decode($_POST['data1']))['cert1']);
      
        $cert2             = _clean($con,get_object_vars(json_decode($_POST['data1']))['cert2']);
      
        $cert3             = _clean($con,get_object_vars(json_decode($_POST['data1']))['cert3']);
      
        $cert4             = _clean($con,get_object_vars(json_decode($_POST['data1']))['cert4']);
      
        $cert5             = _clean($con,get_object_vars(json_decode($_POST['data1']))['cert5']);
      
        $servicedate       = _clean($con,get_object_vars(json_decode($_POST['data1']))['servicedate']);
      
        $servicetime       = _clean($con,get_object_vars(json_decode($_POST['data1']))['servicetime']);
      
        $amount            = _clean($con,get_object_vars(json_decode($_POST['data1']))['amount']);
      
        $cgstper           = _clean($con,get_object_vars(json_decode($_POST['data1']))['cgstper']);
      
        $sgstper           = _clean($con,get_object_vars(json_decode($_POST['data1']))['sgstper']);
      
        $igstper           = _clean($con,get_object_vars(json_decode($_POST['data1']))['igstper']);
      
        $cgstamt           = _clean($con,get_object_vars(json_decode($_POST['data1']))['cgstamt']);
      
        $sgstamt           = _clean($con,get_object_vars(json_decode($_POST['data1']))['sgstamt']);
      
        $igstamt           = _clean($con,get_object_vars(json_decode($_POST['data1']))['igstamt']);
      
        $othercharges      = _clean($con,get_object_vars(json_decode($_POST['data1']))['othercharges']);
      
        $totalcontractamt  = _clean($con,get_object_vars(json_decode($_POST['data1']))['total']);
   
        $json1             = '[{"cert":"'.$cert1.'","cert":"'.$cert2.'","cert":"'.$cert3.'","cert":"'.$cert4.'","cert":"'.$cert5.'"}]';
      
        $itemjson          = $_POST['data2'];

        if (file_exists($_FILES['data3']['tmp_name']) || is_uploaded_file($_FILES['data3']['tmp_name'])){

            $contractidt = str_replace('/', '-',$contractid);

            $path        = getcwd()."/photo/".$contractidt.'.jpg';

            if(file_exists('photo/'.$contractidt.'.jpg')){
                
                unlink('photo/'.$contractidt.'.jpg');

            }

            @move_uploaded_file($_FILES['data3']['tmp_name'], $path);
        }

        $billid = $rand = substr(md5(microtime()),rand(0,26),5);
    
        $billid = $serviceid=$contractid.'/'.$billid;
    
        $billjson='[{"billid":"'.$billid.'","billdate":"'.$billdate.'"}]';
    
        mysqli_query($con,"INSERT INTO workcontractbill (branchid ,customerid,enquiryid ,contractid ,billid ,billdate ,cgstper ,sgstper ,igstper, cgstamt ,sgstamt ,igstamt ,other1 ,contractamt, created_by, created_time) VALUES ('$branchid' ,'$customerid','$enquiryid' ,'$contractid' ,'$billid' ,'$billdate' ,'$cgstper' ,'$sgstper' ,'$igstper' ,'$cgstamt', '$sgstamt', '$igstamt', '$othercharges','$totalcontractamt' ,'$created_by' ,'$CURRENT_MILLIS')");

        $serviceid = $rand = substr(md5(microtime()),rand(0,26),5);
    
        $serviceid = $contractid.'/'.$serviceid;
    
        $servicejson = '[{"serviceid":"'.$serviceid.'","servicename":"'.$ftype.'","servicedate":"'.$servicedate.'","servicetime":"'.$servicetime.'"}]';

        mysqli_query($con,"INSERT INTO workcontractservice (branchid,enquiryid,contractid,serviceid,servicedate,servicetime,servicename,created_by,created_time) VALUES ('$branchid','$enquiryid','$contractid','$serviceid','$servicedate','$servicetime','$ftype','$created_by','$CURRENT_MILLIS')");
        
        $create=mysqli_query($con,"INSERT INTO workcontract (volume,quantity,branchid ,enquiryid ,contractid ,contractdate ,contractstart ,contractend ,servicetime,amount ,cgstper ,sgstper ,igstper ,cgstamt ,sgstamt ,igstamt ,othercharges ,totalcontractamt ,billqty ,servicejson ,billjson ,itemjson ,ftype ,ftype2 ,fname ,termofpayment ,nameofexporter ,addressofexporter ,billingparty ,created_by ,created_time,servicelocation) VALUES  ('$volume','$quantity','$branchid' ,'$enquiryid' ,'$contractid' ,'$contractdate' ,'$servicedate' ,'$servicedate' ,'$servicetime','$amount' ,'$cgstper' ,'$sgstper' ,'$igstper' ,'$cgstamt' ,'$sgstamt' ,'$igstamt' ,'$othercharges' ,'$totalcontractamt' ,'1' ,'$servicejson' ,'$billjson' ,'$itemjson' ,'$ftype' ,'$ftype2' ,'$fname' ,'$termofpayment' ,'$nameofexporter' ,'$addressofexporter' ,'$billingparty' ,'$created_by' ,'$CURRENT_MILLIS','$fumigationplace')");
          
        if($create){
      
            mysqli_query($con,"UPDATE crmmaster SET contractid = '$contractid' WHERE enquiryid = '$enquiryid'");
      
            echo '{"status":"success"}';
      
        } else {
      
            echo '{"status":"falid1"}';
      
        }
                
        _close($con);

    } else {

        
        echo '{"status":"falid2"}';
     
    }

?>