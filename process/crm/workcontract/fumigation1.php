<?php 

    $base    = '../../../';

    $navenq2 = 'background:#1B1464;';

    include('../header.php');

    if (!in_array("Work Contract", $arrysession)) {
   
        echo "<script>window.location.href='/process/dashboard.php';</script>";
       
        exit;

    }


    if (isset($_GET['edit']) == 'true') {

        if(isset($_GET['contractid'])) {

            $contractid = $_GET['contractid']; 
        }
    

    } else { 

        if(isset($_GET['contractid'])) {

            $contractid = $_GET['contractid']; 

        }
        
    }    


    if (session_status() == PHP_SESSION_NONE) { session_start(); }

    $sessionby = $_SESSION['employeeid'];

    $branchid  = $_SESSION['branchid'];

    $yrdata             = strtotime($today);

    $todayDate          =  date('d F, Y',  $yrdata);

    $todayDateTimeStamp = $yrdata*1000;

?>

<style>

    .table-list td,
    .table-list th {
        border: 1px solid #ddd;
        padding: 1px !important;
        font-size: 13px;
    }

    .table-list td {
        padding-top: 10px !important;
    }

    .table-list tr:nth-child(even) {
        background-color: #f2f2f2;
    }

    .table-list th {
        padding-top: 5px;
        padding-bottom: 5px;
        text-align: center;
        background-color: #16a085;
        color: white;
    }

</style>

<br>
<input type="hidden" id="enquiryid" value="<?php echo $_GET['enquiryid']; ?>" data-role="text"
    class="form-control input-sm" />

<input type="hidden" id="contractid" value="<?php echo $_GET['contractid']; ?>" data-role="text"
    class="form-control input-sm" />

    <input type="hidden" id="editCheck" value="<?php echo $_GET['edit']; ?>" data-role="text"
    class="form-control input-sm" />

<div class="container-fluid">

    <div class="row" id="section">

        <div class="col-sm-1">Contract No.</div>

        <div class="col-sm-2">

            <div class="form-group">
                
                <input type="text" value="<?php echo $contractid; ?>"
                    data-role="text" class="form-control input-sm" data-name="contractid" readonly />
            
            </div>
            
        </div>

        <div class="col-sm-1">Customer</div>

        <div class="col-sm-3">

            <div class="form-group">

                <select class="form-control input-sm select-js" data-role="select" id="customerid" data-name="customerid" onchange="putbillno()">

                    <option value="Select">Select Customer</option>

                    <?php 
                          $account = mysqli_query($con,"SELECT customerid,customername FROM customermaster WHERE branchid='$branchid' ORDER BY customername ");

                            while ($row = mysqli_fetch_assoc($account)) {

                               echo '<option value="'.$row['customerid'].'">'.$row['customername'].'</option>';

                            }
                        ?>
                </select>

            </div>

        </div>

        <div class="col-sm-3">
                                        
            <a data-fancybox="newentry"  href="javascript:void(0)" id="type1box" data-type="iframe" data-src="/process/create-master/customer-master/customermaster.php" class="btn btn-danger btn-sm">
                                                    
                Create New Customer
            
            </a>
            
        </div>

        <div class="col-sm-6"></div>

        <div class="col-sm-1" style="display:none;">Enquiery No.</div>

        <div class="col-sm-2" style="display:none;">

            <div class="form-group">
                
                <input type="text" value="<?php echo $enqno; ?>" class="form-control input-sm" data-name="enquiryid" readonly />
            
            </div>

        </div>

    </div>

    <hr style="margin:0;border:1px solid #000;">

    <br>

    <div class="row">

        <div class="col-sm-6" id="section">

            <div class="col-sm-4">NAME OF FUMIGANT</div>

            <div class="col-sm-8">

                <div class="form-group">

                    <select class="form-control input-sm" data-role="select" data-name="fname">

                        <option value="ALP">ALP</option>

                        <option value="MBR">MBR</option>

                        <option value="ALP AND MBR">ALP AND MBR</option>

                    </select>

                </div>

            </div>

            <div class="col-sm-4">Place Of Fumigation</div>

            <div class="col-sm-8">

                <div class="form-group">
                    
                    <input type="text" data-role="text" class="form-control input-sm" data-name="fumigationplace" value = "Same as Customer Address"/>
                
                </div>

            </div>

            <div class="col-sm-4">Contract Date</div>

            <div class="col-sm-8">

                <div class="form-group">

                    <input type="text" data-role="text" class="form-control input-sm datepicker" id="contractdate"
                        value="<?php echo $todayDate;?>" data-stamp="<?php echo $todayDateTimeStamp;?>"
                        data-name="contractdate" />
                
                </div>

            </div>

            <div class="col-sm-4">TYPE</div>

            <div class="col-sm-8">

                <div class="form-group">

                    <select class="form-control input-sm" data-role="select" onchange="ftype(this)" data-name="ftype">

                        <option value="Select">Select</option>

                        <option value="WAREHOUSE">WAREHOUSE</option>

                        <option value="CONTAINER">CONTAINER</option>

                        <option value="PALLET">PALLET</option>

                        <option value="PLANT">PLANT</option>

                        <option value="VESSEL">VESSEL</option>

                    </select>

                </div>

            </div>

            <div class="col-sm-4" id="ftypedetails" style="display:none;"></div>

            <div class="col-sm-8" style="display:none;" id="ftypedetails1"></div>
            
            <div class="col-sm-4">QUANTITY</div>

            <div class="col-sm-8">

                <div class="form-group">
                    
                    <input type="text" class="form-control input-sm" data-name="quantity" />
                
                </div>

            </div>

            <div class="col-sm-4">VOLUME</div>

            <div class="col-sm-8">

                <div class="form-group">
                    
                    <input type="text" class="form-control input-sm" data-name="volume" />
                
                </div>

            </div>




            <div class="col-sm-4">TERM OF PAYMENT</div>

            <div class="col-sm-8">

                <div class="form-group">
                    
                    <input type="text" data-role="text" value="-" class="form-control input-sm"
                        data-name="termofpayment" />
                
                </div>

            </div>

            <div class="col-sm-4">BILLS REMINDER DATE</div>

            <div class="col-sm-8">

                <div class="form-group">
                    
                    <input type="text" data-role="text" class="form-control input-sm datepicker"
                        id="billremiderdate" data-name="billremiderdate" value="<?php echo $todayDate;?>" data-stamp="<?php echo $todayDateTimeStamp;?>" />
                
                </div>

            </div>


        </div>



        <div class="col-sm-6" >

            <table class="table-list table" id="section11">

                <thead>

                    <th>#</th>

                    <th>BILL NO.</th>

                    <th>Action</th>

                </thead>

                <tbody>

                    <tr>
                            
                        <td></td>

                        <td>
                            
                            <select class="form-control input-sm select-js" data-role="select" id="billNo" data-name="billNo">

                                <option value="Select">Select Bill No</option>

                            </select>
                        
                        </td>

                        <td>
                            
                            <button class="btn btn-sm btn-primary center-block" id="btn-add" data-edit=""  onclick="btnenq(this)">Add</button>
                        
                        </td>

                    </tr>

                </tbody>

            </table>

            <table class="table table-list form-group" id="table-order">

                <thead>

                    <th>#</th>

                    <th>Service Name</th>

                    <th width="188px;">Action</th>

                </thead>

                <tbody>

                    <tr>
                        
                        <td></td>

                        <td>

                            <div class="form-group">

                                <select class="select-js form-control input-sm" data-role='select' id="service" data-name="service">

                                    <option value="Select">Select</option>

                                    <?php
                                    
                                        $select = mysqli_query($con,"SELECT servicename,serviceid FROM servicemaster WHERE category = 'Fumigation' ORDER BY servicename ");

                                        while($rows = mysqli_fetch_assoc($select)){
                                                                
                                            echo '<option value="'.$rows['serviceid'].'">'.$rows['servicename'].'</option>';
                                                                
                                        } 
                                    
                                    ?>

                                </select>

                            </div>
                        
                        </td>

                        <td>

                            <button class="btn btn-success btn-sm center-block" id="btn-service" data-editservice="" onclick="add(this)">Add</button>

                        </td>

                    </tr>
                        
                </tbody>
                    
            </table> 
            
            <div class="col-sm-5">JOB ALLOTED DATE AND TIME</div>

            <div class="col-sm-4" id="section">

                <div class="form-group">
                    
                    <input type="text" class="form-control input-sm datepicker" data-role="text"
                        id="servicedate" data-name="servicedate" value="<?php echo $todayDate;?>" data-stamp="<?php echo $todayDateTimeStamp;?>" />
                
                </div>

            </div>

            <div class="col-sm-3" id="section">

                <div class="form-group">
                    
                    <input type="time" class="form-control input-sm " data-role="text" id="servicetime" data-name="servicetime" />
                
                </div>

            </div>

            <div class="col-sm-2">Amount</div>

            <div class="col-sm-4" id="section">

                <div class="form-group">

                    <input type="text" value="0" data-role="number" class="form-control input-sm" id="amount" data-name="amount" />

                    
                    <!-- <input type="text" value="0" data-role="number" class="form-control input-sm" id="amount" onkeyup="calc()" data-name="amount" /> -->
                
                </div>

            </div>

            <div class="col-sm-12"></div>

            <div class="col-sm-2" style="display:none;">CGST%</div>

            <div class="col-sm-2" style="display:none;">

                <div class="form-group">
                    
                    <input type="text" data-role="number" value="0" class="form-control input-sm"
                        id="cgstper" onkeyup="calc()" data-name="cgstper" />
                
                </div>

            </div>

            <div class="col-sm-2" style="display:none;">SGST%</div>

            <div class="col-sm-2" style="display:none;">

                <div class="form-group">
                    
                    <input type="text" data-role="text" value="0" class="form-control input-sm"
                        id="sgstper" onkeyup="calc()" data-name="sgstper" />
                
                </div>

            </div>

            <div class="col-sm-2" style="display:none;">IGST%</div>

            <div class="col-sm-2" style="display:none;">

                <div class="form-group">
                    
                    <input type="text" data-role="number" value="0" class="form-control input-sm"
                        id="igstper" onkeyup="calc()" data-name="igstper" />
                
                </div>

            </div>

            <div class="col-sm-2" style="display:none;">CGST AMT</div>

            <div class="col-sm-2" style="display:none;">

                <div class="form-group">
                    
                    <input type="text" data-role="number" value="0" class="form-control input-sm"
                        id="cgstamt" onkeyup="calc()" data-name="cgstamt" readonly />
                
                </div>
                
            </div>

            <div class="col-sm-2" style="display:none;">SGST AMT</div>

            <div class="col-sm-2" style="display:none;">

                <div class="form-group">
                    
                    <input type="text" data-role="number" value="0" class="form-control input-sm"
                        id="sgstamt" onkeyup="calc()" data-name="sgstamt" readonly />
                
                </div>

            </div>

            <div class="col-sm-2" style="display:none;">IGST AMT</div>

            <div class="col-sm-2" style="display:none;">

                <div class="form-group">
                    
                    <input type="text" data-role="number" value="0" class="form-control input-sm"
                        id="igstamt" onkeyup="calc()" data-name="igstamt" readonly />
                
                </div>
            
            </div>

            <div class="col-sm-3" style="display:none;">Other charges</div>

            <div class="col-sm-4" style="display:none;">

                <div class="form-group">
                    
                    <input type="text" data-role="number" value="0" class="form-control input-sm"
                        id="othercharges" onkeyup="calc()" data-name="othercharges" />
                
                </div>
            
            </div>

            <div class="col-sm-2" style="display:none;">Total</div>

            <div class="col-sm-4" style="display:none;">

                <div class="form-group">
                    
                    <input type="text" data-role="number" value="0" class="form-control input-sm"
                        id="total" onkeyup="calc()" data-name="total" readonly />
                
                </div>
            
            </div>

        </div>

        <div class="col-sm-12">

        
                    <div class="col-sm-3"></div>
                    <div class="col-sm-3"><button class="btn btn-sm btn-block btn-success" id="btn-submit"
                            onclick="submit()">Submit</button></div>
                    <div class="col-sm-3"><button class="btn btn-sm btn-block btn-danger">Reset</button></div>
        </div>

    </div>
</div>

<?php

    include('../footer.php');

?>

<script>

    $("[data-fancybox='newentry']").fancybox({
        smallBtn: true,
        toolbar: false,
        afterClose: function () {
            parent.location.reload(true);
        },
        iframe: {
            css: {
                width: '100%',
                height: '100%',
            }
        }
    });

    $('.datepicker').pickadate({

        selectYears: true,

        selectMonths: true,

        onClose: function () {

            try {

                var dateStamp = this.get('select')['pick'];
                
                var id = this.get('id');

                $('#' + id).attr('data-stamp', dateStamp);

                $('#' + id).parent().find('.timepicker').click();

            } catch (err) {

                console.log(err);

            }

        }

    });

    function ftype(e) {

        if ($(e).val() == 'WAREHOUSE') {

            $('#ftypedetails').text('NAME OF WAREHOUSE');
            
            $('#ftypedetails1').html('<div class="form-group"><input type="text" data-role="text" class="form-control input-sm" data-name="ftype2" /></div>');
        
        }

        if ($(e).val() == 'CONTAINER') {
            
            $('#ftypedetails').text('PHOTO OF CONTAINER NO.');
            
            $('#ftypedetails1').html('<div class="form-group"><input type="file" name="ftype2" id="fileattr" class="form-control input-sm" data-name="ftype2" accept="image/*" /></div>');
        
        }
        
        if ($(e).val() == 'PALLET') {
          
            $('#ftypedetails').text('NO OF PALLETS');
            
            $('#ftypedetails1').html('<div class="form-group"><input type="text" data-role="number" class="form-control input-sm" data-name="ftype2" /></div>');
        
        }

        if ($(e).val() == 'PLANT') {
        
          $('#ftypedetails').text('NAME OF PLANT');
        
          $('#ftypedetails1').html('<div class="form-group"><input type="text" data-role="text" class="form-control input-sm" data-name="ftype2" /></div>');
        
        }
      
        if ($(e).val() == 'VESSEL') {
        
          $('#ftypedetails').text('NAME OF VESSEL');
        
          $('#ftypedetails1').html('<div class="form-group"><input type="text" data-role="text" class="form-control input-sm" data-name="ftype2" /></div>');
      
        }

        
        $('#ftypedetails1').show();
        
        $('#ftypedetails').show();

    }

    function putbillno() {

        let customerid = $('#customerid').val();

        if (customerid != 'Select') {

             $.ajax({

                type: "POST",

                data: "customerid=" + customerid + "&category=Fumigation" + "&edit=" + $('#editCheck').val() + "&contractid=" + $('#contractid').val(),

                url: 'fetchbillno.php',

                cache: false,

                success: function (res) {

                    if (res.status == 'success') {
                        
                        let json = res.json;

                        let str = '<option value="Select">Select Bill No</option>';

                        for (let i in json) {

                            str += '<option value="'+json[i].billno+'">'+json[i].billno+'</option>';

                            
                        }

                        $('#billNo').html(str);
                    }

                }

            })
            
        } else {

            $('#billNo').html('<option value="Select">Select Bill No</option>');
            
        }
        
    }

    function btnenq(e) {

        var chk = $(e).attr('data-edit');

        var data = checker('section11');

        var billNo = data.billNo;

        var valid = true;

        if (checker('section11') != false) {

            valid = valid * true;

        } else {

            valid = valid * false;

        }


        $('#section11 .billNo').each(function () {

            var billNo1 = $(this).text().trim();

            if (billNo1 == billNo && chk == '') {

                valid = valid * false;

                alert('Dublicate Bill No');

            }

        });

        if (valid) {

            var chk = $(e).attr('data-edit');

            if (chk == '') {

                var len = $('#section11 .srNo').length + 1;

                fun_adder(len, billNo);

            } else {

                fun_adder_edit(chk, billNo);

            }

            modals.clear('section11');

            $('#btn-add').attr('data-edit', '');

        }

    }

    function fun_adder(len, billNo) {

        var str = '<tr>';
            
            str += '<td align="center" class="srNo">' + len + '</td>';
            
            str += '<td align="center" class="billNo">' + billNo + '</td>';
            
            str += '<td align="center"><button class="btn btn-sm btn-success" onclick="contactEditor(' + len + ',this) ">E</button>  <button class="btn btn-sm btn-danger remover" onclick="removers(this)">R</buuton></td>';
        
        str += '</tr>';

        $('#section11 > tbody').append(str);

        tableChecker();

    }

    function fun_adder_edit(len, billNo) {

        var str = '<td align="center" class="srNo">' + len + '</td>';
            
            str += '<td align="center" class="billNo">' + billNo + '</td>';
            
            str += '<td align="center"><button class="btn btn-sm btn-success" onclick="contactEditor(' + len + ',this)">E</button>  <button class="btn btn-sm btn-danger remover" onclick="removers(this)">R</buuton></td>';

        $('#section11 .srNo').each(function () {
            
            var srNo = $(this).text().trim();
            
            if (srNo == len) {
            
                $(this).parent().html(str);
            
            }
        
        });

        tableChecker();
    }

    function contactEditor(srNo, e) {

        var billNo = $(e).parent().parent().find('.billNo').text().trim();

        $('#billNo').val(billNo).trigger('change');

        $('#btn-add').attr('data-edit', srNo);

    }

    function tableChecker() {

        let tableOrderLenght = $('#section11 > tbody > tr').length - 1;

        if (tableOrderLenght == 0) {

            $('#customerid').prop('disabled', false);
            
        } else {

            $('#customerid').prop('disabled', 'disabled');

        }

        
    }

    function removers(e) {

        $(e).parent().parent().remove();

        tableChecker();

    }

    function add(e) {

        var chk = $(e).attr('data-editservice');

        var data = checker('table-order');

        var service = data.service;

        var servicename = $('#service').find(":selected").text();

        var valid = true;

        if (checker('table-order') != false) {

            valid = valid * true;

        } else {

            valid = valid * false;

        }


        $('#table-order .service').each(function () {

            var service1 = $(this).text().trim();

            if (service1 == service && chk == '') {

                valid = valid * false;

                alert('Dublicate service name');

            }

        });

        if (valid) {

            var chk = $(e).attr('data-editservice');

            if (chk == '') {

                var len = $('#table-order .srNo').length + 1;

                fun_adder_service(len, service, servicename);

            } else {

                fun_adder_service_edit(chk, service, servicename);

            }

            modals.clear('table-order');

            $('#btn-service').attr('data-editservice', '');

        }

    }


    function fun_adder_service(len, service, servicename) {

        var str = '<tr>';
            
            str += '<td align="center" class="srNo">' + len + '</td>';
            
            str += '<td align="center" class="service" style="display:none;">' + service + '</td>';
            
            str += '<td align="center" class="servicename">' + servicename + '</td>';
            
            str += '<td align="center"><button class="btn btn-sm btn-success" onclick="serviceEditor(' + len + ',this) ">E</button>  <button class="btn btn-sm btn-danger remover" onclick="remover(this)">R</buuton></td>';
        
        str += '</tr>';
        
        $('#table-order > tbody').append(str);

    }

    function fun_adder_service_edit(len, service, servicename) {

        var str = '<td align="center" class="srNo">' + len + '</td>';
            
            str += '<td align="center" class="service" style="display:none;">' + service + '</td>';
            
            str += '<td align="center" class="servicename">' + servicename + '</td>';
            
            str += '<td align="center"><button class="btn btn-sm btn-success" onclick="serviceEditor(' + len + ',this)">E</button>  <button class="btn btn-sm btn-danger remover" onclick="remover(this)">R</buuton></td>';

        $('#table-order .srNo').each(function () {
            
            var srNo = $(this).text().trim();
            
            if (srNo == len) {
            
                $(this).parent().html(str);
            
            }
        
        });

    }

    function serviceEditor(srNo, e) {

        var servicecode = $(e).parent().parent().find('.service').text().trim();

        $('#service').val(servicecode).trigger('change');

        $('#btn-service').attr('data-editservice', srNo);

    }

    function itemc(e) {
    
      var itemname = $(e).find('option:selected').text();
    
      $('#itemname').val(itemname);
    
    }


    function additemadd() {

        var valid = true;

        if (checker('tableadditem') != false) {

            valid = valid * true;

        } else {

            valid = valid * false;

        }

        if (valid) {

            var data = checker('tableadditem');

            var str = '<tr>';

                str += '<td class="itemname">' + data.itemname + '</td>';

                str += '<td class="itemcode" style="display:none;">' + data.itemcode + '</td>';

                str += '<td class="actualqty">' + data.actualqty + '</td>';

                str += '<td class="govqty">' + data.govqty + '</td>';

                str += '<td></td>';

            str += '</tr>';

            $('#tableadditem > tbody').append(str);

            modals.clear('tableadditem');

        }

    }

    function submit() {

        var valid = true;

        if (checker('section') != false) {

            valid = valid * true;

        } else {

            valid = valid * false;

        }

        var data2 = [];

        $('#table-order > tbody > tr').each(function () {

            var service     = $(this).find('.service').text();

            var servicename = $(this).find('.servicename').text();


            if (service != '') {

                data2.push({

                    "service": service,

                    "servicename": servicename,

                });

            }

        })

        if (data2 == '') {

            $('#table-order').css('border', '2px solid red');

            valid = valid * false;


        } else {

            valid = valid * true;

            $('#table-order').css('border', '2px solid green');

        }

        //data2.shift();

        var data3 = [];

        $('#section11 > tbody > tr').each(function () {

            var billNo = $(this).find('.billNo').text();

            if (billNo != '') {

                data3.push({
                    "billNo": billNo,
                });

            }

        })

        if (data3 == '') {

            $('#section11').css('border', '2px solid red');

            valid = valid * false;


        } else {

            valid = valid * true;

            $('#section11').css('border', '2px solid green');

        }

        //data3.shift();

        if (valid) {

            var data1    = checker('section');

            var datastr1 = JSON.stringify(data1);

            var datastr2 = JSON.stringify(data2);

            var datastr4 = JSON.stringify(data3);


            if ($("#fileattr").length != 0) {

                var datastr3 = $("#fileattr")[0].files[0];

            }

            var formData = new FormData();

            formData.append('data1', datastr1);

            formData.append('data2', datastr2);

            formData.append('data3', datastr3);

            formData.append('data4', datastr4);

            $.ajax({

                type: "POST",

                data: formData,

                url: 'fumigationcontractinsert.php',

                cache: false,

                processData: false,

                contentType: false,

                success: function (res) {

                    if (res.status == 'success') {

                        swal({

                            type: 'success',

                            title: 'Fumigation Work Contract Updated',

                            showConfirmButton: false,

                            timer: 1000

                        });

                        setTimeout(function () {

                            location.href = '/process/crm/workcontract.php';

                        }, 1000);

                    }

                }

            })

        }

    }


    if ($('#editCheck').val() == 'true' && $('#contractid').val() != '') {

        $.ajax({

            type: "POST",

            url: 'fumigationselect1.php',

            data: "branchid=" + "<?php echo $_GET['branchid'];?>" + "&contractid=" + $('#contractid').val(),

            cache: false,

            success: function (res) {

                if (res.status == 'success') {

                    modals.putvalue('section', res.json);

                    $('#btn-submit').attr('onclick', 'update()');

                    $('#btn-submit').text('Update');

                    $('#contractdate').val(res.json[0].contractdate);

                    $('#contractdate').attr('data-stamp', res.json[0].contractdate1);

                    $('#billremiderdate').val(res.json[0].billdate);

                    $('#billremiderdate').attr('data-stamp', res.json[0].billdate1);

                    $('#servicedate').val(res.json[0].servicedate);

                    $('#servicedate').attr('data-stamp', res.json[0].servicedate1);

                    let billjson    = res.json[0].billjson;

                    console.log(billjson);

                    let servicejson = res.json[0].servicejson;

                    let count    = 1;

                    let serial   = 1;


                    for (let i in billjson) {
                        
                        fun_adder(count, billjson[i].billNo);

                        count++;
                    }

                    for (let a in servicejson) {
                        
                        fun_adder_service(serial, servicejson[a].service, servicejson[a].servicename);

                        serial++;
                    }

                }

            }

        });

    }




    function update() {

        var valid = true;

        if (checker('section') != false) {

            valid = valid * true;

        } else {

            valid = valid * false;

        }

        var data2 = [];

        $('#table-order > tbody > tr').each(function () {

            var service     = $(this).find('.service').text();

            var servicename = $(this).find('.servicename').text();


            if (service != '') {

                data2.push({

                    "service": service,

                    "servicename": servicename,

                });

            }

        })

        if (data2 == '') {

            $('#table-order').css('border', '2px solid red');

            valid = valid * false;


        } else {

            valid = valid * true;

            $('#table-order').css('border', '2px solid green');

        }

        //data2.shift();

        var data3 = [];

        $('#section11 > tbody > tr').each(function () {

            var billNo = $(this).find('.billNo').text();

            if (billNo != '') {

                data3.push({
                    "billNo": billNo,
                });

            }

        })

        if (data3 == '') {

            $('#section11').css('border', '2px solid red');

            valid = valid * false;


        } else {

            valid = valid * true;

            $('#section11').css('border', '2px solid green');

        }

        //data3.shift();

        if (valid) {

            var data1    = checker('section');

            var datastr1 = JSON.stringify(data1);

            var datastr2 = JSON.stringify(data2);

            var datastr4 = JSON.stringify(data3);


            if ($("#fileattr").length != 0) {

                var datastr3 = $("#fileattr")[0].files[0];

            }

            var formData = new FormData();

            formData.append('data1', datastr1);

            formData.append('data2', datastr2);

            formData.append('data3', datastr3);

            formData.append('data4', datastr4);

            $.ajax({

                type: "POST",

                data: formData,

                url: 'fumigationcontractupdate.php',

                cache: false,

                processData: false,

                contentType: false,

                success: function (res) {

                    if (res.status == 'success') {

                        swal({

                            type: 'success',

                            title: 'Fumigation Work Contract Updated',

                            showConfirmButton: false,

                            timer: 1000

                        });

                        setTimeout(function () {

                            location.href = '/process/crm/workcontract.php';

                        }, 1000);

                    }

                }

            })

        }

    }




    function calc() {

        var amount       = parseFloat($('#amount').val());
        
        var cgstper      = parseFloat($('#cgstper').val());
        
        var sgstper      = parseFloat($('#sgstper').val());
        
        var igstper      = parseFloat($('#igstper').val());
        
        var othercharges = parseFloat($('#othercharges').val());

        var cgstamt      = (amount * cgstper / 100).round(2);
        
        var sgstamt      = (amount * sgstper / 100).round(2);
        
        var igstamt      = (amount * igstper / 100).round(2);

        $('#cgstamt').val(cgstamt);

        $('#sgstamt').val(sgstamt);

        $('#igstamt').val(igstamt);

        var total = amount + cgstamt + sgstamt + igstamt + othercharges;

        $('#total').val(total.round(2));

    }

</script>