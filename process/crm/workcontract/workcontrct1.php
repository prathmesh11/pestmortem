<?php 
$base='../../';

$navenq2='background:#1B1464;';
include('header.php');
$enquiryidnew=mysqli_fetch_assoc(mysqli_query($con,"SELECT id x FROM crmmaster ORDER BY id DESC"))['x'];

  $year1=0;
  $year2=0;

if ( date('m') > 3 ) {
    $year1 = date('y');
    $year2 = date('y')+1;
}else{
    $year1 = date('y')-1;
    $year2 = date('y');
}

if (session_status() == PHP_SESSION_NONE) { session_start(); }
$sessionby = $_SESSION['employeeid'];
$branchid = $_SESSION['branchid'];

$branchshortname=mysqli_fetch_assoc(mysqli_query($con,"SELECT branchshortname x FROM branchmaster WHERE branchid='$branchid'"))['x'];

  if($enquiryidnew){
    $enquiryidnew++;
    $enquiryidnew=str_pad($enquiryidnew,6, '0', STR_PAD_LEFT);
    $enquiryidnew='PMI/'.$branchshortname.'/ENQ/'.$enquiryidnew.'/'.$year1.'-'.$year2;
  }else{
    $enquiryidnew='PMI/'.$year1.'-'.$year2.'/B'.$branchid.'/E'.$sessionby.'/1';
    $enquiryidnew='PMI/'.$branchshortname.'/ENQ/000001/'.$year1.'-'.$year2;
  }

?>
<br>
<style>
.input-container {
 
  max-width: 300px;
  background-color: #EDEDED;
  border: 1px solid #DFDFDF;
  border-radius: 5px;
}

input[type='file'] {
  display: none;
}

.file-info {
  font-size: 0.9em;
}

.browse-btn {
  background: #03A595;
  color: #fff;
  min-height: 35px;
  padding: 10px;
  border: none;
  border-top-left-radius: 5px;
  border-bottom-left-radius: 5px;
}

.browse-btn:hover {
  background: #4ec0b4;
}

@media (max-width: 300px) {
  button {
    width: 100%;
    border-top-right-radius: 5px;
    border-bottom-left-radius: 0;
  }
  
  .file-info {
    display: block;
    margin: 10px 5px;
  }
}
</style>
<img class="hidden" id=imageid src="../../../../img/PestMortemLogo.jpg">

<div class="table-ui container-fluid">
<div class="tr row">
<div class="col-sm-1 th">Enquiry No.</div>    
<div class="col-sm-3 th">Customer Info</div>
<div class="col-sm-1 th">Enquery For</div>
<div class="col-sm-2 th">Offer</div>
<div class="col-sm-2 th">Note</div>
<div class="col-sm-1 th">Work_Contract</div>
<div class="col-sm-2 th">Action</div>
</div>

<?php
$result=mysqli_query($con,"SELECT enquiryid ,category,customerid ,enquirynote ,followupnote ,followuptime FROM crmmaster WHERE created_time<>0 AND created_by<>0 AND enquiry_time<>0 AND enquiry_by<>0 AND wo_time=0 AND wo_by=0 AND close_enquiry_time=0 AND close_enquiry_by=0 AND branchid='$branchid' ORDER BY created_time DESC");

while($rows = mysqli_fetch_assoc($result)){

  $enquiryid=$rows['enquiryid'];
    $customerid=$rows['customerid'];

    $cifo=mysqli_fetch_assoc(mysqli_query($con,"SELECT * FROM customermaster WHERE customerid='$customerid'"));

?>

<div class="row tr">
<div class="col-sm-1 td" style="word-wrap:break-word;"><?php echo $rows['enquiryid']; ?></div>
<div class="col-sm-3 td">
<?php
echo 'Company : '.$cifo['customername']; 
echo '<br>Address : '.get_object_vars(json_decode($cifo['customerjson']))['address']; 
echo '<br>Name  : '.get_object_vars(json_decode($cifo['contactjson'])[0])['name'];
echo '<br>Dept. : '.get_object_vars(json_decode($cifo['contactjson'])[0])['department'];
echo '<br>Mobile : <a href="tel:'.get_object_vars(json_decode($cifo['contactjson'])[0])['mobile'].'">'.get_object_vars(json_decode($cifo['contactjson'])[0])['mobile'].'</a>';
echo '<br>Landline'.get_object_vars(json_decode($cifo['contactjson'])[0])['landline'];
echo '<br>Email : <a href="mailto:'.get_object_vars(json_decode($cifo['contactjson'])[0])['email'].'">'.get_object_vars(json_decode($cifo['contactjson'])[0])['email'].'</a>';
?>
</div>
<div class="col-sm-1 td"><?php echo $rows['category']; ?></div>

<div class="col-sm-2 td">
<!-- offer  -->
<div class="input-container">
  <input type="file" class="real-input" name="quatation" accept="application/pdf">
    <button class="browse-btn">
      Browse Files
    </button>
  <span class="file-info">Upload a file</span>
</div>
<button class="btn btn-primary btn-sm btn-block" data-enquiryid="<?php echo $rows['enquiryid']; ?>" onclick="upload(this)">Upload </button>

<?php 
    $enquir=str_replace('/', '-',$rows['enquiryid']);
    $path="api/offer/".$enquir.'.pdf';
  if(file_exists($path)){
    echo '<br><center><a href="'.$path.'" target="_blank">View PDF</a></center>';
  }else{
    echo '<br>Please Upload .pdf File';
  }
?>
<!-- offer  -->
</div>

<div class="col-sm-2 td" style="word-wrap:break-word;"><?php echo $rows['enquirynote']; echo $rows['followupnote']; ?></div>


<div class="col-sm-1 td" style="word-wrap:break-word;"><?php 
$contractid=mysqli_fetch_assoc(mysqli_query($con,"SELECT contractid x FROM workcontract WHERE enquiryid='$enquiryid'"))['x'];
echo $contractid;
if($contractid){
  echo '<button class="btn btn-sm btn-primary" data-contractid="'.$contractid.'" onclick="contractpdf(this)">Download</button>';
  if ($rows['category']=="Pest Control") {
    echo '<button class="btn btn-sm btn-primary" data-contractid="'.$contractid.'" onclick="servicepdf(this)">Service Pdf</button>';  }


}
?></div>

<div class="col-sm-2 td">
<?php if($contractid){ ?>
<button class="btn btn-success btn-sm btn-block" data-link1="wo_time" data-link2="wo_by" data-enquiryid="<?php echo $rows['enquiryid']; ?>" onclick="confirms(this)">Order Confirm</button>
<?php } ?>
<button class="btn btn-sm btn-block btn-danger" data-enquiryid="<?php echo $rows['enquiryid']; ?>" onclick="closes(this)">Order Close</button>
<?php 
if(!$contractid){
if($rows['category']!='Fumigation'){ ?>

<a class="btn btn-sm btn-block btn-primary" href="workcontract/pest-control.php?enq=<?php echo $rows['enquiryid']; ?>" >Pest Control Contract</a>
<?php }else{ ?>
  <a class="btn btn-sm btn-block btn-primary" href="workcontract/fumigation.php?enq=<?php echo $rows['enquiryid']; ?>" >Fumigation Contract</a>
<?php } } ?>

</div>
</div>
<?php
}
?>
</div>
</div>
</div>

<button class="btn" style="font-size:25px;border-radius:50px;width:60px;height:60px;position: fixed;
    bottom: 40px;
    right: 40px;background:#eb2f06;color:#fff;" data-toggle="modal" data-target="#myModal">+</button>

<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" align="center">New Enquiry</h4>
      </div>
      <div class="modal-body">
          <div class="form-group row">
       <div class="col-sm-4">Enquiry No.</div>
       <div class="col-sm-8"><input type="text" data-name="enquiryid" value="<?php echo $enquiryidnew; ?>" class="form-control input-sm" readonly></div>
       <div class="col-sm-4">Select Customer</div>
       <div class="col-sm-6">
       <select data-role="select" data-name='customerid'  id="customeridnew" class="form-control input-sm select-js">
                <option value="Select">Select</option>
                <?php 
                  $result11=mysqli_query($con,"SELECT customerid,customername  FROM customermaster ORDER by customerid ASC");
                  while($rows = mysqli_fetch_assoc($result11)){
                    echo '<option value="'.$rows['customerid'].'">'.$rows['customername'].'</option>';
                  }
                ?>
        </select>
       </div>
       <div class="col-sm-2">
        <a href="/process/create-master/customer-master/index.php" class="btn btn-block btn-success btn-sm">New</a>
        </div>
        
       <div class="col-sm-4" >Select Category</div>
       <div class="col-sm-8">
       <select data-role="select" data-name='category' class="form-control input-sm">
                <option value="Fumigation">Fumigation</option>
                <option value="Pest Control">Pest Control</option>
        </select>
       </div>

       <input type="hidden" data-name="workorder" value="workorder">
      
       <div class="col-sm-4">Note</div>
       <div class="col-sm-8"><input type="text" data-name="enquirynote" class="form-control input-sm"></div>
       <div class="col-sm-4"></div>
       <div class="col-sm-8"><button class="btn btn-success btn-block btn-sm" id="newqneuiry" onclick="newqneuiry()">Add New Enquery</button></div>
        </div>
        
      </div>
    </div>
  </div>
</div>

<?php 
include('footer.php');
?>
<script>
function getBase64Image(img) {
  var canvas = document.createElement("canvas");
  canvas.width = img.width;
  canvas.height = img.height;
  var ctx = canvas.getContext("2d");
  ctx.drawImage(img, 0, 0);
  var dataURL = canvas.toDataURL("image/png");
  return dataURL.replace('/^data:image\/(png|jpg);base64,/', "");
}

function contractpdf(e) {
  var contractid = $(e).data('contractid');
  $.ajax({
    type: "POST",
    data: "contractid=" + contractid,
    url: 'api/contractpdf.php',
    cache: false,
    success: function (res) {
      if (res.status == 'success') {
        var content=[];
        var json=res.json[0];
        var base64 = getBase64Image(document.getElementById("imageid"));

        content.push({text:'PEST MORTEM (INDIA) PRIVATE LIMITED',style: 'header'});
        content.push({text:'(Approved by National Plant Protection Organization, Goverment of India)',style: 'subheader'});
        content.push({text:'AN ISO 9001 : 2008 CERTIFIED COMPANY',style: 'subheader1'});
        content.push({text:json.address+'\n GST No : ' + json.branchgstnumber,alignment:'center',fontSize: 11});
        content.push({text:'EXPERT IN',alignment:'center',fontSize: 11,bold:true});
        content.push({text:'PEST CONTROL SERVICES AND FUMIGATION OF EXPORT CARGO, SHIPS, CONTAINERS',style: 'subheader'});

        
        // content.push( {table:	{widths: ['auto','*'],	body: [

        //     [{rowSpan: 6,  stack: [{image:base64,
        //               width: 75,
        //               height:40,
        //               margin:[0,20],
        //               },]}, {text: 'PEST MORTEM (INDIA) PRIVATE LIMITED',style: 'header'}],
        //     ['', {text:'(Approved by National Plant Protection Organization, Goverment of India)',style: 'subheader'}],
        //     ['', {text:'AN ISO 9001 : 2008 CERTIFIED COMPANY',style: 'subheader1'}],
        //     ['', {text:json.address,alignment:'center',fontSize: 11}],
        //     ['', {text:'EXPERT IN',alignment:'center',fontSize: 11,bold:true}],
        //     ['', {text:'PEST CONTROL SERVICES AND FUMIGATION OF EXPORT CARGO, SHIPS, CONTAINERS',style: 'subheader'}],

        //     ]
        //     }, layout: 'noBorders',  });
        
        if(json.ftype==''){ 
          content.push({text:'\n'});
          content.push({ columns: [{text:'Contract No. : '+json.contractid,fontSize: 9,bold:true},{text:'Dated : '+json.contractdate,alignment:'right',fontSize:9,bold:true} ]});       
          content.push( {table:	{widths: ['*'],body: [[{text:'Residential Pest Control Service Agreement',alignment:'center',fontSize: 10,bold:true}],]}  }); 
          content.push( {table:	{widths: ['*',10,'*'],body: [[
            {text:'Purchaser',alignment:'center',fontSize: 10,bold:true,border:[false,false,false,true]},{text:'',border:[false,false,false,false]},{text:'Premises',alignment:'center',fontSize: 10,bold:true,border:[false,false,false,true]}],
            [{text:'Name     : ' +json.companyname ,fontSize: 10,border:[false,false,false,true]},{text:'',border:[false,false,false,false]},{text:'Name     : ' +json.companyname ,fontSize: 10,border:[false,false,false,true]}],
            [{text:'Address : ' +json.companyaddress ,fontSize: 10,border:[false,false,false,true]},{text:'',border:[false,false,false,false]},{text:'Address : ' +json.servicelocation ,fontSize: 10,border:[false,false,false,true]}],
            [{text:'GST No  : ' +json.custgstnumber ,fontSize: 10,border:[false,false,false,true]},{text:'',border:[false,false,false,false]},{text:'GST No  : ' +json.custgstnumber ,fontSize: 10,border:[false,false,false,true]}],
            [{text:'Email      : ' +json.email ,fontSize: 10,border:[false,false,false,true]},{text:'',border:[false,false,false,false]},{text:'Email      : ' +json.email ,fontSize: 10,border:[false,false,false,true]}],
            [{text:'Phone     : ' +json.mobile ,fontSize: 10,border:[false,false,false,true]},{text:'',border:[false,false,false,false]},{text:'Phone     : ' +json.mobile ,fontSize: 10,border:[false,false,false,true]}],
          ]}  }); 

            //   content.push({table:{headerRows: 1,widths: ['*'],body: [[''],['']]}, layout: 'headerLineOnly' }); //LINE CODE
        // content.push({ columns: [{text:'Contract No. : '+json.contractid,fontSize: 9,bold:true},{text:'Dated : '+json.contractdate,alignment:'right',fontSize:9,bold:true} ]});
        // content.push({ text:'Period : '+json.contractstart+' TO '+ json.contractend,fontSize: 9,bold:true});
        // content.push({table:{headerRows: 1,widths: ['*'],body: [[''],['']]}, layout: 'headerLineOnly' }); //LINE CODE
        // content.push({ text:'In Consultation and as agreed upon a sum of Rs. '+json.amount+' + GST @ '+( parseFloat(json.cgstper)+parseFloat(json.sgstper)+parseFloat(json.igstper) ) +' % Rs. '+( parseFloat(json.cgstamt)+parseFloat(json.sgstamt)+parseFloat(json.igstamt))+' + Other Charges '+json.othercharges+' = '+json.totalcontractamt +' ( '+json.inword+' )',fontSize: 9});
        // content.push({ text:'to be paid by ' +json.companyname+' (herein after called the owners)',fontSize: 9});
        // content.push({ text:json.companyaddress,fontSize: 9});
        // content.push({ text:'TO M/S PEST MORTEM (INDIA) PVT. LTD. '+json.branchname + ' (herein after called the contractor).',fontSize: 9});
          content.push({text:'\nThis agreement is for an initial period of ('+json.contractstart+' TO '+json.contractend+') from the date of the frist service and, unless canceled by the purchaser, will automatically continue on a monthly basis until canceled by either party upon thirty days notice.\n\n',fontSize: 9,bold:true});
          content.push( {table:	{widths: ['*'],body: [[{text:'BASIC COVERAGE - TREATMENT OR INSPECTION FOR ',alignment:'center',fontSize: 9,bold:true}],]}  }); 

        }else{
          content.push({text:'WORK CONTRACT',alignment:'center',fontSize: 11,bold:true});
          content.push({table:{headerRows: 1,widths: ['*'],body: [[''],['']]}, layout: 'headerLineOnly' }); //LINE CODE
        content.push({ columns: [{text:'Contract No. : '+json.contractid,fontSize: 9,bold:true},{text:'Dated : '+json.contractdate,alignment:'right',fontSize:9,bold:true} ]});
        content.push({ text:'Period : '+json.contractstart+' TO '+ json.contractend,fontSize: 9,bold:true});
        content.push({table:{headerRows: 1,widths: ['*'],body: [[''],['']]}, layout: 'headerLineOnly' }); //LINE CODE
        content.push({ text:'In Consultation and as agreed upon a sum of Rs. '+json.amount+' + GST @ '+( parseFloat(json.cgstper)+parseFloat(json.sgstper)+parseFloat(json.igstper) ) +' % Rs. '+( parseFloat(json.cgstamt)+parseFloat(json.sgstamt)+parseFloat(json.igstamt))+' + Other Charges '+json.othercharges+' = '+json.totalcontractamt +' ( '+json.inword+' )',fontSize: 9});
        content.push({ text:'to be paid by ' +json.companyname+' (herein after called the owners)',fontSize: 9});
        content.push({ text:json.companyaddress,fontSize: 9});
        content.push({ text:'TO M/S PEST MORTEM (INDIA) PVT. LTD. '+json.branchname + ' (herein after called the contractor).',fontSize: 9});
          content.push({text:'\nThe contractor shall carry out the following Fumigation services in the premises of owners viz.',fontSize: 9,bold:true});
          content.push({table:{headerRows: 1,widths: ['*'],body: [[''],['']]}, layout: 'headerLineOnly' }); //LINE CODE
        }
       
        
        var table1 = {
          widths: ['*','auto','auto'],
          dontBreakRows: true,
          body: []
        };
        var table2 = {
          widths: ['*','auto','auto'],
          dontBreakRows: true,
          body: []
        };
        var table3 = {
          widths: ['*','auto','auto'],
          dontBreakRows: true,
          body: []
        };
        
        if(json.ftype==''){  
        var header=['Type of Services','Yes/No','Quantity'];
        var header1=['Type of Services','Yes/No','Quantity'];
        var header2=['Type of Equipments','Returnable','Quantity'];
        table1['body'].push(header);
        table2['body'].push(header1);
         table3['body'].push(header2);
        var services=json.servicejson;
        var serviceslen=parseInt(services.length/2);
        var counter=0;
        for(var i in services){
          var arr=[];
          arr.push(services[i].servicename,services[i].yesno,services[i].frequency);
          if(serviceslen<counter){
            table2['body'].push(arr);
          }else{
            table1['body'].push(arr);
          }
            counter++;
        }
         content.push({ columns: [
          {
            style: 'tablfont',
            table: table1,
            layout: 'noBorders'
          },{	width: 10,text:''},{
            style: 'tablfont',
            table: table2,
            layout: 'noBorders'
          }
        ]})

        content.push( {table:	{widths: ['*'],body: [[{text:'INSTALLATION OF EQUIPMENT',alignment:'center',fontSize: 9,bold:true}],]}  });
        
    
        
        var enquipmentjson=json.enquipmentjson;
          for(var i in enquipmentjson){
            var arr=[];
                arr.push(enquipmentjson[i].machinename,enquipmentjson[i].returnable,enquipmentjson[i].frq);
                table3['body'].push(arr);
            }
       // content.push({table:{headerRows: 1,widths: ['*'],body: [[''],['']]}, layout: 'headerLineOnly' }); //LINE CODE
    
        content.push({ columns: [
          {
            style: 'tablfont',
            width: 300,
            table: table3,
            layout: 'noBorders'
          },{	width: 10,text:''},{text:''}
        ]})

       // content.push( {table:	{widths: ['*'],body: [[{text:'METHOD OF PAYMENT',alignment:'center',fontSize: 9,bold:true}],]}  });
        //content.push({text:'Mode of payment : '+json.paymentby,fontSize: 9});

        }else{
           content.push({text:'Fumigation Type  : '+json.ftype+'\nFumigation Type Details : '+json.ftype2,fontSize: 9,bold:true});  
           content.push({text:'Name Of Fumigant : '+json.fname,fontSize: 9,bold:true});  
           content.push({table:{headerRows: 1,widths: ['*'],body: [[''],['']]}, layout: 'headerLineOnly' }); //LINE CODE
        }

        
        if(json.ftype==''){
        //   content.push({text:'Service Location',fontSize: 9,bold:true});
        // content.push({text:json.servicelocation,fontSize: 9});
        content.push( {table:	{widths: ['*',100,100],	body: [
          [{text: 'PAYMENT DETAILS', colSpan: 3, alignment: 'center',fontSize: 9,bold:true}, {}, {}],
					[{rowSpan: 6, text: 'Mode of payment : '+json.paymentby+'\nAdditional Declaration (if any)\n'+json.additinal,fontSize: 9,bold:true}, {text: 'SERVICE CHARGES',colSpan:2,border:[true,true,true,true],alignment:'center',fontSize: 9,bold:true}, {}],
					['', {text:'Amount',fontSize: 9,bold:true}, {text:  json.amount  ,fontSize: 9,bold:true}],
          ['', {text:'GST %',fontSize: 9,bold:true}, {text:  ( parseFloat(json.cgstper)+parseFloat(json.sgstper)+parseFloat(json.igstper) ) + ' % ',fontSize: 9,bold:true}],
          ['', {text:'GST Amount',fontSize: 9,bold:true}, {text:  ( parseFloat(json.cgstamt)+parseFloat(json.sgstamt)+parseFloat(json.igstamt))+'.00',fontSize: 9,bold:true}],
          ['', {text:'Other Charges',fontSize: 9,bold:true}, {text:  json.othercharges  ,fontSize: 9,bold:true}],
          ['', {text:'Net Amount',fontSize: 9,bold:true}, {text:  json.totalcontractamt  ,fontSize: 9,bold:true}],
          [{text: json.inword,colSpan:3,fontSize: 9,bold:true,alignment: 'center'}, {}, {}],



				]
				}  });

        content.push({table:{headerRows: 1,widths: ['*'],body: [[''],['']]}, layout: 'headerLineOnly' }); //LINE CODE
        // content.push({text:'Additional Declaration (if any)',fontSize: 9,bold:true});
        // content.push({text:json.additinal,fontSize: 9});
        }else{
          content.push({text:'Name Of Exporter : '+json.nameofexporter+' \nAddress Of Exporter : '+json.addressofexporter,fontSize: 9,bold:true});
        content.push({text:json.servicelocation,fontSize: 9});
        content.push({table:{headerRows: 1,widths: ['*'],body: [[''],['']]}, layout: 'headerLineOnly' }); //LINE CODE
        content.push({text:'Additional Declaration (if any)',fontSize: 9,bold:true});
        content.push({text:json.additinal,fontSize: 9});
        content.push({table:{headerRows: 1,widths: ['*'],body: [[''],['']]}, layout: 'headerLineOnly' }); //LINE CODE

        }

        

        if(json.ftype!=''){ 
          content.push({text:'Payment,',fontSize: 9,bold:true});
          content.push({text:'Term of payment : '+json.termofpayment,fontSize: 9});
          content.push({text:'No refund will be considered under any circumstances.',fontSize: 9});
          content.push({text:'No liability attaches to or assumed by the company, its Director or its representatives in respect of this contact.',fontSize: 9});
          content.push({text:'Please see the Terms and conditions overleaf.',fontSize: 9,bold:true});
          content.push({text:'Please Sign and return copy of the contract as token of acceptance.\n\n\n\n',fontSize: 9});
          content.push({ columns: [{text:'FOR PEST MORTEM (INDIA) PVT.LTD',fontSize: 9,bold:true},{text:'FOR OWNER  \nName : '+json.companyname+'\nMob No. '+json.mobile+' \nTel No.'+json.landline,alignment:'right',fontSize: 9,bold:true}]})
        }else{
           content.push({text:'Bank Name : ' + json.bankname + ',Branch Name : ' + json.bankbranchname + ',IFSC Code : ' + json.ifsccode + ',Account No : ' + json.accountno ,fontSize: 9,bold:true});
          content.push({ columns: [{text:'\n\nSignature\n\n\nFOR OWNER \n PEST MORTEM (INDIA) PVT.LTD',fontSize: 9,bold:true},{text:'\n\nSignature\n\n\nFOR PURCHASER  \n '+json.companyname,alignment:'right',fontSize: 9,bold:true}]})
          
        }
        
        dd = {
          pageSize: 'A4',
          pageOrientation: 'portrait',
          pageMargins: [40, 30, 30, 35],
          footer: function (currentPage, pageCount) {
            return {
              margin: 10,
              columns: [{
                fontSize: 9,
                text: [{
                    text: '--------------------------------------------------------------------------' +
                      '\n',
                    margin: [0, 20]
                  },
                  {
                    text: '© Pest Mortem (India) PVT. LTD. | PAGE ' + currentPage.toString() + ' of ' + pageCount,
                  }
                ],
                alignment: 'center'
              }]
            };

          },
          content,
          styles: {
            tablfont: {
              fontSize: 9
            },
            tablfont1: {
              fontSize: 9
            },
            header: {
                            fontSize: 16,
                            bold: true,
                            alignment: 'center',
                          },
                            subheader: {
                            fontSize: 8,
                            bold: true,
                            alignment: 'center',
                          }, 
                          subheader1: {
                            fontSize: 8,
                            bold: true,
                            alignment: 'center',
                          },
                           subheader2: {
                            fontSize: 10,
                            bold: true,
                            
                          },
          }
        }
        var win = window.open('', '_blank');
        pdfMake.createPdf(dd).open({}, win);

      }
    }
  })
}




function servicepdf(e) {
  var contractid = $(e).data('contractid');
  $.ajax({
    type: "POST",
    data: "contractid=" + contractid,
    url: 'api/servicepdf.php',
    cache: false,
    success: function (res) {
      if (res.status == 'success') {
        var content=[];
        var json=res.json[0];
        var base64 = getBase64Image(document.getElementById("imageid"));


         content.push( {table:	{widths: ['auto','*'],	body: [

            [{rowSpan: 3,  stack: [{image:base64,
                      width: 75,
                      height:40,
                      margin:[0,20],
                      },]}, {text: 'PEST MORTEM (INDIA) PRIVATE LIMITED',style: 'header'}],
            ['', {text:'(Approved by National Plant Protection Organization, Goverment of India)',style: 'subheader'}],
            ['', {text:'AN ISO 9001 : 2008 CERTIFIED COMPANY',style: 'subheader'}],
            ]
            }, layout: 'noBorders',  });
 
          content.push( {table:	{widths: [70,'auto','*'],body: [
            [{text:''  ,style: 'subheader1',bold:true,border:[false,false,false,false]},{text:'REGD. OFF    : '  ,style: 'subheader1',bold:true,border:[false,false,false,false]},{text:json.address.toUpperCase(),style: 'subheader1',bold:true,border:[false,false,false,false]}],
            [{text:''  ,style: 'subheader1',bold:true,border:[false,false,false,false]},{text:'Phone            : '  ,style: 'subheader1',bold:true,border:[false,false,false,false]},{text:'+91-22-2412 7935 / 2411  1976/2414 7425',style: 'subheader1',bold:true,border:[false,false,false,false]}],
            [{text:''  ,style: 'subheader1',bold:true,border:[false,false,false,false]},{text:'FAX                : '  ,style: 'subheader1',bold:true,border:[false,false,false,false]},{text:'+91-22-2415 0261',style: 'subheader1',bold:true,border:[false,false,false,false]}],
            [{text:''  ,style: 'subheader1',bold:true,border:[false,false,false,false]},{text:'E-MAIL          : '  ,style: 'subheader1',bold:true,border:[false,false,false,false]},{text:'pestmortem@pestmortem.com',style: 'subheader1',bold:true,border:[false,false,false,false]}],
            [{text:''  ,style: 'subheader1',bold:true,border:[false,false,false,false]},{text:'WEBSITE       : '  ,style: 'subheader1',bold:true,border:[false,false,false,false]},{text:'http://www.pestmortem.com',style: 'subheader1',bold:true,border:[false,false,false,false]}],        
          ]}  }); 

        content.push({text:'\n'});
          content.push( {table:	{widths: ['*'],body: [[{text:' PEST MANAGEMENT SERVICE RECORD',alignment:'center',fontSize: 10,bold:true}],]}  }); 
          content.push({text:'\n'});
          content.push( {table:	{widths: [100,'*'],body: [
            [{text:'Name of Client        : '  ,fontSize: 10,bold:true,border:[false,false,false,false]},{text:json.companyname.toUpperCase(),fontSize: 10,bold:true,border:[false,false,false,false]}],
            [{text:'Address of Client    : '  ,fontSize: 10,bold:true,border:[false,false,false,false]},{text:json.companyaddress.toUpperCase(),fontSize: 10,bold:true,border:[false,false,false,false]}],
            [{text:'Contract No             : '  ,fontSize: 10,bold:true,border:[false,false,false,false]},{text:json.contractid,fontSize: 10,bold:true,border:[false,false,false,false]}],
            [{text:'Contract Date          : '  ,fontSize: 10,bold:true,border:[false,false,false,false]},{text:json.contractdate,fontSize: 10,bold:true,border:[false,false,false,false]}],         
          ]}  }); 
        
        if(json.ftype==''){
          var servicelocation=json.servicelocation;
          
          if (servicelocation=='Same as Purchaser Address') {
           var servicelocation1=json.companyaddress;
          }else{
           var servicelocation1=json.servicelocation;
           
          }
          content.push( {table:	{widths: [100,'*'],body: [
            [{text:'Service Location     : '  ,fontSize: 10,bold:true,border:[false,false,false,false]},{text:servicelocation1.toUpperCase(),fontSize: 10,bold:true,border:[false,false,false,false]}],
          ]}  });  
        } else{
          content.push( {table:	{widths: [100,'*'],body: [
            [{text:'Service Location     : '  ,fontSize: 10,bold:true,border:[false,false,false,false]},{text:json.companyaddress.toUpperCase(),fontSize: 10,bold:true,border:[false,false,false,false]}],
          ]}  }); 
        }
       
        var table1 = {
          widths: ['auto','*','auto','*','auto'],
          dontBreakRows: true,
          body: []
        };
        var header=['Date','Service Performed','Infestation Status*','Name of Operator','Customer Signature'];
        table1['body'].push(header);
        var services=json.servicejson;

        for(var i in services){
          var arr=[];
          arr.push(services[i].servicedate,services[i].servicename,'','','');
            table1['body'].push(arr);
     
        }
        

        content.push({ columns: [
          {
            style: 'tablfont',
            table: table1,
            fontSize: 10,
            bold:true
            
            // layout: 'noBorders'
          }
        ]})

        

  
        
        dd = {
          pageSize: 'A5',
          pageOrientation: 'portrait',
          // pageOrientation: 'landscape',
          pageMargins: [40, 30, 30, 35],
          footer: function (currentPage, pageCount) {
            return {
              margin: 10,
              columns: [{
                fontSize: 9,
                text: [{
                    text: '--------------------------------------------------------------------------' +
                      '\n',
                    margin: [0, 20]
                  },
                  {
                    text: '© Pest Mortem (India) PVT. LTD. | PAGE ' + currentPage.toString() + ' of ' + pageCount,
                  }
                ],
                alignment: 'center'
              }]
            };

          },
          content,
          styles: {
            tablfont: {
              fontSize: 9
            },
            tablfont1: {
              fontSize: 9
            },
            header: {
                            fontSize: 14,
                            bold: true,
                            alignment: 'center',
                          },
                            subheader: {
                            fontSize: 8,
                            bold: true,
                            alignment: 'center',
                          }, 
                          subheader1: {
                            fontSize: 8,
                            bold: true,
                            alignment: 'left',
                          },
                           subheader2: {
                            fontSize: 10,
                            bold: true,
                            
                          },
          }
        }
        var win = window.open('', '_blank');
        pdfMake.createPdf(dd).open({}, win);

      }
    }
  })
}

$('.browse-btn').on('click',function(){
  $(this).parent().find('input').click();
});

$('.real-input').on('change',function(){
  $(this).parent().find('span').text($(this).val().replace(/C:\\fakepath\\/i, ''));
});

function newqneuiry() {
  $('#newqneuiry').hide();
  var valid = true;
  if (checker('myModal') != false) {
    valid = valid * true;
  } else {
    valid = valid * false;
  }
  if (valid) {
    var data=checker('myModal');
    var datastr = JSON.stringify(data);
    $.ajax({
        type: "POST",
        data: {
          data: datastr
        },
        url: 'api/newqneuiry.php',
        cache: false,
        success: function (res) {
          if (res.status == 'success') {
         
              window.location.reload();
           
          }
        }
      })
    }
  }
  
function workcontract(){

}

function setidtomidal(e){
var enquiryid=$(e).data('enquiryid');
$('#inp-enquiryidset').val(enquiryid);

}



function upload(e){
var file=$(e).parent().parent().find('input')[0].files[0];
var enquiryid=$(e).data('enquiryid');
var formData = new FormData();

formData.append('offer',file);
formData.append('enquiryid',enquiryid);
$.ajax({
        type: "POST",
        data: formData,
        url: 'api/offer.php',
        cache: false,
        processData: false,
        contentType: false,
        success: function (res) {
             window.location.reload();
        }
    })
}


</script>