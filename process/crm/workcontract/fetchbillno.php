<?php
    
    $base = '../../../';
    
    include($base.'_in/connect.php');
    
    header('content-type: application/json; charset=utf-8');
    
    header("access-control-allow-origin: *");
    
    if(isset($_POST['customerid']) && isset($_POST['category'])) {

        $con        = _connect();

        $customerid = _clean($con,$_POST["customerid"]);

        $category   = _clean($con,$_POST["category"]);

        $edit       = _clean($con,$_POST["edit"]);

        $contractid = _clean($con,$_POST["contractid"]);

        if (session_status()==PHP_SESSION_NONE) { session_start(); }

        $branchid = $_SESSION['branchid'];

        $results  = mysqli_query($con,"SELECT invoice_bill FROM billing WHERE customerid='$customerid' AND branchid='$branchid' AND category = '$category' AND workcontractstatus = '$contractid'  ORDER BY id DESC ");
      
        while ($row = mysqli_fetch_assoc($results)) {

            $jsons.=',{"billno":"'.$row['invoice_bill'].'"}';
          
        }

        $jsons = substr($jsons,1);
        
        $jsons = '['.$jsons.']';
        
        $result   = mysqli_query($con,"SELECT invoice_bill FROM billing WHERE customerid='$customerid' AND branchid='$branchid' AND category = '$category' AND workcontractstatus = ''  ORDER BY id DESC ");
      
        while ($row = mysqli_fetch_assoc($result)) {

            $json.=',{"billno":"'.$row['invoice_bill'].'"}';
          
        }

        $json = substr($json,1);
        
        $json = '['.$json.']';

        $finaljson = array_merge(json_decode($jsons),json_decode($json));

        if($result){

            if ($edit) {

                echo '{"status":"success","json":'.json_encode($finaljson).'}';

            } else {
            
                echo '{"status":"success","json":'.$json.'}';
            
            }
      
        } else {
            
            echo '{"status":"falid"}';
            
        }
       
        _close($con);

    } else {

        echo '{"status":"falid2"}';

    }

?>