<?php

  $base='../../../';

  include($base.'_in/connect.php');

  header('content-type: application/json; charset=utf-8');

  header("access-control-allow-origin: *");

  if(isset($_POST['branchid']) && isset($_POST['contractid'])){

    $con        = _connect();
    
    $branchid   = _clean($con,$_POST['branchid']);
    
    $contractid = _clean($con,$_POST['contractid']);
    
    $select     = mysqli_fetch_assoc(mysqli_query($con,"SELECT * FROM workcontract WHERE branchid='$branchid' AND contractid='$contractid'"));

    if($select){
          
      $volume            = $select['volume'];    
      
      $quantity          = $select['quantity'];
      
      $contractdate1     = $select['contractdate'];
      
      $contractdate      = $select['contractdate'];
      
      $contractstart     = $select['contractstart'];
      
      $contractstart1    = $select['contractstart'];
      
      $contractend       = $select['contractend'];
      
      $contractend1      = $select['contractend'];
      
      $amount            = $select['amount'];
      
      $cgstper           = $select['cgstper'];
      
      $sgstper           = $select['sgstper'];
      
      $igstper           = $select['igstper'];
      
      $cgstamt           = $select['cgstamt'];
      
      $sgstamt           = $select['sgstamt'];
      
      $igstamt           = $select['igstamt'];
      
      $othercharges      = $select['othercharges'];
      
      $totalcontractamt  = $select['totalcontractamt'];
      
      $billqty           = $select['billqty'];
      
      $servicejson1      = $select['servicejson'];
      
      $billjson          = $select['billjson'];
      
      $jsonbilldate      = json_decode($billjson, true);
      
      $billdate1         = $jsonbilldate[0]['billdate'];
      
      $billdate          = $jsonbilldate[0]['billdate'];
      
      $itemjson          = $select['itemjson'];
      
      $ftype             = $select['ftype'];
      
      $ftype2            = $select['ftype2'];
      
      $fname             = $select['fname'];
      
      $termofpayment     = $select['termofpayment'];
      
      $nameofexporter    = $select['nameofexporter'];
      
      $addressofexporter = $select['addressofexporter'];
      
      $billingparty      = $select['billingparty'];
      
      $servicelocation   = $select['servicelocation'];
      
      $additinal         = $select['additinal'];
      
      $paymentby         = $select['paymentby'];
      
      $enquipmentjson    = $select['enquipmentjson'];

      $servicejson = '';

      if($contractdate!=0 && $contractdate!=''){ $contractdate= date('d F, Y',  $contractdate/1000); }else{  $contractdate=''; }

      if($contractstart1!=0 && $contractstart1!=''){ $contractstart1= date('d F, Y',  $contractstart1/1000); }else{  $contractstart1=''; }

      if($contractend1!=0 && $contractend1!=''){ $contractend1= date('d F, Y',  $contractend1/1000); }else{  $contractend1=''; }

      if($billdate!=0 && $billdate!=''){ $billdate= date('d F, Y',  $billdate/1000); }else{  $billdate=''; }

      $result      = mysqli_query($con,"SELECT servicename,serviceid FROM servicemaster ORDER BY id ASC" );

      while($rows  = mysqli_fetch_assoc($result)){

        $serviceid   = $rows['serviceid'];

        $servicename = $rows['servicename'];

        $counter = (mysqli_fetch_assoc(mysqli_query($con,"SELECT count(id) x FROM `workcontractservice` WHERE contractid='$contractid' AND serviceid='$serviceid'"))['x']);
            
        if($counter!=0){
        
          $yesno = 'YES';
        
          $frq   = $counter;
        
          $servicejson.=',{"serviceid":"'.$serviceid.'","servicename":"'.$servicename.'","frequency":"'.$frq.'","yesno":"'.$yesno.'"}';

        }
            
      }

      $servicejson = substr($servicejson,1);
      
      $servicejson = '['.$servicejson.']';

      $trendanalysis = mysqli_fetch_assoc(mysqli_query($con,"SELECT trendanalysis x FROM crmmaster WHERE enquiryid='$enquiryid' AND contractid='$contractid'"))['x'];

      $json = '{"billlinkjson":'.$select['billlinkjson'].',"servicejson1":'.$servicejson1.',"customerid":"'.$select['customerid'].'","servicejson":'.$servicejson.',"trendanalysis":"'.$trendanalysis.'","enquipmentjson":'.$enquipmentjson.',"paymentaccptable":"'.$paymentby.'","additinalcharges":"'.$additinal.'","servicelocation":'.$servicelocation.',"contractend1":"'.$contractend1.'","contractend":"'.$contractend.'","contractstart1":"'.$contractstart1.'","contractstart":"'.$contractstart.'","billjson":'.$billjson.',"enquiryid":"'.$enquiryid.'","contractdate1":"'.$contractdate1.'","contractid":"'.$contractid.'","billdate1":"'.$billdate1.'","billdate":"'.$billdate.'","servicedate":"'.$servicedate.'","servicedate1":"'.$servicedate1.'","volume":"'.$volume.'","quantity":"'.$quantity.'","contractdate":"'.$contractdate.'","amount":"'.$amount.'","cgstper":"'.$cgstper.'","sgstper":"'.$sgstper.'","igstper":"'.$igstper.'","cgstamt":"'.$cgstamt.'","sgstamt":"'.$sgstamt.'","igstamt":"'.$igstamt.'","othercharges":"'.$othercharges.'","totalcontractamt":"'.$totalcontractamt .'","billqty":"'.$billqty.'","ftype":"'.$ftype.'","ftype2":"'.$ftype2.'","fname":"'.$fname.'","termofpayment":"'.$termofpayment.'","nameofexporter":"'.$nameofexporter.'","addressofexporter":"'.$addressofexporter.'","billingparty":"'.$billingparty.'"}'; 
          
      echo '{"status":"success","json":['.$json.']}';   

    } else {

      echo '{"status":"falid1"}';

    }

      _close($con);

  } else {

    echo '{"status":"falid2"}';

  }

?>