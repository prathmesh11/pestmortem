<?php

    $base = '../../../';

    include($base.'_in/connect.php');

    header('content-type: application/json; charset=utf-8');

    header("access-control-allow-origin: *");
    
    if(isset($_POST['data1']) && isset($_POST['data2']) && isset($_POST['data3']) && isset($_POST['data4']) && isset($_POST['data5']) && isset($_POST['data6']) && isset($_POST['data7'])){

      $con = _connect();
      
      if (session_status()==PHP_SESSION_NONE) { session_start(); }

      $created_by      = $_SESSION['employeeid'];
      
      $branchid        = $_SESSION['branchid'];
      
      $contractid      = _clean($con,get_object_vars(json_decode($_POST['data1']))['contractid']);
      
      $enquiryid       = '';
      
      $customerid      = _clean($con,get_object_vars(json_decode($_POST['data1']))['customerid']);
      
      $contractdate    = _clean($con,get_object_vars(json_decode($_POST['data1']))['contractdate']);
      
      $contractstart   = _clean($con,get_object_vars(json_decode($_POST['data1']))['contractstart']);
      
      $contractend     = _clean($con,get_object_vars(json_decode($_POST['data1']))['contractend']);
      
      $amount          = _clean($con,get_object_vars(json_decode($_POST['data1']))['amount']);
      
      $cgstper         = _clean($con,get_object_vars(json_decode($_POST['data1']))['cgstper']);
      
      $sgstper         = _clean($con,get_object_vars(json_decode($_POST['data1']))['sgstper']);
      
      $igstper         = _clean($con,get_object_vars(json_decode($_POST['data1']))['igstper']);
      
      $cgstamt         = _clean($con,get_object_vars(json_decode($_POST['data1']))['cgstamt']);
      
      $sgstamt         = _clean($con,get_object_vars(json_decode($_POST['data1']))['sgstamt']);
      
      $igstamt         = _clean($con,get_object_vars(json_decode($_POST['data1']))['igstamt']);
      
      $othercharges    = _clean($con,get_object_vars(json_decode($_POST['data1']))['othercharges']);
      
      $contractamt     = _clean($con,get_object_vars(json_decode($_POST['data1']))['total']);
      
      $servicelocation = $_POST['data6'];
      
      $additinal       = _clean($con,get_object_vars(json_decode($_POST['data1']))['additinalcharges']);
      
      $paymentby       = _clean($con,get_object_vars(json_decode($_POST['data1']))['paymentaccptable']);
      
      $billqty         = _clean($con,get_object_vars(json_decode($_POST['data1']))['billqty']);
      
      $trendanalysis   = _clean($con,get_object_vars(json_decode($_POST['data1']))['trendanalysis']);

      $data2           = json_decode($_POST['data2']);
      
      $jsondata2       = '';

      foreach($data2 as $i){

        $serviceid = get_object_vars($i)['serviceids'];  

        $servicename = get_object_vars($i)['servicename'];  
        
        $arr         = get_object_vars($i)['arr'];  

        mysqli_query($con,"INSERT INTO wrokcontractfumigationandpestmortemservice (branchid ,customerid, contractid , serviceid , servicename) VALUES ('$branchid', '$customerid', '$contractid' ,'$serviceid', '$servicename')");
        
        foreach($arr as $k){

          $servicedate  = (strtotime($k)*1000);
          
          $jsondata2.=',{"serviceid":"'.$serviceid.'","servicename":"'.$servicename.'","servicedate":"'.$servicedate.'"}';

          mysqli_query($con,"INSERT INTO workcontractservice (branchid,enquiryid,contractid,serviceid,servicedate,servicename,created_by,created_time) VALUES ('$branchid','$enquiryid','$contractid','$serviceid','$servicedate','$servicename','$created_by','$CURRENT_MILLIS')");

        }

      }

      $data3     = $_POST['data3'];
     
      $jsondata4 = '';
     
      $data4     = json_decode($_POST['data4']);
  
      foreach($data4 as $i){

        $billdate = get_object_vars($i)['bill-1'];
     
        $billid   = $rand = substr(md5(microtime()),rand(0,26),5);
     
        $billid   = $serviceid=$contractid.'/'.$billid;
     
        $billdate = (strtotime($billdate)*1000);
     
        $jsondata4.=',{"billid":"'.$billid.'","billdate":"'.$billdate.'"}';

        mysqli_query($con,"INSERT INTO workcontractbill (branchid ,customerid,enquiryid ,contractid ,billid ,billdate ,cgstper ,sgstper ,igstper,cgstamt ,sgstamt ,igstamt ,other1, contractamt, created_by, created_time) VALUES ('$branchid' ,'$customerid','$enquiryid' ,'$contractid' ,'$billid' ,'$billdate' ,'$cgstper' ,'$sgstper' ,'$igstper' ,'$cgstamt', '$sgstamt', '$igstamt', '$othercharges','$contractamt' ,'$created_by' ,'$CURRENT_MILLIS')");

      }

      $jsondata2 = substr($jsondata2,1);
      
      $jsondata2 = '['.$jsondata2.']';
      
      $jsondata4 = substr($jsondata4,1);
      
      $jsondata4 = '['.$jsondata4.']';
      
      $data5     = $_POST['data5'];

      $data7     = $_POST['data7'];

      $serviceIdJson = json_decode($data7);

      foreach ($serviceIdJson as $i) {

        $billNo = get_object_vars($i)['billNo'];
        
        mysqli_query($con,"UPDATE billing SET workcontractstatus = '$contractid' WHERE branchid='$branchid' AND invoice_bill = '$billNo' AND customerid = '$customerid'");
        
        mysqli_query($con,"INSERT INTO workcontract_billno (branchid ,customerid, contractid , billno) VALUES ('$branchid', '$customerid', '$contractid' ,'$billNo')");

      }

      $create=mysqli_query($con,"INSERT INTO workcontract (branchid ,customerid,enquiryid ,contractid ,contractdate ,contractstart ,contractend ,amount ,cgstper ,sgstper ,igstper ,cgstamt ,sgstamt ,igstamt ,othercharges ,totalcontractamt ,servicelocation ,additinal ,paymentby ,billqty ,servicejson ,billjson ,billlinkjson ,enquipmentjson ,itemjson ,created_by ,created_time) VALUES ('$branchid','$customerid', '$enquiryid', '$contractid', '$contractdate', '$contractstart', '$contractend', '$amount', '$cgstper', '$sgstper', '$igstper', '$cgstamt', '$sgstamt', '$igstamt', '$othercharges', '$contractamt', '$servicelocation', '$additinal', '$paymentby', '$billqty', '$jsondata2', '$jsondata4', '$data7', '$data5', '$data3', '$created_by', '$CURRENT_MILLIS')");

      if($create){

       // mysqli_query($con,"INSERT INTO crmmaster (branchid,enquiryid,trendanalysis,customerid,category,created_by,created_time,enquiry_by,enquiry_time,contractid) VALUES ('$branchid','$enquiryid','$trendanalysis','$customerid','Pest Control','$created_by', '$CURRENT_MILLIS','$created_by', '$CURRENT_MILLIS','$contractid') ");

        mysqli_query($con,"UPDATE crmmaster SET checkcontract = 1 ,customerid = '$customerid'  WHERE contractid = '$contractid' AND branchid = '$branchid'");

        echo '{"status":"success"}';

      } else {

        echo '{"status":"falid1"}';

      }
                
      _close($con);

    } else {
       
      echo '{"status":"falid"}';

    }
?>