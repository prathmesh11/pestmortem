<?php 
$base='../../../';

$navenq2='background:#1B1464;';
include('../header.php');

$year1=0;
$year2=0;

if ( date('m') > 3 ) {
  $year1 = date('y');
  $year2 = date('y')+1;
}else{
  $year1 = date('y')-1;
  $year2 = date('y');
}

if (session_status() == PHP_SESSION_NONE) { session_start(); }
$sessionby = $_SESSION['employeeid'];
$branchid = $_SESSION['branchid'];
$branchshortname=mysqli_fetch_assoc(mysqli_query($con,"SELECT branchshortname x FROM branchmaster WHERE branchid='$branchid'"))['x'];


$contractid = mysqli_fetch_assoc(mysqli_query($con,"SELECT count(contractid) x FROM  crmmaster WHERE category='sales' AND contractid<>'' Order By id desc "))['x'];

if(!$contractid){
  $no='PMI/'.$branchshortname.'/ENQ/1/'.$year1.'-'.$year2;
}else{
    $contractid++;
    $no='PMI/'.$branchshortname.'/ENQ/'.$contractid.'/'.$year1.'-'.$year2;
}

   

$enqno='0';
$contractid='0';

if(isset($_GET['enq'])) {
    $enqno=$_GET['enq']; 
}
// if (isset($_GET['enquiryid']) && isset($_GET['contractid'])){
//     $enqno=$_GET['enquiryid']; 
//     $contractid=$_GET['contractid']; 
// }


if (session_status() == PHP_SESSION_NONE) { session_start(); }
$sessionby = $_SESSION['employeeid'];
$branchid = $_SESSION['branchid'];

$branchjson=json_decode(mysqli_fetch_assoc(mysqli_query($con,"SELECT branchjson x FROM branchmaster WHERE branchid='$branchid'"))['x']);
$customerid=mysqli_fetch_assoc(mysqli_query($con,"SELECT customerid x FROM crmmaster WHERE created_time<>0 AND created_by<>0 AND enquiryid='$enqno'"))['x'];
$customerjson=json_decode(mysqli_fetch_assoc(mysqli_query($con,"SELECT customerjson x FROM customermaster WHERE customerid='$customerid'"))['x']);
$state=get_object_vars($customerjson)['state'];
$customername=get_object_vars($customerjson)['customername'];
$address=get_object_vars($customerjson)['address'];
$contactjson=json_decode(mysqli_fetch_assoc(mysqli_query($con,"SELECT contactjson x FROM customermaster WHERE customerid='$customerid'"))['x'],true);
$mobile=$contactjson[0]['mobile'];
$email=$contactjson[0]['email'];


?>
<style>
.table-list td,
   .table-list th {
       border: 1px solid #ddd;
       padding: 1px !important;
       font-size: 13px;
   }
   .table-list td{
	padding-top: 10px !important;
   }

   .table-list tr:nth-child(even) {
       background-color: #f2f2f2;
   }

   .table-list th {
       padding-top: 5px;
       padding-bottom: 5px;
       text-align: center;
       background-color: #16a085;
       color: white;
   }
</style>
<br>

<input type="hidden" id="enquiryid" value="<?php echo $_GET['enquiryid']; ?>" data-role="text" class="form-control input-sm"  />
<input type="hidden" id="contractid" value="<?php echo $_GET['contractid']; ?>" data-role="text" class="form-control input-sm"  />
<input type="hidden" id="statecode" value="<?php echo $state; ?>" data-role="text" class="form-control input-sm"  />

<div class="container-fluid">
    <div class="row" id="section">
        <div class="col-sm-1">Bill No.</div>
        <div class="col-sm-2">
            <div class="form-group"><input type="text" value="<?php echo str_replace('ENQ', 'SALE',$no); ?>"
                    data-role="text" class="form-control input-sm" data-name="contractid" readonly /></div>
        </div>
       
        <div class="col-sm-6"></div>
        <div class="col-sm-1">Enquiry No.</div>
        <div class="col-sm-2">
            <div class="form-group"><input type="text" value="<?php echo $enqno; ?>" data-role="text"
                    class="form-control input-sm" data-name="enquiryid" readonly /></div>
        </div>
        <div class="col-sm-4"></div>
        <div class="col-sm-1">Date</div>
        <div class="col-sm-2">
            <div class="form-group"><input type="date" value="<?php echo $today; ?>" data-role="date"
                    class="form-control input-sm" data-name="billdate"  id="billdate"/></div>
        </div>
    </div>
    <div class="col-sm-6" id="section">
        <div class="col-sm-4">Customer Name</div>
        <div class="col-sm-8">
            <div class="form-group"><input type="text" data-role='text' data-name='companyname'
                    value="<?php echo $customername;?>" class="form-control input-sm" ></div>
                    <input type="hidden" data-role='text' data-name='customerid'
                    value="<?php echo $customerid;?>" class="form-control input-sm" >
                    
        </div>
        <div class="col-sm-4">Company Address</div>
        <div class="col-sm-8">
            <div class="form-group"><input type="text" data-role='text'  value="<?php echo $address;?>"
                    data-name='caddress' class="form-control input-sm"></div>
        </div>
    </div>
    <div class="col-sm-6" id="section">
        <div class="col-sm-3">Mobile No</div>
        <div class="col-sm-8">
            <div class="form-group"><input type="text" data-role='text' value="<?php echo $mobile;?>" id="billingname"
                    data-name='mnumber' class="form-control input-sm"  value=""></div>
        </div>
        <div class="col-sm-3">Email</div>
        <div class="col-sm-8">
            <div class="form-group"><input type="text" data-role='text' id="billingaddress" data-name='email'
                    value="<?php echo $email;?>" class="form-control input-sm" ></div>
        </div>
    </div><br><br>


    <table class="table-list table" id="table-customer">
        <thead>
            <tr>
                <!-- <th rowspan="2">SR NO</th> -->
                <th rowspan="2" width="200">Item Name</th>
                <th rowspan="2">Qty</th>
                <th rowspan="2">Rate</th>
                <th rowspan="2">Amount</th>
                <th rowspan="2">GST</th>
                <th colspan="2">Cgst</th>
                <th colspan="2">Sgst</th>
                <th colspan="2">Igst</th>
                <th rowspan="2">Net Amount</th>
                <th rowspan="2" width="80">Action</th>
            </tr>
            <tr>
                <th>RATE %</th>
                <th>AMOUNT</th>
                <th>RATE %</th>
                <th>AMOUNT</th>
                <th>RATE %</th>
                <th>AMOUNT</th>
            </tr>
        </thead>
        <tbody>
            <tr id="tr-table1">
                <!-- <td></td> -->
                <td>
                    <select class="form-control input-sm select-js1" id="itemname" data-name="itemcode" > 
                        <option value="Select">Select itemname</option>
                        <?php 
                        // $result=mysqli_query($con,"SELECT machineid,machinename FROM machinemaster");
                        // while($row=mysqli_fetch_assoc($result)){
                        // echo "<option value=".$row['machineid'].">".$row['machinename']."</option>";
                        // }
                        $result=mysqli_query($con,"SELECT  stockid FROM `stockmaster` ORDER BY id DESC");
                    while($rows=mysqli_fetch_assoc($result)) {
                            $itemcode=$rows['stockid'];
                            $itemname=mysqli_fetch_assoc(mysqli_query($con,"SELECT itemname x FROM stockmaster WHERE stockid='$itemcode'"))['x'];
                            echo '<option value="'.$itemcode.'">'.$itemname.'</option>';
                    }
                        ?>
                    </select>
                </td>

                <td><input type="text" data-role="number" id="inp-qty" data-name="qty" onkeyup="calc()"
                        class="form-control input-sm"> </td>
                <td><input type="text" data-role="number" id="inp-rate" data-name="rate" onkeyup="calc()"
                        class="form-control input-sm"> </td>
                <td><input type="text" data-role="number" id="inp-amount" data-name="amount"
                        class="form-control input-sm" readonly></td>
                <td width="100">
                    <select data-role="select" data-name='gst' id="inp-gst" onchange=" calc()"
                        class="form-control input-sm">
                        <option value="0">0</option>
                        <option value="0.25">0.25</option>
                        <option value="3">3</option>
                        <option value="5">5</option>
                        <option value="12">12</option>
                        <option value="18">18</option>
                        <option value="28">28</option>
                    </select>
                </td>
                <td><input type="text" data-role="number" id="inp-cgstper" data-name="cgstper"
                        class="form-control input-sm" ></td>
                <td><input type="text" data-role="number" id="inp-cgstamt" data-name="cgstamt"
                        class="form-control input-sm" ></td>
                <td><input type="text" data-role="number" id="inp-sgstper" data-name="sgstper"
                        class="form-control input-sm" ></td>
                <td><input type="text" data-role="number" id="inp-sgstamt" data-name="sgstamt"
                        class="form-control input-sm" ></td>
                <td><input type="text" data-role="number" id="inp-igstper" data-name="igstper"
                        class="form-control input-sm" ></td>
                <td><input type="text" data-role="number" id="inp-igstamt" data-name="igstamt"
                        class="form-control input-sm" ></td>
                <td><input type="text" data-role="number" id="inp-total" data-name="total" class="form-control input-sm"
                        readonly></td>
                <td><button class="btn btn-sm btn-primary" onclick="addorder()">Add+</button></td>
            </tr>
        </tbody>
    </table>

<script>


</script>
 
<hr style="border:1px solid #000;">
   <div class="col-sm-6"></div>
   <div class="col-sm-6" id="section">
            <div class="col-sm-4">Amount</div>
            <div class="col-sm-8">
                <div class="form-group"><input type="text" data-role='text' id="inp-taxable" data-name='taxablevalue' class="form-control input-sm"
                        readonly></div>
            </div>
            <div class="col-sm-4">CGST Amt of Items</div>
            <div class="col-sm-8">
                <div class="form-group"><input type="text" data-role='text' id="inp-cgsttotal" data-name='cgsttotal'
                        class="form-control input-sm" readonly></div>
            </div>
            <div class="col-sm-4">SGST amt of Items</div>
            <div class="col-sm-8">
                <div class="form-group"><input type="text" data-role='text' id="inp-sgsttotal" data-name='sgsttotal'
                        class="form-control input-sm" readonly></div>
            </div>
            <div class="col-sm-4">IGST amt of Items</div>
            <div class="col-sm-8">
                <div class="form-group"><input type="text" data-role='text' id="inp-igsttotal" data-name='igsttotal'
                        class="form-control input-sm" readonly></div>
            </div>
            <div class="col-sm-4">Other Charges</div>
            <div class="col-sm-8">
                <div class="form-group"><input type="text" data-role='text' value="0" id="inp-othercharges" onkeyup="netvalue()"
                        data-name='othercharges' class="form-control input-sm"></div>
            </div>
            <div class="col-sm-4">Total </div>
            <div class="col-sm-8">
                <div class="form-group"><input type="text" data-role='text' id="inp-invoicevalue" data-name='invoicevalue'
                        class="form-control input-sm" readonly></div>
            </div>
        </div>
<div style="clear:both;"></div>


</div>
<div class="col-sm-12"><hr></div>
<div class="col-sm-3"></div>
<div class="col-sm-3"><button class="btn btn-sm btn-block btn-success" id="btn-submit" onclick="submit()">Submit</button></div>
<div class="col-sm-3"><button class="btn btn-sm btn-block btn-danger">Reset</button></div>
<div class="col-sm-12"><hr></div>
</div>

</div>
</div>
<?php
include('../footer.php');
?>
<script>

if ($('#enquiryid').val() != '' && $('#contractid').val() != '') {
    $.ajax({
        type: "POST",
        data: "enquiryid=" + $('#enquiryid').val() + "&contractid=" + $('#contractid').val(),
        url: 'salesselect.php',
        cache: false,
        success: function (res) {
            if (res.status == 'success') {
                modals.putvalue('section', res.json);
                modals.putvalue('abc', res.json[0].tablejson);
                var tablejson = res.json[0].tablejson;
                $('#btn-submit').text('Update');
                $('#btn-submit').attr('onclick', 'update()');
                $('#billdate').val(res.json[0].date);

               console.log(res.json[0].date);
               

                for (var i in tablejson) {

                    // var len = ($('#table-customer > tbody > tr').length);
                    var str = '<tr class="abc">';
                    // str += '<td align="center">' + len + '</td>';
                    str += '<td class="itemcode" style="display:none;">' + tablejson[i].itemcode + '</td>';
                    str += '<td class="itemname">' + tablejson[i].itemname + '</td>';
                    str += '<td class="qty">' + tablejson[i].qty + '</td>';
                    str += '<td class="rate">' + tablejson[i].rate + '</td>';
                    str += '<td class="amount">' + tablejson[i].amount + '</td>';
                    str += '<td class="gst">' + tablejson[i].gst + '</td>';
                    str += '<td class="cgstper">' + tablejson[i].cgstper + '</td>';
                    str += '<td class="cgstamt">' + tablejson[i].cgstamt + '</td>';
                    str += '<td class="sgstper">' + tablejson[i].sgstper + '</td>';
                    str += '<td class="sgstamt">' + tablejson[i].sgstamt + '</td>';
                    str += '<td class="igstper">' + tablejson[i].igstper + '</td>';
                    str += '<td class="igstamt">' + tablejson[i].igstamt + '</td>';
                    str += '<td class="total">' + tablejson[i].total + '</td>';
                    str += '<td align="center"><button class="btn btn-sm btn-danger" onclick="remover(this);"> R </button><button class="btn btn-sm btn-primary" style="margin-left:4px;" onclick="editmode(this);"> E </button></td>';
                    str += '</tr>';
                    $('#table-customer > tbody').append(str);
                    netvalue();
                    calc();
                }
            }
        }
    });
}

function editmode(e) {

    $('#itemname').val($(e).parent().parent().find('.itemcode').text().trim());
    $('#inp-qty').val($(e).parent().parent().find('.qty').text().trim());
    $('#inp-rate').val($(e).parent().parent().find('.rate').text().trim());
    $('#inp-amount').val($(e).parent().parent().find('.amount').text().trim());
    $('#inp-gst').val($(e).parent().parent().find('.gst').text().trim());
    $('#inp-cgstper').val($(e).parent().parent().find('.cgstper').text().trim());
    $('#inp-cgstamt').val($(e).parent().parent().find('.cgstamt').text().trim());
    $('#inp-sgstper').val($(e).parent().parent().find('.sgstper').text().trim());
    $('#inp-sgstamt').val($(e).parent().parent().find('.sgstamt').text().trim());
    $('#inp-igstper').val($(e).parent().parent().find('.igstper').text().trim());
    $('#inp-igstamt').val($(e).parent().parent().find('.igstamt').text().trim());
    $('#inp-total').val($(e).parent().parent().find('.total').text().trim());
    $(e).parent().parent().remove();
    netvalue();
    calc();

}


function submit() {
    var valid = true;
    if (checker('section') != false) {
        valid = valid * true;
    } else {
        valid = valid * false;
    }
    var arr = modals.tabletdbyclass('table-customer');
    arr.shift();
    if (arr != '') {
        valid = valid * true;
    } else {
        valid = valid * false;
        $('#table-customer').css('border', '2px solid red');
    }

    if (valid) {
        data1 = checker('section');
        var datastr1 = JSON.stringify(data1);
        var datastr2 = JSON.stringify(arr);

        $.ajax({
            type: "POST",
            data: {
                data1: datastr1,
                data2: datastr2

            },
            url: 'salesinsert.php',
            cache: false,
            success: function (res) {
                if (res.status == 'success') {
                    setTimeout(function () {
                        location.href = '/process/crm/workcontract.php';
                    }, 500);
                }
            }
        });
    }
}

function update() {
    var valid = true;
    if (checker('section') != false) {
        valid = valid * true;
    } else {
        valid = valid * false;
    }
    var arr = modals.tabletdbyclass('table-customer');
    arr.shift();
    if (arr != '') {
        valid = valid * true;
    } else {
        valid = valid * false;
        $('#table-customer').css('border', '2px solid red');
    }

    if (valid) {
        data1 = checker('section');
        var datastr1 = JSON.stringify(data1);
        var datastr2 = JSON.stringify(arr);

        $.ajax({
            type: "POST",
            data: {
                data1: datastr1,
                data2: datastr2

            },
            url: 'salesupdate.php',
            cache: false,
            success: function (res) {
                if (res.status == 'success') {
                    setTimeout(function () {
                        location.href = '/process/crm/workcontract.php';
                    }, 500);
                }
            }
        });
    }
}

function calc() {
    var qty = parseFloat($('#inp-qty').val());
    var rate = parseFloat($('#inp-rate').val());
    //   var servicename=$('#inp-service').val();

    var amount = qty * rate;
    var quantity = qty
    //   var service=servicename
    $('#inp-amount').val(amount);
    $('#quantity').val(quantity);
    //   $('#inp_servicename').val(service);

    var gst = parseFloat($('#inp-gst').val());
    var statecode = parseFloat($('#statecode').val());
    if (statecode == 27) {
        gst = gst / 2;
        var gstamount = (gst * amount) / 100;
        $('#inp-cgstper').val(gst);
        $('#inp-cgstamt').val(gstamount);
        $('#inp-sgstper').val(gst);
        $('#inp-sgstamt').val(gstamount);
        $('#inp-igstper').val(0);
        $('#inp-igstamt').val(0);
        var total = ((gstamount * 2) + amount).round(2);
        $('#inp-total').val(total);
    } else {
        var gstamount = (gst * amount) / 100;
        $('#inp-cgstper').val(0);
        $('#inp-cgstamt').val(0);
        $('#inp-sgstper').val(0);
        $('#inp-sgstamt').val(0);
        $('#inp-igstper').val(gst);
        $('#inp-igstamt').val(gstamount);
        var total = (gstamount + amount).round(2);
        $('#inp-total').val(total);
    }
    //   myFunction();
}

function addorder() {
    var itemname = $('#itemname').find(":selected").text();
    console.log(itemname);


    var valid = true;
    if (checker('tr-table1') != false) {
        valid = valid * true;
    } else {
        valid = valid * false;
    }
    if (valid) {
        data = checker('tr-table1');
        tableui(data.itemcode, itemname, data.qty, data.rate, data.amount, data.cgstamt, data.cgstper, data.gst, data.igstamt, data.igstper, data.sgstamt, data.sgstper, data.total);
        modals.clear('tr-table1');
    }
}


function tableui(itemcode, itemname, qty, rate, amount, cgstamt, cgstper, gst, igstamt, igstper, sgstamt, sgstper, total) {
    // var srno=$('#table-customer > tbody > tr').length;
    var str = '<tr class="abc">';
    // str += '<td align="center">' + srno + '</td>';
    str += '<td align="center" class="itemcode" style="display:none;">' + itemcode + '</td>';
    str += '<td align="center" class="itemname">' + itemname + '</td>';
    str += '<td align="center" class="qty">' + qty + '</td>';
    str += '<td align="center" class="rate">' + rate + '</td>';
    str += '<td align="center" class="amount">' + amount + '</td>';
    str += '<td align="center" class="gst">' + gst + '</td>';
    str += '<td align="center" class="cgstper">' + cgstper + '</td>';
    str += '<td align="center" class="cgstamt">' + cgstamt + '</td>';
    str += '<td align="center" class="sgstper">' + sgstper + '</td>';
    str += '<td align="center" class="sgstamt">' + sgstamt + '</td>';
    str += '<td align="center" class="igstper">' + igstper + '</td>';
    str += '<td align="center" class="igstamt">' + igstamt + '</td>';

    str += '<td align="center" class="total">' + total + '</td>';
    str += '<td align="center"><button class="btn btn-sm btn-danger" onclick="remover(this);"> R </button></td>';
    str += '</tr>';
    $('#table-customer > tbody').append(str);

    //  gst();
    netvalue();
}





function netvalue() {
    var cgsttotal = 0;
    var sgsttotal = 0;
    var igsttotal = 0;
    var amounttotal = 0;
    $('#table-customer .abc ').each(function () {
        amounttotal += parseFloat($(this).find('.amount').text().trim());
        console.log(amounttotal);

        cgsttotal += parseFloat($(this).find('.cgstamt').text().trim());
        sgsttotal += parseFloat($(this).find('.sgstamt').text().trim());
        igsttotal += parseFloat($(this).find('.igstamt').text().trim());
    })
    $('#inp-taxable').val(amounttotal);
    $('#inp-cgsttotal').val(cgsttotal);
    $('#inp-sgsttotal').val(sgsttotal);
    $('#inp-igsttotal').val(igsttotal);
    var othercharges = $('#inp-othercharges').val();
    if (othercharges == "") {
        othercharges = 0;
        $('#inp-othercharges').val(0);
    }
    othercharges = parseFloat(othercharges);
    // var trastotal=parseFloat($('#tras-total').val());
    var invoicevalue = amounttotal + cgsttotal + sgsttotal + igsttotal + othercharges;
    $('#inp-invoicevalue').val(invoicevalue);
}

//---Select Box ---//
$(document).ready(function () {
    $('.select-js1').select2({
        width: '100%'
    });
    $('.select').attr('style', 'width:100%!important;');
});
</script>