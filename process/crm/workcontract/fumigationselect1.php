<?php
    
    $base = '../../../';

    include($base.'_in/connect.php');

    header('content-type: application/json; charset=utf-8');

    header("access-control-allow-origin: *");

    if(isset($_POST['branchid']) && isset($_POST['contractid'])){
        
        $con        = _connect();
        
        $branchid   = _clean($con,$_POST['branchid']);
        
        $contractid = _clean($con,$_POST['contractid']);
        
        $select     = mysqli_fetch_assoc(mysqli_query($con,"SELECT customerid,volume,quantity,contractdate,amount,cgstper,sgstper,igstper,cgstamt,sgstamt,igstamt ,othercharges ,totalcontractamt ,billqty ,servicejson , billlinkjson, itemjson ,ftype ,ftype2 ,fname ,termofpayment ,nameofexporter ,addressofexporter ,billingparty,servicelocation,servicetime FROM workcontract WHERE branchid='$branchid' AND contractid='$contractid'  "));

        if($select){
            
            $volume            = $select['volume'];
            
            $quantity          = $select['quantity'];
            
            $contractdate1     = $select['contractdate'];
            
            $contractdate      = $select['contractdate'];
            
            $amount            = $select['amount'];
            
            $cgstper           = $select['cgstper'];
            
            $sgstper           = $select['sgstper'];
            
            $igstper           = $select['igstper'];
            
            $cgstamt           = $select['cgstamt'];
            
            $sgstamt           = $select['sgstamt'];
            
            $igstamt           = $select['igstamt'];
            
            $othercharges      = $select['othercharges'];
            
            $totalcontractamt  = $select['totalcontractamt'];
            
            $billqty           = $select['billqty'];
            
            $servicejson       = $select['servicejson'];
            
            $servicedate1      = mysqli_fetch_assoc(mysqli_query($con,"SELECT servicedate x FROM workcontractservice WHERE contractid = '$contractid' AND branchid='$branchid'"))['x'];
            
            $servicedate       = $servicedate1;
            
            $billjson          = $select['billlinkjson'];
                        
            $billdate1         = mysqli_fetch_assoc(mysqli_query($con,"SELECT billdate x FROM workcontractbill WHERE contractid = '$contractid' AND branchid='$branchid'"))['x'];
            
            $billdate          = $billdate1;
            
            $itemjson          = $select['itemjson'];
            
            $ftype             = $select['ftype'];
            
            $ftype2            = $select['ftype2'];
            
            $fname             = $select['fname'];
            
            $termofpayment     = $select['termofpayment'];
            
            $nameofexporter    = $select['nameofexporter'];
            
            $addressofexporter = $select['addressofexporter'];
            
            $billingparty      = $select['billingparty'];
            
            $fumigationplaces  = json_decode($select['servicelocation'],true);
      
            $fumigationplace   = $fumigationplaces[0]['servicelocation'];

            $servicetime       = $select['servicetime'];

            if($contractdate!=0 && $contractdate!=''){ $contractdate= date('d F, Y',  $contractdate/1000); }else{  $contractdate=''; }
            
            if($billdate!=0 && $billdate!=''){ $billdate= date('d F, Y',  $billdate/1000); }else{  $billdate=''; }
            
            if($servicedate!=0 && $servicedate!=''){ $servicedate= date('d F, Y',  $servicedate/1000); }else{  $servicedate=''; }

            $json = '{"customerid":"'.$select['customerid'].'","billjson":'.$billjson.',"servicejson":'.$servicejson.',"contractdate1":"'.$contractdate1.'","contractid":"'.$contractid.'","billdate1":"'.$billdate1.'","billdate":"'.$billdate.'","servicedate":"'.$servicedate.'","servicedate1":"'.$servicedate1.'","volume":"'.$volume.'","quantity":"'.$quantity.'","contractdate":"'.$contractdate.'","amount":"'.$amount.'","cgstper":"'.$cgstper.'","sgstper":"'.$sgstper.'","igstper":"'.$igstper.'","cgstamt":"'.$cgstamt.'","sgstamt":"'.$sgstamt.'","igstamt":"'.$igstamt.'","othercharges":"'.$othercharges.'","totalcontractamt":"'.$totalcontractamt .'","billqty":"'.$billqty.'","ftype":"'.$ftype.'","ftype2":"'.$ftype2.'","fname":"'.$fname.'","termofpayment":"'.$termofpayment.'","nameofexporter":"'.$nameofexporter.'","addressofexporter":"'.$addressofexporter.'","billingparty":"'.$billingparty.'","fumigationplace":"'.$fumigationplace.'","servicetime":"'.$servicetime.'"}'; 
            
            echo '{"status":"success","json":['.$json.']}';  

        } else {

            echo '{"status":"falid1"}';

        }

        _close($con);
      
    } else {

        echo '{"status":"falid2"}';
    
    }
    
?>