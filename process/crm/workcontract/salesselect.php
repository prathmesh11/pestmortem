<?php
    $base='../../../';
    include($base.'_in/connect.php');
    header('content-type: application/json; charset=utf-8');
    header("access-control-allow-origin: *");
    if(isset($_POST['enquiryid']) && isset($_POST['contractid'])){

      $con = _connect();
      $str = '';

      if (session_status() == PHP_SESSION_NONE) { session_start(); }
      $branchid   = $_SESSION['branchid']; 
      $enquiryid  = _clean($con,$_POST['enquiryid']);
      $contractid = _clean($con,$_POST['contractid']);

      $address           = get_object_vars(json_decode(mysqli_fetch_assoc(mysqli_query($con,"SELECT branchjson x FROM `branchmaster` WHERE branchid='$branchid'"))['x']))['address'];      
      $gstnumber1        = get_object_vars(json_decode(mysqli_fetch_assoc(mysqli_query($con,"SELECT branchjson x FROM `branchmaster` WHERE branchid='$branchid'"))['x']))['gstnumber'];
      $companybranchname = get_object_vars(json_decode(mysqli_fetch_assoc(mysqli_query($con,"SELECT branchjson x FROM `branchmaster` WHERE branchid='$branchid'"))['x']))['companybranchname'];
      $email             = get_object_vars(json_decode(mysqli_fetch_assoc(mysqli_query($con,"SELECT branchjson x FROM `branchmaster` WHERE branchid='$branchid'"))['x']))['email'];
      $phone             = get_object_vars(json_decode(mysqli_fetch_assoc(mysqli_query($con,"SELECT branchjson x FROM `branchmaster` WHERE branchid='$branchid'"))['x']))['phone'];
      $state             = get_object_vars(json_decode(mysqli_fetch_assoc(mysqli_query($con,"SELECT branchjson x FROM `branchmaster` WHERE branchid='$branchid'"))['x']))['state'];
      $statename         = mysqli_fetch_assoc(mysqli_query($con,"SELECT statename x FROM `statemaster` WHERE statecode='$state'"))['x'];

  
    $result = mysqli_fetch_assoc(mysqli_query($con,"SELECT * FROM workcontract WHERE enquiryid='$enquiryid' AND contractid='$contractid' "));
    if($result){

      $customerid   = mysqli_fetch_assoc(mysqli_query($con,"SELECT customerid x FROM crmmaster WHERE enquiryid='$enquiryid' AND contractid='$contractid' "))['x'];
      $customername = get_object_vars(json_decode(mysqli_fetch_assoc(mysqli_query($con,"SELECT customerjson x FROM `customermaster` WHERE branchid='$branchid' AND customerid='$customerid' "))['x']))['customername'];
      $address      = get_object_vars(json_decode(mysqli_fetch_assoc(mysqli_query($con,"SELECT customerjson x FROM `customermaster` WHERE branchid='$branchid' AND customerid='$customerid' "))['x']))['address'];
      $contactjson  = json_decode(mysqli_fetch_assoc(mysqli_query($con,"SELECT contactjson x FROM customermaster WHERE customerid='$customerid' AND branchid='$branchid'"))['x'],true);
      $mobile       = $contactjson[0]['mobile'];
      $email        = $contactjson[0]['email'];

      $gstnumber    = get_object_vars(json_decode(mysqli_fetch_assoc(mysqli_query($con,"SELECT customerjson x FROM `customermaster` WHERE branchid='$branchid' AND customerid='$customerid' "))['x']))['gstnumber'];
      $state        = get_object_vars(json_decode(mysqli_fetch_assoc(mysqli_query($con,"SELECT customerjson x FROM `customermaster` WHERE branchid='$branchid' AND customerid='$customerid' "))['x']))['state'];
      $statename    = mysqli_fetch_assoc(mysqli_query($con,"SELECT statename x FROM `statemaster` WHERE statecode='$state'"))['x'];

      $contractdate = date('Y-m-d',$result['contractdate']/1000);
     // echo $contractdate;
          $str='{"contractid":"'.$result["contractid"].'",
          "enquiryid":"'.$result["enquiryid"].'",
          "date":"'.$contractdate.'",
          "customerid":"'.$customerid.'",
          "gstnumber":"'.$gstnumber.'",
          "statename":"'.$statename.'",
          "companyname":"'.$customername.'",
          "caddress":"'.$address.'",
          "mnumber":"'.$mobile.'",
          "email":"'.$email.'",
          "tablejson":'.$result["itemjson"].',
          "amount":"'.$result["amount"].'",
          "cgstamt":"'.$result["cgstamt"].'",
          "sgstamt":"'.$result["sgstamt"].'",
          "igstamt":"'.$result["igstamt"].'",
          "othercharges":"'.$result["othercharges"].'",
          "total":"'.$result["totalcontractamt"].'"}';

          echo '{"status":"success","json":['.$str.'],"address":"'.$address.'","gstnumber":"'.$gstnumber1.'","companybranchname":"'.$companybranchname.'","email":"'.$email.'","phone":"'.$phone.'","state":"'.$statename.'"}';
      }else{
            echo '{"status":"falid1"}';
      }
             _close($con);
    }else{
        echo '{"status":"falid2"}';
    }
?>
