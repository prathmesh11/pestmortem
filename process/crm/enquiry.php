<?php 

  $base    = '../../';
  $navenq1 = 'background:#1B1464;';
  include('header.php');

  $enquiryidnew = mysqli_fetch_assoc(mysqli_query($con,"SELECT id x FROM crmmaster ORDER BY id DESC"))['x'];

    $year1 = 0;
    $year2 = 0;

  if ( date('m') > 3 ) {

      $year1 = date('y');
      $year2 = date('y')+1;

  }else{

      $year1 = date('y')-1;
      $year2 = date('y');

  }

  if (session_status() == PHP_SESSION_NONE) { session_start(); }
  $sessionby = $_SESSION['employeeid'];
  $branchid  = $_SESSION['branchid'];

  $branchshortname = mysqli_fetch_assoc(mysqli_query($con,"SELECT branchshortname x FROM branchmaster WHERE branchid='$branchid'"))['x'];

  if($enquiryidnew){

      $enquiryidnew++;
      $enquiryidnew = str_pad($enquiryidnew,6, '0', STR_PAD_LEFT);
      $enquiryidnew = 'PMI/'.$branchshortname.'/ENQ/'.$enquiryidnew.'/'.$year1.'-'.$year2;

    }else{

      $enquiryidnew = 'PMI/'.$year1.'-'.$year2.'/B'.$branchid.'/E'.$sessionby.'/1';
      $enquiryidnew = 'PMI/'.$branchshortname.'/ENQ/000001/'.$year1.'-'.$year2;

    }

?>
<br>
<style>

.input-container {

  max-width: 300px;
  background-color: #EDEDED;
  border: 1px solid #DFDFDF;
  border-radius: 5px;
}

input[type='file'] {
  display: none;
}

.file-info {
  font-size: 0.9em;
}

.browse-btn {
  background: #03A595;
  color: #fff;
  min-height: 35px;
  padding: 10px;
  border: none;
  border-top-left-radius: 5px;
  border-bottom-left-radius: 5px;
}

.browse-btn:hover {
  background: #4ec0b4;
}

@media (max-width: 300px) {
  button {
    width: 100%;
    border-top-right-radius: 5px;
    border-bottom-left-radius: 0;
  }

  .file-info {
    display: block;
    margin: 10px 5px;
  }
}

</style>

<img class="hidden" id=imageid src="../../../../img/PestMortemLogo.jpg">

<div class="table-ui container-fluid">

  <div class="tr row">

    <div class="col-sm-1 th">Enquiry No.</div>
    <div class="col-sm-3 th">Customer Info</div>
    <div class="col-sm-1 th">Enquiry For</div>
    <div class="col-sm-2 th">Offer</div>
    <div class="col-sm-2 th">Note</div>
    <div class="col-sm-1 th">Follow Up</div>
    <div class="col-sm-2 th">Action</div>

  </div>

  <?php
  
    $result=mysqli_query($con,"SELECT enquiryid ,category,customerid, followupnote ,followuptime,contractid FROM crmmaster WHERE branchid='$branchid' AND created_time<>0 AND created_by<>0 AND enquiry_time=0 AND enquiry_by=0 AND wo_time=0 AND wo_by=0 AND close_enquiry_time=0 AND close_enquiry_by=0 ORDER BY created_time DESC");
    
    while($rows = mysqli_fetch_assoc($result)){

      $customerid = $rows['customerid'];
      $enquiryid  = $rows['enquiryid'];

      $enquiryid = mysqli_fetch_assoc(mysqli_query($con,"SELECT contractid x FROM sales WHERE enquiryid='$enquiryid'"))['x'];

      $cifo      = mysqli_fetch_assoc(mysqli_query($con,"SELECT * FROM customermaster WHERE customerid='$customerid'"));


  ?>

  <div class="row tr">

    <div class="col-sm-1 td" style="word-wrap:break-word;">

      <?php echo $rows['enquiryid']; ?>

    </div>

    <div class="col-sm-3 td">
      <?php 

        if ($cifo == '') {

          echo 'Company : '.$customerid;
          
        } else {

          echo 'Company : '.$cifo['customername']; 
          echo '<br>Address : '.get_object_vars(json_decode($cifo['customerjson']))['address']; 
          echo '<br>Name  : '.get_object_vars(json_decode($cifo['contactjson'])[0])['name'];
          echo '<br>Dept. : '.get_object_vars(json_decode($cifo['contactjson'])[0])['department'];
          echo '<br>Mobile : <a href="tel:'.get_object_vars(json_decode($cifo['contactjson'])[0])['mobile'].'">'.get_object_vars(json_decode($cifo['contactjson'])[0])['mobile'].'</a>';
          echo '<br>Landline : '.get_object_vars(json_decode($cifo['contactjson'])[0])['landline'];
          echo '<br>Email : <a href="mailto:'.get_object_vars(json_decode($cifo['contactjson'])[0])['email'].'">'.get_object_vars(json_decode($cifo['contactjson'])[0])['email'].'</a>';

        }

      ?>

    </div>

    <div class="col-sm-1 td">
    
      <?php echo $rows['category']; ?>

    </div>

    <div class="col-sm-2 td">
      <!-- offer  -->

      <div class="input-container">

        <input type="file" class="real-input" name="quatation" accept="application/pdf">

        <button class="browse-btn">
          Browse Files
        </button>

        <span class="file-info">Upload a file</span>

      </div>

      <button class="btn btn-primary btn-sm btn-block" data-enquiryid="<?php echo $rows['enquiryid']; ?>"
        onclick="upload(this)">Upload </button>

      <?php 

        $enquir = str_replace('/', '-',$rows['enquiryid']);
        $path   = "api/offer/".$enquir.'.pdf';

        if(file_exists($path)){

          echo '<br><center><a href="'.$path.'" target="_blank">View PDF</a></center>';

        }else{

          echo '<br>Please Upload .pdf File';

        }

      ?>



      <!-- offer  -->
    </div>

    <div class="col-sm-2 td">
      
      <?php echo $rows['followupnote']; ?>

    </div>

    <div class="col-sm-1 td">
    
      <?php 

        $lastfollowup       = $rows['followuptime'];
        $followUpStatusFlag = '';

        if($lastfollowup!=0){

          if($START_OF_THE_DAY<$lastfollowup && $END_OF_THE_DAY<=$lastfollowup){

              $followUpStatusFlag = '#3F51B5';

          }

          if($START_OF_THE_DAY>$lastfollowup && $END_OF_THE_DAY>$lastfollowup){

              $followUpStatusFlag = '#F44336';

          }
          
          if($START_OF_THE_DAY<=$lastfollowup && $END_OF_THE_DAY>$lastfollowup ){

              $followUpStatusFlag = '#4CAF50';

          }

        } else {

          $followUpStatusFlag = '#ff9800';

        }

        if($lastfollowup==0){ $lastfollowup='NO Followup'; }else{ $lastfollowup=date("d-m-Y",$lastfollowup/1000); }

        echo '<p style="background:'.$followUpStatusFlag.';color:#fff;">'.$lastfollowup.'</p>'; 

        $category = $rows['category'];

        if ($category=="sales") {

          if($rows['contractid']){

            echo '<button class="btn btn-sm btn-primary" data-enquiryid="'.$rows['enquiryid'].'" data-contractid="'.$rows['contractid'].'" onclick="salespdf(this)">Download</button>';

          }

        }

      ?>

    </div>

    <div class="col-sm-2 td">

      <button class="btn btn-primary btn-sm btn-block" data-toggle="modal" data-target="#myModal2"
        data-enquiryid="<?php echo $rows['enquiryid']; ?>" onclick="setidtomidal(this)">Followup</button>
        
      <button class="btn btn-success btn-sm btn-block" data-link1="enquiry_time" data-link2="enquiry_by"
        data-enquiryid="<?php echo $rows['enquiryid']; ?>" data-customerid="<?php echo $cifo['customername']; ?>" data-customername="<?php echo $customerid; ?>" onclick="confirms(this)">Order Confirm</button>

      <?php

        $category = $rows['category'];

        if ($category=="sales") {

          if(!$rows['contractid']){

          // echo '<a class="btn btn-warning btn-sm  btn-block" href="workcontract/sales.php?enq='.$rows['enquiryid'].'" >Sales</a>';
          } else {

            echo "<a class='btn btn-default btn-sm  btn-block' href=/process/crm/workcontract/sales.php?enquiryid=".$rows['enquiryid']."&contractid=".$rows['contractid'].">Edit</a>";  }
          
        }

      ?>

      <button class="btn btn-sm btn-block btn-danger" data-enquiryid="<?php echo $rows['enquiryid']; ?>"
        onclick="closes(this)">Order Close</button>

    </div>

  </div>

  <?php

   }

  ?>
</div>



<button class="btn" style="font-size:25px;border-radius:50px;width:60px;height:60px;position: fixed;
    bottom: 40px;
    right: 40px;background:#eb2f06;color:#fff;" data-toggle="modal" data-target="#myModal">+</button>


<div id="myModal" class="modal fade" role="dialog">

  <div class="modal-dialog">
    <!-- Modal content-->

    <div class="modal-content">

      <div class="modal-header">

        <h4 class="modal-title" align="center">New Enquiry</h4>

      </div>

      <div class="modal-body">

        <div class="form-group row">

          <div class="col-sm-4">Enquiry No.</div>

              <div class="col-sm-8">
              
                <input type="text" data-name="enquiryid" value="<?php echo $enquiryidnew; ?>" class="form-control input-sm" readonly>

              </div>

          <div class="col-sm-4">Select Customer</div>

          <div class="col-sm-6">

          
            <input type="text" list="customerid"  data-name='customerid' class="form-control input-sm" />
            
            <datalist id="customerid" >
              <?php

                  $result= mysqli_query($con,"SELECT customerid,customername  FROM customermaster ORDER by customerid ASC ");

                  while($rows = mysqli_fetch_assoc($result)){

                    echo '<option value="'.$rows['customerid'].' '.$rows['customername'].'"></option>';


                  }
              
              ?>
            </datalist>


            <!-- <select data-role="select" data-name='customerid' id="customeridnew" class="form-control input-sm select-js">

              <option value="Select">Select</option>
              <?php 

                  $result11=mysqli_query($con,"SELECT customerid,customername  FROM customermaster ORDER by customerid ASC");

                  while($rows = mysqli_fetch_assoc($result11)){

                    echo '<option value="'.$rows['customerid'].'">'.$rows['customername'].'</option>';

                  }

              ?>

            </select> -->

          </div>

          <div class="col-sm-2">

            <a href="/process/create-master/customer-master/index.php" class="btn btn-block btn-success btn-sm">New</a>

          </div>

          <div class="col-sm-4">Select Category</div>

          <div class="col-sm-8">

            <select data-role="select" data-name='category' class="form-control input-sm">

              <option value="Fumigation">Fumigation</option>
              <option value="Pest Control">Pest Control</option>
              <option value="sales">sales</option>

            </select>

          </div>

          <div class="col-sm-4">Set New Followup</div>

          <div class="col-sm-8">
          
            <input type="text" id="inp-enq-date" class="form-control input-sm">
            <input type="hidden" data-name="followuptime" id="inp-enq-date-stamp">

          </div>

          <div class="col-sm-4">Note</div>

          <div class="col-sm-8">

            <input type="text" data-name="followupnote" class="form-control input-sm">

          </div>

          <div class="col-sm-4"></div>

          <div class="col-sm-8">
          
            <button class="btn btn-success btn-block btn-sm" id="newqneuiry" onclick="newqneuiry()">Add New Enquery</button>

          </div>

        </div>

      </div>

    </div>

  </div>

</div>

<div id="myModal2" class="modal fade" role="dialog">

  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">

      <div class="modal-header">

        <h4 class="modal-title" align="center">New FollowUp</h4>

      </div>

      <div class="modal-body">

        <div class="form-group row">

          <div class="col-sm-4">Set New Followup</div>

          <input type="hidden" data-name="enquiryid" id="inp-enquiryidset">

          <div class="col-sm-8">

            <input type="text" id="inp-enq-olddate" class="form-control input-sm">
            <input type="hidden" id="inp-enq-olddate-stamp" data-name="followuptime">

          </div>
          <div class="col-sm-4">Note</div>

          <div class="col-sm-8">

            <input type="text" data-name="followupnote" class="form-control input-sm">

          </div>

          <div class="col-sm-4"></div>

          <div class="col-sm-8">

            <button class="btn btn-success btn-block btn-sm" onclick="setqneuiry()">Add New FollowUp</button>

          </div>

        </div>

      </div>

    </div>

  </div>

</div>

<?php 
include('footer.php');
?>
<script>



$('.browse-btn').on('click', function () {
  $(this).parent().find('input').click();
});

$('.real-input').on('change', function () {
  $(this).parent().find('span').text($(this).val().replace(/C:\\fakepath\\/i, ''));

});

function newqneuiry() {

  var valid = true;
  if (checker('myModal') != false) {
    valid = valid * true;
  } else {
    valid = valid * false;
    alert('Please Select Customer');
  }
  if (valid) {
    $('#newqneuiry').hide();
    var data = checker('myModal');
    var datastr = JSON.stringify(data);
    $.ajax({
      type: "POST",
      data: {
        data: datastr
      },
      url: 'api/newqneuiry.php',
      cache: false,
      success: function (res) {
        if (res.status == 'success') {

          window.location.reload();

        } else {
          $('#newqneuiry').show();
        }
      }
    })
  }
}

$("#inp-enq-date").pickadate({
  selectYears: true,
  selectMonths: true,
  min: true,
  onClose: function () {
    try {
      var dateStamp = this.get('select')['pick'];
      $("#inp-enq-date-stamp").val(dateStamp);
    } catch (err) {

    }
  }
});

$("#inp-enq-olddate").pickadate({
  selectYears: true,
  selectMonths: true,
  min: true,
  onClose: function () {
    try {
      var dateStamp = this.get('select')['pick'];
      $("#inp-enq-olddate-stamp").val(dateStamp);
    } catch (err) {

    }
  }
});

function setidtomidal(e) {
  var enquiryid = $(e).data('enquiryid');
  $('#inp-enquiryidset').val(enquiryid);
}

function setqneuiry() {
  var valid = true;
  if (checker('myModal2') != false) {
    valid = valid * true;
  } else {
    valid = valid * false;
  }
  if (valid) {
    var data = checker('myModal2');
    var datastr = JSON.stringify(data);
    $.ajax({
      type: "POST",
      data: {
        data: datastr
      },
      url: 'api/setqneuiry.php',
      cache: false,
      success: function (res) {
        if (res.status == 'success') {
          window.location.reload();
        }
      }
    })
  }
}


function upload(e) {
  var file = $(e).parent().parent().find('input')[0].files[0];
  var enquiryid = $(e).data('enquiryid');
  var formData = new FormData();

  formData.append('offer', file);
  formData.append('enquiryid', enquiryid);
  $.ajax({
    type: "POST",
    data: formData,
    url: 'api/offer.php',
    cache: false,
    processData: false,
    contentType: false,
    success: function (res) {
      window.location.reload();
    }
  })
}


// function getBase64Image(img) {
//   var canvas = document.createElement("canvas");
//   canvas.width = img.width;
//   canvas.height = img.height;
//   var ctx = canvas.getContext("2d");
//   ctx.drawImage(img, 0, 0);
//   var dataURL = canvas.toDataURL("image/png");
//   return dataURL.replace('/^data:image\/(png|jpg);base64,/', "");
// }



// function salespdf(e){
//   var contractid = $(e).data('contractid');
//   var enquiryid = $(e).data('enquiryid');

//   $.ajax({
//       type: "POST",
//       data: "contractid=" + contractid +"&enquiryid="+enquiryid,
//       url: 'workcontract/salesselect.php',
//       cache: false,
//       success: function (res) {
//         if (res.status == 'success') {
//           var content=[];
//           json=res.json[0];

//         var base64 = getBase64Image(document.getElementById("imageid"));
//   content.push(
//     {text:'SALES BILL\n\n',fontSize: 14,bold:1,alignment: "center"},
// 	    {
//       table:{
//       margin: [10, 0],
//       widths: [75, 170,'*', '*'],
//       body:[
//         [
//           {
//           rowSpan: 2,
//           stack: [{image:base64,
//                     width: 75,
//                     height:50,
//                     margin:[0,20]
//                     },],
//           },
//           {
//           rowSpan: 2, 
//           stack: [{
//             text: [
//             {text:'Pest Mortem (l) Pvt Ltd\n',fontSize: 10,bold:1,},
//             {text:res.companybranchname+'\n',fontSize: 10},
//             {text:res.address+'\n',fontSize: 10},
//             {text:'GST No: '+res.gstnumber+' \n',fontSize: 10},
//             {text:'State: '+res.state+' \n',fontSize: 10},
//             {text:'Email:'+res.email+' \n',fontSize: 10},
//             {text:'Phone No:'+res.phone+'\n',fontSize: 10},
//             ]},
//             ],
//           },
//         {
//         border:[1,1,0,0],
//         text: [
//           {text:'Voucher No.\n',fontSize: 10},
//           {text:json.contractid+'\n',fontSize: 10,bold:1},
//           ]
//         },
//         {
//         border:[1,1,1,0],
//         text: [
//             {text:'Dated\n',fontSize: 10},
//             {text:json.date+'\n',fontSize: 10,bold:1},
//             ]
//         }
//     ],
//     [
//       {}, {},
//       {
//         border:[0,0,0,0],
//         text: [
//           // {text:'Supplier\'s Ref./Order No.\n',fontSize: 10},
//           // {text:json.gstnumber+'\n',fontSize: 10,bold:1},
//           {text:'\nDespatch through\n',fontSize: 10},
//           {text:'',fontSize: 10},
//           ]
//         }, 
//         { 
//         // border:[1,0,1,0],
//         // text: [
//         //   {text:'Mode/term of Payment\n',fontSize: 10},
//         //   {text:json.creditdays+' Days\n',fontSize: 10,bold:1},
//         //   {text:'\nOther Reference(s)\n',fontSize: 10},
//         //   {text:'',fontSize: 10,bold:1},
//         //   ]
//         }, 
//       ],
//     [
//       {
//       rowSpan: 2,
//       colSpan: 2, 
//           stack: [{
//             text: [
//             {text:'Customer\n',fontSize: 10},
//             {text:json.companyname+'\n',fontSize: 10,bold:1,},
//             {text:json.caddress+'\n',fontSize: 10},
//             {text:'Mumbai \n',fontSize: 10},
//             {text:'GSTIN/UIN        : '+json.gstnumber+' \n',fontSize: 10},
//             {text:'State Name       : '+json.statename+'\n',fontSize: 10}
//             ]},
//           ],    
//         },
//         {},
//         {   
//         rowSpan: 2,
//         border:[1,1,1,1],
//           text: [
//               {text:'\nTerms of Delivery\n',fontSize: 10},
//               {text:'\n',fontSize: 10,bold:1},
//               ]
//         }, 
//         {
//           rowSpan: 2,
//           stack: [{
//             text: [
//               {text:'\nDestination\n',fontSize: 10,bold:1},
//               {text:json.caddress+'\n',fontSize: 10},
//             ]},
//           ], 
//           // text: [
//           //   {text:'\nDestination\n',fontSize: 10,bold:1},
//           //   {text:json.poforbadd+'\n',fontSize: 10},
//           //   ]
//           }
//     ],
//       [{}, {},{}, {}],
//     ]
//     },layout: {
//         hLineColor: function (i, node) {
//           return (i === 0 || i === node.table.body.length) ? 'gray' : 'gray';
//         },
//         vLineColor: function (i, node) {
//           return (i === 0 || i === node.table.widths.length) ? 'gray' : 'gray';
//         },
//       }
//     },
// ),

//     table1 = {
// 				widths: [15,95,40,25,40,40,40,40,40,50],
// 		                dontBreakRows: true,
//                     fontSize: 20,


// 				body: []
//             };
//           var particular=  
//           [{text: '#',fillColor: '#1E90FF'},
//           {text: 'ITEM',color:'white', border: [1,1,1,1],fillColor: '#1E90FF',fontSize: '10',bold: 'true', alignment: 'center'},
//           {text: 'Quantity',color:'white',border: [1,1,1,1],fillColor: '#1E90FF',fontSize: '10',bold: 'true',alignment: 'center',},
//           {text: 'Rate',color:'white',border: [1,1,1,1],fillColor: '#1E90FF',fontSize: '10',bold: 'true',alignment: 'center',},
//           {text: 'Amount',color:'white',border: [1,1,1,1],fillColor: '#1E90FF',fontSize: '10',bold: 'true',alignment: 'center',},
//           {text: 'GST',color:'white',border: [1,1,1,1],fillColor: '#1E90FF',fontSize: '10',bold: 'true',alignment: 'center',},
//           {text: 'CGST AMT',color:'white',border: [1,1,1,1],fillColor: '#1E90FF',fontSize: '10',bold: 'true',alignment: 'center',},
//           {text: 'SGST AMT',color:'white',border: [1,1,1,1],fillColor: '#1E90FF',fontSize: '10',bold: 'true',alignment: 'center',},
//           {text: 'IGST AMT', color:'white',border: [1,1,1,1],fillColor: '#1E90FF',fontSize: '10',bold: 'true',alignment: 'center',},
//           {text: 'NET AMOUNT',color:'white',border: [1,1,1,1],fillColor: '#1E90FF',fontSize: '10',bold: 'true',alignment: 'center',}];

//            table1['body'].push(particular);     

//            content.push({
//         table: {
//             headerRows: 1,
//             // widths: ['*'],
//             widths: [15,95,40,25,40,40,40,40,40,50],
//             alignment: 'center',

//             body: [
//               [],

//             ]
//         },
//     });
//     var cr=1;
//     var tablejson=json.tablejson;
// for(var i in tablejson){
//    var tb=[];
//    tb.push(cr,{text:tablejson[i].itemname,alignment: 'center',fontSize: '9',},{text:tablejson[i].qty,alignment: 'center'},{text:tablejson[i].rate,alignment: 'center'},{text:tablejson[i].amount,alignment: 'center'},{text:tablejson[i].gst,alignment: 'center'},{text:tablejson[i].cgstamt,alignment: 'center'},{text:tablejson[i].sgstamt,alignment: 'center'},{text:tablejson[i].igstamt,alignment: 'center'},{text:tablejson[i].total,alignment: 'center'});
//    table1['body'].push(tb);
//    cr++;

// }



// content.push({
//                         text: "",
//                         fontSize: 20,
//                         alignment: 'center'
//                     }, {
//                         style:'tablfont1',
//                         table: table1
//                     });



// content.push({
//  text: "\n",
//   });


// content.push({
//   table: {
//     // heights: [20, 20, 20],
//     alignment: 'center',
//     widths: ['*','*','*','*','*','*'],
//     fontSize: 20,



//     body: [
//       [{text: '', border: [false, false, false, false]},{text: '', border: [false, false, false, false]},{text: '', border: [false, false, false, false]},{text: '', border: [false, false, false, false]},{text: 'TAX AMOUNT',alignment: 'center',bold: 'true',fontSize: '9', border: [false, false, false, false], fillColor: 'D3D3D3'}, {text: json.amount + '₹',alignment: 'center',fillColor: 'D3D3D3',bold: 'true',fontSize: '9', border: [false, false, false, false]},],
//       [{text: '', border: [false, false, false, false]},{text: '', border: [false, false, false, false]},{text: '', border: [false, false, false, false]},{text: '', border: [false, false, false, false]},{text: '',alignment: 'center', border: [false, false, false, false]},{text: '',alignment: 'center', border: [false, false, false, false]},],
//       [{text: '', border: [false, false, false, false]},{text: '', border: [false, false, false, false]},{text: '', border: [false, false, false, false]},{text: '', border: [false, false, false, false]},{text: 'CGST AMOUNT',alignment: 'center',bold: 'true',fontSize: '9', border: [false, false, false, false], fillColor: '#D3D3D3'}, {text: json.cgstamt + '₹',alignment: 'center',fillColor: '#D3D3D3',bold: 'true',fontSize: '9', border: [false, false, false, false]},],
//       [{text: '', border: [false, false, false, false]},{text: '', border: [false, false, false, false]},{text: '', border: [false, false, false, false]},{text: '', border: [false, false, false, false]},{text: '',alignment: 'center', border: [false, false, false, false]},{text: '',alignment: 'center', border: [false, false, false, false]},],
//       [{text: '', border: [false, false, false, false]},{text: '', border: [false, false, false, false]},{text: '', border: [false, false, false, false]},{text: '', border: [false, false, false, false]},{text: 'SGST AMOUNT',alignment: 'center',bold: 'true',fontSize: '9', border: [false, false, false, false], fillColor: '#D3D3D3'}, {text: json.sgstamt + '₹',alignment: 'center',fillColor: '#D3D3D3',bold: 'true',fontSize: '9', border: [false, false, false, false]},],
//       [{text: '', border: [false, false, false, false]},{text: '', border: [false, false, false, false]},{text: '', border: [false, false, false, false]},{text: '', border: [false, false, false, false]},{text: '',alignment: 'center', border: [false, false, false, false]},{text: '',alignment: 'center', border: [false, false, false, false]},],
//       // [{text: '', border: [false, false, false, false]},{text: '', border: [false, false, false, false]},{text: '', border: [false, false, false, false]},{text: '', border: [false, false, false, false]},{text: '',alignment: 'center', border: [false, false, false, false]},{text: '',alignment: 'center', border: [false, false, false, false]},],
//       [{text: '', border: [false, false, false, false]},{text: '', border: [false, false, false, false]},{text: '', border: [false, false, false, false]},{text: '', border: [false, false, false, false]},{text: 'IGST AMOUNT',alignment: 'center',bold: 'true',fontSize: '9', border: [false, false, false, false], fillColor: '#D3D3D3'},{text: json.igstamt + '₹',alignment: 'center',fillColor: '#D3D3D3',bold: 'true',fontSize: '9', border: [false, false, false, false]},],
//       [{text: '', border: [false, false, false, false]},{text: '', border: [false, false, false, false]},{text: '', border: [false, false, false, false]},{text: '', border: [false, false, false, false]},{text: '',alignment: 'center', border: [false, false, false, false]},{text: '',alignment: 'center', border: [false, false, false, false]},],
//       [{text: '', border: [false, false, false, false]},{text: '', border: [false, false, false, false]},{text: '', border: [false, false, false, false]},{text: '', border: [false, false, false, false]},{text: 'OTHER CHARGES',alignment: 'center',bold: 'true',fontSize: '9', border: [false, false, false, false], fillColor: '#D3D3D3'}, {text: json.othercharges + '₹',alignment: 'center',fillColor: '#D3D3D3',bold: 'true',fontSize: '9', border: [false, false, false, false]},],
//       [{text: '', border: [false, false, false, false]},{text: '', border: [false, false, false, false]},{text: '', border: [false, false, false, false]},{text: '', border: [false, false, false, false]},{text: '',alignment: 'center', border: [false, false, false, false]},{text: '',alignment: 'center', border: [false, false, false, false]},],
//       [{text: '', border: [false, false, false, false]},{text: '', border: [false, false, false, false]},{text: '', border: [false, false, false, false]},{text: '', border: [false, false, false, false]},{text: '',alignment: 'center', border: [false, false, false, false]},{text: '',alignment: 'center', border: [false, false, false, false]},],
//       [{text: convertNumberToWords(json.total)+'Rupes Only',bold: 'true',color:'white',fontSize: '10', border: [false, false, false, false], fillColor: '#1E90FF',colSpan: 4},'','','',{text: 'TOTAL',alignment: 'center',bold: 'true',fontSize: '10', border: [false, false, false, false], fillColor: '#1E90FF'},{text: json.total + '₹',alignment: 'center',fillColor: '#1E90FF',bold: 'true',fontSize: '10', border: [false, false, false, false]},],
//     ]
//   }, style: 'tablfont1',
// });

// content.push({
//  text: "\n",
//   });

// content.push({ columns: [{text:'\n\nSignature\n\n\nFOR OWNER \n PEST MORTEM (INDIA) PVT.LTD',fontSize: 10,bold:true},{text:'\n\nSignature\n\n\nFOR CUSTOMER  \n '+json.companyname,alignment:'right',fontSize: 10,bold:true}]})

//     dd = {
//         pageSize: 'A4',
//         pageOrientation: 'portrait',
//         //pageMargins: [40, 30, 30, 35],
//         pageMargins: [ 50, 30, 30, 40 ],
//         footer: function (currentPage, pageCount) {
//           return {
//             margin: 10,
//             columns: [{
//               fontSize: 9,
//               text: [{
//                   text: '--------------------------------------------------------------------------' +
//                     '\n',
//                   margin: [0, 20]
//                 },
//                 {
//                   text: '© Pest Mortem (India) PVT. LTD. | PAGE ' + currentPage.toString() + ' of ' + pageCount,
//                 }
//               ],
//               alignment: 'center'
//             }]
//           };

//         },
//         content,
//         styles: {
//           tablfont: {
//             fontSize: 9
//           },
//           tablfont1: {
//             fontSize: 9
//           }
//         }
//       }
//       var win = window.open('', '_blank');
//       pdfMake.createPdf(dd).open({}, win);

//     }
//       }
//     });
//   }







// function salespdf(e){
//   var contractid = $(e).data('contractid');
//   var enquiryid = $(e).data('enquiryid');

//   $.ajax({
//       type: "POST",
//       data: "contractid=" + contractid +"&enquiryid="+enquiryid,
//       url: 'workcontract/salesselect.php',
//       cache: false,
//       success: function (res) {
//         if (res.status == 'success') {
//           var content=[];
//           json=res.json[0];


//           var table1 = {
//     widths: ['auto', '*', '*', '*'],
//     dontBreakRows: true,
//     body: []
//   };

//   // var base64 = 'data:image/jpg;base64,' + $('#base64').val();
//   var base64 = getBase64Image(document.getElementById("imageid"));




//   content.push({
//     layout: 'noBorders',
//     table: {
//       widths: ['*', '*'],
//       body: [
//         ['', {
//           text: "PEST MORTEM (INDIA) PRIVATE LIMITED\n",
//           color: '#FF0000',
//           fontSize: 12,
//           bold: true
//         }],
//         [{
//           image: base64,
//           width: 100,
//           height: 50

//         }, {
//           text: 'G-2, Sunder Tower, T. J. Road, Sewree (West), Mumbai, Maharashtra 400015 \n\nGST NO :27AACCP2255R1ZX',
//           alignment: 'justify',
//           fontSize: 11
//         }],
//       ]
//     }
//   });



//   content.push({
//     layout: 'noBorders',
//     table: {
//       widths: ['*', '*'],
//       body: [
//         [{
//           text: "TO,",
//           fontSize: 11
//         }, ''],

//         [{
//           text: "COMPANY NAME : " + json.companyname,
//           fontSize: 11

//         }, ''],
//         [{
//           text: "COMPANY ADDRESS : " + json.caddress,
//            fontSize: 11
//         }, '']



//       ]
//     }
//   });


// content.push({
// text: "\n",
//  });

// content.push({
// text: "\n",
// });

//   content.push({
//     text: 'Sales:',
//     bold: true,
//     fontSize: 11,
//     background: '#ffff00'
//   });



//   var table1 = {
// 				widths: [15,95,40,40,40,40,40,40,40,50],
// 		                dontBreakRows: true,
//                     fontSize: 20,


// 				body: []
//             };
//           var particular=  [{text: 'SR NO',},{text: 'ITEM',alignment: 'center',},{text: 'Quantity',alignment: 'center',},{text: 'Rate',alignment: 'center',},{text: 'Amount',alignment: 'center',},{text: 'GST',alignment: 'center',},{text: 'CGST AMT',alignment: 'center',},{text: 'SGST AMT',alignment: 'center',},{text: 'IGST AMT',alignment: 'center',},{text: 'NET AMOUNT',alignment: 'center',}];
//            table1['body'].push(particular);     

//            content.push({
//         table: {
//             headerRows: 1,
//             // widths: ['*'],
//             widths: [15,95,40,40,40,40,40,40,40,50],
//             alignment: 'center',

//             body: [
//               [],

//             ]
//         },
//     });
//     var cr=1;
//     var tablejson=json.tablejson;
// for(var i in tablejson){
//    var tb=[];
//    tb.push(cr,{text:tablejson[i].itemname,alignment: 'center'},{text:tablejson[i].qty,alignment: 'center'},{text:tablejson[i].rate,alignment: 'center'},{text:tablejson[i].amount,alignment: 'center'},{text:tablejson[i].gst,alignment: 'center'},{text:tablejson[i].cgstamt,alignment: 'center'},{text:tablejson[i].sgstamt,alignment: 'center'},{text:tablejson[i].igstamt,alignment: 'center'},{text:tablejson[i].total,alignment: 'center'});
//    table1['body'].push(tb);
//    cr++;
// }


// content.push({
//                         text: "",
//                         fontSize: 20,
//                         alignment: 'center'
//                     }, {
//                         style:'tablfont1',
//                         table: table1
//                     });


// content.push({
//                         text: "\n",
//                     });

// content.push({
//   table: {
//     // heights: [20, 20, 20],
//     alignment: 'center',
//     widths: ['*','*','*','*','*','*'],
//     fontSize: 20,



//     body: [
//       [{text: '', border: [false, false, false, false]},{text: '', border: [false, false, false, false]},{text: '', border: [false, false, false, false]},{text: '', border: [false, false, false, false]},{text: 'TAX AMOUNT',alignment: 'center',bold: 'true',fontSize: '9', border: [false, false, false, false], fillColor: '#71bbd4'}, {text: json.amount + '₹',alignment: 'center',fillColor: '#71bbd4',bold: 'true',fontSize: '9', border: [false, false, false, false]},],
//       [{text: '', border: [false, false, false, false]},{text: '', border: [false, false, false, false]},{text: '', border: [false, false, false, false]},{text: '', border: [false, false, false, false]},{text: '',alignment: 'center', border: [false, false, false, false]},{text: '',alignment: 'center', border: [false, false, false, false]},],
//       [{text: '', border: [false, false, false, false]},{text: '', border: [false, false, false, false]},{text: '', border: [false, false, false, false]},{text: '', border: [false, false, false, false]},{text: 'CGST AMOUNT',alignment: 'center',bold: 'true',fontSize: '9', border: [false, false, false, false], fillColor: '#71bbd4'}, {text: json.cgstamt + '₹',alignment: 'center',fillColor: '#71bbd4',bold: 'true',fontSize: '9', border: [false, false, false, false]},],
//       [{text: '', border: [false, false, false, false]},{text: '', border: [false, false, false, false]},{text: '', border: [false, false, false, false]},{text: '', border: [false, false, false, false]},{text: '',alignment: 'center', border: [false, false, false, false]},{text: '',alignment: 'center', border: [false, false, false, false]},],
//       [{text: '', border: [false, false, false, false]},{text: '', border: [false, false, false, false]},{text: '', border: [false, false, false, false]},{text: '', border: [false, false, false, false]},{text: 'SGST AMOUNT',alignment: 'center',bold: 'true',fontSize: '9', border: [false, false, false, false], fillColor: '#71bbd4'}, {text: json.sgstamt + '₹',alignment: 'center',fillColor: '#71bbd4',bold: 'true',fontSize: '9', border: [false, false, false, false]},],
//       [{text: '', border: [false, false, false, false]},{text: '', border: [false, false, false, false]},{text: '', border: [false, false, false, false]},{text: '', border: [false, false, false, false]},{text: '',alignment: 'center', border: [false, false, false, false]},{text: '',alignment: 'center', border: [false, false, false, false]},],
//       [{text: '', border: [false, false, false, false]},{text: '', border: [false, false, false, false]},{text: '', border: [false, false, false, false]},{text: '', border: [false, false, false, false]},{text: '',alignment: 'center', border: [false, false, false, false]},{text: '',alignment: 'center', border: [false, false, false, false]},],
//       [{text: '', border: [false, false, false, false]},{text: '', border: [false, false, false, false]},{text: '', border: [false, false, false, false]},{text: '', border: [false, false, false, false]},{text: 'IGST AMOUNT',alignment: 'center',bold: 'true',fontSize: '9', border: [false, false, false, false], fillColor: '#71bbd4'},{text: json.igstamt + '₹',alignment: 'center',fillColor: '#71bbd4',bold: 'true',fontSize: '9', border: [false, false, false, false]},],
//       [{text: '', border: [false, false, false, false]},{text: '', border: [false, false, false, false]},{text: '', border: [false, false, false, false]},{text: '', border: [false, false, false, false]},{text: '',alignment: 'center', border: [false, false, false, false]},{text: '',alignment: 'center', border: [false, false, false, false]},],
//       [{text: '', border: [false, false, false, false]},{text: '', border: [false, false, false, false]},{text: '', border: [false, false, false, false]},{text: '', border: [false, false, false, false]},{text: 'OTHER CHARGES',alignment: 'center',bold: 'true',fontSize: '9', border: [false, false, false, false], fillColor: '#71bbd4'}, {text: json.othercharges + '₹',alignment: 'center',fillColor: '#71bbd4',bold: 'true',fontSize: '9', border: [false, false, false, false]},],
//       [{text: '', border: [false, false, false, false]},{text: '', border: [false, false, false, false]},{text: '', border: [false, false, false, false]},{text: '', border: [false, false, false, false]},{text: '',alignment: 'center', border: [false, false, false, false]},{text: '',alignment: 'center', border: [false, false, false, false]},],
//       [{text: '', border: [false, false, false, false]},{text: '', border: [false, false, false, false]},{text: '', border: [false, false, false, false]},{text: '', border: [false, false, false, false]},{text: '',alignment: 'center', border: [false, false, false, false]},{text: '',alignment: 'center', border: [false, false, false, false]},],
//       [{text: convertNumberToWords(json.total)+'Rupes Only',bold: 'true',fontSize: '10', border: [false, false, false, false], fillColor: '#399cbd',colSpan: 4},'','','',{text: 'TOTAL',alignment: 'center',bold: 'true',fontSize: '10', border: [false, false, false, false], fillColor: '#44a6c6'},{text: json.total + '₹',alignment: 'center',fillColor: '#44a6c6',bold: 'true',fontSize: '10', border: [false, false, false, false]},],
//     ]
//   }, style: 'tablfont1',
// });


//   content.push({
//     text: '\n'
//   });
//   content.push({
//     text: '\n'
//   });


//   content.push({
//     text: '\n'
//   });

//   // content.push({
//   //   text: 'FOR OWNER,\n\n',
//   //   color: 'black',
//   //   fontSize: 10
//   // });
//   // content.push({
//   //   text: 'PEST MORTEM (INDIA) PVT.LTD\n\n',
//   //   color: 'black',
//   //   fontSize: 10
//   // });

//   // content.push({
//   //   text: '\nAuthorized Signatory\n',
//   //   alignment:'right',
//   //   fontSize: 14
//   // });

//   content.push({ columns: [{text:'\n\nSignature\n\n\nFOR OWNER \n PEST MORTEM (INDIA) PVT.LTD',fontSize: 9,bold:true},{text:'\n\nSignature\n\n\nFOR PURCHASER  \n '+json.companyname,alignment:'right',fontSize: 9,bold:true}]})
//   dd = {
//     pageSize: 'A4',
//     pageOrientation: 'portrait',
//     pageMargins: [40, 30, 30, 35],
//     footer: function (currentPage, pageCount) {
//       return {
//         margin: 10,
//         columns: [{
//           fontSize: 8,
//           text: [{
//               text: '--------------------------------------------------------------------------' +
//                 '\n',
//               margin: [0, 20]
//             },
//             {
//               text: '© Pest Mortem (India) PVT. LTD | PAGE ' + currentPage.toString() + ' of ' + pageCount,
//             }
//           ],
//           alignment: 'center'
//         }]
//       };

//     },
//     content,
//     styles: {
//       tablfont: {
//         fontSize: 7
//       },
//       tablfont1: {
//         fontSize: 9
//       }
//     }
//   }
//   var win = window.open('', '_blank');
//   pdfMake.createPdf(dd).open({}, win);

//         }
//       }
//   });  
// }

function convertNumberToWords(amount) {
  var words = new Array();
  words[0] = '';
  words[1] = 'One';
  words[2] = 'Two';
  words[3] = 'Three';
  words[4] = 'Four';
  words[5] = 'Five';
  words[6] = 'Six';
  words[7] = 'Seven';
  words[8] = 'Eight';
  words[9] = 'Nine';
  words[10] = 'Ten';
  words[11] = 'Eleven';
  words[12] = 'Twelve';
  words[13] = 'Thirteen';
  words[14] = 'Fourteen';
  words[15] = 'Fifteen';
  words[16] = 'Sixteen';
  words[17] = 'Seventeen';
  words[18] = 'Eighteen';
  words[19] = 'Nineteen';
  words[20] = 'Twenty';
  words[30] = 'Thirty';
  words[40] = 'Forty';
  words[50] = 'Fifty';
  words[60] = 'Sixty';
  words[70] = 'Seventy';
  words[80] = 'Eighty';
  words[90] = 'Ninety';
  amount = amount.toString();
  var atemp = amount.split(".");
  var number = atemp[0].split(",").join("");
  var n_length = number.length;
  var words_string = "";
  if (n_length <= 9) {
    var n_array = new Array(0, 0, 0, 0, 0, 0, 0, 0, 0);
    var received_n_array = new Array();
    for (var i = 0; i < n_length; i++) {
      received_n_array[i] = number.substr(i, 1);
    }
    for (var i = 9 - n_length, j = 0; i < 9; i++, j++) {
      n_array[i] = received_n_array[j];
    }
    for (var i = 0, j = 1; i < 9; i++, j++) {
      if (i == 0 || i == 2 || i == 4 || i == 7) {
        if (n_array[i] == 1) {
          n_array[j] = 10 + parseInt(n_array[j]);
          n_array[i] = 0;
        }
      }
    }
    value = "";
    for (var i = 0; i < 9; i++) {
      if (i == 0 || i == 2 || i == 4 || i == 7) {
        value = n_array[i] * 10;
      } else {
        value = n_array[i];
      }
      if (value != 0) {
        words_string += words[value] + " ";
      }
      if ((i == 1 && value != 0) || (i == 0 && value != 0 && n_array[i + 1] == 0)) {
        words_string += "Crores ";
      }
      if ((i == 3 && value != 0) || (i == 2 && value != 0 && n_array[i + 1] == 0)) {
        words_string += "Lakhs ";
      }
      if ((i == 5 && value != 0) || (i == 4 && value != 0 && n_array[i + 1] == 0)) {
        words_string += "Thousand ";
      }
      if (i == 6 && value != 0 && (n_array[i + 1] != 0 && n_array[i + 2] != 0)) {
        words_string += "Hundred and ";
      } else if (i == 6 && value != 0) {
        words_string += "Hundred ";
      }
    }
    words_string = words_string.split("  ").join(" ");

  }
  return words_string;
}


</script>