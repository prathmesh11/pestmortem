<?php
    $base = '../../../';
    include($base.'_in/connect.php');
    header('content-type: application/json; charset=utf-8');
    header("access-control-allow-origin: *");
    if(isset($_POST['data'])){
      $con = _connect();
      if (session_status() == PHP_SESSION_NONE) { session_start(); }

      $created_by = $_SESSION['employeeid'];

      $branchid   = $_SESSION['branchid'];

      $data = get_object_vars(json_decode($_POST["data"]));

      $category     = $data['category'];

      if ($category == 'Fumigation') {
        
        $cat = 'FUM';

      } else {

        $cat = 'PC';
    
      }

      $branchshortname = mysqli_fetch_assoc(mysqli_query($con,"SELECT branchshortname x FROM branchmaster WHERE branchid='$branchid'"))['x'];

      $year1 = 0;
      $year2 = 0;

      if ( date('m') > 3 ) {

          $year1 = date('y');

          $year2 = date('y')+1;

      } else {

          $year1 = date('y')-1;

          $year2 = date('y');
      }

      $first = date('Y').'-01-01';

      $last  = date('Y').'-12-31';  

      $invoice_bill = mysqli_fetch_assoc(mysqli_query($con,"SELECT COUNT(invoice_bill) x FROM `billing` WHERE invoice_bill<>'' AND '$today' BETWEEN '$first' AND '$last' AND category='$category' "))['x'];
            
      if($invoice_bill!=''){

        $invoice_bill++;
  
          //$invoice_bill = mysqli_fetch_assoc(mysqli_query($con,"SELECT COUNT(invoice_bill)+1 as x FROM `billing` WHERE invoice_bill<>''"))['x'];
        $invoice_bill = 'PMI/'.$branchshortname.'/'.$cat.'/'.$invoice_bill.'/'.$year1.'-'.$year2;
  
      } else {
  
        $invoice_bill = 'PMI/'.$branchshortname.'/'.$cat.'/1/'.$year1.'-'.$year2;
  
      }
                
      $customername = $data['customerid'];

      $int = (int) filter_var($customername, FILTER_SANITIZE_NUMBER_INT);
      
      if ($int == '') {

        $customerid=$data['customerid'];

      } else {

        $customerid=$int;

      }

      $create=mysqli_query($con,"INSERT INTO billing (branchid,customerid ,invoice_bill ,category ,enquiry_time ,enquiry_by) VALUES ('$branchid','$customerid' ,'$invoice_bill' ,'$category' ,'$CURRENT_MILLIS' ,'$created_by')");

      if($create){

          echo '{"status":"success"}';

      } else {

          echo '{"status":"falid"}';

      }
        
      _close($con);

    } else {

        echo '{"status":"falid"}';

     }
?>