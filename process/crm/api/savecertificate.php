<?php
    $base='../../../';
    include($base.'_in/connect.php');
    header('content-type: application/json; charset=utf-8');
    header("access-control-allow-origin: *");
    if(isset($_POST['data']) && isset($_POST['data1'])){
      $con=_connect();
      $data=get_object_vars(json_decode($_POST["data"]));
        $copy=$_POST['copy'];
      if (session_status() == PHP_SESSION_NONE) { session_start(); }
      $created_by = $_SESSION['employeeid'];
      $branchid = $_SESSION['branchid'];
                $certid=$data['certid'];
                $country=$data['country'];
                $certificateno=$data['certificateno'];
                $enquiryid=$data['enquiryid'];
                $fumdate=$data['fumdate'];
                $fumdate1=$data['fumdate1'];
                $fumplace=$data['fumplace'];
                $fumdosage=$data['fumdosage'];
                $humidity=$data['humidity'];
                $fumdurationdays=$data['fumdurationdays'];
                $fumdurationhrs=$data['fumdurationhrs'];
                $fumperformed=$data['fumperformed'];
                
                $issuedate=$data['issuedate'];
                if($issuedate==""){
                    $issuedate = date("Y-m-d");
                }
                $mintemp=$data['mintemp'];
                $containerno1=$data['containerno1'];
                $containerno2=$data['containerno2'];
                $containerno3=$data['containerno3'];

               // $exporter=  nl2br(htmlentities($data['exporter'], ENT_QUOTES, 'UTF-8')); 
                $exporter1 = $data['exporter1'];
                $exporter2 = $data['exporter2'];
                $exporter3 = $data['exporter3'];
                $exporter4 = $data['exporter4'];


                $consignee1=$data['consignee1'];
                $consignee2=$data['consignee2'];
                $consignee3=$data['consignee3'];
                
                $notifiedparty1=$data['notifiedparty1'];
                $notifiedparty2=$data['notifiedparty2'];
                $notifiedparty3=$data['notifiedparty3'];
                
                $cargo1=$data['cargo1'];
                $cargo2=$data['cargo2'];
                $cargo3=$data['cargo3'];
                

                $consignee4 = $data['consignee4'];

                $qty=$data['qty'];
                $qty1=$data['qty1'];
                $qty2=$data['qty2'];
                $description=$data['description'];
                $description1=$data['description1'];
                $description2=$data['description2'];

                $vesselname=$data['vesselname'];
                $loading=$data['loading'];
                $shipingmark=$data['shipingmark'];
                $shipingmark1=$data['shipingmark1'];
                $entryport=$data['entryport'];
                $portofdischarge=$data['portofdischarge'];
                
                $destinationcon = $data['destinationcon'];

                //ALP new
                $alpitemqty=$data['alpitemqty'];
                $alpcommodity=$data['alpcommodity'];
                $containerno4=$data['containerno4'];

                //MBR
                $conpressure = $data['conpressure'];
                $confreeair = $data['confreeair'];
                $ventilation = $data['ventilation'];
                $enclosure = $data['enclosure'];
                $enclosure1 = $data['enclosure1'];
                
                $commodify = $data['commodify'];
                $commodify1 = $data['commodify1'];
                $plasticwrappingused = $data['plasticwrappingused'];
                $fumigated = $data['fumigated'];
                $plasticwrapping = $data['plasticwrapping'];
                $thick = $data['thick'];
                $dimension = $data['dimension'];
                //new
                $importExport=$data['importExport'];
                $mbrcommodity=$data['mbrcommodity'];
                $pressureinsec=$data['pressureinsec'];
                $mbrexportcountry=$data['mbrexportcountry'];

                //AFAS
                $fumtarget = $data['fumtarget'];
                $consignment = $data['consignment'];
                $consignment2 = $data['consignment2'];
                $countryorg = $data['countryorg'];
                $daffdosage = $data['daffdosage'];
                $fumconducted = $data['fumconducted'];
                $targetfum = $data['targetfum'];
                $additional=$data['additional'];
                $additional1=$data['additional1'];
                $additional2=$data['additional2'];
                $certType=$_POST["data1"];
                $afasvessel=$data['afasvessel'];
                $create=false;
                if($copy==""){
                    $query="UPDATE certificatemaster SET afasvessel='$afasvessel', mbrexportcountry='$mbrexportcountry', pressureinsec='$pressureinsec', mbrcommodity='$mbrcommodity', importExport='$importExport', alpitemqty='$alpitemqty',alpcommodity='$alpcommodity', branchid='$branchid',commodify1='$commodify1',qty2='$qty2',consignee4='$consignee4',exporter4='$exporter4',certificateno='$certificateno',country='$country',fumdate='$fumdate',fumdate1='$fumdate1',fumplace='$fumplace' ,fumdosage='$fumdosage',fumperformed ='$fumperformed',humidity='$humidity' ,fumdurationdays='$fumdurationdays',fumdurationhrs='$fumdurationhrs',issuedate='$issuedate',mintemp='$mintemp',containerno1='$containerno1',containerno2='$containerno2',containerno3='$containerno3',containerno4='$containerno4',destinationcon='$destinationcon',exporter1='$exporter1',exporter2='$exporter2',exporter3='$exporter3',consignee1='$consignee1',consignee2='$consignee2',consignee3='$consignee3',notifiedparty1='$notifiedparty1',notifiedparty2='$notifiedparty2',notifiedparty3='$notifiedparty3',cargo1='$cargo1',cargo2='$cargo2',cargo3='$cargo3',qty='$qty',qty1='$qty1',description='$description',description1='$description1',description2='$description2',vesselname='$vesselname',loading='$loading',shipingmark='$shipingmark',shipingmark1='$shipingmark1',entryport='$entryport',portofdischarge='$portofdischarge',conpressure='$conpressure',confreeair='$confreeair',ventilation='$ventilation',enclosure='$enclosure',enclosure1='$enclosure1',commodify='$commodify',plasticwrappingused='$plasticwrappingused',fumigated='$fumigated',plasticwrapping='$plasticwrapping',thick='$thick',dimension='$dimension',fumtarget='$fumtarget',consignment='$consignment',consignment2='$consignment2',countryorg='$countryorg',daffdosage='$daffdosage',fumconducted='$fumconducted',targetfum='$targetfum',additional='$additional',additional1='$additional1',additional2='$additional2',created='Y'  WHERE id = '$certid' ";
                    $create=mysqli_query($con,$query);
                    if($create){
                        echo '{"status":"success"}';
                    }else{
                        echo '{"status":"falid"}';
                    }
                    
                }else{
                    $srNo=(mysqli_fetch_assoc(mysqli_query($con,"SELECT MAX(srNo) x FROM `certificatemaster` WHERE certType='$certType'"))['x']);
                    $srNo=$srNo+1;
                    $query="INSERT INTO certificatemaster (srNo,mbrexportcountry,pressureinsec,mbrcommodity,importExport,alpitemqty,alpcommodity,branchid,exporter4,commodify1,qty2,consignee4,certificateno,country,certType,fumdate,fumdate1,fumplace,fumdosage,fumperformed,humidity,fumdurationdays,fumdurationhrs,issuedate,mintemp,containerno1,containerno2,containerno3,containerno4,destinationcon,exporter1,exporter2,exporter3,consignee1,consignee2,consignee3,notifiedparty1,notifiedparty2,notifiedparty3,cargo1,cargo2,cargo3,qty,qty1,description,description1,description2,vesselname,loading,shipingmark,shipingmark1,entryport,portofdischarge,conpressure,confreeair,ventilation,enclosure,enclosure1,commodify,plasticwrappingused,fumigated,plasticwrapping,thick,dimension,fumtarget,consignment,consignment2,afasvessel,countryorg,daffdosage,fumconducted,targetfum,additional,additional1,additional2,created,created_by) VALUES('$srNo','$mbrexportcountry','$pressureinsec','$mbrcommodity','$importExport','$alpitemqty','$alpcommodity','$branchid','$exporter4','$commodify1','$qty2','$consignee4','$certificateno','$country','$certType','$fumdate','$fumdate1','$fumplace' ,'$fumdosage', '$fumperformed','$humidity' ,'$fumdurationdays','$fumdurationhrs','$issuedate','$mintemp','$containerno1','$containerno2','$containerno3','$containerno4','$destinationcon','$exporter1','$exporter2','$exporter3','$consignee1','$consignee2','$consignee3','$notifiedparty1','$notifiedparty2','$notifiedparty3','$cargo1','$cargo2','$cargo3','$qty','$qty1','$description','$description1','$description2','$vesselname','$loading','$shipingmark','$shipingmark1','$entryport','$portofdischarge','$conpressure','$confreeair','$ventilation','$enclosure','$enclosure1','$commodify','$plasticwrappingused','$fumigated','$plasticwrapping','$thick','$dimension','$fumtarget','$consignment','$consignment2','$afasvessel','$countryorg','$daffdosage','$fumconducted','$targetfum','$additional','$additional1','$additional2','Y','$created_by')";
                    $create=mysqli_query($con,$query);
                    $create2=mysqli_query($con,"UPDATE certificatemaster SET confirm='' WHERE srNo = '$srNo' ");
                    if($create){
                        echo '{"status":"success"}';
                    }else{
                        echo '{"status":"falid"}';
                    }
                }
                       
    
              
            _close($con);
     }else{
        echo '{"status":"falid"}';
     }
?>