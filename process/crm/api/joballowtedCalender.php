<?php
    
    $base='../../../';
    
    include($base.'_in/connect.php');
    
    header('content-type: application/json; charset=utf-8');
    
    header("access-control-allow-origin: *");
    
    if(isset($_POST['branchid'])) {
    
        $con                  = _connect();
        
        $branchid             = $_POST['branchid'];

        $backgroundColorArray = array("#f56954", "#f39c12", "#0073b7", "#00c0ef","#00a65a","#3c8dbc");
       
        $borderColorArray     = array("#f56954", "#f39c12", "#0073b7", "#00c0ef","#00a65a","#3c8dbc");

        $arrsize              = count($backgroundColorArray);

        $i                    = 0;

        $result               = mysqli_query($con,"SELECT * FROM workcontractservice WHERE branchid='$branchid' AND shedule_time<>0 ORDER BY servicedate ASC");

        while($rows = mysqli_fetch_assoc($result)){

            $contractid   = $rows['contractid'];

            $category     = mysqli_fetch_assoc(mysqli_query($con,"SELECT category x FROM crmmaster WHERE contractid='$contractid'"))['x'];

            if($i > $arrsize - 1) $i = 0;

            $shedulestart_DAY   = date("d", $rows['shedulestart']/1000);
            
            $shedulestart_MONTH = date("m", $rows['shedulestart']/1000);
            
            $shedulestart_YEAR  = date("Y", $rows['shedulestart']/1000);
            
            $sheduleend_DAY     = date("d", $rows['sheduleend']/1000) + 01;
            
            $sheduleend_MONTH   = date("m", $rows['sheduleend']/1000);
            
            $sheduleend_YEAR    = date("Y", $rows['sheduleend']/1000);

            $startdate          = $shedulestart_YEAR .', '.$shedulestart_MONTH.', '.$shedulestart_DAY;

            $enddate            = $sheduleend_YEAR .', '.$sheduleend_MONTH.', '.$sheduleend_DAY;

            $customername       = mysqli_fetch_assoc(mysqli_query($con,"SELECT customername x FROM customermaster INNER JOIN crmmaster ON customermaster.customerid = crmmaster.customerid and crmmaster.contractid = '$contractid'"))['x'];

            $url                = '/process/crm/job-allotment1.php?serviceid='.$rows['serviceid'].'&servicedatetimestap='.$rows['servicedate'].'&servicedate='.date("d F, Y",$rows['servicedate']/1000).'&category='.$category.'&contractid='.$rows['contractid'].'&joballotstatus='.$rows['joballotstatus'].'&allotTime='.$rows['allot_time'].'&serviceName='.$rows['servicename'];

           // $url                = "data-serviceid='".$rows['serviceid']."' data-servicedatetimestap='".$rows['servicedate']."' data-servicedate='".date('d F, Y',$rows['servicedate']/1000)."' data-category='".$category."' data-contractid='".$rows['contractid']."' data-joballotstatus='".$rows['joballotstatus']."' data-toggle='modal' data-target='#materialIssue' onclick ='openAndFillData(this)'";

            $json.= ',{"title":"'.$customername.'","startdate":"'.$startdate.'","enddate":"'.$enddate.'","backgroundColor":"'.$backgroundColorArray[$i].'","borderColor":"'.$borderColorArray[$i].'","allDay" : true,"url":"'.$url.'"}';

            $i++;
            
        }

        $json = substr($json,1);
      
        $json = '['.$json.']';
    
        if ($result) {

            echo '{"status":"success","json":'.$json.'}';   
    
        } else {
    
            echo '{"status":"falid1"}';
    
        }
    
        _close($con);
    
    } else {
    
        echo '{"status":"falid"}';
     
    }

?>