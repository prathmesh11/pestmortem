<?php

    $base = '../../../';

    include($base.'_in/connect.php');

    header('content-type: application/json; charset=utf-8');

    header("access-control-allow-origin: *");

    if(isset($_POST['itemId']) && isset($_POST['branchid']) && isset($_POST['employeeId']) && isset($_POST['contractid']) && isset($_POST['serviceid'])) {

        $con           = _connect();

        $itemId        = $_POST['itemId'];

        $branchid      = $_POST['branchid'];

        $employeeid    = $_POST['employeeId'];

        $contractid    = $_POST['contractid'];

        $serviceid     = $_POST['serviceid'];        

        $total         = mysqli_fetch_assoc(mysqli_query($con,"SELECT allot_qty x FROM `job_allotment` WHERE itemid='$itemId' AND branchid='$branchid' AND employeeid = '$employeeid' AND contractid = '$contractid' AND service_id = '$serviceid'"))['x'];
        
        $employeestock = mysqli_fetch_assoc(mysqli_query($con,"SELECT balance_qty y FROM `employeestock` WHERE itemcode='$itemId' AND branchid='$branchid' AND employeeid = '$employeeid' ORDER BY id DESC LIMIT 1 "))['y'];

        $stockQty      = mysqli_fetch_assoc(mysqli_query($con,"SELECT total z FROM `livestockmaster` WHERE itemcode='$itemId' AND branchid='$branchid' ORDER BY id DESC LIMIT 1 "))['z'];

        if($total){

            echo '{"status":"success","total":"'.$total.'","employeestock":"'.$employeestock.'","stockQty":"'.$stockQty.'"}';
        
        } else {

            echo '{"status":"falid1"}';

        }
        
        _close($con);

    } else {

        echo '{"status":"falid"}';

    }
?>