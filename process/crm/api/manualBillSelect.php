<?php

    $base = '../../../';

    include($base.'_in/connect.php');

    header('content-type: application/json; charset=utf-8');

    header("access-control-allow-origin: *");

    if (isset($_POST['invoice_bill'])) {

        $con=_connect();
        
        $branchId       = $_POST["branchId"];

        if ($branchId=='') {

            if (session_status() == PHP_SESSION_NONE) { session_start(); }

            $branchId  = $_SESSION["branchid"];
            
        } else {

            $branchId = $_POST["branchId"];

        }

        $invoice_bill   = $_POST["invoice_bill"];

        $select = mysqli_fetch_assoc(mysqli_query($con,"SELECT * FROM billing WHERE branchid='$branchId' AND invoice_bill='$invoice_bill'"));
        
        if($select){

            $address   = get_object_vars(json_decode(mysqli_fetch_assoc(mysqli_query($con,"SELECT branchjson x FROM `branchmaster` WHERE branchid='$branchId'"))['x']))['address'];      
            $gstnumber = get_object_vars(json_decode(mysqli_fetch_assoc(mysqli_query($con,"SELECT branchjson x FROM `branchmaster` WHERE branchid='$branchId'"))['x']))['gstnumber'];
      

            $customer_json = json_decode($select['customer_json']);

            $issuedate       = get_object_vars($customer_json[0])['issuedate'];
            $custName        = get_object_vars($customer_json[0])['custName'];
            $custAddress     = get_object_vars($customer_json[0])['custAddress'];
            $custGst         = get_object_vars($customer_json[0])['custGst'];
            $custEmail       = get_object_vars($customer_json[0])['custEmail'];
            $custmobile      = get_object_vars($customer_json[0])['custmobile'];
            $premisesName    = get_object_vars($customer_json[0])['premisesName'];
            $premisesAddress = get_object_vars($customer_json[0])['premisesAddress'];
            $premisesGst     = get_object_vars($customer_json[0])['premisesGst'];
            $premisesEmail   = get_object_vars($customer_json[0])['premisesEmail'];
            $premisesmobile  = get_object_vars($customer_json[0])['premisesmobile'];
            $todate          = get_object_vars($customer_json[0])['todate'];
            $fromdate        = get_object_vars($customer_json[0])['fromdate'];
            $modOfPayment    = get_object_vars($customer_json[0])['modOfPayment'];
            $addDeclaration  = get_object_vars($customer_json[0])['addDeclaration'];
            $bankName        = get_object_vars($customer_json[0])['bankName'];
            $branchName      = get_object_vars($customer_json[0])['branchName'];
            $ifscCode        = get_object_vars($customer_json[0])['ifscCode'];
            $accountNo       = get_object_vars($customer_json[0])['accountNo'];



            $json = '{"service_json":'.$select['service_json'].',"machine_json":'.$select['machine_json'].',"amount":"'.$select['total_amount'].'","cgstPer":"'.$select['cgstper'].'","cgstAmt":"'.$select['cgstamt'].'","sgstPer":"'.$select['sgstper'].'","sgstAmt":"'.$select['sgstamt'].'","igstPer":"'.$select['igstper'].'","igstAmt":"'.$select['igstamt'].'","netAmount":"'.$select['contractamt'].'","issuedate":"'.$issuedate.'","custName":"'.$custName.'","custAddress":"'.$custAddress.'","custGst":"'.$custGst.'","custEmail":"'.$custEmail.'","custmobile":"'.$custmobile.'","premisesName":"'.$premisesName.'","premisesAddress":"'.$premisesAddress.'","premisesGst":"'.$premisesGst.'","premisesEmail":"'.$premisesEmail.'","premisesmobile":"'.$premisesmobile.'","todate":"'.$todate.'","fromdate":"'.$fromdate.'","modOfPayment":"'.$modOfPayment.'","addDeclaration":"'.$addDeclaration.'","bankName":"'.$bankName.'","branchName":"'.$branchName.'","ifscCode":"'.$ifscCode.'","accountNo":"'.$accountNo.'","address":"'.$address.'","branchgstnumber":"'.$gstnumber.'"}'; 

            echo '{"status":"success","json":['.$json.']}';   

        } else {

            echo '{"status":"falid1"}';

        }


        _close($con);

    } else {

        echo '{"status":"falid"}';
        
    }
?>