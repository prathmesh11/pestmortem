<?php

    $base='../../../';
    include($base.'_in/connect.php');
    header('content-type: application/json; charset=utf-8');
    header("access-control-allow-origin: *");
    if(isset($_POST['contractid'])){
      $con=_connect();
      $contractid=_clean($con,$_POST['contractid']);
      $contract=mysqli_fetch_assoc(mysqli_query($con,"SELECT * FROM `workcontract` WHERE contractid='$contractid'"));
     
     
      $ftype=$contract['ftype'];
      $ftype2=$contract['ftype2'];
      $fname=$contract['fname'];
      $termofpayment=$contract['termofpayment'];
      $nameofexporter=$contract['nameofexporter'];
      $addressofexporter=$contract['addressofexporter'];
      $billingparty=$contract['billingparty'];
      $billqty = $contract['billqty'];
      
      $enquiryid=$contract['enquiryid'];
      $branchid=$contract['branchid'];
      $address=get_object_vars(json_decode(mysqli_fetch_assoc(mysqli_query($con,"SELECT branchjson x FROM `branchmaster` WHERE branchid='$branchid'"))['x']))['address'];      
      $gstnumber=get_object_vars(json_decode(mysqli_fetch_assoc(mysqli_query($con,"SELECT branchjson x FROM `branchmaster` WHERE branchid='$branchid'"))['x']))['gstnumber'];
      
      $bankname=get_object_vars(json_decode(mysqli_fetch_assoc(mysqli_query($con,"SELECT branchjson x FROM `branchmaster` WHERE branchid='$branchid'"))['x']))['bankname'];
      $bankbranchname=get_object_vars(json_decode(mysqli_fetch_assoc(mysqli_query($con,"SELECT branchjson x FROM `branchmaster` WHERE branchid='$branchid'"))['x']))['bankbranchname'];
      $bankaddress=get_object_vars(json_decode(mysqli_fetch_assoc(mysqli_query($con,"SELECT branchjson x FROM `branchmaster` WHERE branchid='$branchid'"))['x']))['bankaddress'];
      $ifsccode=get_object_vars(json_decode(mysqli_fetch_assoc(mysqli_query($con,"SELECT branchjson x FROM `branchmaster` WHERE branchid='$branchid'"))['x']))['ifsccode'];
      $accountno=get_object_vars(json_decode(mysqli_fetch_assoc(mysqli_query($con,"SELECT branchjson x FROM `branchmaster` WHERE branchid='$branchid'"))['x']))['accountno'];
      

      $cifo=mysqli_fetch_assoc(mysqli_query($con,"SELECT * FROM customermaster WHERE customerid=(SELECT customerid FROM crmmaster WHERE enquiryid='$enquiryid')"));
      $customerjson=$cifo['customerjson'];
      $contactjson=$cifo['contactjson'];
      $custgstnumber=$cifo['gstnumber'];


      $category=  mysqli_fetch_assoc(mysqli_query($con,"SELECT category x FROM `crmmaster` WHERE branchid='$branchid' AND contractid='$contractid' "))['x'];
    


      $branchname=(mysqli_fetch_assoc(mysqli_query($con,"SELECT branchname x FROM `branchmaster` WHERE branchid='$branchid'"))['x']);      
    
      $cname=get_object_vars(json_decode($contactjson)[0])['name'];
      $mobile=get_object_vars(json_decode($contactjson)[0])['mobile'];
      $landline=get_object_vars(json_decode($contactjson)[0])['landline'];
      $email=get_object_vars(json_decode($contactjson)[0])['email'];

      $companyname=get_object_vars(json_decode($customerjson))['customername'];
      $companyaddress=get_object_vars(json_decode($customerjson))['address'];
      
      $contractdate=date("d-m-Y",$contract['contractdate']/1000); 
      $contractstart=date("d-m-Y",$contract['contractstart']/1000); 
      $contractend=date("d-m-Y",$contract['contractend']/1000); 
      $amount=$contract['amount'];
      $cgstper=$contract['cgstper'];
      $sgstper=$contract['sgstper'];
      $igstper=$contract['igstper'];
      $cgstamt=$contract['cgstamt'];
      $sgstamt=$contract['sgstamt'];
      $igstamt=$contract['igstamt'];
      
      $othercharges=$contract['othercharges'];
      $servicejson='';
      $result=mysqli_query($con,"SELECT servicename FROM servicemaster WHERE category='$category' ORDER BY id ASC" );
                     while($rows = mysqli_fetch_assoc($result)){
                        $servicename=$rows['servicename'];
                        $counter=(mysqli_fetch_assoc(mysqli_query($con,"SELECT count(id) x FROM `workcontractservice` WHERE contractid='$contractid' AND servicename='$servicename'"))['x']);
                        if($counter!=0){
                           $yesno = 'YES';
                           $frq   = $counter;
                           $servicejson.=',{"servicename":"'.$servicename.'","frequency":"'.$frq.'","yesno":"'.$yesno.'"}';
         
                       }
                  //   if($counter!=0){
                  //    $yesno='Yes';
                  //    $frq=$counter;
                  //   }else{
                  //    $yesno='No';
                  //    $frq='Nil';
                  //   }
                  //       $servicejson.=',{"servicename":"'.$servicename.'","frequency":"'.$frq.'","yesno":"'.$yesno.'"}';
                  }

                  $servicejson=substr($servicejson,1);
                  $servicejson='['.$servicejson.']';
                  
      $totalcontractamt=$contract['totalcontractamt'];
      $servicelocation=$contract['servicelocation'];
      $additinal=$contract['additinal'];
      $paymentby=$contract['paymentby'];
      $enquipmentjson=$contract['enquipmentjson'];
      if($enquipmentjson==''){
         $enquipmentjson='[]';
      }
      
      $json='[{"category":"'.$category.'","billqty":"'.$billqty.'","bankname":"'.$bankname.'","bankbranchname":"'.$bankbranchname.'","bankaddress":"'.$bankaddress.'","ifsccode":"'.$ifsccode.'","accountno":"'.$accountno.'","ftype":"'.$ftype.'","ftype2":"'.$ftype2.'","fname":"'.$fname.'","termofpayment":"'.$termofpayment.'","nameofexporter":"'.$nameofexporter.'","addressofexporter":"'.$addressofexporter.'","billingparty":"'.$billingparty.'","branchname":"'.$branchname.'","cname":"'.$cname.'","custgstnumber":"'.$custgstnumber.'","mobile":"'.$mobile.'","landline":"'.$landline.'","email":"'.$email.'","companyname":"'.$companyname.'","companyaddress":"'.$companyaddress.'","address":"'.$address.'","branchgstnumber":"'.$gstnumber.'","contractid":"'.$contractid.'","contractdate":"'.$contractdate.'","contractstart":"'.$contractstart.'","contractend":"'.$contractend.'","amount":"'.$amount.'","cgstper":"'.$cgstper.'","sgstper":"'.$sgstper.'","igstper":"'.$igstper.'","cgstamt":"'.$cgstamt.'","sgstamt":"'.$sgstamt.'","igstamt":"'.$igstamt.'","othercharges":"'.$othercharges.'","totalcontractamt":"'.$totalcontractamt.'","inword":"'._RSTOWORD($totalcontractamt).'","servicelocation":'.$servicelocation.',"additinal":"'.$additinal.'","paymentby":"'.$paymentby.'","enquipmentjson":'.$enquipmentjson.',"servicejson":'.$servicejson.'}]';
      echo '{"status":"success","json":'.$json.'}';

      _close($con);
     }else{
        echo '{"status":"falid"}';
     }
?>
