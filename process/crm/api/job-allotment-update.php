<?php
    
    $base='../../../';
    
    include($base.'_in/connect.php');
    
    header('content-type: application/json; charset=utf-8');
    
    header("access-control-allow-origin: *");
    
    if(isset($_POST['data']) && isset($_POST['itemDetails'])) {
    
        $con = _connect();
    
        if (session_status()==PHP_SESSION_NONE) { session_start(); }
    
        $employeeid     = $_SESSION['employeeid'];
    
        $data           = get_object_vars(json_decode($_POST['data']));
    
        $branchid       = $data['branchid'];
    
        $contractid     = $data['contractid'];
    
        $startdate      = $data['startdate'];

        $enddate        = $data['enddate'];

        $serviceAddress = $data['serviceAddress'];

        $latitude       = $data['latitude'];

        $longitude      = $data['longitude'];

        $serviceid      = $data['serviceid'];

        $itemDetails    = json_decode($_POST['itemDetails']);

        foreach ($itemDetails as $i) {

            $employeeid   = get_object_vars($i)['employeeid'];

            $employeename = get_object_vars($i)['employeename'];
            
            $itemid       = get_object_vars($i)['itemid'];
            
            $itemname     = get_object_vars($i)['itemname'];
            
            $outqty       = get_object_vars($i)['outqty'];

            $select       = mysqli_fetch_assoc(mysqli_query($con,"SELECT * FROM job_allotment WHERE branchid='$branchid' AND contractid='$contractid' AND employeeid = '$employeeid' AND itemid = '$itemid' AND allot_qty = '$outqty' AND service_id = '$serviceid' "));

            if (!$select) {
                
                $select        = mysqli_fetch_assoc(mysqli_query($con,"SELECT * FROM `employeestock` WHERE itemcode='$itemid' AND branchid='$branchid' AND employeeid = '$employeeid' ORDER BY id DESC LIMIT 1"));
            
                if($select){
    
                    $in_qty       = $select['in_qty'];
                   
                    $out_qty      = $select['out_qty'] + $outqty ;
                   
                    $balance_qty  = $in_qty - $out_qty;
                 
                    $create       = mysqli_query($con,"INSERT into employeestock (branchid, employeeid, itemcode, in_qty, out_qty, balance_qty, created_by, created_time) Values  ('$branchid', '$employeeid' , '$itemid', '$in_qty', '$out_qty', '$balance_qty', '$created_by', '$CURRENT_MILLIS')");
    
                }

            }
            
            $delete = mysqli_query($con, "DELETE FROM job_allotment WHERE branchid = '$branchid' AND contractid = '$contractid' AND employeeid = '$employeeid' AND itemid = '$itemid' AND service_id = '$serviceid' ");

            $insert = mysqli_query($con, "INSERT INTO job_allotment (branchid, contractid, service_id, serviceAddress, latitude, longitude, startdate, enddate, employeeid, employeename, itemid, itemname, allot_qty, created_by, created_time) VALUES ('$branchid', '$contractid', '$serviceid', '$serviceAddress', '$latitude', '$longitude', '$startdate', '$enddate', '$employeeid', '$employeename', '$itemid', '$itemname', '$outqty', '$employeeid', '$CURRENT_MILLIS')"); 
   
        }
    
        if($insert){

            mysqli_query ($con,"UPDATE workcontractservice SET serviceAddress='$serviceAddress', latitude='$latitude', longitude='$longitude', shedulestart='$startdate', sheduleend='$enddate', shedule_by='$employeeid', shedule_time='$CURRENT_MILLIS', joballotstatus = 1 WHERE contractid = '$contractid' AND branchid = '$branchid' AND serviceid = '$serviceid'");

            echo '{"status":"success"}';
    
        } else {
    
            echo '{"status":"falid1"}';
    
        }
    
        _close($con);
    
    }else{
    
        echo '{"status":"falid"}';
     
    }
?>