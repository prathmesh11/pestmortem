<?php
    $base='../../../';
    include($base.'_in/connect.php');
    header('content-type: application/json; charset=utf-8');
    header("access-control-allow-origin: *");
    
    $con=_connect();
    $data=get_object_vars(json_decode($_POST["data"]));

    if (session_status() == PHP_SESSION_NONE) { session_start(); }
    $created_by = $_SESSION['employeeid'];
    $branchid   = $_SESSION['branchid'];

    $bill_id = $_POST['id'];

    $check= mysqli_fetch_assoc(mysqli_query($con,"SELECT invoice_bill x FROM `workcontractbill` WHERE billid='$bill_id'"))['x'];
        if($check==''){
            $invoice_bill    = mysqli_fetch_assoc(mysqli_query($con,"SELECT COUNT(invoice_bill)+1 as x FROM `workcontractbill` WHERE invoice_bill<>''"))['x'];
           

            $branchshortname = mysqli_fetch_assoc(mysqli_query($con,"SELECT branchshortname x FROM branchmaster WHERE branchid='$branchid'"))['x'];
            $year1 = 0;
            $year2 = 0;
            if ( date('m') > 3 ) {
                $year1 = date('y');
                $year2 = date('y')+1;
            }else{
                $year1 = date('y')-1;
                $year2 = date('y');
            }
            $invoice_bill='PMI/'.$branchshortname.'/'.$invoice_bill.'/'.$year1.'-'.$year2;
            mysqli_query($con,"UPDATE workcontractbill SET invoice_bill='$invoice_bill' WHERE billid='$bill_id' ");
        }

      $json1='';
      $result    = mysqli_query($con,"SELECT * from workcontractbill WHERE billid = '$bill_id'");
       while($rows = mysqli_fetch_assoc($result)){    

       $billdate      = date("d-m-Y",$rows['billdate']/1000);
       $next_billdate = mysqli_fetch_assoc(mysqli_query($con,"SELECT billdate x from workcontractbill WHERE contractid='$contractid' AND  billdate > '$billdate' LIMIT 1 "))['x'];

       $contractid = $rows['contractid'];
       $billdate1  = $rows['billdate'];
       $billqty = mysqli_fetch_assoc(mysqli_query($con,"SELECT billqty x from workcontract WHERE contractid='$contractid' "))['x'];

       $next_billdate = mysqli_fetch_assoc(mysqli_query($con,"SELECT billdate x from workcontractbill WHERE contractid='$contractid' AND  billdate > '$billdate1' LIMIT 1 "))['x'];
       $totalpaidamt1  = mysqli_fetch_assoc(mysqli_query($con,"SELECT sum(totalpaidamt) x from workcontractbill WHERE contractid='$contractid'"))['x'];
       $due_payment   = 0;

       $totalpaidamt = $rows['totalpaidamt'];
       $cgstper = $rows['cgstper']/$billqty;
       $sgstper = $rows['sgstper']/$billqty;
       $igstper = $rows['igstper']/$billqty;
       $cgstamt = $rows['cgstamt']/$billqty;
       
       $sgstamt = $rows['sgstamt']/$billqty;
       $igstamt = $rows['igstamt']/$billqty;
       $other1 = $rows['other1']/$billqty;
       $discount_amt1  = mysqli_fetch_assoc(mysqli_query($con,"SELECT sum(discount_amt) x from workcontractbill WHERE contractid='$contractid'"))['x'];
       $discount_amt = $rows['discount_amt'];
       $contractamt1  = $rows['contractamt']/$billqty;
       $contractamt  = $rows['contractamt'];
       $due_payment      = $contractamt-($totalpaidamt1+$discount_amt1);
       $total = $contractamt1-($cgstamt+$sgstamt+$igstamt+$other1);
       $paid_time = mysqli_fetch_assoc(mysqli_query($con,"SELECT paid_time x from workcontractbill WHERE contractid='$contractid'  LIMIT 1 "))['x'];
       $grand_total = $contractamt1-$discount_amt;
        $json1= '{"grand_total":"'.number_format($grand_total,2).'","paid_date":"'.date("d-m-Y",$paid_time/1000).'","next_billdate":"'.date("d-m-Y",$next_billdate/1000).'","next_contract_amt":"'.$totalcontractamt.'","paid_amt":"'.$totalpaidamt1.'","due_payment":"'.$due_payment.'","next_totalcontractamt":"'.$contractamt1.'","billid" : "'.$rows['invoice_bill'].'","contractid" : "'.$rows['contractid'].'","billdate" : "'.$billdate.'","billamt" : "'.$rows['billamt'].'","ac" : "'.$rows['ac'].'","qty" : "'.$rows['qty'].'","rate" : "'.$rows['rate'].'","pono" : "'.$rows['pono'].'","cgstper" : "'.$cgstper.'","sgstper" : "'.$sgstper.'","igstper" : "'.$igstper.'","cgstamt" : "'.$cgstamt.'","igstamt" : "'.$igstamt.'","sgstamt" : "'.$sgstamt.'","other1" : "'.number_format($other1,2).'","other2" : "'.$rows['other2'].'","discount_amt" : "'.$discount_amt.'","total":"'.number_format($total,2).'","contractamt" : "'.$rows['contractamt'].'"}';  
          
            echo '{"status":"success","json":'.$json1.'}';


                 
    }

            _close($con);
      
?>