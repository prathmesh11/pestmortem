<?php
    
    $base='../../../';
    
    include($base.'_in/connect.php');
    
    header('content-type: application/json; charset=utf-8');
    
    header("access-control-allow-origin: *");
    
    if(isset($_POST['itemid']) && isset($_POST['employeeid']) && isset($_POST['outqty']) && isset($_POST['branchid']) && isset($_POST['contractid']) && isset($_POST['serviceid']) && isset($_POST['sendto'])) {
    
        $con         = _connect();
        
        $branchid    = $_POST['branchid'];
    
        $contractid  = $_POST['contractid'];
    
        $itemid      = $_POST['itemid'];

        $employeeid  = $_POST['employeeid'];

        $outqtys     = $_POST['outqty'];

        $serviceid   = $_POST['serviceid'];

        $sendto      = $_POST['sendto'];

        $selects     = mysqli_fetch_assoc(mysqli_query($con,"SELECT * FROM `job_allotment` WHERE itemid = '$itemid' AND branchid='$branchid' AND employeeid = '$employeeid' AND contractid = '$contractid' AND service_id = '$serviceid' AND received_qty <> 0 "));

        if ($sendto == 'Branch') {

            $select  = mysqli_fetch_assoc(mysqli_query($con,"SELECT * FROM `livestockmaster` WHERE itemcode='$itemid' AND branchid='$branchid' ORDER BY id DESC LIMIT 1"));

            if($select){

                $opning       = $select['opning'];
            
                $quality      = $select['quality'];
            
                $adjust_plus  = $select['adjust_plus'];
            
                $adjust_minus = $select['adjust_minus'];
            
                $outqty       = $select['outqty'] + $outqtys;
            
                $inqty        = $select['inqty'];
            
                $issue        = $select['issue'];
            
                $total        = ($opning+$quality+$adjust_plus+$inqty)-($issue+$adjust_minus+$outqty);
            
                $update       = mysqli_query($con,"INSERT into livestockmaster (branchid,itemcode,opning,quality,issue,adjust_plus,adjust_minus,outbranch,outqty,inbranch,inqty,total,created_by,created_time) Values ('$branchid','$itemid','$opning','$quality','$issue','$adjust_plus','$adjust_minus','$outbranch','$outqty','$inbranch','$inqty','$total','$created_by','$CURRENT_MILLIS')");

            }
        
        } elseif ($sendto == 'Employee') {

            $select       = mysqli_fetch_assoc(mysqli_query($con,"SELECT * FROM `employeestock` WHERE itemcode='$itemid' AND branchid='$branchid' AND employeeid = '$employeeid' ORDER BY id DESC LIMIT 1"));
        
            if($select){

                $in_qty       = $select['in_qty'];
               
                $out_qty      = $select['out_qty'] + $outqtys;
               
                $balance_qty  = $in_qty - $out_qty;
             
                $create       = mysqli_query($con,"INSERT into employeestock (branchid, employeeid, itemcode, in_qty, out_qty, balance_qty, created_by, created_time) Values  ('$branchid', '$employeeid' , '$itemid', '$in_qty', '$out_qty', '$balance_qty', '$created_by', '$CURRENT_MILLIS')");

            }
            
        }

        if($selects){

            $update = mysqli_query($con, "UPDATE job_allotment SET send_to = '' , received_qty = '' WHERE  branchid = '$branchid' AND contractid = '$contractid' AND service_id = '$serviceid' AND employeeid = '$employeeid' AND itemid = '$itemid'"); 

            echo '{"status":"success"}';
    
        } else {
    
            echo '{"status":"falid1"}';
    
        }
    
        _close($con);
    
    }else{
    
        echo '{"status":"falid"}';
     
    }
?>