<?php
    $base='../../../';
    include($base.'_in/connect.php');
    header('content-type: application/json; charset=utf-8');
    header("access-control-allow-origin: *");

    if(isset($_POST['data1'])){

        $con = _connect();
        if (session_status()==PHP_SESSION_NONE) { session_start(); }
        $created_by      = $_SESSION['employeeid'];
        $branchid        = $_SESSION['branchid'];
        $data            = json_decode($_POST['data1']);
        $branchshortname = mysqli_fetch_assoc(mysqli_query($con,"SELECT branchshortname x FROM branchmaster WHERE branchid='$branchid'"))['x'];
        $year1 = 0;
        $year2 = 0;
        if ( date('m') > 3 ) {
            $year1 = date('y');
            $year2 = date('y')+1;
        } else{
            $year1 = date('y')-1;
            $year2 = date('y');
        }

        foreach($data as $i){
          $billid = get_object_vars($i)['option_billid'];  
          $check  = mysqli_fetch_assoc(mysqli_query($con,"SELECT invoice_bill x FROM `billing` "))['x'];
          if($check!=''){
              $invoice_bill = mysqli_fetch_assoc(mysqli_query($con,"SELECT COUNT(invoice_bill)+1 as x FROM `billing` WHERE invoice_bill<>''"))['x'];
              $invoice_bill = 'PMI/'.$branchshortname.'/'.$invoice_bill.'/'.$year1.'-'.$year2;
            }else {
              $invoice_bill = 'PMI/'.$branchshortname.'/1/'.$year1.'-'.$year2;
            }
              $result = mysqli_query($con,"SELECT * FROM workcontractbill WHERE branchid='$branchid' AND billid='$billid'");
              while ($row = mysqli_fetch_assoc($result)) {
                $customerid   = $row['customerid'];
                $enquiryid    = $row['enquiryid'];
                $contractid   = $row['contractid'];
                $contractdate = mysqli_fetch_assoc(mysqli_query($con,"SELECT contractdate x FROM workcontract WHERE branchid='$branchid' AND contractid='$contractid'"))['x'];
                $billdate     = $row['billdate'];
                $enquiryid    = $row['enquiryid'];
                $billdate     = $row['billdate'];
                $bill_json.=',{"billid":"'.$billid.'","billdate":"'.$billdate.'"}';
    
                $cgstper+= $row['cgstper'];
                $sgstper+= $row['sgstper'];
                $igstper+= $row['igstper'];
                $cgstamt+= $row['cgstamt'];
                $sgstamt+= $row['sgstamt'];
                $igstamt+= $row['igstamt'];
                $discount_amt+= $row['discount_amt'];
                $totalpaidamt+= $row['totalpaidamt'];
                $contractamt+= $row['contractamt'];
    
                $contract_json.=',{"contractid":"'.$contractid.'","contractdate":"'.$contractdate.'"}';
              }
    
         }
         $contract_json = substr($contract_json,1);
         $contract_json = '['.$contract_json.']';
         $bill_json     = substr($bill_json,1);
         $bill_json     = '['.$bill_json.']';
    
         $invoice_bill = mysqli_fetch_assoc(mysqli_query($con,"SELECT invoice_bill x FROM `billing` WHERE bill_json LIKE '%$billid%' "))['x'];
    
         if ($invoice_bill) {
    
           mysqli_query ($con,"UPDATE `billing` SET `contract_json`='$contract_json',`service_json`='[]',`bill_json`='$bill_json',`cgstper`='$cgstper',`sgstper`='$sgstper',`igstper`='$igstper',`cgstamt`='$cgstamt',`sgstamt`='$sgstamt',`igstamt`='$igstamt',`discount_amt`='$discount_amt',`totalpaidamt`='$totalpaidamt',`contractamt`='$contractamt',`created_by`='$created_by',`created_time`='$CURRENT_MILLIS' WHERE `invoice_bill`='$invoice_bill' ");
    
         }else {
    
         mysqli_query($con,"INSERT INTO `billing`(`branchid`, `customerid`, `contract_json`, `service_json`, `bill_json`, `cgstper`, `sgstper`, `igstper`, `cgstamt`, `sgstamt`, `igstamt`, `discount_amt`, `totalpaidamt`, `contractamt`, `created_by`, `created_time`, `invoice_bill`) VALUES ('$branchid','$customerid','$contract_json','[]','$bill_json','$cgstper','$sgstper','$igstper','$cgstamt','$sgstamt','$igstamt','$discount_amt','$totalpaidamt','$contractamt','$created_by','$CURRENT_MILLIS','$invoice_bill')");
         
        }



        $result    = mysqli_query($con,"SELECT * from billing WHERE invoice_bill = '$invoice_bill'");
        while($rows = mysqli_fetch_assoc($result)){    
 
  
        $branchid = $rows['branchid'];
        $branch_info=mysqli_fetch_assoc(mysqli_query($con,"SELECT branchjson,branchname,branchshortname from branchmaster WHERE branchid = '$branchid'"));   

        $branchjson      = $branch_info['branchjson']; 
        $branchname      = $branch_info['branchname']; 
        $branchshortname = $branch_info['branchshortname'];

        $customerid   = $rows['customerid'];
        $cust_info       = mysqli_fetch_assoc(mysqli_query($con,"SELECT customername,gstnumber,customerjson FROM customermaster WHERE customerid='$customerid'"));
        $customername = $cust_info['customername']; 
        $gstno        = $cust_info['gstnumber']; 
        $address      = get_object_vars(json_decode($cust_info['customerjson']))['address'];
        $state        = get_object_vars(json_decode($cust_info['customerjson']))['state'];
        $state        = mysqli_fetch_assoc(mysqli_query($con,"SELECT statename x FROM statemaster WHERE id='$state'"))['x'];

        $totalpaidamt = $rows['totalpaidamt'];
        $cgstper      = $rows['cgstper'];
        $sgstper      = $rows['sgstper'];
        $igstper      = $rows['igstper'];
        $cgstamt      = $rows['cgstamt'];
        $sgstamt      = $rows['sgstamt'];
        $igstamt      = $rows['igstamt'];
        $other1       = $rows['other1'];
        $discount_amt = $rows['discount_amt'];
        $contractamt  = $rows['contractamt'];
        $invoice_bill = $rows['invoice_bill'];
        $contract_json = json_decode($rows['contract_json']);
        foreach($contract_json as $i){
          $contractid   = get_object_vars($i)['contractid'];
          $contractdate = date("d-m-Y",get_object_vars($i)['contractdate']/1000);
          $contract_json1.=',{"contractid":"'.$contractid.'","contractdate":"'.$contractdate.'"}';
        }
              
        $contract_json1=substr($contract_json1,1);
        $contract_json1='['.$contract_json1.']';


        $bill_json = json_decode($rows['bill_json']);
        foreach($bill_json as $i){
          $billid   = get_object_vars($i)['billid'];
          $billdate = date("d-m-Y",get_object_vars($i)['billdate']/1000);
          $bill_json1.=',{"billid":"'.$billid.'","billdate":"'.$billdate.'"}';
        }

      
        $bill_json1=substr($bill_json1,1);
        $bill_json1='['.$bill_json1.']';

         $json1= '{"branchjson":'.$branchjson.',"branchname":"'.$branchname.'","branchshortname":"'.$branchshortname.'","customername":"'.$customername.'","gstno":"'.$gstno.'","address":"'.$address.'","state":"'.$state.'","paid_amt":"'.$totalpaidamt.'","billid" :'.$bill_json1.',"contractid" :'.$contract_json1.',"billamt" : "'.$contractamt.'","cgstper" : "'.$cgstper.'","sgstper" : "'.$sgstper.'","igstper" : "'.$igstper.'","cgstamt" : "'.$cgstamt.'","igstamt" : "'.$igstamt.'","sgstamt" : "'.$sgstamt.'","discount_amt" : "'.$discount_amt.'","contractamt" : "'.$contractamt.'","invoice_bill":"'.$invoice_bill.'"}';  
           
             echo '{"status":"success","json":'.$json1.'}';
 
 
                  
     }


  
     
    }
        _close($con);
?>