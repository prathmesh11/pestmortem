<?php
    
   $base = '../../../';
   
   include($base.'_in/connect.php');
    
   header('content-type: application/json; charset=utf-8');
    
   header("access-control-allow-origin: *");
   
   if(isset($_POST['contractid'])){

      $con            = _connect();
      
      $contractid     = _clean($con,$_POST['contractid']);
      
      $contract       = mysqli_fetch_assoc(mysqli_query($con,"SELECT * FROM `workcontract` WHERE contractid='$contractid'"));
      
      $enquiryid      = $contract['enquiryid'];
      
      $branchid       = $contract['branchid'];
      
      $address        = get_object_vars(json_decode(mysqli_fetch_assoc(mysqli_query($con,"SELECT branchjson x FROM `branchmaster` WHERE branchid='$branchid'"))['x']))['address'];      
      
      $gstnumber      = get_object_vars(json_decode(mysqli_fetch_assoc(mysqli_query($con,"SELECT branchjson x FROM `branchmaster` WHERE branchid='$branchid'"))['x']))['gstnumber'];

      $cifo           = mysqli_fetch_assoc(mysqli_query($con,"SELECT * FROM customermaster WHERE customerid=(SELECT customerid FROM crmmaster WHERE enquiryid='$enquiryid')"));
      
      $customerjson   = $cifo['customerjson'];
      
      $contactjson    = $cifo['contactjson'];
      
      $custgstnumber  = $cifo['gstnumber'];

      $branchname     = (mysqli_fetch_assoc(mysqli_query($con,"SELECT branchname x FROM `branchmaster` WHERE branchid='$branchid'"))['x']);      
    
      $cname          = get_object_vars(json_decode($contactjson)[0])['name'];
      
      $mobile         = get_object_vars(json_decode($contactjson)[0])['mobile'];
      
      $landline       = get_object_vars(json_decode($contactjson)[0])['landline'];
      
      $email          = get_object_vars(json_decode($contactjson)[0])['email'];

      $companyname    = get_object_vars(json_decode($customerjson))['customername'];
      
      $companyaddress = get_object_vars(json_decode($customerjson))['address'];
      
      $contractdate   = date("d-m-Y",$contract['contractdate']/1000); 
      
      $contractstart  = date("d-m-Y",$contract['contractstart']/1000); 
      
      $contractend    = date("d-m-Y",$contract['contractend']/1000); 

      $othercharges   = $contract['othercharges'];
      
      $servicejson    = '';
      
      $result = mysqli_query($con,"SELECT servicename FROM servicemaster ORDER BY id ASC" );

      while($rows = mysqli_fetch_assoc($result)){
                        
         $servicename = $rows['servicename'];
                        
         $counter     = (mysqli_fetch_assoc(mysqli_query($con,"SELECT count(id) x FROM `workcontractservice` WHERE contractid='$contractid' AND servicename='$servicename'"))['x']);
                                
         $servicedate = mysqli_query($con,"SELECT servicedate  FROM `workcontractservice` WHERE contractid='$contractid' AND servicename='$servicename'");
                     
         while($rows = mysqli_fetch_assoc($servicedate)){
                        
            $servicedate1 = date("d-m-Y",$rows['servicedate']/1000);
                        
            $servicejson.=',{"servicename":"'.$servicename.'","servicedate":"'.$servicedate1.'"}';
             
         }
                  
      }
                  
      $servicejson = substr($servicejson,1);
                  
      $servicejson = '['.$servicejson.']';
                  
      $json        = '[{"billingparty":"'.$billingparty.'","branchname":"'.$branchname.'","cname":"'.$cname.'","custgstnumber":"'.$custgstnumber.'","mobile":"'.$mobile.'","landline":"'.$landline.'","email":"'.$email.'","companyname":"'.$companyname.'","companyaddress":"'.$companyaddress.'","address":"'.$address.'","branchgstnumber":"'.$gstnumber.'","contractid":"'.$contractid.'","contractdate":"'.$contractdate.'","contractstart":"'.$contractstart.'","contractend":"'.$contractend.'","servicelocation":"'.$servicelocation.'","servicejson":'.$servicejson.'}]';
      
      echo '{"status":"success","json":'.$json.'}';

      
      _close($con);
     
   } else {
        
      echo '{"status":"falid"}';
     
   }

?>
