<?php
    
    $base='../../../';
    
    include($base.'_in/connect.php');
    
    header('content-type: application/json; charset=utf-8');
    
    header("access-control-allow-origin: *");
    
    if(isset($_POST['data']) && isset($_POST['itemDetails'])) {
    
        $con = _connect();
    
        if (session_status()==PHP_SESSION_NONE) { session_start(); }
    
        $employeeid = $_SESSION['employeeid'];
    
        $data        = get_object_vars(json_decode($_POST['data']));
    
        $branchid    = $data['branchid'];
    
        $contractid  = $data['contractid'];

        $serviceid   = $data['serviceid'];

        $itemDetails = json_decode($_POST['itemDetails']);

        foreach ($itemDetails as $i) {

            $employeeid   = get_object_vars($i)['employeeid'];

            $employeename = get_object_vars($i)['employeename'];
            
            $itemid       = get_object_vars($i)['itemid'];
            
            $itemname     = get_object_vars($i)['itemname'];
            
            $outqty       = get_object_vars($i)['outqty'];

            $sendto       = get_object_vars($i)['sendto'];

            if ($sendto == 'Branch') {

                $select  = mysqli_fetch_assoc(mysqli_query($con,"SELECT * FROM `livestockmaster` WHERE itemcode='$itemid' AND branchid='$branchid' ORDER BY id DESC LIMIT 1"));

                if($select){

                    $opning       = $select['opning'];
                
                    $quality      = $select['quality'];
                
                    $adjust_plus  = $select['adjust_plus'];
                
                    $adjust_minus = $select['adjust_minus'];
                
                    $outqty       = $select['outqty'];
                
                    $inqty        = $select['inqty']  + $outqty;
                
                    $issue        = $select['issue'];
                
                    $total        = ($opning+$quality+$adjust_plus+$inqty)-($issue+$adjust_minus+$outqty);
                
                    $update       = mysqli_query($con,"INSERT into livestockmaster (branchid,itemcode,opning,quality,issue,adjust_plus,adjust_minus,outbranch,outqty,inbranch,inqty,total,created_by,created_time) Values ('$branchid','$itemid','$opning','$quality','$issue','$adjust_plus','$adjust_minus','$outbranch','$outqty','$inbranch','$inqty','$total','$created_by','$CURRENT_MILLIS')");

                }
            
            } elseif ($sendto == 'Employee') {

                $select       = mysqli_fetch_assoc(mysqli_query($con,"SELECT * FROM `employeestock` WHERE itemcode='$itemid' AND branchid='$branchid' AND employeeid = '$employeeid' ORDER BY id DESC LIMIT 1"));
            
                if($select){
    
                    $in_qty       = $select['in_qty'] + $outqty;
                   
                    $out_qty      = $select['out_qty'];
                   
                    $balance_qty  = $in_qty - $out_qty;
                 
                    $create       = mysqli_query($con,"INSERT into employeestock (branchid, employeeid, itemcode, in_qty, out_qty, balance_qty, created_by, created_time) Values  ('$branchid', '$employeeid' , '$itemid', '$in_qty', '$out_qty', '$balance_qty', '$created_by', '$CURRENT_MILLIS')");
    
                }
                
            }

         
            $update = mysqli_query($con, "UPDATE job_allotment SET send_to = '$sendto' , received_qty = '$outqty' WHERE  branchid = '$branchid' AND contractid = '$contractid' AND service_id = '$serviceid' AND employeeid = '$employeeid' AND itemid = '$itemid'"); 
   
        }
    
        if($update){

            mysqli_query ($con,"UPDATE workcontractservice SET  jobcompletedstatus = 1 WHERE contractid = '$contractid' AND branchid = '$branchid' AND serviceid = '$serviceid'");

            echo '{"status":"success"}';
    
        } else {
    
            echo '{"status":"falid1"}';
    
        }
    
        _close($con);
    
    }else{
    
        echo '{"status":"falid"}';
     
    }
?>