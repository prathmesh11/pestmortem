<?php

    $base = '../../../';

    include($base.'_in/connect.php');

    header('content-type: application/json; charset=utf-8');

    header("access-control-allow-origin: *");

    if(isset($_POST['data'])){

      $con = _connect();
    
      if (session_status() == PHP_SESSION_NONE) { session_start(); }

      $created_by = $_SESSION['employeeid'];
      $branchid   = $_SESSION['branchid'];

      $data = get_object_vars(json_decode($_POST["data"]));

      $enquiryid    = $data['enquiryid'];
      $category     = $data['category'];
      $followuptime = $data['followuptime'];
      $followupnote = $data['followupnote'];
      $enquirynote  = $data['enquirynote'];
      $workorder    = $data['workorder'];
                
      $customername = $data['customerid'];

      $int = (int) filter_var($customername, FILTER_SANITIZE_NUMBER_INT);
      
      if ($int == '') {

        $customerid=$data['customerid'];

      } else {

        $customerid=$int;

      }

      $create = false;

      if($workorder=='workorder'){

        $create=mysqli_query($con,"INSERT INTO crmmaster (customerid,branchid ,enquiryid ,category ,enquirynote,enquiry_by,enquiry_time ,created_time ,created_by) VALUES ('$customerid','$branchid' ,'$enquiryid' ,'$category' ,'$enquirynote','$created_by' ,'$CURRENT_MILLIS' ,'$CURRENT_MILLIS' ,'$created_by')");

      } else {

        $create=mysqli_query($con,"INSERT INTO crmmaster (customerid,branchid ,enquiryid ,category ,followupnote ,followuptime ,created_time ,created_by) VALUES ('$customerid','$branchid' ,'$enquiryid' ,'$category','$followupnote' ,'$followuptime' ,'$CURRENT_MILLIS' ,'$created_by')");

      }

        if($create){

            echo '{"status":"success"}';

        } else {

            echo '{"status":"falid"}';

        }
        
        _close($con);

    } else {

        echo '{"status":"falid"}';

     }
?>