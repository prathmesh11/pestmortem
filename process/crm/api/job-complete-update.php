<?php
    
    $base='../../../';
    
    include($base.'_in/connect.php');
    
    header('content-type: application/json; charset=utf-8');
    
    header("access-control-allow-origin: *");
    
    if(isset($_POST['data']) && isset($_POST['itemDetails'])) {
    
        $con = _connect();
    
        if (session_status()==PHP_SESSION_NONE) { session_start(); }
    
        $employeeid = $_SESSION['employeeid'];
    
        $data        = get_object_vars(json_decode($_POST['data']));
    
        $branchid    = $data['branchid'];
    
        $contractid  = $data['contractid'];

        $serviceid   = $data['serviceid'];

        $opning       = 0;
            
        $quality      = 0;
        
        $issue        = 0;
        
        $adjust_plus  = 0;
        
        $adjust_minus = 0;
        
        $outbranch    = 0;
        
        $outqty       = 0;
        
        $inbranch     = 0;
        
        $inqty        = 0;
        
        $total        = 0;
        
        $govqty       = 0;

        $in_qty       = 0;

        $out_qty      = 0;

        $itemDetails = json_decode($_POST['itemDetails']);

        foreach ($itemDetails as $i) {

            $employeeid   = get_object_vars($i)['employeeid'];

            $employeename = get_object_vars($i)['employeename'];
            
            $itemid       = get_object_vars($i)['itemid'];
            
            $itemname     = get_object_vars($i)['itemname'];
            
            $outqtys      = get_object_vars($i)['outqty'];

            $sendto       = get_object_vars($i)['sendto'];
            
            $selects       = mysqli_fetch_assoc(mysqli_query($con,"SELECT * FROM job_allotment WHERE branchid='$branchid' AND contractid='$contractid' AND employeeid = '$employeeid' AND itemid = '$itemid' AND received_qty = '$outqtys' AND service_id = '$serviceid' "));

            if (!$selects) {

                if ($sendto == 'Branch') {

                    $select  = mysqli_fetch_assoc(mysqli_query($con,"SELECT * FROM `livestockmaster` WHERE itemcode='$itemid' AND branchid='$branchid' ORDER BY id DESC LIMIT 1"));
    
                    if($select){
    
                        $opning       = $select['opning'];
                    
                        $quality      = $select['quality'];
                    
                        $adjust_plus  = $select['adjust_plus'];
                    
                        $adjust_minus = $select['adjust_minus'];
                    
                        $outqty       = $select['outqty'];
                    
                        $inqty        = $select['inqty']  + $outqtys;
                    
                        $issue        = $select['issue'];
                    
                        $total        = ($opning+$quality+$adjust_plus+$inqty)-($issue+$adjust_minus+$outqty);

                        $create       = mysqli_query($con,"INSERT into livestockmaster (branchid,itemcode,opning,quality,issue,adjust_plus,adjust_minus,outbranch,outqty,inbranch,inqty,total,created_by,created_time) Values ('$branchid','$itemid','$opning','$quality','$issue','$adjust_plus','$adjust_minus','$outbranch','$outqty','$inbranch','$inqty','$total','$created_by','$CURRENT_MILLIS')");
    
                    }
                
                } elseif ($sendto == 'Employee') {
    
                    $select       = mysqli_fetch_assoc(mysqli_query($con,"SELECT * FROM `employeestock` WHERE itemcode='$itemid' AND branchid='$branchid' AND employeeid = '$employeeid' ORDER BY id DESC LIMIT 1"));
                
                    if($select){
        
                        $in_qty       = $select['in_qty'] + $outqtys;
                       
                        $out_qty      = $select['out_qty'];
                       
                        $balance_qty  = $in_qty - $out_qty;
                     
                        $create       = mysqli_query($con,"INSERT into employeestock (branchid, employeeid, itemcode, in_qty, out_qty, balance_qty, created_by, created_time) Values  ('$branchid', '$employeeid' , '$itemid', '$in_qty', '$out_qty', '$balance_qty', '$created_by', '$CURRENT_MILLIS')");
        
                    }
                    
                }

            }
            

            $update1 = mysqli_query($con, "UPDATE job_allotment SET send_to = '$sendto' , received_qty = '$outqtys' WHERE  branchid = '$branchid' AND contractid = '$contractid' AND service_id = '$serviceid' AND employeeid = '$employeeid' AND itemid = '$itemid'"); 
   
        }
    
        if($update1){

            mysqli_query ($con,"UPDATE workcontractservice SET  jobcompletedstatus = 1 WHERE contractid = '$contractid' AND branchid = '$branchid' AND serviceid = '$serviceid'");

            echo '{"status":"success"}';
    
        } else {
    
            echo '{"status":"falid1"}';
    
        }
    
        _close($con);
    
    }else{
    
        echo '{"status":"falid"}';
     
    }
?>