<?php

    $base = '../../../';

    include($base.'_in/connect.php');

    header('content-type: application/json; charset=utf-8');

    header("access-control-allow-origin: *");

    if (isset($_POST['data']) && isset($_POST['branchId']) && isset($_POST['invoice_bill'])) {

        $con=_connect();

        if (session_status() == PHP_SESSION_NONE) { session_start(); }

        $created_by = $_SESSION['employeeid'];

        $branchId       = $_POST["branchId"];

        $invoice_bill   = $_POST["invoice_bill"];

        $serviceDetails = $_POST["serviceDetails"];

        $data = get_object_vars(json_decode($_POST["data"]));

        $issuedate       = $data['issuedate'];
        $custName        = $data['custName'];
        $custAddress     = $data['custAddress'];
        $custGst         = $data['custGst'];
        $custEmail       = $data['custEmail'];
        $custmobile      = $data['custmobile'];
        $premisesName    = $data['premisesName'];
        $premisesAddress = $data['premisesAddress'];
        $premisesGst     = $data['premisesGst'];
        $premisesEmail   = $data['premisesEmail'];
        $premisesmobile  = $data['premisesmobile'];
        $modOfPayment    = $data['modOfPayment'];
        $addDeclaration  = $data['addDeclaration'];
        $amount          = $data['amount'];
        $cgstPer         = $data['cgstPer'];
        $cgstAmt         = $data['cgstAmt'];
        $sgstPer         = $data['sgstPer'];
        $sgstAmt         = $data['sgstAmt'];
        $igstPer         = $data['igstPer'];
        $igstAmt         = $data['igstAmt'];
        $netAmount       = $data['netAmount'];
        $bankName        = $data['bankName'];
        $branchName      = $data['branchName'];
        $ifscCode        = $data['ifscCode'];
        $accountNo       = $data['accountNo'];

        $json = ',{"issuedate":"'.$issuedate.'","custName":"'.$custName.'","custAddress":"'.$custAddress.'","custGst":"'.$custGst.'","custEmail":"'.$custEmail.'","custmobile":"'.$custmobile.'","premisesName":"'.$premisesName.'","premisesAddress":"'.$premisesAddress.'","premisesGst":"'.$premisesGst.'","premisesEmail":"'.$premisesEmail.'","premisesmobile":"'.$premisesmobile.'","todate":"'.$todate.'","fromdate":"'.$fromdate.'","modOfPayment":"'.$modOfPayment.'","addDeclaration":"'.$addDeclaration.'","bankName":"'.$bankName.'","branchName":"'.$branchName.'","ifscCode":"'.$ifscCode.'","accountNo":"'.$accountNo.'"}';

        $json          = substr($json,1);
        $customer_json = '['.$json.']';

        $update = mysqli_query($con,"UPDATE billing SET customer_json='$customer_json',service_json='$serviceDetails',total_amount='$amount',cgstper='$cgstPer',cgstamt='$cgstAmt',sgstper='$sgstPer',sgstamt='$sgstAmt',igstper='$igstPer',igstamt='$igstAmt',contractamt='$netAmount',created_by='$created_by',created_time='$CURRENT_MILLIS' WHERE branchid='$branchId' AND invoice_bill='$invoice_bill'");

        if ($update) {

            echo '{"status":"success"}';

        } else {

            echo '{"status":"falid"}';
            
        }

        _close($con);

    } else {

        echo '{"status":"falid"}';
        
    }



?>