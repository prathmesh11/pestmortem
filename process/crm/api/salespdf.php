<?php
    $base='../../../';
    include($base.'_in/connect.php');
    header('content-type: application/json; charset=utf-8');
    header("access-control-allow-origin: *");
    if(isset($_POST['enquiryid']) && isset($_POST['contractid'])){

      $con = _connect();
      $str = '';

      if (session_status() == PHP_SESSION_NONE) { session_start(); }
      $branchid   = $_SESSION['branchid']; 
      $branchjson=mysqli_fetch_assoc(mysqli_query($con,"SELECT branchjson x FROM branchmaster WHERE branchid='$branchid'"))['x'];
      $branchname=mysqli_fetch_assoc(mysqli_query($con,"SELECT branchname x FROM branchmaster WHERE branchid='$branchid'"))['x'];

      $enquiryid  = _clean($con,$_POST['enquiryid']);
      $contractid = _clean($con,$_POST['contractid']);

      $billid= mysqli_fetch_assoc(mysqli_query($con,"SELECT billid x FROM `workcontractbill` WHERE enquiryid='$enquiryid' AND contractid='$contractid' "))['x'];

      $check= mysqli_fetch_assoc(mysqli_query($con,"SELECT invoice_bill x FROM `workcontractbill` WHERE billid='$billid'"))['x'];
      if($check==''){
          $invoice_bill= mysqli_fetch_assoc(mysqli_query($con,"SELECT COUNT(invoice_bill)+1 as x FROM `workcontractbill` WHERE invoice_bill<>''"))['x'];
          $branchshortname=mysqli_fetch_assoc(mysqli_query($con,"SELECT branchshortname x FROM branchmaster WHERE branchid='$branchid'"))['x'];
    
          $year1 = 0;
          $year2 = 0;
          if ( date('m') > 3 ) {
              $year1 = date('y');
              $year2 = date('y')+1;
          }else{
              $year1 = date('y')-1;
              $year2 = date('y');
          }
          $invoice_bill='PMI/'.$branchshortname.'/'.$invoice_bill.'/'.$year1.'-'.$year2;
          mysqli_query($con,"UPDATE workcontractbill SET invoice_bill='$invoice_bill' WHERE billid='$billid' ");
      }
    $sqlquery = mysqli_fetch_assoc(mysqli_query($con,"SELECT * from workcontractbill WHERE billid = '$billid'"));
    $json1='';
    $grand_total =  $sqlquery['contractamt'] - $sqlquery['discount_amt'];
     $billdate = date("d-m-Y",$sqlquery['billdate']/1000);
     $json1= '{ "billid" : "'.$sqlquery['invoice_bill'].'","contractid" : "'.$sqlquery['contractid'].'","billdate" : "'.$billdate.'","billamt" : "'.$sqlquery['billamt'].'","ac" : "'.$sqlquery['ac'].'","qty" : "'.$sqlquery['qty'].'","rate" : "'.$sqlquery['rate'].'","pono" : "'.$sqlquery['pono'].'","cgstper" : "'.$sqlquery['cgstper'].'","sgstper" : "'.$sqlquery['sgstper'].'","igstper" : "'.$sqlquery['igstper'].'","cgstamt" : "'.$sqlquery['cgstamt'].'","igstamt" : "'.$sqlquery['igstamt'].'","sgstamt" : "'.$sqlquery['sgstamt'].'","other1" : "'.$sqlquery['other1'].'","other2" : "'.$sqlquery['other2'].'","discount_amt" : "'.$sqlquery['discount_amt'].'","contractamt" : "'.$grand_total.'"}';  
           
    
      $address           = get_object_vars(json_decode(mysqli_fetch_assoc(mysqli_query($con,"SELECT branchjson x FROM `branchmaster` WHERE branchid='$branchid'"))['x']))['address'];      
      $gstnumber1        = get_object_vars(json_decode(mysqli_fetch_assoc(mysqli_query($con,"SELECT branchjson x FROM `branchmaster` WHERE branchid='$branchid'"))['x']))['gstnumber'];
      $companybranchname = get_object_vars(json_decode(mysqli_fetch_assoc(mysqli_query($con,"SELECT branchjson x FROM `branchmaster` WHERE branchid='$branchid'"))['x']))['companybranchname'];
      $email             = get_object_vars(json_decode(mysqli_fetch_assoc(mysqli_query($con,"SELECT branchjson x FROM `branchmaster` WHERE branchid='$branchid'"))['x']))['email'];
      $phone             = get_object_vars(json_decode(mysqli_fetch_assoc(mysqli_query($con,"SELECT branchjson x FROM `branchmaster` WHERE branchid='$branchid'"))['x']))['phone'];
      $state             = get_object_vars(json_decode(mysqli_fetch_assoc(mysqli_query($con,"SELECT branchjson x FROM `branchmaster` WHERE branchid='$branchid'"))['x']))['state'];
      $statename         = mysqli_fetch_assoc(mysqli_query($con,"SELECT statename x FROM `statemaster` WHERE statecode='$state'"))['x'];

    $result = mysqli_fetch_assoc(mysqli_query($con,"SELECT * FROM workcontract WHERE enquiryid='$enquiryid' AND contractid='$contractid' "));
    if($result){

      $customerid   = mysqli_fetch_assoc(mysqli_query($con,"SELECT customerid x FROM crmmaster WHERE enquiryid='$enquiryid' AND contractid='$contractid' "))['x'];
      $customername = get_object_vars(json_decode(mysqli_fetch_assoc(mysqli_query($con,"SELECT customerjson x FROM `customermaster` WHERE branchid='$branchid' AND customerid='$customerid' "))['x']))['customername'];
    //   $gstnumber = mysqli_fetch_assoc(mysqli_query($con,"SELECT gstnumber x FROM `customermaster` WHERE branchid='$branchid' AND customerid='$customerid' "))['x'];
      $address      = get_object_vars(json_decode(mysqli_fetch_assoc(mysqli_query($con,"SELECT customerjson x FROM `customermaster` WHERE branchid='$branchid' AND customerid='$customerid' "))['x']))['address'];
      $contactjson  = json_decode(mysqli_fetch_assoc(mysqli_query($con,"SELECT contactjson x FROM customermaster WHERE customerid='$customerid' AND branchid='$branchid'"))['x'],true);
      $mobile       = $contactjson[0]['mobile'];
      $email        = $contactjson[0]['email'];

      $gstnumber    = get_object_vars(json_decode(mysqli_fetch_assoc(mysqli_query($con,"SELECT customerjson x FROM `customermaster` WHERE branchid='$branchid' AND customerid='$customerid' "))['x']))['gstnumber'];
      $state        = get_object_vars(json_decode(mysqli_fetch_assoc(mysqli_query($con,"SELECT customerjson x FROM `customermaster` WHERE branchid='$branchid' AND customerid='$customerid' "))['x']))['state'];
      $statename    = mysqli_fetch_assoc(mysqli_query($con,"SELECT statename x FROM `statemaster` WHERE statecode='$state'"))['x'];

      $contractdate = date('d-m-Y',$result['contractdate']/1000);
          $str='{"contractid":"'.$result["contractid"].'",
          "enquiryid":"'.$result["enquiryid"].'",
          "date":"'.$contractdate.'",
          "customerid":"'.$customerid.'",
          "gstnumber":"'.$gstnumber.'",
          "statename":"'.$statename.'",
          "companyname":"'.$customername.'",
          "caddress":"'.$address.'",
          "mnumber":"'.$mobile.'",
          "email":"'.$email.'",
          "tablejson":'.$result["itemjson"].',
          "amount":"'.$result["amount"].'",
          "cgstamt":"'.$result["cgstamt"].'",
          "sgstamt":"'.$result["sgstamt"].'",
          "igstamt":"'.$result["igstamt"].'",
          "othercharges":"'.$result["othercharges"].'",
          "total":"'.$result["totalcontractamt"].'"}';

          echo '{"status":"success","billjson":'.$json1.',"branchjson":'.$branchjson.',"branchname":"'.$branchname.'","branchshortname":"'.$branchshortname.'","json":['.$str.'],"address":"'.$address.'","gstnumber":"'.$gstnumber1.'","companybranchname":"'.$companybranchname.'","email":"'.$email.'","phone":"'.$phone.'","state":"'.$statename.'"}';
      }else{
            echo '{"status":"falid1"}';
      }
             _close($con);
    }else{
        echo '{"status":"falid2"}';
    }
?>
