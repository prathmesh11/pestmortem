<?php 

  $base    = '../../';
  
  $navenq4 = 'background:#1B1464;';
  
  include('header.php');

  if (!in_array('Job Completed', $arrysession) ) {
   
    echo "<script>window.location.href='/process/dashboard.php';</script>";
   
    exit;

}

  if (session_status() == PHP_SESSION_NONE) { session_start(); }
  
  $sessionby = $_SESSION['employeeid'];
 
  $branchid  = $_SESSION['branchid'];

?>

<br>

<div class="table-ui container-fluid">

  <div class="tr row">

    <div class="col-sm-1 th" style="word-wrap:break-word;">Enquiry No.</div>

    <div class="col-sm-3 th">Customer Info</div>

    <div class="col-sm-2 th">Job Description</div>

    <div class="col-sm-1 th">Employee</div>

    <div class="col-sm-2 th">Received Batch details</div>

    <div class="col-sm-2 th">Schedule Date</div>

    <div class="col-sm-1 th">Action</div>

  </div>

  <?php
  
    $threedaysbefore = $CURRENT_MILLIS+172800000;
    
    $result          = mysqli_query($con,"SELECT * FROM workcontractservice WHERE servicedate<>0 AND servicedate<'$threedaysbefore' AND branchid='$branchid' AND allot_by<>0 AND allot_time<>0 AND completed_by=0 AND completed_time=0 ORDER BY servicedate DESC"); 

    while($rows = mysqli_fetch_assoc($result)){
  
      $contractid         = $rows['contractid'];
      
      $serviceid          = $rows['serviceid'];

      $jobcompletedstatus = $rows['jobcompletedstatus'];
      
      $customerid = mysqli_fetch_assoc(mysqli_query($con,"SELECT customerid x FROM crmmaster WHERE contractid='$contractid'"))['x'];
    
      $cifo         = mysqli_fetch_assoc(mysqli_query($con,"SELECT * FROM customermaster WHERE customerid='$customerid'"));

      $category     = mysqli_fetch_assoc(mysqli_query($con,"SELECT category x FROM crmmaster WHERE wo_time<>0 AND wo_by<>0 AND contractid='$contractid'"))['x'];

      $contractid   = $rows['contractid'];
  
      $servicedate  = $rows['servicedate'];

      $servicename  = $rows['servicename'];

      $shedulestart = $rows['shedulestart'];

      $sheduleend   = $rows['sheduleend'];

      $empjson      = $rows['empjson'];
  ?>

  <div class="row tr">

    <div class="col-sm-1 td" style="word-wrap:break-word;">

      <?php echo $contractid; ?>
    
    </div>

    <div class="col-sm-3 td">
      
      <?php

        echo 'Company : '.$cifo['customername']; 
        
        echo '<br>Address : '.get_object_vars(json_decode($cifo['customerjson']))['address']; 
        
        echo '<br>Name  : '.get_object_vars(json_decode($cifo['contactjson'])[0])['name'];
        
        echo '<br>Dept. : '.get_object_vars(json_decode($cifo['contactjson'])[0])['department'];
        
        echo '<br>Mobile : <a href="tel:'.get_object_vars(json_decode($cifo['contactjson'])[0])['mobile'].'">'.get_object_vars(json_decode($cifo['contactjson'])[0])['mobile'].'</a>';
        
        echo '<br>Landline'.get_object_vars(json_decode($cifo['contactjson'])[0])['landline'];
        
        echo '<br>Email : <a href="mailto:'.get_object_vars(json_decode($cifo['contactjson'])[0])['email'].'">'.get_object_vars(json_decode($cifo['contactjson'])[0])['email'].'</a>';
      ?>

    </div>

    <div class="col-sm-2 td" style="word-wrap:break-word;">
    
      <?php
          
        echo 'Servcie Name : '.$category.'<br>';

        echo 'Servcie Name : '.$servicename.'<br>';

        $lastfollowup       = $servicedate;

        $followUpStatusFlag = '';

        if($lastfollowup!=0){
        
          if($START_OF_THE_DAY<$lastfollowup && $END_OF_THE_DAY<=$lastfollowup){
        
            $followUpStatusFlag = '#3F51B5';

          }

          if($START_OF_THE_DAY>$lastfollowup && $END_OF_THE_DAY>$lastfollowup){
        
            $followUpStatusFlag = '#F44336';

          }

          if($START_OF_THE_DAY<=$lastfollowup && $END_OF_THE_DAY>$lastfollowup ){
        
            $followUpStatusFlag = '#4CAF50';

          }
        
        } else {
        
          $followUpStatusFlag = '#ff9800';
        
        }

        echo 'Servcie Date : <p style="background:'.$followUpStatusFlag.';color:#fff;">'.date("d-m-Y",$servicedate/1000).'</p>'; 
        
        echo 'Contract No. : '.$contractid.'<br>';
        
        echo 'Servcie No. : '.$serviceid; 

      ?>
    
    </div>

    <div class="col-sm-1 td">
      
      <?php 

        $results  = mysqli_query($con,"SELECT DISTINCT employeename FROM job_allotment WHERE contractid = '$contractid' AND service_id = '$serviceid' AND branchid='$branchid'");

        $rowcount = mysqli_num_rows($results);

        while($rowss = mysqli_fetch_assoc($results)){

          if ($rowcount >= 1 ) {

            echo $rowss['employeename'];


          } else if ($rowcount < 1) {
            
            echo $rowss['employeename'].',';

          }
        
        }

      ?>

    </div>

    <div class="col-sm-2 td">
      <br>
      
      <table class="table-list table" id="table-customer">
        
        <thead>
          
          <tr>
            <!-- <th rowspan="2">SR NO</th> -->
            <th>Item</th>
                        
            <th>Iss_qty</th>
            
            <th>R_Qty</th>
          
          </tr>
        
        </thead>
        
        <?php

          $results  = mysqli_query($con,"SELECT itemname,allot_qty,received_qty FROM job_allotment WHERE contractid = '$contractid' AND service_id = '$serviceid' AND branchid='$branchid'");

          while($rowsss = mysqli_fetch_assoc($results)){

            echo '<tr>';
                
              echo '<td>'.$rowsss['itemname'].'</td>';
              
              echo  '<td>'.$rowsss['allot_qty'].'</td>';

              echo  '<td>'.$rowsss['received_qty'].'</td>';
              
            echo '</tr>';

          }

        ?>

      </table>

    </div>


    <div class="col-sm-2 td">
      
      <?php

        if($shedulestart!=0){
            
          echo date("d-m-Y",$shedulestart/1000).' To '.date("d-m-Y",$sheduleend/1000);
        
        }

        echo "<br>Service Place : " .$rows['serviceAddress'];
      
      ?>
    
    </div>

    <div class="col-sm-1 td">
    
      <?php if ($jobcompletedstatus == 1) { ?>
      
        <button class="btn btn-sm btn-block btn-success" data-serviceid="<?php echo $serviceid; ?>"
          onclick="jobcompleted(this)">Confirm</button>
      
      <?php } ?>

      <a class="btn btn-sm btn-block btn-primary" href="/process/crm/material-receive.php?serviceid=<?php echo $serviceid; ?>&category=<?php echo $category;?>&contractid=<?php echo $contractid;?>&jobcompletedstatus=<?php echo $jobcompletedstatus;?>">Material Received</a>

      <button class="btn btn-sm btn-block btn-primary" data-serviceid="<?php echo $serviceid; ?>"
        data-contractid="<?php echo $contractid; ?>" data-branchid="<?php echo $branchid; ?>"
        onclick="expences(this)">Expences</button>
    
    </div>
  
  </div>

  <?php }  ?>

</div>

<!-- Expences Model Start  -->

<div id="myModal1" class="modal fade" role="dialog">

  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">

      <div class="modal-header">

        <h4 class="modal-title" align="center">Expences</h4>

      </div>

      <div class="modal-body">

        <div id="dummymodalid">
        
          <input type="hidden" data-name="serviceid" id="inp-serviceid">

          <input type="hidden" data-name="contractid" id="inp-contractid">

          <input type="hidden" data-name="branchid" id="inp-branchid">

        </div>

        <div class="row form-group">

        <div class="col-sm-12">

          <br>
          
          <table class="table-list" id="jobalotment-table" style="width:100%!important;">

            <thead>
            
              <tr>

                <th>Sr. No.</th>

                <th>Employee Name</th>

                <th>Expences Name</th>

                <th>Amount</th>

                <th>Action</th>

              </tr>

            </thead>

            <tbody>

              <tr>
                
                <td></td>

                <td>
                    
                    <select data-role="select" data-name='employeeid' id="employeeid" class="form-control input-sm">
                  
                      <option value="Select">Select</option>
  
                          
                    </select>
  
                  </td>

                <td>
                    
                  <select data-role="select" data-name='expencesName' id="expencesName" class="form-control input-sm">
                
                    <option value="Select">Select</option>

                    <option value="Telephone Charges">Telephone Charges</option>

                    <option value="Travelling Expenses">Travelling Expenses</option>

                    <option value="Conveyance Expenses">Conveyance Expenses</option>

                    <option value="Petrol & Fuel Expenses">Petrol & Fuel Expenses</option>
                        
                  </select>

                </td>

                <td>
                  
                  <input type="number" data-role="number" class="form-control input-sm" id='amount' data-name="amount" />
                
                </td>

                <td align="center">
                  
                  <button class="btn btn-primary btn-sm" onclick="modaldata()">Add+</button>
                
                </td>
              
              </tr>

            </tbody>

          </table>

          <br>

          <div class="col-sm-4"></div>

          <div class="col-sm-4">
            
            <button class="btn btn-sm btn-success btn-block" onclick="submitdata()">Submit</button>
          
          </div>

        </div>

      </div>

    </div>

  </div>

</div>

<!-- Expences Model End  -->

<?php

  include('footer.php');

?>

<script>

function expences(e) {

  let serviceid  = $(e).attr('data-serviceid');

  let contractid = $(e).attr('data-contractid');

  let branchid   = $(e).attr('data-branchid');

  $('#inp-serviceid').val(serviceid);

  $('#inp-contractid').val(contractid);

  $('#inp-branchid').val(branchid);

  $('table#jobalotment-table tr#abc').remove();

    if (serviceid != '' && contractid != '' && branchid != '') {

      $.ajax({

        type: "POST",
        
        data: 'serviceid='+serviceid+'&contractid='+contractid+'&branchid='+branchid,
        
        url: 'api/expencesselect.php',
        
        cache: false,
        
        success: function (res) {

          $('#myModal1').modal('show');
        
          if (res.status == 'success') {

            let json = res.json;

            let str = '';

            let count = 1;

            for (let i in json) {

              str +='<tr id="abc">';

                str+='<td>'+count+'</td>';

                str += '<td class="employeeid" style="display:none;">' + json[i].employeeid + '</td>';

                str += '<td class="employeename">' + json[i].employeename + '</td>';

                str += '<td class="expencesName">' + json[i].expencesName + '</td>';

                str += '<td class="amount">' + json[i].amount + '</td>';

                str+='<td align="center"><button class="btn btn-sm btn-danger" onclick="remover(this)">R</button></td>';

              str +='</tr>'; 

              count++;
              
            }

            $('#jobalotment-table tr:last ').after(str);

            let jsons = res.jsons;

            let strs = '';

            for (let i in jsons) {

              strs += '<option value="'+jsons[i].employeeid+'">' + jsons[i].employeename + '</td>';
              
            }

            $('#employeeid').append(strs);

          } 
      
        }
      
      })
    
  } else {

    alert('Service Id Missing');

  }

  
}


function modaldata() {

  var valid = true;

  var data = checker('jobalotment-table');

    let employeeid   = data.employeeid;

    let employeename = $('#employeeid').find(":selected").text();

    let expencesName = data.expencesName;

    $('#jobalotment-table > tbody > tr').each(function () {

      let employeeid1   = $(this).find('.employeeid').text().trim();

      let expencesName1 = $(this).find('.expencesName').text().trim();

      if (employeeid1 == employeeid && expencesName1 == expencesName) {

        valid = valid * false;

        alert('Dublicate Expences Name For Same Employee');

      }

    });

  if (checker('jobalotment-table') != false) {

    valid = valid * true;

  } else {

    valid = valid * false;

  }

  if (valid) {

    var len = $('#jobalotment-table > tbody > tr').length;


    var str = '<tr>';

      str += '<td>' + len + '</td>';

      str += '<td class="employeeid" style="display:none;">' + employeeid + '</td>';

      str += '<td class="employeename">' + employeename + '</td>';

      str += '<td class="expencesName">' + data.expencesName + '</td>';

      str += '<td class="amount">' + data.amount + '</td>';

      str += '<td align="center"><button class="btn btn-sm btn-danger" onclick="remover(this)">R</button></td>';

    str += '</tr>';

    $('#jobalotment-table > tbody').append(str);

    modals.clear('jobalotment-table');


  }

}



function submitdata() {

  var valid = true;

  if (checker('dummymodalid') != false) {

    valid = valid * true;

  } else {

    valid = valid * false;

  }

  if (modals.tabletdbyclass('jobalotment-table') != false) {

    valid = valid * true;

  } else {

    valid = valid * false;

  }

  if (valid) {

    var data1      = checker('dummymodalid');

    var serviceid  = data1.serviceid;
    
    var contractid = data1.contractid;
    
    var branchid   = data1.branchid;
    
    var data       = modals.tabletdbyclass('jobalotment-table');

    data.shift();

    data = JSON.stringify(data);

    $.ajax({

      type: "POST",

      data: {
        data: data,
        serviceid: serviceid,
        branchid: branchid,
        contractid: contractid
      },

      url: 'api/jobcompletexpences.php',

      cache: false,

      success: function (res) {

        if (res.status == 'success') {

          window.location.reload();

        }

      }

    })

  }

}

function jobcompleted(e){

  let serviceid = $(e).data('serviceid');
    
  $.ajax({
        
    type: "POST",
    
    data: 'serviceid='+serviceid,
    
    url: 'api/jobcompleted.php',
    
    cache: false,
    
    success: function (res) {
    
      if (res.status == 'success') {
         
        window.location.reload();
          
      }
        
    }
    
  })

}


$('#myModal1').on('hidden.bs.modal', function (e) {
        
  location.href = 'job-complete.php';

})

</script>