<?php 
$base='../../';
$navenq2='background:#1B1464;';
include('header.php');

if (!in_array('Branch To Branch Trasfer', $arrysession)) {
   
    echo "<script>window.location.href='/process/dashboard.php';</script>";
   
    exit;

}

if (session_status() == PHP_SESSION_NONE) { session_start(); }
$sessionby = $_SESSION['employeeid'];
$branchid = $_SESSION['branchid'];

?>
<style>
.table-list td,
   .table-list th {
       border: 1px solid #ddd;
       padding: 1px !important;
       font-size: 13px;
   }
   .table-list td{
	padding-top: 2px !important;
    margin:0;

   }

   .table-list tr:nth-child(even) {
       background-color: #f2f2f2;
   }

   .table-list th {
       padding-top: 5px;
       padding-bottom: 5px;
       text-align: center;
       background-color: #16a085;
       color: white;
   }
</style>
<br>

<div class="table-ui container-fluid">
    <div class="tr row">
    <div class="col-sm-1 th">Sr No.</div>
        <div class="col-sm-2 th">Item Name</div>
        <div class="col-sm-2 th">Stock Qty</div>
        <div class="col-sm-2 th">Stock Unit</div>
        <div class="col-sm-2 th">Transfer Qty</div>
        <div class="col-sm-2 th">Transfer To</div>
        <div class="col-sm-1 th">Action</div>
        </div>
        <?php
        $counter=1;
              $result=mysqli_query($con,"SELECT DISTINCTROW itemcode FROM `livestockmaster` WHERE branchid='$branchid' ORDER BY id DESC"); 
              while($rows=mysqli_fetch_assoc($result)){
                $itemcode=$rows['itemcode'];
                $itemname=mysqli_fetch_assoc(mysqli_query($con,"SELECT itemname x FROM stockmaster WHERE stockid='$itemcode'"))['x'];
                $unit1=mysqli_fetch_assoc(mysqli_query($con,"SELECT unit1 x FROM stockmaster WHERE stockid='$itemcode'"))['x'];

                $result1=mysqli_query($con,"SELECT * FROM `livestockmaster` WHERE branchid='$branchid' ORDER BY id DESC LIMIT 1"); 
                while($rowss=mysqli_fetch_assoc($result1)){
                    ?>
        <div class="tr row">
            <div class="col-sm-1 td"><?php echo $counter; ?></div>
            <div class="col-sm-2 td"><?php echo $itemname; ?></div>
            <div class="col-sm-2 td itemcode" style="display:none;"><?php echo $itemcode; ?></div>
            <div class="col-sm-2 td stockqty"><?php echo $rowss['total']; ?></div>
            <div class="col-sm-2 td"><?php echo $unit1; ?></div>
            <div class="col-sm-2 td"><input type="text" value="0" class="form-control input-sm ourqty"></div>
            <div class="col-sm-2 td">
            <select class="form-control input-sm select-js1 branchid" data-role="select" >
                        <option value="Select">Select</option>
                            <?php
                                    $result1=mysqli_query($con, "SELECT branchname,branchid FROM `branchmaster`");
                                    while($rows=mysqli_fetch_assoc($result1)) {
                                    echo '<option value="'.$rows['branchid'].'">'.$rows['branchname'].'</option>';
                                }
                            ?>
                        </select>
            </div>
            <div class="col-sm-1 td"><button class="btn btn-sm btn-primary" onclick="trasfer(this)">Trasfer</button></div>
        </div>
                    <?php
                }
            }
        ?>
</div>

<?php 
include('footer.php');
?>
<script>

function trasfer(e){
   var ourqty=$(e).parent().parent().find('.ourqty').val();
   var tobranchid=$(e).parent().parent().find('.branchid').val();
   var itemcode=$(e).parent().parent().find('.itemcode').text();
    if (confirm("Are You Sure TO Confirm Order?")) {
        var ids = $(e).data('ids');
        $.ajax({
            type: "POST",
            data: 'ourqty=' + ourqty+'&tobranchid='+tobranchid+'&itemcode='+itemcode,
            url: 'api/trasfer.php',
            cache: false,
            success: function (res) {
                if (res.status == 'success') {
                    window.location.reload();
                }
            }
        })
    }
}

</script>