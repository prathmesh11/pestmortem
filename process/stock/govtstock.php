<?php 
    $base='../../';
    $navenq4='background:#1B1464;';
    include('header.php');

    if (!in_array('Government Stock', $arrysession)) {
   
        echo "<script>window.location.href='/process/dashboard.php';</script>";
       
        exit;

    }

    if (session_status() == PHP_SESSION_NONE) { session_start(); }
    $sessionby = $_SESSION['employeeid'];
    $branchid = $_SESSION['branchid'];

?>
<style>
     h2{
        margin-left:500px;
    }
    th {
    background: #8e44ad;
    color: #fff;
    text-align: center;
    padding-top: 1px;
    padding-bottom: 1px;
    border: 1px solid #fff;
    }
</style>
<input id="ip-branch" class="hidden" value="<?php echo $branchid ?>">
<div class=" container-fluid">
    <div>
        <h2><b>Government Stock Report</b></h2>
        <div class="row">
            <div class="col-sm-2">
                <div class="form-group">
                <b style="padding-left:20px;">Select Branch :</b>
                    <select class="form-control input-sm select-js1" data-role="select" data-name="branchid" id="branchid">
                        <option value="Select">Select</option>
                         <?php
                                $result1=mysqli_query($con, "SELECT branchname,branchid FROM `branchmaster`");
                                while($rows=mysqli_fetch_assoc($result1)) {
                                echo '<option value="'.$rows['branchid'].'">'.$rows['branchname'].'</option>';
                            }
                            ?> 
                    </select>
                </div>
            </div>
            <div class="col-sm-2">
                <div class="form-group">
                <b style="padding-left:20px;">Material Type :</b>
                    <select data-role="select" data-name='certType' class="form-control input-sm" id="certType">
                    <option value="">Select</option>
                    <option value="ALP">ALP</option>
                    <option value="MBR/AFAS">MBR / AFAS</option>
                </select>
                </div>
            </div>
            <div class="col-sm-2">
                <b style="padding-left:20px;">From Date :</b>
                    <div class="form-group"><input type="date" class="form-control input-sm" data-name="fromdate" id="fromdate">
                </div>
            </div>
            <div class="col-sm-2">
                <b style="padding-left:20px;">To Date :</b>
                    <div class="form-group"><input type="date" class="form-control input-sm" data-name="todate" id="todate">
                </div>
            </div>
            <div class="col-sm-2">
                <br>
                <button class="btn btn-success btn-md btn-block" onclick="submit()">Submit</button>
            </div>
            <div class="col-sm-2">
                <br>
                <button class="btn btn-primary btn-md btn-block btnsave hidden" onclick="insert()">Save</button>
            </div>
    </div>
    <hr style="margin: 2px;">
        <table class="table" id="table" style="border: 1px solid #ddd;">
            <thead>
                <tr>
                <th width="50">Sr No.</th>
                <th width="120">Date of Fumigation</th>
                <th width="150">Comodity</th>
                <th width="150">Certificate No</th>
                <th width="120">Quntity</th>
                <th width="150">Country of Export</th>
                <th width="120">Dose appllied in</th>
                <th width="120">Total used in Kgs.</th>
                <th width="120">Remark</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>  

<?php 
include('footer.php');
?>

<script>
function submit(){
    $.ajax({
        type: "POST",
        data:{
            branchid:$('#branchid').val(),
            certType:$('#certType').val(),
            fromdate:$('#fromdate').val(),
            todate:$('#todate').val()
        },
        url: 'api/select.php',
        cache: false,
        success: function (res) {
            if(res.status=='success'){
                var json=res.json;
               $("#table > tbody").empty();
                for(var i in json){
                    var str='<tr style="border: 1px solid #ddd;">';
                        str+='<td style="border: 1px solid #ddd;">'+json[i].id+'</td>';
                        str+='<td style="border: 1px solid #ddd;" class="fumdate">'+json[i].fumdate+'</td>';
                        if(json[i].certType=="MBR" || json[i].certType=="AFAS"){
                            str+='<td style="border: 1px solid #ddd;" class="commodity">'+json[i].mbrcommodity+'</td>';
                        }else{
                            str+='<td style="border: 1px solid #ddd;" class="commodity">'+json[i].commodity+'</td>';
                        }
                        str+='<td style="border: 1px solid #ddd;" class="certificateno">'+json[i].certificateno+'</td>';
                        str+='<td style="border: 1px solid #ddd;" class="qty">'+json[i].qty+'</td>';
                        str+='<td style="border: 1px solid #ddd;" class="exporter">'+json[i].country+'</td>';
                        str+='<td style="border: 1px solid #ddd;"><input type="text" class="form-control input-sm dosesused" value="'+json[i].fumdosage+'"></td>';
                        str+='<td style="border: 1px solid #ddd;"><input type="text" class="form-control input-sm dosesusedinkgs"value="'+json[i].chemichalissued+'"></td>';
                        str+='<td style="border: 1px solid #ddd;"><input type="text" class="form-control input-sm remarks"></td>';
                        str+='</tr>'
                        $("#table > tbody").append(str);
                        $(".btnsave").removeClass('hidden');
                }
                    
            }
        }
	});

}
function insert(){
    var json = [];
    $("#table > tbody tr").each(function(){
        var data={};
        var fumdate=
        data["fumdate"] = $(this).find(".fumdate").text();
        data["commodity"] = $(this).find(".commodity").text();
        data["certificateno"] = $(this).find(".certificateno").text();
        data["qty"] = $(this).find(".qty").text();
        data["exporter"] = $(this).find(".exporter").text();
        data["dosesused"] = $(this).find(".dosesused").val();
        data["dosesusedinkgs"] = $(this).find(".dosesusedinkgs").val();
        data["remarks"] = $(this).find(".remarks").val();
        json.push(data);
    })
    if(json!=''){
        if (confirm("Are You Sure TO Confirm Goverment Stock Entry?")) {
            $.ajax({
                type: "POST",
                data: 'json='+ JSON.stringify(json),
                url: 'api/insert.php',
                cache: false,
                success: function (res) {
                    if (res.status == 'success') {
                        window.location.reload();
                    }
                }
            })
        }
    }
    
}
</script>