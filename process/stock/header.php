<?php

$css = '<link rel="stylesheet" href="'.$base.'css/classic.css">
<link rel="stylesheet" href="'.$base.'css/classic.date.css">
<link rel="stylesheet" href="'.$base.'css/classic.time.css">
<link rel="stylesheet" href="'.$base.'css/alart.css">
<link href="'.$base.'/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" href="'.$base.'css/grid.min.css">';

$js = '<script src="'.$base.'js/picker.js"></script>
<script src="'.$base.'js/picker.date.js"></script>
<script src="'.$base.'js/picker.time.js"></script>
<script src="'.$base.'js/pdfmake.min.js"></script>
<script src="'.$base.'js/vfs_fonts.js"></script>
<script src="'.$base.'/js/select2.min.js"></script>
<script src="'.$base.'/js/alart.js"></script>
<script src="'.$base.'js/fwork.js"></script>';
    include($base.'_in/header.php');
    include($base.'_in/connect.php');

    if (!in_array("Material Issue", $arrysession) && !in_array('Material Receive', $arrysession) && !in_array('Branch To Branch Trasfer', $arrysession) && !in_array('Stock', $arrysession) && !in_array('Government Stock', $arrysession)) {
   
        echo "<script>window.location.href='/process/dashboard.php';</script>";
       
        exit;

    }

    $con = _connect();
?>
<style>
    .th{
        background:#8e44ad;
        color:#fff;
        text-align:center;
        padding-top:2px;
        padding-bottom:2px;
        border:1px solid #fff;
    }
    .td{
        border:1px solid #ddd;
    }
    .table-ui .btn{
        margin:3px;
    }
    #myModal1 .col-sm-4,.col-sm-8{
      margin-bottom:2px;
    }
    .picker__select--year{
        height:auto;
    }
    .picker__select--month{
        height:auto;
    }
    .table-list td,
   .table-list th {
       border: 1px solid #ddd;
       padding: 1px !important;
       font-size: 13px;
   }
   .table-list td{
	padding-top: 10px !important;
   }

   .table-list tr:nth-child(even) {
       background-color: #f2f2f2;
   }

   .table-list th {
       padding-top: 5px;
       padding-bottom: 5px;
       text-align: center;
       background-color: #16a085;
       color: white;
   }
    </style>
<div class="container-fluid">
    <div id="div-content" class="content">
<table width="100%" >
<tr>
<!-- <td align="center" style="width:10%"><a href="/process/stock/gate-entry.php" style="border:1px solid blue;border-radius:0px;<?php echo $navenq2;?>" class="btn btn-primary btn-block">Material used Return</td> -->
<td align="center" style="<?php echo $MaterialIssue; ?>width:10%"><a href="/process/stock/material-issue.php" style="border:1px solid blue;border-radius:0px;<?php echo $navenq1;?>" class="btn btn-primary btn-block">Material Issue</td>
<td align="center" style="<?php echo $MaterialReceive; ?>width:10%"><a href="/process/stock/material-receive.php" style="border:1px solid blue;border-radius:0px;<?php echo $navenq5;?>" class="btn btn-primary btn-block">Material Receive</td>
<td align="center" style="<?php echo $BranchToBranchTrasfer; ?>width:10%"><a href="/process/stock/transfer.php" style="border:1px solid blue;border-radius:0px;<?php echo $navenq2;?>" class="btn btn-primary btn-block">Branch To Branch Trasfer</td>
<td align="center" style="<?php echo $_Stock; ?>width:10%"><a href="/process/stock/stock.php" style="border:1px solid blue;border-radius:0px;<?php echo $navenq3;?>" class="btn btn-primary btn-block">Stock</td>
<td align="center" style="<?php echo $GovernmentStock; ?>width:10%"><a href="/process/stock/govtstock.php" style="border:1px solid blue;border-radius:0px;<?php echo $navenq4;?>" class="btn btn-primary btn-block">Government Stock</td>
</tr>
</table>
