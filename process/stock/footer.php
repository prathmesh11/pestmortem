<?php
    include($base.'_in/footer.php');
?>
<script>

function branchadmin(e){
  var branchid=$(e).val();
  if(branchid=='allbranch'){
    setCookie('branchid','', '');
    location.reload();
  }else{
    setCookie('branchid',branchid,1);
    location.reload();
  }
}


function setCookie(cname, cvalue, exdays) {
  var d = new Date();
  d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
  var expires = "expires="+d.toUTCString();
  document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}


function confirms(e) {
    if (confirm("Are You Sure TO Confirm Order?")) {
        var link1 = $(e).data('link1');
        var link2 = $(e).data('link2');
        var purchaseid = $(e).data('purchaseid');
        $.ajax({
            type: "POST",
            data: 'purchaseid=' + purchaseid + '&link1=' + link1 + '&link2=' + link2,
            url: 'api/confirm.php',
            cache: false,
            success: function (res) {
                if (res.status == 'success') {
                    window.location.reload();
                }
            }
        })
    }
}

function closes(e) {
    console.log(e);
    
    if (confirm("Are You Sure TO close Order?")) {
        var enquiryid = $(e).data('enquiryid');
        $.ajax({
            type: "POST",
            data: 'enquiryid=' + enquiryid,
            url: 'api/close.php',
            cache: false,
            success: function (res) {
                if (res.status == 'success') {
                    window.location.reload();
                }
            }
        })
    }
}

$(document).ready(function() {
    $('.select-js').select2({width: '100%', tags: true});

    $('.select').attr('style','width:100%!important;');
});
</script>
