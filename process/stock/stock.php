<?php 
$base='../../';
$navenq3='background:#1B1464;';
include('header.php');

if (!in_array('Stock', $arrysession)) {
   
    echo "<script>window.location.href='/process/dashboard.php';</script>";
   
    exit;

}

if (session_status() == PHP_SESSION_NONE) { session_start(); }
$sessionby = $_SESSION['employeeid'];
$branchid = $_SESSION['branchid'];

?>
<style>
.table-list td,
   .table-list th {
       border: 1px solid #ddd;
       padding: 1px !important;
       font-size: 13px;
   }
   .table-list td{
	padding-top: 2px !important;
    margin:0;

   }

   .table-list tr:nth-child(even) {
       background-color: #f2f2f2;
   }

   .table-list th {
       padding-top: 5px;
       padding-bottom: 5px;
       text-align: center;
       background-color: #16a085;
       color: white;
   }
</style>
<?php   
   $admin_branch='';
  if($_SESSION['role']=='admin'){
      $branchid_admin='';
      echo '<select onchange="branchadmin(this)" class="form-control input-sm" style="margin:2px;">';
      if($_COOKIE['branchid']!=''){
        $branchid_admin=$_COOKIE['branchid'];
        $branchname1=mysqli_fetch_assoc(mysqli_query($con,"SELECT branchname x FROM branchmaster WHERE branchid='$branchid_admin'"))['x'];
        echo '<option value="'.$branchid_admin.'">'.$branchname1.'</option>';
      }
        echo '<option value="allbranch">All Branches</option>';
      $select1=mysqli_query($con,"SELECT branchid, branchname FROM branchmaster WHERE branchid<>'$branchid_admin'");
      while($rows = mysqli_fetch_assoc($select1)){
        echo '<option value="'.$rows['branchid'].'">'.$rows['branchname'].'</option>';
      }
    
  echo '</select>';
    }
?>
<div class="table-ui container-fluid">
    <div class="tr row">
        <div class="col-sm-1 th">Sr No.</div>
        <div class="col-sm-2 th">Item Name</div>
        <div class="col-sm-1 th">Stock Qty</div>
        <div class="col-sm-1 th">Stock Unit</div>
        <div class="col-sm-1 th">Opening Qty</div>
        <div class="col-sm-1 th">Reveice Qty</div>
        <div class="col-sm-1 th">Issue Qty</div>
        <div class="col-sm-1 th">Transfer In</div>
        <div class="col-sm-1 th">Transfer Out</div>
        <div class="col-sm-1 th">Adjust (+) qty</div>
        <div class="col-sm-1 th">Adjust (-) qty</div>
        </div>
        <?php
        $counter=1;
    if($_SESSION['role']=='admin'){
        $result=mysqli_query($con,"SELECT DISTINCTROW itemcode FROM `livestockmaster` WHERE branchid='$branchid_admin' ORDER BY id DESC"); 
     }else{
        $result=mysqli_query($con,"SELECT DISTINCTROW itemcode FROM `livestockmaster` WHERE branchid='$branchid' ORDER BY id DESC"); 
     }
             
              while($rows=mysqli_fetch_assoc($result)){
                $itemcode=$rows['itemcode'];
                $itemname=mysqli_fetch_assoc(mysqli_query($con,"SELECT itemname x FROM stockmaster WHERE stockid='$itemcode'"))['x'];
                $unit1=mysqli_fetch_assoc(mysqli_query($con,"SELECT unit1 x FROM stockmaster WHERE stockid='$itemcode'"))['x'];
                if($_SESSION['role']=='admin'){
                $result1=mysqli_query($con,"SELECT * FROM `livestockmaster` WHERE branchid='$branchid_admin' AND itemcode='$itemcode' ORDER BY id DESC LIMIT 1");
                }else{
                $result1=mysqli_query($con,"SELECT * FROM `livestockmaster` WHERE branchid='$branchid' AND itemcode='$itemcode' ORDER BY id DESC LIMIT 1");
                } 
                while($rowss=mysqli_fetch_assoc($result1)){
                    ?>
        <div class="tr row">
            <div class="col-sm-1 td"><?php echo $counter; ?></div>
            <div class="col-sm-2 td"><?php echo $itemname; ?></div>
            <div class="col-sm-1 td stockqty"><?php echo $rowss['total']; ?></div>
            <div class="col-sm-1 td"><?php echo $unit1; ?></div>
            <div class="col-sm-1 td "><?php echo $rowss['opning']; ?></div>
            <div class="col-sm-1 td "><?php echo $rowss['quality']; ?></div>
            <div class="col-sm-1 td "><?php echo $rowss['issue']; ?></div>
            <div class="col-sm-1 td "><?php echo $rowss['inqty']; ?></div>
            <div class="col-sm-1 td "><?php echo $rowss['outqty']; ?></div>
            <div class="col-sm-1 td "><?php echo $rowss['adjust_plus']; ?></div>
            <div class="col-sm-1 td "><?php echo $rowss['adjust_minus']; ?></div>
        </div>
                    <?php
                }
            }
        ?>
</div>

<?php 
include('footer.php');
?>
<script>

function issueqty(e){
   var ourqty=$(e).parent().parent().find('.ourqty').val();
   var govqty=$(e).parent().parent().find('.govqty').val();
   var stockqty=$(e).parent().parent().find('.stockqty').text();
   var itemcode=$(e).parent().parent().find('.itemcode').text();
    if (confirm("Are You Sure TO Confirm Order?")) {
        var ids = $(e).data('ids');
        $.ajax({
            type: "POST",
            data: 'ourqty=' + ourqty+'&govqty='+govqty+'&stockqty='+stockqty+'&itemcode='+itemcode,
            url: 'api/issueqty.php',
            cache: false,
            success: function (res) {
                if (res.status == 'success') {
                    window.location.reload();
                }
            }
        })
    }
}

</script>