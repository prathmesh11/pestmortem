<?php 
    $base='../../';
    $navenq5='background:#1B1464;';
    include('header.php');

    if (!in_array('Material Receive', $arrysession)) {
   
        echo "<script>window.location.href='/process/dashboard.php';</script>";
       
        exit;

    }
    
    if (session_status() == PHP_SESSION_NONE) { session_start(); }
    $sessionby = $_SESSION['employeeid'];
    $branchid  = $_SESSION['branchid'];
    if(isset($_GET['serviceid'])) {
        $serviceid=$_GET['serviceid'];   
    }else {
        header('Location:  "'.$base.'"/process/stock/material-receive.php');
    }
?>
<style>
    .table-list td,
    .table-list th {
        border: 1px solid #ddd;
        padding: 1px !important;
        font-size: 13px;
    }

    .table-list td {
        padding-top: 2px !important;
        margin: 0;

    }

    .table-list tr:nth-child(even) {
        background-color: #f2f2f2;
    }

    .table-list th {
        padding-top: 5px;
        padding-bottom: 5px;
        text-align: center;
        background-color: #16a085;
        color: white;
    }
</style>
<?php   
   $admin_branch = '';
   if($_SESSION['role']=='admin'){
        $branchid_admin = '';
        echo '<select onchange="branchadmin(this)" class="form-control input-sm" style="margin:2px;">';
        if($_COOKIE['branchid']!=''){
            $branchid_admin = $_COOKIE['branchid'];
            $branchname1    = mysqli_fetch_assoc(mysqli_query($con,"SELECT branchname x FROM branchmaster WHERE branchid='$branchid_admin'"))['x'];
            echo '<option value="'.$branchid_admin.'">'.$branchname1.'</option>';
        }
            // echo '<option value="allbranch">All Branches</option>';
        $select1 = mysqli_query($con,"SELECT branchid, branchname FROM branchmaster WHERE branchid<>'$branchid_admin'");
        while($rows = mysqli_fetch_assoc($select1)){
            echo '<option value="'.$rows['branchid'].'">'.$rows['branchname'].'</option>';
        }    
        echo '</select>';
    }
?>

<?php
       if($_SESSION['role']=='admin'){
        echo '<input id="ip-branch" class="hidden" value="'.$branchid_admin.'">';
       }else{
        echo '<input id="ip-branch" class="hidden" value="'.$branchid.'">';
       }
?>

<div class="table-ui container-fluid">
    <div class="tr row">
        <div class="col-sm-2 th">Sr No.</div>
        <div class="col-sm-6 th">Item Name</div>
        <div class="col-sm-2 th">Balance Quntity</div>
        <div class="col-sm-2 th">Action</div>
    </div>
    <div class="tr row abc">
        <div class="col-sm-2 td">1</div>
        <div class="col-sm-6 td selectbox">
            <select class="form-control input-sm select-js1 itemcode" onChange="itemselect(this)">
                <option value="Select">Select</option>
                <?php 
                //   $serviceid =$_GET['serviceid'];
                //  if($serviceid!=''){
                //        $query=mysqli_query($con,"SELECT DISTINCTROW itemid FROM `batchnos` WHERE serviceid LIKE '%$serviceid%' ORDER BY id DESC");
                //      }else{
                //         $query=mysqli_query($con,"SELECT  stockid FROM `stockmaster` ORDER BY id DESC");
                //      }   
                    $result=mysqli_query($con,"SELECT  stockid FROM `stockmaster` ORDER BY id DESC");
                    while($rows=mysqli_fetch_assoc($result)) {
                            $itemcode=$rows['stockid'];
                            
                            $itemname=mysqli_fetch_assoc(mysqli_query($con,"SELECT itemname x FROM stockmaster WHERE stockid='$itemcode'"))['x'];
                            echo '<option value="'.$itemcode.'">'.$itemname.'</option>';
                    }
                ?>
            </select>
        </div>
         <div class="col-sm-2 td stockqty"></div>

        <div class="col-sm-2 td addbatches">

        </div>
    </div>
    <input id="inp-qty1" class="hidden" value="">
    <input id="inp-serviceid" class="hidden" value=<?php echo $_GET['serviceid']; ?>>

</div>

<!--------------------------Model Start--------------------------->
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <input type="hidden" id="inp-id">
                <h5 class="modal-title">Batch number for item : <input type="text" id="inp-itemname"
                        style="border: none" readonly></h5>
            </div>
            <div class="modal-body">
                <div class="row">
                    <input type="hidden" id="inp-itemid">
                    <input type="hidden" id="inp-branchid">
                    <table class="table table-list" id="table">
                        <thead>
                            <tr>
                                <th>Batch No</th>
                                <th>Quantity</th>
                                <th>Received Quantity</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
                <div>
                    <button type="button" class="btn btn-primary btn-block" onclick="addbatch()">Add Batch</button>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary btn-block btn-success"
                    onclick="savetobatchno()">Submit</button>
            </div>
        </div>

    </div>
</div>
<?php 
include('footer.php');
?>
<script>
// var serviceid = $('#inp-serviceid').val();
// if (serviceid != "") {
//     // $('#btn-back').show();
//     itemname();
// } else {
//     // $('#btn-back').hide();
// }
$(document).ready(function () {
    var serviceid = $('#inp-serviceid').val();
    if (serviceid != "") {
        $.ajax({
            type: "POST",
            data: 'serviceid=' + serviceid,
            url: 'api/itemname.php',
            cache: false,
            success: function (res) {
                if (res.status == 'success') {
                    var json = res.json;
                    var count = 2;
                    $('.table-ui > .abc .itemcode').attr('disabled', "disabled");
                    json.forEach(myFunction);
                    function myFunction(item, index, arr) {
                        $(".table-ui").append('<div class="tr row"><div class="col-sm-2 td"> ' + count + '</div><div class="col-sm-6 td"> ' + item.itemname + ' </div><div class="col-sm-2 td"> ' + item.stock_qty + ' </div> <div class="col-sm-2 td batchadd"><button class="btn btn-sm btn-block btn-info batch" title="batch Added" data-itemid="' + item.itemid + '" data-itemname="' + item.itemname + '" data-branch="' + item.branchid + '" onclick="getbatch(this)">Batch</button>         <a class="btn btn-sm btn-warning btn-block" id="btn-back" href="/process/crm/job-complete.php" >Back</a></div></div>')
                        count++;
                    }
                }
            }
        })
    }
});


//---show batch button And Balance Quantity From Stock---//
function itemselect(e) {
    var itemid = $('.itemcode option:selected').val();
    var itemname = $('.itemcode').find(":selected").text();
    var branchid = $("#ip-branch").val();
    console.log(branchid);

    $(".addbatches").html('<button class="btn btn-sm btn-block btn-info batch" title="batch Added" data-itemid="' + itemid + '" data-itemname="' + itemname + '" data-branch="' + branchid + '" onclick="getbatch(this)" >Batch</button>')
    //  $(".addbatches table tbody").empty();
    // $(".btqty table tbody").empty();
    // $("ip-itemid").val(itemid);
    $.ajax({
        type: "POST",
        data: 'itemid=' + itemid + '&branchid=' + branchid,
        url: 'api/getbalstockmatreceive.php',
        cache: false,
        success: function (res) {
            if (res.status == 'success') {
                var json = res.json;
                if (json != null) {
                    $(".stockqty").text(json);
                } else {
                    $(".stockqty").text('0.00');
                }
            }
        }
    })
}


//---collect exiting batch ---//

function getbatch(e) {
    $('#table tbody').empty();
    var itemid = $(e).data('itemid');
    var itemname = $(e).data('itemname');
    var branchid = $(e).data('branch');
    // var branchid =  $(e).attr('data-branch');
    console.log(branchid);

    $("#inp-itemname").val(itemname);
    $("#inp-itemid").val(itemid);
    $("#inp-branchid").val(branchid);
    if (itemid) {
        $('#myModal').modal();
        $.ajax({
            type: "POST",
            data: 'itemid=' + itemid + '&branchid=' + branchid,
            url: 'api/selectbatchmateriareceived.php',
            cache: false,
            success: function (res) {
                if (res.status == 'success') {
                    var json = res.json;
                    console.log(json);

                    json.forEach(myFunction);

                    function myFunction(item, index, arr) {
                        $('#myModal').modal();
                        $("#table tbody").append('<tr class="batchno"><td align="center"><input type="text"class="clsbatch" value=' + item.batchno + ' readonly></td><td align="center"><input type="number" class="clsqty" value=' + item.quntity + '  readonly></td><td align="center"><input type="number" class="receivedqty" value="0"></td></tr>')
                    }
                }
                $('#myModal').modal();
            }
        })
    }
}


//---collect Save Batch ---//

function savetobatchno() {
    var arr = [];
    var itemid = $("#inp-itemid").val();
    var valid = true;
    var totalqty = 0;
    var branchid = $("#inp-branchid").val();
    console.log(branchid);

    $("#table tbody tr").each(function () {
        totalqty += parseFloat($(this).find('td input.receivedqty').val());
    })

    $("#table tbody tr").each(function () {
        if ($(this).find('td input.clsbatch').val() != '' && $(this).find('td input.receivedqty').val() != '' && $(this).find('td input.receivedqty').val() >= '0') {

            arr.push({
                purchaseid: $("#purchaseid").val(),
                batchno: $(this).find('td input.clsbatch').val(),
                qty: $(this).find('td input.clsqty').val(),
                receivedqty: $(this).find('td input.receivedqty').val()
            });
            valid = valid * true;
        } else {
            alert("Something went wrong!")
            valid = valid * false;
        }
    })

    if (arr != '') {
        valid = valid * true;
    } else {
        valid = valid * false;
    }
    if (valid) {
        $.ajax({
            type: "POST",
            data: 'branchid=' + branchid + '&serviceid=' + $('#inp-serviceid').val() + '&itemcode=' + itemid + '&totalreceivedqty=' + totalqty + '&batches=' + JSON.stringify(arr),
            url: 'api/receiveqty.php',
            cache: false,
            success: function (res) {
                if (res.status == 'success') {
                    $('#myModal').modal('hide');

                    if ($('#inp-serviceid').val() != '') {

                        swal({
                            type: 'success',
                            title: 'Material Receive Successfull',
                            showConfirmButton: false,
                            timer: 1000
                        });

                        setTimeout(function () {

                            window.location.href = "/process/crm/job-complete.php";

                        }, 1000);

                    } else {

                        swal({
                            type: 'success',
                            title: 'Material Receive Successfull',
                            showConfirmButton: false,
                            timer: 1000
                        });
                        setTimeout(function () {
                            location.reload();
                        }, 1000);

                    }

                }
            }
        })
    }
}

//---collect add batch Manually ---//
function addbatch() {
    $("#table tbody").append('<tr class="batchno"><td align="center"><input type="text"class="clsbatch"></td><td align="center"><input type="text" class="clsqty" value="0" readonly></td><td align="center"><input type="text" class="receivedqty" value="0"></td><td><button onclick="remover(this)" class="btn-danger btn-sm"  align="center" style="padding: 1px 10px">Remove</button></td></tr>')
    $(".btn-remove").on('click', function () {
        $(this).parent().parent().remove();
    })
}

//---Select Box ---//
$(document).ready(function () {
    $('.select-js1').select2({
        width: '100%'
    });
    $('.select').attr('style', 'width:100%!important;');
});


</script>