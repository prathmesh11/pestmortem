<?php 
    $base='../../';
    $navenq1='background:#1B1464;';
    include('header.php');

    if (!in_array("Material Issue", $arrysession)) {
   
        echo "<script>window.location.href='/process/dashboard.php';</script>";
       
        exit;

    }

    if (session_status() == PHP_SESSION_NONE) { session_start(); }
    $sessionby = $_SESSION['employeeid'];
    $branchid = $_SESSION['branchid'];
    
    $serviceid='';

    if(isset($_GET['serviceid'])) {
        $serviceid=$_GET['serviceid'];   
    }else {
        header('Location:  "'.$base.'"/process/stock/material-issue.php');
    }

?>
<style>
.table-list td,
.table-list th {
    border: 1px solid #ddd;
    padding: 1px !important;
    font-size: 13px;
}

.table-list td {
    padding-top: 2px !important;
    margin: 0;

}

.table-list tr:nth-child(even) {
    background-color: #f2f2f2;
}

.table-list th {
    padding-top: 5px;
    padding-bottom: 5px;
    text-align: center;
    background-color: #16a085;
    color: white;
}
</style>
<?php   
   $admin_branch='';
  if($_SESSION['role']=='admin'){
      $branchid_admin='';
      echo '<select onchange="branchadmin(this)" class="form-control input-sm" style="margin:2px;">';
      if($_COOKIE['branchid']!=''){
        $branchid_admin=$_COOKIE['branchid'];
        $branchname1=mysqli_fetch_assoc(mysqli_query($con,"SELECT branchname x FROM branchmaster WHERE branchid='$branchid_admin'"))['x'];
        echo '<option value="'.$branchid_admin.'">'.$branchname1.'</option>';
      }
        // echo '<option value="allbranch">All Branches</option>';
      $select1=mysqli_query($con,"SELECT branchid, branchname FROM branchmaster WHERE branchid<>'$branchid_admin'");
      while($rows = mysqli_fetch_assoc($select1)){
        echo '<option value="'.$rows['branchid'].'">'.$rows['branchname'].'</option>';
      }
    
  echo '</select>';
    }
?>

<?php
       if($_SESSION['role']=='admin'){
        echo '<input id="ip-branch" class="hidden" value="'.$branchid_admin.'">';
       }else{
        echo '<input id="ip-branch" class="hidden" value="'.$branchid.'">';
       }
    ?>

<div class="table-ui container-fluid">
    <div class="tr row">
        <div class="col-sm-1 th">Sr No.</div>
        <div class="col-sm-4 th">Item Name</div>
        <div class="col-sm-1 th">Balance Quntity</div>
        <div class="col-sm-1 th">Our issue Qty</div>
        <div class="col-sm-4 th">Select Batch</div>
        <div class="col-sm-1 th">Action</div>
    </div>
    <div class="tr row">
    <?php
       if($_SESSION['role']=='admin'){
        echo '<input id="ip-itemid" class="hidden" value="'.$branchid_admin.'">';
       }else{
        echo '<input id="ip-itemid" class="hidden" value="'.$branchid.'">';
       }
    ?>

   
        <div class="col-sm-1 td">1</div>
        <div class="col-sm-4 td">
        <select class="form-control input-sm select-js1 ourqty" onChange="itemselect(this)" >
            <option value="Select" >Select</option>
            <?php 
             if($_SESSION['role']=='admin'){
            $result=mysqli_query($con,"SELECT DISTINCTROW itemcode FROM `livestockmaster` WHERE branchid='$branchid_admin' ORDER BY id DESC");
             }else{
                $result=mysqli_query($con,"SELECT DISTINCTROW itemcode FROM `livestockmaster` WHERE branchid='$branchid' ORDER BY id DESC");
             }   
            while($rows=mysqli_fetch_assoc($result)) {
                        $itemcode=$rows['itemcode'];
                        
                        $itemname=mysqli_fetch_assoc(mysqli_query($con,"SELECT itemname x FROM stockmaster WHERE stockid='$itemcode'"))['x'];

                    echo '<option value="'.$itemcode.'">'.$itemname.'</option>';
                }
            ?>
        </select>
        </div>
        <div class="col-sm-1 td stockqty"></div>
        <div class="col-sm-1 td">
        <input type="text" onkeyup="qtytotalissue(this)" class="form-control input-sm issue-item" readonly>
        </div>
        <div class="col-sm-4 td batches">
        <table class="table">
                <thead>
                    <tr>
                    <td>BatchNo</td>
                    <td align="right">Quntity</td>
                    <td align="center">Issue</td>
                    </tr>
                </thead>
                <tbody>
                </tbody>
        </table>
        </div>
        <div class="col-sm-1 td">
        <button class="btn btn-sm btn-primary btn-block" onclick="issueqty(this)">Issue</button>
        <a class="btn btn-sm btn-warning btn-block" id="btn-back" href="/process/crm/job-allotment.php" >Back</a>
        </div>
    </div>
    <input id="inp-qty1" class="hidden" value="">
    <input id="inp-serviceid" class="hidden" value=<?php echo $_GET['serviceid']; ?>>
</div>
<?php 
include('footer.php');
?>
<script>

    function qtytotalissue(e) {
        var totalrequired = parseFloat($(e).val());
        var totalavailble = $(".stockqty").text();
        if (totalrequired > totalavailble) {
            alert("Required Quntity Not availble !...");
            $(e).val("");
        }
        $(".issueqty-btch").each(function () {
            $(".issueqty-btch").attr("readonly", false);
        })
    }

    function itemselect(e) {
        var itemid = $('.ourqty option:selected').val();
        var branchid = $("#ip-branch").val();
        $(".batches table tbody").empty();
        $(".btqty table tbody").empty();
        $("ip-itemid").val(itemid);
        $.ajax({
            type: "POST",
            data: 'itemid=' + itemid + '&branchid=' + branchid,
            url: 'api/getbalstock.php',
            cache: false,
            success: function (res) {
                if (res.status == 'success') {
                    var json = res.json;
                    if (json != 0) {
                        $(".stockqty").text(json);
                        $(".issue-item").val('');
                        $(".issue-item").attr("readonly", false);
                        var json2 = res.json2;
                        if (json2 == "") {
                            $(".batches table tbody").append('<tr><td class="batchname"><input type="text" placeholder="Enter batch" class="form-control input-xs inp-batchname" style="width=50%"></td><td align="right" class="quntity"></td><td><input type="text" value="0" onkeyup="chekavailble(this)" class="form-control input-xs issueqty-btch" style="width=50%,aling:right"></td></tr>')
                        } else {
                            json2.forEach(myFunction)

                            function myFunction(item, index, arr) {
                                var id = item.id;
                                var bactno = item.batchno;
                                var quntity = item.quntity;
                                json3 = {
                                    "id": id,
                                    "batchno": bactno,
                                    "quntity": quntity
                                };
                                $(".batches table tbody").append('<tr><td class="batchname"><input type="text" placeholder="Enter batch" class="form-control input-xs inp-batchname" style="width=50%" value="' + bactno + '" readonly></td><td align="right" class="quntity">' + quntity + '</td><td><input type="text" value="0" onkeyup="chekavailble(this)" class="form-control input-xs issueqty-btch" style="width=50%,aling:right" readonly></td></tr>')
                            }
                        }
                    } else {
                        $(".stockqty").text('0.00');
                        $(".issue-item").attr("readonly", true);
                    }
                }
            }
        })
    }
    $(document).ready(function () {
        $('.select-js1').select2({
            width: '100%'
        });

        $('.select').attr('style', 'width:100%!important;');
    });

    function chekavailble(e) {
        var qty10 = 0;
        var availableinbatch = parseFloat($(e).parent().prev().text());
        var requiredfrombatch = parseFloat($(e).val());
        var qty11 = parseFloat($(".issue-item").val());
        if (requiredfrombatch > availableinbatch) {
            alert("Required quntity Not available");
            $(e).val(0);
        }
        qty10 = qty10 + parseFloat($(e).val());
        if (qty10 > qty11) {
            alert('Something Went Wrong..');
            $(e).val(0);
        }
        var qty1 = 0;
        $('.issueqty-btch').each(function () {
            var qty100 = parseFloat($(this).val());

            if (isNaN(qty100)) {
                qty100 = 0;
                $(this).val(0);
            }
            qty1 = qty1 + qty100;
            if (qty1 > qty11) {
                alert('Something Went Wrong..');
                $(e).val(0);
            }
        })
        $("#inp-qty1").val(qty1);
        if (qty1 > qty11) {
            alert('Something Went Wrong..');
            $(e).val(0);
        }

    }

    function issueqty(e) {
        var ourqty = parseFloat($(".issue-item").val());
        var stockqty = parseFloat($('.stockqty').text());
        var itemcode = $('.ourqty option:selected').val();
        if (ourqty > stockqty) {
            alert("Something went wrong.....")
        } else {
            var batchname = '';
            var batchsum = $("#inp-qty1").val();
            if (batchsum == ourqty) {
                var batches = [];
                $(".table > tbody > tr").each(function () {
                    batchname = $(this).find('.inp-batchname').val();
                    if (batchname != "") {
                        batches.push({
                            "batchname": $(this).find('.inp-batchname').val(),
                            "quntity": $(this).find('.quntity').text().trim(),
                            "issueqty": $(this).find('.issueqty-btch').val()
                        });
                    } else {
                        alert("Enter Batch number");
                        return false;
                    }
                })
                if (batchname != "") {
                    if (confirm("Are You Sure TO Confirm Order?")) {
                        $.ajax({
                            type: "POST",
                            data: 'branchid=' + $('#ip-branch').val() + '&serviceid=' + $('#inp-serviceid').val() + '&ourqty=' + ourqty + '&stockqty=' + stockqty + '&itemcode=' + itemcode + '&batches=' + JSON.stringify(batches),
                            url: 'api/issueqty.php',
                            cache: false,
                            success: function (res) {
                                if (res.status == 'success') {
                                    //    window.location.reload();

                                    if ($('#inp-serviceid').val() != '') {

                                        swal({
                                            type: 'success',
                                            title: 'Material Issue Successfull',
                                            showConfirmButton: false,
                                            timer: 1000
                                        });

                                        setTimeout(function () {

                                            window.location.href = "/process/crm/job-allotment.php";

                                        }, 1000);
                                        
                                    } else {

                                        swal({
                                            type: 'success',
                                            title: 'Material Issue Successfull',
                                            showConfirmButton: false,
                                            timer: 1000
                                        });
                                        setTimeout(function () {
                                            location.reload();
                                        }, 1000);

                                    }

                                    
                                }
                            }
                        })
                    }
                }

            } else {
                alert("Something Went Wrong....")
            }
        }


    }


    var serviceid = $('#inp-serviceid').val();
    if (serviceid != "") {
        $('#btn-back').show();
    } else {
        $('#btn-back').hide();
    }
</script>