<?php 
$base='../../';
$navenq2='background:#1B1464;';
include('header.php');

if (!in_array('Branch To Branch Trasfer', $arrysession) ) {
   
    echo "<script>window.location.href='/process/dashboard.php';</script>";
   
    exit;

}

    if (session_status() == PHP_SESSION_NONE) { session_start(); }
    $sessionby = $_SESSION['employeeid'];
    $branchid  = $_SESSION['branchid'];
?>
<style>
    .table-list td,
    .table-list th {
        border: 1px solid #ddd;
        padding: 1px !important;
        font-size: 13px;
    }

    .table-list td {
        padding-top: 2px !important;
        margin: 0;

    }

    .table-list tr:nth-child(even) {
        background-color: #f2f2f2;
    }

    .table-list th {
        padding-top: 5px;
        padding-bottom: 5px;
        text-align: center;
        background-color: #16a085;
        color: white;
    }


    .fade-rotate {
        transform: rotate(180deg);
        opacity: 0;
        -webkit-transition: all .25s linear;
        -o-transition: all .25s linear;
        transition: all .25s linear;
    }

    .fade-rotate.in {
        opacity: 1;
        transform: rotate(0deg);
    }

    .fade-rotate .modal-dialog {
        position: absolute;
        left: 0;
        right: 0;
        top: 50%;
        transform: translateY(-50%) !important;
    }
    

</style>
<?php   
   $admin_branch='';
   if($_SESSION['role']=='admin'){

        $branchid_admin = '';
        echo '<select onchange="branchadmin(this)" class="form-control input-sm" style="margin:2px;">';
        if($_COOKIE['branchid']!=''){

            $branchid_admin = $_COOKIE['branchid'];
            $branchname1    = mysqli_fetch_assoc(mysqli_query($con,"SELECT branchname x FROM branchmaster WHERE branchid='$branchid_admin'"))['x'];
            echo '<option value="'.$branchid_admin.'">'.$branchname1.'</option>';
        }
            // echo '<option value="allbranch">All Branches</option>';
            $select1 = mysqli_query($con,"SELECT branchid, branchname FROM branchmaster WHERE branchid<>'$branchid_admin'");
            while($rows = mysqli_fetch_assoc($select1)){
            echo '<option value="'.$rows['branchid'].'">'.$rows['branchname'].'</option>';
            }
        
        echo '</select>';
    }
?>

<?php
       if($_SESSION['role']=='admin'){
        echo '<input id="ip-branch" class="hidden" value="'.$branchid_admin.'">';
       }else{
        echo '<input id="ip-branch" class="hidden" value="'.$branchid.'">';
       }
?>

<div class="table-ui container-fluid">
    <div class="tr row">
        <div class="col-sm-1 th">Sr No.</div>
        <div class="col-sm-3 th">Item Name</div>
        <div class="col-sm-1 th">Balance Quntity</div>
        <div class="col-sm-3 th">Select Batch</div>
        <div class="col-sm-2 th">Select Branch</div>
        <div class="col-sm-2 th">Action</div>
    </div>
    <div class="tr row">
        <?php
            if($_SESSION['role']=='admin'){
                echo '<input id="ip-itemid" class="hidden" value="'.$branchid_admin.'">';
            }else{
                echo '<input id="ip-itemid" class="hidden" value="'.$branchid.'">';
            }
        ?>


        <div class="col-sm-1 td">1</div>
        <div class="col-sm-3 td">
            <select class="form-control input-sm select-js1 itemcode" onChange="itemselect(this)">
                <option value="Select">Select</option>
                <?php 
                    if($_SESSION['role']=='admin'){
                    $result=mysqli_query($con,"SELECT DISTINCTROW itemcode FROM `livestockmaster` WHERE branchid='$branchid_admin' ORDER BY id DESC");
                    }else{
                        $result=mysqli_query($con,"SELECT DISTINCTROW itemcode FROM `livestockmaster` WHERE branchid='$branchid' ORDER BY id DESC");
                    }   
                    while($rows=mysqli_fetch_assoc($result)) {
                                $itemcode=$rows['itemcode'];
                                
                                $itemname=mysqli_fetch_assoc(mysqli_query($con,"SELECT itemname x FROM stockmaster WHERE stockid='$itemcode'"))['x'];

                     echo '<option value="'.$itemcode.'">'.$itemname.'</option>';
                }
            ?>
            </select>
        </div>
        <div class="col-sm-1 td stockqty"></div>
        <div class="col-sm-3 td batches">
            <table class="table" id="issuetable">
                <thead>
                    <tr>
                        <td>BatchNo</td>
                        <td align="right">Quntity</td>
                        <td align="center">Issue</td>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>

        <div class="col-sm-2 td">
            <?php   
                    echo '<select  class="form-control input-sm branchname" style="margin:2px;" onchange="transferbranch(this)">';
                    echo '<option value="Select">Select Branch</option>';
                    $select1=mysqli_query($con,"SELECT branchid, branchname FROM branchmaster ");
                    while($rows = mysqli_fetch_assoc($select1)){
                        echo '<option value="'.$rows['branchid'].'">'.$rows['branchname'].'</option>';
                    }
                    
                echo '</select>';
            ?>
        </div>
        <div class="col-sm-2 td addbatches">
        </div>
    </div>
    <input id="inp-qty1" class="hidden" value="">

</div>


<?php 

                   $count=2;
                    if($_SESSION['role']=='admin'){
                    $result=mysqli_query($con,"SELECT id,branchid,transfer_branch_id,itemid,batchno,quntity FROM `duplicate_batchno` WHERE branchid='$branchid_admin' AND accepted=0 AND rejected=0 ORDER BY id DESC");
                    }else{
                        $result=mysqli_query($con,"SELECT id,branchid,transfer_branch_id,itemid,batchno,quntity FROM `duplicate_batchno` WHERE branchid='$branchid' AND accepted=0 AND rejected=0 ORDER BY id DESC");
                    }   
                    while($rows=mysqli_fetch_assoc($result)) {
                                $receive_id=$rows['id'];
                                $branchid=$rows['branchid'];
                                $transfer_branch_id=$rows['transfer_branch_id'];
                                $transfer_branch_name    = mysqli_fetch_assoc(mysqli_query($con,"SELECT branchname x FROM branchmaster WHERE branchid='$transfer_branch_id'"))['x'];
                                $itemid=$rows['itemid'];
                                $itemname = mysqli_fetch_assoc(mysqli_query($con,"SELECT itemname x FROM stockmaster WHERE stockid='$itemid'"))['x'];
                                $total=mysqli_fetch_assoc(mysqli_query($con,"SELECT total x FROM `livestockmaster` WHERE itemcode='$itemid' AND branchid='$branchid' ORDER BY id DESC LIMIT 1"))['x'];                 
                                $batchno=$rows['batchno'];
                                $quntity=$rows['quntity'];
                           ?>
                           <div class="table-ui container-fluid">

                            <div class="tr row">
                                    <div class="col-sm-1 td"><?php echo $count;?></div>
                                    <div class="col-sm-1 td "  style="display:none;"><?php echo $receive_id;?></div>
                                    <div class="col-sm-1 td" id="receive_branchid" style="display:none;"><?php echo $branchid;?></div>
                                    <div class="col-sm-3 td" style="display:none;" id="receive_itemcode"><?php echo $itemid;?></div>
                                    <div class="col-sm-3 td"><?php echo $itemname;?></div>
                                    <div class="col-sm-1 td"><?php echo $total;?></div>
                                    <div class="col-sm-3 td">            
                                        <table class="table" id="receivetable">
                                            <thead>
                                                <tr>
                                                    <td>Batch No</td>
                                                    <td align="right">Received Quntity</td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                
                                             <td id="receive_batchno"><?php echo $batchno;?></td>
                                             <td align="right" id="receive_quntity"><?php echo $quntity;?></td>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="col-sm-2 td"><?php echo "Material Coming From Branch : " .$transfer_branch_name; ?></div>
                                    <div class="col-sm-2 td">
                                        <button class="btn btn-sm btn-block btn-info" data-id="<?php echo $receive_id;?>" onclick="accept_qty(this)">Accept</button> 
                                        <button class="btn btn-sm btn-block btn-danger" id="reject" data-toggle="modal" data-target="#myModal1" data-id="<?php echo $receive_id;?>">Reject</button> 
                                    </div>
                                </div>
                                </div>
                             <?php
                    $count++;
                }
     ?>

<!--------------------------Batch Model Start--------------------------->
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <input type="hidden" id="inp-itemid">
                <h5 class="modal-title">Batch number for item : <input type="text" id="inp-itemname"
                        style="border: none" readonly></h5>
                <p id="inp-qty">Total Quntity : <input type="text" id="inp-qty123" style="border: none" readonly> </p>
            </div>
            <div class="modal-body">
                <div class="row">
                    <input class="hidden" value="" id="branchid">
                    <table class="table table-list" id="table">
                        <thead>
                            <tr>
                                <th>Batch No</th>
                                <th>Quantity</th>
                                <th>Transfer Quantity</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
                <div>
                    <button type="button" class="btn btn-primary btn-block" onclick="addbatch()">Add Batch</button>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary btn-block btn-success"
                    onclick="savetobatchno()">Submit</button>
            </div>
        </div>
    </div>
</div>


<!--------------------------Reject Model Start--------------------------->
<div class="modal fade-rotate" id="myModal1" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="text-center">Material Reject</h3>
            </div>
            <div class="modal-body">
            <input type="hidden" id="receive_id">
            <textarea class="form-control" rows="5" id="reason" Placeholder="Enter Reason Why You Rejected?"></textarea>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary btn-block btn-success"
                    onclick="reject_qty()">Submit</button>
            </div>
        </div>
    </div>
</div>

<?php 
include('footer.php');
?>
<script>

//-----------------Id Store In Model Input----------------------------//
$(document).on("click", "#reject", function () {
     var receive_id = $(this).data('id');
     $("#receive_id").val(receive_id);
});

//-----------------close Model clear textarea value----------------------------//
$("#myModal1").on("hidden.bs.modal", function(){
    $("#reason").val("");
})
//-----------------Accept Quantity----------------------------//
function accept_qty(e) {
    var receive_id = $(e).attr('data-id');    
    if (receive_id != '') {
        $.ajax({
            type: "POST",
            data: 'receive_id=' + receive_id,
            url: 'api/accept.php',
            cache: false,
            success: function (res) {
                if (res.status == 'success') {
                    swal({
                        type: 'success',
                        title: 'Material Accepted Successfull',
                        showConfirmButton: false,
                        timer: 1000
                    });
                    setTimeout(function () {
                        location.reload();
                    }, 1000);        
                }
            }
        })
    }
}


//-----------------Reject Quantity----------------------------//
function reject_qty() {
    var receive_id = $('#receive_id').val();
    var reason     = $('#reason').val();
    var valid      = true;
    if (reason=='') {
        valid = valid * false;
        $('#reason').css('border-color','red');
    } else{
        valid = valid * true;
        $('#reason').css('border-color','green');
    }

    if (receive_id=='') {
        valid = valid * false;
        alert('Somthing Wents Wrong!');
    } else{
        valid = valid * true;
    }
    if (valid) {
        $.ajax({
            type: "POST",
            data: 'receive_id=' + receive_id + '&reason=' + reason,
            url: 'api/reject.php',
            cache: false,
            success: function (res) {
                if (res.status == 'success') {
                    swal({
                        type: 'success',
                        title: 'Material Rejected',
                        showConfirmButton: false,
                        timer: 1000
                    });
                    setTimeout(function () {
                        location.reload();
                    }, 1000);        
                }
            }
        })
    }
}

function qtytotalissue(e) {
    var totalrequired = parseFloat($(e).val());
    var totalavailble = $(".stockqty").text();
    if (totalrequired > totalavailble) {
        alert("Required Quntity Not availble !...");
        $(e).val("");
    }
    $(".issueqty-btch").each(function () {
        $(".issueqty-btch").attr("readonly", false);
    })
}



function itemselect(e) {
    var itemid = $('.itemcode option:selected').val();
    var branchid = $("#ip-branch").val();
    $(".batches table tbody").empty();
    $(".btqty table tbody").empty();
    $("ip-itemid").val(itemid);
    $.ajax({
        type: "POST",
        data: 'itemid=' + itemid + '&branchid=' + branchid,
        url: 'api/getbalstock.php',
        cache: false,
        success: function (res) {
            if (res.status == 'success') {
                var json = res.json;
                if (json != 0) {
                    $(".stockqty").text(json);
                    $(".issue-item").val('');
                    $(".issue-item").attr("readonly", false);
                    var json2 = res.json2;
                    if (json2 == "") {
                        $(".batches table tbody").append('<tr><td class="batchname"><input type="text" placeholder="Enter batch" class="form-control input-xs inp-batchname" style="width=50%"></td><td align="right" class="quntity"></td><td><input type="text" value="0" onkeyup="chekavailble(this)" class="form-control input-xs issueqty-btch" style="width=50%,aling:right"></td></tr>')
                    } else {
                        json2.forEach(myFunction)

                        function myFunction(item, index, arr) {
                            var id = item.id;
                            var bactno = item.batchno;
                            var quntity = item.quntity;
                            json3 = {
                                "id": id,
                                "batchno": bactno,
                                "quntity": quntity
                            };
                            $(".batches table tbody").append('<tr><td class="batchname"><input type="text" placeholder="Enter batch" class="form-control input-xs inp-batchname" style="width=50%" value="' + bactno + '" readonly></td><td align="right" class="quntity">' + quntity + '</td><td><input type="text" value="0" onkeyup="chekavailble(this)" class="form-control input-xs issueqty-btch" style="width=50%,aling:right" ></td></tr>')
                        }
                    }
                } else {
                    $(".stockqty").text('0.00');
                    $(".issue-item").attr("readonly", true);
                }
            }
        }
    })
}

//-----------------Select box Search----------------------------//
$(document).ready(function () {
    $('.select-js1').select2({
        width: '100%'
    });
    $('.select').attr('style', 'width:100%!important;');
});

//-----------------batch button display----------------------------//
function transferbranch(e) {
    var itemid = $('.itemcode option:selected').val();
    var itemname = $('.itemcode').find(":selected").text();
    var transferbranchid = $(e).val();
    console.log(transferbranchid);
    
    var totalqty = 0;
    if (transferbranchid!='Select') {
        $(".table > tbody > tr").each(function () {
        totalqty += parseFloat($(this).find('td input.issueqty-btch').val());
    })
    $(".addbatches").html('<tr><td class="batchadd"><button class="btn btn-sm btn-block btn-info batch" title="batch Added" data-itemid="' + itemid + '" data-itemname="' + itemname + '" data-branch="' + transferbranchid + '" data-totalqty="' + totalqty + '"  onclick="getbatch(this)">Batch</button></td></tr>')
    }else{
        $('.batch').hide();
    }
   
}

//-----------------Batch Display In Model----------------------------//
function getbatch(e) {
    $('#table tbody').empty();
    var itemid = $(e).data('itemid');
    var itemname = $(e).data('itemname');
    var branchid = $(e).data('branch');
    var transfertotalqty = $(e).attr('data-totalqty');
    $("#inp-itemid").val(itemid);
    $("#inp-qty123").val(transfertotalqty);
    $("#inp-itemname").val(itemname);
    $("#branchid").val(branchid);

    var batches = [];
    var valid = true;


    $("#issuetable > tbody > tr").each(function () {
        batchname = $(this).find('.inp-batchname').val();
        if (batchname != "") {
            batches.push({
                "batchno": $(this).find('.inp-batchname').val(),
                "quntity": $(this).find('.quntity').text().trim(),
                "issueqty": $(this).find('.issueqty-btch').val()
            });
        } else {
            alert("Enter Batch number");
            return false;
        }
    })

    if (batches != '') {
        valid = valid * true;
    } else {
        valid = valid * false;
    }

    if (itemid) {
        // $('#myModal').modal();
        $.ajax({
            type: "POST",
            data: 'itemid=' + itemid + '&branchid=' + branchid + '&transferbatch=' + JSON.stringify(batches),
            url: 'api/selectbatchmateriareceived1.php',
            cache: false,
            success: function (res) {
                if (res.status == 'success') {
                    var json = res.json;
                    // console.log(json);
                    json.forEach(myFunction);
                    function myFunction(item, index, arr) {
                        $('#myModal').modal();
                        $("#table tbody").append('<tr class="batchno"><td align="center"><input type="text"class="clsbatch" value=' + item.batchno + ' readonly></td><td align="center"><input type="number" class="clsqty" value=' + item.quntity + '  readonly></td><td align="center"><input type="number" class="transferqty"  onkeyup="batchentry(this)" value=' + item.issueqty + ' ></td></tr>')
                    }
                }
                $('#myModal').modal();


            }
        })
    }
}

//-----------------Add Batch Manually----------------------------//
function addbatch() {
    $("#table tbody").append('<tr class="batchno"><td align="center"><input type="text"class="clsbatch"></td><td align="center"><input type="number" class="clsqty" value="0" readonly></td><td align="center"><input type="number" class="transferqty"  onkeyup="batchentry(this)" value="0" ></td><td><button onclick="remover(this)" class="btn-danger btn-sm"  align="center" style="padding: 1px 10px">Remove</button></td></tr>')
    $(".btn-remove").on('click', function () {
        $(this).parent().parent().remove();
    })
}


//-----------------Check Model Quantity----------------------------//
function batchentry(e) {
    var qty1 = 0;
    $('.transferqty').each(function () {
        var qty100 = parseFloat($(this).val());
        if (isNaN(qty100)) {
            qty100 = 0;
            $(e).val(0);
        }
        qty1 = qty1 + qty100;
    })

    var qty = parseFloat($("#inp-qty123").val());
    if (qty1 > qty) {
        alert('Quntity must be less than item Quntity');
        $(e).val(0);
    }
}



//---------------Save Batch--------------------//
function savetobatchno() {

    var arr = [];
    var batches = [];
    var itemid = $("#inp-itemid").val();
    var valid = true;
    var qty1 = 0;
    var branchid = $("#branchid").val();

    $('.transferqty').each(function () {
        var qty100 = parseFloat($(this).val());
        if (isNaN(qty100)) {
            qty100 = 0;
            $(e).val(0);
        }
        qty1 = qty1 + qty100;
    })

    var qty = parseFloat($("#inp-qty123").val());
    if (qty1 < qty) {
        alert('Quntity must be less than item Quntity');
        valid = valid * false;
    } else {
        valid = valid * true;
    }

    $("#table tbody tr").each(function () {
        if ($(this).find('td input.clsbatch').val() != '' && $(this).find('td input.transferqty').val() != '' && $(this).find('td input.transferqty').val() >= '0') {
            arr.push({
                batchno: $(this).find('td input.clsbatch').val(),
                qty: $(this).find('td input.clsqty').val(),
                transferqty: $(this).find('td input.transferqty').val()
            });
            valid = valid * true;
        } else {
            alert("Something went wrong!")
            valid = valid * false;
        }
    })

    $("#issuetable > tbody > tr").each(function () {
        batchname = $(this).find('.inp-batchname').val();
        if (batchname != "") {
            batches.push({
                "batchname": $(this).find('.inp-batchname').val(),
                "quntity": $(this).find('.quntity').text().trim(),
                "issueqty": $(this).find('.issueqty-btch').val()
            });
        } else {
            alert("Enter Batch number");
            return false;
        }
    })
    if (batches != '') {
        valid = valid * true;
    } else {
        valid = valid * false;
    }

    if (arr != '') {
        valid = valid * true;
    } else {
        valid = valid * false;
    }
    if (valid) {
        $.ajax({
            type: "POST",
            data: 'transferbranchid=' + $('#ip-branch').val() + '&itemid=' + itemid + '&receivebranchid=' + branchid + '&receivebatch=' + JSON.stringify(arr) + '&transferbatch=' + JSON.stringify(batches) + '&totalqty=' + qty,
            url: 'api/branchtobranchtransfer.php',
            cache: false,
            success: function (res) {
                if (res.status == 'success') {
                    $('#myModal').modal('hide');
                    // location.reload();
                    swal({
                        type: 'success',
                        title: 'Material Transfer Successfully',
                        showConfirmButton: false,
                        timer: 1000
                    });
                    setTimeout(function () {
                        location.reload();
                    }, 1000);
                }
            }
        })
    }
}


//---------------Check Quantity--------------------//
function chekavailble(e) {
    var qty10 = 0;
    var availableinbatch = parseFloat($(e).parent().prev().text());

    var requiredfrombatch = parseFloat($(e).val());

    var qty11 = parseFloat($(".issue-item").val());
    if (requiredfrombatch > availableinbatch) {
        alert("Required quntity Not available");
        $(e).val(0);
    }
    qty10 = qty10 + parseFloat($(e).val());
    if (qty10 > qty11) {
        alert('Something Went Wrong..');
        $(e).val(0);
    }
    var qty1 = 0;
    $('.issueqty-btch').each(function () {
        var qty100 = parseFloat($(this).val());

        if (isNaN(qty100)) {
            qty100 = 0;
            $(this).val(0);
        }
        qty1 = qty1 + qty100;
        console.log(qty1);

        if (qty1 > qty11) {
            alert('Something Went Wrong..');
            $(e).val(0);
        }
    })

    $("#inp-qty1").val(qty1);
    if (qty1 > qty11) {
        alert('Something Went Wrong..');
        $(e).val(0);
    }
    $(e).parent().parent().parent().parent().parent().parent().find('.batch').attr('data-totalqty', qty1);
}
</script>