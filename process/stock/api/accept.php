<?php
    $base='../../../';
    include($base.'_in/connect.php');
    header('content-type: application/json; charset=utf-8');
    header("access-control-allow-origin: *");
    if(isset($_POST['receive_id'])){
     
      $con=_connect();

      if (session_status()==PHP_SESSION_NONE) { session_start(); }
      $created_by = $_SESSION['employeeid'];
      $receive_id = _clean($con,$_POST['receive_id']); 

      $duplicate_batchno=mysqli_fetch_assoc(mysqli_query($con,"SELECT branchid,transfer_branch_id,itemid,batchno,quntity FROM `duplicate_batchno` WHERE id='$receive_id' "));
      if ($duplicate_batchno) {
         $receivebranchid    = $duplicate_batchno['branchid']; 
         $transfer_branch_id = $duplicate_batchno['transfer_branch_id'];
         $itemcode           = $duplicate_batchno['itemid'];
         $batchno            = $duplicate_batchno['batchno']; 
         $stockqty           = $duplicate_batchno['quntity']; 
      }  
 
      $opning1         = 0;
      $quality1        = 0;
      $issue1          = 0;
      $adjust_plus1    = 0;
      $adjust_minus1   = 0;
      $outbranch1      = 0;
      $outqty1         = 0;
      $inbranch1       = 0;
      $inqty1          = $stockqty;
      $total1          = $stockqty;
      $govqty1         = 0;

      
      $stock=mysqli_fetch_assoc(mysqli_query($con,"SELECT * FROM `livestockmaster` WHERE itemcode='$itemcode' AND branchid='$receivebranchid' ORDER BY id DESC LIMIT 1"));
      
      if($stock){
       $opning1       = $stock['opning'];
       $quality2      = $stock['quality'];
       $adjust_plus1  = $stock['adjust_plus'];
       $adjust_minus1 = $stock['adjust_minus'];
       $outqty1       = $stock['outqty'];
       $issue1        = $stock['issue'];
       $inbranch1     = $stock['inbranch'];
       $inqty1        = $stock['inqty']+$stockqty;
       $total1        = ($opning1+$quality1+$adjust_plus1+$inqty1)-($issue1+$adjust_minus1+$outqty1);
       $query         = "INSERT into livestockmaster ( branchid,itemcode,opning,quality,issue,adjust_plus,adjust_minus,outbranch,outqty,inbranch,inqty,total,created_by,created_time) Values ('$receivebranchid','$itemcode','$opning1','$quality2','$issue1','$adjust_plus1','$adjust_minus1','$outbranch1','$outqty1','$inbranch1','$inqty1','$total1','$created_by','$CURRENT_MILLIS')";

       } else{

       $query         = "INSERT into livestockmaster ( branchid,itemcode,opning,quality,issue,adjust_plus,adjust_minus,outbranch,outqty,inbranch,inqty,total,created_by,created_time) Values ('$receivebranchid','$itemcode','$opning1','$quality1','$issue1','$adjust_plus1','$adjust_minus1','$outbranch1','$outqty1','$inbranch1','$inqty1','$total1','$created_by','$CURRENT_MILLIS')";
      
       }

       $update=mysqli_query($con,$query);

       if($update){
        mysqli_query($con,"UPDATE duplicate_batchno SET accepted='$CURRENT_MILLIS' WHERE id = '$receive_id' AND itemid='$itemcode' AND branchid='$receivebranchid'");                            

        $batchno1   = mysqli_fetch_assoc(mysqli_query($con, "SELECT batchno x FROM `batchnos` WHERE batchno='$batchno' AND itemid=$itemcode AND branchid='$receivebranchid' LIMIT 1"))['x'];
        $purchaseid = mysqli_fetch_assoc(mysqli_query($con, "SELECT purchaseid x FROM `batchnos` WHERE batchno='$batchno' AND itemid=$itemcode AND branchid='$receivebranchid' LIMIT 1"))['x'];
        $serviceid  = mysqli_fetch_assoc(mysqli_query($con, "SELECT serviceid x FROM `batchnos` WHERE batchno='$batchno' AND itemid=$itemcode AND branchid='$receivebranchid' LIMIT 1"))['x'];
        $quntity    = mysqli_fetch_assoc(mysqli_query($con, "SELECT quntity x FROM `batchnos` WHERE batchno='$batchno' AND itemid=$itemcode AND branchid='$receivebranchid'"))['x'];

        $totalqty=$quntity+$stockqty;

        if ($purchaseid) {
           mysqli_query($con,"UPDATE batchnos SET quntity='$totalqty', created_by='$created_by', created_time='$CURRENT_MILLIS'  WHERE batchno = '$batchno' AND itemid='$itemcode'AND purchaseid='$purchaseid'AND branchid='$receivebranchid'");
        }else if ($serviceid) {
           mysqli_query($con,"UPDATE batchnos SET quntity='$totalqty',created_by='$created_by', created_time='$CURRENT_MILLIS'  WHERE batchno = '$batchno' AND itemid='$itemcode'AND serviceid LIKE '%$serviceid%' AND branchid='$receivebranchid'");
        }else if ($batchno1==$batchno) {
           mysqli_query($con,"UPDATE batchnos SET quntity='$totalqty',created_by='$created_by', created_time='$CURRENT_MILLIS'  WHERE batchno = '$batchno1' AND itemid='$itemcode' AND branchid='$receivebranchid'");                            
        }else{
           mysqli_query($con,"INSERT INTO `batchnos`(`batchno`, `itemid`, `quntity`,`branchid`, `created_by`, `created_time`) VALUES ('$batchno','$itemcode','$stockqty','$receivebranchid','$created_by','$CURRENT_MILLIS')");
        }
                       
        echo '{"status":"success"}';
      } else{
        echo '{"status":"falid1"}';
     }
      _close($con);

    } else{

      echo '{"status":"falid2"}';
      
    }
?>
