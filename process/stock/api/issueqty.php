<?php
    $base='../../../';
    include($base.'_in/connect.php');
    header('content-type: application/json; charset=utf-8');
    header("access-control-allow-origin: *");
    if(isset($_POST['ourqty']) && isset($_POST['stockqty']) && isset($_POST['itemcode']) && isset($_POST['batches']) && isset($_POST['serviceid'])){
      $con=_connect();

      if (session_status()==PHP_SESSION_NONE) { session_start(); }
      $created_by   = $_SESSION['employeeid'];
      $branchid     = $_POST['branchid'];
      $ourqty       = _clean($con,$_POST['ourqty']); 
      $stockqty     = _clean($con,$_POST['stockqty']); 
      $itemcode     = _clean($con,$_POST['itemcode']); 
      $serviceid1   = _clean($con,$_POST['serviceid']); 
      $batches      = json_decode($_POST['batches']);
      $opning       = 0;
      $quality      = 0;
      $issue        = $ourqty;
      $adjust_plus  = 0;
      $adjust_minus = 0;
      $outbranch    = 0;
      $outqty       = 0;
      $inbranch     = 0;
      $inqty        = 0;
      $total        = 0;
      $govqty       = 0;
        
      $sel = mysqli_fetch_assoc(mysqli_query($con,"SELECT * FROM `livestockmaster` WHERE itemcode='$itemcode' AND branchid='$branchid' ORDER BY id DESC LIMIT 1"));                 
    
      if($sel){
          $opning       = $sel['opning'];
          $quality      = $sel['quality'];
          $adjust_plus  = $sel['adjust_plus'];
          $adjust_minus = $sel['adjust_minus'];
          $outqty       = $sel['outqty'];
          $inqty        = $sel['inqty'];
          $issue        = $sel['issue']+$issue;
          $total        = ($opning+$quality+$adjust_plus+$inqty)-($issue+$adjust_minus+$outqty);
       }
      

      
       $update=mysqli_query($con,"INSERT into livestockmaster ( branchid,itemcode,opning,quality,issue,adjust_plus,adjust_minus,outbranch,outqty,inbranch,inqty,total,created_by,created_time) Values ('$branchid','$itemcode','$opning','$quality','$issue','$adjust_plus','$adjust_minus','$outbranch','$outqty','$inbranch','$inqty','$total','$created_by','$CURRENT_MILLIS')");
     
            if($update){
                mysqli_query($con,"INSERT into issuemaster (`branchid`, `itemcode`, `stock`, `ourqty`, `govqty`, `created_by`, `created_time`) Values ('$branchid','$itemcode','$stockqty','$ourqty','$govqty','$created_by','$CURRENT_MILLIS')");


                foreach($batches as $i){

                    $quntity = get_object_vars($i)['issueqty'];

                    if($quntity<>"0"){

                        $batchno    = get_object_vars($i)['batchname'];
                        $qty        = get_object_vars($i)['quntity']-get_object_vars($i)['issueqty'];
                        $purchaseid = mysqli_fetch_assoc(mysqli_query($con, "SELECT purchaseid x FROM `batchnos` WHERE batchno='$batchno' AND itemid=$itemcode AND branchid='$branchid' LIMIT 1"))['x'];

                        if ($purchaseid!='' && $serviceid1=='') {
                            mysqli_query($con,"UPDATE batchnos SET quntity='$qty', issue_qty='$quntity', created_by='$created_by', created_time='$CURRENT_MILLIS'  WHERE batchno = '$batchno' AND itemid='$itemcode'AND purchaseid='$purchaseid'AND branchid='$branchid'");
                         }

                        $serviceid  = mysqli_fetch_assoc(mysqli_query($con, "SELECT serviceid x FROM `batchnos` WHERE batchno='$batchno' AND itemid=$itemcode AND branchid='$branchid' AND serviceid LIKE '%$serviceid1%' LIMIT 1"))['x'];
                        $issue_old  = mysqli_fetch_assoc(mysqli_query($con, "SELECT issue_qty x FROM `batchnos` WHERE batchno='$batchno' AND itemid=$itemcode AND branchid='$branchid' LIMIT 1"))['x'];
                        
                        $issue_qty=$issue_old+$quntity;
                       
                        if($serviceid1!='' && $serviceid==''){

                            $str = '';
                            $serviceid_old  = mysqli_fetch_assoc(mysqli_query($con, "SELECT serviceid x FROM `batchnos` WHERE batchno='$batchno' AND itemid=$itemcode AND branchid='$branchid' LIMIT 1"))['x'];
                           
                            if($serviceid_old){
                                $str='{"serviceid":"'.$serviceid1.'","issue":"'.$quntity.'"},'.$serviceid_old;
                            }else{
                                $str='{"serviceid":"'.$serviceid1.'","issue":"'.$quntity.'"}';
                            }
                           
                            mysqli_query($con,"UPDATE batchnos SET quntity='$qty', issue_qty='$issue_qty', created_by='$created_by', serviceid='$str', created_time='$CURRENT_MILLIS'  WHERE batchno = '$batchno' AND itemid='$itemcode'AND branchid='$branchid'");
                         }

                        if($serviceid1!='' && $serviceid!=''){

                             $str = '';
                             $service_json = json_decode('['.$serviceid.']');

                             foreach($service_json as $k){
                                $service_var = get_object_vars($k)['serviceid'];
                                $issue_var   = get_object_vars($k)['issue'];
                                if($service_var==$serviceid1){
                                    $issue_var = $issue_var+$quntity;
                                }
                                $str.=',{"serviceid":"'.$service_var.'","issue":"'.$issue_var.'"}';
                             }
                             $str=substr($str, 1);

                            mysqli_query($con,"UPDATE batchnos SET quntity='$qty', issue_qty='$issue_qty', created_by='$created_by', serviceid='$str', created_time='$CURRENT_MILLIS'  WHERE batchno = '$batchno' AND itemid='$itemcode'AND serviceid='$serviceid'AND branchid='$branchid'");
                        } 
                       
                    }
                }
             echo '{"status":"success"}';
            }else{
             echo '{"status":"falid"}';
            }
                
            _close($con);
     }else{
        echo '{"status":"falid"}';
     }
?>