<?php
    $base='../../../';
    include($base.'_in/connect.php');
    header('content-type: application/json; charset=utf-8');
    header("access-control-allow-origin: *");
    if(isset($_POST['transferbranchid']) && isset($_POST['itemid']) && isset($_POST['receivebranchid']) && isset($_POST['receivebatch']) && isset($_POST['transferbatch']) && isset($_POST['totalqty'])){
      
        $con = _connect();

      if (session_status()==PHP_SESSION_NONE) { session_start(); }
      $created_by         = $_SESSION['employeeid'];
      $transferbranchid   = _clean($con,$_POST['transferbranchid']); 
      $receivebranchid    = _clean($con,$_POST['receivebranchid']); 
      $itemcode           = _clean($con,$_POST['itemid']);
      $stockqty           = _clean($con,$_POST['totalqty']);  
      $receivebatch       = json_decode($_POST['receivebatch']);
      $transferbatch      = json_decode($_POST['transferbatch']);
      $opning             = 0;
      $quality            = 0;
      $issue              = 0;
      $adjust_plus        = 0;
      $adjust_minus       = 0;
      $outbranch          = 0;
      $outqty             = $stockqty;
      $inbranch           = 0;
      $inqty              = 0;
      $total              = $stockqty;
      $govqty             = 0;
        
      $sel=mysqli_fetch_assoc(mysqli_query($con,"SELECT * FROM `livestockmaster` WHERE itemcode='$itemcode' AND branchid='$transferbranchid' ORDER BY id DESC LIMIT 1"));                 
    
      if($sel){
          $opning       = $sel['opning'];
          $quality      = $sel['quality'];
          $adjust_plus  = $sel['adjust_plus'];
          $adjust_minus = $sel['adjust_minus'];
          $outqty       = $sel['outqty'];
          $inqty        = $sel['inqty'];
          $issue        = $sel['issue'];
          $outbranch    = $sel['outbranch'];
          $outqty       = $sel['outqty']+$stockqty;
          $total        = ($opning+$quality+$adjust_plus+$inqty)-($issue+$adjust_minus+$outqty);
          $query="INSERT into livestockmaster ( branchid,itemcode,opning,quality,issue,adjust_plus,adjust_minus,outbranch,outqty,inbranch,inqty,total,created_by,created_time) Values ('$transferbranchid','$itemcode','$opning','$quality','$issue','$adjust_plus','$adjust_minus','$outbranch','$outqty','$inbranch','$inqty','$total','$created_by','$CURRENT_MILLIS')";

       }else {
        $query = "INSERT into livestockmaster ( branchid,itemcode,opning,quality,issue,adjust_plus,adjust_minus,outbranch,outqty,inbranch,inqty,total,created_by,created_time) Values ('$transferbranchid','$itemcode','$opning','$quality','$issue','$adjust_plus','$adjust_minus','$outbranch','$outqty','$inbranch','$inqty','$total','$created_by','$CURRENT_MILLIS')";
       }
      
       $opning1             = 0;
       $quality1            = 0;
       $issue1              = 0;
       $adjust_plus1        = 0;
       $adjust_minus1       = 0;
       $outbranch1          = 0;
       $outqty1             = 0;
       $inbranch1           = 0;
       $inqty1              = $stockqty;
       $total1              = $stockqty;
       $govqty1             = 0;


       $update=mysqli_query($con,$query);

            if($update){

                foreach($transferbatch as $i){
                    $issueqty = get_object_vars($i)['issueqty'];
                    if($issueqty<>""){
                        $qty        = get_object_vars($i)['quntity']-get_object_vars($i)['issueqty'];
                        $batchname  = get_object_vars($i)['batchname'];
                        
                        $purchaseid = mysqli_fetch_assoc(mysqli_query($con, "SELECT purchaseid x FROM `batchnos` WHERE batchno='$batchname' AND itemid=$itemcode AND branchid='$transferbranchid' LIMIT 1"))['x'];
                        $serviceid  = mysqli_fetch_assoc(mysqli_query($con, "SELECT serviceid x FROM `batchnos` WHERE batchno='$batchname' AND itemid=$itemcode AND branchid='$transferbranchid' LIMIT 1"))['x'];
                         if ($purchaseid) {
                            mysqli_query($con,"UPDATE batchnos SET quntity='$qty',issue_qty='$issueqty', created_by='$created_by', created_time='$CURRENT_MILLIS'  WHERE batchno = '$batchname' AND itemid='$itemcode'AND purchaseid='$purchaseid'AND branchid='$transferbranchid'");
                         }else if ($serviceid) {
                            mysqli_query($con,"UPDATE batchnos SET quntity='$qty',issue_qty='$issueqty', created_by='$created_by', created_time='$CURRENT_MILLIS'  WHERE batchno = '$batchname' AND itemid='$itemcode'AND serviceid LIKE '%$serviceid%' AND branchid='$transferbranchid'");
                         }else{
                            mysqli_query($con,"UPDATE batchnos SET quntity='$qty',issue_qty='$issueqty', created_by='$created_by', created_time='$CURRENT_MILLIS'  WHERE batchno = '$batchname' AND itemid='$itemcode'AND branchid='$transferbranchid'");
                         }
                       
                    }
                }

                foreach($receivebatch as $i){
                    $transferqty = get_object_vars($i)['transferqty'];
                    if($transferqty<>0){
                        $qty        = get_object_vars($i)['qty']+get_object_vars($i)['transferqty'];
                        $batchno    = get_object_vars($i)['batchno'];
                           mysqli_query($con,"INSERT INTO `duplicate_batchno`(`batchno`, `itemid`, `quntity`,`branchid`,transfer_branch_id, `created_by`, `created_time`) VALUES ('$batchno','$itemcode','$transferqty','$receivebranchid','$transferbranchid','$created_by','$CURRENT_MILLIS')");                 
                    }
                }


            echo '{"status":"success"}';
            }else{
                echo '{"status":"falid"}';
            }
                
            _close($con);
     }else{
        echo '{"status":"falid"}';
     }
?>