<?php
    $base='../../../';
    include($base.'_in/connect.php');
    header('content-type: application/json; charset=utf-8');
    header("access-control-allow-origin: *");
    if(isset($_POST['branchid']) && isset($_POST['serviceid']) && isset($_POST['itemcode']) && isset($_POST['totalreceivedqty']) && isset($_POST['batches'])){
      
      $con = _connect();

      if (session_status()==PHP_SESSION_NONE) { session_start(); }
      $created_by   = $_SESSION['employeeid'];
      $branchid     = _clean($con,$_POST['branchid']); 
      $stockqty     = _clean($con,$_POST['totalreceivedqty']); 
      $itemcode     = _clean($con,$_POST['itemcode']); 
      $serviceid1    = _clean($con,$_POST['serviceid']); 
      $batches      = json_decode($_POST['batches']);
      $opning       = $stockqty;
      $quality      = 0;
      $issue        = 0;
      $adjust_plus  = 0;
      $adjust_minus = 0;
      $outbranch    = 0;
      $outqty       = 0;
      $inbranch     = 0;
      $inqty        = 0;
      $total        = 0;
      $govqty       = 0;
        
      $sel=mysqli_fetch_assoc(mysqli_query($con,"SELECT * FROM `livestockmaster` WHERE itemcode='$itemcode' AND branchid='$branchid' ORDER BY id DESC LIMIT 1"));                 
    
      if($sel){
          $opning       = $sel['opning'];
          $quality      = $sel['quality']+$stockqty;
          $adjust_plus  = $sel['adjust_plus'];
          $adjust_minus = $sel['adjust_minus'];
          $outqty       = $sel['outqty'];
          $inqty        = $sel['inqty'];
          $issue        = $sel['issue'];
          $total        = ($opning+$quality+$adjust_plus+$inqty)-($issue+$adjust_minus+$outqty);

          $query="INSERT into livestockmaster ( branchid,itemcode,opning,quality,issue,adjust_plus,adjust_minus,outbranch,outqty,inbranch,inqty,total,created_by,created_time) Values ('$branchid','$itemcode','$opning','$quality','$issue','$adjust_plus','$adjust_minus','$outbranch','$outqty','$inbranch','$inqty','$total','$created_by','$CURRENT_MILLIS')";

      }else {
        $query = "INSERT into livestockmaster ( branchid,itemcode,opning,quality,issue,adjust_plus,adjust_minus,outbranch,outqty,inbranch,inqty,total,created_by,created_time) Values ('$branchid','$itemcode','$opning','$quality','$issue','$adjust_plus','$adjust_minus','$outbranch','$outqty','$inbranch','$inqty','$opning','$created_by','$CURRENT_MILLIS')";
      }
      



       $update=mysqli_query($con,$query);
     
            if($update){

                foreach($batches as $i){
                    // $quntity = get_object_vars($i)['qty'];
                    $received   = get_object_vars($i)['receivedqty'];
                    if($received<>""){
                        $qty        = get_object_vars($i)['qty']+get_object_vars($i)['receivedqty'];
                        $batchno    = get_object_vars($i)['batchno'];
                        $batchno1   = mysqli_fetch_assoc(mysqli_query($con, "SELECT batchno x FROM `batchnos` WHERE batchno='$batchno' AND itemid=$itemcode AND branchid='$branchid' LIMIT 1"))['x'];
                        $purchaseid = mysqli_fetch_assoc(mysqli_query($con, "SELECT purchaseid x FROM `batchnos` WHERE batchno='$batchno' AND itemid=$itemcode AND branchid='$branchid' LIMIT 1"))['x'];
                       
                        if ($purchaseid!='' && $serviceid1=='') {
                            mysqli_query($con,"UPDATE batchnos SET quntity='$qty', created_by='$created_by', created_time='$CURRENT_MILLIS'  WHERE batchno = '$batchno' AND itemid='$itemcode'AND purchaseid='$purchaseid'AND branchid='$branchid'");
                         }
            
                         $serviceid     = mysqli_fetch_assoc(mysqli_query($con, "SELECT serviceid x FROM `batchnos` WHERE batchno='$batchno' AND itemid=$itemcode AND branchid='$branchid' AND serviceid LIKE '%$serviceid1%' LIMIT 1"))['x'];
                         $received_old  = mysqli_fetch_assoc(mysqli_query($con, "SELECT received_qty x FROM `batchnos` WHERE batchno='$batchno' AND itemid=$itemcode AND branchid='$branchid' LIMIT 1"))['x'];
                         
                         $received_qty=$received_old+$received;
                        
                         if($serviceid1!='' && $serviceid==''){
 
                             $str = '';
                             $serviceid_old  = mysqli_fetch_assoc(mysqli_query($con, "SELECT serviceid x FROM `batchnos` WHERE batchno='$batchno' AND itemid=$itemcode AND branchid='$branchid' LIMIT 1"))['x'];
                            
                             if($serviceid_old){
                                 $str='{"serviceid":"'.$serviceid1.'","issue":"'.$quntity.'","receive":"'.$received.'"},'.$serviceid_old;
                             }else{
                                 $str='{"serviceid":"'.$serviceid1.'","issue":"'.$quntity.'","receive":"'.$received.'"}';
                             }
                            
                             mysqli_query($con,"UPDATE batchnos SET quntity='$qty', received_qty='$received_qty', created_by='$created_by', serviceid='$str', created_time='$CURRENT_MILLIS'  WHERE batchno = '$batchno' AND itemid='$itemcode'AND branchid='$branchid'");
                          }
 
                         if($serviceid1!='' && $serviceid!=''){
 
                              $str = '';
                              $service_json = json_decode('['.$serviceid.']');
 
                              foreach($service_json as $k){
                                 $service_var = get_object_vars($k)['serviceid'];
                                 $issue_var   = get_object_vars($k)['issue'];
                                 $receive_var = get_object_vars($k)['receive'];

                                 if($service_var==$serviceid1){
                                     $receive_var = $receive_var+$received;
                                 }
                                 $str.=',{"serviceid":"'.$service_var.'","issue":"'.$issue_var.'","receive":"'.$receive_var.'"}';
                              }
                              $str=substr($str, 1);
 
                             mysqli_query($con,"UPDATE batchnos SET quntity='$qty', received_qty='$received_qty', created_by='$created_by', serviceid='$str', created_time='$CURRENT_MILLIS'  WHERE batchno = '$batchno' AND itemid='$itemcode'AND serviceid='$serviceid'AND branchid='$branchid'");
                         } 
            
                        if ($batchno1==$batchno) {
                                    mysqli_query($con,"UPDATE batchnos SET quntity='$qty', created_by='$created_by', created_time='$CURRENT_MILLIS'  WHERE batchno = '$batchno1' AND itemid='$itemcode' AND branchid='$branchid'");                            
                        }else {
                                    mysqli_query($con,"INSERT INTO `batchnos`(`batchno`, `itemid`, `quntity`, `branchid`, `created_by`, `created_time`) VALUES ('$batchno','$itemcode','$qty','$branchid','$created_by','$CURRENT_MILLIS')");
                        }  
            
                    }
                       
                }
                
               echo '{"status":"success"}';
            } else {
                echo '{"status":"falid"}';
            }
                
            _close($con);
     } else {
        echo '{"status":"falid"}';
     }
?>