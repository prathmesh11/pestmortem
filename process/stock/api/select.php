<?php
    $base='../../../';
    include($base.'_in/connect.php');
    header('content-type: application/json; charset=utf-8');
    header("access-control-allow-origin: *");
    if(isset($_POST['branchid']) && isset($_POST['certType']) && isset($_POST['fromdate']) && isset($_POST['fromdate']) && isset($_POST['todate'])) {
        $con=_connect();
        $branchid =_clean($con,$_POST["branchid"]);
        $certType =_clean($con,$_POST["certType"]);
        $fromdate =_clean($con,$_POST["fromdate"]);
        $todate =_clean($con,$_POST["todate"]);
        $query='';
        if($branchid=='Select'){$branchid='';}
        if($certType=='Select'){$certType='';}
        if($branchid=='' && $certType=='' && $fromdate!='' && $todate!=''){
            //001
            $query= "SELECT * FROM `certificatemaster` WHERE wpninserted<>'Y' AND calculated='Y' AND fumdate BETWEEN '$fromdate' AND '$todate' ORDER BY id DESC ";
        }
        if($branchid=='' && $certType!='' && $fromdate=='' && $todate==''){
            //010
            if($certType=='ALP'){
                $query= "SELECT * FROM `certificatemaster` WHERE wpninserted<>'Y' AND calculated='Y' AND certType='$certType' ORDER BY id DESC ";
            }else{
                $query= "SELECT * FROM `certificatemaster` WHERE wpninserted<>'Y' AND calculated='Y' AND certType<>'ALP' ORDER BY id DESC ";
            }
        }
        if($branchid=='' && $certType!='' && $fromdate!='' && $todate!=''){
            //011
            if($certType=='ALP'){
                $query= "SELECT * FROM `certificatemaster` WHERE wpninserted<>'Y' AND calculated='Y' AND certType='$certType' AND fumdate BETWEEN '$fromdate' AND '$todate' ORDER BY id DESC ";
            }else{
                $query= "SELECT * FROM `certificatemaster` WHERE wpninserted<>'Y' AND calculated='Y' AND certType<>'ALP' AND fumdate BETWEEN '$fromdate' AND '$todate' ORDER BY id DESC ";
            }
        }
        if($branchid!='' && $certType=='' && $fromdate=='' && $todate==''){
            //100
            $query= "SELECT * FROM `certificatemaster` WHERE wpninserted<>'Y' AND calculated='Y' AND branchid='$branchid' ORDER BY id DESC ";
        }
        if($branchid!='' && $certType=='' && $fromdate!='' && $todate!=''){
            //101
            $query= "SELECT * FROM `certificatemaster` WHERE wpninserted<>'Y' AND calculated='Y' AND branchid='$branchid' AND fumdate BETWEEN '$fromdate' AND '$todate' ORDER BY id DESC ";
        }
        if($branchid!='' && $certType!='' && $fromdate=='' && $todate==''){
            //110
            if($certType=='ALP'){
                $query= "SELECT * FROM `certificatemaster` WHERE wpninserted<>'Y' AND calculated='Y' AND branchid='$branchid' AND certType='$certType' ORDER BY id DESC ";
                
            }else{
                $query= "SELECT * FROM `certificatemaster` WHERE wpninserted<>'Y' AND calculated='Y' AND branchid='$branchid' AND certType<>'ALP' ORDER BY id DESC ";
               // echo $query;
            }
        }
        if($branchid!='' && $certType!='' && $fromdate!='' && $todate!=''){
            //110
            if($certType=='ALP'){
                $query= "SELECT * FROM `certificatemaster` WHERE wpninserted<>'Y' AND calculated='Y' AND branchid='$branchid' AND certType='$certType' AND fumdate BETWEEN '$fromdate' AND '$todate' ORDER BY id DESC ";
            }else{
                $query= "SELECT * FROM `certificatemaster` WHERE wpninserted<>'Y' AND calculated='Y' AND branchid='$branchid' AND certType<>'ALP' AND fumdate BETWEEN '$fromdate' AND '$todate' ORDER BY id DESC ";
            }
        }
        
        $select=mysqli_query($con,$query);
        $json="";
        $count=1;
        while($rows = mysqli_fetch_assoc($select)){
            $id=$rows['id'];
            $fumdate=$rows['fumdate'];
            $certType=$rows['certType'];
            
            $calculated=$rows['calculated'];
            if($certType=="ALP"){
                $commodity=$rows['alpcommodity'];
            }elseif($certType=="MBR"){
                $commodity=$rows['mbrcommodity'];
            }else{
                $commodity=$rows['commodify'];
            }
      
            if($performIn=="wpm"){
                $qty=$rows['totalwpm'];
            }else{
                $qty=$rows['qty'];
            }
            $importExport=$rows['importExport'];
            $fumdosage=$rows['fumdosage'];
            $chemichalissued=$rows['chemichalissued'];
            $mbrcommodity=$rows['mbrcommodity'];
            $performIn=$rows['performIn'];
            if($performIn=='wpm'){
                $query2="SELECT certificateno,country FROM `certificatemaster` WHERE wpnrefno='$id'";
                $result2 = mysqli_query($con,$query2);
                $certificateno="";
                $country="";
                while($rowss = mysqli_fetch_assoc($result2)){
                  $certificateno=$certificateno.''.$rowss['certificateno'].', ';
                  $country=$country.''.$rowss['country'].', ';
                }
              }else{
                $certificateno=$rows['certificateno'];
                $country=$rows['country'];
               }

            $json.=',{"fumdate":"'.$fumdate.'","id":"'.$id.'","certType":"'.$certType.'","mbrcommodity":"'.$mbrcommodity.'","performIn":"'.$performIn.'","commodity":"'.$commodity.'","chemichalissued":"'.$chemichalissued.'","fumdosage":"'.$fumdosage.'","certificateno":"'.$certificateno.'","qty":"'.$qty.'","importExport":"'.$importExport.'","country":"'.$country.'"}';
        }
        $json=substr($json,1);
        $json='['.$json.']';

        echo '{"status":"success","json":'.$json.'}';
        _close($con);
    }else{
        echo '{"status":"falid"}';
    }  
?>