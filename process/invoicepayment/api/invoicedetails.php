<?php

    $base = '../../../';

    include($base.'_in/connect.php');

    header('content-type: application/json; charset=utf-8');

    header("access-control-allow-origin: *");

    if(isset($_POST['invoiceNo']) && isset($_POST['branchid'])){

      $con = _connect();
    
      $invoice_bill = $_POST['invoiceNo'];

      $branchid     = $_POST['branchid'];

      $receiptNos   = $_POST['receiptNos'];

      $checkValid   = '';

      $result=mysqli_fetch_assoc(mysqli_query($con,"SELECT invoiceDate,contractamt FROM billing WHERE invoice_bill='$invoice_bill' AND branchid='$branchid'"));

        if($result){ 

            $invoiceDate  = $result['invoiceDate'];  
            
            $receiveAmt   = mysqli_fetch_assoc(mysqli_query($con,"SELECT SUM(receiveAmt) x FROM invoicepayment WHERE invoiceNo ='$invoice_bill' AND branchid='$branchid'"))['x'];

            //echo "SELECT SUM(receiveAmt) x FROM invoicepayment WHERE invoiceNo ='$invoice_bill' AND branchid='$branchid'";

            $oldestReceiveAmts = mysqli_fetch_assoc(mysqli_query($con,"SELECT receiveAmt x FROM invoicepayment WHERE invoiceNo ='$invoice_bill' AND branchid='$branchid' AND receipt_no = '$receiptNos'"))['x'];

            if (!$oldestReceiveAmts) {
                $oldestReceiveAmts = 0;
            }

            if ($receiveAmt == 0) {

                $totalpaidamt = 0;

                $checkValid   = 'True';

            } else {
                
                $totalpaidamt = $result['contractamt'] - $receiveAmt;

                $checkValid   = 'False';

            }

            echo '{"status":"success","invoiceDate":"'.$invoiceDate.'","invoiceAmount":"'.$result['contractamt'].'","balanceAmt":"'.round($totalpaidamt,2).'","checkValid":"'.$checkValid.'" , "oldestReceiveAmts":"'.$oldestReceiveAmts.'"}';

        } else {

            echo '{"status":"falid1"}';
        }
        
        _close($con);

    } else {

        echo '{"status":"falid"}';

     }
?>