<?php 
    
  $base    = '../../';
  
  $navenq1 = 'background:#1B1464;';

  include('header.php');

  if (!in_array("Fumigation Invoice Payment", $arrysession)) {
  
    echo "<script>window.location.href='/process/dashboard.php';</script>";
    
    exit;

  }

  if (session_status() == PHP_SESSION_NONE) { session_start(); }
  
  $sessionby       = $_SESSION['employeeid'];
  
  $branchid        = $_SESSION['branchid'];

  $receipt_nos     = GetPaymentNo($con,$branchid);


?>

<style>
.clr {
        clear:both;
    }
</style>

<br>

<input type="hidden" id="receiptNos" value="<?php echo $_GET['receiptNo']; ?>" data-role="text" class="form-control input-sm" />

<input type="hidden" id="branchid" value="<?php echo $branchid;?>">

<input type="hidden" id="storeJson">

<input type="hidden" id="getEdit" value="<?php echo $_GET['edit']; ?>" data-role="text" class="form-control input-sm" />

<div class="container-fluid">

  <div class="row" id="section">

    <div class="col-sm-1">Receipt No.</div>

    <div class="col-sm-3">

      <div class="form-group">
            
        <input class="form-control input-sm" type="text" data-role="text" id="receiptNo" data-name="receiptNo" value="<?php echo $receipt_nos;?>" readonly>
      
      </div>

    </div>

    <div class="col-sm-1">Receipt Date.</div>

    <div class="col-sm-2">

      <div class="form-group">

        <input class="form-control input-sm" type="date" data-role="date" id="receiptDate" data-name="receiptDate">

      </div>

    </div>

    <div class="col-sm-1">Customer</div>

    <div class="col-sm-4">

      <div class="form-group">

        <select class="form-control input-md select-js" onchange="invoice_bill();"  data-role="select" data-name="customerid" id="customerid">

          <option value="Select">Select Customer Name</option>

          <?php

            $result= mysqli_query($con,"SELECT customerid,customername  FROM customermaster ORDER by customerid ASC ");

              while($rows = mysqli_fetch_assoc($result)){

                echo '<option value="'.$rows['customerid'].'"> '.$rows['customername'].'</option>';

              }
                      
          ?>

        </select>

      </div>

    </div>

  </div>

  <hr style="margin:0;border:1px solid #000;">

  <div class="col-sm-12" style="margin-top: 10px;">

    <table class="table table-list form-group" id="table-order">

      <thead>

        <th>#</th>

        <th style="width:30%">Invoice No</th>

        <th>Invoice Date</th>

        <th>Invoice Amount</th>

        <th>Balance Amt</th>

        <th>Receive Amt</th>

        <th>Action</th>

      </thead>

      <tbody>
                        
        <tr>
                  
          <td></td>

          <td>

            <select class="form-control input-sm select-js" onchange="invoice_datails();"  data-role="select" data-name="invoiceNo" id="invoiceNo">

              <option value="Select">Select Invoice No</option>

            </select>

          </td>

          <td>

            <input data-name="invoiceDate" class="form-control input-sm" data-role="date" id="invoiceDate" type="date" readonly/>

          </td>

          <td>

            <input data-name="invoiceAmount" class="form-control input-sm" data-role="number" id="invoiceAmount" type="number" readonly/>
            
            <input id="oldestReceiveAmts" class="form-control input-sm" type="text" />

          </td>

          <td>

            <input data-name="balanceAmt" class="form-control input-sm" data-role="number" id="balanceAmt" type="number" readonly/>
            
            <input id="checkValid" class="form-control input-sm" type="text" />

          </td>

          <td>

            <input data-name="receiveAmt" onkeyup="checkLimit();" data-role="number" id="receiveAmt" class="form-control input-sm" type="number" />
            
            <input id="receiveAmts" class="form-control input-sm" type="number" />

          </td>


          <td>

            <button class="btn btn-sm btn-primary" id="btn-add" data-edit="" onclick="add(this)">Add</button>

          </td>

        </tr>

      </tbody>

    </table>

  </div>

  <hr style="margin:0;border:1px solid #000;">
  
  <br>
  
  <div class="row" id="section">

    <div class="col-sm-4">
                        
      <div class="form-group">

        <label for="usr">Instrument Type</label>

        <select class="form-control input-md" data-role="select" data-name="instrumentType" id="instrumentType">

          <option value="Select">Select Instrument Type</option>

          <option value="Cash">Cash</option>

          <option value="Cheque">Cheque</option>

          <option value="NEFT">NEFT</option>

        </select>

      </div>

    </div>

    <div class="col-sm-4" >
                        
      <div class="form-group">

        <label for="usr">No</label>

        <input type="text" class="form-control" data-name="instrumentNo" id="instrumentNo">

      </div>

    </div>

    <div class="col-sm-4" >
          
      <div class="form-group">

        <label for="usr">Date</label>

        <input class="form-control" type="date" data-name="instrumentDate" id="instrumentDate">

      </div>

    </div>

    <div class="col-sm-4" >
        
      <div class="form-group">

        <label for="usr">Amount</label>

        <input type="number" class="form-control" data-name="Amount" data-role="number" id="Amount" readonly>

      </div>

    </div>
  
    <div class="col-sm-8" >
          
        <div class="form-group">

          <label for="usr">Remark</label>

          <textarea type="text" class="form-control" data-name="remark" id="remark"></textarea>

        </div>

    </div>

  </div>

  <hr>

  <div class="col-sm-3"></div>

  <div class="col-sm-3">

    <button class="btn btn-sm btn-block btn-success" id="btn-submit" onclick="submit()">Submit</button>

  </div>

  <div class="col-sm-3">

    <button class="btn btn-sm btn-block btn-danger">Reset</button>

  </div>

  <hr>

</div>

<?php 
  
  include('footer.php');

?>

<script>

function invoice_bill() {

  let customerid = $('#customerid').val();

  if (customerid != 'Select') {

    $.ajax({

      type: "POST",

      data: "customerid=" + customerid + "&edit=" + $('#getEdit').val(),

      url: 'api/invoiceno.php',

      cache: false,

      success: function (res) {

        if (res.status == 'success') {

          let json = res.json;

          let str = '<option value="Select">Select Invoice No</option>';

          for (let i in json) {

            str += '<option value="' + json[i].invoice_bill + '">' + json[i].invoice_bill + '</option>';

          }

          $('#invoiceNo').html(str);

        }

      }

    })

  } else {

    //$('#invoiceNo').;

  }

}

function invoice_datails() {

  let invoiceNo = $('#invoiceNo').val();

  let branchid = $('#branchid').val();

  if (invoiceNo != 'Select') {

    $.ajax({

      type: "POST",

      data: "invoiceNo=" + invoiceNo + "&branchid=" + branchid + "&receiptNos=" + $('#receiptNos').val(),

      url: 'api/invoicedetails.php',

      cache: false,

      success: function (res) {

        if (res.status == 'success') {

          $('#invoiceDate').val(res.invoiceDate);

          $('#invoiceAmount').val(res.invoiceAmount);

          $('#balanceAmt').val(res.balanceAmt);

          $('#checkValid').val(res.checkValid);

          $('#oldestReceiveAmts').val(res.oldestReceiveAmts);

        }

      }

    })

  } else {

    $('#invoiceDate').val('');

    $('#balanceAmt').val('');

    $('#checkValid').val('');

    $('#oldestReceiveAmts').val('');

  }

}

function checkLimit() {

  let balanceAmt        = parseFloat($('#balanceAmt').val());

  let receiveAmt        = parseFloat($('#receiveAmt').val());

  let invoiceAmount     = parseFloat($('#invoiceAmount').val());

  let oldreceiveAmt     = parseFloat($('#receiveAmts').val());

  let oldestReceiveAmts = parseFloat($('#oldestReceiveAmts').val());

  let checkValid        = $('#checkValid').val();

  let total             = oldestReceiveAmts + balanceAmt;

  if(balanceAmt == 0 && checkValid == 'True'){

    if (receiveAmt > invoiceAmount) {

      $('#receiveAmt').val('');

      alert('Receive Amt Greater Than Invoice Amt');
      
    }

  } else if (receiveAmt > total ) {
      
    $('#receiveAmt').val('');

    alert('Receive Amt Greater Than Balance Amt');

  } else if (isNaN(receiveAmt)) {

    $('#receiveAmt').val(0);

  }

}

function checkamt() {

  let receiveAmt = 0;

  let count = 0;

	$('#table-order > tbody > tr').each(function () {

    if(count!=0){

      receiveAmt  += parseFloat($(this).find('.receiveAmt').text().trim());

    } else {

      count++;

    }

	})

  $('#Amount').val(receiveAmt);
  
}


// add function start :
function add(e) {

  var chk           = $(e).data('edit');
  
  var data          = checker('table-order');
  
  var invoiceNo     = data.invoiceNo;
  
  var invoiceDate   = data.invoiceDate;

  var invoiceAmount = data.invoiceAmount;
  
  var balanceAmt    = data.balanceAmt;
  
  var receiveAmt    = data.receiveAmt;

  var valid         = true;

  if (checker('table-order') != false) {

    valid = valid * true;

  } else {

    valid = valid * false;

  }


  $('#table-order .invoiceNo').each(function () {

    var invoiceNo1 = $(this).text().trim();

    if (invoiceNo1 == invoiceNo && chk == '') {

      valid = valid * false;

      alert('Dublicate invoice No');

    }

  });



  if (valid) {

    var chk = $(e).data('edit');

    if (chk == '') {

      var len = $('#table-order .srno').length + 1;

      fun_adder(len, invoiceNo, invoiceDate, invoiceAmount, balanceAmt, receiveAmt);

    } else {

      fun_adder_edit(chk, invoiceNo, invoiceDate, invoiceAmount, balanceAmt, receiveAmt);

    }

    modals.clear('table-order');

    $('#btn-add').data('edit', '');

  }
  //calculation();

}

// add function end :

// fun_adder function start :
function fun_adder(len, invoiceNo, invoiceDate, invoiceAmount, balanceAmt, receiveAmt) {

  var str = '<tr id="abc">';
          
    str += '<td align="center" class="srno">' + len + '</td>';
        
    str += '<td align="center" class="invoiceNo">' + invoiceNo + '</td>';
        
    str += '<td align="center" class="invoiceDate">' + invoiceDate + '</td>';

    str += '<td align="center" class="invoiceAmount">' + invoiceAmount + '</td>';4

    str += '<td align="center"> - </td>';
        
    str += '<td align="center" class="balanceAmt" style="display:none;">' + balanceAmt + '</td>';
        
    str += '<td align="center" class="receiveAmt">' + receiveAmt + '</td>';

    str += '<td align="center">';

    if ($('#getEdit').val() == 'true' && $('#receiptNos').val() != '') {

      let invoiceDetails = JSON.parse($('#storeJson').val());
      
      for (var i in invoiceDetails) {

        let  invoiceNos =  invoiceDetails[i].invoiceNo;

        if (invoiceNo == invoiceNos) {

          str += '<button class="btn btn-sm btn-success" onclick="editor(' + len + ',this)">E</button>';

        }

      }
    
    }
      
    str += '<button class="btn btn-sm btn-danger remover" onclick="remover(this)">R</buuton></td>';
  
  str += '</tr>';
  
  $('#table-order > tbody').append(str);

  tableChecker();

  checkamt();

}

// fun_adder function end :

// fun_adder_edit function start :
function fun_adder_edit(len, invoiceNo, invoiceDate, invoiceAmount, balanceAmt, receiveAmt) {
  
  var str = '<td align="center" class="srno">' + len + '</td>';
      
    str += '<td align="center" class="invoiceNo">' + invoiceNo + '</td>';
      
    str += '<td align="center" class="invoiceDate">' + invoiceDate + '</td>';

    str += '<td align="center" class="invoiceAmount">' + invoiceAmount + '</td>';

    str += '<td align="center"> - </td>';
      
    str += '<td align="center" class="balanceAmt" style="display:none;">' + balanceAmt + '</td>';
      
    str += '<td align="center" class="receiveAmt">' + receiveAmt + '</td>';
     
    str += '<td align="center">';

    if ($('#getEdit').val() == 'true' && $('#receiptNos').val() != '') {

      let invoiceDetails = JSON.parse($('#storeJson').val());
      
      for (var i in invoiceDetails) {

        let  invoiceNos =  invoiceDetails[i].invoiceNo;

        if (invoiceNo == invoiceNos) {

          str += '<button class="btn btn-sm btn-success" onclick="editor(' + len + ',this)">E</button>';

        }

      }
    
    }
      
    str += '<button class="btn btn-sm btn-danger remover" onclick="remover(this)">R</buuton></td>';

  $('#table-order .srno').each(function () {

    var srno = $(this).text().trim();

    if (srno == len) {

      $(this).parent().html(str);

    }

  });

  checkamt();

}

// fun_adder_edit function end :

//editor function start :
function editor(srno, e) {

  var invoiceNo = $(e).parent().parent().find('.invoiceNo').text().trim();
    
	$('#invoiceNo').val(invoiceNo).trigger('change');

  $('#invoiceDate').val($(e).parent().parent().find('.invoiceDate').text().trim());

  $('#invoiceAmount').val($(e).parent().parent().find('.invoiceAmount').text().trim());

  var totalBalance = 0;

  var receiveAmt = parseFloat($(e).parent().parent().find('.receiveAmt').text().trim());
  
  $('#receiveAmts').val(receiveAmt);

  $('#btn-add').data('edit', srno);

}

function remover(e) {

  $(e).parent().parent().remove();

  checkamt();

  tableChecker();

}

function tableChecker() {

  let tableOrderLenght = $('#table-order > tbody > tr').length - 1;

  if (tableOrderLenght == 0) {

    $('#customerid').prop('disabled', false);
      
  } else {

    $('#customerid').prop('disabled', 'disabled');

  }

  
}


function submit() {

  var valid = true;

  var arr = [];

  if (checker('section') != false) {

    valid = valid * true;

  } else {

    valid = valid * false;

  }

  $('#table-order > tbody > tr').each(function () {

    var invoiceNo     = $(this).find('.invoiceNo').text().trim();
    
    var invoiceDate   = $(this).find('.invoiceDate').text().trim();
    
    var invoiceAmount = $(this).find('.invoiceAmount').text().trim();
    
    var balanceAmt    = $(this).find('.balanceAmt').text().trim();
    
    var receiveAmt    = $(this).find('.receiveAmt').text().trim();

    arr.push({

      "invoiceNo": invoiceNo,

      "invoiceDate": invoiceDate,

      "invoiceAmount": invoiceAmount,

      "balanceAmt": balanceAmt,

      "receiveAmt": receiveAmt,


    });

  })

  arr.shift();

  if (arr == '') {

    $('#table-order').css('border', '2px solid red');
        
    valid = valid * false;

  } else {

    $('#table-order').css('border', '2px solid green');

    valid = valid * true;

  }

  if (valid) {

    var data = checker('section');

    var datastr = JSON.stringify(data);

    var invoiceDetails = JSON.stringify(arr);


    $.ajax({

      type: "POST",

      data: {
        data: datastr,
        invoiceDetails: invoiceDetails,
        branchid: $('#branchid').val(),

      },

      url: 'api/invoicepayment.php',

      cache: false,

      success: function (res) {

        if (res.status == 'success') {

          swal({
                        
              type: 'success',
              
              title: 'Payment paid',
              
              showConfirmButton: false,
              
              timer: 3000
          
          });
          
          setTimeout(function () {
              
              location.href = 'index.php';
          
          }, 3000);

        }

      }

    })

  }

}



function edit() {

  let receiptNo = $('#receiptNos').val();

  let branchid  = "<?php echo $_GET['branchid']?>";

  $.ajax({

    type: "POST",

    data: "branchid=" + branchid + "&receiptNo=" + receiptNo,

    url: 'api/invoiceselect.php',

    cache: false,

    success: function (res) {

      if (res.status == 'success') {

        $('#btn-submit').text('Update Bill');

        $('#btn-submit').attr('onclick', 'update()');
        
        $('#receiptNo').val(res.receiptNo);

        $('#receiptDate').val(res.receiptDate);

        $('#customerid').val(res.customerid).trigger("change");

        $('#instrumentNo').val(res.instrumentNo);

        $('#instrumentType').val(res.instrumentType);

        $('#instrumentDate').val(res.instrumentDate);

        $('#Amount').val(res.Amount);

        $('#remark').val(res.remark);

        $('#storeJson').val(JSON.stringify(res.json));

        let invoiceDetails = res.json;

        let couter         = 1;

        for (var i in invoiceDetails) {

          fun_adder(couter, invoiceDetails[i].invoiceNo,invoiceDetails[i].invoiceDate,invoiceDetails[i].invoiceAmount,invoiceDetails[i].balanceAmt,invoiceDetails[i].receiveAmt);

          couter++;

        }

      }

    }

  })

}

function update() {

  var valid = true;

  var arr = [];

  if (checker('section') != false) {

    valid = valid * true;

  } else {

    valid = valid * false;

  }

  $('#table-order > tbody > tr').each(function () {

    var invoiceNo     = $(this).find('.invoiceNo').text().trim();
    
    var invoiceDate   = $(this).find('.invoiceDate').text().trim();
    
    var invoiceAmount = $(this).find('.invoiceAmount').text().trim();
    
    var balanceAmt    = $(this).find('.balanceAmt').text().trim();
    
    var receiveAmt    = $(this).find('.receiveAmt').text().trim();

    arr.push({

      "invoiceNo": invoiceNo,

      "invoiceDate": invoiceDate,

      "invoiceAmount": invoiceAmount,

      "balanceAmt": balanceAmt,

      "receiveAmt": receiveAmt,


    });

  })

  arr.shift();

  if (valid) {

    var data = checker('section');

    var datastr = JSON.stringify(data);

    var invoiceDetails = JSON.stringify(arr);


    $.ajax({

      type: "POST",

      data: {
        data: datastr,
        invoiceDetails: invoiceDetails,
        branchid: $('#branchid').val(),

      },

      url: 'api/invoicepaymentupdate.php',

      cache: false,

      success: function (res) {

        if (res.status == 'success') {

          swal({
                        
              type: 'success',
              
              title: 'Payment Update',
              
              showConfirmButton: false,
              
              timer: 3000
          
          });
          
          setTimeout(function () {
              
              location.href = 'index.php';
          
          }, 3000);

        }

      }

    })

  }

}


$(document).ready(function() {

  if ($('#getEdit').val() == 'true') {

    edit();
    
  }

});


</script>