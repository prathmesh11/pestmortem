<?php 
    
    $base    = '../../';
    $navenq1 = 'background:#1B1464;';

    include('header.php');

    if (!in_array("Fumigation Invoice Payment", $arrysession)) {
   
      echo "<script>window.location.href='/process/dashboard.php';</script>";
     
      exit;

  }

  if (session_status() == PHP_SESSION_NONE) { session_start(); }

  $sessionby = $_SESSION['employeeid'];
  
  $branchid  = $_SESSION['branchid'];

  $signature = mysqli_fetch_assoc(mysqli_query($con,"SELECT signature x FROM branchmaster WHERE branchid='$branchid'"))['x'];



?>

<style>
.clr {
        clear:both;
    }
</style>

<br>
<img style="display:none;" id=imageid src="<?php echo $base;?>img/PestMortemLogo.jpg">

<input type="hidden" id=branchid value="<?php echo $branchid;?>">

<img style="display:none;" id="signature" src="<?php echo $base.$signature;?>">

<div class="table-ui container-fluid">

    <div class="tr row">

        <div class="col-sm-2 th" style="word-wrap:break-word;">Receipt No.</div>

        <div class="col-sm-1 th">Receipt Date</div>

        <div class="col-sm-2 th">Customer Name</div>

        <div class="col-sm-5 th">Summery</div>

        <div class="col-sm-2 th">Action</div>

    </div>

    <div class="row tr">

      <?php

        $result = mysqli_query($con,"SELECT DISTINCTROW  branchid,receipt_no FROM  invoicepayment WHERE confirm_time = 0 AND branchid ='$branchid' ORDER BY receipt_no ASC");

        while ($row = mysqli_fetch_assoc($result)) {

          $receipt_no = $row['receipt_no'];

          $rows = mysqli_fetch_assoc(mysqli_query($con,"SELECT * FROM  invoicepayment WHERE receipt_no='$receipt_no' "));

          $customerid = $rows['customerid'];

          $customername  = mysqli_fetch_assoc(mysqli_query($con,"SELECT customername x FROM customermaster WHERE customerid='$customerid'"))['x'];


      ?>

        <div class="col-sm-2 td" style="word-wrap:break-word;">

          <?php echo $row['receipt_no'];?>

        </div>

        <div class="col-sm-1 td" style="word-wrap:break-word;">

          <?php echo date('d-m-Y',strtotime($rows['receipt_date']));?>

        </div>

        <div class="col-sm-2 td" style="word-wrap:break-word;">

          <?php echo $customername;?>

        </div>

        <div class="col-sm-5 td" style="word-wrap:break-word;">

          <?php 

            if ($rows['invoiceNo'] !='') {

              echo '<table class="table table-list">';

                echo '<thead>';

                  echo '<tr>';

                    echo '<th>Invoice No</th>';

                    echo '<th>Invoice Date</th>';

                    echo '<th>Invoice Amount</th>';

                    // echo '<th>Balance Amt</th>';

                    echo '<th>Receive Amt</th>';

                  echo '</tr>';

                echo '</thead>';

                echo '<tbody>';

                  $results = mysqli_query($con,"SELECT invoiceNo,invoiceDate,invoiceAmount,balanceAmt,receiveAmt FROM  invoicepayment WHERE receipt_no='$receipt_no' ");

                  while ($rowss = mysqli_fetch_assoc($results)) {

                    echo '<tr>';

                      echo '<td align="center">'.$rowss['invoiceNo'].'</td>';

                      echo '<td align="center">'.date('d-m-Y',strtotime($rowss['invoiceDate'])).'</td>';

                      echo '<td align="center">'.$rowss['invoiceAmount'].'</td>';

                    //   echo '<td align="center">'.$rowss['balanceAmt'].'</td>';

                      echo '<td align="center">'.$rowss['receiveAmt'].'</td>';

                    echo '</tr>';

                  }  

                echo '</tbody>';

              echo '</table>';
              # code...
            }
          
          ?>

          <br>

          <p>Amount : <?php echo $rows['amount'];?></p>

        </div>


        <div class="col-sm-2 td" style="word-wrap:break-word;">

          <a class="btn btn-sm btn-primary btn-block" href = "payment.php?edit=true&receiptNo=<?php echo $row['receipt_no'];?>&branchid=<?php echo $row['branchid'];?>" >Edit</a>
          
          <button class="btn btn-sm btn-info btn-block" data-receiptNo="<?php echo $row['receipt_no'];?>" data-branchid="<?php echo $row['branchid'];?>" onclick="printReceipt(this)">Print</button>
         
          <button class="btn btn-sm btn-warning btn-block" data-receiptNo="<?php echo $row['receipt_no'];?>" data-branchid="<?php echo $row['branchid'];?>" onclick="confirms(this)">Confirm</button>

        </div>


      <?php    


        }

      ?>

    </div>

</div>

<a class="btn" style="font-size:25px;border-radius:50px;width:60px;height:60px;position: fixed;bottom: 40px;
    right: 40px;background:#eb2f06;color:#fff; padding-top:10px;" href="payment.php">+</a>



<?php 
include('footer.php');
?>

<script>



function confirms(e) {

  if (confirm("Are You Sure To Confirm Order?")) {

    let receiptNo = $(e).attr('data-receiptno');

    let branchid  = $(e).attr('data-branchid');

    $.ajax({

      type: "POST",

      data: 'receiptNo=' + receiptNo + '&branchid=' + branchid,

      url: 'api/confirm.php',

      cache: false,

      success: function (res) {

        if (res.status == 'success') {

          window.location.reload();

        }

      }

    })

  }

}




function getBase64Image(img) {
  var canvas = document.createElement("canvas");
  canvas.width = img.width;
  canvas.height = img.height;
  var ctx = canvas.getContext("2d");
  ctx.drawImage(img, 0, 0);
  var dataURL = canvas.toDataURL("image/png");
  return dataURL.replace('/^data:image\/(png|jpg);base64,/', "");
}

function printReceipt(e) {

  let receiptNo = $(e).attr("data-receiptNo");

  let branchid = $(e).attr("data-branchid");

  $.ajax({

    type: "POST",

    data: "branchid=" + branchid + "&receiptNo=" + receiptNo,

    url: 'api/invoiceselect.php',

    cache: false,

    success: function (res) {

      if (res.status == 'success') {


        let content = [];

        let json = res.json;

        let base64 = getBase64Image(document.getElementById("imageid"));
        let signature = getBase64Image(document.getElementById("signature"));


        content.push(
              {
              table:{
              margin: [10, 0],
              widths: ['*', '*'],
              body:[
                [
                  {
                  rowSpan: 2,
                  stack: [{image:base64,
                            width: 150,
                            height:70,
                            },],
                  },
                  {
                  rowSpan: 2, 
                  stack: [{
                    text: [
                    {text:'PEST MORTEM (INDIA) PRIVATE LIMITED\n\n',fontSize: 12,bold:1,alignment:'center'},
                    {text:'Receipt Voucher\n',fontSize: 10,alignment:'center'},
                    ]},
                    ],
                  },        
            ],
            [{}, {}],

            ]
            },layout: {
                hLineColor: function (i, node) {
                  return (i === 0 || i === node.table.body.length) ? 'white' : 'white';
                },
                vLineColor: function (i, node) {
                  return (i === 0 || i === node.table.widths.length) ? 'white' : 'white';
                },
              }
            },
        );

        content.push({
            text: '\n\n'
        });

        content.push({
            layout: 'noBorders',
            table: {
                widths: ['*', '*'],
                body: [
                    [{text: ' Receipt No : ' + res.receiptNo ,fontSize: 10,bold:1,alignment:'left'},{text: ' Receipt Date : ' + convertDate(res.receiptDate) ,fontSize: 10,bold:1,alignment:'right'}],
                ]
            }

        });

        content.push({
            text: '\n'
        });


        table1 = {
          widths: [],
          dontBreakRows: true,
          body: []
        };
        table1['widths'].push('*', 100);
        table1['body'].push([{text:'Particular',border: [true, true, true, true],bold:1,alignment:'center'},{text:'Amount',border: [true, true, true, true],bold:1,alignment:'center'}]);
        var tb4 = [];
            tb4.push({text: 'Account : \n', bold:1, alignment: 'left',border: [true, false, true, false]}, {text: '',border: [true, false, true, false]});
            table1['body'].push(tb4);

            var tb5 = [];
            tb5.push({text: res.customername+'\n\n', bold:1, margin:[10,0,0,0],alignment: 'left',border: [true, false, true, false]}, {text: '\n\n',border: [true, false, true, false]});

        table1['body'].push(tb5);
        var totalAmt = 0;

        if (json.length == 0) {

          var tb13 = [];
            tb13.push({text: 'Agst Bill No.  ' + res.Amount + ' CR ', bold:1,alignment: 'left',border: [true, false, true, false]}, {text: res.Amount,alignment: 'center',border: [true, false, true, false]});
            table1['body'].push(tb13);

            totalAmt = res.Amount;

        } else {
          for (var i in json) {
            var arr = [];
          
            arr.push({text: 'Agst Bill No. ' + json[i].invoiceNo + ' ' + json[i].receiveAmt + ' CR ',margin:[10,0,0,0],border: [true, false, true, false]}, {text:json[i].receiveAmt,alignment: 'center',border: [true, false, true, false]});
            table1['body'].push(arr);

            totalAmt +=parseFloat(json[i].receiveAmt);

          }

        }
 

        if (res.instrumentType=='Cheque' || res.instrumentType=='NEFT') {

          var tb6 = [];
            tb6.push({text: '\nThrought : ', bold:1,alignment: 'left',border: [true, false, true, false]}, {text: ' ',border: [true, false, true, false]});
            table1['body'].push(tb6);

          var tb7 = [];
            tb7.push({text: res.instrumentType,margin:[10,0,0,0], bold:1,alignment: 'left',border: [true, false, true, false]}, {text: ' ',border: [true, false, true, false]});
            table1['body'].push(tb7);

          var tb8 = [];
            tb8.push({text: 'Instrument No & Date', bold:1,alignment: 'left',border: [true, false, true, false]}, {text: ' ',border: [true, false, true, false]});
            table1['body'].push(tb8);

            var tb9 = [];
            tb9.push({text:[{text:'Cheque / dd no ' + res.instrumentNo ,alignment: 'left'},{text:' Date ' + convertDate(res.instrumentDate),alignment: 'right'}], bold:1,border: [true, false, true, false]}, {text: ' ',border: [true, false, true, false]});
            table1['body'].push(tb9);
          
        } else{

          var tb6 = [];
            tb6.push({text: '\nThrought : ' , bold:1,alignment: 'left',border: [true, false, true, false]}, {text: ' ',border: [true, false, true, false]});
            table1['body'].push(tb6);

            var tb7 = [];
            tb7.push({text: res.instrumentType, margin:[10,0,0,0],bold:1,alignment: 'left',border: [true, false, true, false]}, {text: ' ',border: [true, false, true, false]});
            table1['body'].push(tb7);

        }

        var tb9 = [];
            tb9.push({text: '\nRemark : ' , bold:1,alignment: 'left',border: [true, false, true, false]}, {text: ' ',border: [true, false, true, false]});
            table1['body'].push(tb9);

        var tb10 = [];
            tb10.push({text: res.remark, margin:[10,0,0,0],bold:1,alignment: 'left',border: [true, false, true, false]}, {text: ' ',border: [true, false, true, false]});
            table1['body'].push(tb10);

        var tb11 = [];
            tb11.push({text: '\nAmount (in words) : ' , bold:1,alignment: 'left',border: [true, false, true, false]}, {text: ' ',border: [true, false, true, false]});
            table1['body'].push(tb11);

        var tb12 = [];
            tb12.push({text: convertNumberToWords(totalAmt), margin:[10,0,0,0],bold:1,alignment: 'left',border: [true, false, true, true]}, {text: totalAmt,bold:1,border: [true, true, true, true],alignment: 'center'});
            table1['body'].push(tb12);


        content.push({
			        	style: 'tablfont',
                table: table1,
                layout: {
                hLineColor: function (i, node) {
                  return (i === 0 || i === node.table.body.length) ? 'gray' : 'gray';
                },
                vLineColor: function (i, node) {
                  return (i === 0 || i === node.table.widths.length) ? 'gray' : 'gray';
                },
              },
            });


        content.push({
            text: '\n'
        });
        content.push({

        table: {
                          widths: ['*','auto'],
                          body: [
                            [{},{text:'For Pest Mortem(India) Pvt. Ltd.',fontSize: 9,bold:1,alignment:'center'}],                  
                            [{},{image: signature,width: 100,height: 50,margin:[10,0,0,0]}],   
                            [{},{text:'Authorised Signatory',fontSize: 9,bold:1,alignment:'center'}],                  
                

                          ]
                        },layout: 'noBorders'
                    });



        dd = {

          pageSize: 'A4',

          pageOrientation: 'portrait',

          pageMargins: [40, 30, 30, 35],

          footer: function (currentPage, pageCount) {

            return {

              margin: 10,

              columns: [{

                fontSize: 9,

                text: [{

                    text: '--------------------------------------------------------------------------' +
                      '\n',
                    margin: [0, 20]

                  },
                  {


                    text: [{text:'© Pest Mortem (India) PVT. LTD. | '},{text: ' REGD OFFICE : ',bold:1},{text: 'G-2, Sunder Tower, T. J. Road, Sewree (West), Mumbai,400015 | PAGE ' + currentPage.toString() + ' of ' + pageCount}],


                  }

                ],

                alignment: 'center'

              }]


            };

          },

          content,
          styles: {

            tablfont: {

              fontSize: 9,
              fonts: 'TimesNewRoman'
            }

          }

        }

        var win = window.open('', '_blank');
        pdfMake.createPdf(dd).open({}, win);

      }

    }

  })

}


</script>