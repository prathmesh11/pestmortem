<?php
$base='../';
$js='<script src="'.$base.'js/alart.js"></script>';
$css='<link rel="stylesheet" href="'.$base.'css/alart.css">';
include($base.'_in/header.php');
include($base.'_in/connect.php');
$con=_connect();

?>
<style>
hr.style-hr {
    border: 0;
    height: 1px;
    background-image: linear-gradient(to right, rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.75), rgba(0, 0, 0, 0));
}
.oldnewpass_error{
        color:red;
        margin:0px;
        padding:2px;
        text-align:center;
        display:none;
      }
      .newpass_error{
        color:red;
        margin:0px;
        padding:2px;
        text-align:center;
        display:none;
      }
      .form-group{
          margin-bottom:5px;
      }
      input[type=text],input[type=password]{
    background: transparent;
    border-bottom: 1px solid #3498db;
    border-radius: 0px;
    background: #FFFFFF;
}
</style>
<div class="container-fluid" style="margin-left:5%">
    <div class="row content">
        <h2 align="center">Change My Password</h2>
        <hr class="style-hr">
        <div class="col-sm-3"> </div>
        <div class="col-sm-6">

            <div class="col-sm-4" style="display:none;">Phone</div>
            <div class="col-sm-8" style="display:none;">
                <div class="form-group"><input type="text" class="form-control input-sm"
                        value="<?php echo $_SESSION['phone']; ?>" id="inp-text-1"></div>
            </div>
            <div class="col-sm-4">Old Password</div>
            <div class="col-sm-8">
                <div class="form-group"><input type="password" class="form-control input-sm password" id="inp-text-2"></div>
                <p class="oldnewpass_error">Please Enter Correct Old Password</p>
            </div>
            <div class="col-sm-4">New Password</div>
            <div class="col-sm-8">
                <div class="form-group"><input type="password" class="form-control input-sm password" id="inp-text-3"></div>
                <p class="newpass_error">Please Enter New Password</p>
            </div>
            <div class="col-sm-4"></div>

            <div class="col-sm-8">
                <input type="checkbox" class="custom-control-input" id="customCheck1" onchange="showPass()">
                <label class="custom-control-label switch" for="customCheck1" id="lblauto">Show Password</label>
            </div>

            <div style="clear:both;"></div>
            <hr class="style-hr">
            <div class="col-sm-6"><button class="btn btn-success btn-sm btn-block" id="btn-submit"
                    style="margin:5px;">Submit</button></div>
            <div class="col-sm-6"><button class="btn btn-danger btn-sm btn-block" id="btn-reset"
                    style="margin:5px;">Reset</button></div>
        </div>
        <div class="col-sm-3"> </div>
    </div>
</div>
<?php
include($base.'_in/footer.php');
?>
<script>


function showPass() {


    if ($('.password').attr('type') == "password") {

        $('.password').attr('type', 'text');

         $(".switch").text("Hide Password");

    } else {

        $('.password').attr('type', 'password');

            $(".switch").text("Show Password");

    }

}

$('#btn-reset').on('click',function(){

    window.location.reload();

});

$('#btn-submit').on('click', function () {

    var phone   = $('#inp-text-1').val(),

        oldpass = $('#inp-text-2').val(),

        newpass = $('#inp-text-3').val();

    var valid   = true;

    var regphone = /^[789]\d{9}$/;

    if (oldpass.length == 0) {

        valid = valid * false;

        $(".oldnewpass_error").show();

    } else {

        valid = valid * true;

        $(".oldnewpass_error").hide();

    }

    if (newpass.length == 0) {

        valid = valid * false;

        $(".newpass_error").show();

    } else {

        valid = valid * true;

        $(".newpass_error").hide();

    }

    if (valid) {

        $.ajax({

            type: "POST",

            data: "phone=" + phone + "&oldpass=" + oldpass + "&newpass=" + newpass,

            url: '/_api/login/change-pass.php',

            success: function (res) {

                if (res.status == 'success') {

                    swal({

						type: 'success',
						title: 'your password has been Update',
						showConfirmButton: false,
						timer: 3000

					});

					setTimeout(function () {

						window.location.href='/_api/login/logout.php';

					}, 3000);

                } else {

                    $(".oldnewpass_error").show();

                }

            }

        });

    }
    
});

</script>