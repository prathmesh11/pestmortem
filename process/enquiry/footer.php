<?php
    include($base.'_in/footer.php');
?>
<script>
$(document).ready(function() {
    $('.select-js').select2({width: '100%', tags: true});

    $('.select').attr('style','width:100%!important;');
});


function confirms1(e) {

    if (confirm("Are You Sure TO Confirm Order?")) {

        let id         = $(e).data('id');

        let enquiryid  = $(e).data('enquiryid');

        let customerid = $(e).data('customerid');

        $.ajax({

            type: "POST",

            data: 'enquiryid=' + enquiryid + '&id=' + id + '&customerid=' + customerid,

            url: 'api/confirm.php',

            cache: false,

            success: function (res) {

                if (res.status == 'success') {

                    window.location.reload();

                }

            }

        })

    }

}


function closes(e) {
    
    if (confirm("Are You Sure TO close Order?")) {

        let enquiryid = $(e).data('enquiryid');

        let id        = $(e).data('id');

        $.ajax({

            type: "POST",

            data: 'enquiryid=' + enquiryid + '&id=' + id,

            url: 'api/close.php',

            cache: false,

            success: function (res) {

                if (res.status == 'success') {

                    window.location.reload();

                }

            }

        })

    }
    
}
</script>