<?php 

  $base    = '../../';

  $navenq3 = 'background:#1B1464;';

  include('header.php');

  if (!in_array('Close Enquiry', $arrysession) ) {
   
    echo "<script>window.location.href='/process/dashboard.php';</script>";
   
    exit;

}

?>
<br>

<style>

  .input-container {

    max-width: 300px;
    background-color: #EDEDED;
    border: 1px solid #DFDFDF;
    border-radius: 5px;
  }

  input[type='file'] {
    display: none;
  }

  .file-info {
    font-size: 0.9em;
  }

  .browse-btn {
    background: #03A595;
    color: #fff;
    min-height: 35px;
    padding: 10px;
    border: none;
    border-top-left-radius: 5px;
    border-bottom-left-radius: 5px;
  }

  .browse-btn:hover {
    background: #4ec0b4;
  }

  @media (max-width: 300px) {
    button {
      width: 100%;
      border-top-right-radius: 5px;
      border-bottom-left-radius: 0;
    }

    .file-info {
      display: block;
      margin: 10px 5px;
    }
  }

  .well-link {
    text-decoration: none !important;
    color: #000000;
    font-weight: bold;
  }

  .panel-shadow {
    box-shadow: 0 8px 10px 1px rgba(0, 0, 0, .14), 0 3px 14px 2px rgba(0, 0, 0, .12), 0 5px 5px -3px rgba(0, 0, 0, .2);
  }

</style>

<div class="table-ui container-fluid">

  <div class="tr row">

    <div class="col-sm-1 th">Enquiry No.</div>

    <div class="col-sm-3 th">Customer Info</div>

    <div class="col-sm-3 th">Enquery For</div>

    <div class="col-sm-1 th">Status</div>

    <div class="col-sm-2 th">Close Status</div>

    <div class="col-sm-2 th">Action</div>

  </div>

  <?php

    $result=mysqli_query($con,"SELECT * FROM enquiry WHERE close_enquiry_time<>0 AND close_enquiry_by<>0 ORDER BY created_time DESC");

    while($rows = mysqli_fetch_assoc($result)){

      $enquiryid = $rows['enquiryid'];

      $state     = $rows['state'];
      
      $stateName = mysqli_fetch_assoc(mysqli_query($con,"SELECT statename x FROM statemaster WHERE statecode= '$state' "))['x'];

  ?>

  <div class="row tr">

    <div class="col-sm-1 td" style="word-wrap:break-word;">
      
      <?php echo $rows['enquiryid']; ?>
      
    </div>

    <div class="col-sm-3 td">

      <?php 

        echo 'Company : '.$rows['customer_name'];

        echo '<br>Address : '.$rows['customeraddress']; 

        echo '<br>Contact No  : '.$rows['contactno'];

        echo '<br>Email Id. : '.$rows['emailid'];

        echo '<br>State : '.$stateName;

      ?>

    </div>

    <div class="col-sm-3 td">

      <?php echo $rows['category']; ?>

      <?php 

        echo '<div class="table-ui">';

          echo '<div class="tr row">'; 

            echo '<div class="col-sm-3 th"> Sr No </div>';

            echo '<div class="col-sm-9 th"> Service Name </div>';

          echo '</div>'; 

          $count = 1;

          $service_json = json_decode($rows['service_json']);

          foreach ($service_json as $i) {

            echo '<div class="tr row">'; 

              echo '<div class="col-sm-3 td"> '.$count.' </div>';

              echo '<div class="col-sm-9 td"> '.get_object_vars($i)['servicename'].' </div>';

            echo '</div>'; 
            
            $count++;
              
          }

        echo '</div>'; 

      ?>

    </div>

    <div class="col-sm-1 td" style="padding:0;">

      <button class="btn btn-primary btn-sm " data-toggle="modal" data-target="#myModal"
        data-enquiryid="<?php echo $rows['enquiryid']; ?>" data-branchid="<?php echo $rows['branchid']; ?>"
        onclick="enquirystatus(this)">Enquiry Status</button>

    </div>

    <div class="col-sm-2 td">

      <?php

        echo 'Close Date : '. date("d-m-Y",$rows['close_enquiry_time']/1000);

        $close_enquiry_by = $rows['close_enquiry_by'];

        $username         = mysqli_fetch_assoc(mysqli_query($con,"SELECT username x FROM employeemaster WHERE employeeid='$close_enquiry_by'"))['x'];

        echo '<br>Close By : '.$username;

        echo '<br>Reason : '.$rows['close_enquiry_reason'];


      ?>

    </div>

    <div class="col-sm-2 td">

      <button class="btn btn-sm btn-block btn-success" data-enquiryid="<?php echo $rows['enquiryid']; ?>"
        onclick="recovers(this)">Recover Order</button>

    </div>


  </div>

  <?php } ?>

</div>


<div id="myModal" class="modal fade" role="dialog">

  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">

      <div class="modal-header">

        <button type="button" class="close" data-dismiss="modal">&times;</button>

        <h4 class="modal-title" align="center">Enquiry Status</h4>

      </div>

      <div class="modal-body" id="enquiryStatus">

      </div>

    </div>

  </div>

</div>

<?php 
include('footer.php');
?>
<script>

function recovers(e) {

  if (confirm("Are You Sure TO recover Order?")) {

    var enquiryid = $(e).data('enquiryid');

    $.ajax({

      type: "POST",

      data: 'enquiryid=' + enquiryid,

      url: 'api/recover.php',

      cache: false,

      success: function (res) {

        if (res.status == 'success') {

          window.location.reload();

        }

      }

    })

  }
  
}


function enquirystatus(e) {

  let enquiryid = $(e).attr('data-enquiryid');

  let branchid = $(e).attr('data-branchid');


  if (enquiryid != '') {

    $.ajax({

      type: "POST",

      data: "enquiryid=" + enquiryid + "&branchid=" + branchid,

      url: 'api/enquirydetails.php',

      cache: false,

      success: function (res) {

        if (res.status == 'success') {

          let json = res.json;

          let str = '<a href="javascript:void(0)" class="well-link"><div class="panel panel-default panel-shadow"><div class="panel-heading" style="background-color:' + res.backgroundcolor + ';color:#fff; text-align: center;">' + res.followuptime + '</div><div class="panel-body">' + res.descenquiry + '</div></div></a>';

          for (let i in json) {

            str += '<a href="javascript:void(0)" class="well-link"><div class="panel panel-default panel-shadow"><div class="panel-heading" style="background-color:' + json[i].background + ';color:#fff;text-align: center;">' + json[i].nextfollowuptime + '</div><div class="panel-body">' + json[i].followupnote + '</div></div></a>';

          }

          $('#enquiryStatus').html(str);

        }

      }

    })

  } else {

    alert('Somthing Went Wrong');

  }

}

</script>
