<?php 

  $base    = '../../';
  $navenq1 = 'background:#1B1464;';

  include('header.php');

  if (!in_array("Enquiry & FollowUp", $arrysession)) {
   
    echo "<script>window.location.href='/process/dashboard.php';</script>";
   
    exit;

}

  if (session_status() == PHP_SESSION_NONE) { session_start(); }
  $sessionby = $_SESSION['employeeid'];
  $branchid  = $_SESSION['branchid'];



?>
<br>
<style>

.input-container {

  max-width: 300px;
  background-color: #EDEDED;
  border: 1px solid #DFDFDF;
  border-radius: 5px;
}

input[type='file'] {
  display: none;
}

.file-info {
  font-size: 0.9em;
}

.browse-btn {
  background: #03A595;
  color: #fff;
  min-height: 35px;
  padding: 10px;
  border: none;
  border-top-left-radius: 5px;
  border-bottom-left-radius: 5px;
}

.browse-btn:hover {
  background: #4ec0b4;
}

@media (max-width: 300px) {
  button {
    width: 100%;
    border-top-right-radius: 5px;
    border-bottom-left-radius: 0;
  }

  .file-info {
    display: block;
    margin: 10px 5px;
  }
}

.well-link {
  text-decoration: none !important;
  color: #000000;
  font-weight: bold;
}
.panel-shadow {
    box-shadow: 0 8px 10px 1px rgba(0, 0, 0, .14), 0 3px 14px 2px rgba(0, 0, 0, .12), 0 5px 5px -3px rgba(0, 0, 0, .2);
}



</style>

<img class="hidden" id=imageid src="../../../../img/PestMortemLogo.jpg">

<div class="table-ui container-fluid">

  <div class="tr row">

    <div class="col-sm-2 th">Enquiry No.</div>

    <div class="col-sm-3 th">Customer Info</div>

    <div class="col-sm-3 th">Enquiry For</div>

    <!-- <div class="col-sm-2 th">Offer</div> -->

    <div class="col-sm-2 th">Enquiry Status</div>

    <!-- <div class="col-sm-1 th">Follow Up</div> -->

    <div class="col-sm-2 th">Action</div>

  </div>

  <?php
  
    $result=mysqli_query($con,"SELECT * FROM enquiry WHERE branchid='$branchid' AND created_time<>0 AND created_by<>0 AND close_enquiry_time=0 AND close_enquiry_by=0 AND order_confirm_by=0 AND order_confirm_time=0 ORDER BY created_time DESC");
    
    while($rows = mysqli_fetch_assoc($result)){

      $enquiryid    = $rows['enquiryid'];

      $state        = $rows['state'];
      
      $stateName    = mysqli_fetch_assoc(mysqli_query($con,"SELECT statename x FROM statemaster WHERE statecode= '$state' "))['x'];

      $customername = $rows['customer_name'];

      $customerid   = $rows['customerid'];

      $quatation_id = $rows['quatation_id'];

  ?>

  <div class="row tr">

    <div class="col-sm-2 td" style="word-wrap:break-word;">

      <?php echo $rows['enquiryid']; ?>

      <?php echo 'Enquiry Reference : '. $rows['enqReference']; ?>

      <br>

      <?php if ($rows['personName'] != 0) {

        $sales_person_id = $rows['personName'];
                        
        $salesPerson     = mysqli_fetch_assoc(mysqli_query($con,"SELECT sales_person_name x FROM salesperson WHERE id='$sales_person_id'"))['x'];

        echo '<p>Sales Person Name : ' . $salesPerson.'</p>'; 

      }?>


    </div>

    <div class="col-sm-3 td">

      <?php 


        echo 'Company : '.$customername; 
        echo '<br>Address : '.$rows['customeraddress']; 
        echo '<br>Contact No  : '.$rows['contactno'];
        echo '<br>Email Id. : '.$rows['emailid'];
        echo '<br>State : '.$stateName;
       
      ?>

    </div>

    <div class="col-sm-3 td">
    
       <?php 

        echo '<div class="table-ui">';

          echo '<div class="tr row">'; 

            echo '<div class="col-sm-3 th"> Sr No </div>';

            echo '<div class="col-sm-9 th"> Service Name </div>';

          echo '</div>'; 

          $count = 1;

          $service_json = json_decode($rows['service_json']);

          foreach ($service_json as $i) {

            echo '<div class="tr row">'; 

              echo '<div class="col-sm-3 td"> '.$count.' </div>';

              echo '<div class="col-sm-9 td"> '.get_object_vars($i)['servicename'].' </div>';

            echo '</div>'; 
            
            $count++;
              
          }

        echo '</div>'; 


       
      ?>

    </div>

    <!-- <div class="col-sm-2 td"> -->
      <!-- offer  -->

      <!-- <div class="input-container">

        <input type="file" class="real-input" name="quatation" accept="application/pdf">

        <button class="browse-btn">
          Browse Files
        </button>

        <span class="file-info">Upload a file</span>

      </div>

      <button class="btn btn-primary btn-sm btn-block" data-enquiryid="<?php echo $rows['enquiryid']; ?>"
        onclick="upload(this)">Upload </button>

      <?php 

        // $enquir = str_replace('/', '-',$rows['enquiryid']);
        // $path   = "api/offer/".$enquir.'.pdf';

        // if(file_exists($path)){

        //   echo '<br><center><a href="'.$path.'" target="_blank">View PDF</a></center>';

        // }else{

        //   echo '<br>Please Upload .pdf File';

        // }

      ?> -->



      <!-- offer  -->
    <!-- </div> -->

    <div class="col-sm-2 td">
      
      <!-- <?php echo $rows['descenquiry']; ?> -->

      <button class="btn btn-primary btn-sm btn-block" data-toggle="modal" data-target="#myModal"
        data-enquiryid="<?php echo $rows['enquiryid']; ?>"   data-branchid="<?php echo $rows['branchid']; ?>" onclick="enquirystatus(this)">Enquiry Status</button>

      <br>

      <?php

        $followuptime = mysqli_fetch_assoc(mysqli_query($con,"SELECT followuptime x FROM enquiryfollowup WHERE enquiryid='$enquiryid' ORDER BY id DESC LIMIT 1 "))['x'];

        $nextfollowuptime = $followuptime;

        $followUpStatusFlags = '';

        if($nextfollowuptime!=0){

          if($START_OF_THE_DAY<$nextfollowuptime && $END_OF_THE_DAY<=$nextfollowuptime){

            $followUpStatusFlags = '#3F51B5';

          }

          if($START_OF_THE_DAY>$nextfollowuptime && $END_OF_THE_DAY>$nextfollowuptime){

            $followUpStatusFlags = '#F44336';

          }
          
          if($START_OF_THE_DAY<=$nextfollowuptime && $END_OF_THE_DAY>$nextfollowuptime ){

            $followUpStatusFlags = '#4CAF50';

          }

        } else {

          $followUpStatusFlags = '#ff9800';

        }

        if($nextfollowuptime==0){ $nextfollowuptime='NO Followup'; }else{ $nextfollowuptime=date("d-m-Y",$nextfollowuptime/1000); }

        echo '<p style="background:'.$followUpStatusFlags.';color:#fff;">'.$nextfollowuptime.'</p>'; 
        
        if ($quatation_id) {
      ?>

        <button class="btn btn-sm btn-block btn-success" data-quatationid="<?php echo $quatation_id; ?>" onclick="pdf(this)">Quotation pdf</button>
     
      <?php } ?>

    </div>

    <div class="col-sm-2 td">

      <button class="btn btn-primary btn-sm btn-block" data-toggle="modal" data-target="#myModal2"
        data-enquiryid="<?php echo $rows['enquiryid']; ?>" data-quatation_id="<?php echo $quatation_id;?>" data-customerid="<?php echo $customerid;?>"  onclick="setidtomidal(this)">Followup</button>

        <button class="btn btn-success btn-sm btn-block" data-enquiryid="<?php echo $rows['enquiryid']; ?>" data-id="<?php echo $rows['id']; ?>" data-customerid="<?php echo $rows['customerid']; ?>" onclick="confirms1(this)">Enquiry Successful</button>

  
        <!-- <button class="btn btn-success btn-sm btn-block" data-enquiryid="<?php echo $rows['enquiryid']; ?>" data-customername="<?php echo $rows['customername']; ?>" data-customeraddress="<?php echo $rows['customeraddress']; ?>" data-emailid="<?php echo $rows['emailid']; ?>" data-state="<?php echo $rows['state']; ?>" data-category="<?php echo $rows['category']; ?>" data-contactno="<?php echo $rows['contactno']; ?>" onclick="confirms(this)">Order Confirm</button> -->        

      <button class="btn btn-sm btn-block btn-danger" data-enquiryid="<?php echo $rows['enquiryid']; ?>" data-id="<?php echo $rows['id']; ?>"
        
        onclick="closeEnquiry(this)" data-toggle="modal" data-target="#myModal3">Enquiry Unsuccessful</button>

        <a href="enquiry.php?enquiryid=<?php echo $rows['enquiryid'];?>&id=<?php echo $rows['id'];?>&edit=true" class="btn btn-info btn-sm btn-block">Edit</a>

    </div>

  </div>

  <?php

   }

  ?>
  
</div>



<a class="btn" style="font-size:25px;border-radius:50px;width:60px;height:60px;position: fixed;
    bottom: 40px;
    right: 40px;background:#eb2f06;color:#fff;padding-top: 10px;" href="/process/enquiry/enquiry.php">+</a>


<!---------------------- Set New FollowUp Model Start----------------- -->

<div id="myModal2" class="modal fade" role="dialog">

  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">

      <div class="modal-header">

        <h4 class="modal-title" align="center">New FollowUp</h4>

      </div>

      <div class="modal-body">

        <div class="form-group row">

          <div class="col-sm-4">Set New Followup</div>

          <input type="hidden" data-name="enquiryid" id="inp-enquiryidset">

          <div class="col-sm-8">

            <input type="text" id="inp-enq-olddate" data-role="text" class="form-control input-sm">
            <input type="hidden" id="inp-enq-olddate-stamp" data-name="followuptime">

          </div>
          <div class="col-sm-4">Note</div>

          <div class="col-sm-8">

            <input type="text" data-name="followupnote" data-role="text" class="form-control input-sm">

          </div>

          <div class="col-sm-4"></div>

          <div class="col-sm-8">

            <button class="btn btn-success btn-block btn-sm" onclick="setqneuiry()">Add New FollowUp</button>

          </div>

          <div class="col-sm-4"></div>

          <div class="col-sm-8">

            <a href="" class="btn btn-success btn-block btn-sm" id="createQuotation">Create Quotation</button>

            <a href='' class="btn btn-success btn-block btn-sm" id="reviseQuotation" style="display:none;">Revise Quotation</a>

          </div>

        </div>

      </div>

    </div>

  </div>

</div>

<!---------------------- Set New FollowUp Model End----------------- -->

<!---------------------- Enquiry Status Model Start----------------- -->

<div id="myModal" class="modal fade" role="dialog">

  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">

      <div class="modal-header">

        <button type="button" class="close" data-dismiss="modal">&times;</button>

        <h4 class="modal-title" align="center">Enquiry Status</h4>

      </div>

      <div class="modal-body" id="enquiryStatus">

      </div>

    </div>

  </div>

</div>

<!---------------------- Enquiry Status Model End----------------- -->

<!---------------------- Enquiry Unsuccessful Model Start-------------------->


<div id="myModal3" class="modal fade" role="dialog">

  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">

      <div class="modal-header">

        <button type="button" class="close" data-dismiss="modal">&times;</button>

        <h4 class="modal-title" align="center">Enquiry Close</h4>

      </div>

      <div class="modal-body">

        <div class="form-group row">

          <input type="hidden" data-name="enquiryid" id="inp-enquiryId">

          <input type="hidden" data-name="tableId" id="inp-tableId">

          <div class="col-sm-1">Reason</div>

          <div class="col-sm-11">

            <textarea type="text" data-name="reasonToClose" data-role="text" class="form-control input-sm"></textarea>

          </div>
        
        </div>

      </div>

      <div class="modal-footer">

        <button class="btn btn-success btn-block btn-sm" onclick="closeEnqiury()">Close Enquiry</button>

      </div>

    </div>

  </div>

</div>

<!---------------------- Enquiry Unsuccessful Model End-------------------->

<?php 
include('footer.php');
?>
<script>



$('.browse-btn').on('click', function () {

  $(this).parent().find('input').click();

});

$('.real-input').on('change', function () {

  $(this).parent().find('span').text($(this).val().replace(/C:\\fakepath\\/i, ''));

});



$("#inp-enq-date").pickadate({

  selectYears: true,

  selectMonths: true,

  min: true,

  onClose: function () {

    try {

      var dateStamp = this.get('select')['pick'];

      $("#inp-enq-date-stamp").val(dateStamp);

    } catch (err) {


    }

  }

});

$("#inp-enq-olddate").pickadate({

  selectYears: true,

  selectMonths: true,

  min: true,

  onClose: function () {

    try {

      var dateStamp = this.get('select')['pick'];

      $("#inp-enq-olddate-stamp").val(dateStamp);

    } catch (err) {


    }

  }

});

function setidtomidal(e) {

  var enquiryid = $(e).data('enquiryid');

  $('#inp-enquiryidset').val(enquiryid);

  let quatationId = $(e).attr('data-quatation_id');

  let customerid  = $(e).attr('data-customerid');

  console.log(quatationId);

  if (quatationId == '') {

    $('#reviseQuotation').hide();

    $('#createQuotation').attr('href',"/process/quatation/quatation/index.php?enquiryid="+enquiryid+"&customerid="+customerid+"&enquiry=create");
    
  } else {

    $('#reviseQuotation').show();

    $('#createQuotation').hide();

    $('#reviseQuotation').attr('href',"/process/quatation/quatation/index.php?enquiryid="+enquiryid+'&quatation_id='+quatationId+"&customerid="+customerid+"&enquiry=edit");

  }
  
}

function setqneuiry() {
  
  var valid = true;

  if (checker('myModal2') != false) {

    valid = valid * true;

  } else {

    valid = valid * false;

  }

  if (valid) {

    var data = checker('myModal2');

    var datastr = JSON.stringify(data);

    $.ajax({

      type: "POST",

      data: {

        data: datastr

      },

      url: 'api/setqneuiry.php',

      cache: false,

      success: function (res) {

        if (res.status == 'success') {

          window.location.reload();

        }

      }

    })

  }

}


function enquirystatus(e) {

  let enquiryid = $(e).attr('data-enquiryid');

  let branchid  = $(e).attr('data-branchid');


  if (enquiryid != '') {

    $.ajax({

      type: "POST",

      data: "enquiryid=" + enquiryid + "&branchid=" + branchid ,

      url: 'api/enquirydetails.php',

      cache: false,

      success: function (res) {

        if (res.status == 'success') {

          let json = res.json;
          
          let str  = '';

          for (let i in json) {

            str  += '<a href="javascript:void(0)" class="well-link"><div class="panel panel-default panel-shadow"><div class="panel-heading" style="background-color:'+json[i].background+';color:#fff;text-align: center;">'+json[i].nextfollowuptime+'</div><div class="panel-body">'+json[i].followupnote+'</div></div></a>';
            
          }

          $('#enquiryStatus').html(str);

        }

      }

    })

  } else {

    alert('Somthing Went Wrong');

  }

}




function confirms(e) {

  let enquiryid       = $(e).attr('data-enquiryid');

  let customername    = $(e).attr('data-customername');

  let customeraddress = $(e).attr('data-customeraddress');

  let emailid         = $(e).attr('data-emailid');

  let state           = $(e).attr('data-state');

  let category        = $(e).attr('data-category');

  let contactno       = $(e).attr('data-contactno');

  window.location     = "/process/create-master/customer-master/index.php?enquiryid=" + enquiryid + "&customername=" + customername + "&customeraddress=" + customeraddress + "&emailid=" + emailid + "&state=" + state + "&category=" + category + "&contactno=" + contactno;


}

function closeEnquiry(e) {

  $('#inp-enquiryId').val($(e).attr('data-enquiryid'));

  $('#inp-tableId').val($(e).attr('data-id'));
  
}

function closeEnqiury() {
  
  var valid = true;

  if (checker('myModal3') != false) {

    valid = valid * true;

  } else {

    valid = valid * false;

  }

  if (valid) {

    var data = checker('myModal3');

    var datastr = JSON.stringify(data);

    $.ajax({

      type: "POST",

      data: {

        data: datastr

      },

      url: 'api/close.php',

      cache: false,

      success: function (res) {

        if (res.status == 'success') {

          window.location.reload();

        }

      }

    })

  }

}

$('#myModal2,#myModal3,#myModal').on('hidden.bs.modal', function (e) {

  $(this).find("input,textarea,select").val('').end();

  $(this).find("input,textarea,select").css("border-bottom", "");

})

function upload(e) {
  var file = $(e).parent().parent().find('input')[0].files[0];
  var enquiryid = $(e).data('enquiryid');
  var formData = new FormData();

  formData.append('offer', file);
  formData.append('enquiryid', enquiryid);
  $.ajax({
    type: "POST",
    data: formData,
    url: 'api/offer.php',
    cache: false,
    processData: false,
    contentType: false,
    success: function (res) {
      window.location.reload();
    }
  })
}



function getBase64FromImageUrl(url) {

  var img = new Image();

  img.setAttribute('crossOrigin', 'anonymous');

  img.onload = function () {

    var canvas    = document.createElement("canvas");
    canvas.width  = this.width;
    canvas.height = this.height;
    var ctx = canvas.getContext("2d");
    ctx.drawImage(this, 0, 0);
    var dataURL = canvas.toDataURL("image/png");
    var base64 = dataURL.replace(/^data:image\/(png|jpg);base64,/, "");
    $('#imageid').val(base64);

  };

  img.src = url;

}

getBase64FromImageUrl('/img/PestMortemLogo.jpg');


function pdf(e) {

  var quatationid = $(e).attr('data-quatationid');

  $.ajax({

    type: "POST",

    data: 'quatationid=' + quatationid,

    url: '/process/quatation/quatation/quatationpdf.php',

    cache: false,

    success: function (res) {

      if (res.status == 'success') {

        var content = [];

        var base64  = 'data:image/jpg;base64,' + $('#imageid').val();

        var regex   = /<br\s*[\/]?>/gi;

        var conditionandscope = res.conditionandscope.replace(regex, "\n");

        var payment_terms     = res.payment_terms.replace(regex, "\n");

        var branchjson        = res.branchjson;
         
        var table1 = {

          widths: ['auto', '*', '*', '*'],
          dontBreakRows: true,
          body: []

        };


        content.push({

          layout: 'noBorders',

          table: {
            widths: ['*', '*'],
            body: [
              ['', {
                text: "PEST MORTEM (INDIA) PRIVATE LIMITED\n",
                color: '#FF0000',
                fontSize: 13,
                bold: true
              }],
              [{
                image: base64,
                width: 100,
                height: 60
                
              }, {
                text: 'Address : '+branchjson.address+ '\nTel : '+branchjson.phone+' \nEmail : ' +branchjson.email+', \nWebsite : www.pestmortem.com, \nPAN NO :'+res.pan+'  \nGST NO :'+branchjson.gstnumber,
                alignment: 'justify',
                fontSize: 9
              }],
            ]
          }

        });

        content.push({

          layout: 'noBorders',
          table: {
            widths: ['*', '*'],
            body: [
              [{
                text: "REF : " + (res.quatation_id),  
                fontSize: 12
              }, {
                text: "Date : " + res.quatation_date,
                fontSize: 11,
                alignment: 'right',
                background: '#ffff00'
              }]
            ]
          }

        });


        content.push({

          layout: 'noBorders',
          table: {
            widths: ['*'],
            body: [
              [{
                text: "TO,",
                fontSize: 11
              }],
              [{
                
                text: res.customername,
                fontSize: 11,
                background: '#ffff00'
              }],
              [{
                
                text: res.customeraddress,
                fontSize: 11
              }],
              [{
                text: "Kind Attn : " + res.kind_attend,
                fontSize: 11
              }],
              [{
                text: "Sub           : " + res.subject,
                fontSize: 11
              }]
        
            ]

          }

        });

        content.push({
          text: 'Ref              : As per enquiry\n\n',
          fontSize: 10,
          alignment: 'justify'
        });

        content.push({
          text: '\n'
        });

        content.push({
          text: 'Conditions & Scope:',
          bold: true,
          fontSize: 11,
          background: '#ffff00'
        });

        content.push({
          text: conditionandscope,
          bold: true,
          fontSize: 11,
        });

        content.push({
          text: '\n'
        });

        content.push({
          text: 'Commercial:',
          bold: true,
          fontSize: 11,
          background: '#ffff00'
        });
  

        var table1 = {

          widths: [15,'*',80,80],
          dontBreakRows: true,
          fontSize: 20,
          body: []

        };

        var particular = [{text: 'SR NO',},{text: 'SERVICES NAME',alignment: 'center'},{text: 'RATE',alignment: 'center',},{text: 'UNIT',alignment: 'center'}];

        table1['body'].push(particular);     

        content.push({

          table: {
              headerRows: 1,
              // widths: ['*'],
              widths: [15,'*',80,80],
              alignment: 'center',

              body: [[],]
          },

        });

        var cr = 1;

        var commercial = res.json;

        for(var i in commercial){
          var tb = [];
          tb.push(cr,{text:commercial[i].servicename,alignment: 'left'},{text:commercial[i].rate,alignment: 'center'},{text:commercial[i].unit,alignment: 'center'});
          table1['body'].push(tb);
          cr++;
        }


        content.push({
          text: "",
          fontSize: 20,
          alignment: 'center'
        }, {
          style: 'tablfont1',
          table: table1
        });

        content.push({
          text: "\n",
        });

        content.push({
          text: 'Payment Terms & Condition:-:-',
          bold: true,
          fontSize: 11,
          background: '#ff0000'
        });

        content.push({
          text: payment_terms,
          bold: true,
          fontSize: 11,
        });


        content.push({
          text: '\nAuthorized By  \n\n'+res.autorised_by,
          bold: true,
          fontSize: 11,
        });

        dd = {
          pageSize: 'A4',
          pageOrientation: 'portrait',
          pageMargins: [40, 30, 30, 35],
          footer: function (currentPage, pageCount) {
            return {
              margin: 10,
              columns: [{
                fontSize: 8,
                text: [{
                    text: '--------------------------------------------------------------------------' +
                      '\n',
                    margin: [0, 20]
                  },
                  {
                    text: '© Pest Mortem (India) PVT. LTD | PAGE ' + currentPage.toString() + ' of ' + pageCount,
                  }
                ],
                alignment: 'center'
              }]
            };

          },
          content,
          styles: {
            tablfont: {
              fontSize: 7
            },
            tablfont1: {
              fontSize: 9
            }
          }
        }

        var win = window.open('', '_blank');

        pdfMake.createPdf(dd).open({}, win);

      }

    }

  })

}

</script>