<?php

    $base = '../../../';

    include($base.'_in/connect.php');

    header('content-type: application/json; charset=utf-8');

    header("access-control-allow-origin: *");

    if(isset($_POST['data'])){

        $con = _connect();

        if (session_status()==PHP_SESSION_NONE) { session_start(); }

        $created_by    = $_SESSION['employeeid'];

        $data          = get_object_vars(json_decode($_POST['data']));

        $enquiryid     = $data['enquiryid'];

        $id            = $data['tableId'];

        $reasonToClose = $data['reasonToClose'];

        $update        = mysqli_query($con,"UPDATE enquiry SET close_enquiry_time='$CURRENT_MILLIS', close_enquiry_by='$created_by', close_enquiry_reason='$reasonToClose' WHERE enquiryid='$enquiryid' AND id = '$id' ");

        if($update){

            echo '{"status":"success"}';

        } else {

            echo '{"status":"falid"}';

        }

        _close($con);

    } else {

        echo '{"status":"falid"}';

    }

?>