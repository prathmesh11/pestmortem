<?php

    $base = '../../../';

    include($base.'_in/connect.php');

    header('content-type: application/json; charset=utf-8');

    header("access-control-allow-origin: *");

    if(isset($_POST['data1']) && isset($_POST['data2'])){

        $con             = _connect();

        if (session_status() == PHP_SESSION_NONE) { session_start(); }

        $created_by      = $_SESSION['employeeid'];

        $data            = get_object_vars(json_decode($_POST["data1"]));

        $branchid        = $data['branchid'];

        $enquiryid       = $data['enquiryid'];

        $followuptime    = $data['followuptime'];

        $enqreference    = $data['enqreference'];

        $personName      = $data['personName'];

        $category        = $data['category'];

        $descenquiry     = $data['descenquiry'];
  
        $customerid      = $data['customerid'];

        $customeraddress = $data['customeraddress'];

        $contactno       = $data['contactno'];

        $emailid         = $data['emailid'];

        $state           = $data['state'];

        $data2           = $_POST["data2"];

        $customerNameInMaster = mysqli_fetch_assoc(mysqli_query($con,"SELECT customername x FROM customermaster WHERE customerid='$customerid' "))['x'];

        if ($customerNameInMaster) {
            
            $customername = $customerNameInMaster;

            $custid       = $customerid;

        } else {
           
            $customername = $customerid;

            $custid        = '';

        }

        $enquiryids = mysqli_fetch_assoc(mysqli_query($con,"SELECT enquiryid x FROM enquiry WHERE enquiryid='$enquiryid' AND branchid= '$branchid'"))['x'];

        if ($enquiryids) {

            $new_enquiryid = GetEnquiryNo($con,$branchid);

            $update = mysqli_query($con,"INSERT INTO enquiry (branchid,enquiryid,followuptime,enqReference,personName,category,descenquiry,customerid,customer_name,customeraddress,contactno,emailid,state,service_json,created_by,created_time) VALUES ('$branchid','$new_enquiryid','$followuptime','$enqreference','$personName','$category','$descenquiry','$custid','$customername','$customeraddress','$contactno','$emailid','$state','$data2','$created_by','$CURRENT_MILLIS')");
            
        } else {

            $update = mysqli_query($con,"INSERT INTO enquiry (branchid,enquiryid,followuptime,enqReference,personName,category,descenquiry,customerid,customer_name,customeraddress,contactno,emailid,state,service_json,created_by,created_time) VALUES ('$branchid','$enquiryid','$followuptime','$enqreference','$personName','$category','$descenquiry','$custid','$customername','$customeraddress','$contactno','$emailid','$state','$data2','$created_by','$CURRENT_MILLIS')");

        }

        if($update){

            mysqli_query($con,"INSERT INTO enquiryfollowup (branchid,enquiryid,followuptime,followupnote,created_by,created_time) VALUES ('$branchid','$enquiryid','$followuptime','$descenquiry','$created_by','$CURRENT_MILLIS')");

            echo '{"status":"success"}';

        } else {

            echo '{"status":"falid1"}';

        }

        _close($con);

    } else {

        echo '{"status":"falid"}';

    }

?>