<?php

    $base = '../../../';

    include($base.'_in/connect.php');

    header('content-type: application/json; charset=utf-8');

    header("access-control-allow-origin: *");

    if (isset($_POST['customerName'])) {

        $con          = _connect();
        
        $customerid   = $_POST["customerid"];

        $customerName = $_POST["customerName"];

        $edit         = $_POST["edit"];

        if ($customerid == '') {

            $select     = mysqli_fetch_assoc(mysqli_query($con,"SELECT customeraddress,contactno,emailid,state FROM enquiry WHERE customer_name='$customerName'"));
            
            $address     = $select['customeraddress'];

            $state       = $select['state'];
  
            $mobile      = $select['contactno'];
 
            $email       = $select['emailid'];
            
        } else if ($customerid != '' && $edit == 'true') {

            $select       = mysqli_fetch_assoc(mysqli_query($con,"SELECT customeraddress,contactno,emailid,state FROM enquiry WHERE customerid='$customerid'"));

            if ($select) {

                $address     = $select['customeraddress'];

                $state       = $select['state'];
  
                $mobile      = $select['contactno'];
 
                $email       = $select['emailid'];
                
                
            } else {
                
                $select     = mysqli_fetch_assoc(mysqli_query($con,"SELECT customerjson,contactjson FROM customermaster WHERE customerid='$customerid'"));

                $address     = get_object_vars(json_decode($select['customerjson']))['address'];

                $state       = get_object_vars(json_decode($select['customerjson']))['state'];
     
                $contactjson = json_decode($select['contactjson']);
     
                $mobile      = get_object_vars($contactjson[0])['mobile'];
     
                $email       = get_object_vars($contactjson[0])['email'];

            }
            
            

        } else if ($customerid != '' && $edit == '') {

               $select       = mysqli_fetch_assoc(mysqli_query($con,"SELECT customeraddress,contactno,emailid,state FROM enquiry WHERE customerid='$customerid'"));

                if ($select) {

                    $address     = $select['customeraddress'];

                    $state       = $select['state'];
    
                    $mobile      = $select['contactno'];
    
                    $email       = $select['emailid'];
                    
                    
                } else {
                    
                    $select     = mysqli_fetch_assoc(mysqli_query($con,"SELECT customerjson,contactjson FROM customermaster WHERE customerid='$customerid'"));

                    $address     = get_object_vars(json_decode($select['customerjson']))['address'];

                    $state       = get_object_vars(json_decode($select['customerjson']))['state'];
        
                    $contactjson = json_decode($select['contactjson']);
        
                    $mobile      = get_object_vars($contactjson[0])['mobile'];
        
                    $email       = get_object_vars($contactjson[0])['email'];
                    
                }
            
        
        } else {

            $address     = '';

            $state       = 'Select';
  
            $mobile      = '';
 
            $email       = '';

        }
        

        if($select){

           $json = '{"customeraddress":"'.$address.'","contactno":"'.$mobile.'","emailid":"'.$email.'","state":"'.$state.'"}'; 

            echo '{"status":"success","json":['.$json.']}';   

        } else {

            $json = '{"customeraddress":"'.$address.'","contactno":"'.$mobile.'","emailid":"'.$email.'","state":"Select"}'; 

            echo '{"status":"falid1","json":['.$json.']}';

        }


        _close($con);

    } else {

        echo '{"status":"falid"}';
        
    }
?>