<?php

    $base = '../../../';

    include($base.'_in/connect.php');

    header('content-type: application/json; charset=utf-8');

    header("access-control-allow-origin: *");

    if(isset($_POST['enquiryid']) && isset($_POST['branchid'])){

        $con = _connect();

        $enquiryid = $_POST['enquiryid'];

        $branchid  = $_POST['branchid'];

        $setNextFollowUp = mysqli_query($con,"SELECT followuptime,followupnote FROM enquiryfollowup WHERE branchid='$branchid' AND enquiryid='$enquiryid' ORDER BY followuptime ASC");

            while ($row = mysqli_fetch_assoc($setNextFollowUp)) {

                $nextfollowuptime = $row['followuptime'];

                $followUpStatusFlags = '';
    
                if($nextfollowuptime!=0){
        
                  if($START_OF_THE_DAY<$nextfollowuptime && $END_OF_THE_DAY<=$nextfollowuptime){
        
                    $followUpStatusFlags = '#3F51B5';
        
                  }
        
                  if($START_OF_THE_DAY>$nextfollowuptime && $END_OF_THE_DAY>$nextfollowuptime){
        
                    $followUpStatusFlags = '#F44336';
        
                  }
                  
                  if($START_OF_THE_DAY<=$nextfollowuptime && $END_OF_THE_DAY>$nextfollowuptime ){
        
                    $followUpStatusFlags = '#4CAF50';
        
                  }
        
                } else {
        
                  $followUpStatusFlags = '#ff9800';
        
                }
        
                if($nextfollowuptime==0){ $nextfollowuptime='NO Followup'; }else{ $nextfollowuptime=date("d-m-Y",$nextfollowuptime/1000); }

                $json.= ',{"nextfollowuptime":"'.$nextfollowuptime.'","background":"'.$followUpStatusFlags.'","followupnote":"'.$row['followupnote'].'"}';
                
            }

            $json = substr($json,1);

            $json  = '['.$json.']';

            echo '{"status":"success" ,"json":'.$json.'}';

    

        _close($con);

    } else {

        echo '{"status":"falid"}';

    }

?>