<?php

    $base = '../../../';

    include($base.'_in/connect.php');

    header('content-type: application/json; charset=utf-8');

    header("access-control-allow-origin: *");

    if(isset($_POST['data1']) && isset($_POST['data2'])){

        $con             = _connect();

        if (session_status() == PHP_SESSION_NONE) { session_start(); }

        $created_by      = $_SESSION['employeeid'];

        $data            = get_object_vars(json_decode($_POST["data1"]));

        $branchid        = $data['branchid'];

        $id              = $data['id'];

        $enquiryid       = $data['enquiryid'];

        $followuptime    = $data['followuptime'];

        $enqreference    = $data['enqreference'];

        $personName      = $data['personName'];

        $category        = $data['category'];

        $descenquiry     = $data['descenquiry'];
  
        $customerid      = $data['customerid'];

        $customeraddress = $data['customeraddress'];

        $contactno       = $data['contactno'];

        $emailid         = $data['emailid'];

        $state           = $data['state'];

        $data2           = $_POST["data2"];

        $customerNameInMaster = mysqli_fetch_assoc(mysqli_query($con,"SELECT customername x FROM customermaster WHERE customerid='$customerid' "))['x'];

        if ($customerNameInMaster) {
            
            $customername = $customerNameInMaster;

            $custid       = $customerid;

        } else {
           
            $customername = $customerid;

            $custid        = '';

        }

        // $customerid      = mysqli_fetch_assoc(mysqli_query($con,"SELECT customerid x FROM customermaster WHERE customerid='$customername' "))['x'];

        // if ($customername == $customerid) {

        //     $custid = $customername;

        // } else {

        //     $json  = '{"created_by":"'.$created_by.'","branchid":"'.$branchid.'","ccode":"","customername":"'.$customername.'","address":"'.$customeraddress.'","emailId":"'.$emailid.'","custCategory":"'.$category.'","customergroupcode":"","creditdays":"30","country":"IN","state":"'.$state.'","gstnumber":"","gstOnBill":"","vendorcode":"","westageallowed":"","paymentTerms":"","opening_balance":"","opening_date":""}';
            
        //     $json1 = '[{"srNo":"1","name":"","department":"","category":"","mobile":"'.$contactno.'","landline":"","email":"'.$emailid.'"}]';
            
        //     $custid = mysqli_fetch_assoc(mysqli_query($con, "SELECT max(id) x FROM customermaster"))['x'];

        //     if($custid) { $custid++; } else { $custid=1; }

        //     mysqli_query($con,"INSERT INTO customermaster (`branchid`, `customerid`, `customername`, `city`, `gstnumber`, `customergroupcode`, `customerjson`, `contactjson`, `chajson`, `opening_balance`, `opening_date`, `created_by`, `created_time`) VALUES ('$branchid','$custid','$customername','','','','$json','$json1','[]','','','$created_by','$CURRENT_MILLIS')");

        // }

        $update            = mysqli_query($con,"UPDATE enquiry SET branchid = '$branchid',followuptime = '$followuptime',enqReference ='$enqreference',personName = '$personName',category = '$category',descenquiry = '$descenquiry',customerid = '$custid',customer_name = '$customername',customeraddress = '$customeraddress',contactno = '$contactno',emailid = '$emailid',state = '$state',service_json = '$data2',updated_by = '$created_by',updated_time = '$CURRENT_MILLIS' WHERE enquiryid = '$enquiryid' AND id = '$id' ");
        
        $enquiryfollowupId = mysqli_fetch_assoc(mysqli_query($con,"SELECT id x FROM enquiryfollowup WHERE enquiryid = '$enquiryid' "))['x'];

        if($update){

            mysqli_query($con,"UPDATE enquiryfollowup SET followuptime = '$followuptime',followupnote = '$descenquiry', updated_by = '$created_by',updated_time = '$CURRENT_MILLIS' WHERE enquiryid = '$enquiryid' AND id = '$enquiryfollowupId'");

            echo '{"status":"success"}';

        } else {

            echo '{"status":"falid1"}';

        }

        _close($con);

    } else {

        echo '{"status":"falid"}';

    }

?>