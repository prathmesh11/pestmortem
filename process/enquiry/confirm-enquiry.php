<?php 

  $base    = '../../';

  $navenq2 = 'background:#1B1464;';

  include('header.php');

  if (!in_array('Confirm Enquiry', $arrysession)) {
   
    echo "<script>window.location.href='/process/dashboard.php';</script>";
   
    exit;

}

?>
<br>

<style>

  .input-container {

    max-width: 300px;
    background-color: #EDEDED;
    border: 1px solid #DFDFDF;
    border-radius: 5px;
  }

  input[type='file'] {
    display: none;
  }

  .file-info {
    font-size: 0.9em;
  }

  .browse-btn {
    background: #03A595;
    color: #fff;
    min-height: 35px;
    padding: 10px;
    border: none;
    border-top-left-radius: 5px;
    border-bottom-left-radius: 5px;
  }

  .browse-btn:hover {
    background: #4ec0b4;
  }

  @media (max-width: 300px) {
    button {
      width: 100%;
      border-top-right-radius: 5px;
      border-bottom-left-radius: 0;
    }

    .file-info {
      display: block;
      margin: 10px 5px;
    }
  }

  .well-link {
    text-decoration: none !important;
    color: #000000;
    font-weight: bold;
  }

  .panel-shadow {
    box-shadow: 0 8px 10px 1px rgba(0, 0, 0, .14), 0 3px 14px 2px rgba(0, 0, 0, .12), 0 5px 5px -3px rgba(0, 0, 0, .2);
  }

</style>

<img class="hidden" id=imageid src="../../../../img/PestMortemLogo.jpg">

<div class="table-ui container-fluid">

  <div class="tr row">

    <div class="col-sm-2 th">Enquiry No.</div>

    <div class="col-sm-3 th">Customer Info</div>

    <div class="col-sm-3 th">Enquery For</div>

    <div class="col-sm-2 th">Status</div>

    <div class="col-sm-2 th">Close Status</div>

  </div>

  <?php

    $result=mysqli_query($con,"SELECT * FROM enquiry WHERE order_confirm_by<>0 AND order_confirm_time<>0 ORDER BY created_time DESC");

    while($rows = mysqli_fetch_assoc($result)){

      $enquiryid = $rows['enquiryid'];

      $state     = $rows['state'];

      $stateName = mysqli_fetch_assoc(mysqli_query($con,"SELECT statename x FROM statemaster WHERE statecode= '$state' "))['x'];

  ?>

  <div class="row tr">

    <div class="col-sm-2 td" style="word-wrap:break-word;">
      
      <?php echo $rows['enquiryid']; ?>

      <?php echo 'Enquiry Reference : '. $rows['enqReference']; ?>

      <br>

      <?php if ($rows['personName'] != 0) {

        $sales_person_id = $rows['personName'];
                        
        $salesPerson     = mysqli_fetch_assoc(mysqli_query($con,"SELECT sales_person_name x FROM salesperson WHERE id='$sales_person_id'"))['x'];

        echo '<p>Sales Person Name : ' . $salesPerson.'</p>'; 

      }?>
      
    </div>

    <div class="col-sm-3 td">

      <?php 

        echo 'Company : '.$rows['customer_name'];

        echo '<br>Address : '.$rows['customeraddress']; 

        echo '<br>Contact No  : '.$rows['contactno'];

        echo '<br>Email Id. : '.$rows['emailid'];

        echo '<br>State : '.$stateName;

      ?>

    </div>

    <div class="col-sm-3 td">

      <?php 

        echo '<div class="table-ui">';

          echo '<div class="tr row">'; 

            echo '<div class="col-sm-3 th"> Sr No </div>';

            echo '<div class="col-sm-9 th"> Service Name </div>';

          echo '</div>'; 

          $count = 1;

          $service_json = json_decode($rows['service_json']);

          foreach ($service_json as $i) {

            echo '<div class="tr row">'; 

              echo '<div class="col-sm-3 td"> '.$count.' </div>';

              echo '<div class="col-sm-9 td"> '.get_object_vars($i)['servicename'].' </div>';

            echo '</div>'; 
            
            $count++;
              
          }

        echo '</div>'; 

      ?>

    </div>

    <div class="col-sm-2 td">

      <button class="btn btn-primary btn-sm btn-block" data-toggle="modal" data-target="#myModal"
        data-enquiryid="<?php echo $rows['enquiryid']; ?>" data-branchid="<?php echo $rows['branchid']; ?>"
        onclick="enquirystatus(this)">Enquiry Status</button>

        <br>

      <?php

        $followuptime = mysqli_fetch_assoc(mysqli_query($con,"SELECT followuptime x FROM enquiryfollowup WHERE enquiryid='$enquiryid' ORDER BY id DESC LIMIT 1 "))['x'];

        $nextfollowuptime = $followuptime;

        $followUpStatusFlags = '';

        if($nextfollowuptime!=0){

          if($START_OF_THE_DAY<$nextfollowuptime && $END_OF_THE_DAY<=$nextfollowuptime){

            $followUpStatusFlags = '#3F51B5';

          }

          if($START_OF_THE_DAY>$nextfollowuptime && $END_OF_THE_DAY>$nextfollowuptime){

            $followUpStatusFlags = '#F44336';

          }
          
          if($START_OF_THE_DAY<=$nextfollowuptime && $END_OF_THE_DAY>$nextfollowuptime ){

            $followUpStatusFlags = '#4CAF50';

          }

        } else {

          $followUpStatusFlags = '#ff9800';

        }

        if($nextfollowuptime==0){ $nextfollowuptime='NO Followup'; }else{ $nextfollowuptime=date("d-m-Y",$nextfollowuptime/1000); }

        echo '<p style="background:'.$followUpStatusFlags.';color:#fff;">'.$nextfollowuptime.'</p>'; 
        
        if ($rows['quatation_id']) {


      ?>

        <button class="btn btn-sm btn-block btn-success" data-quatationid="<?php echo $rows['quatation_id']; ?>" onclick="pdf(this)">Quotation pdf</button>
     
      <?php } ?>

    </div>

    <div class="col-sm-2 td">

      <?php

        echo 'Confirm Date : '. date("d-m-Y",$rows['order_confirm_time']/1000);

        $order_confirm_by = $rows['order_confirm_by'];

        $username         = mysqli_fetch_assoc(mysqli_query($con,"SELECT username x FROM employeemaster WHERE employeeid='$order_confirm_by'"))['x'];

        echo '<br>Confirm By : '.$username;

      ?>

    </div>


  </div>

  <?php } ?>

</div>


<div id="myModal" class="modal fade" role="dialog">

  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">

      <div class="modal-header">

        <button type="button" class="close" data-dismiss="modal">&times;</button>

        <h4 class="modal-title" align="center">Enquiry Status</h4>

      </div>

      <div class="modal-body" id="enquiryStatus">

      </div>

    </div>

  </div>

</div>

<?php 
include('footer.php');
?>
<script>

function recovers(e) {

  if (confirm("Are You Sure TO recover Order?")) {

    var enquiryid = $(e).data('enquiryid');

    $.ajax({

      type: "POST",

      data: 'enquiryid=' + enquiryid,

      url: 'api/recover.php',

      cache: false,

      success: function (res) {

        if (res.status == 'success') {

          window.location.reload();

        }

      }

    })

  }
  
}


function enquirystatus(e) {

  let enquiryid = $(e).attr('data-enquiryid');

  let branchid = $(e).attr('data-branchid');


  if (enquiryid != '') {

    $.ajax({

      type: "POST",

      data: "enquiryid=" + enquiryid + "&branchid=" + branchid,

      url: 'api/enquirydetails.php',

      cache: false,

      success: function (res) {

        if (res.status == 'success') {

          let json = res.json;

          let str = '';

          for (let i in json) {

            str += '<a href="javascript:void(0)" class="well-link"><div class="panel panel-default panel-shadow"><div class="panel-heading" style="background-color:' + json[i].background + ';color:#fff;text-align: center;">' + json[i].nextfollowuptime + '</div><div class="panel-body">' + json[i].followupnote + '</div></div></a>';

          }

          $('#enquiryStatus').html(str);

        }

      }

    })

  } else {

    alert('Somthing Went Wrong');

  }

}


function getBase64FromImageUrl(url) {

  var img = new Image();

  img.setAttribute('crossOrigin', 'anonymous');

  img.onload = function () {

    var canvas    = document.createElement("canvas");
    canvas.width  = this.width;
    canvas.height = this.height;
    var ctx = canvas.getContext("2d");
    ctx.drawImage(this, 0, 0);
    var dataURL = canvas.toDataURL("image/png");
    var base64 = dataURL.replace(/^data:image\/(png|jpg);base64,/, "");
    $('#imageid').val(base64);

  };

  img.src = url;

}

getBase64FromImageUrl('/img/PestMortemLogo.jpg');

function pdf(e) {

  var quatationid = $(e).attr('data-quatationid');

  $.ajax({

    type: "POST",

    data: 'quatationid=' + quatationid,

    url: '/process/quatation/quatation/quatationpdf.php',

    cache: false,

    success: function (res) {

      if (res.status == 'success') {

        var content           = [];

        var base64            = 'data:image/jpg;base64,' + $('#imageid').val();

        var regex             = /<br\s*[\/]?>/gi;

        var conditionandscope = res.conditionandscope.replace(regex, "\n");

        var payment_terms     = res.payment_terms.replace(regex, "\n");

        var branchjson        = res.branchjson;
         
        var table1 = {

          widths: ['auto', '*', '*', '*'],
          dontBreakRows: true,
          body: []

        };


        content.push({

          layout: 'noBorders',

          table: {
            widths: ['*', '*'],
            body: [
              ['', {
                text: "PEST MORTEM (INDIA) PRIVATE LIMITED\n",
                color: '#FF0000',
                fontSize: 13,
                bold: true
              }],
              [{
                image: base64,
                width: 100,
                height: 60
                
              }, {
                text: 'Address : '+branchjson.address+ '\nTel : '+branchjson.phone+' \nEmail : ' +branchjson.email+', \nWebsite : www.pestmortem.com, \nPAN NO :'+res.pan+'  \nGST NO :'+branchjson.gstnumber,
                alignment: 'justify',
                fontSize: 9
              }],
            ]
          }

        });

        content.push({

          layout: 'noBorders',
          table: {
            widths: ['*', '*'],
            body: [
              [{
                text: "REF : " + (res.quatation_id),  
                fontSize: 12
              }, {
                text: "Date : " + res.quatation_date,
                fontSize: 11,
                alignment: 'right',
                background: '#ffff00'
              }]
            ]
          }

        });


        content.push({

          layout: 'noBorders',
          table: {
            widths: ['*'],
            body: [
              [{
                text: "TO,",
                fontSize: 11
              }],
              [{
                
                text: res.customername,
                fontSize: 11,
                background: '#ffff00'
              }],
              [{
                
                text: res.customeraddress,
                fontSize: 11
              }],
              [{
                text: "Kind Attn : " + res.kind_attend,
                fontSize: 11
              }],
              [{
                text: "Sub           : " + res.subject,
                fontSize: 11
              }]
        
            ]

          }

        });

        content.push({
          text: 'Ref              : As per enquiry\n\n',
          fontSize: 10,
          alignment: 'justify'
        });

        content.push({
          text: '\n'
        });

        content.push({
          text: 'Conditions & Scope:',
          bold: true,
          fontSize: 11,
          background: '#ffff00'
        });

        content.push({
          text: conditionandscope,
          bold: true,
          fontSize: 11,
        });

        content.push({
          text: '\n'
        });

        content.push({
          text: 'Commercial:',
          bold: true,
          fontSize: 11,
          background: '#ffff00'
        });
  

        var table1 = {

          widths: [15,'*',80,80],
          dontBreakRows: true,
          fontSize: 20,
          body: []

        };

        var particular = [{text: 'SR NO',},{text: 'SERVICES NAME',alignment: 'center'},{text: 'RATE',alignment: 'center',},{text: 'UNIT',alignment: 'center'}];

        table1['body'].push(particular);     

        content.push({

          table: {
              headerRows: 1,
              // widths: ['*'],
              widths: [15,'*',80,80],
              alignment: 'center',

              body: [[],]
          },

        });

        var cr = 1;

        var commercial = res.json;

        for(var i in commercial){
          var tb = [];
          tb.push(cr,{text:commercial[i].servicename,alignment: 'left'},{text:commercial[i].rate,alignment: 'center'},{text:commercial[i].unit,alignment: 'center'});
          table1['body'].push(tb);
          cr++;
        }


        content.push({
          text: "",
          fontSize: 20,
          alignment: 'center'
        }, {
          style: 'tablfont1',
          table: table1
        });

        content.push({
          text: "\n",
        });

        content.push({
          text: 'Payment Terms & Condition:-:-',
          bold: true,
          fontSize: 11,
          background: '#ff0000'
        });

        content.push({
          text: payment_terms,
          bold: true,
          fontSize: 11,
        });


        content.push({
          text: '\nAuthorized By  \n\n'+res.autorised_by,
          bold: true,
          fontSize: 11,
        });

        dd = {
          pageSize: 'A4',
          pageOrientation: 'portrait',
          pageMargins: [40, 30, 30, 35],
          footer: function (currentPage, pageCount) {
            return {
              margin: 10,
              columns: [{
                fontSize: 8,
                text: [{
                    text: '--------------------------------------------------------------------------' +
                      '\n',
                    margin: [0, 20]
                  },
                  {
                    text: '© Pest Mortem (India) PVT. LTD | PAGE ' + currentPage.toString() + ' of ' + pageCount,
                  }
                ],
                alignment: 'center'
              }]
            };

          },
          content,
          styles: {
            tablfont: {
              fontSize: 7
            },
            tablfont1: {
              fontSize: 9
            }
          }
        }

        var win = window.open('', '_blank');

        pdfMake.createPdf(dd).open({}, win);

      }

    }

  })

}

</script>
