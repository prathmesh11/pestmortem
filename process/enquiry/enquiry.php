<?php 
    
    $base    = '../../';

    $navenq1 = 'background:#1B1464;';



    include('header.php');

    if (!in_array("Enquiry & FollowUp", $arrysession)) {
   
        echo "<script>window.location.href='/process/dashboard.php';</script>";
       
        exit;

    }

    if (session_status() == PHP_SESSION_NONE) { session_start(); }

    $sessionby = $_SESSION['employeeid'];

    $branchid  = $_SESSION['branchid'];

    $enquiryidnew = GetEnquiryNo($con,$branchid);

    $yrdata             = strtotime($today);

    $todayDate          =  date('d F, Y',  $yrdata);

    $todayDateTimeStamp = $yrdata*1000;

?>

<br>

<div class="container-fluid" >

    <div class="row" id="section">

        <input type="hidden" value="<?php echo $_GET['enquiryid']; ?>" class="form-control input-sm" id="Getenquiryid" />
       
        <input type="hidden" value="<?php echo $_GET['id']; ?>"  class="form-control input-sm" data-name="id" id="id" />
       
        <input type="hidden" value="<?php echo $_GET['edit']; ?>" class="form-control input-sm" id="edit" />

        <input type="hidden" value="<?php echo $branchid; ?>" data-role="text" class="form-control input-sm" data-name="branchid" />

        <div class="col-sm-1">Enquiry No.</div>

        <div class="col-sm-3">

            <div class="form-group">

                <input type="text" value="<?php echo $enquiryidnew; ?>" data-role="text" class="form-control input-sm" data-name="enquiryid" readonly />

            </div>

        </div>

        <div class="col-sm-3"></div>


        <div class="col-sm-2">Set New Followup</div>

        <div class="col-sm-3">

            <div class="form-group">

                <input type="text" id="inp-enq-date" data-role="date" class="form-control input-sm" value="<?php echo $todayDate;?>">

                <input type="hidden" data-name="followuptime" data-role="date" id="inp-enq-date-stamp" value="<?php echo $todayDateTimeStamp;?>">

            </div>

        </div>

    </div>

    <hr style="margin:0;border:1px solid #000;">

    <br>

    <div class="row">

        <div class="col-sm-6" id="section">

            <div class="col-sm-3">Customer Name</div>

            <div class="col-sm-6">

                <div class="form-group">

                    <select class="form-control input-sm select-js" data-role="select" id="customerName" data-name="customerid" onchange="custdetails(this)">

                        <option value="Select">Select</option>

                        <?php 

                            $result1=mysqli_query($con, "SELECT customerid,customername FROM `customermaster` ORDER BY customername");

                            while($rows=mysqli_fetch_assoc($result1)) {

                                echo '<option value="'.$rows['customerid'].'">'.$rows['customername'].'</option>';

                            }

                        ?>

                    </select>

                </div>

            </div>

            <div class="col-sm-3">
                            
                <!-- <button class="btn btn-sm btn-info" data-toggle="modal" data-target="#myModal3">New Customer Create</button> -->

                <a data-fancybox="newentry"  href="javascript:void(0)" id="type1box" data-type="iframe" data-src="/process/create-master/customer-master/customermaster.php" class="btn btn-danger btn-sm">
                                        
                                        Create New Customer

                                    </a>
            </div>

            <div style="clear:both;"></div>

            <div class="col-sm-3">Customer Address</div>

            <div class="col-sm-9">

                <div class="form-group">

                    <input type="text" data-name="customeraddress" id="customerAddress" data-role="text" class="form-control input-sm" readonly>

                </div>

            </div>

            <div class="col-sm-3">Contact No</div>

            <div class="col-sm-9">

                <div class="form-group">

                    <input type="number" data-name="contactno" id="contactNo" data-role="number" class="form-control input-sm" readonly>

                </div>

            </div>

            <div class="col-sm-3">Email Id</div>

            <div class="col-sm-9">

                <div class="form-group">

                    <input type="text" data-name="emailid" id="emailId" data-role="text" class="form-control input-sm" readonly>

                </div>

            </div>

            <div class="col-sm-3">State</div>
            
            <div class="col-sm-9">

                <div class="form-group">

                    <select class="form-control input-sm select-js" data-role="select" id="state" data-name="state" readonly>

                        <option value="Select">Select</option>

                        <?php 

                            $result1=mysqli_query($con, "SELECT * FROM `statemaster` ORDER BY statename");

                            while($rows=mysqli_fetch_assoc($result1)) {

                                echo '<option value="'.$rows['statecode'].'">'.$rows['statename'].'</option>';

                            }

                        ?>

                    </select>

                </div>

            </div>

        </div>

        <div class="col-sm-6" id="section">

            <div class="col-sm-3">Reference Of Enquiry</div>

            <div class="col-sm-9">

                <div class="form-group">

                    <select class="form-control input-sm" data-role="select" id="enqReference" onchange="salesperson()" data-name="enqreference">

                        <option value="Select">Select Reference</option>

                        <option value="Customer Reference">Customer Reference</option>

                        <option value="Sales Person">Sales Person</option>

                        <option value="Advertisement">Advertisement</option>

                        <option value="Telephonic">Telephonic</option>

                        <option value="Email">Email</option>

                        <option value="Other">Other</option>

                    </select>

                </div>

            </div>

            <div class="col-sm-3 salePerson">Sales Person Name</div>

            <div class="col-sm-9 salePerson">

                <div class="form-group salePerson" id="salePersonssss">

                    <select class="form-control input-sm select-js" data-name="personName" id="personName" data-role="" class="form-control input-sm">

                        <option value="Select">Select Sales Person</option>

                            <?php

                                $account = mysqli_query($con,"SELECT id,sales_person_name FROM salesperson WHERE branch_id='$branchid' ORDER BY sales_person_name");

                                while ($row = mysqli_fetch_assoc($account)) {

                                    echo '<option value="'.$row['id'].'">'.$row['sales_person_name'].'</option>';

                                }
                                            
                            ?>

                    </select>

                </div>

            </div>

            <div class="col-sm-3">Category</div>

            <div class="col-sm-9">

                <div class="form-group">

                    <select class="form-control input-sm" data-role="select" id="category" data-name="category">

                        <option value="Select">Select Category</option>

                        <option value="Individual">Individual</option>

                        <option value="Business">Business</option>

                    </select>

                </div>

            </div>


            <div class="col-sm-3">Description Of Enquiry</div>

            <div class="col-sm-9">

                <div class="form-group">

                    <textarea type="text" data-name="descenquiry" data-role="text" id="descEnquiry" class="form-control" ></textarea>

                </div>

            </div>

        </div>

        <div class="col-sm-6">

            <div class="table-responsive" id="serviceTable">

                <table class="table table-list form-group" id="table-order">

                    <thead>

                        <th>#</th>

                        <th>Service Name</th>

                        <th width="20px;">Action</th>

                    </thead>

                    <tbody>

                        <tr>
                            
                            <td></td>

                            <td>

                                <div class="form-group">

                                    <select class="select-js form-control input-sm" data-role='select' id="service" data-name="service">

                                        <option value="Select">Select</option>

                                        <?php
                                        
                                            $select = mysqli_query($con,"SELECT servicename,serviceid FROM servicemaster ORDER BY servicename ");

                                            while($rows = mysqli_fetch_assoc($select)){
                                                                    
                                                echo '<option value="'.$rows['serviceid'].'">'.$rows['servicename'].'</option>';
                                                                    
                                            } 
                                        
                                        ?>

                                    </select>

                                </div>
                            
                            </td>

                            <td>

                                <button class="btn btn-success btn-sm" id="btn-add" data-edit="" onclick="add(this)">Add</button>

                            </td>

                        </tr>
                            
                    </tbody>
                    
                </table>   

            </div>       
                
        </div>

        <div class="col-sm-6"></div>

        <div class="col-sm-3"></div>

        <div class="col-sm-3">

            <button class="btn btn-sm btn-block btn-success" id="btn-submit" onclick="submit()">Submit</button>

        </div>

        <div class="col-sm-3">

            <button class="btn btn-sm btn-block btn-danger">Reset</button>

        </div>


    </div>

</div>





<?php 
include('footer.php');
?>

<script>

$("#state").select2({disabled:'readonly'});

function closeWin() {
    window.location.reload();
  //myWindow.close();   // Closes the new window
}

$(document).ready(function () {

    $('.select-js').select2({width: '100%'});

    $('.select').attr('style','width:100%!important;');

    // $(".js-example-tags").select2({

	// 	tags: true

	// });

});



function custdetails(e) {

    let customerid   = $(e).val();

    let customerName = $(e).find(":selected").text();

    let edit         = $('#edit').val();
    
    if(!customerName){
        
        customerName = '';
    
    }

    if(customerName != 'Select' || customerName != ''){
 
        $.ajax({

            type: "POST",

            data: "customerid=" + customerid + "&customerName=" + customerName + "&edit=" + edit,

            url: 'api/custdetails.php',

            cache: false,

            success: function (res) {

                if (res.status == 'success') {
                    // $('#customerAddress').val('');
                    
                    // $('#contactNo').val('');
                
                    // $('#emailId').val('');

                    // $('#state').val('Select').trigger('change');
                     modals.putvalue('section', res.json);

                } else {
                    
                    modals.putvalue('section', res.json);

                }
                  

            }

        })
    
    } 

    
}

function salesperson() {
    
    let enqReference = $('#enqReference').val();

    if (enqReference == 'Sales Person') {

        $("#personName").attr("data-role","select");

    } else {

        $("#personName").attr("data-role","");

        $("#personName").attr("style","");

    }

}

function category() {

    let category = $('#category').val();

    if (category != 'Select') {
        
        $.ajax({

            type: "POST",

            data: "category="+category,

            url: 'api/category.php',

            cache: false,

            success: function (res) {

                if (res.status == 'success') {

                    let json = res.json;

                    let str = "<option value='Select'>Select Category</option>";

                    for(let i in json){
                        
                        str+="<option value="+json[i].serviceid+">"+json[i].servicename+"</option>";

                    }

                    $('#service').html(str);

                } else {

                    $('#service').html("<option value='Select'>Select Category</option>");

                }

            }

        });

    } else {

        $('#service').html("<option value='Select'>Select Category</option>");

    }
    
}

$("#inp-enq-date").pickadate({

    selectYears: true,

    selectMonths: true,

    min: true,

    onClose: function () {

        try {

            var dateStamp = this.get('select')['pick'];

            $("#inp-enq-date-stamp").val(dateStamp);

        } catch (err) {


        }

    }

});

$("#inp-enq-olddate").pickadate({

    selectYears: true,

    selectMonths: true,
    
    min: true,

    onClose: function () {

        try {

            var dateStamp = this.get('select')['pick'];

            $("#inp-enq-olddate-stamp").val(dateStamp);

        } catch (err) {


        }

    }

});

function setidtomidal(e) {

    var enquiryid = $(e).data('enquiryid');

    $('#inp-enquiryidset').val(enquiryid);

}


//--------------------------Add Multiple Services start-------------------------------------//

function add(e) {

    var chk = $(e).attr('data-edit');

    var data = checker('table-order');

    var service = data.service;

    var servicename = $('#service').find(":selected").text();

    var valid = true;

    if (checker('table-order') != false) {

        valid = valid * true;

    } else {

        valid = valid * false;

    }


    $('#table-order .service').each(function () {

        var service1 = $(this).text().trim();

        if (service1 == service && chk == '') {

            valid = valid * false;

            alert('Dublicate service name');

        }

    });



    if (valid) {

        var chk = $(e).attr('data-edit');

        if (chk == '') {

            var len = $('#table-order .srNo').length + 1;

            fun_adder(len, service, servicename);

        } else {

            fun_adder_edit(chk, service, servicename);

        }

        modals.clear('table-order');

        $('#btn-contact').attr('data-edit', '');

    }

}


function fun_adder(len, service, servicename) {

    var str = '<tr>';
    str += '<td align="center" class="srNo">' + len + '</td>';
    str += '<td align="center" class="service" style="display:none;">' + service + '</td>';
    str += '<td align="center" class="servicename">' + servicename + '</td>';
    str += '<td align="center"><button class="btn btn-sm btn-success" data-toggle="modal" data-target="#myModal2" onclick="contactEditor(' + len + ',this) ">E</button>  <button class="btn btn-sm btn-danger remover" onclick="remover(this)">R</buuton></td>';
    str += '</tr>';
    $('#table-order > tbody').append(str);

}

function fun_adder_edit(len, service, servicename) {

    var str = '<td align="center" class="srNo">' + len + '</td>';
    str += '<td align="center" class="service" style="display:none;">' + service + '</td>';
    str += '<td align="center" class="servicename">' + servicename + '</td>';
    str += '<td align="center"><button class="btn btn-sm btn-success" data-toggle="modal" data-target="#myModal2" onclick="contactEditor(' + len + ',this)">E</button>  <button class="btn btn-sm btn-danger remover" onclick="remover(this)">R</buuton></td>';

    $('#table-order .srNo').each(function () {
        var srNo = $(this).text().trim();
        if (srNo == len) {
            $(this).parent().html(str);
        }
    });


}

function contactEditor(srNo, e) {

    var servicecode = $(e).parent().parent().find('.service').text().trim();

    $('#service').val(servicecode).trigger('change');

    $('#btn-add').attr('data-edit', srNo);

}

//-----------------------Add Multiple Services End-------------------------//



//--------------------------Insert Function End----------------------//

function submit() {

    var valid = true;

    if (checker('section') != false) {

        valid = valid * true;

    } else {

        valid = valid * false;

    }

    var data2 = [];

    $('#table-order > tbody > tr').each(function () {

        var service     = $(this).find('.service').text();
        var servicename = $(this).find('.servicename').text();

        data2.push({
            "service": service,
            "servicename": servicename
        });

    })

    data2.shift();

    if (data2 == '') {

        $('#serviceTable').css('border', '2px solid red');

        valid = valid * false;

    } else {

        $('#serviceTable').css('border', '2px solid green');

        valid = valid * true;

    }

    let enqReference = $('#enqReference').val();

    let personName   = $('#personName').val();

    if (enqReference == 'Sales Person' && personName == 'Select' ) {

        $("#salePersonssss").css("border-bottom","1px solid red");

        valid = valid * false;

    } else {

        $("#salePersonssss").css("border-bottom","1px solid green");

        valid = valid * true;

    }
    


    if (valid) {

        var data1    = checker('section');
        var datastr1 = JSON.stringify(data1);
        var datastr2 = JSON.stringify(data2);


        $.ajax({
            type: "POST",
            data: {
                data1: datastr1,
                data2: datastr2

            },
            url: 'api/insert.php',
            cache: false,
            success: function (res) {
                if (res.status == 'success') {

                    swal({
                        type: 'success',
                        title: 'Pest Control Work Contract Create',
                        showConfirmButton: false,
                        timer: 1000
                    });

                    setTimeout(function () {
                        location.href = 'index.php';
                    }, 1000);

                }

            }

        });

    }

}

//--------------------------Insert Function End----------------------//

//--------------------------Edit Function Start----------------------//


if ($('#Getenquiryid').val() != '' && $('#id').val() != '' && $('#edit').val() == 'true') {

    $.ajax({

        type: "POST",

        data: "enquiryid=" + $('#Getenquiryid').val() + "&id=" + $('#id').val(),

        url: 'api/enquirySelect.php',

        cache: false,

        success: function (res) {

            if (res.status == 'success') {

                modals.putvalue('section', res.json);

                $('#btn-submit').attr('onclick', 'update()');

                $('#btn-submit').text('Update');

                $('#inp-enq-date').val(res.json[0].followupdate);

                $('#inp-enq-date').attr('data-stamp', res.json[0].followuptime);

               // var newOption = new Option(res.json[0].customername, res.json[0].customername, false, false);
                // var newOption = '<option value="'+res.json[0].customerid+'">'+res.json[0].customername+'</option>';
                // $('#customerName').append(newOption);
                // $('#customerName').val(res.json[0].customerid).trigger('change');

               // $('#customerName').text(res.json[0].customername).trigger('change');
               
               
                if (res.json[0].customerid == '') {
                    var newOption = '<option value="'+res.json[0].customerid+'">'+res.json[0].customername+'</option>';
                $('#customerName').append(newOption);
                $('#customerName').val(res.json[0].customerid).trigger('change');
                    // var newOption = '<option value="">'+res.json[0].customername+'</option>';
                    // $('#customerName').append(newOption);
                    // // var newOption = new Option(res.json[0].customername, res.json[0].customername, false, false);
                    // // $('#customerName').append(newOption);
                    //  $('#customerName').val().trigger('change');
                //    setTimeout(function () {
                        
                     

                //    }, 1000);




                }



                var serviceJson = res.json[0].service_json;
               
                var couter      = 1;
               
                for (var i in serviceJson) {

                    fun_adder(couter, serviceJson[i].service, serviceJson[i].servicename);

                    couter++;

                }

            }

        }

    });

}

//--------------------------Edit Function End----------------------//


//--------------------------Insert Function End----------------------//

function update() {

    var valid = true;

    if (checker('section') != false) {

        valid = valid * true;

    } else {

        valid = valid * false;

    }

    var data2 = [];

    $('#table-order > tbody > tr').each(function () {

        var service     = $(this).find('.service').text();
        var servicename = $(this).find('.servicename').text();

        data2.push({
            "service": service,
            "servicename": servicename
        });

    })

    data2.shift();

    if (data2 == '') {

        $('#serviceTable').css('border', '2px solid red');
        
        valid = valid * false;

    } else {

        $('#serviceTable').css('border', '2px solid green');

        valid = valid * true;

    }

    let enqReference = $('#enqReference').val();

    let personName   = $('#personName').val();

    if (enqReference == 'Sales Person' && personName == 'Select' ) {

        $("#salePersonssss").css("border-bottom","1px solid red");
        
        valid = valid * false;

    } else {

        $("#salePersonssss").css("border-bottom","1px solid green");

        valid = valid * true;

    }
    


    if (valid) {

        var data1    = checker('section');
        var datastr1 = JSON.stringify(data1);
        var datastr2 = JSON.stringify(data2);


        $.ajax({
            type: "POST",
            data: {
                data1: datastr1,
                data2: datastr2

            },
            url: 'api/update.php',
            cache: false,
            success: function (res) {
                if (res.status == 'success') {

                    swal({
                        type: 'success',
                        title: 'Pest Control Work Contract Create',
                        showConfirmButton: false,
                        timer: 1000
                    });

                    setTimeout(function () {
                        location.href = 'index.php';
                    }, 1000);

                }

            }

        });

    }

}

//--------------------------update Function End----------------------//

$("[data-fancybox='newentry']").fancybox({
          smallBtn: true,
          toolbar: false,
          afterClose: function () {
            parent.location.reload(true); 
          },
          iframe: {
            css: {
              width: '100%',
              height: '100%',
            }
          }
        });

</script>