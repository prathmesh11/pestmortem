<?php 

    $base    = '../../../';

    $navenq1 = 'background:#1B1464;';

    include('../header.php');



    if (session_status() == PHP_SESSION_NONE) { session_start(); }

    $sessionby = $_SESSION['employeeid'];

    $branchid  = $_SESSION['branchid'];

    $no        = getNewQuatation($con,$branchid);

?>

<style>

.table-list td,
.table-list th {
    border: 1px solid #ddd;
    padding: 1px !important;
    font-size: 13px;
}

.table-list td {
    padding-top: 10px !important;
}

.table-list tr:nth-child(even) {
    background-color: #f2f2f2;
}

.table-list th {
    padding-top: 5px;
    padding-bottom: 5px;
    text-align: center;
    background-color: #16a085;
    color: white;
}

</style>

<br>

<input type="hidden" id="quatation_id" value="<?php echo $_GET['quatation_id']; ?>" data-role="text" class="form-control input-sm" />

<div class="container-fluid">

    <div class="row" id="section">

        <input type="hidden" id="enquiryid" data-name="enquiryid" value="<?php echo $_GET['enquiryid']; ?>" data-role="text" class="form-control input-sm" />

        <div class="col-sm-2">Quatation No.</div>

        <div class="col-sm-2">

            <div class="form-group">
                
                <input type="text" value="<?php echo $no; ?>" data-role="text"
                    class="form-control input-sm" id="quatationid" data-name="quatation" readonly />

            </div>

        </div>

        <div class="col-sm-4"></div>

        <div class="col-sm-2">Quatation Date</div>

        <div class="col-sm-2">

            <div class="form-group">

                <input type="date" value="<?php echo $today; ?>" data-role="date"
                    class="form-control input-sm" data-name="quatationdate" id="quatationdate" />
            
            </div>

        </div>

    </div>

    <div class="row" id="section">

        <!-- <div class="col-sm-2">Select Catagory</div>

        <div class="col-sm-2">

            <select data-role="select" data-name='catagory' id="catagory" onchange="catagory()" class="form-control input-sm select-js">

                <option value="Select">Select Catagory</option>

                <option value="Pest Control">Pest Control</option>

                <option value="Fumigation">Fumigation</option>

            </select>

        </div> -->

        <!-- <div class="col-sm-2"></div> -->

        <div class="col-sm-2">Customer Name</div>

        <div class="col-sm-4">

            <select data-role="select" data-name='customerid' id="customerid" class="form-control input-sm select-js">

                <option value="Select">Select</option>

                <?php 

                  $result11=mysqli_query($con,"SELECT customerid,customername  FROM customermaster ORDER by customerid ASC");

                  while($rows = mysqli_fetch_assoc($result11)){

                    echo '<option value="'.$rows['customerid'].'">'.$rows['customername'].'</option>';

                  }

                ?>

            </select>

        </div>

    </div>

    <br>

    <div class="row" id="section">

        <div class="col-sm-2">Subject</div>

        <div class="col-sm-10">

            <input type="text" data-role="text" class="form-control input-sm" data-name="subject" id="subject" />

        </div>

    </div>

    <br>

    <div class="row" id="section">

        <div class="col-sm-2">Kind Attn.</div>

        <div class="col-sm-10">

            <input type="text" data-role="text" class="form-control input-sm" data-name="kind_attn" id="kind_attn" />

        </div>

    </div>

    <br>

    <div class="row" id="section">

        <div class="col-sm-2">Conditions & scope</div>

        <div class="col-sm-10">

            <div class="form-group">
            
                <textarea data-role='textarea' data-name='conditionandscope' id="conditionandscope"
                    class="form-control input-lg"> </textarea>

            </div>

        </div>

    </div>

    <hr style="border:1px solid #000;">

    <div style="clear:both;"></div>

    <table class="table-list table" id="table-service">

        <thead>

            <tr>

                <th rowspan="2" width="80">SR NO</th>

                <th rowspan="2">Service Name</th>

                <th rowspan="1" width="100">Rate</th>

                <th rowspan="1" width="100">unit</th>

                <!-- <th colspan="1" width="80">Min. Value</th>

                <th rowspan="1" width="80">GST</th>

                <th rowspan="2">Description</th> -->

                <th rowspan="2" width="100">Action</th>

            </tr>

        </thead>

        <tbody>

            <tr id="tr-table1">

                <td></td>

                <td>

                    <select class="form-control input-sm select-js1" id="servicename" data-name="servicename">

                        <option value="Select">Select</option>

                        <?php
                        
                            $select = mysqli_query($con,"SELECT servicename,serviceid FROM servicemaster ORDER BY servicename ");

                            while($rows = mysqli_fetch_assoc($select)){
                                                                    
                                echo '<option value="'.$rows['serviceid'].'">'.$rows['servicename'].'</option>';
                                                                    
                            } 
                        
                        ?>
                    
                    </select>

                </td>

                <td>
                
                    <input type="number" data-role="number" id="inp-rate" data-name="rate" class="form-control input-sm"> 

                </td>

                <td>

                    <!-- <input type="text" data-role="text" id="inp-unit" data-name="unit" class="form-control input-sm"> -->

                     <select class="form-control input-sm select-js1" data-role="select" id="inp-unit" data-name="unit">

                        <option value="Select">Select</option>

                        <?php 

                            $results=mysqli_query($con,"SELECT unitcode,uniteway FROM munit");

                            while($rows=mysqli_fetch_assoc($results)) {

                                echo '<option value="'.$rows["unitcode"].'">'.$rows["uniteway"].'</option>'; 

                            }

                        ?>
                               
                    </select>

                </td>

                <!-- <td>

                    <input type="number" data-role="number" id="inp-minval" data-name="minval" class="form-control input-sm"> 

                </td>

                <td>
                    
                    <input type="number" data-role="number" id="inp-gst" data-name="gst" class="form-control input-sm">

                </td>

                <td>

                    <input type="text" data-role="text" id="inp-description" data-name="description" class="form-control input-sm"> 
                    
                </td> -->

                <td>
                    
                    <button class="btn btn-sm btn-primary" onclick="add(this)" data-edit="" id="btn-add">Add+</button>

                </td>

            </tr>

        </tbody>

    </table>

    <div class="row" id="section">

        <div class="col-sm-2">Payment Terms & Condition</div>

        <div class="col-sm-10">

            <div class="form-group">
                
                <textarea data-role='textarea' data-name='payment_terms' id="payment_terms" class="form-control input-lg"> </textarea>
                
            </div>

        </div>

    </div>

    <div class="row" id="section">

        <div class="col-sm-2">Autorised by</div>

        <div class="col-sm-10">

            <div class="form-group">

                <input type="text" data-role="text" class="form-control input-sm" data-name="autorised_by"
                    id="autorised_by" value="Shailesh Shetty (9483800987)" />

            </div>

        </div>

    </div>

</div>

<div class="col-sm-12">

    <hr>

</div>

<div class="col-sm-3"></div>

<div class="col-sm-3">
    
    <button class="btn btn-sm btn-block btn-success" id="btn-submit" onclick="submit()">Submit</button>

</div>

<div class="col-sm-3">
    
    <button class="btn btn-sm btn-block btn-danger">Reset</button>

</div>

<div class="col-sm-12">
    <hr>
</div>

<?php

    include('../footer.php');

?>

<script>


//---Select Box ---//

<?php
    if (isset($_GET['customerid']) != '') {    
?>
    $(document).ready(function () {

        $('#customerid').val('<?php echo $_GET['customerid'];?>').trigger("change");

        $("#customerid").select2({disabled:'readonly'});

    });

<?php } ?>

// add function start :
function add(e) {

	var chk         = $(e).data('edit');
	
    var data        = checker('table-service');
	
    var servicename = $('#servicename').find(":selected").text();
	
    var serviceid   = data.servicename;
	
    var rate        = data.rate;
    
    var unitcode    = $('#inp-unit').find(":selected").text();
	
    var unit        = data.unit;
    // var minval = data.minval;
    // var gst = data.gst;
	// var description = data.description;
	var valid       = true;

	if (checker('table-service') != false) {
		
        valid = valid * true;
	
    } else {
	
    	valid = valid * false;
	
    }

	$('#table-service #servicename').each(function () {
	
    	var servicename1 = $(this).text().trim();
        
		if (servicename1 == servicename && chk == '') {
		
        	valid = valid * false;
		
        	alert('Dublicate Item Name');
		
        }
	
    });

	if (valid) {
	
    	var chk = $(e).data('edit');
	
    	if (chk == '') {
	
    		var len = $('#table-service .srno').length + 1;
	
    		fun_adder(len, servicename, serviceid, rate, unit,unitcode);
	
    	} else {
	
    		fun_adder_edit(chk, servicename, serviceid, rate, unit,unitcode);
	
    	}
	
    	modals.clear('table-service');
	
    	$('#btn-add').data('edit', '');
	
    }

}

// add function end :

// fun_adder function start :
function fun_adder(len, servicename, serviceid, rate, unit,unitcode) {
		
    var str = '<tr>';
		    
        str += '<td align="center" class="srno">' + len + '</td>';
        
        str += '<td align="center" class="servicename">' + servicename + '</td>';
        
        str += '<td align="center" class="serviceid" style="display:none;">' + serviceid + '</td>';
        
        str += '<td align="center" class="rate">' + rate + '</td>';
        
        str += '<td align="center" class="unit" style="display:none;">' + unit + '</td>';
        
        str += '<td align="center" class="unitcode">' + unitcode + '</td>';

    // str += '<td align="center" class="minval">' + minval + '</td>';
    // str += '<td align="center" class="gst">' + gst + '</td>';
    // str += '<td align="center" class="description">' + description + '</td>';

        str += '<td align="center"><button class="btn btn-sm btn-success" onclick="editor(' + len + ',this)">E</button><button class="btn btn-sm btn-danger remover" onclick="remover(this)">R</buuton></td>';
    
    str += '</tr>';
    
    $('#table-service > tbody').append(str);

}

// fun_adder function end :

// fun_adder_edit function start :

function fun_adder_edit(len, servicename, serviceid, rate, unit,unitcode) {

    var str = '<td align="center" class="srno">' + len + '</td>';
	    
        str += '<td align="center" class="servicename">' + servicename + '</td>';
		
        str += '<td align="center" class="serviceid" style="display:none;">' + serviceid + '</td>';
		
        str += '<td align="center" class="rate">' + rate + '</td>';
		
        str += '<td align="center" class="unit" style="display:none;">' + unit + '</td>';
        
        str += '<td align="center" class="unitcode" >' + unitcode + '</td>';

		// str += '<td align="center" class="minval">' + minval + '</td>';
		// str += '<td align="center" class="gst">' + gst + '</td>';
        // str += '<td align="center" class="description">' + description + '</td>';
		str += '<td align="center"><button class="btn btn-sm btn-success" onclick="editor(' + len + ',this)">E</button><button class="btn btn-sm btn-danger remover" onclick="remover(this)">R</buuton></td>';

	$('#table-service .srno').each(function(){
		
       	var srno = $(this).text().trim();
			
        if(srno==len){
			
           	$(this).parent().html(str);
			
        }
		
    });
	  
}
// fun_adder_edit function end :

//editor function start :

function editor(srno,e){

	var serviceid = $(e).parent().parent().find('.serviceid').text().trim();

	$('#servicename').val(serviceid);

	$('#servicename').trigger('change');

    $('#inp-rate').val($(e).parent().parent().find('.rate').text().trim());

	$('#inp-unit').val($(e).parent().parent().find('.unit').text().trim());

    $('#inp-unit').trigger('change');


	// $('#inp-minval').val($(e).parent().parent().find('.minval').text().trim());

    // $('#inp-gst').val($(e).parent().parent().find('.gst').text().trim());

	// $('#inp-description').val($(e).parent().parent().find('.description').text().trim());

	$('#btn-add').data('edit',srno);

}
//editor function end :

var quatation_id = $('#quatation_id').val();

if (quatation_id != "") {

	edit();

}

//--------------------------------------------catagory----------------------------------//

// function catagory() {

//     var catagory = $('#catagory').val();

//     if (catagory == 'Select') {

//         alert('Pls Select Catagory');

//     } else {

//         $.ajax({

//             type: "POST",

//             data: 'catagory=' + catagory,

//             url: 'service.php',

//             cache: false,

//             success: function (res) {

//                 if (res.status == 'success') {

//                     var json = res.json;

//                     var str = '<option value="Select">Select</option>';

//                     for (var i in json) {

//                         str += '<option value="' + json[i].serviceid + '">' + json[i].servicename + '</option>';

//                     }

//                     $('#servicename').html(str);

//                 }

//             }

//         });

//     }

// }

//-----------------------------Add <br> tag-----------------------//

$(function () {

    $('#payment_terms').keyup(function (e) {

        if (e.keyCode == 13) {

        var curr = getCaret(this);

        var val  = $(this).val();

        var end  = val.length;

        $(this).val(val.substr(0, curr) + '<br>' + val.substr(curr, end));

        }

    })

});

$(function () {

    $('#conditionandscope').keyup(function (e) {

        if (e.keyCode == 13) {

        var curr = getCaret(this);

        var val  = $(this).val();

        var end  = val.length;

        $(this).val(val.substr(0, curr) + '<br>' + val.substr(curr, end));

        }

    })

});

function getCaret(el) {

    if (el.selectionStart) {

        return el.selectionStart;

    } else if (document.selection) {

        el.focus();

        var r = document.selection.createRange();

        if (r == null) {

            return 0;

        }

        var re = el.createTextRange(),

            rc = re.duplicate();

        re.moveToBookmark(r.getBookmark());

        rc.setEndPoint('EndToStart', re);

        return rc.text.length;
        
    }

    return 0;

}


//------------------------------------Insert-------------------------------------//

function submit() {

    var valid = true;

    if (checker('section') != false) {

        valid = valid * true;

    } else {

        valid = valid * false;

    }

    var data = modals.tabletdbyclass('table-service');

    data.shift();

    if (data != '') {

        valid = valid * true;

        $('#table-service').css('border', '2px solid green');

    } else {

        valid = valid * false;

        $('#table-service').css('border', '2px solid red');

    }

    if (valid) {

        data1        = checker('section');

        var datastr1 = JSON.stringify(data1);

        var datastr2 = JSON.stringify(data);

        $.ajax({

            type: "POST",

            data: {

                data1: datastr1,

                data2: datastr2

            },

            url: 'insert.php',

            cache: false,

            success: function (res) {

                if (res.status == 'success') {

                    setTimeout(function () {

                        if ('<?php echo $_GET['enquiry'] == 'create'?>') {

                            location.href = '/process/enquiry/index.php';

                        } else {
                            
                            location.href = '/process/quatation/index.php';

                        }

                    }, 500);

                }

            }

        });

    }

}


//-----------------------------Edit---------------------------------//
function edit() {

    if ($('#quatation_id').val() != '') {

        $.ajax({

            type: "POST",

            data: "quatation_id=" + $('#quatation_id').val(),

            url: 'select.php',

            cache: false,

            success: function (res) {

                if (res.status == 'success') {

                    $('#btn-submit').text('Update');

                    $('#btn-submit').attr('onclick', 'update()');

                    $('#quatationid').val(res.quatation_id);

                    $('#quatationdate').val(res.quatation_date);

                    // $('#catagory').val(res.catagory);
                    $("#catagory").val(res.catagory).trigger('change');

                    $("#customerid").val(res.customerid);

                    $('#subject').val(res.subject);

                    $('#kind_attn').val(res.kind_attend);

                    $('#conditionandscope').val(res.conditionandscope);

                    $('#payment_terms').val(res.payment_terms);

                    $('#autorised_by').val(res.autorised_by);

                    $('#customerid').append('<option value="' + res.customerid + '" selected>' + res.customername + '</option>');


                    var json = res.json;

                    var couter = 1;

                    for (var i in json) {

                        fun_adder(couter, json[i].servicename, json[i].serviceid, json[i].rate, json[i].unit, json[i].minval, json[i].gst, json[i].description);

                        couter++;

                    }

                    catagory();
                }

            }
            
        });

    }

}


//---------------------------------update------------------------------------//

function update() {

    var valid = true;

    if (checker('section') != false) {

        valid = valid * true;

    } else {

        valid = valid * false;

    }

    var data = modals.tabletdbyclass('table-service');

	data.shift();

    if(data!=''){

        valid = valid*true;

        $('#table-service').css('border','2px solid green');

    } else {

        valid = valid*false;

        $('#table-service').css('border','2px solid red');

    }

    if (valid) {

        data1        = checker('section');

        var datastr1 = JSON.stringify(data1);

        var datastr2 = JSON.stringify(data);

        $.ajax({

            type: "POST",

            data: {

                data1: datastr1,

                data2: datastr2

            },

            url: 'update.php',

            cache: false,

            success: function (res) {

                if (res.status == 'success') {

                    setTimeout(function () {

                        if ('<?php echo $_GET['enquiry'] == 'edit'?>') {

                            location.href = '/process/enquiry/index.php';

                        } else {
                            
                            location.href = '/process/quatation/index.php';

                        }

                    }, 500);

                }

            }

        });

    }

}




//---Select Box ---//
$(document).ready(function () {

    $('.select-js1').select2({

        width: '100%'

    });

    $('.select').attr('style', 'width:100%!important;');

});
</script>