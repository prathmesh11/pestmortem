<?php

    $base = '../../../';

    include($base.'_in/connect.php');

    header('content-type: application/json; charset=utf-8');

    header("access-control-allow-origin: *");
    
    if(isset($_POST['quatationid'])) {

      $con         = _connect();

      $quatationid = _clean($con,$_POST["quatationid"]);

      $select=mysqli_query($con,"SELECT * FROM quatation WHERE quatation_id='$quatationid'");

      while($rows = mysqli_fetch_assoc($select)){

        $branchid          = $rows['branchid'];

        $branchjson        = mysqli_fetch_assoc(mysqli_query($con,"SELECT branchjson x from branchmaster WHERE branchid = '$branchid'"))['x'];

        $quatation_id      = $rows['quatation_id'];

        $quatation_date    = date('d-m-Y',$rows['quatation_date']/1000);

        $customerid        = $rows['customerid'];

        $customername      = mysqli_fetch_assoc(mysqli_query($con,"SELECT customername x FROM customermaster WHERE customerid='$customerid'"))['x'];

        $customerjson      = mysqli_fetch_assoc(mysqli_query($con,"SELECT customerjson x FROM customermaster WHERE customerid='$customerid'"))['x'];
              
        $customeraddress   = get_object_vars(json_decode($customerjson))['address'];

        $subject           = $rows['subject'];

        $kind_attend       = $rows['kind_attend'];

        $conditionandscope = $rows['conditionandscope'];
 
        $autorised_by      = $rows['autorised_by'];

        $payment_terms     = $rows['payment_terms'];

        $servicename       = $rows['servicename'];

        $rate              = $rows['rate'];

        $unit              = $rows['unit'];

        $minval            = $rows['minval'];

        $gst               = $rows['gst'];

        $pan               = substr(get_object_vars(json_decode($branchjson))['gstnumber'], 2, -3);

        $description       = $rows['description'];

        $json.=',{"servicename":"'.$servicename.'","rate":"'.$rate.'","unit":"'.$unit.'","minval":"'.$minval.'","gst":"'.$gst.'","description":"'.$description.'"}';

      }

      $json = substr($json,1);

      $json = '['.$json.']';
    
      echo '{"status":"success","json":'.$json.',"pan":"'.$pan.'","branchjson":'.$branchjson.',"quatation_id":"'.$quatation_id.'","quatation_date":"'.$quatation_date.'","customername":"'.$customername.'","customeraddress":"'.$customeraddress.'","subject":"'.$subject.'","kind_attend":"'.$kind_attend.'","conditionandscope":"'.$conditionandscope.'","payment_terms":"'.$payment_terms.'","autorised_by":"'.$autorised_by.'"}';

    _close($con);

  } else {

    echo '{"status":"falid2"}';

  }

?>