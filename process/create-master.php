<?php
$base='../';
include($base.'_in/header.php');
?>
<style>
.panel{
    z-index: 1;
    -webkit-box-shadow: 0px 3px 7px -1px rgba(0,0,0,0.75);
-moz-box-shadow: 0px 3px 7px -1px rgba(0,0,0,0.75);
box-shadow: 0px 3px 7px -1px rgba(0,0,0,0.75);
}
.panel-heading,.panel-body{
text-align:center;
}
.panel-heading{
    -webkit-box-shadow: 0px 3px 7px -1px rgba(0,0,0,0.75);
-moz-box-shadow: 0px 3px 7px -1px rgba(0,0,0,0.75);
box-shadow: 0px 3px 7px -1px rgba(0,0,0,0.75);  
}
.panel:hover{
    -webkit-box-shadow: 4px 6px 14px -2px rgba(0,0,0,0.75);
-moz-box-shadow: 4px 6px 14px -2px rgba(0,0,0,0.75);
box-shadow: 4px 6px 14px -2px rgba(0,0,0,0.75);

}
.hidestyle{
display:none;
}
</style>
<div class="container-fluid">
    <div class="row content">
        <div class="col-sm-3">
            <div class="panel panel-default">
            <a href="/process/create-master/item-master/index.php"> <div class="panel-heading">Item Master</div>
                <div class="panel-body">Click Here</div>
                </a>
            </div>
          
        </div>
        <div class="col-sm-3">
            <div class="panel panel-default">
            <a href="/process/create-master/category/index.php"> 
                <div class="panel-heading">Category</div>
                <div class="panel-body">Click Here</div>
                </a>
            </div>
        </div>

        <!-- <div class="col-sm-3 hidestyle">
            <div class="panel panel-default">
            <a href="/process/create-master/item-master/index.php"> 
                <div class="panel-heading">Stock Items</div>
                <div class="panel-body">Click Here</div>
                </a>
            </div>   
        </div> -->

         <!--<div class="col-sm-3">
            <div class="panel panel-default">
            <a href="/process/create-master/bom/index.php"> 
                <div class="panel-heading">Bill Of Material</div>
                <div class="panel-body">Click Here</div>
                </a>
            </div>-->
          
        </div>
        <div class="col-sm-3">
            <div class="panel panel-default">
            <a href="/process/create-master/supplier-master/index.php"> 
                <div class="panel-heading">supplier Master</div>
                <div class="panel-body">Click Here</div>
                </a>
            </div>
        </div>
        <div class="col-sm-3">
            <div class="panel panel-default">
            <a href="/process/create-master/supplier-item-rate-master/index.php"> 
                <div class="panel-heading">Item Rate supplier Master</div>
                <div class="panel-body">Click Here</div>
                </a>
            </div>
        </div>

         <div class="col-sm-3">
            <div class="panel panel-default">
            <a href="/process/create-master/customer-master/index.php"> 
                <div class="panel-heading">Customer Master</div>
                <div class="panel-body">Click Here</div>
                </a>
            </div>
        </div>
    
        <!-- <hr style="border-color:#FFF;">
        <div class="col-sm-3 hidestyle">
            <div class="panel panel-default">
                <div class="panel-heading">Unit Master</div>
                <div class="panel-body">Click Here</div>
            </div>
        </div> 
        <div class="col-sm-3 ">
            <div class="panel panel-default">
            <a href="/process/create-master/process-master/index.php"> 
                <div class="panel-heading">Process Master</div>
                <div class="panel-body">Click Here</div>
                </a>
            </div>
        </div>
        <div class="col-sm-3 ">
            <div class="panel panel-default">
            <a href="/process/create-master/machine-master/index.php"> 
                <div class="panel-heading">Machine Master</div>
                <div class="panel-body">Click Here</div>
                </a>
            </div>
        </div> -->
        <!-- <div class="col-sm-3 hidestyle">
            <div class="panel panel-default">
                <div class="panel-heading">State Master</div>
                <div class="panel-body">Click Here</div>
            </div>
        </div> 

        <hr style="border-color:#FFF;">
        <div class="col-sm-3 ">
            <div class="panel panel-default">
            <a href="/process/create-master/lme-master/index.php"> 
                <div class="panel-heading">LME Rate Master</div>
                <div class="panel-body">Click Here</div>
                </a>
            </div>
        </div>  -->
        <div class="col-sm-3">
            <div class="panel panel-default">
            <a href="/process/create-master/purchase-order/index.php"> 
                <div class="panel-heading">Purchase Order</div>
                <div class="panel-body">Click Here</div>
                </a>
            </div>
        </div>
    </div>
</div>

<?php
include($base.'_in/footer.php');
?>