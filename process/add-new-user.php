<?php
$base='../';
$js='<script src="'.$base.'js/fwork.js"></script>
<script src="'.$base.'js/alart.js"></script>';
$css='<link rel="stylesheet" href="'.$base.'css/fwork.css">
<link rel="stylesheet" href="'.$base.'css/alart.css">';
include($base.'_in/header.php');
include($base.'_in/connect.php');
$con=_connect();
?>
<style>
hr.style-hr {
    border: 0;
    height: 1px;
    background-image: linear-gradient(to right, rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.75), rgba(0, 0, 0, 0));
}
</style>
<div class="container-fluid" style="margin-left:5%">
    <div class="row content">
    <h2 align="center">Create New User Account</h2>
    <hr class="style-hr">
    <div class="col-sm-3"></div>
    <div class="col-sm-6" id="section">
    <div class="col-sm-4">Name</div><div class="col-sm-8"><div class="form-group"><input type="text" data-role='text' data-name='uname' class="form-control input-sm" ></div></div>
    <div class="col-sm-4">Phone</div><div class="col-sm-8"><div class="form-group"><input type="text" id='inp-phone' data-role='phone' data-name='phone' class="form-control input-sm" ></div></div>
    <div class="col-sm-4">Password</div><div class="col-sm-8"><div class="form-group"><input type="password" data-role='password' data-name='password' class="form-control input-sm" ></div></div>
    <div class="col-sm-4">Role</div><div class="col-sm-8"><div class="form-group"><select class="form-control input-sm" id="branch" onchange="role(this)" data-role='select' data-name='role'>
    <option value="Select">Select</option>
    <option value="admin">Admin</option>
    <option value="manager">Manager</option>
    <option value="employee">Employee</option>

    </select></div></div>
    <div class="col-sm-4 role" style="display:none;">Branch</div><div class="col-sm-8 role"  style="display:none;"><div class="form-group"><select class="form-control input-sm" data-name='branchid'>
    <option value="0">Select</option>

    <?php   
        $result1=mysqli_query($con,"SELECT id,sname FROM mbranch ORDER by sname ASC");
        while($rows1 = mysqli_fetch_assoc($result1)){
             $id=$rows1["id"];
             $sname=$rows1["sname"];
             echo '<option value="'.$id.'">'.$sname.'</option>';
    } ?>

    </select></div></div>

    <div style="clear:both;"></div>
    <hr class="style-hr">
    <div class="col-sm-6"><button class="btn btn-success btn-sm btn-block" id="btn-submit" style="margin:5px;">Submit</button></div>
    <div class="col-sm-6"><button class="btn btn-danger btn-sm btn-block" id="btn-reset" style="margin:5px;">Reset</button></div>
    </div>
    <div class="col-sm-3"></div>
    </div>
</div>
<?php
include($base.'_in/footer.php');
?>
<script>
function role(e){
var role=$(e).val();
if(role=='admin'){
$('.role').hide();
$('#branch').val('0');
}else{
$('.role').show();
}
}

$('#btn-reset').on('click',function(){
    window.location.reload();
});

$('#btn-submit').on('click',function(){
    var valid=true;
    if(checker('section') !=false ) {
			valid=valid*true;
		}else{
		    valid=valid*false;
         }
         
    if(valid){
        data=checker('section');
      
        var datastr = JSON.stringify(data);

			$.ajax({
				type: "POST",
				data: {
					data: datastr
				},
				url: '/_api/login/add-new-user.php',
				cache: false,
				success: function (res) {
					if (res.status == 'success') {
						swal({
							type: 'success',
							title: 'New User Account Created',
							showConfirmButton: false,
							timer: 3000
						});
						setTimeout(function () {
							window.location.reload();
						}, 3000);
                    }
                    if(res.status == 'falid2'){
                        swal('Phone Number already Exist', '', 'error');
                        $('#inp-phone').val('');
                        $('#inp-phone').css('border-bottom', '1px solid red');
                    }
				}
			});
    }
})

</script>