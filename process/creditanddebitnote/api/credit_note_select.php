<?php

  $base='../../../';

  include($base.'_in/connect.php');

  header('content-type: application/json; charset=utf-8');

  header("access-control-allow-origin: *");

  if(isset($_POST['creditNoteNo']) && isset($_POST['id'])){

    $con          = _connect();
    
    $creditNoteNo = _clean($con,$_POST['creditNoteNo']);
    
    $id           = _clean($con,$_POST['id']);
    
    $select = mysqli_fetch_assoc(mysqli_query($con,"SELECT * FROM creditnote WHERE credit_note_no = '$creditNoteNo' AND id='$id'"));

    if($select){
      
      $json = '{"credit_note_no":"'.$select['credit_note_no'].'","customerid":"'.$select['customerid'].'","credit_date":"'.$select['credit_date'].'","customer_gst_no":"'.$select['customer_gst_no'].'","invoiceno":"'.$select['invoiceno'].'","credit_note_reason":"'.$select['credit_note_reason'].'","description":"'.$select['description'].'","invoice_amount":"'.$select['invoice_amount'].'","credit_taxable_amount":"'.$select['credit_taxable_amount'].'","cgst_rate":"'.$select['cgst_rate'].'","cgst_amount":"'.$select['cgst_amount'].'","sgst_rate":"'.$select['sgst_rate'].'","sgst_amount":"'.$select['sgst_amount'].'","igst_rate":"'.$select['igst_rate'].'","igst_amount":"'.$select['igst_amount'].'","credit_net_amount":"'.$select['credit_net_amount'].'"}';
          
      echo '{"status":"success","json":['.$json.']}';   

    } else {

      echo '{"status":"falid1"}';

    }

      _close($con);

  } else {

    echo '{"status":"falid2"}';

  }

?>