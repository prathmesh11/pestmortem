<?php

    $base = '../../../';

    include($base.'_in/connect.php');

    header('content-type: application/json; charset=utf-8');

    header("access-control-allow-origin: *");

    if(isset($_POST['data']) && isset($_POST['branchid']) && isset($_POST['id'])){

        $con = _connect();

        if (session_status()==PHP_SESSION_NONE) { session_start(); }

        $created_by           = $_SESSION['employeeid'];

        $id                   = $_POST['id'];

        $branchid             = $_POST['branchid'];

        $data                 = get_object_vars(json_decode($_POST['data']));

        $debit_note_no        = $data['debit_note_no'];
        
        $debit_date           = $data['debit_date'];
        
        $supplierid           = $data['supplierid'];
        
        $supplier_gst_no      = $data['supplier_gst_no'];
        
        $purchase_bill_no     = $data['purchase_bill_no'];
        
        $debit_note_reason    = $data['debit_note_reason'];
        
        $description          = $data['description'];
        
        $purchse_bill_amount  = $data['purchse_bill_amount'];
        
        $debit_taxable_amount = $data['debit_taxable_amount'];
        
        $cgst_rate            = $data['cgst_rate'];
        
        $cgst_amount          = $data['cgst_amount'];
        
        $sgst_rate            = $data['sgst_rate'];
        
        $sgst_amount          = $data['sgst_amount'];
        
        $igst_rate            = $data['igst_rate'];
        
        $igst_amount          = $data['igst_amount'];

        $debit_net_amount     = $data['debit_net_amount']; 

        mysqli_query($con,"UPDATE purchasebill SET debitnotestatus = '' WHERE branchid = '$branchid' AND supplierid = '$supplierid' AND purchaseid = '$purchase_bill_no'");

        $update = mysqli_query($con,"UPDATE debitnote SET debit_note_no = '$debit_note_no', debit_date = '$debit_date', supplierid = '$supplierid', supplier_gst_no = '$supplier_gst_no', purchase_bill_no = '$purchase_bill_no', debit_note_reason = '$debit_note_reason', description = '$description', purchse_bill_amount = '$purchse_bill_amount', debit_taxable_amount = '$debit_taxable_amount', cgst_rate = '$cgst_rate', cgst_amount = '$cgst_amount', sgst_rate = '$sgst_rate', sgst_amount = '$sgst_amount', igst_rate = '$igst_rate', igst_amount = '$igst_amount', debit_net_amount = '$debit_net_amount', updated_by = '$created_by', updated_time = '$CURRENT_MILLIS' WHERE branchid = '$branchid' AND debit_note_no = '$debit_note_no' AND id = '$id'");

        if($update){     

            mysqli_query($con,"UPDATE purchasebill SET debitnotestatus = '$debit_note_no' WHERE branchid = '$branchid' AND supplierid = '$supplierid' AND purchaseid = '$purchase_bill_no'");

            echo '{"status":"success"}';

        } else {

            echo '{"status":"falid1"}';
        }
        
        _close($con);

    } else {

        echo '{"status":"falid"}';

    }
?>