<?php

    $base = '../../../';

    include($base.'_in/connect.php');

    header('content-type: application/json; charset=utf-8');

    header("access-control-allow-origin: *");

    if(isset($_POST['data']) && isset($_POST['branchid']) && isset($_POST['id'])){

        $con = _connect();

        if (session_status()==PHP_SESSION_NONE) { session_start(); }

        $created_by            = $_SESSION['employeeid'];

        $branchid              = $_POST['branchid'];

        $id                    = $_POST['id'];

        $data                  = get_object_vars(json_decode($_POST['data']));

        $credit_note_no        = $data['credit_note_no'];
        
        $credit_date           = $data['credit_date'];

        $customerid            = $data['customerid'];
        
        $customer_gst_no       = $data['customer_gst_no'];
        
        $invoiceno             = $data['invoiceno'];
        
        $credit_note_reason    = $data['credit_note_reason'];
        
        $description           = $data['description'];
        
        $invoice_amount        = $data['invoice_amount'];
        
        $credit_taxable_amount = $data['credit_taxable_amount'];
        
        $cgst_rate             = $data['cgst_rate'];
        
        $cgst_amount           = $data['cgst_amount'];
        
        $sgst_rate             = $data['sgst_rate'];
        
        $sgst_amount           = $data['sgst_amount'];
        
        $igst_rate             = $data['igst_rate'];
        
        $igst_amount           = $data['igst_amount'];
        
        $credit_net_amount     = $data['credit_net_amount'];


        mysqli_query($con,"UPDATE billing SET creditnotestatus = '' WHERE branchid = '$branchid' AND customerid = '$customerid' AND creditnotestatus = '$credit_note_no' ");


        $update = mysqli_query($con,"UPDATE creditnote SET credit_date = '$credit_date', customerid = '$customerid', customer_gst_no = '$customer_gst_no', invoiceno = '$invoiceno', credit_note_reason = '$credit_note_reason', description = '$description', invoice_amount = '$invoice_amount', credit_taxable_amount = '$credit_taxable_amount', cgst_rate = '$cgst_rate', cgst_amount = '$cgst_amount', sgst_rate = '$sgst_rate', sgst_amount = '$sgst_amount', igst_rate = '$igst_rate', igst_amount = '$igst_amount', credit_net_amount = '$credit_net_amount', created_by = '$created_by', created_time = '$CURRENT_MILLIS' WHERE branchid = '$branchid' AND credit_note_no = '$credit_note_no' AND id = '$id'");

        if($update){     

            mysqli_query($con,"UPDATE billing SET creditnotestatus = '$credit_note_no' WHERE branchid = '$branchid' AND customerid = '$customerid' AND invoice_bill = '$invoiceno'");

            echo '{"status":"success"}';

        } else {

            echo '{"status":"falid1"}';
        }
        
        _close($con);

    } else {

        echo '{"status":"falid"}';

    }
?>