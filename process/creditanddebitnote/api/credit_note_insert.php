<?php

    $base = '../../../';

    include($base.'_in/connect.php');

    header('content-type: application/json; charset=utf-8');

    header("access-control-allow-origin: *");

    if(isset($_POST['data']) && isset($_POST['branchid'])){

        $con = _connect();

        if (session_status()==PHP_SESSION_NONE) { session_start(); }

        $created_by            = $_SESSION['employeeid'];

        $branchid              = $_POST['branchid'];

        $data                  = get_object_vars(json_decode($_POST['data']));

        $credit_note_no        = $data['credit_note_no'];
        
        $credit_date           = $data['credit_date'];
        
        $customerid            = $data['customerid'];
        
        $customer_gst_no       = $data['customer_gst_no'];
        
        $invoiceno             = $data['invoiceno'];
        
        $credit_note_reason    = $data['credit_note_reason'];
        
        $description           = $data['description'];
        
        $invoice_amount        = $data['invoice_amount'];
        
        $credit_taxable_amount = $data['credit_taxable_amount'];
        
        $cgst_rate             = $data['cgst_rate'];
        
        $cgst_amount           = $data['cgst_amount'];
        
        $sgst_rate             = $data['sgst_rate'];
        
        $sgst_amount           = $data['sgst_amount'];
        
        $igst_rate             = $data['igst_rate'];
        
        $igst_amount           = $data['igst_amount'];
        
        $credit_net_amount     = $data['credit_net_amount'];

        $result                = mysqli_fetch_assoc(mysqli_query($con,"SELECT credit_note_no x FROM `creditnote` WHERE credit_note_no='$credit_note_no' AND branchid = '$branchid'"))['x'];

        if ($result) {

            $credit_note_nos_new = GetCreditNoteNo($con,$branchid);

            $insert = mysqli_query($con,"INSERT INTO creditnote (branchid, credit_note_no, credit_date, customerid, customer_gst_no, invoiceno, credit_note_reason, description, invoice_amount, credit_taxable_amount, cgst_rate, cgst_amount, sgst_rate, sgst_amount, igst_rate,igst_amount, credit_net_amount, created_by, created_time) VALUES ('$branchid', '$credit_note_nos_new', '$credit_date', '$customerid', '$customer_gst_no', '$invoiceno', '$credit_note_reason', '$description', '$invoice_amount', '$credit_taxable_amount', '$cgst_rate', '$cgst_amount', '$sgst_rate', '$sgst_amount', '$igst_rate', '$igst_amount', '$credit_net_amount', '$created_by', '$CURRENT_MILLIS')");
           
            mysqli_query($con,"UPDATE billing SET creditnotestatus = '$credit_note_nos_new' WHERE branchid = '$branchid' AND customerid = '$customerid' AND invoice_bill = '$invoiceno'");

        } else {

            $insert = mysqli_query($con,"INSERT INTO creditnote (branchid, credit_note_no, credit_date, customerid, customer_gst_no, invoiceno, credit_note_reason, description, invoice_amount, credit_taxable_amount, cgst_rate, cgst_amount, sgst_rate, sgst_amount, igst_rate,igst_amount, credit_net_amount, created_by, created_time) VALUES ('$branchid', '$credit_note_no', '$credit_date', '$customerid', '$customer_gst_no', '$invoiceno', '$credit_note_reason', '$description', '$invoice_amount', '$credit_taxable_amount', '$cgst_rate', '$cgst_amount', '$sgst_rate', '$sgst_amount', '$igst_rate', '$igst_amount', '$credit_net_amount', '$created_by', '$CURRENT_MILLIS')");
           
            mysqli_query($con,"UPDATE billing SET creditnotestatus = '$credit_note_no' WHERE branchid = '$branchid' AND customerid = '$customerid' AND invoice_bill = '$invoiceno'");

        }

        if($insert){     


            echo '{"status":"success"}';

        } else {

            echo '{"status":"falid1"}';
        }
        
        _close($con);

    } else {

        echo '{"status":"falid"}';

    }
?>