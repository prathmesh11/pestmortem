<?php

    $base = '../../../';

    include($base.'_in/connect.php');

    header('content-type: application/json; charset=utf-8');

    header("access-control-allow-origin: *");

    if(isset($_POST['data']) && isset($_POST['branchid'])){

        $con = _connect();

        if (session_status()==PHP_SESSION_NONE) { session_start(); }

        $created_by           = $_SESSION['employeeid'];

        $branchid             = $_POST['branchid'];

        $data                 = get_object_vars(json_decode($_POST['data']));

        $debit_note_no        = $data['debit_note_no'];
        
        $debit_date           = $data['debit_date'];
        
        $supplierid           = $data['supplierid'];
        
        $supplier_gst_no      = $data['supplier_gst_no'];
        
        $purchase_bill_no     = $data['purchase_bill_no'];
        
        $debit_note_reason    = $data['debit_note_reason'];
        
        $description          = $data['description'];
        
        $purchse_bill_amount  = $data['purchse_bill_amount'];
        
        $debit_taxable_amount = $data['debit_taxable_amount'];
        
        $cgst_rate            = $data['cgst_rate'];
        
        $cgst_amount          = $data['cgst_amount'];
        
        $sgst_rate            = $data['sgst_rate'];
        
        $sgst_amount          = $data['sgst_amount'];
        
        $igst_rate            = $data['igst_rate'];
        
        $igst_amount          = $data['igst_amount'];
        
        $debit_net_amount     = $data['debit_net_amount'];

        $result                = mysqli_fetch_assoc(mysqli_query($con,"SELECT debit_note_no x FROM `debitnote` WHERE debit_note_no='$debit_note_no' AND branchid = '$branchid'"))['x'];

        if ($result) {

            $debit_note_nos_new = GetDebitNoteNo($con,$branchid);

            $insert = mysqli_query($con,"INSERT INTO debitnote (branchid, debit_note_no, debit_date, supplierid, supplier_gst_no, purchase_bill_no, debit_note_reason, description, purchse_bill_amount, debit_taxable_amount, cgst_rate, cgst_amount, sgst_rate, sgst_amount, igst_rate,igst_amount, debit_net_amount, created_by, created_time) VALUES ('$branchid', '$debit_note_nos_new', '$debit_date', '$supplierid', '$supplier_gst_no', '$purchase_bill_no', '$debit_note_reason', '$description', '$purchse_bill_amount', '$debit_taxable_amount', '$cgst_rate', '$cgst_amount', '$sgst_rate', '$sgst_amount', '$igst_rate', '$igst_amount', '$debit_net_amount', '$created_by', '$CURRENT_MILLIS')");
            
            mysqli_query($con,"UPDATE purchasebill SET debitnotestatus = '$debit_note_nos_new' WHERE branchid = '$branchid' AND supplierid = '$supplierid' AND purchaseid = '$purchase_bill_no'");

        } else {
            
            $insert = mysqli_query($con,"INSERT INTO debitnote (branchid, debit_note_no, debit_date, supplierid, supplier_gst_no, purchase_bill_no, debit_note_reason, description, purchse_bill_amount, debit_taxable_amount, cgst_rate, cgst_amount, sgst_rate, sgst_amount, igst_rate,igst_amount, debit_net_amount, created_by, created_time) VALUES ('$branchid', '$debit_note_no', '$debit_date', '$supplierid', '$supplier_gst_no', '$purchase_bill_no', '$debit_note_reason', '$description', '$purchse_bill_amount', '$debit_taxable_amount', '$cgst_rate', '$cgst_amount', '$sgst_rate', '$sgst_amount', '$igst_rate', '$igst_amount', '$debit_net_amount', '$created_by', '$CURRENT_MILLIS')");
            
            mysqli_query($con,"UPDATE purchasebill SET debitnotestatus = '$debit_note_no' WHERE branchid = '$branchid' AND supplierid = '$supplierid' AND purchaseid = '$purchase_bill_no'");

        }


        if($insert){     

            echo '{"status":"success"}';

        } else {

            echo '{"status":"falid1"}';
        }
        
        _close($con);

    } else {

        echo '{"status":"falid"}';

    }
?>