<?php 

  $base    = '../../';

  $navenq1 = 'background:#1B1464;';

  include('header.php');

  if (!in_array("Credit Note", $arrysession)) {
   
    echo "<script>window.location.href='/process/dashboard.php';</script>";
   
    exit;

}

  if (session_status() == PHP_SESSION_NONE) { session_start(); }

  $sessionby = $_SESSION['employeeid'];

  $branchid  = $_SESSION['branchid'];

?>

<br>

<style>

  .input-container {

    max-width: 300px;
    background-color: #EDEDED;
    border: 1px solid #DFDFDF;
    border-radius: 5px;
  }

 

  @media (max-width: 300px) {
    button {
      width: 100%;
      border-top-right-radius: 5px;
      border-bottom-left-radius: 0;
    }

    
  }

    .tableFixHead {
        overflow-y: auto;
        height: 600px;
    }

    .tableFixHead thead th {
        position: sticky;
        top: 0;
    }

    .tableFixHead table {
        border-collapse: collapse;
        width: 100%;
    }

    /* #components th, td { padding: 8px 16px; } */
    .tableFixHead th {
        background: #2980b9;
        color: #fff;
    }

</style>

<img class="hidden" id=imageid src="../../../../img/PestMortemLogo.jpg">

<div class="tableFixHead table-responsive">

  <table class="table table-bordered " id="table-order">

    <thead>

      <tr>

        <th class="text-center">Credit Note No</th>

        <th class="text-center">Credit Date</th>

        <th class="text-center">Customer</th>

        <th class="text-center">Amount</th>

        <th class="text-center" width="15%;">Action</th>

      </tr>

    </thead>

    <tbody>

      <?php

        $result = mysqli_query($con,"SELECT * FROM creditnote WHERE branchid = '$branchid' AND delete_time = 0 ");

        while ($row = mysqli_fetch_assoc($result)) {

          $customerid   = $row['customerid'];

          $customerName = mysqli_fetch_assoc(mysqli_query($con,"SELECT customername x FROM customermaster WHERE customerid = '$customerid'"))['x'];
      
      ?>

        <tr>

          <td><?php echo $row['credit_note_no'];?> </td>

          <td><?php echo date('d-m-Y', strtotime($row['credit_date']));?> </td>
          
          <td><?php echo $customerName;?> </td>

          <td class="text-right"><?php echo $row['credit_net_amount'];?> </td>

          <td class="text-center"> 
            
            <a class="btn btn-sm btn-warning" href="creditnote.php?credit_note_no=<?php echo $row['credit_note_no']; ?>&id=<?php echo $row['id']; ?>&edit=true">Edit</a>
                
            <button class="btn btn-sm btn-danger" data-credit_debit_note_no = "<?php echo $row['credit_note_no']; ?>" data-branchid="<?php echo $branchid;?>" data-id = "<?php echo $row['id']; ?>" data-debit_credit = "Credit" onclick ="DeleteDebitCreditNote(this)">Delete</button>
                
          </td>

        </tr>

      <?php
          
        } 
      
      ?>
    
        
    </tbody>

  </table>

</div>

<a class="btn" style="font-size:25px;border-radius:50px;width:60px;height:60px;position: fixed;bottom: 40px;
    right: 40px;background:#eb2f06;color:#fff;padding-top: 10px;" href="/process/creditanddebitnote/creditnote.php">+</a>

<?php 

  include('footer.php');

?>

<script>

function DeleteDebitCreditNote(e){

    let credit_debit_note_no = $(e).attr('data-credit_debit_note_no');

    let id                   = $(e).attr('data-id');

    let branchid             = $(e).attr('data-branchid');

    let debit_credit         = $(e).attr('data-debit_credit');

    $.ajax({

        type: "POST",

        data: 'id=' + id + '&credit_debit_note_no=' + credit_debit_note_no + '&branchid=' + branchid + '&debit_credit=' + debit_credit,

        url: 'api/delete_credit_debit_note.php',

        cache: false,

        success: function (res) {

          if (res.status == 'success') {

            swal({
                        
              type: 'success',
                        
              title: 'Credit Note Delete',
                        
              showConfirmButton: false,
                        
              timer: 3000
                    
            });
                    
            setTimeout(function () {
                        
              location.href = 'index.php';
                    
            }, 3000);

          }

        }

    })
    
}




</script>