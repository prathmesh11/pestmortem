<?php 
    
  $base    = '../../';
  
  $navenq1 = 'background:#1B1464;';

  include('header.php');

  if (!in_array("Credit Note", $arrysession)) {
  
    echo "<script>window.location.href='/process/dashboard.php';</script>";
    
    exit;

  }

  if (session_status() == PHP_SESSION_NONE) { session_start(); }
  
  $sessionby       = $_SESSION['employeeid'];
  
  $branchid        = $_SESSION['branchid'];

  $credit_note_nos = GetCreditNoteNo($con,$branchid);

?>

<style>
.clr {
        clear:both;
    }
</style>

<br>

<input type="hidden" id="branchid" value="<?php echo $branchid;?>">

<div class="container-fluid" id="section">

<input type="hidden" id="creditNoteNo" value="<?php echo $_GET['credit_note_no']; ?>" class="form-control input-sm" />

<input type="hidden" id="getId" value="<?php echo $_GET['id']; ?>" class="form-control input-sm" />

<input type="hidden" id="getEdit" value="<?php echo $_GET['edit']; ?>" class="form-control input-sm" />

  <div class="row">

    <div class="col-sm-3">

      <div class="form-group">

        <label for="">Credit Note</label>
            
        <input class="form-control input-sm" type="text" data-role="text" id="receiptNo" data-name="credit_note_no" value="<?php echo $credit_note_nos;?>" readonly>
      
      </div>

    </div>

    <div class="col-sm-2">

      <div class="form-group">

        <label for="">Credit Date.</label>

        <input class="form-control input-sm" type="date" data-role="date" id="creditDate" value="<?php echo $today;?>" data-name="credit_date">

      </div>

    </div>

    <div class="col-sm-3">

      <div class="form-group">

        <label for="">Customer</label>

        <select class="form-control input-md select-js" onchange="invoice_bill()"  data-role="select" data-name="customerid" id="customerid">

          <option value="Select">Select Customer Name</option>

          <?php

            $result= mysqli_query($con,"SELECT customerid,customername  FROM customermaster ORDER by customerid ASC ");

            while($rows = mysqli_fetch_assoc($result)){

              echo '<option value="'.$rows['customerid'].'"> '.$rows['customername'].'</option>';

            }
                      
          ?>

        </select>

      </div>

    </div>

    <div class="col-sm-2">

      <div class="form-group">

        <label for="">Company Gst No</label>
        
        <input class="form-control input-sm" type="text" id="gstno" data-name="customer_gst_no" readonly>

      </div>

    </div>

    <div class="col-sm-2">

      <div class="form-group">

        <label for="">Invoice No</label>

        <select class="form-control input-md select-js" onchange="invoice_amount()"  data-name="invoiceno" id="invoiceno">

          <option value="Select">Select Invoice No</option>

        </select>

      </div>

    </div>

    <div class="col-sm-4">

      <div class="form-group">

        <label for="">Credit Note & Description</label>

        <select class="form-control input-md" data-role="select" data-name="credit_note_reason" id="creditNoteReason">

          <option value="Sales Return">Sales Return</option>

          <option value="Post Sale Discount">Post Sale Discount</option>
          
          <option value="Deficiency In Service">Deficiency In Service</option>
          
          <option value="Correction In Invoice">Correction In Invoice</option>
          
          <option value="Change In POS">Change In POS</option>
          
          <option value="Finalization Of Provisional Assesment">Finalization Of Provisional Assesment</option>
          
          <option value="Others">Others</option>

        </select>

      </div>

    </div>

     <div class="col-sm-6">

        <div class="form-group">

            <label for="">Description</label>

            <textarea class="form-control input-md" type="text" data-role="text" id="description" data-name="description"></textarea>

        </div>

    </div>

     <div class="col-sm-2">

        <div class="form-group">

            <label for="">Invoice Amount</label>

            <input class="form-control input-md" type="text" id="invoiceAmt" data-name="invoice_amount" readonly>

        </div>

    </div>

  </div>

  <hr style="margin:0;border:1px solid #000;">

  <div class="col-sm-12" style="margin-top: 10px;">

    <table class="table table-list form-group" id="table-order">

      <thead>

        <tr>

            <th rowspan="2" style="vertical-align : middle;text-align:center;">Amount</th>

            <th colspan="2">CGST</th>

            <th colspan="2">SGST</th>

            <th colspan="2">IGST</th>

            <th rowspan="2" style="vertical-align : middle;text-align:center;">Net Amount</th>

        </tr>

         <tr>

            <th>Rate</th>

            <th>Amount</th>

            <th>Rate</th>

            <th>Amount</th>
            
            <th>Rate</th>

            <th>Amount</th>


        </tr>

      </thead>

      <tbody>
                        
        <tr>
                  
          <td>
              
            <input data-name="credit_taxable_amount" class="form-control input-sm" data-role="number" onkeyup ="checkAmount()" id="taxableAmount" type="number"  />

          </td>

          <td>

            <input data-name="cgst_rate" data-role="number" id="cgstper" class="form-control input-sm" onkeyup="cgstrate()" type="number"/>

          </td>

          <td>

            <input data-name="cgst_amount" data-role="number" id="cgstamount" class="form-control input-sm" type="number" readonly/>

          </td>

          <td>

            <input data-name="sgst_rate" data-role="number" id="sgstper" class="form-control input-sm" onkeyup="sgstrate()" type="number"/>
            
          </td>

          <td>

            <input data-name="sgst_amount" data-role="number" id="sgstamount" class="form-control input-sm" type="number" readonly/>
            
          </td>

          <td>

            <input data-name="igst_rate" data-role="number" id="igstper" class="form-control input-sm" onkeyup="igstrate()" type="number"/>
            
          </td>


          <td>

            <input data-name="igst_amount" data-role="number" id="igstamount" class="form-control input-sm" type="number" readonly/>

          </td>

          <td>

            <input data-name="credit_net_amount" data-role="text" id="totalamount" class="form-control input-sm" type="text" readonly/>

          </td>

        </tr>

      </tbody>

    </table>

  </div>

  <div class="col-sm-3"></div>

  <div class="col-sm-3">

    <button class="btn btn-sm btn-block btn-success" id="btn-submit" onclick="submit()">Submit</button>

  </div>

  <div class="col-sm-3">

    <button class="btn btn-sm btn-block btn-danger">Reset</button>

  </div>

  <hr>

</div>

<?php 
  
  include('footer.php');

?>

<script>

function invoice_bill() {

  let customerid = $('#customerid').val();

  if (customerid != 'Select') {

    $.ajax({

      type: "POST",

      data: "customerid=" + customerid + "&edit=" + $('#getEdit').val() + "&creditNoteNo=" + $('#creditNoteNo').val(),

      url: 'api/invoiceno.php',

      cache: false,

      success: function (res) {

        if (res.status == 'success') {

          let json = res.json;

          let str = '<option value="Select">Select Invoice No</option>';

          for (let i in json) {

            str += '<option value="' + json[i].invoice_bill + '">' + json[i].invoice_bill + '</option>';

          }

          $('#invoiceno').html(str);

          $('#gstno').val(res.gstno);

        }

      }

    })

  } else {

    $('#invoiceno').html('<option value="Select">Select Invoice No</option>');

    $('#gstno').val('');

    $('#invoiceAmt').val('');

  }

  checkAmount();

}

function invoice_amount() {

  let invoiceno = $('#invoiceno').val();

  let branchid  = $('#branchid').val();

  if (invoiceno != 'Select') {

    $.ajax({

      type: "POST",

      data: "invoiceno=" + invoiceno + "&branchid=" + branchid,

      url: 'api/invoiceamount.php',

      cache: false,

      success: function (res) {

        if (res.status == 'success') {

          $('#invoiceAmt').val(res.contractamt);
          
          checkAmount();

        }

      }

    })

  } else {

    $('#invoiceAmt').val('');

  }


}

function checkAmount() {

  let taxableAmount = parseFloat($('#taxableAmount').val());

  let invoiceAmt    = parseFloat($('#invoiceAmt').val());

  if (!isNaN(invoiceAmt)) {

    if (taxableAmount >= invoiceAmt) {

      alert('Taxable Amount is Greater Than Invoice Amount');

      $('#taxableAmount').val('');
      
    } else {

      cgstrate();
     
      sgstrate();
     
      igstrate();
     
      totalamount();

    }
     
  } else {
    
    cgstrate();
     
    sgstrate();
  
    igstrate();
  
    totalamount();

  }

}

function cgstrate() {

  checkbox();

  var cgstper = parseFloat($('#cgstper').val());

  var amounts = parseFloat($('#taxableAmount').val());

  var cgstamt = (amounts * cgstper) / 100;


  if (isNaN(cgstamt)) {

    cgstamt = 0;

  }

  $('#cgstamount').val(cgstamt.round(2));

  totalamount();

}

function sgstrate() {

  checkbox();

  var sgstper = parseFloat($('#sgstper').val());

  var amounts = parseFloat($('#taxableAmount').val());

  var sgstamt = (amounts * sgstper) / 100;

  if (isNaN(sgstamt)) {

    sgstamt = 0;

  }

  $('#sgstamount').val(sgstamt.round(2));

  totalamount();

}

function igstrate() {
    
  checkbox();
  
  var igstper = parseFloat($('#igstper').val());
  
  var amounts = parseFloat($('#taxableAmount').val());
  
  var igstamt = (amounts * igstper) / 100;
  
  if (isNaN(igstamt)) {

    igstamt = 0;
  
  }
  
  $('#igstamount').val(igstamt.round(2));
  
  totalamount();

}

function totalamount() {

  var cgstamount = parseFloat($('#cgstamount').val());
  
  var sgstamount = parseFloat($('#sgstamount').val());

  var igstamount = parseFloat($('#igstamount').val());

  var amounts    = parseFloat($('#taxableAmount').val());
  
  var totalamount = 0;
  
  if (isNaN(cgstamount)) {

    cgstamount = 0;

  }

  if (isNaN(sgstamount)) {

    sgstamount = 0;

  }
  
  if (isNaN(igstamount)) {

    igstamount = 0;

  }
  
  var totalamount = amounts + cgstamount + sgstamount + igstamount;

  $('#totalamount').val(totalamount.round(2));

}


function checkbox(){
    
  var valcgstper=$('#cgstper').val();
  
  var valsgstper=$('#sgstper').val();
  
  var valigstper=$('#igstper').val();
  
  if(valcgstper!='' && valsgstper!='' && valigstper==''){
  
    $('#cgstper').prop('readonly',false);

    $('#sgstper').prop('readonly',false);

    $('#igstper').val(0);

    $('#igstper').prop('readonly',true);
      
  } 

  if(valcgstper=='' && valsgstper=='' && valigstper!=''){

    $('#cgstper').val(0);

    $('#sgstper').val(0);

    $('#cgstper').prop('readonly',true);

    $('#sgstper').prop('readonly',true);

    $('#igstper').prop('readonly',false);

  } 

}

function submit() {

  var valid = true;

  if (checker('section') != false) {

    valid = valid * true;

  } else {

    valid = valid * false;

  }

  if (valid) {

    var data = checker('section');

    var datastr = JSON.stringify(data);

    $.ajax({

      type: "POST",

      data: {
        
        data: datastr,

        branchid: $('#branchid').val(),

      },

      url: 'api/credit_note_insert.php',

      cache: false,

      success: function (res) {

        if (res.status == 'success') {

          swal({
                        
              type: 'success',
              
              title: 'Credit Note Create',
              
              showConfirmButton: false,
              
              timer: 3000
          
          });
          
          setTimeout(function () {
              
              location.href = 'index.php';
          
          }, 3000);

        }

      }

    })

  }

}



function edit() {

  $.ajax({

    type: "POST",

    data: "creditNoteNo=" + $('#creditNoteNo').val() + "&id=" + $('#getId').val(),

    url: 'api/credit_note_select.php',

    cache: false,

    success: function (res) {

      if (res.status == 'success') {

        $('#btn-submit').text('Update Bill');

        $('#btn-submit').attr('onclick', 'update()');
        
        modals.putvalue('section', res.json);

        setTimeout(function () {
              
          $('#invoiceno').val(res.json[0].invoiceno).trigger("change");
          
        }, 500);
       

      }

    }

  })

}

function update() {

  var valid = true;

  if (checker('section') != false) {

    valid = valid * true;

  } else {

    valid = valid * false;

  }

  if (valid) {

    var data    = checker('section');

    var datastr = JSON.stringify(data);

    $.ajax({

      type: "POST",

      data: {

        data: datastr,
        
        id: $('#getId').val(),
        
        branchid: $('#branchid').val(),

      },

      url: 'api/credit_note_update.php',

      cache: false,

      success: function (res) {

        if (res.status == 'success') {

          swal({
                        
            type: 'success',
            
            title: 'Credit Note Update',
            
            showConfirmButton: false,
            
            timer: 3000
          
          });
          
          setTimeout(function () {
              
            location.href = 'index.php';
          
          }, 3000);

        }

      }

    })

  }

}


$(document).ready(function() {

  if ($('#creditNoteNo').val() != '' && $('#getId').val() != '' && $('#getEdit').val() == 'true') {

    edit();
    
  }

});


</script>