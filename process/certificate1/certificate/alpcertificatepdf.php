<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Print Sample</title>
    <script src="../../../../../js/jquery-3.3.1.min.js"></script>
    <script src="../../../../../js/pdfmake.min.js"></script>
    <script src="../../../../../js/vfs_fonts.js"></script>
</head>
<body>

</body>
</html>
<script>
printer();
function printer() {
var fullDate = new Date()
var twoDigitMonth = ((fullDate.getMonth().length + 1) === 1) ? (fullDate.getMonth() + 1) : '' + (fullDate.getMonth() + 1);
var currentDate = fullDate.getDate() + "/" + twoDigitMonth + "/" + fullDate.getFullYear();
var content=[];

                 dd = {
                        

                    pageSize: 'A4',
                    pageMargins: [40, 20, 20, 40],
                    footer: function (currentPage, pageCount) {
                    return {
                        margin: 10,
                        columns: [{
                            fontSize: 8,
                            text: [{
                                    text: '--------------------------------------------------------------------------' +
                                        '\n',
                                    margin: [0, 20]
                                },
                                {
                                    text: '©Pest Mortem (India) PVT. LTD. || Printed On ' +currentDate ,
                                }
                            ],
                            alignment: 'center'
                        }]
                    };

                },
                    content: [
                            {
                                text: 'PEST MORTEM (INDIA) PRIVATE LIMITED',
                                style: 'header'
                            },
                            {
                                    text: '(Approved by National Plant Protection Organization, Government of India)',
                                    style: 'subheader'
                                },

                                '\n',
                                 {
                                    text: 'FUMIGATION CERTIFICATE',
                                    style: 'subheader1'
                                },
                                '\n',
                                {
                                    style: 'tableExample',
                                    table: {
                                         widths: [100, '*', 100, '*'],
                                   
                                        body: [
                                            ['Sr. No.', '0072', 'Treatment','PMI/MUM/ALP/722/18-19'],
                                            ['Dte. PPQS Regd. NO.', '030/ALP', 'Date of Issue','03/12/2018']
                                        ]
                                    },
                                        layout: 'noBorders'
                                },
                                 {
                                    text: 'This is to certify that the goods described below were treated in accordance with the fumigation treatment requirements of importing country MYANMAR and declared that the consignment has been verified free of impervious surfaces/layers such as plastic wrapping or laminated plastic films, lacquered or painted surfaces, aluminium foil, tarred or waxed papers etc. that may adversely effect the penetration of the fumigant, prior to fumigation.',
                                    style: 'declr'
                                },
                                '\n',
                                {
                                    text: 'Details of Treatment',
                                    style: 'subheader2'
                                },
                                {
                                    style: 'tableExample1',
                                    table: {
                                         widths: [100, '*', 100, '*'],
                                   
                                        body: [
                                            ['Name of fumigation', ': ALUMINIUM PHOSPHIDE', 'Date of fumigation',': 23/11/2018'],
                                            ['Place of fumigation', ': NAVKAR,Panvel', 'Dosage of fumigant',': 9 GMS PER M.T.'],
                                            ['Duration of fumigation', ': 3 DAYS/72 HOURS', 'Average ambient humidity fumigant',': 79%'],
                                            ['Fumigation performed', ': in container', 'Mini. Air temp(Deg. Cent.)',': 22'],
                                               
                                        ]
                                    },
                                        layout: 'noBorders'
                                },
                                 '\n',
                                {
                                    text: 'Description of Goods',
                                    style: 'subheader2'
                                },
                                 '\n',
                                
                                {
                                    style: 'tableExample1',
                                    table: {
                                         widths: [100, '*' ],
                                   
                                        body: [
                                            ['Container No.', ': 22x20 FT Containers & seal nos. As per list attached'],
                                            ['Name and addres of exporter', ': Ruchi Soya Limited \n : Ruchi House, Royal Palms, Survey No. 169, Aarey Milk Colony,Goregoan (east),Mumbai-400065, INDIA'],
                                            ['Name and addres of notified party', ': 1. Mega Feed Industrial Co. Ltd., Ground Floor, \n : Both=ahtayung township,Myanmar '],
                                            ['Type and description of cargo', ': Soyabean Meal (Animal Feed)'],
                                            ['Quantity(MTs)/No. of packages', ': Gross Weight: 489.330 MT'],
                                            ['Description of packaging material', ': 9813 Bags packing : In new PP Bags Each of About 50 kgs'],
                                            
                                            ['Vessel Name', ': Ever Conquest VOY. No. 083E'],
                                            ['Port Of Loading', ': Nhava Sheva, India'],
                                            ['Shipping mark or brand', ': -- '],
                                            ['Port of Entry', ': Yangon, Myanmar'],
                                            
                                            
                                        ]
                                    },
                                        layout: 'noBorders'
                                },
                                  '\n',
                                {
                                    text: 'Additional Declaration',
                                    style: 'subheader2'
                                },
                                '\n','\n','\n',
                                 {
                                    style: 'tableExample1',
                                    table: {
                                         widths: [300, '*' ],
                                   
                                        body: [
                                            ['Endorsed by Specified Officer of Dte. of','Name & sign of Accerdited Fumigation'],
                                            ['PPQS Name: sign & Office Seal','Operator with seal & date/Accerditation Number'],
                                            
                                            ]
                                       },
                                       layout: 'noBorders'
                                    },    

                            ],
                    styles: {
                            header: {
                            fontSize: 16,
                            bold: true,
                            alignment: 'center',
                          },
                            subheader: {
                            fontSize: 12,
                            bold: true,
                            alignment: 'center',
                          }, 
                          subheader1: {
                            fontSize: 14,
                            bold: true,
                            alignment: 'center',
                          },
                           subheader2: {
                            fontSize: 10,
                            bold: true,
                            
                          },
                          declr: {
                            fontSize: 10,
                            italics: true,
                            
                          },
                            tableExample: {
                                border : 0,
                                bold: true,
                                 fontSize: 10,
                                margin: [0, 5, 0, 15],
                            },
                             tableExample1: { 
                                bold: true,
                                 fontSize: 8,
                                margin: [0, 5, 0, 15],
                            },
                            tableHeader: {
                                bold: true,
                                fontSize: 13,
                                color: 'black',
                            },
                        tablfont: {
                            fontSize: 10
                        }
                    }
                }

                pdfMake.createPdf(dd).download('testprint.pdf');
              //pdfMake.createPdf(dd).open();
            
        
}
</script>