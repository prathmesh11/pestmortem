<?php 
  $base='../../../';
  $navenq1='background:#1B1464;';
  include('../header.php');

  $enqno='0';
  if($_GET['enq']){
      $enqno=$_GET['enq'];
  }
  if($_GET['id']){
      $certid=$_GET['id'];
  }
  if($_GET['copy']){
    $copy=$_GET['copy'];
  }
  if($_GET['confirmedit']){
    $confirmedit=$_GET['confirmedit'];
  }
  if($_GET['edit']){
    $edit=$_GET['edit'];
  }
  if($_GET['reEdit']){
    $reEdit=$_GET['reEdit'];
  }
  if($_GET['create']){
    $create=$_GET['create'];
  }
  if($_GET['draft']){
    $draft=$_GET['draft'];
  }

  if($_GET['newId']){
    $newId=$_GET['newId'];
  }

  if($_GET['isdraftId']){
    $isdraftId=$_GET['isdraftId'];
  }
  
  $enqno1 =$enqno;
  $enqno=str_replace('ENQ', 'ALP',$enqno);
    
  if (session_status() == PHP_SESSION_NONE) { session_start(); }
  $sessionby = $_SESSION['employeeid'];
  $branchid = $_SESSION['branchid'];

  

  $customerid=mysqli_fetch_assoc(mysqli_query($con,"SELECT customerid x FROM crmmaster WHERE created_time<>0 AND created_by<>0 AND enquiry_time<>0 AND enquiry_by<>0 AND wo_time<>0 AND wo_by<>0 AND cert_time=0 AND cert_by=0 AND job_all_time=0 AND job_all_by=0 AND job_com_time=0 AND job_com_by=0 AND close_enquiry_time=0 AND close_enquiry_by=0 AND enquiryid='$enqno' ORDER BY created_time DESC"))['x'];
  $result = mysqli_query($con,"SELECT * FROM certificatemaster WHERE id ='$certid' ");
  $rows = mysqli_fetch_assoc($result);
      $srNo=$rows['srNo'];
      $enquiryids=$rows['enquiryid'];
      $country=$rows['country'];
      $issuedate=$rows['issuedate'];
      $fumdate=$rows['fumdate'];
      $fumplace=$rows['fumplace'];
      $fumdosage=$rows['fumdosage'];
      $isdraft=$rows['isdraft'];
      if ($isdraft=='') {

        if ($newId!='') {

          $isdraft = mysqli_fetch_assoc(mysqli_query($con,"SELECT isdraft x FROM certificatemaster WHERE srNo='$newId' AND branchid='$branchid' ORDER BY id DESC "))['x'];

        }
       

      } else {

        $isdraft=$rows['isdraft'];

      }
      $branchid = $rows['branchid'];
  $branchjson=json_decode(mysqli_fetch_assoc(mysqli_query($con,"SELECT branchjson x FROM branchmaster WHERE branchid='$branchid'"))['x']);

  $branchshortname=mysqli_fetch_assoc(mysqli_query($con,"SELECT branchshortname x FROM branchmaster WHERE branchid='$branchid'"))['x'];
  class FiscalYear extends DateTime {
    private $start;
    private $end;
    public function __construct($time = null, $fiscalstart = '04-01') {
        parent::__construct($time,null);
        list($m, $d) = explode('-', $fiscalstart);
        $this->start = new DateTime();
        $this->start->setTime(0, 0, 0);
        $this->end = new DateTime();
        $this->end->setTime(23, 59, 59);
        $year = $this->format('Y');
        $this->start->setDate($year, $m, $d);
        $this->end = clone $this->start;
        if ($this->start <= $this) {
            $this->end->add(new DateInterval('P1Y'));
            $this->end->sub(new DateInterval('P1D'));
        } else {
            $this->start->sub(new DateInterval('P1Y'));
            $this->end->sub(new DateInterval('P1D'));
        }
    }
    public function Start() {
        return $this->start;
    }
    public function End() {
        return $this->end;
    }
  }
    $year = date('Y');
    $month = date("M");
    $date= date("d");
    $t1 = new FiscalYear($date+'-'+$month+'-'+$year);
    $startdate=$t1->Start()->Format('Y-m-d');
    $enddate=$t1->End()->Format('Y-m-d');
    //echo $startdate.'-'.$enddate;
    $fumdate=$rows['fumdate'];

  
  if($copy){
    //copy certificate new certificate generation
    //edit available
    //new certificate new certificate generation
      //edit available
      if (date('m') > 3) {
        $year1 = date('y');
        $year2 = date('y')+1;
        //echo $year1."-".$year2;
      }
      else {
        $year1 = date('y') -1;
        $year2 = date('y');
        //echo $year1."-".$year2;
    }

    if ($isdraft == '') {

      $srNonew=(mysqli_fetch_assoc(mysqli_query($con,"SELECT COUNT(certificateno) x FROM `certificatemaster` WHERE certType='ALP' AND certificateno<>'' AND wpmcreated<>'Y' AND isdraft<>'Y' AND fumdate BETWEEN '$startdate' AND '$enddate' "))['x']);
      //$srNonew=(mysqli_fetch_assoc(mysqli_query($con,"SELECT COUNT(certificateno) x FROM `certificatemaster` WHERE certType='ALP' AND certificateno<>'' AND wpmcreated<>'Y' AND $today BETWEEN '$startdate' AND '$enddate'"))['x']);
  
     // echo "SELECT COUNT(certificateno) x FROM `certificatemaster` WHERE certType='ALP' AND certificateno<>'' AND wpmcreated<>'Y' AND fumdate BETWEEN '$startdate' AND '$enddate' ";
      if($srNonew==""){
        //when certificate not there bet dates
        //runs ones in year
        $certno = "PMI/".$branchshortname."/ALP/1/".$year1."-".$year2;
        $srNoFY=1;
      }else{
          $certno = "PMI/".$branchshortname."/ALP/".($srNonew+1)."/".$year1."-".$year2;
        }
        // $srNocpy=(mysqli_fetch_assoc(mysqli_query($con,"SELECT COUNT(certificateno) x FROM `certificatemaster` WHERE certType='ALP' AND certificateno<>'' AND fumdate BETWEEN '$startdate' AND '$enddate'"))['x']);
        // $certno = "PMI/".$branchshortname."/ALP/".$srNocpy."/".date("y")."-".(date("y")+1);
      //$certno = "PMI/".$branchshortname."/ALP/".$srNo."/".date("y")."-".(date("y")+1);

    } else {

      $certno = "PMI/".$branchshortname."/ALP/DRAFT".$certid."/".$year1."-".$year2;

    }

    }elseif($edit){
      $certno=$rows['certificateno'];
    }elseif($confirmedit){
      $certno=$rows['certificateno'];
    }elseif($reEdit){
      $certno=$rows['certificateno'];
    }elseif($isdraft=='Y'){
      //$certno=$rows['certificateno'];
      //
      if (date('m') > 3) {
        $year1 = date('y');
        $year2 = date('y')+1;
        //echo $year1."-".$year2;
      }
      else {
        $year1 = date('y') -1;
        $year2 = date('y');
        //echo $year1."-".$year2;
      }
      $certno = "PMI/".$branchshortname."/ALP/DRAFT".$certid."/".$year1."-".$year2;
    }else{
      //new certificate new certificate generation
      //edit available
      if (date('m') > 3) {
        $year1 = date('y');
        $year2 = date('y')+1;
        //echo $year1."-".$year2;
      }
      else {
        $year1 = date('y') -1;
        $year2 = date('y');
    }

    $srNonew=(mysqli_fetch_assoc(mysqli_query($con,"SELECT COUNT(certificateno) x FROM `certificatemaster` WHERE certType='ALP' AND certificateno<>'' AND wpmcreated<>'Y' AND isdraft<>'Y' AND fumdate BETWEEN '$startdate' AND '$enddate' "))['x']);
    if($srNonew==""){
      //when certificate not there bet dates
      //runs ones in year
      $certno = "PMI/".$branchshortname."/ALP/1/".$year1."-".$year2;
      $srNoFY=1;
    }else{
        $certno = "PMI/".$branchshortname."/ALP/".($srNonew+1)."/".$year1."-".$year2;
      }

    }
?>
<style>
  .table-list td,
    .table-list th {
        border: 1px solid #ddd;
        padding: 1px !important;
        font-size: 13px;
    }
    .table-list td{
    padding-top: 10px !important;
    }

    .table-list tr:nth-child(even) {
        background-color: #f2f2f2;
    }

    .table-list th {
        padding-top: 5px;
       padding-bottom: 5px;
       text-align: center;
       background-color: #16a085;
       color: white;
   }
</style>
<br>
<input type="hidden" id="performIn" value="<?php echo $_GET['performIn']; ?>">
<input type="hidden" id="confirmedit"  value="<?php echo $_GET['confirmedit']; ?>">
<input type="hidden" id="copy"  value="<?php echo $_GET['copy']; ?>">
<input type="hidden" id="certType"  value="<?php echo $_GET['type']; ?>">
<input type="hidden" id="reEdit"  value="<?php echo $_GET['reEdit']; ?>">
<input type="hidden" id="edit"  value="<?php echo $_GET['edit']; ?>">
<input type="hidden" id="create"  value="<?php echo $_GET['create']; ?>">
<input type="hidden" id="draft"  value="<?php echo $_GET['draft']; ?>">
<input type="hidden" id="isdraftId"  value="<?php echo $_GET['isdraftId']; ?>">


<input type="hidden" id="isdraft"  value="<?php echo $isdraft; ?>">
<input type="hidden" id="branchid"  value="<?php echo $_SESSION['branchid']; ?>">
<input type="hidden" id="newsrNo"  value="<?php echo $newId; ?>">


<div class="col-sm-2"></div>
<div class="col-sm-8" style="background:#fff;">
<div class="row">
<div class="col-sm-12" align="center">
<h3><b>PEST MORTEM (INDIA) PRIVATE LIMITED</b></h3>
<h4><b>(Approved by National Plant Protection Organization, Government of India)</b></h4>
<h4><?php echo get_object_vars($branchjson)['address']; ?></h4>
 <h4><b>FUMIGATION CERTIFICATE</b></h4>
 <hr style="border:1px solid #000;">
</div>
<div id="alpcert">
      <input type="number" data-name="srNoFY" id="srNoFY" class="form-control hidden" value="<?php echo $srNoFY ?>">
      <input type="date" data-name="startdate" id="startdate" class="form-control hidden" value="<?php echo $startdate ?>">
      <input type="date" data-name="enddate" id="enddate" class="form-control hidden" value="<?php echo $enddate ?>">
<input type="hidden" data-name="enquiryid" value="<?php echo $enqno1; ?>">
<?php 
  $enqstr = explode("/",$enqno);
  $srno = $enqstr[3];   
?>
<div class="col-sm-6"><b>Sr. No. : <?php echo $certid; ?></b></div>

<div class="col-sm-6">
<!-- <b>Treatment Certificate No. : <?php echo $certno; ?></b> -->
<div class="col-sm-4"><b>Certificate No. :</b></div>
        <div class="col-sm-8">
          <?php
          if($edit){
            if($isdraft=='Y'){
            ?>
            <input type="text" data-role="text" data-name="certificateno" id="certificateno" class="form-control" value="<?php echo $certno; ?>">
            <?php
            }else{
              ?>
              <input type="text" data-role="text" data-name="certificateno" id="certificateno" class="form-control" value="<?php echo $certno; ?>">
              <?php
            } ?>
         <?php }elseif($reEdit || $confirmedit){ ?>
             <input type="text" data-name="certificateno"  class="form-control" value="<?php echo $certno; ?>" readonly>
          <?php
          }else{
          ?>
          <input type="text" data-role="text" data-name="certificateno" id="certificateno" class="form-control" value="<?php echo $certno; ?>">
          <?php
          }
          ?>
        </div>
</div>

<div class="col-sm-6"><b>Dte. PPQS Regd. No.:  030/ALP Dated: 20.04.2012</b></div>

<div class="col-sm-3" align="right"><b>Date of issue : </b></div><div class="col-sm-3"><input type="date" data-name="issuedate" class="form-control" value="<?php echo $issuedate; ?>"></div>

<?php if($_GET['draft']=="Y"){

?>
<div class="col-sm-3"> Remove From Draft : </div>
<div class="col-sm-3">
  <select data-role="select" id="draftInGet" class="form-control" onchange="changeDraft(<?php echo $certid;?>,'ALP')">

    <option value="N">NO</option>
    <option value="Y">YES</option>

  </select>
</div>
<?php
}
?>
 <div class="col-sm-12">
<hr style="border:1px solid #000;">
 <I>
     This is to certify that the goods described below were treated in accordance with the fumigation treatment requirements of importing country 
     <select data-name="country" data-role="select" id="country" class=' form-contol input-sm' style="width:200px">
      <option value="Select">Select</option>
      <?php if($country){ ?>
        <option value="<?php echo $country; ?>" selected><?php echo $country; ?></option>
      <?php } ?>
      <?php
      $result10=mysqli_query($con, "SELECT countryname FROM countrymaster WHERE countryname<>'$country'");
        while($rows10=mysqli_fetch_assoc($result10)) {
            echo '<option value="'.$rows10['countryname'].'">'.$rows10['countryname'].'</option>';
        }
      ?>
      </select> 
     and declared that the consignment has been verified free of impervious surfaces/layers such as plastic wrapping or laminated plastic films, lacquered or painted surfaces, aluminium foil, tarred or waxed papers etc. that may adversely effect the penetration of the fumigant, prior to fumigation.
 </I>
 <I>The Certifcate is valid for the consignments shipped within 21 days from the date of completion of fumigation</I>
 <BR> 
 <BR>
<div class="col-sm-6"><b>Details of Treatment</b></div>
<div class="col-sm-12">
  <div class="col-sm-3"> Name of fumigant :  </div>
  <div class="col-sm-3">
    <input class="form-control " value="ALUMINIUM PHOSPHIDE" readonly>
  </div>
  <div class="col-sm-3"> Date of fumigation : </div><div class="col-sm-3">
    <input type="date" data-role="date" data-name="fumdate" id="fumdate" class="form-control " value="<?php echo $fumdate; ?>">
  </div>
</div>
<div class="col-sm-12">
<div class="col-sm-3"> Place of fumigation : </div><div class="col-sm-3"><input type="text" data-name="fumplace" class="form-control" value="<?php echo $fumplace; ?>"></div>
<div class="col-sm-3"> Dosage of fumigant : </div>
  <div class="col-sm-3">
  <input type="number" data-name="fumdosage" data-role="text" class="form-control" value="<?php echo $fumdosage; ?>">
</div>
</div>
<div class="col-sm-12">
    <div class="col-sm-3"> Duration of fumigation : </div>
    <div class="col-sm-1">
          <p>DAYS</p>
          <p>HOURS</p>
    </div>
    <div class="col-sm-2">
      <input type="text" data-name="fumdurationdays" value="<?php echo $rows['fumdurationdays'];?>" class="form-control">
      <input type="text" data-name="fumdurationhrs" value="<?php echo $rows['fumdurationhrs'];?>" class="form-control">
    </div>
    
  <div class="col-sm-3"> 
      Average Ambient humidity(%):<br>
      <p>Fumigation performed under gastight sheet :</p>
  </div>
  <div class="col-sm-3">
          <input type="number" data-name="humidity" id="humidity" value="<?php echo $rows['humidity'];?>" class="form-control">
          <select data-name="fumperformed" data-role="select" class="form-control">
                    <option value="">Select</option>
                    <?php
                      if($rows['fumperformed']){
                      ?>
                      <option value="<?php echo $rows['fumperformed']; ?>" selected><?php echo $rows['fumperformed']; ?></option>
                      <?php
                      }
                      ?>
                    <option value="Yes">YES</option>
                    <option value="No">No</option>
          </select>
  </div>
</div>

<div class="col-sm-12">
  <div class="col-sm-3"style="color:red;"> Import / Export : </div>
  <div class="col-sm-3">
  <select data-name="importExport" data-role="select"  id="importExport" class="form-control" style="border-color:red;">
                <option value="Select">Select</option>
                    <?php
                      if($rows['importExport']){
                      ?>
                      <option value="<?php echo $rows['importExport']; ?>" selected><?php echo $rows['importExport']; ?></option>
                      <?php
                      }
                      ?>
                <option value="Import">Import</option>
                <option value="Export">Export</option>
              </select>
  </div>
    <?php
    $performIn=$_GET['performIn'];
    if($performIn=="dosage using mt"){
    ?>
          <div class="col-sm-3">Commodity :</div>
          <div class="col-sm-3">
            <input type="text" data-name="alpcommodity" class="form-control" value="<?php echo $rows['alpcommodity']; ?>">
          </div>
    <?php
      }elseif($performIn=="dosage using m3"){
    ?>
      <div class="col-sm-3" style="color:red;">Commodity : </div>
          <div class="col-sm-3">
            <select data-name="alpcommodity" data-role="select" id="alpcommodity" class="form-control select-js1" style="border-color:red;">
              <option value="Select">Select</option>
              <?php
                      if($rows['alpcommodity']){
                      ?>
                      <option value="<?php echo $rows['alpcommodity']; ?>" selected><?php echo $rows['alpcommodity']; ?></option>
                      <?php
                      }
                      ?>
              <?php
              $commodity=mysqli_query($con,"SELECT commodityname FROM `commoditymaster`");
              while($rowd = mysqli_fetch_assoc($commodity)){
                echo '<option value="'.$rowd['commodityname'].'">'.$rowd['commodityname'].'</option>';
              }
              ?>
            </select>
          </div>
    <?php
    }else{

    }
    ?>
</div>
<div class="col-sm-12">
<div class="col-sm-3"style="color:red;">Quantity (MTs) :</div>
<div class="col-sm-3">
    <input type="number" data-role="text" data-name="qty" class="form-control" value="<?php echo $rows['qty'];?>" maxlength="75" style="border-color:red;">
 </div>
 <div class="col-sm-3">Accreditation Operator : </div>
 <div class="col-sm-3">
   <select data-name="accno" data-role="select" id="accno" class="form-control select-js1">
     <option value="Select">Select</option>
     <?php
      if($rows['accno']){
        $acc_no=$rows['accno'];
      ?>
      <option value="<?php echo $rows['accno']; ?>" selected><?php echo $rows['accname']; ?></option>
      <?php
      }
     $accreditation=mysqli_query($con,"SELECT name,accno FROM `accreditationmaster` WHERE certType='ALP' AND branchid='$branchid' AND accno<>'$acc_no'");
     while($rowss = mysqli_fetch_assoc($accreditation)){
       echo '<option value="'.$rowss['accno'].'">'.$rowss['name'].'</option>';
     }
     ?>
   </select>
</div>

</div>
<BR>
<BR>
<BR>
<div class="col-sm-6"> <b>Description of Goods</b> </div>
  <div class="col-sm-12">
 <div class="col-sm-4">Container Number/seal Number : </div>
 <div class="col-sm-8">
    <input type="text" data-name="containerno1" class="form-control" value="<?php echo $rows['containerno1'];?>" maxlength="75">
    <input type="text" data-name="containerno2" class="form-control" value="<?php echo $rows['containerno2'];?>" maxlength="75">
    <input type="text" data-name="containerno3" class="form-control" value="<?php echo $rows['containerno3'];?>" maxlength="75">
    <input type="text" data-name="containerno4" class="form-control" value="<?php echo $rows['containerno4'];?>" maxlength="75">
 </div>
 </div>
 <div class="col-sm-12">
<div class="col-sm-4"> Name and Address of exporter : </div><div class="col-sm-8">


<input type="text" list="exporter1"  onchange="exporterDetails(this)" data-name="exporter1" class="form-control" value="<?php echo $rows['exporter1'];?>" maxlength="75"/>
            <datalist id="exporter1" >
              <?php

                  $result= mysqli_query($con,"SELECT DISTINCTROW  exporter1  FROM `certificatemaster` WHERE branchid='$branchid' AND  certType='ALP' AND exporter2<>'' AND exporter3<>'' ");

                  while($rowex = mysqli_fetch_assoc($result)){

                    echo '<option value="'.$rowex['exporter1'].'">'.$rowex['exporter1'].'</option>';


                  }
              
              ?>
            </datalist>

    <!-- <input type="text" data-name="exporter1" class="form-control" value="<?php echo $rows['exporter1'];?>" maxlength="75">  -->
    <input type="text" data-name="exporter2" id="exporter2" class="form-control" value="<?php echo $rows['exporter2'];?>" maxlength="75"> 
    <input type="text" data-name="exporter3" id="exporter3" class="form-control" value="<?php echo $rows['exporter3'];?>" maxlength="75"> 
     


</div>
</div>
<div class="col-sm-12">
<div class="col-sm-4"> Name and Address of Importer : </div><div class="col-sm-8">
  <input type="text" data-name="consignee1" class="form-control" value="<?php echo $rows['consignee1'];?>" maxlength="75">
    <input type="text" data-name="consignee2" class="form-control" value="<?php echo $rows['consignee2'];?>" maxlength="75">  
    <input type="text" data-name="consignee3" class="form-control" value="<?php echo $rows['consignee3'];?>" maxlength="75">
     
</div>
</div>
<div class="col-sm-12">
<div class="col-sm-4"> Name and Address of notified party : </div>
  <div class="col-sm-8">
    <input type="text" data-name="notifiedparty1" class="form-control" value="<?php echo $rows['notifiedparty1'];?>" maxlength="75">  
    <input type="text" data-name="notifiedparty2" class="form-control" value="<?php echo $rows['notifiedparty2'];?>" maxlength="75">  
    <input type="text" data-name="notifiedparty3" class="form-control" value="<?php echo $rows['notifiedparty3'];?>" maxlength="75"> 
  </div>
</div>
<div class="col-sm-12">
<div class="col-sm-4"> Type and description of cargo : </div><div class="col-sm-8">
  <input type="text" data-name="cargo1" class="form-control" value="<?php echo $rows['cargo1'];?>" maxlength="75">  
    <input type="text" data-name="cargo2" class="form-control" value="<?php echo $rows['cargo2'];?>" maxlength="75">  
    <input type="text" data-name="cargo3" class="form-control" value="<?php echo $rows['cargo3'];?>" maxlength="75"> 
</div>
</div>
 <div class="col-sm-12">
 <div class="col-sm-4">Quantity (MTs)/No. of packages:</div>
 
 <div class="col-sm-8">
 <input type="text" data-name="qty1" class="form-control" value="<?php echo $rows['qty1'];?>" maxlength="75"> 
 <input type="text" data-name="qty2" class="form-control" value="<?php echo $rows['qty2'];?>" maxlength="75">
 </div>
 </div>
 <div class="col-sm-12">
  <div class="col-sm-4"> Description of packaging material : </div>
  <div class="col-sm-8">
  <input type="text" data-name="description" class="form-control" value="<?php echo $rows['description'];?>" maxlength="75">  
  <input type="text" data-name="description1" class="form-control" value="<?php echo $rows['description1'];?>" maxlength="75">  
     
</div>

</div>
<div class="col-sm-12">
<div class="col-sm-4"> Vessel name : </div><div class="col-sm-8"><input type="text" data-name="vesselname" class="form-control" value="<?php echo $rows['vesselname'];?>" maxlength="75"> </div>
</div>
<div class="col-sm-12">
<div class="col-sm-4">Port of loading : </div><div class="col-sm-8"><input type="text" data-name="loading"
 class="form-control" value="<?php echo $rows['loading'];?>" maxlength="75"> </div>
</div>
<div class="col-sm-12">
<div class="col-sm-4"> Shiping mark and brand : </div>
<div class="col-sm-8">
  <input type="text" data-name="shipingmark" class="form-control" value="<?php echo $rows['shipingmark'];?>" maxlength="75">
  <input type="text" data-name="description2" class="form-control" value="<?php echo $rows['description2'];?>" maxlength="75">
  </div>
</div>
<div class="col-sm-12">
<div class="col-sm-4"> Port of entry : </div><div class="col-sm-8"><input type="text" data-name="entryport" class="form-control" value="<?php echo $rows['entryport'];?>" maxlength="75"> </div>
</div>

 <div class="col-sm-12">
<div class="col-sm-4"><b> Additional Declaration : </b></div><div class="col-sm-8">
<input type="text" data-name="additional" class="form-control" value="<?php echo $rows['additional'];?>" maxlength="75">
<input type="text" data-name="additional1" class="form-control" value="<?php echo $rows['additional1'];?>" maxlength="75">
<input type="text" data-name="additional2" class="form-control" value="<?php echo $rows['additional2'];?>" maxlength="75">
  </div>
</div> 
<BR><BR>
<button class="btn btn-lg btn-primary" onclick="savecertificate();" id="saveCertificate" style="border-radius:0px;position:fixed;bottom:10px;right:10px;">Save Certificate</button>

 <input type="hidden" data-name="certid" value="<?php echo $certid; ?>">
<input type="hidden" data-name="enquiryid" value="<?php echo $enqno1; ?>">

</div>
<?php
include('../footer.php');
?>
<style type="text/css">
  .picker__select--year{
        height:auto;
    }
    .picker__select--month{
        height:auto;
    }

</style>
<script>

function changeDraft(e,ALP) {

  var draftInGet = $('#draftInGet').val();
  var id         = e;
  var cert       = ALP;

  if (draftInGet == 'Y') {

    Swal.fire({
      title: 'Are you sure?',
      text: "You want To Remove This Certificate From Draft !",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, Remove It!'
    }).then((result) => {


      if (result.value) {
        $.ajax({

          type: "POST",

          data: 'draftValue=' + draftInGet + '&certId=' + id + '&cert=' + cert,

          url: 'isdraft.php',

          cache: false,

          success: function (res) {

            if (res.status == 'success') {
              Swal.fire(
                'Removed!',
                'This Certificate has been Removed From Draft .',
                'success'
              )

            }
          }

        });

      } else {
        swal('Ok ! This Certificate is Keep In Draft', '', 'error');
        $('#draftInGet>option:eq(0)').prop('selected', true);

      }
    })

  } else {

    Swal.fire({
      title: 'Are you sure?',
      text: "You want To Add This Certificate From Draft !",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, Add It!'
    }).then((result) => {
      if (result.value) {


        $.ajax({

          type: "POST",

          data: 'draftValue=' + draftInGet + '&certId=' + id + '&cert=' + cert,

          url: 'isdraft.php',

          cache: false,

          success: function (res) {

            if (res.status == 'success') {
              Swal.fire(
                'Add!',
                'This Certificate has been Add From Draft .',
                'success'
              )

            }
          }

        });

      } else {
        swal('Ok ! This Certificate is Keep In Alp', '', 'error');
        $('#draftInGet>option:eq(1)').prop('selected', true);

      }
    })

  }


}

function exporterDetails(e) {

  var ExporterName = $(e).val();

  var branchid = $("#branchid").val();


  $.ajax({

    type: "POST",

    data: 'ExporterName=' + ExporterName + '&branchid=' + branchid,

    url: '../api/exporterDetails.php',

    cache: false,

    success: function (res) {

      if (res.status == 'success') {

        $("#exporter2").val(res.exporter2);
        $("#exporter3").val(res.exporter3);


      } else {

        $("#exporter2").val('');
        $("#exporter3").val('');

      }

    }

  });

}

$("#fumdate").on('change', function () {
  var date = $(this).val();
  var branchid = $("#branchid").val()
  $.ajax({
    type: "POST",
    data: 'date=' + date + '&branchid=' + branchid,
    url: '../api/selecttemp.php',
    cache: false,
    success: function (res) {
      if (res.status == 'success') {
        var json = JSON.stringify(res.json)
        $("#humidity").val(res.humidity);
      }
    }
  });
})
$('.datepicker').pickadate({
  selectYears: 100,
  selectMonths: true,
  onClose: function () {
    try {
      var dateStamp = this.get('select')['pick'];
      var id = this.get('id');
      $('#' + id).attr('data-stamp', dateStamp);
      $('#' + id).parent().find('.timepicker').click();
    } catch (err) {
      console.log(err);
    }
  }
});


$('body').css('background', '#eee');

function funtb(e) {
  var arr = [];
  $('.tds').each(function () {
    var serv = $(this).find('.serv').text().trim();
    var yesno = $(this).find('.yesno').val();
    var frq = $(this).find('.frq').val();
    if (yesno != 'NO' && frq != 'Nil') {
      arr.push({
        "serv": serv,
        "yesno": yesno,
        "frq": frq
      });
      var str = '<table class="table-list table">';
      str += '<thead>';
      str += '<tr>';
      str += '<th>Service</th>';
      str += '<th>Freq.</th>';
      str += '<th>Start Date</th>';
      str += '</tr>';
      str += '</thead>';
      str += '<tbody>';
      str += '<tr>';
      str += '<td>';
      for (var i in arr) {
        str += arr[i].serv + '<br>';
      }
      str += '</td>';
      str += '<td><input type="number" onkeyup="coter(this)"></td>';
      str += '<td id="coter">';

      str += '</td>';
      str += '</tr>';
      str += '</tbody>';
      str += '</table>';
      $('#table-ui').html(str);
    }

  })
}

function coter(e) {
  var cot = $(e).val().trim();
  if (cot != '') {
    cot = parseInt(cot);
    var str = '<table class="table">';
    str += '<thead>';
    str += '<tr>';
    str += '<th>Sr. No.</th>';
    str += '<th>Service Date</th>';
    str += '</tr>';
    str += '</thead>';
    str += '<tbody>';
    for (var i = 1; i <= cot; i++) {
      str += '<tr>';
      str += '<td>' + i + '</td>';
      str += '<td><input type="date"></td>';
      str += '</tr>';
    }
    str += '</tbody>';
    str += '</table>';
    console.log(str);

    $('#coter').html(str);
  }
}

function savecertificate() {



  if ($("#country").val() == "") {
    $("#country").val("Select");
  }
  if ($("#importExport").val() == "") {
    $("#importExport").val("Select");
  }
  if ($("#alpcommodity").val() == "") {
    $("#alpcommodity").val("Select");
  }
  var valid = true;
  if (checker('alpcert') != false) {
    valid = valid * true;
  } else {
    valid = valid * false;
  }
  if (valid) {
    $('#saveCertificate').hide();

    var data = checker('alpcert');
    var datastr = JSON.stringify(data);
    var copy = $('#copy').val();
    var certType = $('#certType').val();
    var confirmedit = $('#confirmedit').val();
    var performIn = $('#performIn').val();
    var reEdit = $("#reEdit").val();
    var edit = $('#edit').val();
    var create = $('#create').val();
    var isdraft = $("#isdraft").val();
    var newsrNo = $("#newsrNo").val();
    var isdraftId = $("#isdraftId").val();


    $.ajax({
      type: "POST",
      data: {
        data: datastr,
        copy: copy,
        confirmedit: confirmedit,
        edit: edit,
        isdraft: isdraft,
        create: create,
        reEdit: reEdit,
        performIn: performIn,
        data1: certType,
        newsrNo:newsrNo,
        isdraftId:isdraftId
      },
      url: '../api/savecertificate.php',
      cache: false,
      success: function (res) {
        if (res.status == 'success') {
          if (confirmedit) {
            window.location = "../materialissue.php";
          }
          if (reEdit) {

            if (certType == "ALP") {
              window.location = "/process/calculation/alp.php";

            } else {
              window.location = "/process/calculation/mbr.php";

            }
          }
          if ($('#draft').val() == 'Y') {
            window.location = "../draft.php";
          }
          if (!reEdit && !confirmedit && $('#draft').val() == '') {
            window.location = "../certificate.php?certPage=ALP";
          }
        }
        if (res.status == 'Duplication') {
          alert('Certificate ' + res.status + ' : Please Change Certificate No.');
          $("#certificateno").css("border-color", "red");
          $('#saveCertificate').show();

        }
        if (res.status == 'EmptybranchOrEmp') {
          //if branch id or employeeId is empty
          window.location = "../../../index.php";
        }
      }
    })
  }
}

</script>