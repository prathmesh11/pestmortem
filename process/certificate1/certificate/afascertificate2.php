<?php 
  $base='../../../';

  $navenq3='background:#1B1464;';
  include('../header.php');

  $enqno='0';
  if($_GET['enq']){
      $enqno=$_GET['enq'];
  }
  if($_GET['id']){
      $certid=$_GET['id'];
  }
  if($_GET['copy']){
    $copy=$_GET['copy'];
  }
  if($_GET['confirmedit']){
    $confirmedit=$_GET['confirmedit'];
  }
  if($_GET['reEdit']){
    $reEdit=$_GET['reEdit'];
  }
  if($_GET['edit']){
    $edit=$_GET['edit'];
  }
  if($_GET['create']){
    $create=$_GET['create'];
  }
  
  $enqno=str_replace('ENQ', 'AFAS',$enqno);

  if (session_status() == PHP_SESSION_NONE) { session_start(); }
  $sessionby = $_SESSION['employeeid'];
  $branchid = $_SESSION['branchid'];


  $customerid=mysqli_fetch_assoc(mysqli_query($con,"SELECT customerid x FROM crmmaster WHERE created_time<>0 AND created_by<>0 AND enquiry_time<>0 AND enquiry_by<>0 AND wo_time<>0 AND wo_by<>0 AND cert_time=0 AND cert_by=0 AND job_all_time=0 AND job_all_by=0 AND job_com_time=0 AND job_com_by=0 AND close_enquiry_time=0 AND close_enquiry_by=0 AND enquiryid='$enqno' ORDER BY created_time DESC"))['x'];

  $result = mysqli_query($con,"SELECT * FROM certificatemaster WHERE id ='$certid' ");
  $rows = mysqli_fetch_assoc($result);
  $isdraft=$rows['isdraft'];
  $srNo=$rows['srNo'];
  if($rows['fumtarget']=="Commodity"){ $comchecked = "checked"; }
  if($rows['fumtarget']=="Packing"){ $packchecked = "checked"; }
  if($rows['fumtarget']=="Both Commodity and Packing"){ $bothchecked = "checked"; }

  if($rows['fumconducted']=="Un-sheeted Container"){ $unchecked = "checked"; }
  if($rows['fumconducted']=="Sheeted Container"){ $shhecked = "checked"; }
  if($rows['fumconducted']=="Pressure Tested Container"){ $prchecked = "checked"; }
  if($rows['fumconducted']=="Sheeted Stack"){ $sschecked = "checked"; }
  if($rows['fumconducted']=="Chember"){ $chchecked = "checked"; }

  if($rows['targetfum']=="YES"){ $targetyeschecked = "checked"; }
  if($rows['targetfum']=="NO"){ $targetnochecked = "checked"; }

  $branchid = $rows['branchid'];
  $branchjson=json_decode(mysqli_fetch_assoc(mysqli_query($con,"SELECT branchjson x FROM branchmaster WHERE branchid='$branchid'"))['x']);

  $branchshortname=mysqli_fetch_assoc(mysqli_query($con,"SELECT branchshortname x FROM branchmaster WHERE branchid='$branchid'"))['x'];
  // if($copy==""){
  // $certno = "PMI/".$branchshortname."/AFAS/".$srNo."/".date("y")."-".(date("y")+1);
  // }else{
  //   if(!$confirmedit){
  //   $srNocpy=(mysqli_fetch_assoc(mysqli_query($con,"SELECT MAX(srNo) x FROM `certificatemaster` WHERE certType='AFAS'"))['x']);
  //   $srNocpy=$srNocpy+1;
  //   $certno = "PMI/".$branchshortname."/AFAS/".$srNocpy."/".date("y")."-".(date("y")+1);
  //  }
  // }
  class FiscalYear extends DateTime {
    private $start;
    private $end;
    public function __construct($time = null, $fiscalstart = '04-01') {
        parent::__construct($time,null);
        list($m, $d) = explode('-', $fiscalstart);
        $this->start = new DateTime();
        $this->start->setTime(0, 0, 0);
        $this->end = new DateTime();
        $this->end->setTime(23, 59, 59);
        $year = $this->format('Y');
        $this->start->setDate($year, $m, $d);
        $this->end = clone $this->start;
        if ($this->start <= $this) {
            $this->end->add(new DateInterval('P1Y'));
            $this->end->sub(new DateInterval('P1D'));
        } else {
            $this->start->sub(new DateInterval('P1Y'));
            $this->end->sub(new DateInterval('P1D'));
        }
    }
    public function Start() {
        return $this->start;
    }
    public function End() {
        return $this->end;
    }
  }
  $year = date('Y');
    $month = date("M");
    $date= date("d");
    $t1 = new FiscalYear($date+'-'+$month+'-'+$year);
    $startdate=$t1->Start()->Format('Y-m-d');
    $enddate=$t1->End()->Format('Y-m-d');
    $fumdate=$rows['fumdate'];
  
  if($copy){
    if (date('m') > 3) {
      $year1 = date('y');
      $year2 = date('y')+1;
    }
    else {
      $year1 = date('y') -1;
      $year2 = date('y');
  }
  $srNonew=(mysqli_fetch_assoc(mysqli_query($con,"SELECT COUNT(certificateno) x FROM `certificatemaster` WHERE certType='AFAS' AND certificateno<>'' AND wpmcreated<>'Y' AND fumdate BETWEEN '$startdate' AND '$enddate'"))['x']);
  if($srNonew==""){
    //when certificate not there bet dates
    //runs ones in year
    $certno = "PMI/".$branchshortname."/AFAS/1/".$year1."-".$year2;
    $srNoFY=1;
  }else{
     // $srNo=$rows['srNo'];
      $certno = "PMI/".$branchshortname."/AFAS/".($srNonew+1)."/".$year1."-".$year2;
    }
      // $srNocpy=(mysqli_fetch_assoc(mysqli_query($con,"SELECT COUNT(certificateno) x FROM `certificatemaster` WHERE certType='AFAS' AND certificateno<>'' AND fumdate BETWEEN '$startdate' AND '$enddate' "))['x']);
      // $certno = "PMI/".$branchshortname."/AFAS/".$srNocpy."/".date("y")."-".(date("y")+1);
    }elseif($edit){
      $certno=$rows['certificateno'];
    }elseif($confirmedit){
      $certno=$rows['certificateno'];
    }elseif($reEdit){
      $certno=$rows['certificateno'];
    }else{
      if (date('m') > 3) {
        $year1 = date('y');
        $year2 = date('y')+1;
        //echo $year1."-".$year2;
      }
      else {
        $year1 = date('y') -1;
        $year2 = date('y');
        //echo $year1."-".$year2;
    }
    $srNonew=(mysqli_fetch_assoc(mysqli_query($con,"SELECT COUNT(certificateno) x FROM `certificatemaster` WHERE certType='AFAS' AND certificateno<>'' AND wpmcreated<>'Y' AND fumdate BETWEEN '$startdate' AND '$enddate'"))['x']);
    if($srNonew==""){
      //when certificate not there bet dates
      //runs ones in year
      $certno = "PMI/".$branchshortname."/AFAS/1/".$year1."-".$year2;
      $srNoFY=1;
    }else{
       // $srNo=$rows['srNo'];
        $certno = "PMI/".$branchshortname."/AFAS/".($srNonew+1)."/".$year1."-".$year2;
      }
      // $srNonew=(mysqli_fetch_assoc(mysqli_query($con,"SELECT MAX(srNo) x FROM `certificatemaster` WHERE certType='AFAS'"))['x']);
      // $certno = "PMI/".$branchshortname."/AFAS/".$srNonew."/".date("y")."-".(date("y")+1);
    }
?>
<style>
  .table-list td,
    .table-list th {
        border: 1px solid #ddd;
        padding: 1px !important;
        font-size: 13px;
    }
    .table-list td{
    padding-top: 10px !important;
    }

    .table-list tr:nth-child(even) {
        background-color: #f2f2f2;
    }

    .table-list th {
        padding-top: 5px;
       padding-bottom: 5px;
       text-align: center;
       background-color: #16a085;
       color: white;
   }
</style>
<br>
<input type="hidden" id="copy"  value="<?php echo $_GET['copy']; ?>">
<input type="hidden" id="confirmedit"  value="<?php echo $_GET['confirmedit']; ?>">
<input type="hidden" id="certType"  value="<?php echo $_GET['type']; ?>">
<input type="hidden" id="performIn" value="<?php echo $_GET['performIn']; ?>">
<input type="hidden" id="reEdit"  value="<?php echo $_GET['reEdit']; ?>">
<input type="hidden" id="edit"  value="<?php echo $_GET['edit']; ?>">
<input type="hidden" id="create"  value="<?php echo $_GET['create']; ?>">
<input type="hidden" id="isdraft"  value="<?php echo $rows['isdraft']; ?>">
<input type="hidden" id="branchid"  value="<?php echo $_SESSION['branchid']; ?>">

<div class="col-sm-2"></div>
<div class="col-sm-8" style="background:#fff;">
<div class="row">
  <div class="col-sm-12" align="center">
      <h3><b>PEST MORTEM (INDIA) PRIVATE LIMITED</b></h3>
      <h4><b>(Approved by National Plant Protection Organization, Government of India)</b></h4>
      <h4><?php echo get_object_vars($branchjson)['address']; ?></h4>
      <h4><b>AFAS-METHYL BROMIDE FUMIGATION CERTIFICATE</b></h4>
  </div>
<div id="alpcert">
    <input type="hidden" data-name="enquiryid" value="<?php echo $enqno; ?>">
    <input type="number" data-name="srNoFY" id="srNoFY" class="form-control hidden" value="<?php echo $srNoFY ?>">
      <input type="date" data-name="startdate" id="startdate" class="form-control hidden" value="<?php echo $startdate ?>">
      <input type="date" data-name="enddate" id="enddate" class="form-control hidden" value="<?php echo $enddate ?>">

<div class="row">
      <div class="col-sm-3" style="padding-left:30px;"><b>Certificate No. : </b></div>
      <div class="col-sm-3">
      <?php
            if($edit){
                if($isdraft=='Y'){
                ?>
                <input type="text" data-role="text" data-name="certificateno" id="certificateno" class="form-control" value="<?php echo $certno; ?>">
                <?php
                }else{
                  ?>
                  <input type="text" data-role="text" data-name="certificateno" id="certificateno" class="form-control" value="<?php echo $certno; ?>">
                  <?php
                } ?>
                <?php }elseif($reEdit || $confirmedit){ ?>
                  <input type="text" data-name="certificateno"  class="form-control" value="<?php echo $certno; ?>" readonly>
               <?php
            }else{
            ?>
            <input type="text" data-role="text" data-name="certificateno" id="certificateno" class="form-control" value="<?php echo $certno; ?>">
            <?php
            }
            ?>
      </div>
      <div class="col-sm-3">Accreditation Operator : </div>
      <div class="col-sm-3" style="padding-right:30px;"> 
        <select data-name="accno" data-role="select" id="accno" class="form-control select-js1" >
          <option value="Select">Select</option>
          <?php
            if($rows['accno']){
              $acc_no=$rows['accno'];
            ?>
            <option value="<?php echo $rows['accno']; ?>" selected><?php echo $rows['accname']; ?></option>
            <?php
            }
          $accreditation=mysqli_query($con,"SELECT name,accno FROM `accreditationmaster` WHERE certType='AFAS' AND branchid='$branchid'AND accno<>'$acc_no'");
          while($rowss = mysqli_fetch_assoc($accreditation)){
            echo '<option value="'.$rowss['accno'].'">'.$rowss['name'].'</option>';
          }
          ?>
        </select>
      </div>
      
      <div class="col-sm-3" style="padding-left:30px;"><b>Date of Issue: </b></div>
      <div class="col-sm-3">
      <input type="date" data-name="issuedate" class="form-control" value="<?php echo $rows['issuedate']; ?>">
      </div>
      <div class="col-sm-4" style="padding-left:120px;">
        <b>AEI No. : IN 0012 MB</b>
        </div>
        
      </div>


 <div class="col-sm-12">
    <BR>
    <div class="col-sm-12" align="center"><b>TARGET OF FUMIGATION DETAILS</b></div>
    <hr style="border:1px solid #000;">
    <div class="col-sm-12">
        <div class="col-sm-3"> Target of Fumigation : </div>
        <div class="col-sm-9">
          <input type="radio" data-name="fumtarget" name="fumtarget" value="Commodity" <?php echo $comchecked; ?>>Commodity &nbsp;
          <input type="radio" data-name="fumtarget" name="fumtarget" value="Packing" <?php echo $packchecked; ?>>Packing &nbsp;
          <input type="radio" data-name="fumtarget" name="fumtarget" value="Both Commodity and Packing" <?php echo $bothchecked; ?>>Both Commodity and Packing
        </div>
    </div>
  <div class="col-sm-12">
    <div class="col-sm-3" style="color:red;">Commodity : </div>
    <div class="col-sm-3">
        <input type="text" data-role="text" data-name="mbrcommodity" class="form-control" value="<?php echo $rows['mbrcommodity']; ?>" style="border-color:red;">
    </div>
    
  </div>

  <div class="col-sm-12">
      <div class="col-sm-6">Commodity
          <input type="text" data-name="commodify" class="form-control" value="<?php echo $rows['commodify']; ?>" maxlength="40">
          <input type="text" data-name="commodify1" class="form-control" value="<?php echo $rows['commodify1']; ?>" maxlength="20">
          Consignment Link
            <input type="text" data-name="consignment" class="form-control" value="<?php echo $rows['consignment']; ?>"  maxlength="100">
      </div>
        <div class="col-sm-6">Quantity
          <input type="text" data-name="qty1" class="form-control" value="<?php echo $rows['qty1']; ?>"  maxlength="40">
          <input type="text" data-name="qty2" class="form-control" value="<?php echo $rows['qty2']; ?>"  maxlength="40">
          <input type="text" data-name="qty3" class="form-control" value="<?php echo $rows['qty3']; ?>"  maxlength="40">
        </div>
      
      <div class="col-sm-3">Country of Origin</div>
      <div class="col-sm-3">
        <select data-name="countryorg" data-role="select" id="countryorg" class='form-control'>
        <option value="Select">Select</option>
        <?php $countryorg=$rows['countryorg'];
        if($countryorg){ ?>
          <option value="<?php echo $rows['countryorg']; ?>" selected><?php echo $rows['countryorg']; ?></option>
        <?php } ?>
        <?php
        $result10=mysqli_query($con, "SELECT countryname FROM countrymaster WHERE countryname<>'$countryorg'");
          while($rows10=mysqli_fetch_assoc($result10)) {
              echo '<option value="'.$rows10['countryname'].'">'.$rows10['countryname'].'</option>';
          }
        ?>
        </select> 
      </div>
      <div class="col-sm-3">Destination Country</div>
        <div class="col-sm-3">
          <select data-name="destinationcon" data-role="select" id="destinationcon" class='form-control'>
          <option value="Select">Select</option>
          <?php $destinationcon = $rows['destinationcon'];
           if($destinationcon){ ?>
            <option value="<?php echo $rows['destinationcon']; ?>" selected><?php echo $rows['destinationcon']; ?></option>
          <?php } ?>
          <?php
          $result10=mysqli_query($con, "SELECT countryname FROM countrymaster WHERE countryname<>'$destinationcon'");
            while($rows10=mysqli_fetch_assoc($result10)) {
                echo '<option value="'.$rows10['countryname'].'">'.$rows10['countryname'].'</option>';
            }
          ?>
        </select> 
        </div>
        <div class="col-sm-3">Vessel</div>
        <div class="col-sm-3">
          <input type="text" data-name="afasvessel" class="form-control" value="<?php echo $rows['afasvessel']; ?>"  maxlength="28">
        </div>
      <div class="col-sm-3">Port of Loading</div>
          <div class="col-sm-3">
            <input type="text" placeholder="Port of Loading" data-name="loading" class="form-control" value="<?php echo $rows['loading']; ?>"  maxlength="28">
        </div>
        <div class="col-sm-3">Port Of Discharge</div>
        <div class="col-sm-3">
          <input type="text" placeholder="Port Of Discharge" data-name="portofdischarge" class="form-control" value="<?php echo $rows['portofdischarge']; ?>"  maxlength="28">
        </div>
        <div class="col-sm-3" style="color:red;">Import / Export :</div>
        <div class="col-sm-3">
            <select data-name="importExport" data-role="select" class="form-control" style="border-color:red;">
              <option value="Select">Select</option>
              <?php
              if($rows['importExport']){
                ?>
                <option value="<?php echo $rows['importExport']; ?>" selected><?php echo $rows['importExport']; ?></option>
              <?php
              }
              ?>
              <option value="Import">Import</option>
              <option value="Export">Export</option>
            </select>
        </div>
        <?php
          $performIn=$rows['performIn'];
          if($performIn=="container"){
        ?>
        <div class="col-sm-3" style="color:red;">Number of container</div>
        <div class="col-sm-3">
            <input type="number" data-name="noofcontainer" data-role="number" class="form-control" value="<?php echo $rows['noofcontainer']; ?>"  maxlength="28" style="border-color:red;">
        </div>
        <div class="col-sm-3" style="color:red;">Size of Container : </div>
              <div class="col-sm-3">
              <select data-name="sizeofcontainer" data-role="select" id="sizeofcontainer" class="form-control" style="border-color:red;">
                    <option value="Select">Select</option>
                    <?php
                    if($rows['sizeofcontainer']){
                      ?>
                      <option value="<?php echo $rows['sizeofcontainer']; ?>" selected><?php echo $rows['sizeofcontainer']; ?></option>
                    <?php
                    }
                    ?>
                    <option value="20">20'</option>
                    <option value="40">40'</option>
                  </select>
          </div>
          <div class="col-sm-3"  style="color:red;"> Applied dose rate(g/m<sup>3</sup>) :</div>
          <div class="col-sm-3">
            <input type="number" data-role="number" data-name="fumdosage" class="form-control" value="<?php echo $rows['fumdosage']; ?>" style="border-color:red;">
          </div>
        <?php
          }elseif($performIn=='container said to be loaded' || $performIn=="warehouse"){
        ?>
          <div class="col-sm-3" style="color:red;">Quntity in MT</div>
          <div class="col-sm-3">
              <input type="number" data-role="number" data-name="qty" class="form-control" value="<?php echo $rows['qty'];?>" maxlength="10" style="border-color:red;">
          </div>
          <div class="col-sm-3" style="color:red;">Commodity : </div>
          <div class="col-sm-3">
            <select data-name="mbrcommodity" data-role="select" id="mbrcommodity" class="form-control select-js1" style="border-color:red;">
              <option value="Select">Select</option>
              <?php
                    if($rows['mbrcommodity']){
                      ?>
                      <option value="<?php echo $rows['mbrcommodity']; ?>" selected><?php echo $rows['mbrcommodity']; ?></option>
                    <?php
                    }
                    ?>
              <?php
              $commodity=mysqli_query($con,"SELECT commodityname FROM `commoditymaster`");
              while($rowss = mysqli_fetch_assoc($commodity)){
                echo '<option value="'.$rowss['commodityname'].'">'.$rowss['commodityname'].'</option>';
              }
              ?>
            </select>
          </div>
          <div class="col-sm-3" style="color:red;"> Applied dose rate(g/m<sup>3</sup>) :</div>
          <div class="col-sm-3">
            <input type="number" data-role="number" data-name="fumdosage" class="form-control" value="<?php echo $rows['fumdosage']; ?>" style="border-color:red;">
          </div>
        <?php
        }elseif($performIn=="vessel"){
        ?>
       <div class="col-sm-3" style="color:red;">Volume of vessal hold in m<sup>3</sup></div>
       <div class="col-sm-3">
          <input type="number" data-name="qty" data-role="number" class="form-control" value="<?php echo $rows['qty'];?>" maxlength="10" style="border-color:red;">
       </div>
       <div class="col-sm-3" style="color:red;"> Applied dose rate(g/m<sup>3</sup>) :</div>
          <div class="col-sm-3">
            <input type="number" data-role="number" data-name="fumdosage" class="form-control" value="<?php echo $rows['fumdosage']; ?>" style="border-color:red;">
      </div>
        <?php
        }elseif($performIn=="other"){
        ?>
         <div class="col-sm-3" style="color:red;">Volume m<sup>3</sup></div>
                <div class="col-sm-3">
                    <input type="number" data-role="number" data-name="qty" class="form-control" value="<?php echo $rows['qty'];?>" maxlength="10" style="border-color:red;">
                </div>
                <div class="col-sm-3">Where to Performe</div>
                  <div class="col-sm-3">
                      <input type="text" data-role="text" data-name="mbrcommodity" class="form-control" value="<?php echo $rows['mbrcommodity']; ?>">
                  </div>
      <div class="col-sm-3" style="color:red;"> Applied dose rate(g/m<sup>3</sup>) :</div>
          <div class="col-sm-3">
            <input type="number" data-role="number" data-name="fumdosage" class="form-control" value="<?php echo $rows['fumdosage']; ?>" style="border-color:red;">
      </div>
        <?php
          }else{
        ?>
        <div class="col-sm-2" style="color:red;">Quantity In No:</div>
        <div class="col-sm-4">
            <input type="number" data-name="qty" class="form-control" value="<?php echo $rows['qty']; ?>"  maxlength="28" style="border-color:red;">
        </div>
        <div class="col-sm-3"> Applied dose rate(g/m<sup>3</sup>) :</div>
        <div class="col-sm-3">
          <input type="number" data-role="number" data-name="fumdosage" class="form-control" value="<?php echo $rows['fumdosage']; ?>">
        </div>
        <?php
         }
        ?>
      </div>
  </div>
<br>
 <hr style="border:1px solid #000;">
 <div class="col-sm-12">
    <div class="col-sm-6"> Name and Address of exporter : 
        <input type="text" data-name="exporter1" class="form-control" value="<?php echo $rows['exporter1']; ?>" maxlength="55"> 
        <input type="text" data-name="exporter2" class="form-control" value="<?php echo $rows['exporter2']; ?>" maxlength="55"> 
        <input type="text" data-name="exporter3" class="form-control" placeholder="Enter county of Export"  value="<?php echo $rows['exporter3']; ?>" maxlength="45"> 
        <input type="text" data-name="exporter4" class="form-control" value="<?php echo $rows['exporter4']; ?>" maxlength="55"> 
    </div>
    <div class="col-sm-6"> Name and Address of importer :
        <input type="text" data-name="consignee1" class="form-control" value="<?php echo $rows['consignee1']; ?>" maxlength="55"> 
        <input type="text" data-name="consignee2" class="form-control" value="<?php echo $rows['consignee2']; ?>" maxlength="55"> 
        <input type="text" data-name="consignee3" class="form-control" value="<?php echo $rows['consignee3']; ?>" maxlength="55"> 
        <input type="text" data-name="consignee4" class="form-control" value="<?php echo $rows['consignee4']; ?>" maxlength="55"> 
      </div>
</div>
 <hr style="border:1px solid #000;">
<div class="col-sm-12" align="center"><b>TREATMENT DETAILS</b></div>

<div class="col-sm-12">
<div class="col-sm-3"> Date of fumigation completed : </div><div class="col-sm-3"><input type="date" data-name="fumdate1" class="form-control" value="<?php echo $rows['fumdate1']; ?>"></div>
<div class="col-sm-3"> Place of fumigation : </div><div class="col-sm-3"><input type="text" data-name="fumplace" class="form-control" value="<?php echo $rows['fumplace']; ?>"></div>
</div>


<div class="col-sm-12">
<div class="col-sm-3"> DAFF Prescribed dose rate(g/m<sup>3</sup>) : </div>
<div class="col-sm-3">
<input type="number" data-name="daffdosage" class="form-control" value="<?php echo $rows['daffdosage']; ?>">
</div>
<div class="col-sm-3"> Exposure Period(hrs) : </div>
<div class="col-sm-3">
<input type="number" data-name="fumdurationhrs" class="form-control" value="<?php echo $rows['fumdurationhrs']; ?>">
</div>
</div>



<div class="col-sm-12">
<div class="col-sm-3">Forecast minimum temp(&#8451;):</div>
  <div class="col-sm-3">
    <input type="number" data-role="number" id="mintemp" data-name="mintemp" class="form-control" value="<?php echo $rows['mintemp']; ?>">
  </div>
<div class="col-sm-3" style="color:red;">Date of Fumigation</div>
    <div class="col-sm-3"> 
      <input type="date" data-role="date" data-name="fumdate" id="fumdate" class="form-control" value="<?php echo $rows['fumdate']; ?>" style="border-color:red;">
    </div>
</div>

<BR>
<BR>
<hr style="border:1px solid #000;">

<div class="col-sm-12">
<div class="col-sm-4"> How was the fumigation conducted ?   </div>
<div class="col-sm-8">
<input type="radio" data-name="fumconducted" name="fumconducted" value="Un-sheeted Container" <?php echo $unchecked; ?>>Un-sheeted Container &nbsp;
<input type="radio" data-name="fumconducted" name="fumconducted" value="Sheeted Container" <?php echo $shhecked; ?>>Sheeted Container/s &nbsp;
<input type="radio" data-name="fumconducted" name="fumconducted" value="Chember" <?php echo $chchecked; ?>>Chember &nbsp;<br>
<input type="radio" data-name="fumconducted" name="fumconducted" value="Pressure Tested Container" <?php echo $prchecked; ?>>Pressure Tested Container &nbsp;
<input type="radio" data-name="fumconducted" name="fumconducted" value="Sheeted Stack" <?php echo $sschecked; ?> >Sheeted Stack &nbsp;


</div>
</div>
<div class="col-sm-12">
<div class="col-sm-4">Container Number/s(Where applicable):</div><div class="col-sm-8"><input type="text" data-name="containerno1" class="form-control" value="<?php echo $rows['containerno1']; ?>" maxlength="60"></div>
 
</div>

<hr style="border:1px solid #000;">

<div class="col-sm-12">
<div class="col-sm-6"> Does the target of the fumigation conform to the DAFF plastic wrapping, Impervious surface and timber thickness requirements at the time of fumgation?   </div>
<div class="col-sm-6">
<input type="radio" data-name="targetfum" name="targetfum" value="YES" <?php echo $targetyeschecked; ?>>YES &nbsp;
<input type="radio" data-name="targetfum" name="targetfum" value="NO" <?php echo $targetnochecked; ?>>NO &nbsp;
 

</div>
</div>

<hr style="border:1px solid #000;">
<div class="col-sm-12">
<div class="col-sm-4"><b>Ventilation:</b> FINAL TLV READING (PPM)</div>
<div class="col-sm-2">
    <input type="text" data-name="ventilation" class="form-control" value="<?php echo $rows['ventilation']; ?>" maxlength="90">
</div>
<div class="col-sm-6">(Not required for stack or pernanent chamber fumigation)</div>
</div>



<hr style="border:1px solid #000;">
<div class="col-sm-12" align="center"><b>DECLARATION</b></div>
By signing below, I, the AFAS accredited fumigator responsible, declare the these details are true and correct and the Fumigation has been carried out in accordance with all the requirements in the DAFF Methyl Bromide Fumigation Standard.
<hr style="border:1px solid #000;">
 <div class="col-sm-12">
<div class="col-sm-4"><b> Additional Declaration : </b></div><div class="col-sm-8">


    <input type="text" data-name="additional" class="form-control" value="<?php echo $rows['additional']; ?>"maxlength="120"> 
    <input type="text" data-name="additional1" class="form-control" value="<?php echo $rows['additional1']; ?>" maxlength="120"> 
    <input type="text" data-name="additional2" class="form-control" value="<?php echo $rows['additional2']; ?>" maxlength="120"> 
</div>
</div> 
 

<BR><BR>
<button class="btn btn-lg btn-primary" onclick="savecertificate();" style="border-radius:0px;position:fixed;bottom:10px;right:10px;">Save Certificate</button>

 
<input type="hidden" data-name="certid" value="<?php echo $certid; ?>">
<input type="hidden" data-name="enquiryid" value="<?php echo $enqno1; ?>">

</div>
<?php
include('../footer.php');
?>
<style type="text/css">
  .picker__select--year{
        height:auto;
    }
    .picker__select--month{
        height:auto;
    }

</style>
<script>
$("#fumdate").on('change',function(){
  var date = $(this).val();
  var branchid = $("#branchid").val()
  $.ajax({
			type: "POST",
			data: 'date='+date + '&branchid='+branchid,
			url: '../api/selecttemp.php',
			cache: false,
			success: function (res) {
          if(res.status=='success'){
            var json=JSON.stringify(res.json)
           $("#mintemp").val(res.temperature);
          }
	        }
        });
})
$('.datepicker').pickadate({     
    selectYears: 100,
  selectMonths: true,
  onClose: function () {
    try {
      var dateStamp = this.get('select')['pick'];
      var id = this.get('id');
      $('#' + id).attr('data-stamp', dateStamp);
      $('#' + id).parent().find('.timepicker').click();
    } catch (err) {
      console.log(err);
    }
  }
});
$('body').css('background','#eee');
function funtb(e){
    var arr=[];
    $('.tds').each(function (){
        var serv=$(this).find('.serv').text().trim();
        var yesno=$(this).find('.yesno').val();
        var frq=$(this).find('.frq').val();
        if(yesno!='NO' && frq!='Nil'){
            arr.push({
                "serv":serv,
                "yesno":yesno,
                "frq":frq
            });
            var str='<table class="table-list table">';
            str+='<thead>';
            str+='<tr>';
            str+='<th>Service</th>'; 
            str+='<th>Freq.</th>';
            str+='<th>Start Date</th>';
            str+='</tr>';
            str+='</thead>';  
            str+='<tbody>'; 
            str+='<tr>';
            str+='<td>';
            for(var i in arr){
            str+=arr[i].serv+'<br>';
            }
            str+='</td>';
            str+='<td><input type="number" onkeyup="coter(this)"></td>';
            str+='<td id="coter">';

            str+='</td>';
            str+='</tr>';
            str+='</tbody>'; 
            str+='</table>'; 
            $('#table-ui').html(str);
        }

    })
}
function coter(e){
var cot=$(e).val().trim();
if(cot!=''){
cot=parseInt(cot);
var str='<table class="table">';
str+='<thead>';
str+='<tr>';
str+='<th>Sr. No.</th>';
str+='<th>Service Date</th>';
str+='</tr>';
str+='</thead>';
str+='<tbody>';
for(var i=1;i<=cot;i++){
    str+='<tr>';
    str+='<td>'+i+'</td>';
    str+='<td><input type="date"></td>';
    str+='</tr>';
}
str+='</tbody>';
str+='</table>';

$('#coter').html(str);
}
}
$("#mintemp").change(function(){
  var temp=$("#mintemp").val();
  if(temp<21){
    confirm("Temperature bellow 21 degree,Have you incresed dosage?")
  }
});
function savecertificate() {
  //$('#newqneuiry').hide();
  if($("#mbrcommodity").val()==""){
    $("#mbrcommodity").val("Select");
  }
  if($("#importExport").val()==""){
    $("#importExport").val("Select");
  }
  if($("#sizeofcontainer").val()==""){
    $("#sizeofcontainer").val("Select");
  }
  if($("#noofcontainer").val()==""){
    $("#noofcontainer").val("");
  }
  var valid = true;
  if (checker('alpcert') != false) {
    valid = valid * true;
  } else {
    valid = valid * false;
  }
  if (valid) {
    var data=checker('alpcert');
    var datastr = JSON.stringify(data);
    var copy=$('#copy').val();
    var confirmedit = $('#confirmedit').val();
    var certType=$('#certType').val();
    var performIn=$('#performIn').val();
    var reEdit=$("#reEdit").val();
    var edit = $('#edit').val();
    var create = $('#create').val();
    var isdraft=$("#isdraft").val();

    $.ajax({
        type: "POST",
        data: {
          data: datastr,
          copy:copy,
          confirmedit:confirmedit,
          edit:edit,
          reEdit:reEdit,
          isdraft:isdraft,
          create:create,
          performIn:performIn,
          data1: certType
        },
        url: '../api/savecertificate.php',
        cache: false,
        success: function (res) {
          if (res.status == 'success') {
            if(confirmedit){
            window.location = "../materialissue.php";
          }
          if(reEdit){
            window.location = "../materialissue.php";
          }
          if(!reEdit && !confirmedit){
            window.location = "../certificate.php?certPage=AFAS";
          }
          }
          if(res.status == 'Duplication'){
            alert('Certificate '+res.status+' : Please Change Certificate No.');
            $("#certificateno").css("border-color", "red");
        }
        if(res.status == 'EmptybranchOrEmp'){
            //if branch id or employeeId is empty
            window.location = "../../../index.php";
        }
        }
      })
    
  }
  }
  
</script>