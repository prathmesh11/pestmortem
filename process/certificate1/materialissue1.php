<?php 

    $base='../../';
    $navenq5='background:#1B1464;';
    include('header.php');
    if (session_status() == PHP_SESSION_NONE) { session_start(); }
    $sessionby = $_SESSION['employeeid'];
    $branchid = $_SESSION['branchid'];
    $country=$_GET['country'];
    $exporter1=addslashes($_GET['exporter1']);
    $consignee1=addslashes($_GET['consignee1']);

    $certificateid=$_GET['certificateid'];
    if($country=='Select'){$country='';}
    if($exporter1=='Select'){$exporter1='';}
    if($consignee1=='Select'){$consignee1='';}
    if($certificateid=='Select'){$certificateid='';}
?>
<style>
    .table-list td,
    .table-list th {
        border: 1px solid #ddd;
        padding: 1px !important;
        font-size: 13px;
    }
    .table-list td {
        padding-top: 10px !important;
    }
    .table-list tr:nth-child(even) {
        background-color: #f2f2f2;
    }
    .table-list th {
        padding-top: 5px;
        padding-bottom: 5px;
        text-align: center;
        background-color: #16a085;
        color: white;
    }
    h2{
        margin-left:500px;
    }
    th {
    background: #8e44ad;
    color: #fff;
    text-align: center;
    padding-top: 2px;
    padding-bottom: 2px;
    border: 1px solid #fff;
    }



    
</style>
<input type="hidden" id="inp-branch" value='<?php echo $branchid; ?>' />

<div class="row" style="padding-top:10px;">
    <div class="col-sm-2"></div>
    <div class="col-sm-2">
        <div class="form-group">
            <select class="form-control input-md select-js1" data-role="select" data-name="country" id="country">
                <option value="Select">Select Country</option>
                <?php

                              $result1=mysqli_query($con, "SELECT DISTINCT country FROM `certificatemaster` WHERE  branchid='$branchid' AND confirm ='Y' AND calculated<>'Y' ORDER BY confirmtime DESC");
                            while($rows=mysqli_fetch_assoc($result1)) {
                                echo '<option value="'.$rows['country'].'">'.$rows['country'].'</option>';
                            }
                        ?>
            </select>
        </div>
    </div>
    <div class="col-sm-2">
        <div class="form-group">
            <select class="form-control input-md select-js1" data-role="select" data-name="exporter1" id="exporter1">
                <option value="Select">Select Name of Exporter</option>
                <?php
                          $result1=mysqli_query($con, "SELECT DISTINCT exporter1 FROM `certificatemaster` WHERE  branchid='$branchid' AND confirm ='Y' AND calculated<>'Y' ORDER BY confirmtime DESC");
                          while($rows=mysqli_fetch_assoc($result1)) {
                            echo '<option value="'.$rows['exporter1'].'">'.$rows['exporter1'].'</option>';
                        }
                    ?>
            </select>
        </div>
    </div>

    <div class="col-sm-2">
        <div class="form-group">
            <select class="form-control input-md select-js1" data-role="select" data-name="consignee1" id="consignee1">
                <option value="Select">Select Name of Importer</option>
                <?php
                          $result1=mysqli_query($con, "SELECT DISTINCT consignee1  FROM `certificatemaster` WHERE  branchid='$branchid' AND confirm ='Y' AND calculated<>'Y' ORDER BY confirmtime DESC");
                          while($rows=mysqli_fetch_assoc($result1)) {
                            echo '<option value="'.$rows['consignee1'].'">'.$rows['consignee1'].'</option>';
                        }
                    ?>
            </select>
        </div>
    </div>


    <div class="col-sm-2">
        <div class="form-group">
            <select class="form-control input-md select-js1" data-role="select" data-name="certificateid"
                id="certificateid">
                <option value="Select">Select Certificate Number</option>
                <?php 
                        
                          $result2=mysqli_query($con, "SELECT id, certificateno FROM `certificatemaster` WHERE   branchid='$branchid' AND confirm ='Y' AND calculated<>'Y' ORDER BY confirmtime DESC");
                        
                        while($rows=mysqli_fetch_assoc($result2)) {
                            echo '<option value="'.$rows['id'].'">'.$rows['certificateno'].'</option>';
                        }
                        ?>

            </select>
        </div>
    </div>
    <div class="col-sm-2">
        <a class="btn btn-primary btn-sm btn-block" onclick="submit()">Submit</a>
    </div>
</div>

<br>
<div class="table-ui container-fluid">
  <div class="tr row">
      <div class="col-sm-1 th">Sr. No.</div> 
      <div class="col-sm-2 th">Certificate No.</div>  
      <div class="col-sm-7 th">Summary</div>  
      <div class="col-sm-1 th">CertType</div> 
      <div class="col-sm-1 th">Action</div>
  </div>
<?php

    //All Blank
    if($country=='' && $exporter1=='' && $certificateid=='' && $consignee1 == ''){ //000

        $query= "SELECT * FROM `certificatemaster` WHERE branchid='$branchid' AND  confirm='Y' AND calculated<>'Y' AND created='Y' ";

    }

    //Country


    if($country!='' && $exporter1=='' && $certificateid=='' && $consignee1==''){

        $query= "SELECT * FROM `certificatemaster` WHERE branchid='$branchid' AND  confirm='Y' AND calculated<>'Y' AND created='Y' AND country='$country' ";

    }

    if($country!='' && $exporter1!='' && $certificateid=='' && $consignee1==''){
        //110
        $query= "SELECT * FROM `certificatemaster` WHERE branchid='$branchid' AND  confirm='Y' AND calculated<>'Y' AND created='Y' AND country='$country' AND exporter1 LIKE '%$exporter1%' ";

    }

    if($country!='' && $exporter1=='' && $certificateid!='' && $consignee1==''){
        //101
        $query= "SELECT * FROM `certificatemaster` WHERE branchid='$branchid' AND  confirm='Y' AND calculated<>'Y' AND created='Y' AND country='$country' AND id='$certificateid' ";

    }

    if($country!='' && $exporter1=='' && $certificateid=='' && $consignee1!=''){
        //110
        $query= "SELECT * FROM `certificatemaster` WHERE branchid='$branchid' AND  confirm='Y' AND calculated<>'Y' AND created='Y' AND country='$country' AND consignee1 Like '%$consignee1%' ";

    }

    if($country!='' && $exporter1!='' && $certificateid!='' && $consignee1==''){

        $query= "SELECT * FROM `certificatemaster` WHERE branchid='$branchid' AND  confirm='Y' AND calculated<>'Y' AND created='Y' AND country='$country' AND exporter1 LIKE  '%$exporter1%' AND id='$certificateid' ";

    }

    if($country!='' && $exporter1!='' && $certificateid=='' && $consignee1!=''){

        $query= "SELECT * FROM `certificatemaster` WHERE branchid='$branchid' AND  confirm='Y' AND calculated<>'Y' AND created='Y' AND country='$country' AND exporter1 LIKE  '%$exporter1%' AND consignee1 LIKE '%$consignee1%' ";
        
    }

    if($country!='' && $exporter1=='' && $certificateid!='' && $consignee1!=''){

        $query= "SELECT * FROM `certificatemaster` WHERE branchid='$branchid' AND  confirm='Y' AND calculated<>'Y' AND created='Y' AND country='$country' AND id='$certificateid' AND consignee1 LIKE '%$consignee1%' ";
        
    }

        //exporter1
    if($country=='' && $exporter1!='' && $certificateid=='' && $consignee1==''){

        $query= "SELECT * FROM `certificatemaster` WHERE branchid='$branchid' AND  confirm='Y' AND calculated<>'Y' AND created='Y' AND exporter1 LIKE '%$exporter1%' ";

    }
            

    if($country=='' && $exporter1!='' && $certificateid!='' && $consignee1==''){

        $query= "SELECT * FROM `certificatemaster` WHERE branchid='$branchid' AND  confirm='Y' AND calculated<>'Y' AND created='Y' AND exporter1 LIKE  '%$exporter1%' AND id='$certificateid' ";

    }

    if($country=='' && $exporter1!='' && $certificateid=='' && $consignee1!=''){

        $query= "SELECT * FROM `certificatemaster` WHERE branchid='$branchid' AND  confirm='Y' AND calculated<>'Y' AND created='Y' AND exporter1 LIKE  '%$exporter1%' AND consignee1 LIKE '%$consignee1%' ";

    }

    if($country=='' && $exporter1!='' && $certificateid!='' && $consignee1!=''){

        $query= "SELECT * FROM `certificatemaster` WHERE branchid='$branchid' AND  confirm='Y' AND calculated<>'Y' AND created='Y' AND exporter1 LIKE  '%$exporter1%' AND consignee1 LIKE '%$consignee1%' AND id='$certificateid' ";

    }

    //certificateid
    if($country=='' && $exporter1=='' && $certificateid!='' && $consignee1==''){

        $query= "SELECT * FROM `certificatemaster` WHERE branchid='$branchid' AND  confirm='Y' AND calculated<>'Y' AND created='Y' AND id='$certificateid' ";

    }

    if($country=='' && $exporter1=='' && $certificateid!='' && $consignee1!=''){

        $query= "SELECT * FROM `certificatemaster` WHERE branchid='$branchid' AND  confirm='Y' AND calculated<>'Y' AND created='Y' AND id='$certificateid' AND consignee1 LIKE '%$consignee1%' ";

    }

    //consignee1
    if($country=='' && $exporter1=='' && $certificateid=='' && $consignee1!=''){

        $query= "SELECT * FROM `certificatemaster` WHERE branchid='$branchid' AND  confirm='Y' AND calculated<>'Y' AND created='Y' AND consignee1 LIKE '%$consignee1%' ";

    }
    
    
   
  $query=$query." ORDER BY id DESC";
  //echo $query;
  $result = mysqli_query($con,$query);
//   while($rows = mysqli_fetch_assoc($result)){
//   $result = mysqli_query($con,"SELECT * FROM certificatemaster WHERE branchid='$branchid' AND confirm ='Y' AND calculated<>'Y' ORDER BY confirmtime DESC");
  while($rows = mysqli_fetch_assoc($result)){
      $enquiryids=$rows['enquiryid'];
      $enquiryids= explode(",", $enquiryids);
      $exporter=$rows['exporter1'];
      $importer=$rows['consignee'];
      $consignee=$rows['consignee1'];
      $country=$rows['country'];
      $portofdischarge=$rows['loading'];
      $applieddose=$rows['fumdosage'];
      $chemichalissued=$rows['chemichalissued'];
      
?>
<div class="row tr">
  <div class="col-sm-1 td text-center" style="word-wrap:break-word;"><?php echo $rows['id']; ?></div>
  <div class="col-sm-2 td text-center" style="word-wrap:break-word;">
    <?php if($rows['wpmcreated']=='Y'){ ?> 
        Certificate Numbers
        <?php
              $wpmmainid=$rows['id'];
              $query="SELECT certificateno FROM `certificatemaster` WHERE wpnrefno='$wpmmainid'";
              $result2 = mysqli_query($con,$query);
              while($rowss = mysqli_fetch_assoc($result2)){
                echo '<br>';
                echo $rowss['certificateno'];
              }
              ?>
    <?php } else{ ?>
        <p>Certificate No.</p>
        <?php echo $rows['certificateno']; ?>
        <BR><br>
        <p>Sr. No.<?php echo $rows['srNo']; ?></p>
    <?php } ?>
  </div>
  <div class="col-sm-7 td">
  <?php if($rows['created'] == "Y") { ?>
        Name of Exporter: <?php echo $exporter; ?><BR>
  <?php if($rows['certType']!="AFAS"){ ?>
        Name of Consignee: <?php echo $consignee; ?> <BR>
  <?php } else{ ?>
    
        Name of Importer: <?php echo $consignee; ?> <BR>
  <?php } ?>
  <?php if($rows['certType']!="AFAS"){ ?>
        Country: <?php echo $country; ?><BR>
  <?php } ?>
        <?php if($rows['wpninserted']!='Y'){ ?>
        Port Of Discharge: <?php echo $portofdischarge; ?><BR>
        Applied Dosage : <?php echo $applieddose; ?> <BR>
        Date Of Issue: <?php echo date("d-m-Y", strtotime($rows['issuedate'])); ?><BR>
        <?php } ?>
        <?php
       if($rows['certType']=="MBR" || $rows['certType']=="AFAS"){
       ?>
       Performed In : <?php echo $rows['performIn']; ?> 
       <?php
       }elseif($rows['certType']=="ALP"){
       ?>
       Using : <?php echo $rows['performIn']; ?> 
       <?php } ?>
       <br>
       <?php if($rows['wpninserted']=='Y'){ ?>
            Number of Wooden Packing Material : <?php echo $rows['totalwpm']; ?><br>
            WPM commodities : <?php echo $rows['mbrcommodity']; ?><br>
            Date of Fumigation : <?php echo $rows['fumdate']; ?><br>
            Place of Fumigation : <?php echo $rows['fumplace']; ?><br>
            WPM refered to : <?php echo $rows['wpnrefno']; ?><br>
            <?php } ?>
        <?php  }else{ ?>
            Performed In : <?php echo $rows['performIn']; ?><br>
            Number of Wooden Packing Material : <?php echo $rows['totalwpm']; ?><br>
            Date of Fumigation : <?php echo $rows['fumdate']; ?><br>
            Place of Fumigation : <?php echo $rows['fumplace']; ?><br>
            <?php if($rows['mbrcommodity']!=''){ ?>
                WPM commodities : <?php echo $rows['mbrcommodity']; ?><br>
            <?php } ?>
        
    <?php } ?>
</div>

<div class="col-sm-1 td text-center"><?php echo $rows['certType']; ?></div>
<div class="col-sm-1 td">
        <?php if($rows['created'] == "Y" ){ ?>
            <a class="btn btn-sm btn-block btn-primary"  onclick="printer_<?php echo $rows['certType']; ?>(<?php echo $rows['id']; ?>,<?php echo $rows['branchid'];?>)" >View PDF</a>
        <?php } ?>
    <?php 
    if($rows['certType']=="ALP"){ ?>
        <p align="center">
            <a class="btn btn-sm  btn-block btn-warning" href="certificate/alpcertificate.php?id=<?php echo $rows['id']; ?>&type=<?php echo $rows['certType'];?>&performIn=<?php echo $rows['performIn']; ?>&reEdit=true" >Cert Edit </a>
            <a class="btn btn-sm  btn-block btn-warning" href="certificate/alpcertificate.php?id=<?php echo $rows['id'];  ?>&type=<?php echo $rows['certType']; ?>&performIn=<?php echo $rows['performIn']; ?>&copy=true" > Copy Cert </a>
        <?php 
        if($rows['chemichalissued']!=""){?>
                <a class="btn btn-sm  btn-block btn-danger" href="alpcalculator.php?id=<?php echo $rows['id'];  ?>&type=<?php echo $rows['certType']; ?>&recal=true" >Re Calulate </a>
        <?php }else{ ?>
                <a class="btn btn-sm  btn-block btn-info" href="alpcalculator.php?id=<?php echo $rows['id'];  ?>&type=<?php echo $rows['certType']; ?>" > Calulation </a>
        <?php } ?>
        </p>
    <?php }elseif($rows['certType']=="MBR") { ?>
       
        <?php if($rows['wpninserted']=='Y'){ ?>
            <a class="btn btn-sm  btn-block btn-warning" href="certificate/mbrcertificate.php?id=<?php echo $rows['id']; ?>&type=<?php echo $rows['certType']; ?>&performIn=<?php echo $rows['performIn']; ?>&reEdit=true" >Cert Edit </a>
        <?php }elseif($rows['wpmcreated']=='Y'){ 
            if($rows['mbrcommodity']==''){ ?>
                <button class="btn btn-sm  btn-block btn-primary" onclick="addcommodities(<?php echo $rows['id'];?>,<?php echo $rows['totalwpm'];?>)" >Commodities</button>
            <?php }else{ ?>
                    <?php if($rows['chemichalissued']!=""){ ?>
                        <p align="center">
                        <button class="btn btn-sm  btn-block btn-primary" onclick="editcommodities(<?php echo $rows['id'];?>,<?php echo $rows['totalwpm'];?>)" >Edit Comm.</button>
                        </p>
                        <p align="center">
                            <a class="btn btn-sm  btn-block btn-danger" href="mbrcalculator.php?id=<?php echo $rows['id'];  ?>&type=<?php echo $rows['certType']; ?>&recal=true" >Re Calulate </a>
                        </p>
                <?php }else{ ?>
                        <p align="center">
                            <button class="btn btn-sm  btn-block btn-primary" onclick="editcommodities(<?php echo $rows['id'];?>,<?php echo $rows['totalwpm'];?>)" >Edit Comm.</button>
                        </p>
                        <p align="center">
                            <a class="btn btn-sm  btn-block btn-info" href="mbrcalculator.php?id=<?php echo $rows['id'];  ?>&type=<?php echo $rows['certType']; ?>" > Calulation </a>
                        </p>
                <?php } ?>
            <?php } ?>
            
        <?php }else{ ?>
            <a class="btn btn-sm  btn-block btn-warning" href="certificate/mbrcertificate.php?id=<?php echo $rows['id']; ?>&type=<?php echo $rows['certType']; ?>&performIn=<?php echo $rows['performIn']; ?>&reEdit=true" >Cert Edit </a>
            <a class="btn btn-sm  btn-block btn-warning" href="certificate/mbrcertificate.php?id=<?php echo $rows['id'];  ?>&type=<?php echo $rows['certType']; ?>&performIn=<?php echo $rows['performIn']; ?>&copy=true" > Copy Cert </a>
            <?php if($rows['chemichalissued']!=""){ ?>
            <p align="center">
                <a class="btn btn-sm  btn-block btn-danger" href="mbrcalculator.php?id=<?php echo $rows['id'];  ?>&type=<?php echo $rows['certType']; ?>&recal=true" >Re Calulate </a>
            </p>
            <?php }else{ ?>
                <p align="center">
                    <a class="btn btn-sm  btn-block btn-info" href="mbrcalculator.php?id=<?php echo $rows['id'];  ?>&type=<?php echo $rows['certType']; ?>" > Calulation </a>
                </p>
            <?php } ?>
        <?php } ?>
    <?php }elseif($rows['certType']=="AFAS") { ?>
        <p align="center">
            <a class="btn btn-sm  btn-block btn-warning" href="certificate/afascertificate.php?id=<?php echo $rows['id']; ?>&type=<?php echo $rows['certType']; ?>&performIn=<?php echo $rows['performIn']; ?>&reEdit=true" >Cert Edit </a>
            <a class="btn btn-sm  btn-block btn-warning" href="certificate/afascertificate.php?id=<?php echo $rows['id'];  ?>&type=<?php echo $rows['certType']; ?>&performIn=<?php echo $rows['performIn']; ?>&copy=true" > Copy Cert </a>
        <?php 
        if($rows['chemichalissued']!=""){?>
                <a class="btn btn-sm  btn-block btn-danger" href="mbrcalculator.php?id=<?php echo $rows['id'];  ?>&type=<?php echo $rows['certType']; ?>&recal=true" >Re Calulate </a>
        <?php }else{ ?>
                <a class="btn btn-sm  btn-block btn-info" href="mbrcalculator.php?id=<?php echo $rows['id'];  ?>&type=<?php echo $rows['certType']; ?>" > Calulation </a>
           
        <?php } ?>
        </p>
    <?php } ?>

    <?php 
        if($rows['chemichalissued']!="" || $rows['wpninserted']=='Y'){?>
            <button class="btn btn-success  btn-block btn-sm"  data-id="<?php echo $rows['id']; ?>" data-enquiryid="<?php echo $rows['enquiryid']; ?>" onclick="confirmcertificate2(this)">Confirm</button>
    <?php } ?>
    
</div>
</div>
<?php } ?>
</div>
</div>
</div>
<div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <input type="text" data-role="text" data-name="id" id="rowid" class="hidden"> 
                <h5 class="modal-title"><b>Total Quntity : </b>
                    <input type="number" data-name="inp-qty" id="inp-qty"  style="border: none" readonly>
                </h5>
            </div>
                <div class="modal-body">
                <div class="row">
                <input class="hidden" value="" id="purchaseid" >
                <table class="table table-list" id="table">
                    <thead>
                        <tr>
                            <th>Commodity Name</th>
                            <th>Quantity</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
                </div>
                <div>
                <button type="button" class="btn btn-primary btn-block" onclick="addbatch()">Add Commodity</button>
                </div>
            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-primary btn-block btn-success" onclick="insertcommodities()">Submit</button>
            </div>
        </div>
        
        </div>
    </div>
<?php 
    include('footer.php');
?>        
<script>

$(document).ready(function(){
      $('.select-js1').select2({width: '100%'});
      $('.select').attr('style','width:100%!important;');
    });

function submit(){
      var country       = $('#country').val();
      var exporter1     = $('#exporter1').val();
      var consignee1    = $('#consignee1').val();
      var certificateid = $('#certificateid').val();
      window.location   = "materialissue.php?country="+country+"&exporter1="+exporter1+"&consignee1="+consignee1+"&certificateid="+certificateid;
    }



function editcommodities(id,totalwpm){
    $("#inp-qty").val(totalwpm);
    $("#rowid").val(id);
    var comjson='';
    if(id){
        $.ajax({
            type: "POST",
            data: 'id=' + id,
            url: 'api/getcomjson.php',
            cache: false,
            success: function (res) {
                 if (res.status == 'success') {
                     comjson=JSON.parse(res.json.wpmCommodityJson);
                     $("#table tbody").empty();
                     comjson.forEach(myFunction);
                    function myFunction(item, index, arr) {
                        var commodityname='"'+item.commodityname+'"';
                        $("#table tbody").append('<tr class="batchno"><td align="center"><input type="text"class="clsbatch" value='+commodityname+'></td><td align="center"><input type="number" class="clsqty" value='+item.quntity+' onkeyup="batchentry(this)"></td><td><button onclick="remover(this)" class="btn-danger btn-sm"  align="center" style="padding: 1px 10px">Remove</button></td></tr>'
                            )
                            $('#myModal').modal();
                        }
                    }
                        // $.each(comjson, function() {
                        //     var key = Object.keys(this);
                        //     var value = this[key];
                        //     $("#table tbody").append('<tr class="batchno"><td align="center"><input type="text"class="clsbatch" value='+this.commodityname+'></td><td align="center"><input type="number" class="clsqty" value='+this.quntity+' onkeyup="batchentry(this)"></td><td><button onclick="remover(this)" class="btn-danger btn-sm"  align="center" style="padding: 1px 10px">Remove</button></td></tr>'
                        //     )
                        //     $('#myModal').modal();
                        // });
                //}
            }
        })
    }
    
    
}
function addcommodities(id,totalwpm){
    $("#inp-qty").val(totalwpm);
    $("#rowid").val(id);
    $('#myModal').modal();  
}
function addbatch(){
    $("#table tbody").append('<tr class="batchno"><td align="center"><input type="text"class="clsbatch"></td><td align="center"><input type="number" class="clsqty"  onkeyup="batchentry(this)"></td><td><button onclick="remover(this)" class="btn-danger btn-sm"  align="center" style="padding: 1px 10px">Remove</button></td></tr>'
    )
    $(".btn-remove").on('click',function(){
    $(this).parent().parent().remove();
})
}
function batchentry(e){
    var qty1=0;
    $('.clsqty').each(function(){
        var qty100=parseFloat($(this).val());
        qty1 = qty1 + qty100;
    })
    var qty=parseFloat($("#inp-qty").val());
    if(qty1>qty){
        alert('Quntity must be less than :'+qty);
        $(e).val(0);
    }
}
function insertcommodities(){
    var arr=[];
    var id=$("#rowid").val();
    var valid=true;
    var str='';
    var str2='';
    var totalqty=0;
    var totalwpm=parseFloat($("#inp-qty").val());
    $("#table tbody tr").each(function(){
        if ($(this).find('td input.clsbatch').val() != '' && $(this).find('td input.clsqty').val() != '0') {
            var commodityname=$(this).find('td input.clsbatch').val();
            var qty=$(this).find('td input.clsqty').val();
            str2+=',{"commodityname":"'+commodityname+'","quntity":"'+qty+'"}';
            str+=', '+commodityname+' : '+qty;
            totalqty+=parseFloat(qty);
            totalqty=parseFloat(totalqty);
        } else {
            alert("Something went wrong!")
            valid = valid * false;
        }
    })
    if(totalqty>totalwpm){
        alert("Quntity Must be : "+totalwpm);
        valid = valid * false;
    }else if(totalqty<totalwpm){
        console.log(totalqty,totalwpm)
        alert("Quntity Must be : "+totalwpm);
        valid = valid * false;
    }else{
        valid = valid * true;
    }
    str2='['+str2.substring(1, str2.length)+']'; 
    console.log(str2)
    str=str.substring(1, str.length);
    if(str2!='' && str!=''){
        valid=valid*true;
    }else{
        valid=valid*false;
    }
    if(valid){
        $.ajax({
            type: "POST",
            data: {
                    id:id,
                    strcommodities:str,
                    commodities:str2
                },
            url: 'api/insertcommodities.php',
            cache: false,
            success: function (res) {
                if (res.status == 'success') {
                    $('#myModal').modal('hide');
                    location.reload();
                }
            }
        })
   }
    
   
}

function confirmcertificate2(e) {
    if (confirm("Are You Sure TO Confirm Certification?")) {
        var id = $(e).data('id');
        var enquiryid = $(e).data('enquiryid');
        $.ajax({
            type: "POST",
            data: 'enquiryid=' + enquiryid + '&id=' + id ,
            url: 'api/confirmcertificate2.php',
            cache: false,
            success: function (res) {
                 if (res.status == 'success') {
                    window.location = "materialissue.php";
                }
            }
        })
    }
}

function printer_ALP(certid,branchid) {
   var jsondata = $.parseJSON($.ajax({
         type: "POST",
        data: {
          certid: certid
        },
        url: 'api/addjson.php',
        cache: false,
        success: function (res) {
        //jsondata =JSON.stringify(res.json);
         },
        dataType: "json", 
        async: false
    }).responseText);
    jsondata =JSON.stringify(jsondata);
    data1 = JSON.parse(jsondata);  
    json1 = data1.json;   

    var brjsondata = $.parseJSON($.ajax({
            type: "POST",
            data: {
              certid: certid,branchid:branchid
            },
            url: 'api/getbrjson.php',
            cache: false,
            success: function (res) {
            //jsondata =JSON.stringify(res.json);
            },
            dataType: "json", 
            async: false
        }).responseText);
    brjsondata =JSON.stringify(brjsondata);
    brdata = JSON.parse(brjsondata);  
    brjson = brdata.branchjson;   


    //alert(data1.status);
    var fumdate = new Date(json1.fumdate);
    var issuedate = new Date(json1.issuedate);

    fumdate = ('0' + fumdate.getDate()).slice(-2) + "/" + ('0' + (fumdate.getMonth()+1)).slice(-2) + "/" + fumdate.getFullYear();
    issuedate = ('0' + issuedate.getDate()).slice(-2)+ "/" +  ('0' + (issuedate.getMonth()+1)).slice(-2) + "/" + issuedate.getFullYear();

    var fullDate = new Date();
    var twoDigitMonth = ((fullDate.getMonth().length + 1) === 1) ? (fullDate.getMonth() + 1) : '' + (fullDate.getMonth() + 1);
    var currentDate = fullDate.getDate() + "/" + twoDigitMonth + "/" + fullDate.getFullYear();
    var content=[]; 

    certno = 'PMI/'+brjson.branchshortname+'/ALP/'+json1.srno+'/'+fullDate.getFullYear().toString().substr(2,2)+'-'+((fullDate.getFullYear()+1).toString().substr(2,2));
                    dd = {
                    pageSize: 'A4',
                    pageMargins: [40, 30, 30, 40],
                    footer: function (currentPage, pageCount) {
                        return {
                        };

                       },
    content: [
	    '\n',
            {
                text: 'PEST MORTEM (INDIA) PRIVATE LIMITED',fontSize: 14,bold: true,alignment: 'center',margin:[0,5,0,0]
            },
            {
                text: '(Approved by National Plant Protection Organization, Government of India)',
                 fontSize: 8, bold: true, alignment: 'center',
            },
            {
                text: 'AN ISO 9001:2008 CERTIFIED COMPANY',
                 fontSize: 8,alignment: 'center',margin:[0,0,0,10]
            },
            
	        {     margin:[0,-4,0,0],
                table:{
                widths:[510],
                body:[
                    [{margin:[-5,-3,-5,-3],
                      border:[0,0,0,0],
    	                table:{
    	                widths:["*"],
    	                body:[
    	                   [{text: 'FUMIGATION CERTIFICATE',fontSize: 12,bold: true,alignment: 'center',margin:[0,0,0,0],border:[0,0,0,0],},]
    	                    ]
        	            },
	            },
	            ],
    	        [{
    	            margin:[-5,-3,-5,-3],
    	            table:{
    	                widths:['auto',10,'auto','*'],
    	                body:[
        	            [
        	            {stack: [{text: [
                        {text:'PEST MORTEM (INDIA) PRIVATE LIMITED\n',style: 'left',bold:1},
                        {text:brjson.address+'\n',style:'left',bold:1},
                        {text:'Dte. PPQS Regd. No.\t030/ALP/DATED : 20.04.2012  ',style:'left',bold:1},
                        
                        ],}],border:[0,0,0,0]
        	            },
    	                {
    	                stack: [{text: [
                        {text:'\n',style:'left'},
                        {text:'\n',style:'left'},
                        {text:'\n',style:'left'},
                        ],}],border:[0,0,1,0]   
    	                },
    	                {
    	                stack: [{text: [
                        {text:'Treatment Cert. No. : \n',style: 'left',bold:1},
                        {text:'\n',style:'left'},
                        {text:'Date of Issue :\n',style: 'left',bold:1}
                        ],}],border:[0,0,0,0]   
    	                },
    	                {
    	                stack: [{text: [
                        {text:certno+'\n',style: 'left',bold:1},
                        {text:'\n',style:'left'},
                        {text:issuedate+'\n',style: 'left',bold:1}
                        ],}],border:[0,0,0,0]   
    	                },
    	                ]
    	                    ]
    	            },layout: {
        				hLineColor: function (i, node) {
        					return (i === 0 || i === node.table.body.length) ? '#778899' : '#778899';
        				},
        				vLineColor: function (i, node) {
        					return (i === 0 || i === node.table.widths.length) ? '#778899' : '#778899';
        				},
        	        }
    	        }],
    	        [
    	           {
    	                stack: [{text: [
    	                    {text:'\tThis is to certify that the goods described below were treated in accordance with the fumigation treatment requirements of importing country ',fontSize: 8,italics: true},
                            {text: json1.country.toUpperCase(), italics: false, fontSize: 9,bold:1},
                            {text:' and declared that the consignment has been verified free of impervious surfaces/layers such as plastic wrapping or laminated plastic films, lacquered or painted surfaces, aluminium foil, tarred or waxed papers etc. that may adversely effect the penetration of the fumigant, prior to fumigation.',fontSize: 8,italics: true},
                            {text:'\nThe Certificate is valid for the consignments shipped within 21 days from the date of completion of fumigation',style: '',color:'',fontSize: 8,},
                        ],}],border:[1,1,1,1]   
    	                }, 
    	       ],
    	        [{
    	            margin:[-5,-3,-5,-3],
    	            table:{
    	                widths:[165,'*'],
    	                
    	                body:[
    	                [{text:'Details of Treatment\n',style:'left',colSpan: 2,bold:1},{},],
    	                [{text:'Name of Fumigant\n',style:'left'},{text:'ALUMINIUM PHOSPHIDE\n',style:'left'},],
    	                [{text:'Date of Fumigation\n',style:'left'},{text:fumdate+'\n',style:'left'},],
    	                [{text:'Place of Fumigation\n',style:'left'},{text:json1.fumplace+'\n',style:'left'},],
    	                [{text:'Dosage of Fumigant\n',style:'left'},{text:json1.fumdosage+' GMS / M.T\n',style:'left'},],
    	                [{text:'Duration of Fumigation (in days)\n',style:'left'},{text:json1.fumdurationdays+' / '+json1.fumdurationhrs+'\n',style:'left'},],
    	                [{text:'Average ambient humidity during \nfumigation (RH)\n',style:'left'},{text:json1.humidity+' %\n',style:'left'},],
    	                [{text:'Fumigation performed under gas tight sheets\n',style:'left'},{text:json1.fumperformed+'\n',style:'left',bold:1},],
    	                [{text:'Description of Goods\n',style:'left',colSpan: 2,bold:1},{text:'\n',style:'left'},],
    	                [{text:'Container Number (or numerical link).\n/Seal Number\n',style:'left'},
    	                {stack: [{text: [
    	                    {text:json1.containerno1+'\n',style:'left'},
    	                    {text:json1.containerno2+'\n',style:'left',},
    	                    {text:json1.containerno3+'\n',style:'left',},
    	                    {text:json1.containerno4+'\n',style:'left',},
    	                    ]}]}
    	                ],
    	                [{text:'Name & Address of Exporter\n',style:'left'},
    	                {stack: [{text: [
    	                    {text:json1.exporter1+'\n',style:'left'},
    	                    {text:json1.exporter2+'\n',style:'left',},
    	                    {text:json1.exporter3+'\n',style:'left',},
    	                    ]}]}
    	                    ],
                      [{text:'Name and Address of Importer\n',style:'left'},
    	                 {stack: [{text: [
    	                    {text:json1.consignee1+'\n',style:'left'},
    	                    {text:json1.consignee2+'\n',style:'left',},
    	                    {text:json1.consignee3+'\n',style:'left',},
    	                    ]}]}
    	                ],
                      [{text:'Name and Address of notified party\n',style:'left'},
    	                 {stack: [{text: [
    	                    {text:json1.notifiedparty1+'\n',style:'left'},
    	                    {text:json1.notifiedparty2+'\n',style:'left',},
    	                    {text:json1.notifiedparty3+'\n',style:'left',},
    	                    ]}]}
    	                ],
    	                
    	                [{text:'Type and description of cargo\n',style:'left'},
    	                 {stack: [{text: [
    	                    {text:json1.cargo1+'\n',style:'left'},
    	                    {text:json1.cargo2+'\n',style:'left',},
    	                    {text:json1.cargo3+'\n',style:'left',},
    	                    ]}]}
    	                ],
    	                [{text:'Quantity (MTs)/ No of packages/No\n of pieces',style:'left'},
    	                {stack: [{text: [
    	                    {text:json1.qty+'\n',style:'left'},
                          {text:json1.qty2+'\n',style:'left'},
    	                    ]}]}
    	                ],
    	                [{text:'Description of packaging material\n',style:'left'},
    	                {stack: [{text: [
    	                    {text:json1.description+'\n',style:'left'},
    	                    {text:json1.description1+'\n',style:'left',},
    	                    ]}]}
    	                ],
        	            [{text:'Shipping mark or brand\n',style:'left'},
        	            {stack: [{text: [
    	                    {text:json1.shipingmark+'\n',style:'left'},
    	                    {text:json1.description2+'\n',style:'left',},
    	                    ]}]}
        	            ],
                      [{text:'Port & country Of Loading\n',style:'left'},
                      {text:json1.loading+'\n',style:'left'},],
                      [{text:'Port of Discharge\n',style:'left'},
                      {text:json1.entryport+'\n',style:'left'},],
                      [{text:'Name of the Vessel\n',style:'left'},
                      {text:json1.vesselname+'\n',style:'left'},],
                      [{text:'Additional Declaration\n',style:'left'},
        	            {stack: [{text: [
    	                    {text:json1.additional+'\n',style:'left'},
    	                    {text:json1.additional1+'\n',style:'left',},
                          {text:json1.additional2+'\n',style:'left',},
    	                    ]}]}
        	            ],
                      [{text:'Name &,Signature of Accredited\nFumigation Operator with seal & date/\nAccreditation Number\n',style:'left',bold:1},
                      {stack: [{text: [
                        {text:json1.accname+'\n',style:'left'},
    	                    {text:json1.accno+'\n',style:'left',},
                          {text:'Date:'+issuedate+'\n',style:'left',},
    	                    ]}]}
                          ],
    	                ],
    	            },layout: {
        				hLineColor: function (i, node) {
        					return (i === 0 || i === node.table.body.length) ? '#778899' : '#778899';
        				},
        				vLineColor: function (i, node) {
        					return (i === 0 || i === node.table.widths.length) ? '#778899' : '#778899';
        				},
        	        }
    	        }
    	       ],
    	       
	        ]
		},layout: {

				hLineColor: function (i, node) {
					return (i === 0 || i === node.table.body.length) ? '#778899' : '#778899';
				},
				vLineColor: function (i, node) {
					return (i === 0 || i === node.table.widths.length) ? '#778899' : '#778899';
				},
	        }
		
		},
		{text:'\n\n'},
		
		
		],
		styles: {
        left:{
            fontSize: 8,
    		alignment:'left',
        },
        right:{
            fontSize: 8,
    		alignment:'right',
        },
		centerbold: {
			fontSize: 8,
			alignment:'center',
			bold:1
		},
		center: {
			fontSize: 8,
			alignment:'center',
		},
		bn: {
			fontSize: 8,
			border:[0,0,0,0],
		},
    }

                               }
             pdfMake.createPdf(dd).open();
}
function printer_AFAS(certid,branchid) {
 
   var jsondata = $.parseJSON($.ajax({
         type: "POST",
        data: {
          certid: certid
        },
        url: 'api/addjson.php',
        cache: false,
        success: function (res) {
        //jsondata =JSON.stringify(res.json);
         },
        dataType: "json", 
        async: false
    }).responseText);
  jsondata =JSON.stringify(jsondata);
  data1 = JSON.parse(jsondata);  
  json1 = data1.json;      
  //alert(data1.status);

  var brjsondata = $.parseJSON($.ajax({
          type: "POST",
          data: {
            certid: certid,branchid:branchid
          },
          url: 'api/getbrjson.php',
          cache: false,
          success: function (res) {
          //jsondata =JSON.stringify(res.json);
          },
          dataType: "json", 
          async: false
      }).responseText);
  brjsondata =JSON.stringify(brjsondata);
  brdata = JSON.parse(brjsondata);  
  brjson = brdata.branchjson;   
  var fumdate = new Date(json1.fumdate);
  var issuedate = new Date(json1.issuedate);
  var fumdate1 = new Date(json1.fumdate1);
  fumdate1 = fumdate1.getDate()+ "/" + (fumdate1.getMonth()+1) + "/" + fumdate1.getFullYear();
  fumdate = ('0' + fumdate.getDate()).slice(-2) + "/" + ('0' + (fumdate.getMonth()+1)).slice(-2) + "/" + fumdate.getFullYear();
    issuedate = ('0' + issuedate.getDate()).slice(-2)+ "/" +  ('0' + (issuedate.getMonth()+1)).slice(-2) + "/" + issuedate.getFullYear();
  var fullDate = new Date();
  var twoDigitMonth = ((fullDate.getMonth().length + 1) === 1) ? (fullDate.getMonth() + 1) : '' + (fullDate.getMonth() + 1);
  var currentDate = fullDate.getDate() + "/" + twoDigitMonth + "/" + fullDate.getFullYear();
  var content=[]; 
  certno = 'PMI/'+brjson.branchshortname+'/AFAS/'+json1.srno+'/'+fullDate.getFullYear().toString().substr(2,2)+'-'+((fullDate.getFullYear()+1).toString().substr(2,2));
                  dd = {
                    pageSize: 'A4',
                    pageMargins: [50, 30, 30, 40],
                    footer: function (currentPage, pageCount) {
                        return {
                        };

                       },

                    content: [
                            '\n',
                            {
                                text: 'PEST MORTEM (INDIA) PRIVATE LIMITED',
                                 fontSize: 14,
                                      bold: true,
                                      alignment: 'center',
                            },
                            {
                                    text: '(Approved by National Plant Protection Organization, Government of India)',
                                     fontSize: 8,
                                      bold: true,
                                      alignment: 'center',
                                },
  
                                {
                                    text: brjson.address,
                                     fontSize: 8,
                                       alignment: 'center',bold:1,
                                },
 
                                {
                                    text: 'AN ISO 9001:2008 CERTIFIED COMPANY',
                                     fontSize: 8,
                                      alignment: 'center',
                                },
 
                                 {
                                    text: 'AFAS-METHYL BROMIDE FUMIGATION CERTIFICATE',
                                    fontSize: 12,
                                      bold: true,
                                      alignment: 'center',
                                },
 
                                {
                                     border : 0,
                                    bold: true,
                                     fontSize: 8,
                                    margin: [0, 5, 0, 5],
                                    table: {
                                         widths: [80, 340, 100],
                                   
                                        body: [
                                            ['CERTIFICATE NO. :', certno.toUpperCase(), 'AEI NO : IN 0012 MB'],
                                        ]
                                    },
                                        layout: 'noBorders'
                                },
                                 {
                                    text: 'TARGET OF FUMIGATION DETAILS',
                                    fontSize: 8,
                                      bold: true,
                                      alignment: 'center',
                                },
                                {canvas: [{ type: 'line', x1: 0, y1: 5, x2: 595-2*40, y2: 5, lineWidth: 0.1 }]},
                                
                                
                                {
                                   border : 0, 
                                     fontSize: 8,
                                    margin: [0, 5, 0, 5],
                                    table: {
                                         widths: [81, 140 ,70,'*'],
                                   
                                        body: [
                                          ['Target of Fumigation  ',': '+ json1.fumtarget,'Commodity ', ': '+json1.commodify],
                                          ['Port of Loading',': '+ json1.loading,'',': '+ json1.commodify1],
                                          ['Country Of Origin  ', ': '+json1.countryorg,'Quantity ',': '+json1.qty1],
                                          ['Port of Discharge ', ': '+json1.portofdischarge,'',{text:': '+json1.qty2,border: [false, false, false, false]}],
                                          ['Country Of Destination',': '+ json1.destinationcon,'',{text:': '+json1.qty3,border: [false, false, false, false]}],
                                          ['Vessel',': '+json1.afasvessel,'Consignment Link ',': '+ json1.consignment],
                                          ['','','',': '+ json1.consignment2,],
                                           ]
                                     },
                                        layout: 'noBorders'
                                  },
                                  {
                                    fontSize: 8,
                                    margin: [0, 5, 0, 5],
                                    table: {
                                         widths: [250, '*'],
                                   
                                        body: [
                                            [
                                            { border : [false, true, true, false], 
                                            text: 'Name and address of exporter', fontSize: 8,bold:true
                                            },
                                             {
                                               border : [false, true, false, false], 
                                              text: 'Name and address of importer',  fontSize: 8,bold:true}],
                  
                                          [
                                          {
                                            border : [false, false, true, false],
                                            text: json1.exporter1, fontSize: 8
                                          },
                                            {
                                              border : [true, false, false, false],
                                              text: json1.consignee1, fontSize: 8
                                            }],
                                             [
                                          {
                                            border : [false, false, true, false],
                                            text: json1.exporter2, fontSize: 8
                                          },
                                            {
                                              border : [false, false, false, false],
                                              text: json1.consignee2, fontSize: 8
                                            }],
                                             [
                                          {
                                            border : [false, false, true, false],
                                            text: json1.exporter3, fontSize: 8
                                          },
                                            {
                                              border : [false, false, false, false],
                                              text: json1.consignee3, fontSize: 8
                                            }],
                                            [
                                          {
                                            border : [false, false, true, true],
                                            text: json1.exporter4, fontSize: 8
                                          },
                                            {
                                              border : [false, false, false, true],
                                              text: json1.consignee4, fontSize: 8
                                            }]
                                         
                                            
                                        ]
                                    },
                                          
                                },


                                  {
                                    text: 'TREATMENT DETAILS',
                                    fontSize :8,
                                    bold:true,
                                    alignment : 'center',
                                },
                                {
                                    border : 0, 
                                     fontSize: 8,
                                    margin: [0, 5, 0, 5],
                                    table: {
                                         widths: [120, '*', 100, '*'],
                                   
                                        body: [
                                            ['Date of fumigation complieted  ', ': '+fumdate1, 'Place of fumigation ',': '+json1.fumplace],
                                            ['DAFF Prescribes dose rate ',': '+ json1.daffdosage, 'Exposure Period ',': '+json1.fumdurationhrs+' HOURS'],
                                            ['Mini. Air temp(Deg. Cent.) ',': '+json1.mintemp,'Applied dose rate ',': '+ json1.fumdosage], 
                                               
                                        ]
                                    },
                                        layout: 'noBorders'
                                },
                                 {canvas: [{ type: 'line', x1: 0, y1: 5, x2: 595-2*40, y2: 5, lineWidth: 1 }]},
                                {
                                    border : 0, 
                                     fontSize: 8,
                                    margin: [0, 5, 0, 5],
                                    table: {
                                         widths: [200, '*'],
                                   
                                        body: [
                                            ['How was the fumigation conducted :', json1.fumconducted],
                                            ['Container number (where applicable) : ', json1.containerno1 ], 
                                               
                                        ]
                                    },
                                        layout: 'noBorders'
                                },
                                  {canvas: [{ type: 'line', x1: 0, y1: 5, x2: 595-2*40, y2: 5, lineWidth: 1 }]},
                                   {
                                    border : 0, 
                                     fontSize: 8,
                                    margin: [0, 5, 0, 5],
                                    table: {
                                         widths: [300, '*'],
                                   
                                        body: [
                                            ['Does the target of the fumigation conform to the DAFF plastic wrapping, Impervious surface and timber thickness requirements at the time of fumgation? : ', json1.targetfum],
                                             
                                            
                                               
                                        ]
                                    },
                                        layout: 'noBorders'
                                },
                                {canvas: [{ type: 'line', x1: 0, y1: 5, x2: 595-2*40, y2: 5, lineWidth: 1 }]},
                                '\n',
                                {
                                  text  : [{text:'Ventilation\t\t\t',fontSize: 8,bold:1},{text:'FINAL TLV READING (PPM)',fontSize: 8},{text:'\t\t\t\t\t\t'+json1.ventilation,fontSize: 8},{text:'\t\t\t\t\t\t\t(Not required for stack or pernanent chamber fumigation)',fontSize: 8}],
                                },
                                   
                                  {canvas: [{ type: 'line', x1: 0, y1: 5, x2: 595-2*40, y2: 5, lineWidth: 1 }]},
                                  '\n',
                                   {
                                    text: 'DECLARATION',
                                    fontSize :8,
                                    bold : true,
                                    alignment : 'center',
                                  },
                                  '\n',
                                    {text: 'By signing below, I, the AFAS accredited fumigator responsible, declare the these details are true and correct and the Fumigation has been carried out in accordance with all the requirements in the DAFF Methyl Bromide Fumigation Standard.',
                                     fontSize: 8,
                                  },
                                {canvas: [{ type: 'line', x1: 0, y1: 5, x2: 595-2*40, y2: 5, lineWidth: 1 }]},
                                '\n',
                                {
                                    text: 'ADDITIONAL DECLARATION',
                                    fontSize :8,
                                    bold : true,
                                    alignment : 'center',
                                },
                                {text: json1.additional,
                                 fontSize: 8,
                                },
                                {text: json1.additional1,
                                 fontSize: 8,
                                },
                                {text: json1.additional2,
                                 fontSize: 8,
                                },
                                '\n',
                                {canvas: [{ type: 'line', x1: 0, y1: 5, x2: 595-2*40, y2: 5, lineWidth: 1 }]},
                                '\n','\n','\n','\n',
                                 {
                                    bold: true,
                                 fontSize: 8,
                                margin: [0, 5, 0, 5],
                                    table: {
                                         widths: [120, 250,60],
                                   
                                        body: [
                                            [ {text:'Signature',alignment: 'center'},{text:'Date : '+issuedate,alignment: 'center'},'Company Stamp'],
                                            [ {text:json1.accname,alignment: 'center'},{text:json1.accno,alignment: 'center'},''],
                                            [{text:'Name of Accredited Fumigator',alignment: 'center'},{text:'AFAS-Accreditation Number',alignment: 'center'},''],
                                            ]
                                       },
                                       layout: 'noBorders'
                                    },    
 

                        ]
                     }
                              
            //  pdfMake.createPdf(dd).download("AFAScertificate_"+certid+".pdf");
             pdfMake.createPdf(dd).open();
}
function printer_MBR(certid,branchid) {
 
   var jsondata = $.parseJSON($.ajax({
         type: "POST",
        data: {
          certid: certid
        },
        url: 'api/addjson.php',
        cache: false,
        success: function (res) {
       
         },
        dataType: "json", 
        async: false
    }).responseText);
  jsondata =JSON.stringify(jsondata);
  data1 = JSON.parse(jsondata);  
  json1 = data1.json;      

  var brjsondata = $.parseJSON($.ajax({
          type: "POST",
          data: {
            certid: certid,branchid:branchid
          },
          url: 'api/getbrjson.php',
          cache: false,
          success: function (res) {
          //jsondata =JSON.stringify(res.json);
          },
          dataType: "json", 
          async: false
      }).responseText);
  brjsondata =JSON.stringify(brjsondata);
  brdata = JSON.parse(brjsondata);  
  brjson = brdata.branchjson;   


  var fumdate = new Date(json1.fumdate);
  var issuedate = new Date(json1.issuedate);

    fumdate = ('0' + fumdate.getDate()).slice(-2) + "/" + ('0' + (fumdate.getMonth()+1)).slice(-2) + "/" + fumdate.getFullYear();
    issuedate = ('0' + issuedate.getDate()).slice(-2)+ "/" +  ('0' + (issuedate.getMonth()+1)).slice(-2) + "/" + issuedate.getFullYear();


  var fullDate = new Date();
  var twoDigitMonth = ((fullDate.getMonth().length + 1) === 1) ? (fullDate.getMonth() + 1) : '' + (fullDate.getMonth() + 1);
  var currentDate = fullDate.getDate() + "/" + twoDigitMonth + "/" + fullDate.getFullYear();
  var content=[]; 
  

  certno = 'PMI/'+brjson.branchshortname+'/MBR/'+json1.srno+'/'+fullDate.getFullYear().toString().substr(2,2)+'-'+((fullDate.getFullYear()+1).toString().substr(2,2));
                 dd = {
                    pageSize: 'A4',
                    pageMargins: [40, 30, 30, 40],
                    //pageMargins: [40, 25, 20, 30],
                    footer: function (currentPage, pageCount) {
                        return {
                        };

                       },
            content: [
            '\n',
            {
                text: 'PEST MORTEM (INDIA) PRIVATE LIMITED',fontSize: 14,bold: true,alignment: 'center',margin:[0,5,0,0]
            },
            {
                text: '(Approved by National Plant Protection Organization, Government of India)',
                 fontSize: 8, bold: true, alignment: 'center',
            },
            {
                text: 'AN ISO 9001:2008 CERTIFIED COMPANY',
                 fontSize: 8,alignment: 'center',margin:[0,0,0,10]
            },
	        {
                margin:[0,-4,0,0],
                table:{
                widths:[510],
                body:[
                    [{
                      margin:[-5,-3,-5,-3],
                      border:[0,0,0,0],
    	                table:{
                        
    	                widths:["*"],
    	                body:[
    	                   [{text: 'FUMIGATION CERTIFICATE',fontSize: 12,bold: true,alignment: 'center',margin:[0,0,0,0],border:[0,0,0,0],},]
    	                    ]
        	            },
	            },
	            ],
    	        [{
    	            margin:[-4,-3,-5,-3],
    	            table:{
    	                widths:['auto',10,'auto','*'],
    	                
    	                body:[
        	            [
        	            {stack: [{text: [
                        {text: 'PEST MORTEM (INDIA) PRIVATE LIMITED\n',style: 'left',bold:1},
                        {text:brjson.address+'\n',style:'left',bold:1},
                        {text:'Dte. PPQS Regd. No.\t012/MB/DATED : 25.07.2005  ',style:'left',bold:1},
                        
                        ],}],border:[0,0,0,0]
        	            },
    	                {
    	                stack: [{text: [
                        {text:'\n',style: 'left'},
                        {text:'\n',style:'left'},
                        {text:'\n',style:'left'},
                        ],}],border:[0,0,1,0]   
    	                },
    	                //{text:'',border:[0,0,0,0]},
    	                {
    	                stack: [{text: [
                        {text:'Treatment Cert. No. : \n',style: 'left',bold:1},
                        {text:'\n',style:'left'},
                        {text:'Date of Issue :\n',style: 'left',bold:1}
                        ],}],border:[0,0,0,0]   
    	                },
    	                {
    	                stack: [{text: [
                        {text:certno+'\n',style: 'left',bold:1},
                        {text:'\n',style:'left'},
                        {text:issuedate+'\n',style: 'left',bold:1}
                        ],}],border:[0,0,0,0]   
    	                },
    	                ]
    	                    ]
    	            },layout: {
        				hLineColor: function (i, node) {
        					return (i === 0 || i === node.table.body.length) ? '#778899' : '#778899';
        				},
        				vLineColor: function (i, node) {
        					return (i === 0 || i === node.table.widths.length) ? '#778899' : '#778899';
        				},
        	        }
    	        }],
    	        [
    	           {
    	                stack: [{text: [
    	                    {text:'\tThis is to certify that the goods described below were treated in accordance with the fumigation treatment requirements of importing country ',fontSize: 8,italics: true},
                            {text: json1.country.toUpperCase(), italics: false, fontSize: 9,bold:1},
                            {text:' and declared that the consignment has been verified free of impervious surfaces/layers such as plastic wrapping or laminated plastic films, lacquered or painted surfaces, aluminium foil, tarred or waxed papers etc. that may adversely effect the penetration of the fumigant, prior to fumigation.',fontSize: 8,italics: true},
                            {text:'\nThe Certificate is valid for the consignments shipped within 21 days from the date of completion of fumigation',style: '',color:'',fontSize: 8,},
                        ],}],border:[1,1,1,1]   
    	                }, 
    	       ],
    	        [{
    	            margin:[-5,-3,-5,-3],
    	            table:{
    	                widths:[165,'*'],
    	                
    	                body:[
    	                [{text:'Details of Treatment\n',style:'left',colSpan: 2,bold:1},{},],
    	                [{text:'Name of Fumigant\n',style:'left'},{text:'METHYL BROMIDE\n',style:'left'},],
    	                [{text:'Date of Fumigation\n',style:'left'},{text:fumdate+'\n',style:'left'},],
    	                [{text:'Place of Fumigation\n',style:'left'},{text:json1.fumplace+'\n',style:'left'},],
    	                [{text:'Dosage of Fumigant\n',style:'left'},{text:json1.fumdosage+' GMS/M3\n',style:'left'},],
    	                [{text:'Duration of Fumigation (in HRS)\n',style:'left'},{text:json1.fumdurationhrs+'\n',style:'left'},],
    	                [{text:'Average ambient temp during Fumigation(°C)\n',style:'left'},{text:json1.mintemp+' °C\n',style:'left'},],
    	                [{text:'Fumigation performed under gas tight sheets\n',style:'left'},{text:json1.fumperformed+'\n',style:'left',bold:1},],
                      [{text:'If containers are not fumigated under gastight\nsheets, pressure in seconds decay value (from\n200-100 Pascals in seconds)\n',style:'left'},{text:json1.pressureinsec+'\n',style:'left'},],
    	                [{text:'Description of Goods\n',style:'left',colSpan: 2,bold:1},{text:'\n',style:'left'},],
    	                [{text:'Container Number (or numerical link).\n/Seal Number\n',style:'left'},
    	                {stack: [{text: [
    	                    {text:json1.containerno1+'\n',style:'left'},
    	                    {text:json1.containerno2+'\n',style:'left',},
    	                    {text:json1.containerno3+'\n',style:'left',},
    	                    {text:json1.containerno4+'\n',style:'left',},
    	                    ]}]}
    	                ],
    	                [{text:'Name & Address of Exporter\n',style:'left'},
    	                {stack: [{text: [
    	                    {text:json1.exporter1+'\n',style:'left'},
    	                    {text:json1.exporter2+'\n',style:'left',},
    	                    {text:json1.exporter3+'\n',style:'left',},
    	                    ]}]}
    	                    ],
    	                [{text:'Name & Address of Importer\n',style:'left'},
    	                 {stack: [{text: [
    	                    {text:json1.consignee1+'\n',style:'left'},
    	                    {text:json1.consignee2+'\n',style:'left',},
    	                    {text:json1.consignee3+'\n',style:'left',},
    	                    ]}]}
    	                ],
                      [{text:'Name & Address of notified party\n',style:'left'},
    	                 {stack: [{text: [
    	                    {text:json1.notifiedparty1+'\n',style:'left'},
    	                    {text:json1.notifiedparty2+'\n',style:'left',},
    	                    {text:json1.notifiedparty3+'\n',style:'left',},
    	                    ]}]}
    	                ],
    	                [{text:'Type and description of cargo\n',style:'left'},
    	                 {stack: [{text: [
    	                    {text:json1.description+'\n',style:'left'},
    	                    {text:json1.description1+'\n',style:'left',},
    	                    ]}]}
    	                ],
    	                [{text:'Quantity (MTs)/ No of packages/No\n of pieces',style:'left'},
    	                {stack: [{text: [
    	                    {text:json1.qty1+'\n',style:'left'},
                          {text:json1.qty2,style:'left'},
    	                    ]}]}
    	                ],
    	                [{text:'Description of packaging material\n',style:'left'},
    	                {stack: [{text: [
    	                    {text:json1.description2+'\n',style:'left'},
    	                    {text:json1.description3+'\n',style:'left',},
    	                    ]}]}
    	                ],
        	            [{text:'Shipping mark or brand\n',style:'left'},
        	            {stack: [{text: [
    	                    {text:json1.shipingmark+'\n',style:'left'},
    	                    {text:json1.shipingmark1+'\n',style:'left',},
    	                    ]}]}
        	            ],
                      [{text:'Port & country Of Loading\n',style:'left'},
                      {text:json1.loading+'\n',style:'left'},],
                      [{text:'Port of Discharge\n',style:'left'},
                      {text:json1.entryport+'\n',style:'left'},],
                      [{text:'Name of the Vessel\n',style:'left'},
                      {text:json1.vesselname+'\n',style:'left'},],
                      [{text:'Additional Declaration\n',style:'left'},
        	            {stack: [{text: [
    	                    {text:json1.additional+'\n',style:'left'},
    	                    {text:json1.additional1+'\n',style:'left',},
                          {text:json1.additional2+'\n',style:'left',},
    	                    ]}]}
        	            ],
        	            [{text:'Name &,Signature of Accredited\nFumigation Operator with seal & date/\nAccreditation Number\n',style:'left',bold:1},
                      {stack: [{text: [
                        {text:json1.accname+'\n',style:'left'},
    	                    {text:json1.accno+'\n',style:'left',},
                          {text:'Date:'+issuedate+'\n',style:'left',},
    	                    ]}]}
                          ],
        	       
    	                ],
    	            },layout: {
        				hLineColor: function (i, node) {
        					return (i === 0 || i === node.table.body.length) ? '#778899' : '#778899';
        				},
        				vLineColor: function (i, node) {
        					return (i === 0 || i === node.table.widths.length) ? '#778899' : '#778899';
        				},
        	        }
    	        }
    	       ],
    	       
	        ]
		},layout: {

				hLineColor: function (i, node) {
					return (i === 0 || i === node.table.body.length) ? '#778899' : '#778899';
				},
				vLineColor: function (i, node) {
					return (i === 0 || i === node.table.widths.length) ? '#778899' : '#778899';
				},
	        }
		
		},
		{text:'\n\n'},
		
		
		],
		styles: {
        left:{
            fontSize: 8,
    		alignment:'left',
        },
        right:{
            fontSize: 8,
    		alignment:'right',
        },
		centerbold: {
			fontSize: 8,
			alignment:'center',
			bold:1
		},
		center: {
			fontSize: 8,
			alignment:'center',
		},
		bn: {
			fontSize: 8,
			border:[0,0,0,0],
		},
    }
                               }
                              
            
              pdfMake.createPdf(dd).open();
}
</script>