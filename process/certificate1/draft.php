<?php 

    $base='../../';
    $navenq5='background:#1B1464;';
   include('header.php');
   
   $enquiryidnew=mysqli_fetch_assoc(mysqli_query($con,"SELECT id x FROM crmmaster"))['x'];
     $year1=0;
     $year2=0;
   if ( date('m') > 3 ) {
       $year1 = date('y');
       $year2 = date('y')+1;
   }else{
       $year1 = date('y')-1;
       $year2 = date('y');
   }
   if (session_status() == PHP_SESSION_NONE) { session_start(); }
   $sessionby = $_SESSION['employeeid'];
   $branchid = $_SESSION['branchid'];
   $role = $_SESSION['role'];

   $branchshortname=mysqli_fetch_assoc(mysqli_query($con,"SELECT branchshortname x FROM branchmaster WHERE branchid='$branchid'"))['x'];
     if($enquiryidnew){
       $enquiryidnew++;
       $enquiryidnew=str_pad($enquiryidnew,6, '0', STR_PAD_LEFT);
       $enquiryidnew='PMI/'.$branchshortname.'/ENQ/'.$enquiryidnew.'/'.$year1.'-'.$year2;
     }else{
       $enquiryidnew='PMI/'.$year1.'-'.$year2.'/B'.$branchid.'/E'.$sessionby.'/1';
       $enquiryidnew='PMI/'.$branchshortname.'/ENQ/000001/'.$year1.'-'.$year2;
     }
     $certType=$_GET['certType'];
     $country=$_GET['country'];
     $exporter1=addslashes($_GET['exporter1']);
     $consignee1=addslashes($_GET['consignee1']);

     $certificateid=$_GET['certificateid'];

     if($certType=='Select'){$certType='';}
     if($country=='Select'){$country='';}
     if($exporter1=='Select'){$exporter1='';}
     if($consignee1=='Select'){$consignee1='';}
     if($certificateid=='Select'){$certificateid='';}
     
   ?>
<br>
<style>
   .input-container {
   max-width: 300px;
   background-color: #EDEDED;
   border: 1px solid #DFDFDF;
   border-radius: 5px;
   }
   input[type='file'] {
   display: none;
   }
   .file-info {
   font-size: 0.9em;
   }
   .browse-btn {
   background: #03A595;
   color: #fff;
   min-height: 35px;
   padding: 10px;
   border: none;
   border-top-left-radius: 5px;
   border-bottom-left-radius: 5px;
   }
   .browse-btn:hover {
   background: #4ec0b4;
   }
   @media (max-width: 300px) {
   button {
   width: 100%;
   border-top-right-radius: 5px;
   border-bottom-left-radius: 0;
   }
   .file-info {
   display: block;
   margin: 10px 5px;
   }
   }
</style>
<input type="hidden" id="certPage" value="<?php echo $_GET['certPage']; ?>">
<div class="row" style="padding-top:-40px;">
<div class="col-sm-2">
            <div class="form-group">
             <select class="form-control input-md select-js1"  data-role="select" data-name="certType" id="certType1">
                    <option value="Select">Select Certificate Type</option>
                    <option value="ALP">ALP</option>
                    <option value="MBR">MBR</option>
                    <option value="AFAS">AFAS</option>

                   
                </select>
            </div>
    </div>
    <div class="col-sm-2">
            <div class="form-group">
             <select class="form-control input-md select-js1"  data-role="select" data-name="country" id="country">
                    <option value="Select">Select Country</option>
                    <?php
                            if($country!=''){
                              echo '<option value="'.$country.'" selected>'.$country.'</option>'; 
                            }
   
                            $result1=mysqli_query($con, "SELECT DISTINCT country FROM `certificatemaster` WHERE country<>'$country' AND created='y' AND confirm<>'Y'");
                            while($rows=mysqli_fetch_assoc($result1)) {
                                echo '<option value="'.$rows['country'].'">'.$rows['country'].'</option>';
                            }
                        ?> 
                </select>
            </div>
    </div>
    <div class="col-sm-2">
        <div class="form-group">
        <select class="form-control input-md select-js1"  data-role="select" data-name="exporter1" id="exporter1">
                <option value="Select">Select Name of Exporter</option>
                <?php
                        if($exporter1!=''){
                          echo '<option value="'.stripslashes($exporter1).'" selected>'.stripslashes($exporter1).'</option>'; 
                        }
                        
                          $result1=mysqli_query($con, "SELECT DISTINCT exporter1 FROM `certificatemaster` WHERE exporter1<>'$exporter1'  AND created='y' AND confirm<>'Y'");
                          while($rows=mysqli_fetch_assoc($result1)) {
                            echo '<option value="'.$rows['exporter1'].'">'.$rows['exporter1'].'</option>';
                        }
                    ?> 
            </select>
        </div>
    </div>

    <div class="col-sm-2">
        <div class="form-group">
        <select class="form-control input-md select-js1"  data-role="select" data-name="consignee1" id="consignee1">
                <option value="Select">Select Name of Importer</option>
                <?php
                        if($consignee1!=''){
                          echo '<option value="'.stripslashes($consignee1).'" selected>'.stripslashes($consignee1).'</option>'; 
                        }
                       
                          $result1=mysqli_query($con, "SELECT DISTINCT consignee1 FROM `certificatemaster` WHERE consignee1<>'$consignee1'  AND  created='y' AND confirm<>'Y'");
                          
                          while($rows=mysqli_fetch_assoc($result1)) {
                            echo '<option value="'.$rows['consignee1'].'">'.$rows['consignee1'].'</option>';
                        }
                    ?> 
            </select>
        </div>
    </div>


    <div class="col-sm-2">
        <div class="form-group">
              <select class="form-control input-md select-js1"  data-role="select" data-name="certificateid" id="certificateid">
                        <option value="Select">Select Certificate Number</option>
                        <?php 
                         if($certificateid!=''){
                          $certificateno=mysqli_fetch_assoc(mysqli_query($con,"SELECT certificateno x FROM certificatemaster WHERE id='$certificateid'"))['x'];
                          echo '<option value="'.$certificateid.'" selected>'.$certificateno.'</option>'; 
                        }
                        
                          $result2=mysqli_query($con, "SELECT id, certificateno FROM `certificatemaster` WHERE  isdraft='Y' AND created='y' AND confirm<>'Y' AND id<>'$certificateid'");
                        
                          while($rows=mysqli_fetch_assoc($result2)) {
                            echo '<option value="'.$rows['id'].'">'.$rows['certificateno'].'</option>';
                        }
                        ?>
                        
              </select>
        </div>
    </div>
    <div class="col-sm-2">
        <a class="btn btn-primary btn-sm btn-block" onclick="submit()">Submit</a>
    </div>
</div>
<div class="table-ui container-fluid" id="maintableview">
   <div class="tr row">
      <div class="col-sm-1 th">Sr. No.</div>
      <div class="col-sm-2 th">Certificate No. / Sr. No.</div>
      <div class="col-sm-7 th">Summary</div>
      <div class="col-sm-1 th">Cert Type</div>
      <div class="col-sm-1 th">Action</div>
   </div>
   <?php

      //echo $country,$exporter1,$certType;
      if($certType=='' && $country=='' && $exporter1=='' && $certificateid=='' && $consignee1==''){ //000
          $query= "SELECT * FROM `certificatemaster` WHERE branchid='$branchid' AND  confirm<>'Y' AND calculated<>'Y' AND isdraft='Y' ";
      }

      
      
      //certType
      if($certType!='' && $country=='' && $exporter1=='' && $certificateid=='' && $consignee1==''){

        $query= "SELECT * FROM `certificatemaster` WHERE branchid='$branchid' AND certType='$certType'  AND confirm<>'Y' AND calculated<>'Y' AND created='Y' AND isdraft='Y'";


      }

      if($certType!='' && $country!='' && $exporter1=='' && $certificateid=='' && $consignee1==''){

        $query= "SELECT * FROM `certificatemaster` WHERE branchid='$branchid' AND certType='$certType' AND country='$country' AND confirm<>'Y' AND calculated<>'Y' AND created='Y' AND isdraft='Y'";


      }

      if($certType!='' && $country=='' && $exporter1!='' && $certificateid=='' && $consignee1==''){

        $query= "SELECT * FROM `certificatemaster` WHERE branchid='$branchid' AND certType='$certType' AND exporter1 LIKE '%$exporter1%' AND confirm<>'Y' AND calculated<>'Y' AND created='Y' AND isdraft='Y'";


      }

      if($certType!='' && $country=='' && $exporter1=='' && $certificateid!='' && $consignee1==''){

        $query= "SELECT * FROM `certificatemaster` WHERE branchid='$branchid' AND certType='$certType' AND id='$certificateid' AND confirm<>'Y' AND calculated<>'Y' AND created='Y' AND isdraft='Y'";


      }

      if($certType!='' && $country=='' && $exporter1=='' && $certificateid=='' && $consignee1!=''){

        $query= "SELECT * FROM `certificatemaster` WHERE branchid='$branchid' AND certType='$certType' AND consignee1 Like '%$consignee1%' AND confirm<>'Y' AND calculated<>'Y' AND created='Y' AND isdraft='Y'";


      }

      if($certType!='' && $country!='' && $exporter1!='' && $certificateid=='' && $consignee1==''){

        $query= "SELECT * FROM `certificatemaster` WHERE branchid='$branchid' AND certType='$certType' AND country='$country' AND exporter1 LIKE '%$exporter1%' AND confirm<>'Y' AND calculated<>'Y' AND created='Y' AND isdraft='Y'";


      }

      if($certType!='' && $country!='' && $exporter1=='' && $certificateid!='' && $consignee1==''){

        $query= "SELECT * FROM `certificatemaster` WHERE branchid='$branchid' AND certType='$certType' AND country='$country' AND id='$certificateid' AND confirm<>'Y' AND calculated<>'Y' AND created='Y' AND isdraft='Y'";


      }

      if($certType!='' && $country!='' && $exporter1=='' && $certificateid=='' && $consignee1!=''){

        $query= "SELECT * FROM `certificatemaster` WHERE branchid='$branchid' AND certType='$certType' AND country='$country' AND consignee1 LIKE '%$consignee1%' AND confirm<>'Y' AND calculated<>'Y' AND created='Y' AND isdraft='Y'";

      }

      if($certType!='' && $country=='' && $exporter1!='' && $certificateid!='' && $consignee1==''){

        $query= "SELECT * FROM `certificatemaster` WHERE branchid='$branchid' AND certType='$certType' AND id='$certificateid' AND exporter1 LIKE '%$exporter1%' AND confirm<>'Y' AND calculated<>'Y' AND created='Y' AND isdraft='Y'";

      }

      if($certType!='' && $country=='' && $exporter1!='' && $certificateid=='' && $consignee1!=''){

        $query= "SELECT * FROM `certificatemaster` WHERE branchid='$branchid' AND certType='$certType' AND consignee1 LIKE '%$consignee1%' AND exporter1 LIKE '%$exporter1%' AND confirm<>'Y' AND calculated<>'Y' AND created='Y' AND isdraft='Y'";


      }

      if($certType!='' && $country=='' && $exporter1=='' && $certificateid!='' && $consignee1!=''){

        $query= "SELECT * FROM `certificatemaster` WHERE branchid='$branchid' AND certType='$certType' AND consignee1 LIKE '%$consignee1%' AND id='$certificateid' AND confirm<>'Y' AND calculated<>'Y' AND created='Y' AND isdraft='Y'";


      }

      if($certType!='' && $country!='' && $exporter!='' && $certificateid!='' && $consignee1==''){

        $query= "SELECT * FROM `certificatemaster` WHERE branchid='$branchid' AND certType='$certType' AND country='$country' AND exporter1 LIKE '%$exporter1%' AND id='$certificateid' AND confirm<>'Y' AND calculated<>'Y' AND created='Y' AND isdraft='Y'";


      }

      if($certType!='' && $country=='' && $exporter!='' && $certificateid!='' && $consignee1!=''){

        $query= "SELECT * FROM `certificatemaster` WHERE branchid='$branchid' AND certType='$certType' AND consignee1 LIKE '%$consignee1%' AND exporter1 LIKE '%$exporter1%' AND id='$certificateid' AND confirm<>'Y' AND calculated<>'Y' AND created='Y' AND isdraft='Y'";


      }


      //country
      if($certType=='' && $country!='' && $exporter1=='' && $certificateid=='' && $consignee1==''){

        $query= "SELECT * FROM `certificatemaster` WHERE branchid='$branchid' AND  confirm<>'Y' AND calculated<>'Y' AND created='Y' AND country='$country' AND isdraft='Y'";

      }

      if($certType=='' && $country!='' && $exporter1!='' && $certificateid=='' && $consignee1==''){
        //110
        $query= "SELECT * FROM `certificatemaster` WHERE branchid='$branchid' AND  confirm<>'Y' AND calculated<>'Y' AND created='Y' AND country='$country' AND exporter1 LIKE '%$exporter1%' AND isdraft='Y'";
      }

      if($certType=='' && $country!='' && $exporter1=='' && $certificateid!='' && $consignee1==''){
        //101
        $query= "SELECT * FROM `certificatemaster` WHERE branchid='$branchid' AND  confirm<>'Y' AND calculated<>'Y' AND created='Y' AND country='$country' AND id='$certificateid' AND isdraft='Y'";
      }

      if($certType=='' && $country!='' && $exporter1=='' && $certificateid=='' && $consignee1!=''){
        //110
        $query= "SELECT * FROM `certificatemaster` WHERE branchid='$branchid' AND  confirm<>'Y' AND calculated<>'Y' AND created='Y' AND country='$country' AND consignee1 Like '%$consignee1%' AND isdraft='Y'";
      }

      if($certType=='' && $country!='' && $exporter1!='' && $certificateid!='' && $consignee1==''){

        $query= "SELECT * FROM `certificatemaster` WHERE branchid='$branchid' AND  confirm<>'Y' AND calculated<>'Y' AND created='Y' AND country='$country' AND exporter1 LIKE  '%$exporter1%' AND id='$certificateid' AND isdraft='Y'";

      }

      if($certType=='' && $country!='' && $exporter1!='' && $certificateid=='' && $consignee1!=''){

        $query= "SELECT * FROM `certificatemaster` WHERE branchid='$branchid' AND  confirm<>'Y' AND calculated<>'Y' AND created='Y' AND country='$country' AND exporter1 LIKE  '%$exporter1%' AND consignee1 LIKE '%$consignee1%' AND isdraft='Y'";
        
      }

      if($certType=='' && $country!='' && $exporter1=='' && $certificateid!='' && $consignee1!=''){

        $query= "SELECT * FROM `certificatemaster` WHERE branchid='$branchid' AND  confirm<>'Y' AND calculated<>'Y' AND created='Y' AND country='$country' AND id='$certificateid' AND consignee1 LIKE '%$consignee1%' AND isdraft='Y'";
        
      }


      if($certType=='' && $country!='' && $exporter!='' && $certificateid!='' && $consignee1!=''){

        $query= "SELECT * FROM `certificatemaster` WHERE branchid='$branchid' AND country='$country' AND consignee1 LIKE '%$consignee1%' AND exporter1 LIKE '%$exporter1%' AND id='$certificateid' AND confirm<>'Y' AND calculated<>'Y' AND created='Y' AND isdraft='Y'";


      }
    
      //exporter1
      if($certType=='' && $country=='' && $exporter1!='' && $certificateid=='' && $consignee1==''){

        $query= "SELECT * FROM `certificatemaster` WHERE branchid='$branchid' AND  confirm<>'Y' AND calculated<>'Y' AND created='Y' AND exporter1 LIKE '%$exporter1%' AND isdraft='Y'";

      }
      

      if($certType=='' && $country=='' && $exporter1!='' && $certificateid!='' && $consignee1==''){

        $query= "SELECT * FROM `certificatemaster` WHERE branchid='$branchid' AND  confirm<>'Y' AND calculated<>'Y' AND created='Y' AND exporter1 LIKE  '%$exporter1%' AND id='$certificateid' AND isdraft='Y'";

      }

      if($certType=='' && $country=='' && $exporter1!='' && $certificateid=='' && $consignee1!=''){

        $query= "SELECT * FROM `certificatemaster` WHERE branchid='$branchid' AND  confirm<>'Y' AND calculated<>'Y' AND created='Y' AND exporter1 LIKE  '%$exporter1%' AND consignee1 LIKE '%$consignee1%' AND isdraft='Y'";

      }

      if($certType=='' && $country=='' && $exporter1!='' && $certificateid!='' && $consignee1!=''){

        $query= "SELECT * FROM `certificatemaster` WHERE branchid='$branchid' AND  confirm<>'Y' AND calculated<>'Y' AND created='Y' AND exporter1 LIKE  '%$exporter1%' AND consignee1 LIKE '%$consignee1%' AND id='$certificateid' AND isdraft='Y'";

      }
      
      //certificateid
      if($certType=='' && $country=='' && $exporter1=='' && $certificateid!='' && $consignee1==''){

        $query= "SELECT * FROM `certificatemaster` WHERE branchid='$branchid' AND  confirm<>'Y' AND calculated<>'Y' AND created='Y' AND id='$certificateid' AND isdraft='Y'";

      }

      if($certType=='' && $country=='' && $exporter1=='' && $certificateid!='' && $consignee1!=''){

        $query= "SELECT * FROM `certificatemaster` WHERE branchid='$branchid' AND  confirm<>'Y' AND calculated<>'Y' AND created='Y' AND id='$certificateid' AND consignee1 LIKE '%$consignee1%' AND isdraft='Y'";
 
      }

      //consignee1
      if($certType=='' && $country=='' && $exporter1=='' && $certificateid=='' && $consignee1!=''){

        $query= "SELECT * FROM `certificatemaster` WHERE branchid='$branchid' AND  confirm<>'Y' AND calculated<>'Y' AND created='Y' AND consignee1 LIKE '%$consignee1%' AND isdraft='Y'";

      }

      //All
      if($certType!='' && $country!='' && $exporter1!='' && $certificateid!='' && $consignee1!=''){

        $query= "SELECT * FROM `certificatemaster` WHERE branchid='$branchid' AND certType='$certType' AND  confirm<>'Y' AND calculated<>'Y' AND created='Y' AND country='$country' AND exporter1 LIKE  '%$exporter1%' AND id='$certificateid' AND consignee1 LIKE '%$consignee1%' AND isdraft='Y'";

        
      }
      
      $query=$query." ORDER BY id DESC";
      //echo $query;
      $result = mysqli_query($con,$query);
      while($rows = mysqli_fetch_assoc($result)){
          $enquiryids=$rows['enquiryid'];
          $enquiryids= explode(",", $enquiryids);
          $exporter=$rows['exporter1'];
          // $importer=$rows['consignee'];
          $consignee=$rows['consignee1'];
          $country=$rows['country'];
          $portofdischarge=$rows['loading'];
          $applieddose=$rows['fumdosage'];
      ?>
   <div class="row tr" id="<?php echo $rows['id']; ?>">
      <div class="col-sm-1 td text-center" style="word-wrap:break-word;">
         <?php echo $rows['id']; ?>
      </div>
      <?php if($rows['created'] == "Y") { ?>
      <div class="col-sm-2 td text-center" style="word-wrap:break-word;">
         <p>Certificate No.</p>
         <?php echo $rows['certificateno']; ?>
         <BR><BR>
         <p>Sr. No.<?php echo $rows['srNo']; ?></p>
      </div>
      <?php } else{ ?>
        <div class="col-sm-2 td text-center" style="word-wrap:break-word;">
          <?php if($rows['performIn']=='wpm' && $rows['wpninserted']=='Y' ){ ?>
            <p>Sr. No.<?php echo $rows['srNo']; ?></p>
            <?php } elseif($rows['wpmcreated']=='Y'){ ?>
              Certificate Numbers.
              <?php
              $wpmmainid=$rows['id'];
              $query="SELECT certificateno FROM `certificatemaster` WHERE wpnrefno='$wpmmainid' AND certificateno<>''";
              $result2 = mysqli_query($con,$query);
              while($rowss = mysqli_fetch_assoc($result2)){
                echo '<br>';
                echo $rowss['certificateno'];
              }
              ?>
              
              
            <?php }else{ ?>
              <p>Sr. No.<?php echo $rows['srNo']; ?></p>
            <?php } ?>
        </div>
      <?php } ?>
      <div class="col-sm-7 td">
         <?php if($rows['created'] == "Y" && $rows['wpninserted'] !="Y") { ?>
         Name of Exporter: <?php echo $exporter; ?><BR>
         <?php if($rows['certType']!="AFAS"){ ?>
         Name of Consignee: <?php echo $consignee; ?> <BR>
         <?php } else{ ?>
         Name of Importer: <?php echo $consignee; ?> <BR>
         <?php } ?>
         <?php if($rows['certType']!="AFAS"){ ?>
         Country: <?php echo $country; ?><BR>
         <?php } ?>
         Port Of Discharge: <?php echo $portofdischarge; ?><BR>
         <!-- Applied Dosage : <?php echo $applieddose; ?> <BR> -->
         Date Of Issue: <?php echo date("d-m-Y", strtotime($rows['issuedate'])); ?><BR>
         Date of Fumigation : <?php echo date("d-m-Y",strtotime($rows['fumdate'])); ?><br>

         <?php
            if($rows['certType']=="MBR" || $rows['certType']=="AFAS"){
              ?>
         Performed In : <?php echo $rows['performIn']; ?> 
         <?php
            }elseif($rows['certType']=="ALP"){
              ?>
         Using : <?php echo $rows['performIn']; ?> 
         <?php } ?>
         <?php  }elseif($rows['wpmcreated'] == "Y"){ ?>
         Performed In : <?php echo $rows['performIn']; ?><br>
         Number of Wooden Packing Material : <?php echo $rows['totalwpm']; ?><br>
         Date of Fumigation : <?php echo $rows['fumdate']; ?><br>
         Place of Fumigation : <?php echo $rows['fumplace']; ?><br>
         Remaining Wooden Packing Material : <?php echo $rows['remwpm']; ?><br>
         <?php  }elseif($rows['wpninserted'] == "Y"){ ?>
          Name of Exporter: <?php echo $exporter; ?><BR>
          Name of Consignee: <?php echo $rows['consignee1']; ?><BR>
          Country: <?php echo $country; ?><BR>
         Number of Wooden Packing Material : <?php echo $rows['totalwpm']; ?><br>
         Date of Fumigation : <?php echo $rows['fumdate']; ?><br>
         Place of Fumigation : <?php echo $rows['fumplace']; ?><br>
         WPM refered to : <?php echo $rows['wpnrefno']; ?><br>
         <?php }else{ 
            if($rows['certType']=="MBR" || $rows['certType']=="AFAS"){
             ?>
         Performed In : <?php echo $rows['performIn']; ?> 
         <?php
            }elseif($rows['certType']=="ALP"){
              ?>
         Using : <?php echo $rows['performIn']; ?> 
         <?php } ?>
         <?php  } ?>
      </div>
      <div class="col-sm-1 td text-center">
         <?php echo $rows['certType']; ?>
      </div>
      <div class="col-sm-1 td">
         <p align="center">
            <?php  
               if($rows['created'] == "Y" ){
               ?>
            <a class="btn btn-sm btn-block btn-primary"  onclick="printer_<?php echo $rows['certType']; ?>(<?php echo $rows['id']; ?>,<?php echo $rows['branchid'];?>)" >View PDF</a>
            <?php } ?>
            <?php 
               if($rows['created'] == "" ){
                 if($rows['certType']=="ALP"){ ?>
                    <a class="btn btn-sm btn-block btn-primary" href="certificate/alpcertificate.php?id=<?php echo $rows['id']; ?>&type=<?php echo $rows['certType']; ?>&performIn=<?php echo $rows['performIn']; ?>&create=true&draft=<?php echo $rows['isdraft'];?>" >Create</a> 
                    <a class="btn btn-sm btn-block btn-danger" onclick="deleterow(<?php echo $rows['id'];?>,'<?php echo $rows['certType'];?>','<?php echo $rows['performIn'];?>')" >Delete</a> 
            <?php 
               }elseif($rows['certType']=="MBR") { ?><?php echo $user['user_id'];?>
            <?php if($rows['performIn']=='wpm'){ 
               if($rows['wpmcreated']=='Y'){ ?>
            <button class="btn btn-sm  btn-block btn-primary"  onclick="addrow(<?php echo $rows['id'];?>,<?php echo $rows['totalwpm']; ?>,<?php echo $rows['remwpm']; ?>)">ADD</button>
            <button class="btn btn-sm  btn-block btn-warning"  onclick="editrow(<?php echo $rows['id'];?>,'<?php echo $rows['fumplace']; ?>','<?php echo $rows['fumdate']; ?>','<?php echo $rows['totalwpm']; ?>','<?php echo $rows['remwpm']; ?>')">Edit</button>

            <?php
              if ($rows['totalwpm']!=$rows['remwpm']) {
                if ($role =='admin') {

            ?>
            <a class="btn btn-sm btn-block btn-danger"  onclick="editQty(<?php echo $rows['id'];?>,'<?php echo $rows['totalwpm'];?>','<?php echo $rows['remwpm'];?>','<?php echo $rows['certType'];?>','<?php echo $rows['performIn'];?>')" >Edit Cert Qty</a>

            <?php
              }    }

            ?>
            <!-- data-toggle="modal" data-target="#myModalqty" -->
            <?php if($rows['remwpm']==0){ ?>
              <button class="btn btn-success  btn-block btn-sm"  data-id="<?php echo $rows['id']; ?>" data-enquiryid="<?php echo $rows['enquiryid']; ?>" data-certType="<?php echo $rows['certType']; ?>" onclick="confirmcertificate(<?php echo $rows['id']; ?>,'<?php echo $rows['certType']; ?>','<?php echo $rows['performIn']; ?>')">Confirm</button>
            <?php } ?>
            <?php  }elseif($rows['wpninserted'] == "Y"){ ?>
            <a class="btn btn-sm  btn-block btn-primary" href="certificate/mbrcertificate.php?id=<?php echo $rows['id']; ?>&type=<?php echo $rows['certType']; ?>&performIn=<?php echo $rows['performIn']; ?>&create=true&draft=<?php echo $rows['isdraft'];?>">Create</a>
            <a class="btn btn-sm  btn-block btn-info" onclick="copycert(<?php echo $rows['id'];?>,'<?php echo $rows['fumplace']; ?>','<?php echo $rows['fumdate']; ?>','<?php echo $rows['totalwpm']; ?>','<?php echo $rows['remwpm']; ?>','<?php echo $rows['wpnrefno']; ?>','<?php echo $rows['isdraft']; ?>')">Copy Cert</a>
            <?php
                if ($role =='admin') {
            
            ?>
                            <a class="btn btn-sm btn-block btn-danger" onclick="deleteWpm(<?php echo $rows['id'];?>,'<?php echo $rows['wpnrefno'];?>','<?php echo $rows['totalwpm'];?>','<?php echo $rows['certType'];?>','<?php echo $rows['performIn'];?>')" >Delete</a>

            <?php
                }
            ?>

            <?php }else{ ?>
            <button class="btn btn-sm  btn-block btn-primary" onclick="addcertificate3(<?php echo $rows['id'];?>)" >Create</button>
            <?php
                if ($role =='admin') {
            
            ?>
            <a class="btn btn-sm btn-block btn-danger" onclick="deleterow(<?php echo $rows['id'];?>,'<?php echo $rows['certType'];?>','<?php echo $rows['performIn'];?>')" >Delete</a>
            
            <?php } } ?>
            <?php }else{ ?>
            <a class="btn btn-sm  btn-block btn-primary" href="certificate/mbrcertificate.php?id=<?php echo $rows['id']; ?>&type=<?php echo $rows['certType']; ?>&performIn=<?php echo $rows['performIn']; ?>&create=true&draft=<?php echo $rows['isdraft'];?>">Create</a>
            <?php
                if ($role =='admin') {
            
            ?>
            <a class="btn btn-sm btn-block btn-danger" onclick="deleterow(<?php echo $rows['id'];?>,'<?php echo $rows['certType'];?>','<?php echo $rows['performIn'];?>')" >Delete</a>
            <?php }} ?>
            <?php 
               }elseif($rows['certType']=="AFAS") { ?>
            <a class="btn btn-sm  btn-block btn-primary" href="certificate/afascertificate.php?id=<?php echo $rows['id']; ?>&type=<?php echo $rows['certType']; ?>&performIn=<?php echo $rows['performIn']; ?>&create=true&draft=<?php echo $rows['isdraft'];?>" >Create</a>
            <?php
                if ($role =='admin') {
            
            ?>
            <a class="btn btn-sm btn-block btn-danger" onclick="deleterow(<?php echo $rows['id'];?>,'<?php echo $rows['certType'];?>','<?php echo $rows['performIn'];?>')" >Delete</a> 
            <?php } } 
               }else{  
               if($rows['certType']=="ALP"){ ?>
            <a class="btn btn-sm  btn-block btn-warning" href="certificate/alpcertificate.php?id=<?php echo $rows['id']; ?>&type=<?php echo $rows['certType']; ?>&performIn=<?php echo $rows['performIn']; ?>&edit=true&draft=<?php echo $rows['isdraft'];?>" > Edit </a>
            <a class="btn btn-sm  btn-block btn-info" onclick="copyALP(<?php echo $rows['id'];  ?>,'<?php echo $rows['performIn']; ?>','<?php echo $rows['isdraft']; ?>')"> Copy Cert </a>

            <?php

              if ($role =='admin') {

            ?>

            <a class="btn btn-sm btn-block btn-danger" onclick="deleterow(<?php echo $rows['id'];?>,'<?php echo $rows['certType'];?>','<?php echo $rows['performIn'];?>')" >Delete</a> 

            <?php
                
              }
            
            ?>

            <?php 
               }elseif($rows['certType']=="MBR") { ?>
            <a class="btn btn-sm  btn-block btn-warning" href="certificate/mbrcertificate.php?id=<?php echo $rows['id']; ?>&type=<?php echo $rows['certType']; ?>&performIn=<?php echo $rows['performIn']; ?>&edit=true&draft=<?php echo $rows['isdraft'];?>" > Edit </a>
            <?php if($rows['wpninserted'] == "Y"){

              ?>
              <?php

                if ($role =='admin') {

              ?>
                <a class="btn btn-sm btn-block btn-danger" onclick="deleteWpm(<?php echo $rows['id'];?>,'<?php echo $rows['wpnrefno'];?>','<?php echo $rows['totalwpm'];?>','<?php echo $rows['certType'];?>','<?php echo $rows['performIn'];?>')" >Delete</a>
              <?php
                
                }
            
              ?>
              <?php
               }else{ ?>
              <a class="btn btn-sm  btn-block btn-info" onclick="copyMBR(<?php echo $rows['id'];  ?>,'<?php echo $rows['performIn']; ?>','<?php echo $rows['isdraft']; ?>')" > Copy Cert </a>
              <?php

                if ($role =='admin') {

              ?>

                <a class="btn btn-sm btn-block btn-danger" onclick="deleterow(<?php echo $rows['id'];?>,'<?php echo $rows['certType'];?>','<?php echo $rows['performIn'];?>')" >Delete</a> 

              <?php
                  
                }

              ?>
            <?php } ?>
            <?php 
               }elseif($rows['certType']=="AFAS") { ?>
            <a class="btn btn-sm  btn-block btn-warning" href="certificate/afascertificate.php?id=<?php echo $rows['id']; ?>&type=<?php echo $rows['certType']; ?>&performIn=<?php echo $rows['performIn']; ?>&edit=true&draft=<?php echo $rows['isdraft'];?>" > Edit </a>
            <a class="btn btn-sm  btn-block btn-info" onclick="copyAFAS(<?php echo $rows['id'];  ?>,'<?php echo $rows['performIn']; ?>','<?php echo $rows['isdraft']; ?>')" > Copy Cert </a>
            <?php

              if ($role =='admin') {

            ?>

              <a class="btn btn-sm btn-block btn-danger" onclick="deleterow(<?php echo $rows['id'];?>,'<?php echo $rows['certType'];?>','<?php echo $rows['performIn'];?>')" >Delete</a> 

            <?php
                
              }

            ?>
            <?php } ?>
            <button class="btn btn-success  btn-block btn-sm"  data-id="<?php echo $rows['id']; ?>" data-enquiryid="<?php echo $rows['enquiryid']; ?>" data-certType="<?php echo $rows['certType']; ?>" onclick="confirmcertificate(<?php echo $rows['id']; ?>,'<?php echo $rows['certType']; ?>','<?php echo $rows['performIn']; ?>')">Confirm</button>
            <?php } ?>
         </p>
      </div>
   </div>
   <?php 
      } 
      ?>
   <br><br><br><br><br>
</div>
</div>
</div>
<button class="btn" style="font-size:25px;border-radius:50px;width:60px;height:60px;position: fixed;
   bottom: 40px; right: 40px;background:#eb2f06;color:#fff;" data-toggle="modal" data-target="#myModal">+</button>
<div id="myModal" class="modal fade" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title" align="center">Add Certificate</h4>
         </div>
         <div class="modal-body">
            <div class="form-group row">
               <div class="col-sm-4" >Select Certificate</div>
               <div class="col-sm-6">
                  <select type="select" data-role="select" data-name='certType[]' id='certType' class="certType form-control input-sm">
                     <option value="Select">Select</option>
                     <option value="ALP">ALP</option>
                     <option value="MBR">MBR</option>
                     <option value="AFAS">AFAS</option>
                  </select>
               </div>
               <br> <br> 
               <div class="col-sm-4" >Certificate Type</div>
               <div class="col-sm-6">
                  <select type="select" data-role="select" data-name='toperformin' id='toperformin' class="toperformin form-control input-sm" readonly>
                  </select>
               </div>
               <br> <br> 
               <div class="col-sm-4">Draft?</div>
               <div class="col-sm-6">
                  <select type="select" data-role="select" data-name="draft" id="draft" class="draft form-control input-sm">
                    <option value="" selected>No</option>
                    <option value="Y">Yes</option>
                  </select>
               </div>
               <br> <br><br>
            </div>
         </div>
         <div class="modal-footer">
            <div class="col-sm-4"></div>
            <div class="col-sm-8">
               <button class="btn btn-success btn-block btn-sm" id="addcertificate" onclick="addcertificate2();">Add Certificate</button>
            </div>
         </div>
      </div>
   </div>
</div>

<div id="myModal2" class="modal fade" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title" id="modal-title" align="center">Add Certificate</h4>
         </div>
         <div class="modal-body">
            <input type="text" data-role="text" data-name="id" id="rowid" class="hidden" > 
            <div class="col-sm-12">
               <div class="col-sm-1"></div>
               <div class="col-sm-4">
                  <b>Date of Fumigation:</b>
               </div>
               <div class="col-sm-5">
                  <div class="form-group">
                     <input type="date" data-role="date" id="fumdate" data-name="fumdate" value="<?php echo $rows['fumdate']; ?>" class="form-control">
                  </div>
               </div>
               <div class="col-sm-1"></div>
            </div>
            <div class="col-sm-12">
               <div class="col-sm-1"></div>
               <div class="col-sm-4"><b>Place of Fumigation</b></div>
               <div class="col-sm-5">
                  <div class="form-group"><input type="text" data-role="text" id="fumplace" data-name="fumplace" class="form-control" maxlength="70"></div>
               </div>
               <div class="col-sm-1"></div>
            </div>
            <div class="col-sm-12">
               <div class="col-sm-1"></div>
               <div class="col-sm-4"><b>Total WPM<br> (In Numbers)</b></div>
               <div class="col-sm-5">
                  <div class="form-group">
                    <input type="number" data-role="number" id="totalwpm"  data-name="totalwpm" class="form-control" maxlength="20"></div>
                  </div>
               <div class="col-sm-1"></div>
            </div>
         </div>
         <br><br><br><br><br><br><br>
         <?php echo $rows['id']; ?>
         <div class="modal-footer">
            <div class="col-sm-4"></div>
            <div class="col-sm-8">
               <button class="btn btn-success btn-block btn-sm" id="addcertificate2" onclick="addcertificate4();">Add Certificate</button>
               <button class="btn btn-success btn-block btn-sm" id="addcertificate3" onclick="addcertificate6();">Edit Certificate</button>
            </div>
         </div>
      </div>
   </div>
</div>

<div id="myModal4" class="modal fade" role="dialog">
   <div class="modal-dialog modal-lg">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title" align="center">Copy Certificate</h4>
         </div>
         <div class="modal-body">
            <input type="text" data-role="text" data-name="id" id="id" class="hidden" >
            <div class="form-group row">
               <div class="col-sm-3" >Select Certificate Number</div>
               <div class="col-sm-9">
                  <select type="select" data-role="select" data-name='certificateno' id='certificateno' class="form-control input-sm select-js1">
                     <option value="Select">Select</option>
                     <?php
                        $result10=mysqli_query($con, "SELECT id, certificateno,exporter1,country FROM certificatemaster WHERE wpninserted='Y' AND certificateno<>''");
                          while($rows10=mysqli_fetch_assoc($result10)) {
                              echo '<option value="'.$rows10['id'].'">'.$rows10['certificateno']. ' || ' .$rows10['exporter1']. ' || ' .$rows10['country'].'</option>';
                          }
                        ?>
                  </select>
               </div>
               <br> <br><br>
            </div>
         </div>
         <div class="modal-footer">
            <div class="col-sm-4"></div>
            <div class="col-sm-8">
               <button class="btn btn-success btn-block btn-sm" id="addcertificate" onclick="copycertificate();">Copy Certificate</button>
            </div>
         </div>
      </div>
   </div>
</div>

<div id="myModal3" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" align="center">Add Certificate</h4>
      </div>
      <div class="modal-body">
        <input type="text" data-role="text" data-name="id" id="rowid" class="hidden">
        <h5 class="modal-title"><b>Total Quntity : </b><input type="number" data-name="inp-qty" id="inp-qty"
            style="border: none" readonly></h5>
        <p id="remqty"><b>Remaining Quntity : </b><input type="number" data-name="inp-remqty" id="inp-remqty"
            style="border: none" readonly> </p>
        <div class="col-sm-12">
          <div class="col-sm-1"></div>
          <div class="col-sm-5"><b>Enter Quntity for certificate</b></div>
          <div class="col-sm-4">
            <div class="form-group">
              <input type="number" data-role="number" value="0" data-name="certqty" id="certqty"
                onkeyup="qtyentry(this)" class="form-control" maxlength="10">
            </div>
          </div>
          <div class="col-sm-1"></div>
        </div>
      </div>
      <br><br><br><br>
      <?php echo $rows['id']; ?>
      <div class="modal-footer">
        <div class="col-sm-4"></div>
        <div class="col-sm-8">
          <button class="btn btn-success btn-block btn-sm" id="addcertificate" onclick="addcertificate5();">Add
            Certificate</button>
        </div>
      </div>
    </div>
  </div>
</div>



<!--------------------------Certificate Adjust Qty Model Start--------------------------->
<div class="modal fade" id="myModalqty" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <input type="hidden" id="inp-itemid">
                <h5 class="modal-title">WPM refered to : <input type="text" id="inp-WpmRef"
                        style="border: none" readonly="readonly"></h5>
                <p>Total Number of Wooden Packing Material : <input type="text" id="inp-TotalQty" style="border: none" readonly="readonly"> </p>
                <p>Total Number of Remaining Wooden Packing Material : <input type="text" id="inp-TotalRemWpm" style="border: none" readonly="readonly"> </p>
                <p align="center" style="color:red; font-weight:bold">If Adjust Qty Will Be 0 (Zero) Certificate Are Deleted. </p>

            </div>
            <div class="modal-body">
                <div class="row">
                    <table class="table table-list" id="table">
                        <thead>
                            <tr>
                                <th>Sr No</th>
                                <th style="width:25%">Certificate No</th>
                                <th style="width:30%">Certificate/Sr No. Quantity</th>
                                <th>Certificate Qty Declared :</th>
                                <th>Adjust Qty In Plus(+) Or Minus (-)</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
                <!-- <div>
                    <button type="button" class="btn btn-primary btn-block" onclick="addbatch()">Add Batch</button>
                </div> -->
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary btn-block btn-success"
                    onclick="savetobatchno()">Submit</button>
            </div>
        </div>
    </div>
</div>
  <?php 
    include('footer.php');
  ?>
<script>

  function editQty(e, totalwpm, remwpm, certType, performIn) {

    $('#table tbody').empty();

    var WpmRef   = e;
    var totalwpm = totalwpm;
    var remwpm   = remwpm;

    $("#inp-WpmRef").val(WpmRef);

    $("#inp-TotalQty").val(totalwpm);

    $("#inp-TotalRemWpm").val(remwpm);

    if (WpmRef) {

      $.ajax({

        type: "POST",

        data: 'WpmRef=' + WpmRef,

        url: 'api/selectCertificate.php',

        cache: false,

        success: function (res) {

          if (res.status == 'success') {

            var json = res.json;
            // console.log(json);
            json.forEach(myFunction);

            function myFunction(item, index, arr) {

              $('#myModalqty').modal();

              $("#table tbody").append('<tr class="batchno"><td align="center" style="display:none;"><input type="text"class="certId form-control" value=' + item.id + ' readonly="readonly"></td><td align="center"><input type="text"class="certSrNo form-control" value=' + item.srNo + ' readonly="readonly"></td><td align="center"><input type="text" class="certNo form-control" value="' + item.certificateno + '"  readonly="readonly"></td><td align="center"><input type="text" class="certificateQty form-control" value="' + item.certificateQty + '"  readonly="readonly"></td><td align="center"><input type="number" class="totalwpm form-control"  value=' + item.totalwpm + ' readonly="readonly"></td><td align="center"><input type="number" id="transferqty" class="transferqty form-control"  onkeyup="batchentry(this)" value=' + item.totalwpm + ' data-val=' + item.totalwpm + ' ></td></tr>')

            }

          }

          $('#myModalqty').modal();

        }

      })

    }

  }

  //-----------------Check Model Quantity----------------------------//
  function batchentry(e) {

      var qty1      = 0;

      var totalwpm  = $(e).attr('data-val');

      var AdjustQty = $(e).val();

      $('.transferqty').each(function () {

          var qty100 = parseFloat($(this).val());

          if (isNaN(qty100)) {

              qty100 = 0;

              $(e).val(0);

          }

          qty1 = qty1 + qty100;

      })


      var qty = parseFloat($("#inp-TotalQty").val());
      
      if (qty1 > qty) {

          alert('Adjust Quantity Total must be less than Total Number of Wooden Packing Material Quantity ');
          $(e).val(totalwpm);
      } 

      if (AdjustQty == 0) {
          alert('If Adjust Qty Will Be 0 (Zero) Certificate Are Deleted.');
      }
      
  }



  //---------------Save Batch--------------------//
  function savetobatchno() {

    var WpmRef = $("#inp-WpmRef").val();

    var TotalQty = $("#inp-TotalQty").val();

    var TotalRemWpm = $("#inp-TotalRemWpm").val();

    var arr = [];
    var valid = true;
    var qty1 = 0;

    $('.transferqty').each(function () {

      var qty100 = parseFloat($(this).val());

        if (isNaN(qty100)) {

            qty100 = 0;

            $(e).val(0);

        }

        qty1 = qty1 + qty100;

    })

    if (qty1 < qty) {
      alert('Quntity must be less than item Quntity');
      valid = valid * false;
    } else {
      valid = valid * true;
    }

    var qty = parseFloat($("#inp-TotalQty").val());
      
    if (qty1 > qty) {

      alert('Adjust Quantity Total must be less than Total Number of Wooden Packing Material Quantity ');

      valid = valid * false;

    } else {

      valid = valid * true;

    }

    

    $("#table tbody tr").each(function () {

      if ($(this).find('td input.certId').val() != '' && $(this).find('td input.transferqty').val() != '') {

        arr.push({

          certId: $(this).find('td input.certId').val(),
          certSrNo: $(this).find('td input.certSrNo').val(),
          certNo: $(this).find('td input.certNo').val(),
          totalwpm: $(this).find('td input.totalwpm').val(),
          transferqty: $(this).find('td input.transferqty').val()

        });

        valid = valid * true;

      } else {

        alert("Something went wrong!")
        valid = valid * false;

      }

    })

    

    if (arr != '') {

      valid = valid * true;

    } else {

      valid = valid * false;

    }
    if (valid) {
      $.ajax({
        type: "POST",
        data: 'WpmRef=' + WpmRef + '&TotalQty=' + TotalQty + '&TotalRemWpm=' + TotalRemWpm + '&transferbatch=' + JSON.stringify(arr),
        url: 'api/adjustQty.php',
        cache: false,
        success: function (res) {
          if (res.status == 'success') {
            alert('Quantity Adjust SuccessFully');
            window.location = "draft.php";
          }
        }
      })
    }
  }


  // function copyALP(id, performIn, isdraft) {

  //   Swal.fire({
  //     title: 'Are you sure?',
  //     text: "You want To Add This Certificate From Draft !",
  //     icon: 'warning',
  //     showCancelButton: true,
  //     confirmButtonColor: '#3085d6',
  //     cancelButtonColor: '#d33',
  //     confirmButtonText: 'Yes, Add It!'
  //   }).then((result) => {

  //     if (result.value) {

  //       var certids = ['ALP'];


  //       $.ajax({
  //         type: "POST",
  //         data: {
  //           certid: certids,
  //           toperformin: performIn,
  //           draft: 'Y'
  //         },
  //         url: 'api/addcertificate.php',
  //         cache: false,
  //         success: function (res) {
  //           // alert(res.status);
  //           if (res.status == 'success') {

  //             window.location = "certificate/alpcertificate.php?id=" + id + "&type=ALP&performIn=" + performIn + "&copy=true&draft=Y&newId="+res.newId;


  //           }
  //         }
  //       })

  //     } else {
  //       swal('Ok ! This Certificate is Keep In Alp', '', 'error');
  //       $('#draftInGet>option:eq(1)').prop('selected', true);
  //       window.location = "certificate/alpcertificate.php?id=" + id + "&type=ALP&performIn=" + performIn + "&copy=true&draft=" + isdraft;

  //     }

  //   })

  //   }


  //   function copyMBR(id,performIn,isdraft){
  //   Swal.fire({
  //     title: 'Are you sure?',
  //     text: "You want To Add This Certificate From Draft !",
  //     icon: 'warning',
  //     showCancelButton: true,
  //     confirmButtonColor: '#3085d6',
  //     cancelButtonColor: '#d33',
  //     confirmButtonText: 'Yes, Add It!'
  //   }).then((result) => {

  //     if (result.value) {

  //       var certids = ['MBR'];


  //       $.ajax({
  //         type: "POST",
  //         data: {
  //           certid: certids,
  //           toperformin: performIn,
  //           draft: 'Y'
  //         },
  //         url: 'api/addcertificate.php',
  //         cache: false,
  //         success: function (res) {
  //           // alert(res.status);
  //           if (res.status == 'success') {

  //             window.location = "certificate/mbrcertificate.php?id=" + id + "&type=MBR&performIn=" + performIn + "&copy=true&draft=Y&newId="+res.newId;


  //           }
  //         }
  //       })

  //     } else {
  //       swal('Ok ! This Certificate is Keep In Alp', '', 'error');
  //       $('#draftInGet>option:eq(1)').prop('selected', true);
  //       window.location = "certificate/mbrcertificate.php?id="+id+"&type=MBR&performIn="+performIn+"&copy=true&draft="+isdraft;
  //     }

  //   })
  //   }

  //   function copyAFAS(id,performIn,isdraft){
  //   Swal.fire({
  //     title: 'Are you sure?',
  //     text: "You want To Add This Certificate From Draft !",
  //     icon: 'warning',
  //     showCancelButton: true,
  //     confirmButtonColor: '#3085d6',
  //     cancelButtonColor: '#d33',
  //     confirmButtonText: 'Yes, Add It!'
  //   }).then((result) => {

  //     if (result.value) {

  //       var certids = ['AFAS'];


  //       $.ajax({
  //         type: "POST",
  //         data: {
  //           certid: certids,
  //           toperformin: performIn,
  //           draft: 'Y'
  //         },
  //         url: 'api/addcertificate.php',
  //         cache: false,
  //         success: function (res) {
  //           // alert(res.status);
  //           if (res.status == 'success') {

  //             window.location = "certificate/afascertificate.php?id=" + id + "&type=AFAS&performIn=" + performIn + "&copy=true&draft=Y&newId="+res.newId;


  //           }
  //         }
  //       })

  //     } else {
  //       swal('Ok ! This Certificate is Keep In AFAS', '', 'error');
  //       $('#draftInGet>option:eq(1)').prop('selected', true);
  //       console.log('ababab');
  //       window.location = "certificate/afascertificate.php?id="+id+"&type=AFAS&performIn="+performIn+"&copy=true&draft=";
  //     }

  //   })
  //   }

    function copyALP(id,performIn,isdraft){
      if(confirm("Are you sure to copy this certificate to create new ALP Certificate")){
        window.location = "certificate/alpcertificate.php?id="+id+"&type=ALP&performIn="+performIn+"&copy=true&draft="+isdraft;
      }
    }
    function copyMBR(id,performIn,isdraft){
      if(confirm("Are you sure to copy this certificate to create new MBR Certificate")){
        window.location = "certificate/mbrcertificate.php?id="+id+"&type=MBR&performIn="+performIn+"&copy=true&draft="+isdraft;
      }
    }
    function copyAFAS(id,performIn,isdraft){
      if(confirm("Are you sure to copy this certificate to create new AFAS Certificate")){
        window.location = "certificate/afascertificate.php?id="+id+"&type=AFAS&performIn="+performIn+"&copy=true&draft="+isdraft;
      }
    }

    function submit(){
      var certType=$("#certType1").val();
      var country=$('#country').val();
      var exporter1=$('#exporter1').val();
      var certificateid=$('#certificateid').val();
      var consignee1=$('#consignee1').val();

      window.location= "draft.php?country="+country+"&exporter1="+exporter1+"&certificateid="+certificateid+"&certType="+certType+"&consignee1="+consignee1;
    }
    $(document).ready(function(){
      $('.select-js1').select2({width: '100%'});
      $('.select').attr('style','width:100%!important;');
    });
    function copycertificate(){
     var copyfrom = $("#certificateno").val();
     var original = $("#id").val();
      window.location = "certificate/mbrcertificate.php?id="+copyfrom+"&type=MBR&performIn=wpm&wpmcopy=true&original="+original+"";
    }
    function copycert(id,fumplace,fumdate,totalwpm,remwpm,wpnrefno,draft){
      if(confirm("Are sure to copy wpm certificate?")){
      $("#id").val(id);
      $("#fumplace").val(fumplace);
      $("#fumdate").val(fumdate);
      $("#totalwpm").val(totalwpm);
      $("#myModal4").modal();
      }
    }
   function addcertificate3(e){
     var id=e;
     $("#rowid").val(id)
     $('#myModal2').modal();
     $("#addcertificate3").addClass("hidden");
   }
   $("#certType").change(function(){
     var option='';
     if($(this).val()=="AFAS"){
       option+='<option value="Select">Select</option>';
       option+='<option value="container">Container</option>';
       option+='<option value="container said to be loaded">Container said to be loaded</option>';
       option+='<option value="warehouse">Warehouse</option>';
       option+='<option value="vessel">Vessel</option>';
       option+='<option value="other">Other</option>';
       $("#toperformin").empty().append(option);
       $("#toperformin").removeAttr("readonly");
     }
     if($(this).val()=="MBR"){
       option+='<option value="Select">Select</option>';
       option+='<option value="container">Container</option>';
       option+='<option value="container said to be loaded">Container said to be loaded</option>';
       option+='<option value="warehouse">Warehouse</option>';
       option+='<option value="vessel">Vessel</option>';
       option+='<option value="wpm">WPM</option>';
       option+='<option value="other">Other</option>';
       $("#toperformin").empty().append(option);
       $("#toperformin").removeAttr("readonly");
       $("#toperformin").change(function(){
       
       });
     }
     if($(this).val()=="ALP"){
       option+='<option value="Select">Select</option>';
       option+='<option value="dosage using mt">Dosage Using MT</option>';
       option+='<option value="dosage using m3">Dosage Using M<sup>3</sup></option>';
       $("#toperformin").empty().append(option);
       $("#toperformin").removeAttr("readonly");
     }
     if($(this).val()=="Select"){
       option+='<option value="Select">Select</option>';
       $("#toperformin").empty().append(option);
       $("#toperformin").removeAttr("readonly");
     }
     
   })
   function deleterow(e,certType,performIn){
     if(confirm("Are you sure to delete?")){
      var id=e;
           $.ajax({
               type: "POST",
               data: 'id=' + id,
               url: 'api/deleterow.php',
               cache: false,
               success: function (res) {
                    if (res.status == 'success') {
                      // window.location = "draft.php";
                      alert('Certificate Deleted');

                      $('#' + id).fadeOut(1000, function () {
                        $('#' + id).remove();
                      });
                      

                   }
               }
           })
     }
   }

   function deleteWpm(e,wpnrefno,totalwpm,certType,performIn){
     if(confirm("Are you sure to delete?")){
      var id=e;
      var wpnrefno=wpnrefno;
      var totalwpm=totalwpm;

           $.ajax({
               type: "POST",
               data: 'id=' + id + '&wpnrefno='+wpnrefno+'&totalwpm='+totalwpm,
               url: 'api/deleteWpm.php',
               cache: false,
               success: function (res) {
                    if (res.status == 'success') {
                      // window.location = "draft.php";

                      alert('Certificate Deleted');
                      $('#' + id).fadeOut(1000, function () {
                        $('#' + id).remove();
                      });
                   }
               }
           })
     }
   }

   function addrow(id,totalwpn,remwpm){
     $("#inp-qty").val(totalwpn);
     $("#inp-remqty").val(remwpm);
     $("#rowid").val(id);
     $('#myModal3').modal();
   }
   function editrow(id,fumplace,fumdate,totalwpm,remwpm){
    $("#rowid").val(id);
    $("#fumplace").val(fumplace);
    $("#fumdate").val(fumdate);
    $("#totalwpm").val(totalwpm);
    $("#modal-title").text("Edit Certificate");
    $("#addcertificate2").addClass("hidden");
    $("#myModal2").modal();
    var issuedwpm=totalwpm-remwpm;
    $("#totalwpm").change(function(){
      if($(this).val()<issuedwpm){
        alert("Wpn Certificates issued for :"+issuedwpm);
        $(this).val(totalwpm);
      }
    })
   }
    function qtyentry(e){
      var remwpm=parseFloat($("#inp-remqty").val());
      var qtyent=parseFloat($("#certqty").val());
      if(qtyent>remwpm){
        alert("WPM Quntity must be less than"+remwpm);
        $("#certqty").val("0");
      }
    }
    $('.browse-btn').on('click',function(){
      $(this).parent().find('input').click();
    });
    $('.real-input').on('change',function(){
      $(this).parent().find('span').text($(this).val().replace(/C:\\fakepath\\/i, ''));
    
    });
    function confirmcertificate(id,certType,performIn) {
        if (confirm("Are You Sure TO Confirm Certification?")) {
            $.ajax({
                type: "POST",
                data: 'id=' + id ,
                url: 'api/confirmcertificate.php',
                cache: false,
                success: function (res) {
                      if (res.status == 'success') {
                        // window.location = "draft.php";
                        alert('Certificate Confirm');

                        $('#' + id).fadeOut(1000, function () {
                          $('#' + id).remove();
                        });

                    }
                }
            })
        }
    }
    function addcertificate5() {
      var id=$("#rowid").val();
      var remwpm=parseFloat($("#inp-remqty").val());
      var qtyent=parseFloat($("#certqty").val());
      if(qtyent>remwpm){
      alert("WPM Quntity must be less than: "+remwpm);
      $("#certqty").val("0");
      }else if(qtyent==0){
        alert("Plese Enter Quntity");
      }else if(!qtyent){
        alert("Plese Enter Quntity");
      }else{
      if(id){
      $('#myModal3').hide();
      $.ajax({
              type: "POST",
              data: 'id=' + id+'&remwpm='+remwpm+'&qtyent='+qtyent,
              url: 'api/addcertificate3.php',
              cache: false,
              success: function (res) {
                  if (res.status == 'success') {
                  
                  window.location.reload();
                  }
              }
          }) 
      }
      }
    }
    function addcertificate4(){
        var valid = true;
        if (checker('myModal2') != false){
            valid = valid * true;
        }else{
            valid = valid * false;
        }
        if(valid){
            var data=checker('myModal2');
            var datastr = JSON.stringify(data);
            
            $.ajax({
                type: "POST",
                data: {
                    data: datastr
                  },
                url: 'api/addcertificate2.php',
                cache: false,
                success: function (res) {
                  if (res.status == 'success'){
                    $('#myModal2').hide();
                      window.location.reload();
                  }else if(res.status == 'duplicate'){
                    if(confirm('Fumigation already performed on date and place....Have you performed Fumigation Again?')){
                      duplotwpm();
                    }else{
                      $('#myModal2').hide();
                      window.location.reload();
                    }
                  }
                }
            }) 
        }
    }
    function duplotwpm(){
      var valid = true;
        if (checker('myModal2') != false){
            valid = valid * true;
        }else{
            valid = valid * false;
        }
        if(valid){
            var data=checker('myModal2');
            var datastr = JSON.stringify(data);
            $('#myModal2').hide();
            $.ajax({
                type: "POST",
                data: {
                    data: datastr
                  },
                url: 'api/duplotwpm.php',
                cache: false,
                success: function (res) {
                  if (res.status == 'success'){
                      window.location.reload();
                  }
                }
            }) 
        }
    }
    function addcertificate6() {
          var valid = true;
          if (checker('myModal2') != false) {
              valid = valid * true;
          } else {
              valid = valid * false;
          }
          if(valid){
              var data=checker('myModal2');
              var datastr = JSON.stringify(data);
              $('#myModal2').hide();
              $.ajax({
                  type: "POST",
                  data: {
                      data: datastr
                    },
                  url: 'api/addcertificate6.php',
                  cache: false,
                  success: function (res) {
                  // alert(res.status);
                  if (res.status == 'success') {
                  
                      window.location.reload();
                  
                  }
                  }
              }) 
          }
      
    }
    function addcertificate1(e) {
      var valid = true;
      if (checker('myModal') != false) {
        valid = valid * true;
      } else {
        valid = valid * false;
      }
      if (valid) {
        var data=checker('myModal');
        var datastr = JSON.stringify(data);
        // alert(datastr);
        $.ajax({
            type: "POST",
            data: {
              data: datastr
            },
            url: 'api/addcertificate.php',
            cache: false,
            success: function (res) {
              if (res.status == 'success') {
              
                  window.location.reload();
                
              }
            }
          })
        }
      }
    
    function addcertificate2() {
      var valid = true;
      if (checker('myModal') != false) {
        valid = valid * true;
      } else {
        valid = valid * false;
      }
      if (valid) {
        var data = checker('myModal');
        var datastr = JSON.stringify(data);
        $('#myModal').hide();
        var certids = [];
        var performIn = [];
        $.each($(".certType option:selected"), function () {
          certids.push($(this).val());
        });
        var enqids = 0;
        $.ajax({
          type: "POST",
          data: {
            certid: certids,
            toperformin: $(".toperformin option:selected").val(),
            draft: $(".draft option:selected").val()
          },
          url: 'api/addcertificate.php',
          cache: false,
          success: function (res) {
            // alert(res.status);
            if (res.status == 'success') {

              if ($(".draft option:selected").val() == 'Y') {

                window.location = "draft.php";

              } else {

                window.location.reload();

              }


            }
          }
        })
      }

    }
     
    function workcontract(){}
    function setidtomidal(e){
     var enquiryid=$(e).data('enquiryid');
     $('#inp-enquiryidset').val(enquiryid);
     }
     function upload(e){
     var file=$(e).parent().parent().find('input')[0].files[0];
     var enquiryid=$(e).data('enquiryid');
     var formData = new FormData();
   
     formData.append('offer',file);
     formData.append('enquiryid',enquiryid);
     $.ajax({
             type: "POST",
             data: formData,
             url: 'api/offer.php',
             cache: false,
             processData: false,
             contentType: false,
             success: function (res) {
                 window.location.reload();
             }
         })
     }
    function printer_ALP(certid,branchid) {
      var jsondata = $.parseJSON($.ajax({
            type: "POST",
           data: {
             certid: certid
           },
           url: 'api/addjson.php',
           cache: false,
           success: function (res) {
           //jsondata =JSON.stringify(res.json);
            },
           dataType: "json", 
           async: false
       }).responseText);
       jsondata =JSON.stringify(jsondata);
       data1 = JSON.parse(jsondata);  
       json1 = data1.json;   
   
       var brjsondata = $.parseJSON($.ajax({
               type: "POST",
               data: {
                 certid: certid,branchid:branchid
               },
               url: 'api/getbrjson.php',
               cache: false,
               success: function (res) {
               //jsondata =JSON.stringify(res.json);
               },
               dataType: "json", 
               async: false
           }).responseText);
       brjsondata =JSON.stringify(brjsondata);
       brdata = JSON.parse(brjsondata);  
       brjson = brdata.branchjson;   
   
   
       //alert(data1.status);
       var fumdate = new Date(json1.fumdate);
       var issuedate = new Date(json1.issuedate);
           
       fumdate = ('0' + fumdate.getDate()).slice(-2) + "/" + ('0' + (fumdate.getMonth()+1)).slice(-2) + "/" + fumdate.getFullYear();
       issuedate = ('0' + issuedate.getDate()).slice(-2)+ "/" +  ('0' + (issuedate.getMonth()+1)).slice(-2) + "/" + issuedate.getFullYear();
   
       var fullDate = new Date();
       var twoDigitMonth = ((fullDate.getMonth().length + 1) === 1) ? (fullDate.getMonth() + 1) : '' + (fullDate.getMonth() + 1);
       var currentDate = fullDate.getDate() + "/" + twoDigitMonth + "/" + fullDate.getFullYear();
       var content=[]; 
   
       certno = json1.certificateno;
                       dd = {
                       pageSize: 'A4',
                       pageMargins: [40, 30, 30, 40],
                       footer: function (currentPage, pageCount) {
                           return {
                             
                           };
   
                          },
       content: [
   	    '\n',
               {
                   text: 'PEST MORTEM (INDIA) PRIVATE LIMITED',fontSize: 14,bold: true,alignment: 'center',margin:[0,5,0,0]
               },
               {
                   text: '(Approved by National Plant Protection Organization, Government of India)',
                    fontSize: 8, bold: true, alignment: 'center',
               },
               {
                   text: 'AN ISO 9001:2008 CERTIFIED COMPANY',
                    fontSize: 8,alignment: 'center',margin:[0,0,0,10]
               },
               
   	        {     margin:[0,-4,0,0],
                   table:{
                   widths:[510],
                   body:[
                       [{margin:[-5,-3,-5,-3],
                         border:[0,0,0,0],
       	                table:{
       	                widths:["*"],
       	                body:[
       	                   [{text: 'FUMIGATION CERTIFICATE',fontSize: 12,bold: true,alignment: 'center',margin:[0,0,0,0],border:[0,0,0,0],},]
       	                    ]
           	            },
   	            },
   	            ],
       	        [{
       	            margin:[-5,-3,-5,-3],
       	            table:{
       	                widths:['auto',10,'auto','*'],
       	                body:[
           	            [
           	            {stack: [{text: [
                           {text:'PEST MORTEM (INDIA) PRIVATE LIMITED\n',style: 'left',bold:1},
                           {text:brjson.address+'\n',style:'left',bold:1},
                           {text:'Dte. PPQS Regd. No.\t030/ALP/DATED : 20.04.2012  ',style:'left',bold:1},
                           
                           ],}],border:[0,0,0,0]
           	            },
       	                {
       	                stack: [{text: [
                           {text:'\n',style:'left'},
                           {text:'\n',style:'left'},
                           {text:'\n',style:'left'},
                           ],}],border:[0,0,1,0]   
       	                },
       	                {
       	                stack: [{text: [
                           {text:'Treatment Cert. No. : \n',style: 'left',bold:1},
                           {text:'\n',style:'left'},
                           {text:'Date of Issue :\n',style: 'left',bold:1}
                           ],}],border:[0,0,0,0]   
       	                },
       	                {
       	                stack: [{text: [
                           {text:certno+'\n',style: 'left',bold:1},
                           {text:'\n',style:'left'},
                           {text:issuedate+'\n',style: 'left',bold:1}
                           ],}],border:[0,0,0,0]   
       	                },
       	                ]
       	                    ]
       	            },layout: {
           				hLineColor: function (i, node) {
           					return (i === 0 || i === node.table.body.length) ? '#778899' : '#778899';
           				},
           				vLineColor: function (i, node) {
           					return (i === 0 || i === node.table.widths.length) ? '#778899' : '#778899';
           				},
           	        }
       	        }],
       	        [
       	           {
       	                stack: [{text: [
       	                    {text:'\tThis is to certify that the goods described below were treated in accordance with the fumigation treatment requirements of importing country ',fontSize: 8,italics: true},
                               {text: json1.country.toUpperCase(), italics: false, fontSize: 9,bold:1},
                               {text:' and declared that the consignment has been verified free of impervious surfaces/layers such as plastic wrapping or laminated plastic films, lacquered or painted surfaces, aluminium foil, tarred or waxed papers etc. that may adversely effect the penetration of the fumigant, prior to fumigation.',fontSize: 8,italics: true},
                               {text:'\nThe Certificate is valid for the consignments shipped within 21 days from the date of completion of fumigation',style: '',color:'',fontSize: 8,},
                           ],}],border:[1,1,1,1]   
       	                }, 
       	       ],
       	        [{
       	            margin:[-5,-3,-5,-3],
       	            table:{
       	                widths:[165,'*'],
       	                
       	                body:[
       	                [{text:'Details of Treatment\n',style:'left',colSpan: 2,bold:1},{},],
       	                [{text:'Name of Fumigant\n',style:'left'},{text:'ALUMINIUM PHOSPHIDE\n',style:'left'},],
       	                [{text:'Date of Fumigation\n',style:'left'},{text:fumdate+'\n',style:'left'},],
       	                [{text:'Place of Fumigation\n',style:'left'},{text:json1.fumplace+'\n',style:'left'},],
       	                [{text:'Dosage of Fumigant\n',style:'left'},{text:json1.fumdosage+' GMS / M.T\n',style:'left'},],
       	                [{text:'Duration of Fumigation (in days)\n',style:'left'},{text:json1.fumdurationdays+' / '+json1.fumdurationhrs+'\n',style:'left'},],
       	                [{text:'Average ambient humidity during \nfumigation (RH)\n',style:'left'},{text:json1.humidity+' %\n',style:'left'},],
       	                [{text:'Fumigation performed under gas tight sheets\n',style:'left'},{text:json1.fumperformed+'\n',style:'left',bold:1},],
       	                [{text:'Description of Goods\n',style:'left',colSpan: 2,bold:1},{text:'\n',style:'left'},],
       	                [{text:'Container Number (or numerical link).\n/Seal Number\n',style:'left'},
       	                {stack: [{text: [
       	                    {text:json1.containerno1+'\n',style:'left'},
       	                    {text:json1.containerno2+'\n',style:'left',},
       	                    {text:json1.containerno3+'\n',style:'left',},
       	                    {text:json1.containerno4+'\n',style:'left',},
       	                    ]}]}
       	                ],
       	                [{text:'Name & Address of Exporter\n',style:'left'},
       	                {stack: [{text: [
       	                    {text:json1.exporter1+'\n',style:'left'},
       	                    {text:json1.exporter2+'\n',style:'left',},
       	                    {text:json1.exporter3+'\n',style:'left',},
       	                    ]}]}
       	                    ],
                         [{text:'Name and Address of Importer\n',style:'left'},
       	                 {stack: [{text: [
       	                    {text:json1.consignee1+'\n',style:'left'},
       	                    {text:json1.consignee2+'\n',style:'left',},
       	                    {text:json1.consignee3+'\n',style:'left',},
       	                    ]}]}
       	                ],
                         [{text:'Name and Address of notified party\n',style:'left'},
       	                 {stack: [{text: [
       	                    {text:json1.notifiedparty1+'\n',style:'left'},
       	                    {text:json1.notifiedparty2+'\n',style:'left',},
       	                    {text:json1.notifiedparty3+'\n',style:'left',},
       	                    ]}]}
       	                ],
       	                
       	                [{text:'Type and description of cargo\n',style:'left'},
       	                 {stack: [{text: [
       	                    {text:json1.cargo1+'\n',style:'left'},
       	                    {text:json1.cargo2+'\n',style:'left',},
       	                    {text:json1.cargo3+'\n',style:'left',},
       	                    ]}]}
       	                ],
       	                [{text:'Quantity (MTs)/ No of packages/No\n of pieces',style:'left'},
       	                {stack: [{text: [
       	                    {text:json1.qty1+'\n',style:'left'},
                             {text:json1.qty2+'\n',style:'left'},
       	                    ]}]}
       	                ],
       	                [{text:'Description of packaging material\n',style:'left'},
       	                {stack: [{text: [
       	                    {text:json1.description+'\n',style:'left'},
       	                    {text:json1.description1+'\n',style:'left',},
       	                    ]}]}
       	                ],
           	            [{text:'Shipping mark or brand\n',style:'left'},
           	            {stack: [{text: [
       	                    {text:json1.shipingmark+'\n',style:'left'},
       	                    {text:json1.description2+'\n',style:'left',},
       	                    ]}]}
           	            ],
                         [{text:'Port & country Of Loading\n',style:'left'},
                         {text:json1.loading+'\n',style:'left'},],
                         [{text:'Port of Discharge\n',style:'left'},
                         {text:json1.entryport+'\n',style:'left'},],
                         [{text:'Name of the Vessel\n',style:'left'},
                         {text:json1.vesselname+'\n',style:'left'},],
                         [{text:'Additional Declaration\n',style:'left'},
           	            {stack: [{text: [
       	                    {text:json1.additional+'\n',style:'left'},
       	                    {text:json1.additional1+'\n',style:'left',},
                             {text:json1.additional2+'\n',style:'left',},
       	                    ]}]}
           	            ],
                         [{text:'Name &,Signature of Accredited\nFumigation Operator with seal & date/\nAccreditation Number\n',style:'left',bold:1},
                         {stack: [{text: [
                           {text:json1.accname+'\n',style:'left'},
       	                    {text:json1.accno+'\n',style:'left',},
                             {text:'Date:'+issuedate+'\n',style:'left',},
       	                    ]}]}
                             ],
       	                ],
       	            },layout: {
           				hLineColor: function (i, node) {
           					return (i === 0 || i === node.table.body.length) ? '#778899' : '#778899';
           				},
           				vLineColor: function (i, node) {
           					return (i === 0 || i === node.table.widths.length) ? '#778899' : '#778899';
           				},
           	        }
       	        }
       	       ],
       	       
   	        ]
   		},layout: {
   
   				hLineColor: function (i, node) {
   					return (i === 0 || i === node.table.body.length) ? '#778899' : '#778899';
   				},
   				vLineColor: function (i, node) {
   					return (i === 0 || i === node.table.widths.length) ? '#778899' : '#778899';
   				},
   	        }
   		
   		},
   		{text:'\n\n'},
   		
   		
   		],
   		styles: {
           left:{
               fontSize: 8,
       		alignment:'left',
           },
           right:{
               fontSize: 8,
       		alignment:'right',
           },
   		centerbold: {
   			fontSize: 8,
   			alignment:'center',
   			bold:1
   		},
   		center: {
   			fontSize: 8,
   			alignment:'center',
   		},
   		bn: {
   			fontSize: 8,
   			border:[0,0,0,0],
   		},
       }
   
                                  }
                pdfMake.createPdf(dd).open();
    }
    function printer_AFAS(certid,branchid) {
      var jsondata = $.parseJSON($.ajax({
            type: "POST",
           data: {
             certid: certid
           },
           url: 'api/addjson.php',
           cache: false,
           success: function (res) {
            },
           dataType: "json", 
           async: false
       }).responseText);
     jsondata =JSON.stringify(jsondata);
     data1 = JSON.parse(jsondata);  
     json1 = data1.json;      
     var brjsondata = $.parseJSON($.ajax({
             type: "POST",
             data: {
               certid: certid,branchid:branchid
             },
             url: 'api/getbrjson.php',
             cache: false,
             success: function (res) {
             },
             dataType: "json", 
             async: false
         }).responseText);
     brjsondata =JSON.stringify(brjsondata);
     brdata = JSON.parse(brjsondata);  
     brjson = brdata.branchjson;   
     var fumdate = new Date(json1.fumdate);
     var issuedate = new Date(json1.issuedate);
     var fumdate1 = new Date(json1.fumdate1);
      fumdate1 = ('0' + fumdate1.getDate()).slice(-2) + "/" + ('0' + (fumdate1.getMonth()+1)).slice(-2) + "/" + fumdate1.getFullYear();
      fumdate = ('0' + fumdate.getDate()).slice(-2) + "/" + ('0' + (fumdate.getMonth()+1)).slice(-2) + "/" + fumdate.getFullYear();
      issuedate = ('0' + issuedate.getDate()).slice(-2)+ "/" +  ('0' + (issuedate.getMonth()+1)).slice(-2) + "/" + issuedate.getFullYear();
     var fullDate = new Date();
     var twoDigitMonth = ((fullDate.getMonth().length + 1) === 1) ? (fullDate.getMonth() + 1) : '' + (fullDate.getMonth() + 1);
     var currentDate = fullDate.getDate() + "/" + twoDigitMonth + "/" + fullDate.getFullYear();
     var content=[]; 
     certno = json1.certificateno;
      dd = {
        pageSize: 'A4',
        pageMargins: [50, 30, 30, 40],
        footer: function (currentPage, pageCount) {
            return {
            };

          },
        content: [
                '\n',
                {
                    text: 'PEST MORTEM (INDIA) PRIVATE LIMITED',
                    fontSize: 14,
                          bold: true,
                          alignment: 'center',
                },
                {
                        text: '(Approved by National Plant Protection Organization, Government of India)',
                        fontSize: 8,
                          bold: true,
                          alignment: 'center',
                    },

                    {
                        text: brjson.address,
                        fontSize: 8,
                          alignment: 'center',bold:1,
                    },

                    {
                        text: 'AN ISO 9001:2008 CERTIFIED COMPANY',
                        fontSize: 8,
                          alignment: 'center',
                    },

                    {
                        text: 'AFAS-METHYL BROMIDE FUMIGATION CERTIFICATE',
                        fontSize: 12,
                          bold: true,
                          alignment: 'center',
                    },

                    {
                        border : 0,
                        bold: true,
                        fontSize: 8,
                        margin: [0, 5, 0, 5],
                        table: {
                            widths: [80, 340, 100],
                      
                            body: [
                                ['CERTIFICATE NO. :', certno.toUpperCase(), 'AEI NO : IN 0012 MB'],
                            ]
                        },
                            layout: 'noBorders'
                    },
                    {
                        text: 'TARGET OF FUMIGATION DETAILS',
                        fontSize: 8,
                          bold: true,
                          alignment: 'center',
                    },
                    {canvas: [{ type: 'line', x1: 0, y1: 5, x2: 595-2*40, y2: 5, lineWidth: 0.1 }]},
                    
                    
                    {
                      border : 0, 
                        fontSize: 8,
                        margin: [0, 5, 0, 5],
                        table: {
                            widths: [81, 140 ,70,'*'],
                      
                            body: [
                              ['Target of Fumigation  ',': '+ json1.fumtarget.toUpperCase(),'Commodity ', ': '+json1.commodify],
                              ['Port of Loading',': '+ json1.loading,'',': '+ json1.commodify1],
                              ['Country Of Origin  ', ': '+json1.countryorg.toUpperCase(),'Quantity ',': '+json1.qty1],
                              ['Port of Discharge ', ': '+json1.portofdischarge,'',{text:': '+json1.qty2,border: [false, false, false, false]}],
                              ['Country Of Destination',': '+ json1.destinationcon.toUpperCase(),'',{text:': '+json1.qty3,border: [false, false, false, false]}],
                              ['Vessel',': '+json1.afasvessel,'Consignment Link ',': '+ json1.consignment],
                              // ['','','',': '+ json1.consignment2,],
                              ]
                        },
                            layout: 'noBorders'
                      },
                      {
                        fontSize: 8,
                        margin: [0, 5, 0, 5],
                        table: {
                            widths: [250, '*'],
                      
                            body: [
                                [
                                { border : [false, true, true, false], 
                                text: 'Name and address of exporter', fontSize: 8,bold:true
                                },
                                {
                                  border : [false, true, false, false], 
                                  text: 'Name and address of importer',  fontSize: 8,bold:true}],
      
                              [
                              {
                                border : [false, false, true, false],
                                text: json1.exporter1, fontSize: 8
                              },
                                {
                                  border : [true, false, false, false],
                                  text: json1.consignee1, fontSize: 8
                                }],
                                [
                              {
                                border : [false, false, true, false],
                                text: json1.exporter2, fontSize: 8
                              },
                                {
                                  border : [false, false, false, false],
                                  text: json1.consignee2, fontSize: 8
                                }],
                                [
                              {
                                border : [false, false, true, false],
                                text: json1.exporter3, fontSize: 8
                              },
                                {
                                  border : [false, false, false, false],
                                  text: json1.consignee3, fontSize: 8
                                }],
                                [
                              {
                                border : [false, false, true, true],
                                text: json1.exporter4, fontSize: 8
                              },
                                {
                                  border : [false, false, false, true],
                                  text: json1.consignee4, fontSize: 8
                                }]
                            ]
                        },
                    },
                    {
                        text: 'TREATMENT DETAILS',
                        fontSize :8,
                        bold:true,
                        alignment : 'center',
                    },
                    {
                        border : 0, 
                        fontSize: 8,
                        margin: [0, 5, 0, 5],
                        table: {
                            widths: [120, '*', 100, '*'],
                      
                            body: [
                                ['Date of fumigation complieted  ', ': '+fumdate1, 'Place of fumigation ',': '+json1.fumplace],
                                ['DAFF Prescribes dose rate ',': '+ json1.daffdosage, 'Exposure Period ',': '+json1.fumdurationhrs+' HOURS'],
                                ['Mini. Air temp(Deg. Cent.) ',': '+json1.mintemp,'Applied dose rate ',': '+ json1.fumdosage], 
                                  
                            ]
                        },
                            layout: 'noBorders'
                    },
                    {canvas: [{ type: 'line', x1: 0, y1: 5, x2: 595-2*40, y2: 5, lineWidth: 1 }]},
                    {
                        border : 0, 
                        fontSize: 8,
                        margin: [0, 5, 0, 5],
                        table: {
                            widths: [200, '*'],
                      
                            body: [
                                ['How was the fumigation conducted :', json1.fumconducted],
                                ['Container number (where applicable) : ', json1.containerno1 ], 
                                  
                            ]
                        },
                            layout: 'noBorders'
                    },
                      {canvas: [{ type: 'line', x1: 0, y1: 5, x2: 595-2*40, y2: 5, lineWidth: 1 }]},
                      {
                        border : 0, 
                        fontSize: 8,
                        margin: [0, 5, 0, 5],
                        table: {
                            widths: [300, '*'],
                      
                            body: [
                                ['Does the target of the fumigation conform to the DAFF plastic wrapping, Impervious surface and timber thickness requirements at the time of fumgation? : ', json1.targetfum],
                                
                                
                                  
                            ]
                        },
                            layout: 'noBorders'
                    },
                    {canvas: [{ type: 'line', x1: 0, y1: 5, x2: 595-2*40, y2: 5, lineWidth: 1 }]},
                    '\n',
                    {
                      text  : [{text:'Ventilation\t\t\t',fontSize: 8,bold:1},{text:'FINAL TLV READING (PPM)',fontSize: 8},{text:'\t\t\t\t\t\t'+json1.ventilation,fontSize: 8},{text:'\t\t\t\t\t\t\t(Not required for stack or pernanent chamber fumigation)',fontSize: 8}],
                    },
                      
                      {canvas: [{ type: 'line', x1: 0, y1: 5, x2: 595-2*40, y2: 5, lineWidth: 1 }]},
                      '\n',
                      {
                        text: 'DECLARATION',
                        fontSize :8,
                        bold : true,
                        alignment : 'center',
                      },
                      '\n',
                        {text: 'By signing below, I, the AFAS accredited fumigator responsible, declare that these details are true and correct and the Fumigation has been carried out in accordance with all the requirements in the DAFF Methyl Bromide Fumigation Standard.',
                        fontSize: 8,
                      },
                    {canvas: [{ type: 'line', x1: 0, y1: 5, x2: 595-2*40, y2: 5, lineWidth: 1 }]},
                    '\n',
                    {
                        text: 'ADDITIONAL DECLARATION\n\n',
                        fontSize :8,
                        bold : true,
                        alignment : 'center',
                    },
                    {text: json1.additional,
                    fontSize: 8,
                    },
                    {text: json1.additional1,
                    fontSize: 8,
                    },
                    {text: json1.additional2,
                    fontSize: 8,
                    },
                    '\n',
                    {canvas: [{ type: 'line', x1: 0, y1: 5, x2: 595-2*40, y2: 5, lineWidth: 1 }]},
                    '\n','\n',
                    {
                        bold: true,
                    fontSize: 8,
                    margin: [0, 5, 0, 5],
                        table: {
                            widths: [120, 250,60],
                      
                            body: [
                                [ {text:'Signature',alignment: 'center'},{text:'Date : '+issuedate,alignment: 'center'},''],
                                [ {text:json1.accname,alignment: 'center'},{text:json1.accno,alignment: 'center'},''],
                                [{text:'Name of Accredited Fumigator',alignment: 'center'},{text:'AFAS-Accreditation Number',alignment: 'center'},'Company Stamp'],
                                ]
                          },
                          layout: 'noBorders'
                        },    


            ]
        }
        pdfMake.createPdf(dd).open();
    }
    function printer_MBR(certid,branchid) {
      
        var jsondata = $.parseJSON($.ajax({
              type: "POST",
            data: {
              certid: certid
            },
            url: 'api/addjson.php',
            cache: false,
            success: function (res) {
            
              },
            dataType: "json", 
            async: false
        }).responseText);
      jsondata =JSON.stringify(jsondata);
      data1 = JSON.parse(jsondata);  
      json1 = data1.json;      
    
      var brjsondata = $.parseJSON($.ajax({
              type: "POST",
              data: {
                certid: certid,branchid:branchid
              },
              url: 'api/getbrjson.php',
              cache: false,
              success: function (res) {
              //jsondata =JSON.stringify(res.json);
              },
              dataType: "json", 
              async: false
          }).responseText);
      brjsondata =JSON.stringify(brjsondata);
      brdata = JSON.parse(brjsondata);  
      brjson = brdata.branchjson;   
    
    
      var fumdate = new Date(json1.fumdate);
      var issuedate = new Date(json1.issuedate);
    
      //fumdate = fumdate.getDate()+ "/" + (fumdate.getMonth()+1) + "/" + fumdate.getFullYear();
      //issuedate = issuedate.getDate()+ "/" + (issuedate.getMonth()+1) + "/" + issuedate.getFullYear();
      fumdate = ('0' + fumdate.getDate()).slice(-2) + "/" + ('0' + (fumdate.getMonth()+1)).slice(-2) + "/" + fumdate.getFullYear();
        issuedate = ('0' + issuedate.getDate()).slice(-2)+ "/" +  ('0' + (issuedate.getMonth()+1)).slice(-2) + "/" + issuedate.getFullYear();
    
    
      var fullDate = new Date();
      var twoDigitMonth = ((fullDate.getMonth().length + 1) === 1) ? (fullDate.getMonth() + 1) : '' + (fullDate.getMonth() + 1);
      var currentDate = fullDate.getDate() + "/" + twoDigitMonth + "/" + fullDate.getFullYear();
      var content=[]; 
      
    
      certno = json1.certificateno;
      //'PMI/'+brjson.branchshortname+'/MBR/'+json1.srno+'/'+fullDate.getFullYear().toString().substr(2,2)+'-'+((fullDate.getFullYear()+1).toString().substr(2,2));
                      dd = {
                        pageSize: 'A4',
                        pageMargins: [40, 30, 30, 40],
                        //pageMargins: [40, 25, 20, 30],
                        footer: function (currentPage, pageCount) {
                            return {
                                // margin: 10,
                                // columns: [{
                                //     fontSize: 8,
                                //     text: [{
                                //             text: '--------------------------------------------------------------------------' +
                                //                 '\n',
                                //             margin: [0, 20]
                                //         },
                                //         {
                                //             text: '©Pest Mortem (India) PVT. LTD. || Printed On ' +currentDate ,
                                //         }
                                //     ],
                                //     alignment: 'center'
                                // }]
                            };
    
                            },
                content: [
                '\n',
                {
                    text: 'PEST MORTEM (INDIA) PRIVATE LIMITED',fontSize: 14,bold: true,alignment: 'center',margin:[0,5,0,0]
                },
                {
                    text: '(Approved by National Plant Protection Organization, Government of India)',
                      fontSize: 8, bold: true, alignment: 'center',
                },
                {
                    text: 'AN ISO 9001:2008 CERTIFIED COMPANY',
                      fontSize: 8,alignment: 'center',margin:[0,0,0,10]
                },
              {
                    margin:[0,-4,0,0],
                    table:{
                    widths:[510],
                    body:[
                        [{
                          margin:[-5,-3,-5,-3],
                          border:[0,0,0,0],
                          table:{
                            
                          widths:["*"],
                          body:[
                            [{text: 'FUMIGATION CERTIFICATE',fontSize: 12,bold: true,alignment: 'center',margin:[0,0,0,0],border:[0,0,0,0],},]
                              ]
                          },
                  },
                  ],
                  [{
                      margin:[-4,-3,-5,-3],
                      table:{
                          widths:['auto',10,'auto','*'],
                          
                          body:[
                          [
                          {stack: [{text: [
                            {text: 'PEST MORTEM (INDIA) PRIVATE LIMITED\n',style: 'left',bold:1},
                            {text:brjson.address+'\n',style:'left',bold:1},
                            {text:'Dte. PPQS Regd. No.\t012/MB/DATED : 25.07.2005  ',style:'left',bold:1},
                            
                            ],}],border:[0,0,0,0]
                          },
                          {
                          stack: [{text: [
                            {text:'\n',style: 'left'},
                            {text:'\n',style:'left'},
                            {text:'\n',style:'left'},
                            ],}],border:[0,0,1,0]   
                          },
                          //{text:'',border:[0,0,0,0]},
                          {
                          stack: [{text: [
                            {text:'Treatment Cert. No. : \n',style: 'left',bold:1},
                            {text:'\n',style:'left'},
                            {text:'Date of Issue :\n',style: 'left',bold:1}
                            ],}],border:[0,0,0,0]   
                          },
                          {
                          stack: [{text: [
                            {text:certno+'\n',style: 'left',bold:1},
                            {text:'\n',style:'left'},
                            {text:issuedate+'\n',style: 'left',bold:1}
                            ],}],border:[0,0,0,0]   
                          },
                          ]
                              ]
                      },layout: {
                    hLineColor: function (i, node) {
                      return (i === 0 || i === node.table.body.length) ? '#778899' : '#778899';
                    },
                    vLineColor: function (i, node) {
                      return (i === 0 || i === node.table.widths.length) ? '#778899' : '#778899';
                    },
                      }
                  }],
                  [
                    {
                          stack: [{text: [
                              {text:'\tThis is to certify that the goods described below were treated in accordance with the fumigation treatment requirements of importing country ',fontSize: 8,italics: true},
                                {text: json1.country.toUpperCase(), italics: false, fontSize: 9,bold:1},
                                {text:' and declared that the consignment has been verified free of impervious surfaces/layers such as plastic wrapping or laminated plastic films, lacquered or painted surfaces, aluminium foil, tarred or waxed papers etc. that may adversely effect the penetration of the fumigant, prior to fumigation.',fontSize: 8,italics: true},
                                {text:'\nThe Certificate is valid for the consignments shipped within 21 days from the date of completion of fumigation',style: '',color:'',fontSize: 8,},
                            ],}],border:[1,1,1,1]   
                          }, 
                ],
                  [{
                      margin:[-5,-3,-5,-3],
                      table:{
                          widths:[165,'*'],
                          
                          body:[
                          [{text:'Details of Treatment\n',style:'left',colSpan: 2,bold:1},{},],
                          [{text:'Name of Fumigant\n',style:'left'},{text:'METHYL BROMIDE\n',style:'left'},],
                          [{text:'Date of Fumigation\n',style:'left'},{text:fumdate+'\n',style:'left'},],
                          [{text:'Place of Fumigation\n',style:'left'},{text:json1.fumplace+'\n',style:'left'},],
                          [{text:'Dosage of Fumigant\n',style:'left'},{text:json1.fumdosage+' GMS/M3\n',style:'left'},],
                          [{text:'Duration of Fumigation (in HRS)\n',style:'left'},{text:json1.fumdurationhrs+'\n',style:'left'},],
                          [{text:'Average ambient temp during Fumigation(°C)\n',style:'left'},{text:json1.mintemp+' °C\n',style:'left'},],
                          [{text:'Fumigation performed under gas tight sheets\n',style:'left'},{text:json1.fumperformed+'\n',style:'left',bold:1},],
                          [{text:'If containers are not fumigated under gastight\nsheets, pressure in seconds decay value (from\n200-100 Pascals in seconds)\n',style:'left'},{text:json1.pressureinsec+'\n',style:'left'},],
                          [{text:'Description of Goods\n',style:'left',colSpan: 2,bold:1},{text:'\n',style:'left'},],
                          [{text:'Container Number (or numerical link).\n/Seal Number\n',style:'left'},
                          {stack: [{text: [
                              {text:json1.containerno1+'\n',style:'left'},
                              {text:json1.containerno2+'\n',style:'left',},
                              {text:json1.containerno3+'\n',style:'left',},
                              {text:json1.containerno4+'\n',style:'left',},
                              ]}]}
                          ],
                          [{text:'Name & Address of Exporter\n',style:'left'},
                          {stack: [{text: [
                              {text:json1.exporter1+'\n',style:'left'},
                              {text:json1.exporter2+'\n',style:'left',},
                              {text:json1.exporter3+'\n',style:'left',},
                              ]}]}
                              ],
                          [{text:'Name & Address of Importer\n',style:'left'},
                          {stack: [{text: [
                              {text:json1.consignee1+'\n',style:'left'},
                              {text:json1.consignee2+'\n',style:'left',},
                              {text:json1.consignee3+'\n',style:'left',},
                              ]}]}
                          ],
                          [{text:'Name & Address of notified party\n',style:'left'},
                          {stack: [{text: [
                              {text:json1.notifiedparty1+'\n',style:'left'},
                              {text:json1.notifiedparty2+'\n',style:'left',},
                              {text:json1.notifiedparty3+'\n',style:'left',},
                              ]}]}
                          ],
                          [{text:'Type and description of cargo\n',style:'left'},
                          {stack: [{text: [
                              {text:json1.description+'\n',style:'left'},
                              {text:json1.description1+'\n',style:'left',},
                              ]}]}
                          ],
                          [{text:'Quantity (MTs)/ No of packages/No\n of pieces',style:'left'},
                          {stack: [{text: [
                              {text:json1.qty1+'\n',style:'left'},
                              {text:json1.qty2,style:'left'},
                              ]}]}
                          ],
                          [{text:'Description of packaging material\n',style:'left'},
                          {stack: [{text: [
                              {text:json1.description2+'\n',style:'left'},
                              {text:json1.description3+'\n',style:'left',},
                              ]}]}
                          ],
                          [{text:'Shipping mark or brand\n',style:'left'},
                          {stack: [{text: [
                              {text:json1.shipingmark+'\n',style:'left'},
                              {text:json1.shipingmark1+'\n',style:'left',},
                              ]}]}
                          ],
                          [{text:'Port & country Of Loading\n',style:'left'},
                          {text:json1.loading+'\n',style:'left'},],
                          [{text:'Port of Discharge\n',style:'left'},
                          {text:json1.entryport+'\n',style:'left'},],
                          [{text:'Name of the Vessel\n',style:'left'},
                          {text:json1.vesselname+'\n',style:'left'},],
                          [{text:'Additional Declaration\n',style:'left'},
                          {stack: [{text: [
                              {text:json1.additional+'\n',style:'left'},
                              {text:json1.additional1+'\n',style:'left',},
                              {text:json1.additional2+'\n',style:'left',},
                              ]}]}
                          ],
                          [{text:'Name &,Signature of Accredited\nFumigation Operator with seal & date/\nAccreditation Number\n',style:'left',bold:1},
                          {stack: [{text: [
                            {text:json1.accname+'\n',style:'left'},
                              {text:json1.accno+'\n',style:'left',},
                              {text:'Date:'+issuedate+'\n',style:'left',},
                              ]}]}
                              ],
                    
                          ],
                      },layout: {
                    hLineColor: function (i, node) {
                      return (i === 0 || i === node.table.body.length) ? '#778899' : '#778899';
                    },
                    vLineColor: function (i, node) {
                      return (i === 0 || i === node.table.widths.length) ? '#778899' : '#778899';
                    },
                      }
                  }
                ],
                
              ]
        },layout: {
    
            hLineColor: function (i, node) {
              return (i === 0 || i === node.table.body.length) ? '#778899' : '#778899';
            },
            vLineColor: function (i, node) {
              return (i === 0 || i === node.table.widths.length) ? '#778899' : '#778899';
            },
              }
        
        },
        {text:'\n\n'},
        
        
        ],
        styles: {
            left:{
                fontSize: 8,
            alignment:'left',
            },
            right:{
                fontSize: 8,
            alignment:'right',
            },
        centerbold: {
          fontSize: 8,
          alignment:'center',
          bold:1
        },
        center: {
          fontSize: 8,
          alignment:'center',
        },
        bn: {
          fontSize: 8,
          border:[0,0,0,0],
        },
        }
        }
        pdfMake.createPdf(dd).open();
    }
</script>