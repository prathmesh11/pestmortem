<?php
date_default_timezone_set('Asia/Kolkata');
include('env.php');
$CURRENT_MILLIS=round(microtime(true) * 1000);
$CURRENT_DAY=date("d", $CURRENT_MILLIS/1000);
$CURRENT_MONTH=date("m", $CURRENT_MILLIS/1000);
$CURRENT_YEAR=date("Y", $CURRENT_MILLIS/1000);
$START_OF_THE_DAY=strtotime($CURRENT_YEAR.'-'.$CURRENT_MONTH.'-'.$CURRENT_DAY.' 00:00:00') * 1000;
$END_OF_THE_DAY=strtotime($CURRENT_YEAR.'-'.$CURRENT_MONTH.'-'.$CURRENT_DAY.' 24:00:00') * 1000;;
$START_OF_SMALL_PERIOD=$CURRENT_MILLIS-1800000;
$END_OF_SMALL_PERIOD=$CURRENT_MILLIS+1800000;
function _clean($con,$str){
  return mysqli_real_escape_string($con,$str);
}

$month = date('m');
$day = date('d');
$year = date('Y');
$today = $year . '-' . $month . '-' . $day;

function notify($con,$note) {
  $jsonnotify='';

  if (session_status()==PHP_SESSION_NONE) {
      session_start();
  }
  
  $created_by=$_SESSION['aid'];
  $CURRENT_MILLIS=round(microtime(true) * 1000);
  $notifyfuc=mysqli_query($con, "SELECT aid,username FROM admin");

  while($rows=mysqli_fetch_assoc($notifyfuc)) {
      $id=$rows["aid"];
      $username=$rows["username"];
      $jsonnotify.=',{"id":"'.$id.'","username":"'.$username.'","notetime":"'.$CURRENT_MILLIS.'"}';
  }
  $jsonnotify=substr($jsonnotify, 1);
  $jsonnotify='['.$jsonnotify.']';
  
  $result=mysqli_query($con, "INSERT INTO notify (note,sended,created_by,created_time) VALUES ('$note','$jsonnotify','$created_by','$CURRENT_MILLIS')");

}

  function _RSTOWORD($number) {
    $no = round($number);
    $decimal = round($number - ($no = floor($number)), 2) * 100;    
    $digits_length = strlen($no);    
    $i = 0;
    $str = array();
    $words = array(
        0 => '',
        1 => 'One',
        2 => 'Two',
        3 => 'Three',
        4 => 'Four',
        5 => 'Five',
        6 => 'Six',
        7 => 'Seven',
        8 => 'Eight',
        9 => 'Nine',
        10 => 'Ten',
        11 => 'Eleven',
        12 => 'Twelve',
        13 => 'Thirteen',
        14 => 'Fourteen',
        15 => 'Fifteen',
        16 => 'Sixteen',
        17 => 'Seventeen',
        18 => 'Eighteen',
        19 => 'Nineteen',
        20 => 'Twenty',
        30 => 'Thirty',
        40 => 'Forty',
        50 => 'Fifty',
        60 => 'Sixty',
        70 => 'Seventy',
        80 => 'Eighty',
        90 => 'Ninety');
    $digits = array('', 'Hundred', 'Thousand', 'Lakh', 'Crore');
    while ($i < $digits_length) {
        $divider = ($i == 2) ? 10 : 100;
        $number = floor($no % $divider);
        $no = floor($no / $divider);
        $i += $divider == 10 ? 1 : 2;
        if ($number) {
            $plural = (($counter = count($str)) && $number > 9) ? 's' : null;            
            $str [] = ($number < 21) ? $words[$number] . ' ' . $digits[$counter] . $plural : $words[floor($number / 10) * 10] . ' ' . $words[$number % 10] . ' ' . $digits[$counter] . $plural;
        } else {
            $str [] = null;
        }  
    }
    
    $Rupees = implode(' ', array_reverse($str));
    $paise = ($decimal) ? "And Paise " . ($words[$decimal - $decimal%10]) ." " .($words[$decimal%10])  : '';
    return ($Rupees ? ' ' . $Rupees : '') . $paise . " Only";
}

function GetInvoiceNo($con,$branchid){
    $invoice_bill = mysqli_fetch_assoc(mysqli_query($con,"SELECT invoice_bill x FROM `billing` WHERE invoice_bill<>'' AND category<>'proformaBill' AND branchid = '$branchid' order by id desc LIMIT 1"))['x'];
  
    $invoice_pice = explode("/", $invoice_bill);
    $invoice_year=$invoice_pice[2];

    if (date('m') > 3) {
        $year = date('y')."-".(date('y') +1);
    }
    else {
        $year = (date('y')-1)."-".date('y');
    }
   
    $branchshortname = mysqli_fetch_assoc(mysqli_query($con,"SELECT branchshortname x FROM branchmaster WHERE branchid='$branchid'"))['x'];
    if($invoice_year==$year){
        $invoiceId=$invoice_pice[1];
        $invoiceId++;

        $number = sprintf('%03d',$invoiceId);

        $invoiceNo = $branchshortname.'/'.$number.'/'.$year;
        return $invoiceNo;
    }else{
        $invoiceId='001';
        $invoiceNo = $branchshortname.'/'.$invoiceId.'/'.$year;
        return $invoiceNo;
    }
}


//MUM/2992/20-21 last invoice No.
function newProformaBillNo($con,$branchid){
        $invoice_bill = mysqli_fetch_assoc(mysqli_query($con,"SELECT invoice_bill x FROM `billing` WHERE invoice_bill<>'' AND category='proformaBill' AND branchid = '$branchid' order by id desc LIMIT 1"))['x'];
      
        $invoice_pice = explode("/", $invoice_bill);
        $invoice_year=$invoice_pice[3];

        if (date('m') > 3) {
            $year = date('y')."-".(date('y') +1);
        }
        else {
            $year = (date('y')-1)."-".date('y');
        }
       
        $branchshortname = mysqli_fetch_assoc(mysqli_query($con,"SELECT branchshortname x FROM branchmaster WHERE branchid='$branchid'"))['x'];
        if($invoice_year==$year){
            $invoiceId=$invoice_pice[2];
            $invoiceId++;
            $number = sprintf('%03d',$invoiceId);

            $invoiceNo = $branchshortname.'/PF/'.$number.'/'.$year;

            return $invoiceNo;
        }else{
            $invoiceId='1';
            $invoiceNo = $branchshortname.'/PF/'.$invoiceId.'/'.$year;
            return $invoiceNo;
        }
       
       
    }

    function contractid($con,$branchid,$category)
    {
        
        if ($category == "Fumigation") {


            $contractid   = mysqli_fetch_assoc(mysqli_query($con,"SELECT contractid x FROM `crmmaster` WHERE category='Fumigation' AND branchid = '$branchid' order by id desc LIMIT 1"))['x'];

            $invoice_pice = explode("/", $contractid);
           
            $invoice_year = $invoice_pice[4];

            if (date('m') > 3) {

                $year = date('y')."-".(date('y') +1);

            } else {

                $year = (date('y')-1)."-".date('y');

            }

            $branchshortname = mysqli_fetch_assoc(mysqli_query($con,"SELECT branchshortname x FROM branchmaster WHERE branchid='$branchid'"))['x'];

            if($invoice_year==$year){

                $invoiceId = $invoice_pice[3];
                
                $invoiceId++;
                
                $number      = sprintf($invoiceId);

                $contract_id = 'PMI/'.$branchshortname.'/FUM/'.$number.'/'.$year;

                return $contract_id;

            }else{

                $invoiceId = '1';

                $contract_id = 'PMI/'.$branchshortname.'/FUM/'.$invoiceId.'/'.$year;
                
                return $contract_id;

            }

        } elseif ($category == "Pest Control") {

            $contractid = mysqli_fetch_assoc(mysqli_query($con,"SELECT contractid x FROM `crmmaster` WHERE category='Pest Control' AND branchid = '$branchid' order by id desc LIMIT 1"))['x'];

            $invoice_pice = explode("/", $contractid);
           
            $invoice_year = $invoice_pice[4];

            if (date('m') > 3) {

                $year = date('y')."-".(date('y') +1);

            } else {

                $year = (date('y')-1)."-".date('y');

            }

            $branchshortname = mysqli_fetch_assoc(mysqli_query($con,"SELECT branchshortname x FROM branchmaster WHERE branchid='$branchid'"))['x'];

            if($invoice_year==$year){

                $invoiceId = $invoice_pice[3];
                
                $invoiceId++;
                
                $number      = sprintf($invoiceId);

                $contract_id = 'PMI/'.$branchshortname.'/PC/'.$number.'/'.$year;

                return $contract_id;

            }else{

                $invoiceId = '1';

                $contract_id = 'PMI/'.$branchshortname.'/PC/'.$invoiceId.'/'.$year;
                
                return $contract_id;

            }

        }

    }

    
    
    // Get Expenses Id

    function GetExpensesNo($con,$branchid){

        $expense_id = mysqli_fetch_assoc(mysqli_query($con,"SELECT expense_id x FROM `expense` WHERE expense_id<>'' AND branchid = '$branchid' order by id desc LIMIT 1"))['x'];
      
        $invoice_pice = explode("/", $expense_id);

        $invoice_year=$invoice_pice[3];
    
        if (date('m') > 3) {

            $year = date('y')."-".(date('y') +1);
        }
        else {
            $year = (date('y')-1)."-".date('y');
        }
       
        $branchshortname = mysqli_fetch_assoc(mysqli_query($con,"SELECT branchshortname x FROM branchmaster WHERE branchid='$branchid'"))['x'];

        if($invoice_year==$year){

            $invoiceId=$invoice_pice[2];

            $invoiceId++;
    
            $number = sprintf('%03d',$invoiceId);
    
            $invoiceNo = 'PAY/'.$branchshortname.'/'.$number.'/'.$year;
            return $invoiceNo;
        }else{
            $invoiceId='001';
            $invoiceNo = 'PAY/'.$branchshortname.'/'.$invoiceId.'/'.$year;
            return $invoiceNo;
        }
    }


     // Get Enquiry No

     function GetEnquiryNo($con,$branchid){

        $enquiry_id = mysqli_fetch_assoc(mysqli_query($con,"SELECT enquiryid x FROM `enquiry` WHERE enquiryid<>'' AND branchid = '$branchid' order by id desc LIMIT 1"))['x'];
      
        $invoice_pice = explode("/", $enquiry_id);

        $invoice_year=$invoice_pice[4];
    
        if (date('m') > 3) {

            $year = date('y')."-".(date('y') +1);
        }
        else {
            $year = (date('y')-1)."-".date('y');
        }
       
        $branchshortname = mysqli_fetch_assoc(mysqli_query($con,"SELECT branchshortname x FROM branchmaster WHERE branchid='$branchid'"))['x'];

        if($invoice_year==$year){

            $invoiceId=$invoice_pice[3];

            $invoiceId++;
    
            $number = sprintf('%06d',$invoiceId);
    
            $enqNos = 'PMI/'.$branchshortname.'/ENQ/'.$number.'/'.$year;

            return $enqNos;

        }else{
            $invoiceId='000001';
            $enqNos = 'PMI/'.$branchshortname.'/ENQ/'.$invoiceId.'/'.$year;
            return $enqNos;
        }
    }
    

     // Get Payment No

     function GetPaymentNo($con,$branchid){

        $receipt_id = mysqli_fetch_assoc(mysqli_query($con,"SELECT receipt_no x FROM `invoicepayment` WHERE receipt_no<>'' AND branchid = '$branchid' order by id desc LIMIT 1"))['x'];
      
        $invoice_pice = explode("/", $receipt_id);

        $invoice_year=$invoice_pice[4];
    
        if (date('m') > 3) {

            $year = date('y')."-".(date('y') +1);
        }
        else {
            $year = (date('y')-1)."-".date('y');
        }
       
        $branchshortname = mysqli_fetch_assoc(mysqli_query($con,"SELECT branchshortname x FROM branchmaster WHERE branchid='$branchid'"))['x'];

        if($invoice_year==$year){

            $invoiceId=$invoice_pice[3];

            $invoiceId++;
    
            $number = $invoiceId;
    
            $receiptNos = 'PMI/'.$branchshortname.'/PAY/'.$number.'/'.$year;

            return $receiptNos;

        }else{

            $invoiceId = '1';

            $receiptNos    = 'PMI/'.$branchshortname.'/PAY/1/'.$year;

            return $receiptNos;

        }
    }


    // Get Credit Note No

    function GetCreditNoteNo($con,$branchid){

        $creditNote_id = mysqli_fetch_assoc(mysqli_query($con,"SELECT credit_note_no x FROM `creditnote` WHERE credit_note_no<>'' AND branchid = '$branchid' order by id desc LIMIT 1"))['x'];
      
        $invoice_pice = explode("/", $creditNote_id);

        $invoice_year=$invoice_pice[4];
    
        if (date('m') > 3) {

            $year = date('y')."-".(date('y') +1);
        }
        else {
            $year = (date('y')-1)."-".date('y');
        }
       
        $branchshortname = mysqli_fetch_assoc(mysqli_query($con,"SELECT branchshortname x FROM branchmaster WHERE branchid='$branchid'"))['x'];

        if($invoice_year==$year){

            $invoiceId=$invoice_pice[3];

            $invoiceId++;
    
            $number = $invoiceId;
    
            $creditNoteNos = 'PMI/'.$branchshortname.'/CR/'.$number.'/'.$year;

            return $creditNoteNos;

        }else{

            $invoiceId = '1';

            $creditNoteNos    = 'PMI/'.$branchshortname.'/CR/1/'.$year;

            return $creditNoteNos;

        }
    }


    // Get Debit Note No

    function GetDebitNoteNo($con,$branchid){

        $debitNote_id = mysqli_fetch_assoc(mysqli_query($con,"SELECT debit_note_no x FROM `debitnote` WHERE debit_note_no<>'' AND branchid = '$branchid' order by id desc LIMIT 1"))['x'];
      
        $invoice_pice = explode("/", $debitNote_id);

        $invoice_year=$invoice_pice[4];
    
        if (date('m') > 3) {

            $year = date('y')."-".(date('y') +1);
        }
        else {
            $year = (date('y')-1)."-".date('y');
        }
       
        $branchshortname = mysqli_fetch_assoc(mysqli_query($con,"SELECT branchshortname x FROM branchmaster WHERE branchid='$branchid'"))['x'];

        if($invoice_year==$year){

            $invoiceId=$invoice_pice[3];

            $invoiceId++;
    
            $number = $invoiceId;
    
            $creditNoteNos = 'PMI/'.$branchshortname.'/DR/'.$number.'/'.$year;

            return $creditNoteNos;

        }else{

            $invoiceId = '1';

            $creditNoteNos    = 'PMI/'.$branchshortname.'/DR/1/'.$year;

            return $creditNoteNos;

        }
    }

    // Get Debit Note No

    function getNewQuatation($con,$branchid){

        $quatation_id = mysqli_fetch_assoc(mysqli_query($con,"SELECT quatation_id x FROM `quatation` WHERE quatation_id<>'' AND branchid = '$branchid' order by id desc LIMIT 1"))['x'];
      
        $invoice_pice = explode("/", $quatation_id);

        $invoice_year=$invoice_pice[4];
    
        if (date('m') > 3) {

            $year = date('y')."-".(date('y') +1);
        }
        else {
            $year = (date('y')-1)."-".date('y');
        }
       
        $branchshortname = mysqli_fetch_assoc(mysqli_query($con,"SELECT branchshortname x FROM branchmaster WHERE branchid='$branchid'"))['x'];

        if($invoice_year==$year){

            $invoiceId=$invoice_pice[3];

            $invoiceId++;
    
            $number = $invoiceId;
    
            $quatationIds = 'PMI/'.$branchshortname.'/QUO/'.$number.'/'.$year;

            return $quatationIds;

        }else{

            $invoiceId = '1';

            $quatationIds    = 'PMI/'.$branchshortname.'/QUO/1/'.$year;

            return $quatationIds;

        }
    }

?>