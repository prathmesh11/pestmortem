<?php 
if (session_status() == PHP_SESSION_NONE) {
  session_start();
}
if (!isset($_SESSION['phone']) && !isset($_SESSION['username']) && !isset($_SESSION['employeeid']) &&  !isset($_SESSION['role'])) {
  header('Location: /index.php');
}
include('industry.php');


if(!isset($title)){ $title=$industry_name;}
if(!isset($css)){ $css="";}
if(!isset($js)){ $js="";}

$_pagearr=json_decode($_SESSION['pagearr']);

$DASHBOARD='style="display:none;"';
$LEDGER='style="display:none;"';
$OVERDUE='style="display:none;"';
$ENQUIRY ='style="display:none;"';
$QUOTATION='style="display:none;"';
$CRM='style="display:none;"';
$INVOICE='style="display:none;"';
$INVOICEPAYMENT='style="display:none;"';
$CERTIFICATE='style="display:show;"';
$CALCULATION='style="display:none;"';
$AMC='style="display:none;"';
$PURCHASE='style="display:none;"';
$STOCK='style="display:none;"';
$MASTER='style="display:none;"';
$SALARY_MASTER='style="display:none;"';
$COMMISSION_MASTER='style="display:none;"';
$CREDITANDDEBITNOTE='style="display:none;"';

$EmployeeReports='style="display:none;"';
$SalaryReports='style="display:none;"';
$EnquiryFollowUp='display:none;';
$WorkContract='display:none;';
$Certification='display:none;';
$JobAllotment='display:none;';
$JobCompleted='display:none;';
$RescheduleJob = 'display:none;';
$Biling='display:none;';
$Payment='display:none;';
$CloseEnquiry='display:none;';

$fumigationInvoice='display:none;';
$pestcontrolInvoice='display:none;';
$ProductSaleBillInvoice='display:none;';
$ProformaBillInvoice='display:none;';

$fumigationInvoicePayment='display:none;';

$Purchaserequisition='display:none;';
$approvalOrder='display:none;';
$PurchaseOrder='display:none;';
$GateEntry='display:none;';
$Quality='display:none;';
$PurchaseBill='display:none;';
$PaymentBill='display:none;';
$Reporting='display:none;';


$MaterialIssue='display:none;';
$MaterialReceive='display:none;';
$BranchToBranchTrasfer='display:none;';
$_Stock='display:none;';
$GovernmentStock='display:none;';


$AdditionalMaster='style="display:none;"';
$ItemGroupMaster='style="display:none;"';
$ItemMaster='style="display:none;"';
$ItemMasterReOrder='style="display:none;"';
$SupplierMaster='style="display:none;"';
$ItemRateMaster='style="display:none;"';
$CustomerMaster='style="display:none;"';
$BranchMaster='style="display:none;"';
$ServiceMaster='style="display:none;"';
$EmployeeLoginMaster='style="display:none;"';
$MachineAssetsMaster='style="display:none;"';
$commoditymaster='style="display:none;"';
$accreditationmaster='style="display:none;"';
$countrymaster='style="display:none;"';
$temperaturemaster='style="display:none;"';
$salesperson='style="display:none;"';
$Document='style="display:none;"';
$EmployeeStock='style="display:none;"';
$EmployeeStockReport='style="display:none;"';
$AccountHead='style="display:none;"';
$Expenses='style="display:none;"';

$SalaryMaster='style="display:none;"';
$LeaveOpeningBalance='style="display:none;"';
$ProfessionalTaxMaster='style="display:none;"';
$PaidHolidays='style="display:none;"';
$MonthlyAttendance='style="display:none;"';
$LeaveAllotment='style="display:none;"';
$LoanAdvanceDetails='style="display:none;"';
$SalaryProcessing='style="display:none;"';

$ALPCertification='style="display:show;"';
$MBRCertification='style="display:show;"';
$AFASCertification='style="display:show;"';
$WPMCertification='style="display:show;"';

$CommissionRequest='style="display:none;"';
$CommissionApproval='style="display:none;"';
$BillPaid='style="display:none;"';
$CommissionMasterAgent='style="display:none;"';


$Calculation='style="display:none;"';
$reports='style="display:none;"';
$Quatation='style="display:none;"';

$EnquiryFollowUp='style="display:none;"';
$ConfirmEnquiry='style="display:none;"';
$CloseEnquiry='style="display:none;"';

$customerledger='style="display:none;"';
$supplierledger='style="display:none;"';

$creditNote='style="display:none;"';
$debitNote='style="display:none;"';

$customerbill='style="display:none;"';
$supplierbill='style="display:none;"';

$workcontract='style="display:none;"';


$arrysession=[];

foreach($_pagearr as $i){



  if(get_object_vars($i)['mainpage']=='DASHBOARD' && get_object_vars($i)['status']==true){$DASHBOARD='style="display:show;"';
    $subpagearr=get_object_vars($i)['subpage'];
    foreach($subpagearr as $ii){
    
      if(get_object_vars($ii)['subpage']=='Employee Reports' && get_object_vars($ii)['status']==true){$EmployeeReports='style="display:show;"';}
     if(get_object_vars($ii)['subpage']=='Certificate Reports' && get_object_vars($ii)['status']==true){$CertificateReports='style="display:show;"';}
     if(get_object_vars($ii)['subpage']=='Salary Reports' && get_object_vars($ii)['status']==true){$SalaryReports='style="display:show;"';}
     
     
     if (get_object_vars($ii)['status']==true) {
        array_push($arrysession,get_object_vars($ii)['subpage']);

     }

    }
  }


  if(get_object_vars($i)['mainpage']=='QUOTATION' && get_object_vars($i)['status']==true){$QUOTATION='style="display:show;"';
    $subpagearr=get_object_vars($i)['subpage'];
    foreach($subpagearr as $ii){
     if(get_object_vars($ii)['subpage']=='Quatation' && get_object_vars($ii)['status']==true){$Quatation='display:show;';}
     if (get_object_vars($ii)['status']==true) {
      array_push($arrysession,get_object_vars($ii)['subpage']);

   }

    }
  }

  if(get_object_vars($i)['mainpage']=='LEDGER' && get_object_vars($i)['status']==true){$LEDGER='style="display:show;"';
    $subpagearr=get_object_vars($i)['subpage'];
    foreach($subpagearr as $ii){
     if(get_object_vars($ii)['subpage']=='Customer Ledger' && get_object_vars($ii)['status']==true){$customerledger='display:show;';}
     if(get_object_vars($ii)['subpage']=='Supplier Ledger' && get_object_vars($ii)['status']==true){$supplierledger='style="display:show;"';}
     if (get_object_vars($ii)['status']==true) {
      array_push($arrysession,get_object_vars($ii)['subpage']);

   }

    }
  }

  if(get_object_vars($i)['mainpage']=='OVERDUE' && get_object_vars($i)['status']==true){$OVERDUE='style="display:show;"';
    $subpagearr=get_object_vars($i)['subpage'];
    foreach($subpagearr as $ii){
     if(get_object_vars($ii)['subpage']=='Customer Bills' && get_object_vars($ii)['status']==true){$customerbill='display:show;';}
     if(get_object_vars($ii)['subpage']=='Supplier Bills' && get_object_vars($ii)['status']==true){$supplierbill='style="display:show;"';}
     if (get_object_vars($ii)['status']==true) {
      array_push($arrysession,get_object_vars($ii)['subpage']);

   }

    }
  }

  if(get_object_vars($i)['mainpage']=='ENQUIRY' && get_object_vars($i)['status']==true){$ENQUIRY='style="display:show;"';
    $subpagearr=get_object_vars($i)['subpage'];
    foreach($subpagearr as $ii){
     if(get_object_vars($ii)['subpage']=='Enquiry & FollowUp' && get_object_vars($ii)['status']==true){$EnquiryFollowUp='display:show;';}
     if(get_object_vars($ii)['subpage']=='Confirm Enquiry' && get_object_vars($ii)['status']==true){$ConfirmEnquiry='style="display:show;"';}
     if(get_object_vars($ii)['subpage']=='Close Enquiry' && get_object_vars($ii)['status']==true){$CloseEnquiry='style="display:show;"';}
     if (get_object_vars($ii)['status']==true) {
      array_push($arrysession,get_object_vars($ii)['subpage']);

   }

    }
  }

  if(get_object_vars($i)['mainpage']=='CRM' && get_object_vars($i)['status']==true){$CRM='style="display:show;"';
    $subpagearr=get_object_vars($i)['subpage'];
    foreach($subpagearr as $ii){
     if(get_object_vars($ii)['subpage']=='Work Contract' && get_object_vars($ii)['status']==true){$WorkContract='display:show;';}
     if(get_object_vars($ii)['subpage']=='Job Allotment' && get_object_vars($ii)['status']==true){$JobAllotment='display:show;';}
     if(get_object_vars($ii)['subpage']=='Job Completed' && get_object_vars($ii)['status']==true){$JobCompleted='display:show;';}
     if(get_object_vars($ii)['subpage']=='Reschedule Job' && get_object_vars($ii)['status']==true){$RescheduleJob='display:show;';}

     if (get_object_vars($ii)['status']==true) {
      array_push($arrysession,get_object_vars($ii)['subpage']);

   }

    }
  }
  if(get_object_vars($i)['mainpage']=='INVOICE' && get_object_vars($i)['status']==true){$INVOICE='style="display:show;"';
    $subpagearr=get_object_vars($i)['subpage'];
    foreach($subpagearr as $ii){
     if(get_object_vars($ii)['subpage']=='Fumigation Invoice' && get_object_vars($ii)['status']==true){$fumigationInvoice='display:show;';}
     if(get_object_vars($ii)['subpage']=='Pestcontrol Invoice' && get_object_vars($ii)['status']==true){$pestcontrolInvoice='display:show;';}
     if(get_object_vars($ii)['subpage']=='Product Sale Bill Invoice' && get_object_vars($ii)['status']==true){$ProductSaleBillInvoice='display:show;';}
     if(get_object_vars($ii)['subpage']=='Proforma Bill Invoice' && get_object_vars($ii)['status']==true){$ProformaBillInvoice='display:show;';}
     if (get_object_vars($ii)['status']==true) {
      array_push($arrysession,get_object_vars($ii)['subpage']);

   }

    }
  }
  if(get_object_vars($i)['mainpage']=='INVOICE PAYMENT' && get_object_vars($i)['status']==true){$INVOICEPAYMENT='style="display:show;"';
    $subpagearr=get_object_vars($i)['subpage'];
    foreach($subpagearr as $ii){
     if(get_object_vars($ii)['subpage']=='Fumigation Invoice Payment' && get_object_vars($ii)['status']==true){$fumigationInvoicePayment='display:show;';}
    //  if(get_object_vars($ii)['subpage']=='Pestcontrol Invoice' && get_object_vars($ii)['status']==true){$pestcontrolInvoice='display:show;';}
    //  if(get_object_vars($ii)['subpage']=='Product Sale Bill Invoice' && get_object_vars($ii)['status']==true){$ProductSaleBillInvoice='display:show;';}
    //  if(get_object_vars($ii)['subpage']=='Proforma Bill Invoice' && get_object_vars($ii)['status']==true){$ProformaBillInvoice='display:show;';}
    if (get_object_vars($ii)['status']==true) {
      array_push($arrysession,get_object_vars($ii)['subpage']);

   }

    }
  }
  if(get_object_vars($i)['mainpage']=='AMC' && get_object_vars($i)['status']==true){$AMC='style="display:show;"';
    $subpagearr=get_object_vars($i)['subpage'];
    foreach($subpagearr as $ii){
     if(get_object_vars($ii)['subpage']=='Annual Work Contract' && get_object_vars($ii)['status']==true){$workcontract='display:show;';}
     if (get_object_vars($ii)['status']==true) {
      array_push($arrysession,get_object_vars($ii)['subpage']);

   }

    }
  }
  if(get_object_vars($i)['mainpage']=='CERTIFICATION' && get_object_vars($i)['status']==true){$CERTIFICATE='style="display:show;"';
    $subpagearr=get_object_vars($i)['subpage'];
    foreach($subpagearr as $ii){
     if(get_object_vars($ii)['subpage']=='ALP' && get_object_vars($ii)['status']==true){$ALPCertification='style="display:show;"';}
     if(get_object_vars($ii)['subpage']=='MBR' && get_object_vars($ii)['status']==true){$MBRCertification='style="display:show;"';}
     if(get_object_vars($ii)['subpage']=='AFAS' && get_object_vars($ii)['status']==true){$AFASCertification='style="display:show;"';}
     if(get_object_vars($ii)['subpage']=='WPM' && get_object_vars($ii)['status']==true){$WPMCertification='style="display:show;"';}
    //  if(get_object_vars($ii)['subpage']=='Calculation' && get_object_vars($ii)['status']==true){$Calculation='style="display:show;"';}
    if (get_object_vars($ii)['status']==true) {
      array_push($arrysession,get_object_vars($ii)['subpage']);

   }
  
  }
  }

  if(get_object_vars($i)['mainpage']=='CALCULATION' && get_object_vars($i)['status']==true){$CALCULATION='style="display:show;"';
    $subpagearr=get_object_vars($i)['subpage'];
    foreach($subpagearr as $ii){
     if(get_object_vars($ii)['subpage']=='Calculation' && get_object_vars($ii)['status']==true){$Calculation='style="display:show;"';}
     if(get_object_vars($ii)['subpage']=='reports' && get_object_vars($ii)['status']==true){$reports='style="display:show;"';}
     if (get_object_vars($ii)['status']==true) {
      array_push($arrysession,get_object_vars($ii)['subpage']);

   }

    }
  }


  if(get_object_vars($i)['mainpage']=='PURCHASE' && get_object_vars($i)['status']==true){$PURCHASE='style="display:show;"';
    $subpagearr=get_object_vars($i)['subpage'];
    foreach($subpagearr as $ii){
     if(get_object_vars($ii)['subpage']=='Purchase Requisition' && get_object_vars($ii)['status']==true){$Purchaserequisition='display:show;';}
     if(get_object_vars($ii)['subpage']=='Approval Order' && get_object_vars($ii)['status']==true){$approvalOrder='display:show;';}
     if(get_object_vars($ii)['subpage']=='Purchase Order' && get_object_vars($ii)['status']==true){$PurchaseOrder='display:show;';}
     if(get_object_vars($ii)['subpage']=='Gate Entry' && get_object_vars($ii)['status']==true){$GateEntry='display:show;';}
     if(get_object_vars($ii)['subpage']=='Purchase Bill' && get_object_vars($ii)['status']==true){$PurchaseBill='display:show;';}
     if(get_object_vars($ii)['subpage']=='Payment Bill' && get_object_vars($ii)['status']==true){$PaymentBill='display:show;';}
     if(get_object_vars($ii)['subpage']=='Reporting' && get_object_vars($ii)['status']==true){$Reporting='display:show;';}

     if (get_object_vars($ii)['status']==true) {
      array_push($arrysession,get_object_vars($ii)['subpage']);

   }

    }
  }
  if(get_object_vars($i)['mainpage']=='STOCK' && get_object_vars($i)['status']==true){$STOCK='style="display:show;"';
    $subpagearr=get_object_vars($i)['subpage'];
    foreach($subpagearr as $ii){
     if(get_object_vars($ii)['subpage']=='Material Issue' && get_object_vars($ii)['status']==true){$MaterialIssue='display:show;';}
     if(get_object_vars($ii)['subpage']=='Material Receive' && get_object_vars($ii)['status']==true){$MaterialReceive='display:show;';}
     if(get_object_vars($ii)['subpage']=='Branch To Branch Trasfer' && get_object_vars($ii)['status']==true){$BranchToBranchTrasfer='display:show;';}
     if(get_object_vars($ii)['subpage']=='Stock' && get_object_vars($ii)['status']==true){$_Stock='display:show;';}
     if(get_object_vars($ii)['subpage']=='Government Stock' && get_object_vars($ii)['status']==true){$GovernmentStock='display:show;';}
     if (get_object_vars($ii)['status']==true) {
      array_push($arrysession,get_object_vars($ii)['subpage']);

   }

    }
  }


  if(get_object_vars($i)['mainpage']=='MASTER' && get_object_vars($i)['status']==true){$MASTER='style="display:show;"';
    $subpagearr=get_object_vars($i)['subpage'];
    foreach($subpagearr as $ii){
      if(get_object_vars($ii)['subpage']=='Additional Master' && get_object_vars($ii)['status']==true){$AdditionalMaster='style="display:show;"';}
      if(get_object_vars($ii)['subpage']=='Item Group Master' && get_object_vars($ii)['status']==true){$ItemGroupMaster='style="display:show;"';}
      if(get_object_vars($ii)['subpage']=='Item Master' && get_object_vars($ii)['status']==true){$ItemMaster='style="display:show;"';}
      if(get_object_vars($ii)['subpage']=='Item Master ReOrder' && get_object_vars($ii)['status']==true){$ItemMasterReOrder='style="display:show;"';}
      if(get_object_vars($ii)['subpage']=='Supplier Master' && get_object_vars($ii)['status']==true){$SupplierMaster='style="display:show;"';}
      if(get_object_vars($ii)['subpage']=='Item Rate Master' && get_object_vars($ii)['status']==true){$ItemRateMaster='style="display:show;"';}
      if(get_object_vars($ii)['subpage']=='Customer Master' && get_object_vars($ii)['status']==true){$CustomerMaster='style="display:show;"';}
      if(get_object_vars($ii)['subpage']=='Branch Master' && get_object_vars($ii)['status']==true){$BranchMaster='style="display:show;"';}
      if(get_object_vars($ii)['subpage']=='Service Master' && get_object_vars($ii)['status']==true){$ServiceMaster='style="display:show;"';}
      if(get_object_vars($ii)['subpage']=='Employee Login Master' && get_object_vars($ii)['status']==true){$EmployeeLoginMaster='style="display:show;"';}
      if(get_object_vars($ii)['subpage']=='Machine/Assets Master' && get_object_vars($ii)['status']==true){$MachineAssetsMaster='style="display:show;"';}
      if(get_object_vars($ii)['subpage']=='Commodity Master' && get_object_vars($ii)['status']==true){$commoditymaster='style="display:show;"';}
      if(get_object_vars($ii)['subpage']=='Accreditation Master' && get_object_vars($ii)['status']==true){$accreditationmaster='style="display:show;"';}
      if(get_object_vars($ii)['subpage']=='Country Master' && get_object_vars($ii)['status']==true){$countrymaster='style="display:show;"';}
      if(get_object_vars($ii)['subpage']=='Temperature Master' && get_object_vars($ii)['status']==true){$temperaturemaster='style="display:show;"';}
      if(get_object_vars($ii)['subpage']=='Sales Person Master' && get_object_vars($ii)['status']==true){$salesperson='style="display:show;"';}
      if(get_object_vars($ii)['subpage']=='Document' && get_object_vars($ii)['status']==true){$Document='style="display:show;"';}
      if(get_object_vars($ii)['subpage']=='Employee Stock' && get_object_vars($ii)['status']==true){$EmployeeStock='display:show;';}
      if(get_object_vars($ii)['subpage']=='Employee Stock Report' && get_object_vars($ii)['status']==true){$EmployeeStockReport='display:show;';}
      if(get_object_vars($ii)['subpage']=='Account Head' && get_object_vars($ii)['status']==true){$AccountHead='style="display:show;"';}
      if(get_object_vars($ii)['subpage']=='Expenses' && get_object_vars($ii)['status']==true){$Expenses='style="display:show;"';}


      if (get_object_vars($ii)['status']==true) {
        array_push($arrysession,get_object_vars($ii)['subpage']);

     }

    }
  }
  if(get_object_vars($i)['mainpage']=='SALARY_MASTER' && get_object_vars($i)['status']==true){$SALARY_MASTER='style="display:show;"';
    $subpagearr=get_object_vars($i)['subpage'];
    foreach($subpagearr as $ii){
    if(get_object_vars($ii)['subpage']=='Salary Master' && get_object_vars($ii)['status']==true){$SalaryMaster='style="display:show;"';}
    if(get_object_vars($ii)['subpage']=='Leave Opening Balance' && get_object_vars($ii)['status']==true){$LeaveOpeningBalance='style="display:show;"';}
    if(get_object_vars($ii)['subpage']=='Professional Tax Master' && get_object_vars($ii)['status']==true){$ProfessionalTaxMaster='style="display:show;"';}
    if(get_object_vars($ii)['subpage']=='Paid Holidays' && get_object_vars($ii)['status']==true){$PaidHolidays='style="display:show;"';}
    if(get_object_vars($ii)['subpage']=='Monthly Attendance' && get_object_vars($ii)['status']==true){$MonthlyAttendance='style="display:show;"';}
    if(get_object_vars($ii)['subpage']=='Leave Allotment' && get_object_vars($ii)['status']==true){$LeaveAllotment='style="display:show;"';}
    if(get_object_vars($ii)['subpage']=='Loan Advance Details' && get_object_vars($ii)['status']==true){$LoanAdvanceDetails='style="display:show;"';}
    if(get_object_vars($ii)['subpage']=='Salary Processing' && get_object_vars($ii)['status']==true){$SalaryProcessing='style="display:show;"';}
    if (get_object_vars($ii)['status']==true) {
      array_push($arrysession,get_object_vars($ii)['subpage']);

   }

  }
}

if(get_object_vars($i)['mainpage']=='COMMISSION_MASTER' && get_object_vars($i)['status']==true){$COMMISSION_MASTER='style="display:show;"';
    $subpagearr=get_object_vars($i)['subpage'];
    foreach($subpagearr as $ii){
    if(get_object_vars($ii)['subpage']=='Commission Request' && get_object_vars($ii)['status']==true){$CommissionRequest='style="display:show;"';}
    if(get_object_vars($ii)['subpage']=='Commission Approval' && get_object_vars($ii)['status']==true){$CommissionApproval='style="display:show;"';}
    if(get_object_vars($ii)['subpage']=='Bill Paid' && get_object_vars($ii)['status']==true){$BillPaid='style="display:show;"';}
    if(get_object_vars($ii)['subpage']=='Commission Master Agent' && get_object_vars($ii)['status']==true){$CommissionMasterAgent='style="display:show;"';}

    if (get_object_vars($ii)['status']==true) {
      array_push($arrysession,get_object_vars($ii)['subpage']);

   }

  }
}

if(get_object_vars($i)['mainpage']=='CREDIT AND DEBIT NOTE' && get_object_vars($i)['status']==true){$CREDITANDDEBITNOTE='style="display:show;"';
  $subpagearr=get_object_vars($i)['subpage'];
  foreach($subpagearr as $ii){
   if(get_object_vars($ii)['subpage']=='Credit Note' && get_object_vars($ii)['status']==true){$creditNote='display:show;';}
   if(get_object_vars($ii)['subpage']=='Debit Note' && get_object_vars($ii)['status']==true){$debitNote='style="display:show;"';}
   if (get_object_vars($ii)['status']==true) {
    array_push($arrysession,get_object_vars($ii)['subpage']);

 }

  }
}

}

?>

<!DOCTYPE html>
<html lang="en">
<head>
  <title><?php echo $title; ?></title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
  <link rel="stylesheet" href="<?php echo $base; ?>css/bootstrap.min.css">
  <link rel="stylesheet" href="<?php echo $base; ?>css/main.css">
  <?php echo $css; ?>
  <script src="<?php echo $base; ?>js/jquery-3.3.1.min.js"></script>
  <script src="<?php echo $base; ?>js/bootstrap.min.js"></script>
  <script src="<?php echo $base; ?>js/main.js"></script>
  <script src="<?php echo $base; ?>js/select2.min.js"></script>
  <?php echo $js; ?>
 <style>
    @media only screen and (max-width: 600px) {
      .fontter{
      font-size:20px!important;
    }
    }
    .fontter{
      font-size:30px;
    }

    input {font-weight:bold;}
    input[type="text"]
{
    font-size:13px;
}
    .navbar .dropdown-menu.notify-drop {
      min-width: 330px;
      background-color: #fff;
      min-height: 360px;
      max-height: 360px;
    }
    .navbar .dropdown-menu.notify-drop .notify-drop-title {
      border-bottom: 1px solid #e2e2e2;
      padding: 5px 15px 10px 15px;
    }
    .navbar .dropdown-menu.notify-drop .drop-content {
      min-height: 280px;
      max-height: 280px;
      overflow-y: scroll;
    }
    .navbar .dropdown-menu.notify-drop .drop-content::-webkit-scrollbar-track
    {
      background-color: #F5F5F5;
    }

    .navbar .dropdown-menu.notify-drop .drop-content::-webkit-scrollbar
    {
      width: 8px;
      background-color: #F5F5F5;
    }

    .navbar .dropdown-menu.notify-drop .drop-content::-webkit-scrollbar-thumb
    {
      background-color: #ccc;
    }
    .navbar .dropdown-menu.notify-drop .drop-content > li {
      border-bottom: 1px solid #e2e2e2;
      padding: 10px 0px 5px 0px;
    }
    .navbar .dropdown-menu.notify-drop .drop-content > li:nth-child(2n+0) {
      background-color: #fafafa;
    }
    .navbar .dropdown-menu.notify-drop .drop-content > li:after {
      content: "";
      clear: both;
      display: block;
    }
    .navbar .dropdown-menu.notify-drop .drop-content > li:hover {
      background-color: #fcfcfc;
    }
    .navbar .dropdown-menu.notify-drop .drop-content > li:last-child {
      border-bottom: none;
    }
    .navbar .dropdown-menu.notify-drop .drop-content > li .notify-img {
      float: left;
      display: inline-block;
      width: 45px;
      height: 45px;
      margin: 0px 0px 8px 0px;
    }
    .navbar .dropdown-menu.notify-drop .allRead {
      margin-right: 7px;
    }
    .navbar .dropdown-menu.notify-drop .rIcon {
      float: right;
      color: #999;
    }
    .navbar .dropdown-menu.notify-drop .rIcon:hover {
      color: #333;
    }
    .navbar .dropdown-menu.notify-drop .drop-content > li a {
      font-size: 12px;
      font-weight: normal;
    }
    .navbar .dropdown-menu.notify-drop .drop-content > li {
      font-weight: bold;
      font-size: 11px;
    }
    .navbar .dropdown-menu.notify-drop .drop-content > li hr {
      margin: 5px 0;
      width: 70%;
      border-color: #e2e2e2;
    }
    .navbar .dropdown-menu.notify-drop .drop-content .pd-l0 {
      padding-left: 0;
    }
    .navbar .dropdown-menu.notify-drop .drop-content > li p {
      font-size: 11px;
      color: #666;
      font-weight: normal;
      margin: 3px 0;
    }
    .navbar .dropdown-menu.notify-drop .drop-content > li p.time {
      font-size: 10px;
      font-weight: 600;
      top: -6px;
      margin: 8px 0px 0px 0px;
      padding: 0px 3px;
      border: 1px solid #e2e2e2;
      position: relative;
      background-image: linear-gradient(#fff,#f2f2f2);
      display: inline-block;
      border-radius: 2px;
      color: #B97745;
    }
    .navbar .dropdown-menu.notify-drop .drop-content > li p.time:hover {
      background-image: linear-gradient(#fff,#fff);
    }
    .navbar .dropdown-menu.notify-drop .notify-drop-footer {
      border-top: 1px solid #e2e2e2;
      bottom: 0;
      position: relative;
      padding: 8px 15px;
    }
    .navbar .dropdown-menu.notify-drop .notify-drop-footer a {
      color: #777;
      text-decoration: none;
    }
    .navbar .dropdown-menu.notify-drop .notify-drop-footer a:hover {
      color: #333;
    }
 </style>
</head>
<body>

<nav class="navbar" style="background:#ddd;border-radius:0px!important;box-shadow: 0 2px 5px 0 rgba(0,0,0,.16), 0 2px 10px 0 rgba(0,0,0,.12);">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="btn btn-primary" id="mainnav" style="margin:10px;" onclick="openNav(this)" style="float:left;margin-left: 10px;" data-toggle="collapse" data-target="#myNavbar">&#9776; </button>
      <img src="/img/PestMortemLogo.jpg" width="80" height="40"/><span class="fontter" style="padding-left:10px;text-shadow: 0 1px 2px rgba(0,0,0,.4);font-family: 'Times New Roman', Times, serif;"><?php echo $industry_name; ?></span>
    </div>
    <div class="mobhide" style="float:right;padding:10px;">
      <div id="div-extra-menus" style="display:inline;"></div>
          <a class="btn btn-success btn-sm" id="branch-nav-inp-btn" href="/_api/login/logout.php" > </a>
          <!-- <a class="btn btn-danger btn-sm" href="/_api/login/logout.php" ><?php echo $_SESSION['username']; ?></a> -->
          <a class="btn btn-danger btn-sm" href="/process/profile.php" ><?php echo $_SESSION['username']; ?></a>
          <a class="btn btn-danger btn-sm" href="/_api/login/logout.php" >Log Off</a>
          <a class="btn btn-default btn-sm"> <?php echo 'Version : PMI 1.1 : '.date("d-m-Y",filemtime($base."_in/header.php")); ?> </a>
    </div>
  </div>
</nav>
<div id="mySidenav" class="sidenav" style="background:#ddd;box-shadow: 0 2px 5px 0 rgba(0,0,0,.16), 0 2px 10px 0 rgba(0,0,0,.12);">
  <ul style="list-style: none;padding:10px;">
  <li <?php echo $DASHBOARD; ?> class="nav-border"><a style="padding:0;margin:0;" href="/process/dashboard.php"><img src='/img/icon/dashboard.png'  height="30" width="30" class="img-responsive img-nav" title="Dashboard"/><span class="viewer">Dashboard<span></a></li>
  <li <?php echo $LEDGER; ?> class="nav-border"><a style="padding:0;margin:0;" href="/process/ledger/customerledger.php"><img src='/img/icon/ledger.png'  height="30" width="30" class="img-responsive img-nav" title="CRM"/><span class="viewer">CRM<span></a></li>
  <li <?php echo $OVERDUE; ?> class="nav-border"><a style="padding:0;margin:0;" href="/process/overdue/customerbill.php"><img src='/img/icon/overdue.png'  height="30" width="30" class="img-responsive img-nav" title="Overdues"/><span class="viewer">Overdues<span></a></li>
  <li <?php echo $ENQUIRY; ?> class="nav-border"><a style="padding:0;margin:0;" href="/process/enquiry/index.php"><img src='/img/icon/enquiry.png' height="30" width="30" class="img-responsive img-nav" title="Enquiry" /><span class="viewer">Enquiry<span></a></li>
  <li <?php echo $QUOTATION; ?> class="nav-border"><a style="padding:0;margin:0;" href="/process/quatation/index.php"><img src='/img/icon/quatation.png' height="30" width="30" class="img-responsive img-nav" title="Quotation" /><span class="viewer">Quotation<span></a></li>
  <li <?php echo $CRM; ?> class="nav-border"><a style="padding:0;margin:0;" href="/process/crm/index.php"><img src='/img/icon/crm.png' height="30" width="30" class="img-responsive img-nav" title="Job Work" /><span class="viewer">Job Work<span></a></li>
  <li <?php echo $INVOICE; ?> class="nav-border"><a style="padding:0;margin:0;" href="/process/invoice/index.php"><img src='/img/icon/invoice.png' height="30" width="30" class="img-responsive img-nav" title="INVOICE" /><span class="viewer">Invoice<span></a></li>
  <li <?php echo $INVOICEPAYMENT; ?> class="nav-border"><a style="padding:0;margin:0;" href="/process/invoicepayment/index.php"><img src='/img/icon/invoice-payment.png' height="30" width="30" class="img-responsive img-nav" title="INVOICE PAYMENT" /><span class="viewer">Invoice Payment<span></a></li>
  <li <?php echo $AMC; ?> class="nav-border"><a style="padding:0;margin:0;" href="/process/amc/index.php"><img src='/img/icon/contract.png' height="30" width="30" class="img-responsive img-nav" title="AMC" /><span class="viewer">AMC<span></a></li>
  <li <?php echo $CERTIFICATE; ?> class="nav-border"><a style="padding:0;margin:0;" href="/process/certificate/index.php"><img src='/img/icon/certificate.png' height="30" width="30" class="img-responsive img-nav" title="Certificate" /><span class="viewer">Certificate<span></a></li>
  <li <?php echo $CALCULATION; ?> class="nav-border"><a style="padding:0;margin:0;" href="/process/calculation/index.php"><img src='/img/icon/calculator.png' height="30" width="30" class="img-responsive img-nav" title="Calculation" /><span class="viewer">Calculation<span></a></li>
  <li <?php echo $PURCHASE; ?> class="nav-border"><a style="padding:0;margin:0;" href="/process/purchase/index.php"><img src='/img/icon/transaction.png' height="30" width="30" class="img-responsive img-nav" title="Purchase" /><span class="viewer">Purchase<span></a></li>
  <li <?php echo $STOCK; ?> class="nav-border"><a style="padding:0;margin:0;" href="/process/stock/index.php"><img src='/img/icon/stock.png' height="30" width="30" class="img-responsive img-nav" title="Stock" /><span class="viewer">Stock<span></a></li>
  <li <?php echo $MASTER; ?> class="nav-border"><img src='/img/icon/create-master.png' class="img-responsive img-nav"  height="30" width="30" title="Masters" onclick="masterexpand()" /><span onclick="masterexpand()" class="viewer"> Masters</span><div class="viewer">
    <ul class="masterexpand">

      <li <?php echo $AdditionalMaster;  ?>><a href="/process/create-master/additional_master/index.php" class="linker">Additional Master</a></li>
      <li <?php echo $ItemGroupMaster;  ?>><a href="/process/create-master/item-group/index.php" class="linker">Item Group Master</a></li>
      <li <?php echo $ItemMaster;  ?>><a href="/process/create-master/item-master/index.php" class="linker"> Item Master</a></li>
      <li <?php echo $ItemMasterReOrder;  ?>><a href="/process/create-master/item-master-reorder/index.php" class="linker"> Item Master ReOrder</a></li>
      <li <?php echo $SupplierMaster;  ?>><a href="/process/create-master/supplier-master/supplier.php" class="linker"> Supplier Master</a></li>
      <li <?php echo $ItemRateMaster;  ?>><a href="/process/create-master/supplier-item-rate-master/index.php" class="linker"> Item Rate Master</a></li>
      <li <?php echo $CustomerMaster;  ?>><a href="/process/create-master/customer-master/index.php" class="linker"> Customer Master</a></li>
     <?php  if($_SESSION['role']=='admin'){ ?>
      <li <?php echo $BranchMaster;  ?>><a href="/process/create-master/branch/index.php" class="linker"> Branch Master</a></li>
      <?php } ?>
      <li <?php echo $ServiceMaster; ?>><a href="/process/create-master/service/index.php" class="linker"> Service Master</a></li>
      <li <?php echo $EmployeeLoginMaster; ?>><a href="/process/create-master/employee/index.php" class="linker"> Employee Login Master</a></li>
      <li <?php echo $MachineAssetsMaster; ?>><a href="/process/create-master/machine-master/index.php" class="linker">Machine/Assets Master</a>
      <li <?php echo $commoditymaster;  ?>><a href="/process/create-master/commodity-master/index.php" class="linker"> Commodity Master</a></li>
      <li <?php echo $accreditationmaster;  ?>><a href="/process/create-master/accreditation-master/index.php" class="linker"> Accreditation Master</a></li>
      <li <?php echo $countrymaster;  ?>><a href="/process/create-master/country-master/index.php" class="linker"> Country Master</a></li>
      <li <?php echo $countrymaster;  ?>><a href="/process/create-master/temperature-master/index.php" class="linker"> Temperature Master</a></li>
      <li <?php echo $salesperson;  ?>><a href="/process/create-master/salesperson/index.php" class="linker"> Sales Person Master</a></li>
      <li <?php echo $Document;  ?>><a href="/process/create-master/document/index.php" class="linker"> Document Upload</a></li>
      <li <?php echo $EmployeeStock; ?>><a href="/process/create-master/employee-stock/employee-stock.php" class="linker">Employee Stock</a></li>
      <li <?php echo $AccountHead; ?>><a href="/process/create-master/account/account-head.php" class="linker">Account Head</a></li>
      <li <?php echo $Expenses; ?>><a href="/process/create-master/expenses/expenses.php" class="linker">Expenses</a></li>

    </ul>
  </li>
  <li <?php echo $SALARY_MASTER; ?> class="nav-border"><img src='/img/icon/create-master.png' class="img-responsive img-nav"  height="30" width="30" title="Masters" onclick="salarymasterexpand()" /><span onclick="salarymasterexpand()" class="viewer"> Salary Masters</span><div class="viewer">
    <ol class="salarymasterexpand">   
      <li> Masters 
        <ul>

          <li <?php echo $SalaryMaster; ?> ><a href="/process/create-master/salary/index.php" class="linker"> Salary Master</a></li>
          <li <?php echo $LeaveOpeningBalance; ?> ><a href="/process/create-master/leaveopeningbalance/index.php" class="linker"> Leave Opening Balance</a></li>
          <li <?php echo $ProfessionalTaxMaster; ?> ><a href="/process/create-master/professionaltax-master/index.php" class="linker"> Professional Tax Master</a></li>
          <li <?php echo $PaidHolidays; ?> ><a href="/process/create-master/paidholidays/index.php" class="linker"> Paid Holidays</a></li>
        </ul>
      </li>

      <li>Transaction
        <ul>
            <li <?php echo $MonthlyAttendance; ?> ><a href="/process/create-master/attendance-master/index.php" class="linker">Monthly Attendance</a></li>
            <li <?php echo $LeaveAllotment; ?> ><a href="/process/create-master/leaveallotment/index.php" class="linker"> Leave Allotment</a></li>
            <li <?php echo $LoanAdvanceDetails; ?> ><a href="/process/create-master/loanadvance/index.php" class="linker"> Loan Advance Details</a></li>
            <li <?php echo $SalaryProcessing; ?> ><a href="/process/create-master/salaryprocess/index.php" class="linker">Salary Processing</a></li>
              <!-- <li><a href="/process/create-master/attendance/attendancecard.php" class="linker"> Attendance Card</a></li> -->
        </ul>
      </li>
    </ol>
  </li>

  <li <?php echo $COMMISSION_MASTER; ?> class="nav-border"><a style="padding:0;margin:0;" href="/process/commission-master/commssion-management/index.php"><img src='/img/icon/commission.png' height="30" width="30" class="img-responsive img-nav" title="Commission Masters" /><span class="viewer">Commission Masters<span></a></li>

  <li <?php echo $CREDITANDDEBITNOTE; ?> class="nav-border"><a style="padding:0;margin:0;" href="/process/creditanddebitnote/index.php"><img src='/img/icon/atm.png'  height="30" width="30" class="img-responsive img-nav" title="Credit & Debit Note"/><span class="viewer">Crebit & Debit Note<span></a></li>

<?php  if($_SESSION['role']=='admin' || $_SESSION['role']=='login manager'){ ?>
    <li class="nav-border"><a style="padding:0;margin:0;" onclick="backupdb()"><img src='/img/icon/backup.png' class="img-responsive img-nav"  height="30" width="30" title="Add New User"/><span class="viewer">Database Backup<span></a></li>
<?php } ?>
  <li class="nav-border"><a style="padding:0;margin:0;" href="/process/change-password.php"><img src='/img/icon/change-password.png'  height="30" width="30" class="img-responsive img-nav" title="Add New User"/><span class="viewer">Change Password<span></a></li>
  <!-- <li class="nav-border"><a style="padding:0;margin:0;" href="/_api/login/logout.php"><img src='/img/icon/logout.png' class="img-responsive img-nav"  height="30" width="30" title="Logout" /><span class="viewer">Logoff<span></a></li> -->
  
  </ul>
</div>

<input type="hidden" id='accessrole' value="<?php echo $_SESSION['role']; ?>" /> 
<input type="hidden" id='inp-session-branchid' value="<?php echo $_SESSION['branchid']; ?>" />


