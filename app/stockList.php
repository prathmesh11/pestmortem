<?php

    include 'include/config.php';

	include 'include/admin-functions.php';

    header('content-type: application/json; charset=utf-8');

    header("access-control-allow-origin: *");

	$admin = new AdminFunctions();
    
    if (isset($_POST['employeeid']) && !empty($_POST['employeeid'])){

        $employeeid = $admin->escape_string($admin->strip_all($_POST['employeeid']));

        if(empty($employeeid)) {

			$response      = "Unable to Show The List, Employee Id Not Found";

            $obj['status'] = $response;

		}  else {

            $results = $admin->getUniqueStockListByEmployeeID($employeeid);

            if($results->num_rows > 0) {

                while($row = $admin->fetch($results)){

                    $itemcode   = $row['itemcode'];

                    $itemname   = $admin->fetch($admin->query("SELECT itemname x FROM `stockmaster` WHERE stockid = '$itemcode'"))['x'];
                    
                    $balanceQty = $admin->fetch($admin->query("SELECT balance_qty y FROM `employeestock` WHERE itemcode = '$itemcode' order by id desc limit 1"))['y'];

                    
                    $obj1                 = array();
                    $obj1['status']       = 'success';
                    $obj1['itemname']     = $itemname;
                    $obj1['qty']          = $balanceQty;
					$jobArr[]             = $obj1;

                }

				$obj['status'] = $jobArr;
                
            } else {
			
                $response      = "Job Not Avaiable";

                $obj['status'] = $response;

			}

        }

    } else {

        $obj['status'] = "failed";

    }

    echo json_encode($obj);
?>