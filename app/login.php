<?php

    include 'include/config.php';

	include 'include/admin-functions.php';

    header('content-type: application/json; charset=utf-8');

    header("access-control-allow-origin: *");

	$admin = new AdminFunctions();
    
    if (isset($_POST['mobileno']) && !empty($_POST['mobileno']) && isset($_POST['password']) && !empty($_POST['password']) && isset($_POST['deviceid']) && !empty($_POST['deviceid'])){

        $mobileno = $admin->escape_string($admin->strip_all($_POST['mobileno']));
		$password = $admin->escape_string($admin->strip_all($_POST['password']));
		$deviceid = $admin->escape_string($admin->strip_all($_POST['deviceid']));

        if(empty($mobileno) || !(is_numeric($mobileno)) || !(strlen($mobileno)==10)) {
			$response = "Unable to login, Please enter a valid mobile number";
            $obj['status'] = $response;
		} else if(empty($password)) {
            $response = "Unable to login, Please enter a valid Password";
            $obj['status'] = $response;
		} else if(empty($deviceid)) {
			$response = "Unable to login, Please enter a Device Not Found";
            $obj['status'] = $response;
		} else {

            $check = $admin->adminlogin($mobileno,$password);

            if($check->num_rows > 0) {
				$userDetails = $admin->fetch($check);

                if($userDetails['phone'] == $mobileno &&  $userDetails['password'] == $password) {

                    $query = "update ".PREFIX."employeemaster set deviceid='".$deviceid."' where id = '".$userDetails['id']."'";
					$admin->query($query);

                    $obj['userId'] = "".$userDetails['id'];
					$obj['mobileno'] = "".$userDetails['phone'];
					$obj['username'] = $userDetails['username'];
                    $obj['branchid'] = $userDetails['branchid'];
					$obj['status'] = "success";


                } else {

                    $response = "Unable to login, Something Went Wrong";

                    $obj['status'] = $response;

                }
            } else {
			
                $response = "Unable to login, Please register with your mobile number ".$mobileno." before login to continue.";
                $obj['status'] = $response;
			}

        }

    } else {

        $obj['status'] = "failed";

    }

    echo json_encode($obj);
?>