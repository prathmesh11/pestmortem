<?php

    include 'include/config.php';

	include 'include/admin-functions.php';

    header('content-type: application/json; charset=utf-8');

    header("access-control-allow-origin: *");

	$admin = new AdminFunctions();
    
    if (isset($_POST['employeeid']) && !empty($_POST['employeeid']) && isset($_POST['jobid']) && !empty($_POST['jobid']) && isset($_POST['reschedule']) && !empty($_POST['reschedule'])){

        $employeeid = $admin->escape_string($admin->strip_all($_POST['employeeid']));

        $jobid      = $admin->escape_string($admin->strip_all($_POST['jobid']));

        $reschedule = $admin->escape_string($admin->strip_all($_POST['reschedule']));


        if(empty($employeeid)) {

			$response      = "Unable to login, Employee Id Not Found";

            $obj['status'] = $response;

		} else if (empty($jobid)) {

            $response      = "Unable to Reschedule, Job Id Not Found";

            $obj['status'] = $response;

        } else if (empty($reschedule)) {

            $response      = "Unable to Reschedule, Reschedule Reason Not Avaible";

            $obj['status'] = $response;

        } else {

            $results = $admin->getUniqueJobdetails($employeeid,$jobid);

            if($results->num_rows > 0) {

                $userDetails = $admin->fetch($results);

                if($userDetails['employeeid'] == $employeeid && $userDetails['service_id'] == $jobid ) {

                           

                    $query  = $admin->query("update ".PREFIX."job_allotment set reschedule='".$reschedule."' where employeeid = '".$employeeid."' AND service_id = '".$jobid."' ");
                    $update = $admin->query("update ".PREFIX."workcontractservice set reschedule='".$reschedule."' where serviceid = '".$jobid."'");

                    if ($update) {

                        $obj['status']     = "success";
                        
                    } else {

                        $obj['status'] = "failed1";

                    }

                } else {

                    $obj['status'] = "failed1";

                }

                
            } else {
			
                $response      = "Job Not Avaiable";

                $obj['status'] = $response;

			}

        }

    } else {

        $obj['status'] = "failed";

    }

    echo json_encode($obj);
?>