<?php

    include 'include/config.php';

	include 'include/admin-functions.php';

    header('content-type: application/json; charset=utf-8');

    header("access-control-allow-origin: *");

	$admin = new AdminFunctions();
    
    if (isset($_POST['employeeid']) && !empty($_POST['employeeid']) && isset($_POST['latitude']) && !empty($_POST['latitude']) && isset($_POST['longitude']) && !empty($_POST['longitude'])){

        $employeeid = $admin->escape_string($admin->strip_all($_POST['employeeid']));
		
        $latitude   = $admin->escape_string($admin->strip_all($_POST['latitude']));
		
        $longitude  = $admin->escape_string($admin->strip_all($_POST['longitude']));

        if(empty($employeeid)) {

			$response      = "Unable to Find Location, Employee Id Not Found";

            $obj['status'] = $response;

		} else if(empty($latitude)) {

			$response      = "Unable to Find Location, Latitude Not Found";

            $obj['status'] = $response;

		} else if(empty($longitude)) {

			$response      = "Unable to Find Location, Longitude Not Found";

            $obj['status'] = $response;

		} else {

            $result = $admin->addEmployesCurrentlatAndLong($employeeid,$latitude,$longitude);

            if ($result) {

                $obj['status'] = 'success';
                
            } else {

                $obj['status'] = "failed";

            }


        }

    } else {

        $obj['status'] = "failed";

    }

    echo json_encode($obj);
?>