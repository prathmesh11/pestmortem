<?php

    include 'include/config.php';

	include 'include/admin-functions.php';

    header('content-type: application/json; charset=utf-8');

    header("access-control-allow-origin: *");

	$admin = new AdminFunctions();
    
    if (isset($_POST['employeeid']) && !empty($_POST['employeeid']) && isset($_POST['limit']) && !empty($_POST['limit'])){

        $employeeid = $admin->escape_string($admin->strip_all($_POST['employeeid']));

        $limit      = $admin->escape_string($admin->strip_all($_POST['limit']));

        if(empty($employeeid)) {

			$response      = "Unable to Show The List, Employee Id Not Found";

            $obj['status'] = $response;

		} else if(empty($limit)) {

			$response      = "Unable to Show The List, Limit Not Found";

            $obj['status'] = $response;

		} else {

            $results = $admin->getUniqueExpensesByEmployeeID($employeeid,$limit);

            if($results->num_rows > 0) {

                while($row = $admin->fetch($results)){
                    
                    $obj1                 = array();
                    $obj1['status']       = 'success';
                    $obj1['date']         =  date("d-m-Y", strtotime($row['expensesdate']));
                    $obj1['desciption']   = $row['expencesName'];
                    $obj1['amount']       = $row['amount'];
					$jobArr[]             = $obj1;

                }

				$obj['status'] = $jobArr;
                
            } else {
			
                $response      = "Job Not Avaiable";

                $obj['status'] = $response;

			}

        }

    } else {

        $obj['status'] = "failed";

    }

    echo json_encode($obj);
?>