<?php

    include 'include/config.php';

	include 'include/admin-functions.php';

    header('content-type: application/json; charset=utf-8');

    header("access-control-allow-origin: *");

	$admin = new AdminFunctions();
    
    if (isset($_POST['employeeid']) && !empty($_POST['employeeid'])){

        $employeeid = $admin->escape_string($admin->strip_all($_POST['employeeid']));

        if(empty($employeeid)) {

			$response      = "Unable to login, Employee Id Not Found";

            $obj['status'] = $response;

		} else {

            $results = $admin->getUniqueJoblistByEmployeeID($employeeid);

            if($results->num_rows > 0) {

                while($row = $admin->fetch($results)){

                    $JobStartDate = date("d-m-Y",$row['startdate']/1000);

                    $contractid = $row['contractid'];
                    
                    $rowss      = $admin->fetch($admin->query("SELECT *,(Select customername FROM customermaster WHERE workcontract.customerid = customermaster.customerid) as customername FROM workcontract WHERE contractid = '".$contractid."'"));

                    $obj1                 = array();
                    $obj1['status']       = 'success';
                    $obj1['service_id']   = $row['service_id'];
                    $obj1['jobdatetime']  = $JobStartDate;
                    $obj1['customername'] = $rowss['customername'];
					$jobArr[]             = $obj1;

                }

				$obj['status'] = $jobArr;
                
            } else {
			
                $response      = "Job Not Avaiable";

                $obj['status'] = $response;

			}

        }

    } else {

        $obj['status'] = "failed";

    }

    echo json_encode($obj);
?>