<?php

    include 'include/config.php';

	include 'include/admin-functions.php';

    header('content-type: application/json; charset=utf-8');

    header("access-control-allow-origin: *");

	$admin = new AdminFunctions();
    
    if (isset($_POST['employeeid']) && !empty($_POST['employeeid'])){

        $employeeid = $admin->escape_string($admin->strip_all($_POST['employeeid']));

        if(empty($employeeid)) {

			$response = "Unable to log Out, Employee Id Not Found";

            $obj['status'] = $response;

		}  else {

            $obj['status'] = "success";

        }

    } else {

        $obj['status'] = "failed";

    }

    echo json_encode($obj);
?>