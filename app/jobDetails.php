<?php

    include 'include/config.php';

	include 'include/admin-functions.php';

    header('content-type: application/json; charset=utf-8');

    header("access-control-allow-origin: *");

	$admin = new AdminFunctions();
    
    if (isset($_POST['employeeid']) && !empty($_POST['employeeid']) && isset($_POST['jobid']) && !empty($_POST['jobid']) ){

        $employeeid = $admin->escape_string($admin->strip_all($_POST['employeeid']));

        $jobid      = $admin->escape_string($admin->strip_all($_POST['jobid']));


        if(empty($employeeid)) {

			$response      = "Unable to login, Employee Id Not Found";

            $obj['status'] = $response;

		} else if (empty($jobid)) {

            $response      = "Unable to login, Job Id Not Found";

            $obj['status'] = $response;

        } else {

            $results = $admin->getUniqueJobdetails($employeeid,$jobid);

            if($results->num_rows > 0) {

                $userDetails = $admin->fetch($results);

                if($userDetails['employeeid'] == $employeeid && $userDetails['service_id'] == $jobid ) {

                    $JobStartDate = date("d-m-Y",$userDetails['startdate']/1000);

                    $contractid = $userDetails['contractid'];
                    
                    $rowss      = $admin->fetch($admin->query("SELECT *,(Select customername FROM customermaster WHERE workcontract.customerid = customermaster.customerid) as customername FROM workcontract WHERE contractid = '".$contractid."'"));

                    $obj['status']       = "success";

                    $obj['service_id']   = $userDetails['service_id'];

                    $obj['customername'] = $rowss['customername'];
                    
                    $obj['location']     = $rowss['customeraddress'];
                    
                    $obj['latitude']     = ''; 

                    $obj['longitude']    = '';   

                    $obj['jobdatetime']  = $JobStartDate;   

                    $obj['contactno']    = '';                    


                } else {

                    $obj['status'] = "failed1";

                }

                
            } else {
			
                $response      = "Job Not Avaiable";

                $obj['status'] = $response;

			}

        }

    } else {

        $obj['status'] = "failed";

    }

    echo json_encode($obj);
?>