<?php

    include 'include/config.php';

	include 'include/admin-functions.php';

    header('content-type: application/json; charset=utf-8');

    header("access-control-allow-origin: *");

	$admin = new AdminFunctions();
    
    if (isset($_POST['employeeid']) && !empty($_POST['employeeid'])){

        $employeeid = $admin->escape_string($admin->strip_all($_POST['employeeid']));

        if(empty($employeeid)) {

			$response      = "Unable to login, Employee Id Not Found";

            $obj['status'] = $response;

		} else {

            $check = $admin->getUniqueAdminById($employeeid);

            if($check->num_rows > 0) {

				$userDetails = $admin->fetch($check);

                if($userDetails['id'] == $employeeid) {

                    $intime =  date('Y-m-d H:i:s', CURRENTMILIS/1000);

                    $query  = "Insert into ".PREFIX."attendancemaster (branchid, employeeid, intime, created_by, created_time) VALUES ('".$userDetails['branchid']."', '".$employeeid."', '".$intime."','".$employeeid."', '".CURRENTMILIS."')";
					
                    $insert = $admin->query($query);

                    if ($insert ) {

                        $obj['status']     = "success";

                        $obj['indatetime'] = $intime;
                        
                    } else {

                        $obj['status'] = "failed1";

                    }
					
                } else {

                    $response      = "Unable to login, Something Went Wrong";

                    $obj['status'] = $response;

                }

            } else {
			
                $response      = "Unable to login, Please register with your mobile number ".$mobileno." before Attendance In.";

                $obj['status'] = $response;
			}

        }

    } else {

        $obj['status'] = "failed";

    }

    echo json_encode($obj);
?>