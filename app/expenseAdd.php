<?php

    include 'include/config.php';

	include 'include/admin-functions.php';

    header('content-type: application/json; charset=utf-8');

    header("access-control-allow-origin: *");

	$admin = new AdminFunctions();
    
    if (isset($_POST['employeeid']) && !empty($_POST['employeeid']) && isset($_POST['expensesdate']) && !empty($_POST['expensesdate']) && isset($_POST['desciption']) && !empty($_POST['desciption'])&& isset($_POST['amount']) && !empty($_POST['amount'])&& isset($_POST['jobid']) && !empty($_POST['jobid'])){

        $employeeid = $admin->escape_string($admin->strip_all($_POST['employeeid']));

        $jobid      = $admin->escape_string($admin->strip_all($_POST['jobid']));

        $expensesdate = $admin->escape_string($admin->strip_all($_POST['expensesdate']));

        $amount = $admin->escape_string($admin->strip_all($_POST['amount']));

        $desciption = $admin->escape_string($admin->strip_all($_POST['desciption']));

        if(empty($employeeid)) {

			$response      = "Unable to Add Expenses, Employee Id Not Found";

            $obj['status'] = $response;

		} else if (empty($jobid)) {

            $response      = "Unable to Add Expenses, Job Id Not Found";

            $obj['status'] = $response;

        } else if (empty($expensesdate)) {

            $response      = "Unable to Add Expenses, Date Not Avaible";

            $obj['status'] = $response;

        } else if (empty($desciption)) {

            $response      = "Unable to Add Expenses, Desciption Not Selected";

            $obj['status'] = $response;

        } else if (empty($expensesdate)) {

            $response      = "Unable to Add Expenses, Amount Not Avaible";

            $obj['status'] = $response;

        } else {

            $results = $admin->getUniqueJobdetails($employeeid,$jobid);

            if($results->num_rows > 0) {

                $userDetails = $admin->fetch($results);

                $query   = "Insert into ".PREFIX."jobcompleteexpenses (branchid, contractid, serviceid, employeeid, expensesdate, expencesName, amount, created_by, created_time) VALUES ('".$userDetails['branchid']."', '".$userDetails['contractid']."', '".$jobid."','".$employeeid."', '".$expensesdate."', '".$desciption."', '".$amount."','".$employeeid."', '".CURRENTMILIS."')";
					
                $insert  = $admin->query($query);
    
                if ($insert ) {
    
                    $obj['status']     = "success";
                        
                } else {
    
                    $obj['status'] = "failed1";
    
                }

            } 

        }

    } else {

        $obj['status'] = "failed";

    }

    echo json_encode($obj);
?>